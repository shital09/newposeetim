/*
 * UART_interface.c
 *
 *  Created on: 09-Apr-2014
 *      Author: root
 */


#include <openssl/rsa.h>
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include <posapi.h>
#include <termios.h>
#include <uart.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "cstclog.h"
#include "cstc_defines.h"
#include "tcp_drvr.h"
#include "db_interface.h"
#include "uart_interface.h"
#include "cstc_error.h"
#include "clock.h"
#include "gprs_interface.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <ctype.h>
#include <zlib.h>


/* Execute the command using this shell program.  */
#define SHELL "/bin/sh"


#define	SIGPIPE	13
#define SIG_IGN	((__sighandler_t) 1)

extern void *memset (void *__s, int __c, size_t __n) __THROW __nonnull ((1));
extern int close (int __fd);
extern int usleep (__useconds_t __useconds);

extern int get_start_end_db_data(char *start_tkt_cnt,char *end_tkt_cnt);

#define baudrate B115200
#define uart_file_name "/dev/ttyS3"
#define SERIAL_API 1
#define MAC_ID_VERIFY_DISABLED 0

extern pthread_mutex_t upload_semaphore_waybill;
extern pthread_mutex_t upload_semaphore_schedule ;

//#define SERVER_ADDR "10.20.41.199"

packet_format_struct packet_format_struct_var;
waybill_mas_packet_id_struct waybill_mas_packet_id_struct_var;
sched_mas_packet_id_struct sched_mas_packet_id_struct_var;
route_mas_packet_id_struct route_mas_packet_id_struct_var;
fare_chart_packet_id_struct fare_chart_packet_id_struct_var;
duty_mas_packet_id_struct duty_mas_packet_id_struct_var;
req_for_wb_struct req_for_wb_struct_var;
req_wb_packet_id_struct req_wb_packet_id_struct_var;
extern waybill_master_struct waybill_master_struct_var;
//schedule_master_struct schedule_master_struct_var;
duty_master_struct *pduty_master_struct_var;
//route_master_struct route_master_struct_var;
//fare_chart_struct fare_chart_struct_var;
registration_response_struct registration_response_struct_var;
reg_response_packet_id_struct reg_response_packet_id_struct_var;
bs_sync_req_struct bs_sync_req_struct_var;
ack_packet_struct ack_packet_struct_var;
//transaction_log_struct * uart_transaction_log_struct_var;
download_response_struct download_response_struct_var;
download_response_packet_struct download_response_packet_struct_var;
download_transaction_log_struct download_transaction_log_struct_var;
extern master_status_struct master_status_struct_var;
extern duty_master_struct duty_master_struct_var;
extern all_flags_struct all_flags_struct_var;

extern volatile char upload_semaphore;

//static unsigned
//get_file_size (const char * file_name);
//static unsigned char *
//read_whole_file (const char * file_name);

int prev_end_tkt_count =0;

void memset_packet_format(void);

#if COMPRESSION_DECOMPRESSION
char *global_buff = NULL;
#else
char global_buff[2048] ={'\0'};
#endif

char g_checksumbuffer[50];
int global_iTemp=0;
static int global_temp ;
static int gUpdateDataFlag=0;
int New_pkt_recv_flag = 0;
int Total_packet_length = 0;
uLongf Uncompressed_data_length = 0;
//uLongf Return_uncompDataLen = 1500;
char Err_Msg[ERROR_MSG_BUFFER_LEN];
int Err_Len=0;
int RecordCntToRecv =0,TotalRecordCntRecvd =0,Global_rowCount=0,GDownload_pktCnt=0;
char global_schedule[SCHEDULE_NO_LEN];
char global_Rate_Id[SCHEDULE_NO_LEN];
char global_Route_Id[SCHEDULE_NO_LEN];
char global_waybill[WAYBILL_NO_LEN];

int rt_count=0;
int g_checksumFlag=0;
int MultipleScheduleFlag=0;


char *DB_Insert_str = NULL;
char *Uncompressed_data = NULL;

void uart_demo(void){
	int ifd = -1;
	unsigned char bTemp[1024];
	int           iTemp;

	struct timeval	t;
	fd_set			rfds;


	lcd_clean();
	ifd = open("/dev/ttyS3",  O_RDWR);
	if (ifd >=0){

		CstcLog_printf( "SETTING: 115200,8,n,1");

		//		lcd_flip();
		tty_property_config(ifd, 115200, 8,'n',1,'n');
		while (1){
			t.tv_sec  = 0;
			t.tv_usec = 500000;

			FD_ZERO(&rfds);
			FD_SET(ifd, &rfds);

			if ((select(ifd + 1, &rfds, NULL, NULL, &t) > 0)&&
					FD_ISSET(ifd, &rfds))
			{
				iTemp = read(ifd, bTemp, sizeof(bTemp));
				if (iTemp > 0){
					write(ifd, bTemp, iTemp);
					CstcLog_printf( "Recived %d bytes....", iTemp);
					//					lcd_flip();
				}
			}
			else {

				strcpy((char*)bTemp, "SPC : HELLO WORLD");
				iTemp = strlen((char*)bTemp);
				write(ifd, bTemp, iTemp);
				usleep(100000);
			}
		}
	}else{
		CstcLog_printf( "OPEN UART Failed.");
		;
	}


	if (ifd >=0){
		close(ifd);
		ifd = -1;
	}
}

int init_UART(int *fd, struct termios *oldtio)
{
	struct termios newtio;
	CstcLog_printf("uart server");
	*fd = open("/dev/ttyS3", O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (*fd <0)
	{
		CstcLog_printf("Failed to open serial port");
		return 0;
	}

	tcgetattr(*fd,oldtio);

	bzero(&newtio, sizeof(newtio));
	newtio.c_cflag = B115200 | CRTSCTS | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = 0;

	newtio.c_lflag = 0;

	newtio.c_cc[VTIME]    = 0;
	newtio.c_cc[VMIN]     = 255;

	tcflush(*fd, TCIFLUSH);
	tcsetattr(*fd,TCSANOW,&newtio);
	CstcLog_printf("serial port open");

	return 1;
}

int deinit_UART(int *fd, struct termios *oldtio)
{
	CstcLog_printf( "CLOSEING UART.");
	tcsetattr(*fd,TCSANOW,oldtio);
	return 1;
}
int UART_Manual_Duty_Assignment_server(int DownloadEnableFlag)
{
	int retval=0, key=0;
	int fd=-1;
	char bTemp[5120]={'\0'};
	int iTemp = 0;
	int j = 0;

	struct timeval	t;
	fd_set			rfds;

	CstcLog_printf("UART_Manual_Duty_Assignment_server");

	fd=open("/dev/ttyS3", O_RDWR );
	if(fd<0)
	{
		CstcLog_printf("Failed to open serial port");
		return 0;
	}
	CstcLog_printf("serial port open");

	tty_property_config(fd,115200,8,'n',1,'n');

	CstcLog_printf("complete");
	if(DownloadEnableFlag)
		MultipleScheduleFlag=1;

	CstcLog_printf("fd %d", fd);
	while (1){
		if (j == 0){
			key = 0;
			key = Kb_Get_Key_Sym_New_uart();
			if(key==KEY_SYM_ESCAPE)
				break;
			key = 0;
		}
		else if (DownloadEnableFlag == 1){
			key = 0;
			key = Kb_Get_Key_Sym_New_uart();
			if(key==KEY_SYM_ESCAPE)
				break;
			key = 0;
		}
		if(UART_check_master_status()==1)
		{
			retval=1;
			break;
		}
		else
		{
			usleep(500);
			DbInterface_Get_Master_Status_Data(&(master_status_struct_var.ticket_fare), &(master_status_struct_var.download_ticket),
					&(master_status_struct_var.machine_register), &(master_status_struct_var.upload_master), &(master_status_struct_var.erase_ticket),
					&(master_status_struct_var.start_duty), &(master_status_struct_var.insert_waybill)/*,&(master_status_struct_var.insert_route)*/);
			if(UART_check_master_status()==1)
			{
				retval=1;
				break;
			}
		}
		if (j == 0){
			key = 0;
			key = Kb_Get_Key_Sym_New_uart();
			if(key==KEY_SYM_ESCAPE)
				break;
			key = 0;
		}
		else if (DownloadEnableFlag == 1){
			key = 0;
			key = Kb_Get_Key_Sym_New_uart();
			if(key==KEY_SYM_ESCAPE)
				break;
			key = 0;
		}
		t.tv_sec  = 0;
		t.tv_usec = 100;

		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);

		if (j > 0){

			if (DownloadEnableFlag == 1){
				CstcLog_printf("1. size of bTemp : %d",sizeof(bTemp));
				iTemp = read(fd, bTemp, sizeof(bTemp));

				CstcLog_printf("iTemp %d", iTemp);

				if (iTemp > 0){
					CstcLog_printf("iTemp %d", iTemp);
					bTemp[iTemp]='\0';
					CstcLog_printf("Read bytes are : ");
					CstcLog_HexDump(bTemp,iTemp);
					CstcLog_printf_2("\n .......Received...........\n ");

					CstcLog_HexDump(bTemp,iTemp);

					if((UART_compose_response_packet((char*)bTemp, fd,iTemp,&j)))
					{

						CstcLog_printf("Response done iTemp : %d",iTemp);

					}
					CstcLog_printf("2. size of bTemp : %d",sizeof(bTemp));
					memset(bTemp,0,sizeof(bTemp));
					iTemp=0;
				}
			}
			else
			{
				if ((select(fd + 1, &rfds, NULL, NULL, NULL) > 0)&&FD_ISSET(fd, &rfds))
				{
					CstcLog_printf("1. size of bTemp : %d",sizeof(bTemp));
					iTemp = read(fd, bTemp, sizeof(bTemp));

					CstcLog_printf("iTemp %d", iTemp);

					if (iTemp > 0){
						CstcLog_printf("iTemp %d", iTemp);
						bTemp[iTemp]='\0';
						CstcLog_printf("Read bytes are : ");
						CstcLog_HexDump_1(bTemp,iTemp);
						CstcLog_printf_2("\n .......Received...........\n ");
						CstcLog_HexDump(bTemp,iTemp);

						if((UART_compose_response_packet((char*)bTemp, fd,iTemp,&j)))
						{

							CstcLog_printf("Response done iTemp : %d",iTemp);

						}
						CstcLog_printf("2. size of bTemp : %d",sizeof(bTemp));
						memset(bTemp,0,sizeof(bTemp));
						iTemp=0;
					}
				}
			}
		}
		else
		{
			key = 0;
			key = Kb_Get_Key_Sym_New_uart();
			if(key==KEY_SYM_ESCAPE)
				break;
			key = 0;
			CstcLog_printf("2. size of bTemp : %d",sizeof(bTemp));
			iTemp = read(fd, bTemp, sizeof(bTemp));

			CstcLog_printf("iTemp %d", iTemp);

			if (iTemp > 0){
				j=iTemp;
				CstcLog_printf("iTemp %d", iTemp);
				bTemp[iTemp]='\0';
				CstcLog_printf("Read bytes are : ");
				CstcLog_HexDump(bTemp,iTemp);
				CstcLog_printf_2("\n .......Received...........\n ");

				CstcLog_HexDump_2(bTemp,iTemp);

				if(!(UART_compose_response_packet((char*)bTemp, fd,iTemp,&j)))
				{
					//retval =1;
					//break;
					CstcLog_printf("Response done iTemp : %d",iTemp);
				}
				CstcLog_printf("2. size of bTemp : %d",sizeof(bTemp));
				memset(bTemp,0,sizeof(bTemp));//5120
				iTemp=0;
			}
		}
	}

	CstcLog_printf( "OPEN UART Failed.");
	if(DownloadEnableFlag)
		MultipleScheduleFlag=0;

	if (fd >=0){
#ifdef SERIAL_API
		close(fd);
#else
		deinit_UART(&fd, &oldtio);
#endif
		fd = -1;
	}
	CstcLog_printf( "OPEN UART returnnnnnnnnnnn");

	return retval;
}


int UART_Schedule_Upload_server(void)
{
	int retval=0, key=0;
	int fd=-1;
	char bTemp[5120]={'\0'};
	int iTemp = 0;
	int j = 0;

	//	struct timeval	t;
	fd_set			rfds;

	CstcLog_printf("UART_Schedule_Upload_server");

	fd=open("/dev/ttyS3", O_RDWR );
	if(fd<0)
	{
		CstcLog_printf("Failed to open serial port");
		return 0;
	}
	CstcLog_printf("serial port open");

	tty_property_config(fd,115200,8,'n',1,'n');
	CstcLog_printf("complete");

	MultipleScheduleFlag=1;
	CstcLog_printf("fd %d", fd);
	while (1){
		//		if (j == 0){
		key = 0;
		key = Kb_Get_Key_Sym_New_uart();
		if(key==KEY_SYM_ESCAPE)
			break;
		key = 0;
		//		}


		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);

		if (j > 0){
			//			if ((select(fd + 1, &rfds, NULL, NULL, NULL) > 0)&&FD_ISSET(fd, &rfds))
			//			{
			key = 0;
			key = Kb_Get_Key_Sym_New_uart();
			if(key==KEY_SYM_ESCAPE)
				break;
			key = 0;
			CstcLog_printf("1. size of bTemp : %d",sizeof(bTemp));
			usleep(200000);
			iTemp = read(fd, bTemp, sizeof(bTemp));

			CstcLog_printf("iTemp %d", iTemp);
			if (iTemp > 0){
				CstcLog_printf("iTemp %d", iTemp);
				bTemp[iTemp]='\0';
				j=0;
				CstcLog_printf("Read bytes are : ");
				CstcLog_HexDump(bTemp,iTemp);
				if((retval = UART_compose_response_packet((char*)bTemp, fd,iTemp,&j))>0)
				{
					CstcLog_printf("Response done iTemp : %d",iTemp);
				}
				CstcLog_printf("2. size of bTemp : %d",sizeof(bTemp));
				memset(bTemp,0,sizeof(bTemp));//5120
				iTemp=0;
			}
			else
			{
				CstcLog_printf("3. size of bTemp : %d",sizeof(bTemp));
				memset(bTemp,0,sizeof(bTemp));//5120
				iTemp=0;
			}
			//			}
		}
		else
		{
			key = 0;
			key = Kb_Get_Key_Sym_New_uart();
			if(key==KEY_SYM_ESCAPE)
				break;
			key = 0;
			CstcLog_printf("2. size of bTemp : %d",sizeof(bTemp));
			usleep(200000);
			iTemp = read(fd, bTemp, sizeof(bTemp));

			CstcLog_printf("iTemp %d", iTemp);
			if (iTemp > 0){
				j=iTemp;
				CstcLog_printf("iTemp %d", iTemp);
				bTemp[iTemp]='\0';
				CstcLog_printf("Read bytes are : ");
				CstcLog_HexDump(bTemp,iTemp);
				//				if(!(UART_compose_response_packet((char*)bTemp, fd,iTemp,&j)))
				if((retval=UART_compose_response_packet((char*)bTemp, fd,iTemp,&j))==0)
				{
					CstcLog_printf("Response done iTemp : %d",iTemp);
				}
				CstcLog_printf("2. size of bTemp : %d",sizeof(bTemp));
				memset(bTemp,0,sizeof(bTemp));//5120
				iTemp=0;
			}
			else
			{
				CstcLog_printf("4. size of bTemp : %d",sizeof(bTemp));
				memset(bTemp,0,sizeof(bTemp));//5120
				iTemp=0;
			}
		}
	}
	MultipleScheduleFlag=0;
	CstcLog_printf( "OPEN PC Connection Failed.");

	if (fd >=0){
		close(fd);
		fd = -1;
	}
	CstcLog_printf( "UART_Schedule_Upload_server retval = %d",retval);
	return retval;
}


/****************************************************************************************************
                                  COMPOSE DIFFERENT PACKETS - FUNCTIONS
 *****************************************************************************************************/

void UART_final_packet_lan(char * pkt_format_ptr, char * pkt_ptr, int *length)
{
	int temp = 0;
#if !COMPRESSION_DECOMPRESSION
	packet_format_struct_var.start_indicator = ';';
#endif
	temp = *length + START_INDICATOR_LEN+DATA_LENGTH_LEN+CHECKSUM_LEN; // (length - 45);

	sprintf((packet_format_struct_var.data_length), "%08d", temp);
	packet_format_struct_var.data = pkt_ptr;

	memset(packet_format_struct_var.checksum,'0', CHECKSUM_LEN);
#if COMPRESSION_DECOMPRESSION
	memcpy(pkt_format_ptr, "CTS", START_INDICATOR_LEN);
#else
	memcpy(pkt_format_ptr, &(packet_format_struct_var.start_indicator), START_INDICATOR_LEN);
#endif
	memcpy((pkt_format_ptr + START_INDICATOR_LEN) , (packet_format_struct_var.data_length), DATA_LENGTH_LEN);
	memcpy((pkt_format_ptr + START_INDICATOR_LEN+DATA_LENGTH_LEN), packet_format_struct_var.data, *length);
	memcpy((pkt_format_ptr + START_INDICATOR_LEN+DATA_LENGTH_LEN + *length), packet_format_struct_var.checksum,CHECKSUM_LEN);
	CstcLog_printf("pkt_format_ptr = === %s",pkt_format_ptr);
	*length += START_INDICATOR_LEN+DATA_LENGTH_LEN+CHECKSUM_LEN;
}

void UART_compose_download_response_packet(char * pkt_ptr,int *length)
{
	int rowCount=0;
	int temp = DOWNLOAD_DATA_COUNT_RESPONSE_PCK_ID;

	CstcLog_printf("INSIDE UART_compose_download_response_packet FUNCTION");
	CstcLog_printf("DOWNLOAD_RESPONSE_PCK_ID = %d ",temp);
	sprintf(download_response_packet_struct_var.pkt_id_download_response, "%d", temp);

	rowCount=DbInterface_Get_Row_Count("transaction_log",16,0);
	CstcLog_printf("rowCount = %d ",rowCount);
	Global_rowCount=rowCount;
	GDownload_pktCnt =0;

	if(rowCount == 0)
	{
		strcpy(download_response_struct_var.row_cnt,"00000");
		strcpy(download_response_struct_var.start_tkt_cnt,"00000");
		strcpy(download_response_struct_var.end_tkt_cnt, "00000");
	}
	else
	{
		get_start_end_db_data(download_response_struct_var.start_tkt_cnt,download_response_struct_var.end_tkt_cnt);
		sprintf(download_response_struct_var.row_cnt,"%05d",rowCount);
	}
	CstcLog_printf("start_tkt_cnt = %s , end_tkt_cnt =%s , row_cnt=%d",download_response_struct_var.start_tkt_cnt,download_response_struct_var.end_tkt_cnt,rowCount);

	memcpy(pkt_ptr,download_response_packet_struct_var.pkt_id_download_response, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),download_response_struct_var.row_cnt, ROW_CNT_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+ROW_CNT_LEN),download_response_struct_var.start_tkt_cnt, TICKET_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+ROW_CNT_LEN+TICKET_NO_LEN), download_response_struct_var.end_tkt_cnt, TICKET_NO_LEN);

	CstcLog_printf("PKT_PTR %s", pkt_ptr);
	*length += PKT_ID_LEN+ROW_CNT_LEN+TICKET_NO_LEN+TICKET_NO_LEN;
}

int UART_compose_transaction_data_packets(char * pkt_ptr,int *length)
{
	int Transaction_no=0,loop_count=0;//,i,record_length=0;
	int temp=TRANSACTION_DATA;
	CstcLog_printf("TRANSACTION_DATA = %d ",temp);

	if(Global_rowCount>DOWNLOAD_TICKET_RANGE)
	{
		loop_count=DOWNLOAD_TICKET_RANGE;
		Global_rowCount=Global_rowCount-DOWNLOAD_TICKET_RANGE;
		CstcLog_printf("loop_count = %d Global_rowCount=%d ",loop_count,Global_rowCount);
	}
	else
	{
		loop_count=Global_rowCount;
		Global_rowCount=0;
		CstcLog_printf("loop_count = %d Global_rowCount =%d",loop_count,Global_rowCount);
	}

	CstcLog_printf("download_response_struct_var.start_tkt_cnt %s",download_response_struct_var.start_tkt_cnt);
	//	Transaction_no=atoi(download_response_struct_var.start_tkt_cnt);
	if(prev_end_tkt_count > atoi(download_response_struct_var.start_tkt_cnt)) //Swap to avoid crash due to skipped transaction nos.
		Transaction_no=prev_end_tkt_count; //Swap to avoid crash due to skipped transaction nos.
	else
		Transaction_no=atoi(download_response_struct_var.start_tkt_cnt);
	CstcLog_printf("Transaction_no = %d ",Transaction_no);
	if(Transaction_no == 0)
	{

		strcpy(pkt_ptr,"Transaction Complete");
	}
	else
	{
		sprintf(download_transaction_log_struct_var.pkt_id_ack_packet, "%d", temp);
		sprintf(pkt_ptr,"%s",download_transaction_log_struct_var.pkt_id_ack_packet);
		//		record_length=PKT_ID_LEN;
		//		CstcLog_printf("record_length = %d ",record_length);

		//		uart_transaction_log_struct_var=(transaction_log_struct*)malloc(sizeof(transaction_log_struct));

		if(0>=get_transaction_log_data(Transaction_no,pkt_ptr))
		{
			Global_rowCount=Global_rowCount+loop_count;
			return 0;
		}
#if 0
		for(i=0;i<loop_count;i++)
		{
			//			CstcLog_printf("i = %d ",i);

			sprintf((pkt_ptr+record_length),"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',"
					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"

					"\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%s\',\'%s\',"

					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%d\',\'%d\',\'%d\',\'%s\',"

					"\'%s\',\'%c\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%s\',\'%s\',\'%c\',\'%c\',"
					"\'%s\',\'%s\',\'%c\',\'%s\',\'%s\',\'%s\',\'%s\'|",

					(uart_transaction_log_struct_var+i)->waybill_no,
					(uart_transaction_log_struct_var+i)->schedule_no,
					(uart_transaction_log_struct_var+i)->trip_no,
					(uart_transaction_log_struct_var+i)->etim_no,
					(uart_transaction_log_struct_var+i)->route_no,

					(uart_transaction_log_struct_var+i)->route_id,

					(uart_transaction_log_struct_var+i)->transaction_no,
					(uart_transaction_log_struct_var+i)->ticket_no,
					(uart_transaction_log_struct_var+i)->tkt_type_short_code,
					(uart_transaction_log_struct_var+i)->tkt_sub_type_short_code,
					(uart_transaction_log_struct_var+i)->from_stop_seq_no,

					(uart_transaction_log_struct_var+i)->from_bus_stop_code_english,
					(uart_transaction_log_struct_var+i)->from_bus_stop_id,

					(uart_transaction_log_struct_var+i)->till_stop_seq_no,

					(uart_transaction_log_struct_var+i)->till_bus_stop_code_english,
					(uart_transaction_log_struct_var+i)->till_bus_stop_id,

					(uart_transaction_log_struct_var+i)->travelled_km,

					(uart_transaction_log_struct_var+i)->px_count,
					(uart_transaction_log_struct_var+i)->lugg_units,
					(uart_transaction_log_struct_var+i)->px_total_amount,
					(uart_transaction_log_struct_var+i)->lugg_total_amount,
					(uart_transaction_log_struct_var+i)->toll_amt,

					(uart_transaction_log_struct_var+i)->total_ticket_amount,
					(uart_transaction_log_struct_var+i)->concession_type,
					(uart_transaction_log_struct_var+i)->group_ticket_mode,
					(uart_transaction_log_struct_var+i)->payment_mode,
					(uart_transaction_log_struct_var+i)->ticket_date,

					(uart_transaction_log_struct_var+i)->ticket_time,
					(uart_transaction_log_struct_var+i)->upload_flag,
					(uart_transaction_log_struct_var+i)->bus_service_id,
					(uart_transaction_log_struct_var+i)->line_check_status,
					(uart_transaction_log_struct_var+i)->depot_id,

					(uart_transaction_log_struct_var+i)->vehicle_no,
					(uart_transaction_log_struct_var+i)->ticket_code,
					(uart_transaction_log_struct_var+i)->pass_id,
					(uart_transaction_log_struct_var+i)->epurse_card_no,
					(uart_transaction_log_struct_var+i)->epurse_card_issuer_name,

					(uart_transaction_log_struct_var+i)->epurse_curr_amount_qstr,
					(uart_transaction_log_struct_var+i)->epurse_last_amount_qstr,
					(uart_transaction_log_struct_var+i)->epurse_last_waybill_no_qstr,
					(uart_transaction_log_struct_var+i)->epurse_last_tkt_no,
					(uart_transaction_log_struct_var+i)->epurse_last_tkt_date,

					(uart_transaction_log_struct_var+i)->epurse_last_tkt_time,
					(uart_transaction_log_struct_var+i)->epurse_last_from_stop_code ,
					(uart_transaction_log_struct_var+i)->epurse_last_to_stop_code,
					(uart_transaction_log_struct_var+i)->tkt_printed_flag,
					(uart_transaction_log_struct_var+i)->shift, //swap change needed check this
					(uart_transaction_log_struct_var+i)->trip_status,
					(uart_transaction_log_struct_var+i)->trip_end_condition,
					(uart_transaction_log_struct_var+i)->waybill_type,
					(uart_transaction_log_struct_var+i)->schedule_id,
					(uart_transaction_log_struct_var+i)->conductor_token_id,
					(uart_transaction_log_struct_var+i)->rate_id,
					(uart_transaction_log_struct_var+i)->stage_count
			);
			record_length = strlen(pkt_ptr);
			//			CstcLog_printf("i = %d record_length = %d ",i,record_length);

			//			CstcLog_printf("PKT_PTR1:::: %s", pkt_ptr);  //Swap Enable this after chk for the download crash
		}
#endif
		//			Transaction_no++;
		//			CstcLog_printf("Transaction_no= %d ",Transaction_no);
		//			transaction_log_struct_clear(&uart_transaction_log_struct_var);

		//		free(uart_transaction_log_struct_var);
		//		uart_transaction_log_struct_var = NULL;

		//}

	}
	//	CstcLog_printf("PKT_PTR_Main %s", pkt_ptr);  //Swap Enable this after chk for the download crash
	CstcLog_printf_2("PKT_PTR_Len = %d", strlen(pkt_ptr));
	*length+=strlen(pkt_ptr);
	CstcLog_HexDump(pkt_ptr,strlen(pkt_ptr));
	return 1;
}


void UART_compose_registration_response_packet(char * pkt_ptr,int *length)
{
	char *sn = NULL;
	char *etim_no = NULL;
	char *mac_ID = NULL;
	FILE* tempFile= NULL;
	int nbytes = 10;
	int temp = REGISTRATION_RESPONSE_PKT_ID;

	char Err_MsgAry[3]={'\0'};

	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);
	UART_get_machine_info(sn, nbytes);

	etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
	memset(etim_no,0,(TERMINAL_ID_LEN+1));
	tempFile= fopen(ETIM_NO_FILE_ADDR,"r+");
	if(tempFile == NULL)
	{
		CstcLog_printf("ERROR OPENING FILE etim_no.txt");
		sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
		strcat(Err_Msg,Err_MsgAry);
		Err_Len=Err_Len+2;//strlen(Err_MsgAry);
		CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
		memset(Err_MsgAry,0,3);
	}
	else
	{
		fscanf(tempFile,"%s",etim_no);
		fflush(tempFile);
		CstcLog_printf("etim_no :- %s", etim_no);
		fclose(tempFile);
	}

	mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
	memset(mac_ID,0,(MAC_ADDR_LEN+1));
	tempFile= fopen(MAC_FILE_ADDR,"r+");
	if(tempFile == NULL)
	{
		CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
		sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_mac_ID);
		strcat(Err_Msg,Err_MsgAry);
		Err_Len=Err_Len+2;//strlen(Err_MsgAry);
		CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
		memset(Err_MsgAry,0,3);
	}
	else
	{
		fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
		fflush(tempFile);
		CstcLog_printf("mac_ID :- %s", mac_ID);
		fclose(tempFile);
	}

	sprintf(registration_response_struct_var.pkt_id_reg_response, "%d", temp);
	memcpy(registration_response_struct_var.machine_no, sn, nbytes); //serial no
	if(strlen(mac_ID)==0)
		memset(registration_response_struct_var.mac_address,'0', MAC_ADDR_LEN);
	else
		strcpy(registration_response_struct_var.mac_address,mac_ID);//"00257E019279");
	if(strlen(etim_no)==0)
		memset(registration_response_struct_var.terminal_id,'0', TERMINAL_ID_LEN);
	else
		strcpy(registration_response_struct_var.terminal_id, etim_no);//etim no
	strcpy(registration_response_struct_var.version_no,APP_VERSION);
	strcpy(registration_response_struct_var.version_date,APP_VER_DATETIME);
	if(strlen(waybill_master_struct_var.waybill_no)>0)
		sprintf(registration_response_struct_var.waybill_no,"%14s",waybill_master_struct_var.waybill_no);
	else
		memset(registration_response_struct_var.waybill_no,'0',WAYBILL_NO_LEN);
	CstcLog_printf("Reg Res waybill_no = %s",registration_response_struct_var.waybill_no);

	memcpy(pkt_ptr,registration_response_struct_var.pkt_id_reg_response, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),registration_response_struct_var.machine_no, MACHINE_SN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN),registration_response_struct_var.mac_address, MAC_ADDR_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), registration_response_struct_var.terminal_id, TERMINAL_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN),registration_response_struct_var.version_no, APP_VER_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+APP_VER_NO_LEN),registration_response_struct_var.version_date, APP_VER_DATETIME_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+APP_VER_NO_LEN+APP_VER_DATETIME_LEN),registration_response_struct_var.waybill_no, WAYBILL_NO_LEN);
	CstcLog_printf("pkt_ptr = %s",pkt_ptr);

	*length += PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+APP_VER_NO_LEN+APP_VER_DATETIME_LEN+WAYBILL_NO_LEN;
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	if(etim_no!=NULL)
	{
		free(etim_no);
		etim_no=NULL;
	}
	if(mac_ID!=NULL)
	{
		free(mac_ID);
		mac_ID=NULL;
	}
}

void UART_compose_schedule_response_packet(char * pkt_ptr,int *length)
{
	int temp = SCHEDULE_RESPONSE_PCK_ID,i,record_length=0,rowCount=0;

	CstcLog_printf("INSIDE UART_compose_download_response_packet FUNCTION");
	CstcLog_printf("DOWNLOAD_RESPONSE_PCK_ID = %d ",temp);

	rowCount=DbInterface_Get_Row_Count("duty_master",strlen("duty_master"),0);
	CstcLog_printf("rowCount = %d ",rowCount);
	if(rowCount == 0)
	{
		strcpy(pkt_ptr,"04\'00\',\'00\',\'00\',\'00\',\'00\',\'00\',\'00\'|");
	}
	else
	{
		pduty_master_struct_var =(duty_master_struct*)malloc(sizeof(duty_master_struct)*(rowCount+1));
		sprintf(download_transaction_log_struct_var.pkt_id_ack_packet, "%02d", temp);
		sprintf(pkt_ptr,"%s",download_transaction_log_struct_var.pkt_id_ack_packet);
		record_length=PKT_ID_LEN;
		get_duty_master_data(pduty_master_struct_var);
		CstcLog_printf("RETURN............");
		for(i=0;i<rowCount;i++)
		{
			CstcLog_printf("(pduty_master_struct_var+i)->duty_no = %s",(pduty_master_struct_var+i)->duty_no);
			CstcLog_printf("(pduty_master_struct_var+i)->schedule_no = %s",(pduty_master_struct_var+i)->schedule_no);
			CstcLog_printf("(pduty_master_struct_var+i)->schedule_status = %s",(pduty_master_struct_var+i)->schedule_status);
			CstcLog_printf("(pduty_master_struct_var+i)->updated_by = %s",(pduty_master_struct_var+i)->updated_by);
			CstcLog_printf("(pduty_master_struct_var+i)->updated_date = %s",(pduty_master_struct_var+i)->updated_date);
			CstcLog_printf("(pduty_master_struct_var+i)->updated_time = %s",(pduty_master_struct_var+i)->updated_time);

			sprintf((pkt_ptr+record_length),"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\'|",
					(pduty_master_struct_var+i)->duty_no,
					(pduty_master_struct_var+i)->schedule_no,
					(pduty_master_struct_var+i)->schedule_status,
					(pduty_master_struct_var+i)->updated_by,
					(pduty_master_struct_var+i)->updated_date,
					(pduty_master_struct_var+i)->updated_time,
					(pduty_master_struct_var+i)->schedule_id
			);
			record_length = strlen(pkt_ptr);
			CstcLog_printf("record_length = %d ",record_length);
			CstcLog_printf("PKT_PTR %s", pkt_ptr);
		}
		free(pduty_master_struct_var);
	}
	CstcLog_printf("Final PKT_PTR %s", pkt_ptr);
	*length+=strlen(pkt_ptr);
}


//----------------------------------------ACKNOWLEDGEMENT PACKET---------------------------------------

void UART_compose_ack_packet(char *pkt_ptr,int *length)
{
	char *sn = NULL;
	char *etim_no = NULL;
	//	char *mac_ID = NULL;
	FILE* tempFile= NULL;
	int nbytes = 10;
	int temp = ACK_PKT_ID;

	char Err_MsgAry[3]={'\0'};

	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);
	UART_get_machine_info(sn, nbytes);

	etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
	memset(etim_no,0,(TERMINAL_ID_LEN+1));
	tempFile= fopen(ETIM_NO_FILE_ADDR,"r+");
	if(tempFile == NULL)
	{
		CstcLog_printf("ERROR OPENING FILE etim_no.txt");
		sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
		strcat(Err_Msg,Err_MsgAry);
		Err_Len=Err_Len+2;//strlen(Err_MsgAry);
		memset(Err_MsgAry,0,3);
	}
	else
	{
		fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile);
		fflush(tempFile);
		CstcLog_printf("etim_no :- %s", etim_no);
		fclose(tempFile);
	}

	sprintf(ack_packet_struct_var.pkt_id_ack_packet, "%d", temp);

	memcpy(ack_packet_struct_var.machine_no, sn, MACHINE_SN_LEN);

	strcpy(ack_packet_struct_var.mac_address,"000000000000");

	if(strlen(etim_no)==0)
		memset(ack_packet_struct_var.terminal_id,'0', TERMINAL_ID_LEN);
	else
		strcpy(ack_packet_struct_var.terminal_id, etim_no);

	ack_packet_struct_var.err_ack=(char *)malloc((Err_Len+1)*sizeof(char));
	memset(ack_packet_struct_var.err_ack,0,(Err_Len+1));

	if(Err_Len==0)
	{
		sprintf(Err_Msg,"%02d",SUCCESS);
		Err_Len=2;//strlen(Err_Msg);
		CstcLog_printf("Err_Len =%d ,Err_Msg = %s",Err_Len,Err_Msg);
	}
	else
	{
		CstcLog_printf("Err_Len =%d ,Err_Msg = %s",Err_Len,Err_Msg);
	}


	CstcLog_HexDump(Err_Msg,Err_Len);

	memcpy(ack_packet_struct_var.err_ack,Err_Msg,Err_Len);
	ack_packet_struct_var.err_ack[Err_Len]='\0';

	CstcLog_printf("Err_Len =%d",Err_Len);

	sprintf(ack_packet_struct_var.err_length,"%02d",Err_Len);

	memcpy(pkt_ptr, ack_packet_struct_var.pkt_id_ack_packet, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),ack_packet_struct_var.terminal_id, TERMINAL_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN), ack_packet_struct_var.machine_no, MACHINE_SN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN), ack_packet_struct_var.mac_address, MAC_ADDR_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), ack_packet_struct_var.err_length,ERR_LENGTH_LEN );
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+ERR_LENGTH_LEN), ack_packet_struct_var.err_ack,Err_Len );

	CstcLog_printf("ack pkt_ptr %s ",pkt_ptr);
	*length += PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+ERR_LENGTH_LEN+Err_Len;
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	if(etim_no!=NULL)
	{
		free(etim_no);
		etim_no=NULL;
	}
	//	if(mac_ID!=NULL)
	//	{
	//		free(mac_ID);
	//		mac_ID=NULL;
	//	}
	if(ack_packet_struct_var.err_ack!=NULL)
	{
		free(ack_packet_struct_var.err_ack);
		ack_packet_struct_var.err_ack=NULL;
	}
}

//**************************************************************************************************
//****************************************************************************************************

int UART_compose_response_packet(char * bTemp, int iclient_socket ,int iTemp,int *j)
{
#if COMPRESSION_DECOMPRESSION
	char tempCh[4] ={'\0'};
	char Global_tempCh[4] ={'\0'};

#else
	char tempCh = bTemp[0];
#endif
	char recvPktID[(PKT_ID_LEN+1)]={'\0'};
	char recvPktLen[(DATA_LENGTH_LEN+1)] ={'\0'};
	//char recvSchedule[(SCHEDULE_NO_LEN+1)]={'\0'};
	char recvRateId[(SCHEDULE_NO_LEN+1)]={'\0'};
	char result[(SCHEDULE_NO_LEN+1)]={'\0'};

	char recvWaybillNo[(WAYBILL_NO_LEN+1)]={'\0'};
	char result_waybill[(WAYBILL_NO_LEN+1)]={'\0'};

	char recvRouteId[(SCHEDULE_NO_LEN + 1)]={'\0'};
	char *tempInitBuff=NULL;
	char *tempName=NULL;//[100]={'\0'};

	FILE* dbDataFile= NULL;
	int ack_flag=0,ptr_inc=0,g_ptr_inc=0;//i=0,
	int length = 0;
	char * pkt_ptr = NULL;
	char * pkt_format_ptr  = NULL;

	char Err_MsgAry[3]={'\0'};
	char temp_cmd[50];
	char recvCount[5]={'\0'};
	char *sub_string=NULL;

#if COMPRESSION_DECOMPRESSION
	//char *Data_to_UnComp = NULL;
	char unComDataLen[(DATA_LENGTH_LEN+1)] ={'\0'};
	//int ret = 0;
	//char *uncompData = NULL;
	//Bytef uncompData[7000]={'\0'};
//	uLongf uncompDataLen = 0 ;
#endif

	int checksumFlag=0;

	//char CRC_Data[32]={'\0'};
	//	char check_crc[8]={'\0'};
	//	char Calculated_crc[9]={'\0'};
#if COMPRESSION_DECOMPRESSION
	if((sub_string=strstr(bTemp,"CHK")) != NULL)
#else
		if((sub_string=strstr(bTemp,"|")) != NULL)
#endif
		{
			checksumFlag=1;
			CstcLog_printf("checksumFlag = %d",checksumFlag);
			//		for(i=0;i<iTemp;i++)
			//		{
			//			if(bTemp[i] != '|')
			//			{
			//				dbTemp[i] = bTemp[i];
			//			}
			//			else if(bTemp[i] == '|')
			//				break;
			//		}
			//		dbTemp[i] = '\0';
			memset(g_checksumbuffer,0,sizeof(g_checksumbuffer));
			//memcpy(g_checksumbuffer,(bTemp+i+1),(iTemp-i-1));
#if COMPRESSION_DECOMPRESSION
			CstcLog_printf("(sub_string+3) = %d",strlen((sub_string+3)));
			memcpy(g_checksumbuffer,(sub_string+3),strlen((sub_string+3)));
			CstcLog_printf("(sub_string+3) = %s",(sub_string+3));
			*(sub_string)=';';
			*(sub_string+1)='\0';
#else
			CstcLog_printf("(sub_string+1) = %d",strlen((sub_string+1)));
			memcpy(g_checksumbuffer,(sub_string+1),strlen((sub_string+1)));
			CstcLog_printf("(sub_string+1) = %s",(sub_string+1));
			*(sub_string)=';';
			*(sub_string+1)='\0';
#endif

		}
		else
		{
			//		memset(dbTemp,0,5120);
			//		memcpy(dbTemp,bTemp,iTemp);
			//		dbTemp[iTemp]='\0';
			if(g_checksumFlag)
			{
				CstcLog_printf("g_checksumbuffer = %s",g_checksumbuffer);
				strcat(g_checksumbuffer,bTemp);//dbTemp
			}
		}
	//bTemp_string = &dbTemp;

	//	CstcLog_printf("dbTemp = %s",dbTemp);
	CstcLog_printf("new dbTemp = %s",bTemp);
	CstcLog_printf("g_checksumFlag = %d",g_checksumFlag);
	CstcLog_printf("g_checksumbuffer = %s \n %d",g_checksumbuffer,strlen(g_checksumbuffer));
#if COMPRESSION_DECOMPRESSION
	memset(tempCh,0,3);
	memcpy(tempCh,bTemp,3);
	tempCh[strlen(tempCh)]= '\0';
#endif
	//	memcpy(CRC_Data,g_checksumbuffer,32);
	//	CstcLog_printf("CRC_Data = %s  %d \n\n",CRC_Data,strlen(CRC_Data));

	//	strcat(CRC_Data,"00000000");
	//	CstcLog_printf(" CRC_Data after concat = %s %d \n\n ",CRC_Data,strlen(CRC_Data));

	//	CstcLog_printf("CRC : %" PRIX32 "\n\n", rc_crc32(0, CRC_Data , strlen(CRC_Data)));

	//	snprintf(Calculated_crc, sizeof Calculated_crc, "%" PRIX32, rc_crc32(0, CRC_Data , strlen(CRC_Data)));
	//	CstcLog_printf("Calculated_crc = %s \n\n",Calculated_crc);

	//	memcpy(check_crc,(g_checksumbuffer+32),8);
	//	CstcLog_printf("check_crc = %s  %d \n\n",check_crc,strlen(check_crc));
	//if(strcmp(check_crc,Calculated_crc) == 0)     //open when backend is ready with crc
	{
		CstcLog_printf("\n.......CRC SUCCESS.........\n ");
#if COMPRESSION_DECOMPRESSION
		CstcLog_printf("tempCh =%s",tempCh);
		if(strcmp(tempCh,"CTS") == 0)
#else
			CstcLog_printf("tempCh =%c",tempCh);
			if((tempCh== ';') /* && (New_pkt_recv_flag == 0)*/)  //start indicator needs to be change from ; to (CTS)
#endif
		{
#if COMPRESSION_DECOMPRESSION
			if(iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3))
#else
				if(iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3))
#endif
				{
#if COMPRESSION_DECOMPRESSION
					global_buff = (char *)malloc((sizeof(char)*2048));
					memset(global_buff,0,2048);
#else
					memset(global_buff,0,2048);

#endif
					if(iTemp<2047)
					{
						memcpy(global_buff,bTemp,iTemp);//dbTemp,iTemp);
						global_buff[iTemp]='\0';
					}
					else
					{
						memcpy(global_buff,bTemp,2047);//dbTemp,iTemp);
						global_buff[2047]='\0';
					}
					global_iTemp=iTemp;
					CstcLog_printf("global_iTemp(%d) in iTemp(%d) != Total_packet_length condition  %d", global_iTemp,iTemp,Total_packet_length);
					New_pkt_recv_flag=1;
					return 0;
				}
#if COMPRESSION_DECOMPRESSION
				else if(iTemp > (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3))
#else
					else if(iTemp > (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3))
#endif
					{
						memset(recvPktLen,0,DATA_LENGTH_LEN);
						memcpy(recvPktLen,bTemp+START_INDICATOR_LEN,DATA_LENGTH_LEN);

						CstcLog_printf("recvPktLen =%s bytes", recvPktLen);
						Total_packet_length = 0;
						Total_packet_length = atoi(recvPktLen);
						CstcLog_printf("Total_packet_length %d ", Total_packet_length);
#if COMPRESSION_DECOMPRESSION
						memset(unComDataLen,0,DATA_LENGTH_LEN);
						memcpy(unComDataLen,bTemp+START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN,DATA_LENGTH_LEN);
						CstcLog_printf("unComDataLen =%s bytes", unComDataLen);
						Uncompressed_data_length = 0;
						Uncompressed_data_length = (atoi(unComDataLen));
						CstcLog_printf("Uncompressed_data_length %d ", Uncompressed_data_length);
#endif

						memcpy(recvPktID,bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),PKT_ID_LEN);//dbTemp
						CstcLog_printf("recvPktID =%s bTemp[%d]=%s bytes", recvPktID,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN), bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN));

						memset(temp_cmd,0,50);
						memcpy(temp_cmd,bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN),1);//dbTemp
						gUpdateDataFlag=0;
						gUpdateDataFlag=atoi(temp_cmd);
						CstcLog_printf("updateDataFlag =%d bTemp[%d]=%c bytes", gUpdateDataFlag,(START_INDICATOR_LEN+DATA_LENGTH_LEN), bTemp[(START_INDICATOR_LEN+DATA_LENGTH_LEN)]);
						global_temp=0;
						global_temp=atoi(recvPktID);//temp;
						CstcLog_printf("global_temp %d ", global_temp);
						memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
						Err_Len=0;

						if((global_temp == SCHEDULE_MASTER_PKT_ID) || (global_temp == ROUTE_MASTER_PKT_ID) || /*(global_temp == FARE_CHART_PKT_ID) ||*/ (global_temp == ROUTE_HEADER_PKT_ID) )
						{

							CstcLog_printf("if global_temp == SCHEDULE_MASTER_PKT_ID");
#if COMPRESSION_DECOMPRESSION

							memcpy(recvRouteId,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)),SCHEDULE_NO_LEN);//dbTemp
#else
							memcpy(recvRouteId,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),SCHEDULE_NO_LEN);//dbTemp
#endif
							CstcLog_printf("recvRouteId =%s bTemp[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
							remove_stars(recvRouteId,result);
							CstcLog_printf("after remove_stars result %s ", result);
							memset(global_Route_Id,0,SCHEDULE_NO_LEN);
							strcpy(global_Route_Id,result);
							global_Route_Id[strlen(result)]='\0';
							CstcLog_printf("global_schedule = %s ", global_Route_Id);

						}

						else if(global_temp == 1)
						{

							CstcLog_printf("if global_temp == WAYBILL");
#if !COMPRESSION_DECOMPRESSION
							memcpy(recvWaybillNo,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+1)),WAYBILL_NO_LEN);//dbTemp
							CstcLog_printf("recvWaybillNo =%s bTemp[%d]=%s bytes", recvWaybillNo,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+1), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
							remove_space(recvWaybillNo,result_waybill);
							CstcLog_printf("after remove spaces result %s ", result_waybill);
							memset(global_waybill,0,WAYBILL_NO_LEN);
							strcpy(global_waybill,result_waybill);
							global_waybill[strlen(result_waybill)]='\0';
							CstcLog_printf("global_waybill = %s ", global_waybill);
#endif
						}
						else if(global_temp == 19)
						{
#if COMPRESSION_DECOMPRESSION

							CstcLog_printf("if global_temp == SCHEDULE_MASTER_PKT_ID");
							memcpy(recvRateId,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)),SCHEDULE_NO_LEN);//dbTemp
							CstcLog_printf("recvSchedule =%s bTemp[%d]=%s bytes", recvRateId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#else
							memcpy(recvRateId,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),SCHEDULE_NO_LEN);//dbTemp
#endif
							CstcLog_printf("recvRateId =%s bTemp[%d]=%s bytes", recvRateId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
							remove_stars(recvRateId,result);
							CstcLog_printf("after remove_stars result %s ", result);
							memset(global_Rate_Id,0,SCHEDULE_NO_LEN);
							strcpy(global_Rate_Id,result);
							global_Rate_Id[strlen(result)]='\0';
							CstcLog_printf("global_Rate_Id = %s ", global_Rate_Id);

						}

						else if(global_temp == FARE_CHART_PKT_ID)
						{
#if COMPRESSION_DECOMPRESSION

							memcpy(recvRouteId,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)),SCHEDULE_NO_LEN);//dbTemp
							CstcLog_printf("recvRouteId =%s bTemp[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#else
							memcpy(recvRouteId,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),SCHEDULE_NO_LEN);//dbTemp
							CstcLog_printf("recvRouteId =%s bTemp[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#endif
							remove_stars(recvRouteId,result);
							CstcLog_printf("after remove_stars result %s ", result);
							memset(global_Route_Id,0,SCHEDULE_NO_LEN);
							strcpy(global_Route_Id,result);
							global_Route_Id[strlen(result)]='\0';
							CstcLog_printf("global_schedule = %s ", global_Route_Id);

#if COMPRESSION_DECOMPRESSION
							CstcLog_printf("if global_temp == 9");

							memcpy(recvCount,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN)),3);//dbTemp
							CstcLog_printf("recvCount =%s bTemp[%d]=%s bytes rt_count=%d", recvCount,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN), bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),rt_count);
#else
							memcpy(recvCount,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)),3);//dbTemp
#endif
							recvCount[3]='\0';
							rt_count=0;
							rt_count=atoi(recvCount);
							CstcLog_printf("recvCount =%s bTemp[%d]=%s bytes rt_count=%d", recvCount,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN), bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),rt_count);
							CstcLog_printf("recvCount =%s rt_count=%d", recvCount,rt_count);
						}

						else if(global_temp == 22) //not used
						{
#if COMPRESSION_DECOMPRESSION

							CstcLog_printf("if global_temp == CHECK_ROUTE_PKT_ID");
							memcpy(recvRouteId,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)),ROUTE_ID_LEN);//dbTemp
							CstcLog_printf("recvRouteId =%s bTemp[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#else
							memcpy(recvRouteId,(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),ROUTE_ID_LEN);//dbTemp
							CstcLog_printf("recvRouteId =%s bTemp[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#endif
							remove_stars(recvRouteId,result);
							CstcLog_printf("after remove_stars result %s ", result);
							memset(global_Route_Id,0,ROUTE_ID_LEN);
							strcpy(global_Route_Id,result);
							global_Route_Id[strlen(result)]='\0';
							CstcLog_printf("global_Rate_Id = %s ", global_Route_Id);
						}
						else
						{
							CstcLog_printf(".............................");
						}
					}
			CstcLog_printf("Redbug:::--iTemp = %d Total_packet_length = %d",iTemp,Total_packet_length);

			if(iTemp != Total_packet_length)
			{
#if COMPRESSION_DECOMPRESSION

				CstcLog_printf("before global_buff malloc");

				global_buff = (char *)malloc((Total_packet_length-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+42))*sizeof(char));
				memset(global_buff,0,(Total_packet_length-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+42)));

				if(iTemp < (Total_packet_length-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+42)))
				{
					memcpy(global_buff,bTemp,(Total_packet_length-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+42)));//dbTemp,iTemp);
					global_buff[Total_packet_length-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+42)]='\0';
				}
				else
				{
					memcpy(global_buff,bTemp,2047);//dbTemp,iTemp);
					global_buff[2047]='\0';
				}
				CstcLog_printf("after global_buff malloc");

#else
				memset(global_buff,0,2048);

				if(iTemp < 2047)
				{
					memcpy(global_buff,bTemp,iTemp);//dbTemp,iTemp);
					global_buff[iTemp]='\0';
				}
				else
				{
					memcpy(global_buff,bTemp,2047);//dbTemp,iTemp);
					global_buff[2047]='\0';
				}
#endif

				CstcLog_printf("global_buff %s", global_buff);
				dbDataFile = NULL;
				if(global_temp == 60 )
					dbDataFile= fopen(UPDATED_PKG_FILE_ADDR,"w");
				else
					dbDataFile= fopen(DB_INPUT_FILE_ADDR,"w");
				if(dbDataFile == NULL)
				{
					CstcLog_printf("ERROR_OPENING_FILE_db_input");
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_db_input);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len=%d Err_Msg=%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					ack_flag=1;
					g_checksumFlag=0;
				}
				else
				{
					CstcLog_printf("Tempfile open success :%d",dbDataFile);

					if((global_temp != 60 )/* || (global_temp != 58 )*/ || (global_temp != 54) || (global_temp != 56) || (global_temp !=45) || (global_temp != 47) || (global_temp !=50)|| (global_temp !=02) ||(global_temp !=06) )
					{
						tempName=(char*)malloc(100*sizeof(char));
						memset(tempName,0,100);
						switch(global_temp)
						{
						CstcLog_printf("before insert table atoi(recvPktID) =%s ",global_temp);

						case 1 :/*-------------------WAYBILL----------------------*/
							memcpy(tempName, "waybill_master",14);
							break;

						case 3: /*-----------------SCHEDULE-----------------------*/
							memcpy(tempName, "schedule_master", 15);
							break;

						case 5:/*--------------------DUTY------------------------*/
							memcpy(tempName, "duty_master",11);
							break;

						case 7: /*-------------------ROUTE------------------------*/
							memcpy(tempName, "route_master",12);
							break;

						case 8:/*--------------------FARE RECORD LENGTH------------------------*/
#if COMPRESSION_DECOMPRESSION
							RecordCntToRecv = atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)));//dbTemp
							CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",RecordCntToRecv, atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN))));//dbTemp
#else
							RecordCntToRecv = atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
							CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",RecordCntToRecv, atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN))));//dbTemp
#endif
							TotalRecordCntRecvd=0;
							rt_count=0;
							CstcLog_printf(" In case 8 TotalRecordCntRecvd is %d ",TotalRecordCntRecvd);
							break;

						case 9:/*--------------------FARE------------------------*/
							memcpy(tempName, "fare_chart",10);
							CstcLog_printf("TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
							CstcLog_printf("Incr TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
							break;

						case 10:/*-----------------Ticket Type-------------------------*/
							memcpy(tempName, "ticket_type",11);
							break;

						case 11:/*---------------------Ticket sub Type------------------------*/
							memcpy(tempName, "ticket_sub_type",15);
							break;

						case 12:/*--------------------Bus Services------------------------------*/
							memcpy(tempName, "bus_service",11);
							break;

						case 13:/*----------------------Bus Brand-------------------------------*/
							memcpy(tempName, "bus_brand",9);
							break;

						case 14:/*----------------------Depot-------------------------------*/
							memcpy(tempName, "depot",5);
							break;

						case 15:/*-----------------Route Header------------------------*/
							memcpy(tempName, "route_header",12);
							break;

						case 16:/*--------------------ETM RW key---------------------------*/
							memcpy(tempName, "etim_rw_keys",13);
							break;

						case 17:/*--------------------TRUNK AND FEEDER---------------------------*/
							memcpy(tempName, "trunk_feeder_master",19);
							break;

						case 18:/*-------------------SERVICE OP-----------------------*/
							memcpy(tempName, "service_op",10);
							break;

						case 19:/*-------------------RATE MASTER -----------------------*/
							memcpy(tempName, "rate_master",11);
							break;

						case 23:/*-------------------LC Login -----------------------*/
							memcpy(tempName, "lc_login",8);
							break;

						case 24:/*----------------GPRS APN----------------------*/
							memcpy(tempName, "gprs_table",strlen("gprs_table"));
							break;

						case 25:/*----------------route_card----------------------*/
							memcpy(tempName, "route_card",strlen("route_card"));
							break;

						case 26:/*----------------shift_type----------------------*/
							memcpy(tempName, "shift_type",strlen("shift_type"));
							break;

						case 27:/*----------------concession_type----------------------*/
							memcpy(tempName, "concession_type",strlen("concession_type"));
							break;

						case 28:/*----------------Activity Master----------------------*/
							memcpy(tempName, "activity_master",strlen("activity_master"));
							break;

						default:
							break;
						}
						if((global_temp != 8)&&(global_temp >0))
						{
							CstcLog_printf("Its not packet id 8.............");
#if COMPRESSION_DECOMPRESSION
							DB_Insert_str = (char *)malloc(100*sizeof(char));
							memset(DB_Insert_str ,0 ,100);
							sprintf(DB_Insert_str,"insert into %s select",tempName );
							DB_Insert_str[strlen(DB_Insert_str)]='\0';
#else
							fprintf(dbDataFile,"%s","insert into ");
							CstcLog_printf("insert into");
							fprintf(dbDataFile,"%s",tempName);
							CstcLog_printf("%s",tempName);
							fprintf(dbDataFile,"%s"," select ");
							CstcLog_printf("select");
#endif
						}
						free(tempName);
						tempName=NULL;
					}
					if((global_temp == SCHEDULE_MASTER_PKT_ID) || (global_temp == 7) || (global_temp == 15) || (global_temp == 19) )//|| (global_temp == 1))
					{

						/************ redbug:- erase schedule_master for new insert ***************/
						if((global_temp == SCHEDULE_MASTER_PKT_ID)){
							if(DbInterface_Delete_Table("Delete from schedule_master")!=1)
								{
									CstcLog_printf("Redbug 1:: Delete from schedule_master FAILED");
									return 1;
								}
						}


						CstcLog_printf("Inside if(global_temp == SCHEDULE_MASTER_PKT_ID)");
#if COMPRESSION_DECOMPRESSION
//						fprintf(dbDataFile,"%s",uncompData);//dbTemp
//						free(DB_Insert_str);
//						DB_Insert_str=NULL;
#else
						fprintf(dbDataFile,"%s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)));//dbTemp
#endif
					}
					else if(global_temp == 9 )
					{
						CstcLog_printf("Inside if(global_temp == SCHEDULE_MASTER_PKT_ID)");
#if COMPRESSION_DECOMPRESSION
//						fprintf(dbDataFile,"%s",uncompData);//dbTemp
//						free(DB_Insert_str);
//						DB_Insert_str=NULL;
#else
						CstcLog_printf("(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3) = %s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)));
						fprintf(dbDataFile,"%s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)));//dbTemp
#endif
					}
					else if(global_temp == 60 )  ///Store pkg file data in pkg file here
					{
						CstcLog_printf("Inside if(global_temp == SCHEDULE_MASTER_PKT_ID)");
						fprintf(dbDataFile,"%s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)));//dbTemp
					}
					else if(global_temp > 0 ) //if((global_temp == 54) || (global_temp == 56) || (global_temp ==45) || (global_temp == 47) || (global_temp ==50)|| (global_temp ==02) ||(global_temp ==06) )//
					{
#if COMPRESSION_DECOMPRESSION
//						fprintf(dbDataFile,"%s",uncompData);//dbTemp
//						free(DB_Insert_str);
//						DB_Insert_str=NULL;
#else
						fprintf(dbDataFile,"%s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
#endif
					}
					fflush(dbDataFile);
					fclose(dbDataFile);
				}
				dbDataFile = NULL;
				//			memset(dbTemp,0,sizeof(dbTemp));
				global_iTemp=iTemp;
				CstcLog_printf("global_iTemp(%d) in iTemp(%d) != Total_packet_length condition  %d", global_iTemp,iTemp,Total_packet_length);
				New_pkt_recv_flag=1;
				return 0;
			}
			else
			{
				dbDataFile = NULL;
				if(global_temp == 60 )
					dbDataFile= fopen(UPDATED_PKG_FILE_ADDR,"w");
				else
					dbDataFile= fopen(DB_INPUT_FILE_ADDR,"w");
				if(dbDataFile == NULL)
				{
					CstcLog_printf("ERROR_OPENING_FILE_db_input");
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_db_input);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len=%d Err_Msg=%s Err_MsgAry= %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					ack_flag=1;
				}
				else
				{
					CstcLog_printf("Tempfile open success :%d",dbDataFile);

					if((global_temp != 60 )/* || (global_temp != 58 ) */|| (global_temp != 54) || (global_temp != 56) || (global_temp !=45) || (global_temp != 47) || (global_temp !=50) || (global_temp !=02) ||(global_temp !=06) )
					{
						tempName=(char*)malloc(100*sizeof(char));
						memset(tempName,0,100);
						switch(global_temp)
						{
						CstcLog_printf("before insert table atoi(recvPktID)=%s ",global_temp);

						case 1 :/*-------------------WAYBILL----------------------*/
							memcpy(tempName, "waybill_master",14);
							break;

						case 3: /*-----------------SCHEDULE-----------------------*/
							memcpy(tempName, "schedule_master", 15);
							break;

						case 5:/*--------------------DUTY------------------------*/
							memcpy(tempName, "duty_master",11);
							break;

						case 7: /*-------------------ROUTE------------------------*/
							memcpy(tempName, "route_master",12);
							break;

						case 8:/*--------------------FARE RECORD LENGTH------------------------*/

#if COMPRESSION_DECOMPRESSION
							RecordCntToRecv = atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)));//dbTemp
							CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",RecordCntToRecv, atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN))));//dbTemp
#else
							RecordCntToRecv = atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
							CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",RecordCntToRecv, atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN))));//dbTemp
#endif
							TotalRecordCntRecvd=0;
							rt_count=0;
							CstcLog_printf(" In case 8 TotalRecordCntRecvd is %d ",TotalRecordCntRecvd);
							break;

						case 9:/*--------------------FARE------------------------*/
							memcpy(tempName, "fare_chart",10); /*--search for union all select in dbTemp add that count in TotalRecordCntRecvd--*/
							CstcLog_printf("TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
							CstcLog_printf("Incr TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
							break;

						case 10:/*-----------------Ticket Type-------------------------*/
							memcpy(tempName, "ticket_type",11);
							break;

						case 11:/*---------------------Ticket sub Type------------------------*/
							memcpy(tempName, "ticket_sub_type",15);
							break;

						case 12:/*--------------------Bus Services------------------------------*/
							memcpy(tempName, "bus_service",11);
							break;

						case 13:/*----------------------Bus Brand-------------------------------*/
							memcpy(tempName, "bus_brand",9);
							break;

						case 14:/*----------------------Depot-------------------------------*/
							memcpy(tempName, "depot",5);
							break;

						case 15:/*-----------------Route Header------------------------*/
							memcpy(tempName, "route_header",12);
							break;

						case 16:/*--------------------ETM RW key---------------------------*/
							memcpy(tempName, "etim_rw_keys",13);
							break;

						case 17:/*--------------------TRUNK AND FEEDER---------------------------*/
							memcpy(tempName, "trunk_feeder_master",19);
							break;

						case 18:/*-------------------SERVICE OP-----------------------*/
							memcpy(tempName, "service_op",10);
							break;

						case 19:/*-------------------RATE MASTER -----------------------*/
							memcpy(tempName, "rate_master",11);
							break;

						case 23:/*-------------------LC Login-----------------------*/
							memcpy(tempName, "lc_login",8);
							break;

						case 24:/*-------------------GPRS APN-----------------------*/
							memcpy(tempName, "gprs_table",strlen("gprs_table"));
							break;

						case 25:/*----------------route_card----------------------*/
							memcpy(tempName, "route_card",strlen("route_card"));
							break;

						case 26:/*----------------shift_type----------------------*/
							memcpy(tempName, "shift_type",strlen("shift_type"));
							break;

						case 27:/*----------------concession_type----------------------*/
							memcpy(tempName, "concession_type",strlen("concession_type"));
							break;

						case 28:/*----------------Activity Master----------------------*/
							memcpy(tempName, "activity_master",strlen("activity_master"));
							break;

						default:
							break;
						}
						if(global_temp != 8)
						{
#if COMPRESSION_DECOMPRESSION
							DB_Insert_str = (char *)malloc(100*sizeof(char));
							memset(DB_Insert_str ,0 ,100);
							sprintf(DB_Insert_str,"insert into %s select",tempName );
							DB_Insert_str[strlen(DB_Insert_str)]='\0';
#else
							fprintf(dbDataFile,"%s","insert into ");
							CstcLog_printf("insert into");
							fprintf(dbDataFile," %s ",tempName);
							CstcLog_printf("%s",tempName);
							fprintf(dbDataFile,"%s"," select ");
							CstcLog_printf("select");
#endif
						}
						free(tempName);
						tempName=NULL;

					}
					if((global_temp == SCHEDULE_MASTER_PKT_ID) || (global_temp == 7) || (global_temp == 15) || (global_temp == 19))// || (global_temp == 1))
					{
						CstcLog_printf("Inside if(global_temp == SCHEDULE_MASTER_PKT_ID)");

						/************ redbug:- erase schedule_master for new insert ***************/
						if((global_temp == SCHEDULE_MASTER_PKT_ID)){
							if(DbInterface_Delete_Table("Delete from schedule_master")!=1)
							{
								CstcLog_printf("Redbug 2 :: Delete from schedule_master FAILED");
								return 1;
							}
						}
#if COMPRESSION_DECOMPRESSION

						decompression(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN),(Total_packet_length-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+43)));
						if(DB_Insert_str != NULL)
						{
							free(DB_Insert_str);
							DB_Insert_str=NULL;
						}
#else
						fprintf(dbDataFile,"%s;",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)));//dbTemp
#endif
					}
					else if(global_temp == 9 )
					{
						CstcLog_printf("Inside if(global_temp == 9)");
#if COMPRESSION_DECOMPRESSION
						//fprintf(dbDataFile,"%s",uncompData);//dbTemp
						decompression(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3),(Total_packet_length-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3+43)));
						if(DB_Insert_str != NULL)
						{
							free(DB_Insert_str);
							DB_Insert_str=NULL;
						}
#else
						CstcLog_printf("(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3) = %s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)));
						fprintf(dbDataFile,"%s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)));//dbTemp
#endif
					}
					//					else if(global_temp == 58 )  ///Store pkg file data in pkg file here
					//					{
					//						CstcLog_printf("Inside if(global_temp == SCHEDULE_MASTER_PKT_ID)");
					//						RecordCntToRecv = atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+MACHINE_SN_LEN)));//dbTemp
					//						CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",RecordCntToRecv, atoi((bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+MACHINE_SN_LEN))));//dbTemp
					//						TotalRecordCntRecvd=0;
					//					}
					else if(global_temp == 60 )  ///Store pkg file data in pkg file here
					{
						CstcLog_printf("Inside if(global_temp == 60)");
#if COMPRESSION_DECOMPRESSION
//						fprintf(dbDataFile,"%s",uncompData);//dbTemp
						decompression(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN),(Total_packet_length-((START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+43))));
						if(DB_Insert_str != NULL)
						{
							free(DB_Insert_str);
							DB_Insert_str=NULL;
						}
#else
						fprintf(dbDataFile,"%s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN/*+MACHINE_SN_LEN*/)));//dbTemp
#endif
					}
					else
					{
#if COMPRESSION_DECOMPRESSION
//						fprintf(dbDataFile,"%s",uncompData);//dbTemp
						CstcLog_printf("hereeeeeeee............global_temp = %d",global_temp);
						if((global_temp == GET_DETAILS_PKT_ID) || (global_temp == REGISTRATION_REQUEST_PKT_ID)|| (global_temp == SCHEDULE_DETAILS_PKT_ID)|| (global_temp == START_DUTY_PKT_ID))
						{
							CstcLog_printf("hereeeeeeee............1");
						}
						else
						{
							decompression(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN),(Total_packet_length-((START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+43))));
							CstcLog_printf("hereeeeeeee............3");
						}
						if(DB_Insert_str != NULL)
						{
							free(DB_Insert_str);
							DB_Insert_str=NULL;
						}
						CstcLog_printf("hereeeeeeee............4");

#else
						fprintf(dbDataFile,"%s",(bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
#endif
					}
					CstcLog_printf("hereeeeeeee............5");
#if !COMPRESSION_DECOMPRESSION
					fflush(dbDataFile);
					fclose(dbDataFile);
#endif
					CstcLog_printf("hereeeeeeee............6");

				}
				CstcLog_printf("hereeeeeeee............7");

				dbDataFile = NULL;
				global_iTemp=iTemp;
				CstcLog_printf("global_iTemp in else condition %d", global_iTemp);
				if(UART_received_packet_lan(global_temp, tempCh, iclient_socket,bTemp, gUpdateDataFlag,j) == 1)//dbTemp
					memset(temp_cmd,0,50);
				CstcLog_printf("1. If fail ack then crashes");
				strcpy(temp_cmd,"rm -f ");
				if(global_temp == 60 )
					strcat(temp_cmd,UPDATED_PKG_FILE_ADDR);
				else
					strcat(temp_cmd,DB_INPUT_FILE_ADDR);
				CstcLog_printf("2. If fail ack then crashes");
				CstcLog_printf("temp_cmd :%s",temp_cmd);
				system(temp_cmd);
				memset(temp_cmd,0,50);
				CstcLog_printf("3. If fail ack then crashes");
#if COMPRESSION_DECOMPRESSION
				if(global_buff != NULL)
				{
					CstcLog_printf("1.Don't want to do serial........");

					free(global_buff);
					global_buff = NULL;
				}
				CstcLog_printf("2.Don't want to do serial........");

				if(Uncompressed_data != NULL)
				{
					CstcLog_printf("3.Don't want to do serial........");

					free(Uncompressed_data);
					Uncompressed_data = NULL;
				}
				CstcLog_printf("4.Don't want to do serial........");

				memset(tempCh,0,sizeof(tempCh));


#else
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(bTemp,0,sizeof(bTemp));
				CstcLog_printf("4. If fail ack then crashes");
				sub_string=NULL;//memset(dbTemp,0,sizeof(dbTemp));
				memset(global_schedule,0,sizeof(global_schedule));
				memset(global_Rate_Id,0,sizeof(global_Rate_Id));
				memset(global_Route_Id,0,sizeof(global_Route_Id));
				CstcLog_printf("5. If fail ack then crashes");
				Total_packet_length=0;
				Uncompressed_data_length=0;
				gUpdateDataFlag=0;
				g_checksumFlag=0;

				CstcLog_printf("6. If fail ack then crashes");
				if(global_temp == 9)
					sleep(2);
				else
					usleep(500000);
				if(global_temp == 60 )
				{
					global_temp=0;
					CstcLog_printf("7. If fail ack then crashes");
					return 2;
				}
				else
				{
					global_temp=0;
					CstcLog_printf("7. If fail ack then crashes");
					//			sleep(2);	// 	sleep(1);  modified on 14_oct
					return 1;
				}
			}
		}
		else
		{
			if(New_pkt_recv_flag)
			{
				CstcLog_printf("Inside (iTemp(%d)+global_iTemp(%d)) == Total_packet_length(%d)",iTemp,global_iTemp,Total_packet_length);
				if((iTemp+global_iTemp)<2047)
				{
					CstcLog_printf("5 iTemp = %d",iTemp);
					//				strncat(global_buff,bTemp,iTemp);//dbTemp,iTemp);
					memcpy(global_buff+global_iTemp,bTemp,iTemp);//dbTemp,iTemp);
					global_buff[(iTemp+global_iTemp)]='\0';
				}
				else if((global_iTemp)<2047)
				{
					CstcLog_printf("5 g_checksumFlag = %d",g_checksumFlag);
					//				strncat(global_buff,bTemp,(2047-global_iTemp-1));//dbTemp,iTemp);
					memcpy(global_buff+global_iTemp,bTemp,(2047-global_iTemp-1));//dbTemp,iTemp);
					global_buff[2047]='\0';
				}

				CstcLog_printf("6 Inside global_iTemp(%d) == Total_packet_length(%d)",global_iTemp,Total_packet_length);
				//			else
				//			{
				////				memcpy(global_buff,bTemp,2048);//dbTemp,iTemp);
				//				global_buff[2048]='\0';
				//			}

				CstcLog_printf("6 global_buff %s", global_buff);

#if COMPRESSION_DECOMPRESSION
				if(((iTemp+global_iTemp) >= (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3))
						&&(global_iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3)))
#else
					if(((iTemp+global_iTemp) >= (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3))
							&&(global_iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)))
#endif
					{
						CstcLog_printf("global_iTemp < 30 Refill all data from global_buff");
#if COMPRESSION_DECOMPRESSION
						memset(Global_tempCh,0,3);
						memcpy(Global_tempCh,global_buff,3);
#endif
						memset(recvPktLen,0,DATA_LENGTH_LEN);
						memcpy(recvPktLen,global_buff+START_INDICATOR_LEN,DATA_LENGTH_LEN);

						CstcLog_printf("recvPktLen =%s bytes", recvPktLen);
						Total_packet_length = 0;
						Total_packet_length = atoi(recvPktLen);
						CstcLog_printf("Total_packet_length %d ", Total_packet_length);
#if COMPRESSION_DECOMPRESSION
						memset(unComDataLen,0,DATA_LENGTH_LEN);
						memcpy(unComDataLen,global_buff+START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN,DATA_LENGTH_LEN);

						CstcLog_printf("unComDataLen =%s bytes", unComDataLen);
						Uncompressed_data_length = 0;
						Uncompressed_data_length = (atoi(unComDataLen));
						CstcLog_printf("Uncompressed_data_length %d ", Uncompressed_data_length);
#endif
						memcpy(recvPktID,global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),PKT_ID_LEN);//dglobal_buff
						CstcLog_printf("recvPktID =%s global_buff[%d]=%s bytes", recvPktID,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN), global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN));

						memset(temp_cmd,0,50);
						memcpy(temp_cmd,global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN),1);//dglobal_buff
						gUpdateDataFlag=0;
						gUpdateDataFlag=atoi(temp_cmd);
						CstcLog_printf("updateDataFlag =%d global_buff[%d]=%c bytes", gUpdateDataFlag,(START_INDICATOR_LEN+DATA_LENGTH_LEN), global_buff[(START_INDICATOR_LEN+DATA_LENGTH_LEN)]);
						global_temp=0;
						global_temp=atoi(recvPktID);//temp;
						CstcLog_printf("global_temp %d ", global_temp);
						memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
						Err_Len=0;

						if((global_temp == SCHEDULE_MASTER_PKT_ID) || (global_temp == ROUTE_MASTER_PKT_ID) || /*(global_temp == FARE_CHART_PKT_ID) ||*/ (global_temp == ROUTE_HEADER_PKT_ID) )
						{
#if COMPRESSION_DECOMPRESSION

							CstcLog_printf("if global_temp == SCHEDULE_MASTER_PKT_ID");
							memcpy(recvRouteId,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)),SCHEDULE_NO_LEN);//dglobal_buff
							CstcLog_printf("recvRouteId =%s global_buff[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN), (global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+DATA_LENGTH_LEN)));
#else
							memcpy(recvRouteId,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),SCHEDULE_NO_LEN);//dglobal_buff
							CstcLog_printf("recvRouteId =%s global_buff[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#endif
							remove_stars(recvRouteId,result);
							CstcLog_printf("after remove_stars result %s ", result);
							memset(global_Route_Id,0,SCHEDULE_NO_LEN);
							strcpy(global_Route_Id,result);
							global_Route_Id[strlen(result)]='\0';
							CstcLog_printf("global_schedule = %s ", global_Route_Id);
						}

						else if(global_temp == 1)
						{
							CstcLog_printf("if global_temp == WAYBILL");

#if !COMPRESSION_DECOMPRESSION
							memcpy(recvWaybillNo,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+1)),WAYBILL_NO_LEN);//dbTemp
							CstcLog_printf("recvWaybillNo =%s bTemp[%d]=%s bytes", recvWaybillNo,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+1), (bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
							remove_space(recvWaybillNo,result_waybill);
							CstcLog_printf("after remove_stars result %s ", result_waybill);
							memset(global_waybill,0,WAYBILL_NO_LEN);
							strcpy(global_waybill,result_waybill);
							global_waybill[strlen(result_waybill)]='\0';
							CstcLog_printf("global_waybill = %s ", global_waybill);
#endif
						}
						else if(global_temp == 19)
						{
#if COMPRESSION_DECOMPRESSION

							CstcLog_printf("if global_temp == SCHEDULE_MASTER_PKT_ID");
							memset(recvRateId,0,(SCHEDULE_NO_LEN+1));
							memcpy(recvRateId,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)),SCHEDULE_NO_LEN);//dglobal_buff
							CstcLog_printf("recvSchedule =%s global_buff[%d]=%s bytes", recvRateId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN), (global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#else
							memset(recvRateId,0,(SCHEDULE_NO_LEN+1));
							memcpy(recvRateId,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),SCHEDULE_NO_LEN);//dglobal_buff
							CstcLog_printf("recvSchedule =%s global_buff[%d]=%s bytes", recvRateId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#endif
							remove_stars(recvRateId,result);
							CstcLog_printf("after remove_stars result %s ", result);
							memset(global_Rate_Id,0,SCHEDULE_NO_LEN);
							strcpy(global_Rate_Id,result);
							global_Rate_Id[strlen(result)]='\0';
							CstcLog_printf("global_Rate_Id = %s ", global_Rate_Id);
						}

						else if(global_temp == FARE_CHART_PKT_ID)
						{
#if COMPRESSION_DECOMPRESSION
							//						memset(Data_to_UnComp,0,sizeof(Data_to_UnComp));

							memcpy(recvRouteId,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)),SCHEDULE_NO_LEN);//dglobal_buff
							CstcLog_printf("recvRouteId =%s global_buff[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN), (global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+DATA_LENGTH_LEN)));
#else
							memcpy(recvRouteId,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),SCHEDULE_NO_LEN);//dglobal_buff
							CstcLog_printf("recvRouteId =%s global_buff[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
#endif
							remove_stars(recvRouteId,result);
							CstcLog_printf("after remove_stars result %s ", result);
							memset(global_Route_Id,0,SCHEDULE_NO_LEN);
							strcpy(global_Route_Id,result);
							global_Route_Id[strlen(result)]='\0';
							CstcLog_printf("global_schedule = %s ", global_Route_Id);

							CstcLog_printf("if global_temp == 9");
							memset(recvCount,0,5);
#if COMPRESSION_DECOMPRESSION
							memcpy(recvCount,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN)),3);//dglobal_buff
#else
							memcpy(recvCount,(global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)),3);//dglobal_buff
#endif
							recvCount[3]='\0';
							rt_count=0;
							rt_count=atoi(recvCount);
							CstcLog_printf("recvCount =%s global_buff[%d]=%s bytes rt_count=%d", recvCount,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN), global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),rt_count);
							CstcLog_printf("recvCount =%s rt_count=%d", recvCount,rt_count);

						}
						else
						{
							CstcLog_printf("----------***********--------------");
						}
					}

				CstcLog_printf("global_iTemp + iTemp  %d", iTemp+global_iTemp);
				CstcLog_printf("In New_flag TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
				/*if(global_temp == 9)
				{
					//				CstcLog_printf("In New_flag sub_string_len = %d ",sub_string_len);
					//
					if (global_iTemp<=30)
					{
						CstcLog_printf("2 g_checksumFlag = %d",g_checksumFlag);
						ptr_inc=0;
						ptr_inc=(30-global_iTemp);
					}
					else
						ptr_inc=0;
					CstcLog_printf("Incr in New_flag TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
				}
				CstcLog_printf("1 g_checksumFlag = %d",g_checksumFlag);*/
#if COMPRESSION_DECOMPRESSION
				if ( (global_iTemp<=37) && //Swap:This needs to be look into for rt_count
						((global_temp == SCHEDULE_MASTER_PKT_ID) || (global_temp == 7) || (global_temp == 15)|| (global_temp == 19))
				)
#else
					if ( (global_iTemp<=27) && //Swap:This needs to be look into for rt_count
										((global_temp == SCHEDULE_MASTER_PKT_ID) || (global_temp == 7) || (global_temp == 15)|| (global_temp == 19))
								)
#endif
				{
					CstcLog_printf("2 g_checksumFlag = %d",g_checksumFlag);
					ptr_inc=0;
#if COMPRESSION_DECOMPRESSION
					ptr_inc=(37-global_iTemp);
#else
					ptr_inc=(27-global_iTemp);
#endif
//					if(global_temp == 19)
//					{
//						ptr_inc=(37-global_iTemp);
//					}
//					else
//						ptr_inc=(27-global_iTemp);
				}
				else
				{
					ptr_inc=0;
#if COMPRESSION_DECOMPRESSION
					ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#endif
					//ptr_inc=0;
				}

				if(global_temp == 9)
				{
					//				CstcLog_printf("In New_flag sub_string_len = %d ",sub_string_len);
					//
					if (global_iTemp<=30)
					{
						CstcLog_printf("2 g_checksumFlag = %d",g_checksumFlag);
						ptr_inc=0;
						ptr_inc=(30-global_iTemp);
					}
					else
						ptr_inc=0;
					CstcLog_printf("Incr in New_flag TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
				}
				CstcLog_printf("1 g_checksumFlag = %d",g_checksumFlag);

				CstcLog_printf("ptr_inc %d ,global_schedule  %s",ptr_inc,global_schedule);
				CstcLog_printf("In New_flag dbTemp = %s ",bTemp);//dbTemp
				if(!g_checksumFlag)
				{
					CstcLog_printf("3 g_checksumFlag = %d",g_checksumFlag);
					dbDataFile = NULL;
					if(global_temp == 60 )
						dbDataFile= fopen(UPDATED_PKG_FILE_ADDR,"a+");
					else
						dbDataFile= fopen(DB_INPUT_FILE_ADDR,"a+");
					if(dbDataFile == NULL)
					{
						CstcLog_printf("ERROR_OPENING_FILE_db_input");
						sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_db_input);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len = Err_Len + 2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);

						ack_flag=1;
						memset(temp_cmd,0,50);
						strcpy(temp_cmd,"rm -f ");
						strcat(temp_cmd,DB_INPUT_FILE_ADDR);
						CstcLog_printf("temp_cmd :%s",temp_cmd);
						system(temp_cmd);
						memset(temp_cmd,0,50);
						memset(bTemp,0,sizeof(bTemp));
						sub_string=NULL;//memset(dbTemp,0,sizeof(dbTemp));
						memset(global_schedule,0,sizeof(global_schedule));
						memset(global_Rate_Id,0,sizeof(global_Rate_Id));
						memset(global_Route_Id,0,sizeof(global_Route_Id));
						gUpdateDataFlag=0;
#if COMPRESSION_DECOMPRESSION
					if(global_buff != NULL)
					{
						free(global_buff);
						global_buff = NULL;
					}
					if(Uncompressed_data != NULL)
					{
						free(Uncompressed_data);
						Uncompressed_data = NULL;
					}
					memset(tempCh,0,sizeof(tempCh));

#else
					memset(global_buff,0,sizeof(global_buff));
#endif
					}
					else
					{
						CstcLog_printf("1.Tempfile open success :%d",dbDataFile);


						g_ptr_inc=0;
#if COMPRESSION_DECOMPRESSION
						memset(Global_tempCh,0,3);
						memcpy(Global_tempCh,global_buff,3);
						Global_tempCh[strlen(Global_tempCh)]='\0';
						CstcLog_printf("Global_tempCh =%s",Global_tempCh);

						if(((iTemp+global_iTemp) >= (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3))
								&&(global_iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3)))
#else
							if(((iTemp+global_iTemp) >= (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3))
									&&(global_iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)))
#endif
							{
								tempName=(char*)malloc(100*sizeof(char));
								memset(tempName,0,100);
								CstcLog_printf("global_iTemp < 30 Refill all data in db_input.txt file");
								if((global_temp != 60) || (global_temp != 54) || (global_temp != 56) || (global_temp !=45) || (global_temp != 47) || (global_temp !=50) || (global_temp !=02) ||(global_temp !=06) )
								{
									switch(global_temp)
									{
									CstcLog_printf("before insert table atoi(recvPktID)=%s ",global_temp);

									case 1 :/*-------------------WAYBILL----------------------*/
										memcpy(tempName, "waybill_master",14);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 3: /*-----------------SCHEDULE-----------------------*/
										memcpy(tempName, "schedule_master", 15);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
#endif
										break;

									case 5:/*--------------------DUTY------------------------*/
										memcpy(tempName, "duty_master",11);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 7: /*-------------------ROUTE------------------------*/
										memcpy(tempName, "route_master",12);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
#endif
										break;

									case 8:/*--------------------FARE RECORD LENGTH------------------------*/
#if COMPRESSION_DECOMPRESSION
										RecordCntToRecv = atoi((global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN)));//dbTemp
										CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",RecordCntToRecv, atoi((global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN))));//dbTemp
										TotalRecordCntRecvd=0;
										rt_count=0;
										CstcLog_printf(" In case 8 TotalRecordCntRecvd is %d ",TotalRecordCntRecvd);
#else
										RecordCntToRecv = atoi((global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
										CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",RecordCntToRecv, atoi((global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN))));//dbTemp
										TotalRecordCntRecvd=0;
										rt_count=0;
										CstcLog_printf(" In case 8 TotalRecordCntRecvd is %d ",TotalRecordCntRecvd);
#endif
										break;

									case 9:/*--------------------FARE------------------------*/
										memcpy(tempName, "fare_chart",10); /*--search for union all select in dbTemp add that count in TotalRecordCntRecvd--*/
										CstcLog_printf("TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
										CstcLog_printf("Incr TotalRecordCntRecvd = %d ",TotalRecordCntRecvd);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN+3;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3;
#endif
										break;

									case 10:/*-----------------Ticket Type-------------------------*/
										memcpy(tempName, "ticket_type",11);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 11:/*---------------------Ticket sub Type------------------------*/
										memcpy(tempName, "ticket_sub_type",15);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#endif
										break;

									case 12:/*--------------------Bus Services------------------------------*/
										memcpy(tempName, "bus_service",11);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 13:/*----------------------Bus Brand-------------------------------*/
										memcpy(tempName, "bus_brand",9);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 14:/*----------------------Depot-------------------------------*/
										memcpy(tempName, "depot",5);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 15:/*-----------------Route Header------------------------*/
										memcpy(tempName, "route_header",12);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
#endif
										break;

									case 16:/*--------------------ETM RW key---------------------------*/
										memcpy(tempName, "etim_rw_keys",13);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 17:/*--------------------TRUNK AND FEEDER---------------------------*/
										memcpy(tempName, "trunk_feeder_master",19);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 18:/*-------------------SERVICE OP-----------------------*/
										memcpy(tempName, "service_op",10);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 19:/*-------------------SERVICE OP-----------------------*/
										memcpy(tempName, "rate_master",11);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN+SCHEDULE_NO_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
#endif
										break;
									case 23:/*-----------------LC LOGIN------------------------*/
										memcpy(tempName, "lc_login",8);
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 24:/*-------------------GPRS APN-----------------------*/
										memcpy(tempName, "gprs_table",strlen("gprs_table"));
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 25:/*----------------route_card----------------------*/
										memcpy(tempName, "route_card",strlen("route_card"));
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 26:/*----------------shift_type----------------------*/
										memcpy(tempName, "shift_type",strlen("shift_type"));
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 27:/*----------------concession_type----------------------*/
										memcpy(tempName, "concession_type",strlen("concession_type"));
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									case 28:/*----------------activity_master----------------------*/
										memcpy(tempName, "activity_master",strlen("activity_master"));
#if COMPRESSION_DECOMPRESSION
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+DATA_LENGTH_LEN;
#else
										g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
#endif
										break;

									default:
										break;
									}
									if(global_temp != 8)
									{
#if COMPRESSION_DECOMPRESSION
										DB_Insert_str = (char *)malloc(100*sizeof(char));
										memset(DB_Insert_str ,0 ,100);
										sprintf(DB_Insert_str,"insert into %s select",tempName );
										DB_Insert_str[strlen(DB_Insert_str)]='\0';
#else
										fprintf(dbDataFile,"%s","insert into ");
										CstcLog_printf("insert into");
										fprintf(dbDataFile," %s ",tempName);
										CstcLog_printf("%s",tempName);
										fprintf(dbDataFile,"%s"," select ");
										CstcLog_printf("select");
#endif
									}
									free(tempName);
									tempName=NULL;
								}
								if(global_iTemp>g_ptr_inc)
								{
									tempInitBuff=(char*)malloc(50*sizeof(char));
									memset(tempInitBuff,0,50);
									memcpy(tempInitBuff,(global_buff+g_ptr_inc),(global_iTemp-g_ptr_inc));
									tempInitBuff[(global_iTemp-g_ptr_inc)]='\0';
									CstcLog_printf("tempInitBuff :%s",tempInitBuff);
									fprintf(dbDataFile,"%s",tempInitBuff);//dbTemp
									free(tempInitBuff);
									tempInitBuff=NULL;
								}
							}
						CstcLog_printf("Tempfile open success :%d",dbDataFile);
#if COMPRESSION_DECOMPRESSION
						CstcLog_printf("(global_buff+g_ptr_inc) = %s",(global_buff+g_ptr_inc) );
						CstcLog_printf("(global_buff+ptr_inc) = %s",(global_buff+ptr_inc) );
						CstcLog_printf("(Total_packet_length-(g_ptr_inc+43)) = %d\n ptr_inc =%d",(Total_packet_length-(g_ptr_inc+43)),ptr_inc);

						if((global_temp == GET_DETAILS_PKT_ID) || (global_temp == REGISTRATION_REQUEST_PKT_ID) || (global_temp == START_DUTY_PKT_ID))
						{
							CstcLog_printf("sorry for handling UART");
						}
						else
						{
							decompression((global_buff+ptr_inc),(Total_packet_length-(ptr_inc+43)));
						}
//						fprintf(dbDataFile,"%s",(uncompData+ptr_inc));
						if(DB_Insert_str != NULL)
						{
							free(DB_Insert_str);
							DB_Insert_str = NULL;
						}
#else
						CstcLog_printf("(bTemp+ptr_inc) = %s",(bTemp+ptr_inc));
						fprintf(dbDataFile,"%s",(bTemp+ptr_inc));//dbTemp
#endif
					}
				}
				if(checksumFlag)
				{
					CstcLog_printf("4 g_checksumFlag = %d",g_checksumFlag);
					g_checksumFlag =1;
					CstcLog_printf("5 g_checksumFlag = %d",g_checksumFlag);
				}

				global_iTemp=iTemp+global_iTemp;
				CstcLog_printf("Inside global_iTemp(%d) == Total_packet_length(%d)",global_iTemp,Total_packet_length);
				//			else
				//			{
				////				memcpy(global_buff,bTemp,2048);//dbTemp,iTemp);
				//				global_buff[2048]='\0';
				//			}

				CstcLog_printf("global_buff %s", global_buff);

				if(global_iTemp == Total_packet_length)
				{
					CstcLog_printf("Inside iTemp(%d),global_iTemp(%d) == Total_packet_length(%d)",iTemp,global_iTemp,Total_packet_length);
					//				fprintf(dbDataFile,"%s",(char*)";");
					CstcLog_printf("Before checking dbDataFile ");
					if(dbDataFile != NULL)
					{
						CstcLog_printf("After checking dbDataFile ");
						fflush(dbDataFile);
						fclose(dbDataFile);
						dbDataFile = NULL;
						CstcLog_printf("After closing dbDataFile ");
					}
					CstcLog_printf("For closed dbDataFile  ");
#if COMPRESSION_DECOMPRESSION
					if(UART_received_packet_lan(global_temp, Global_tempCh,iclient_socket,global_buff, gUpdateDataFlag,j) == 1) //global_buff[0]
						memset(temp_cmd,0,50);
#else
					if(UART_received_packet_lan(global_temp, global_buff[0],iclient_socket,global_buff, gUpdateDataFlag,j) == 1) //
						memset(temp_cmd,0,50);
#endif
					CstcLog_printf("8. If fail ack then crashes");
					strcpy(temp_cmd,"rm -f ");
					if(global_temp == 60 )
						strcat(temp_cmd,UPDATED_PKG_FILE_ADDR);
					else
						strcat(temp_cmd,DB_INPUT_FILE_ADDR);
					CstcLog_printf("9. If fail ack then crashes");
					CstcLog_printf("temp_cmd :%s",temp_cmd);
					system(temp_cmd);
					memset(temp_cmd,0,50);
					CstcLog_printf("10. If fail ack then crashes");

					New_pkt_recv_flag=0;
#if COMPRESSION_DECOMPRESSION
					if(global_buff != NULL)
					{
						free(global_buff);
						global_buff = NULL;
					}
					if(Uncompressed_data != NULL)
					{
						free(Uncompressed_data);
						Uncompressed_data = NULL;
					}
					memset(tempCh,0,sizeof(tempCh));


#else
					memset(global_buff,0,sizeof(global_buff));
#endif
					memset(bTemp,0,sizeof(bTemp));
					CstcLog_printf("11. If fail ack then crashes");
					memset(g_checksumbuffer,0,sizeof(g_checksumbuffer));
					//					memset(CRC_Data,0,sizeof(CRC_Data));
					//		memset(check_crc,0,sizeof(check_crc));
					//		memset(Calculated_crc,0,sizeof(Calculated_crc));
					sub_string=NULL;//memset(dbTemp,0,sizeof(dbTemp));
					gUpdateDataFlag=0;
					Total_packet_length=0;
					Uncompressed_data_length=0;
					g_checksumFlag=0;

					CstcLog_printf("12. If fail ack then crashes");
					if(global_temp == 9)
						sleep(2);
					else
						usleep(500000);
					if(global_temp == 60 )
					{
						global_temp=0;
						CstcLog_printf("13. If fail ack then crashes");
						return 2;
					}
					else
					{
						global_temp=0;
						CstcLog_printf("13. If fail ack then crashes");
						//				sleep(2); //sleep(1); modified on 14_oct
						return 1;
					}
				}
				else if(global_iTemp > Total_packet_length)
				{
					Show_Error_Msg_UART("Received Packet size is larger than total packet length in the packet");
					CstcLog_printf("Packet size is larger:Before checking dbDataFile ");
					if(dbDataFile != NULL)
					{
						CstcLog_printf("Packet size is larger:After checking dbDataFile ");
						fflush(dbDataFile);
						fclose(dbDataFile);
						dbDataFile = NULL;
						CstcLog_printf("Packet size is larger:After closing dbDataFile ");
					}
					CstcLog_printf("Packet size is larger:For closed dbDataFile  ");

					sprintf(Err_MsgAry,"%02d",GREATER_PACKET_LENGTH);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					ack_flag=1;
					memset(temp_cmd,0,50);
					strcpy(temp_cmd,"rm -f ");
					strcat(temp_cmd,DB_INPUT_FILE_ADDR);
					CstcLog_printf("temp_cmd :%s",temp_cmd);
					system(temp_cmd);
					memset(temp_cmd,0,50);
#if COMPRESSION_DECOMPRESSION
					if(global_buff != NULL)
					{
						free(global_buff);
						global_buff = NULL;
					}
					if(Uncompressed_data != NULL)
					{
						free(Uncompressed_data);
						Uncompressed_data = NULL;
					}
					memset(tempCh,0,sizeof(tempCh));
#else
					memset(global_buff,0,sizeof(global_buff));
#endif
					memset(bTemp,0,sizeof(bTemp));
					memset(g_checksumbuffer,0,sizeof(g_checksumbuffer));
					//					memset(CRC_Data,0,sizeof(CRC_Data));
					//					memset(check_crc,0,sizeof(check_crc));
					//					memset(Calculated_crc,0,sizeof(Calculated_crc));
					sub_string=NULL;//memset(dbTemp,0,sizeof(dbTemp));
					gUpdateDataFlag=0;
					checksumFlag=0;
					global_temp=0;
					Total_packet_length=0;
					Uncompressed_data_length=0;

				}
				else
				{
					CstcLog_printf("default Before checking dbDataFile ");
					if(dbDataFile != NULL)
					{
						CstcLog_printf("default After checking dbDataFile ");
						fflush(dbDataFile);
						fclose(dbDataFile);
						dbDataFile = NULL;
						CstcLog_printf("default After closing dbDataFile ");
					}
					CstcLog_printf("default For closed dbDataFile  ");
					return 0;
				}
				memset(bTemp,0,sizeof(bTemp));
				sub_string=NULL;//memset(dbTemp,0,sizeof(dbTemp));
			}
			else
			{
				Show_Error_Msg_UART("Resend Packets");

				sprintf(Err_MsgAry,"%02d",RESEND_PACKETS);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s ",Err_Len,Err_Msg);
				memset(Err_MsgAry,0,3);

				ack_flag=1;
				memset(temp_cmd,0,50);
				strcpy(temp_cmd,"rm -f ");
				strcat(temp_cmd,DB_INPUT_FILE_ADDR);
				CstcLog_printf("temp_cmd :%s",temp_cmd);
				system(temp_cmd);
				memset(temp_cmd,0,50);
//				memset(global_buff,0,sizeof(global_buff));
				memset(bTemp,0,sizeof(bTemp));
				memset(g_checksumbuffer,0,sizeof(g_checksumbuffer));
				//				memset(CRC_Data,0,sizeof(CRC_Data));
				//				memset(check_crc,0,sizeof(check_crc));
				//				memset(Calculated_crc,0,sizeof(Calculated_crc));
#if COMPRESSION_DECOMPRESSION
					if(global_buff != NULL)
					{
						free(global_buff);
						global_buff = NULL;
					}
					if(Uncompressed_data != NULL)
					{
						free(Uncompressed_data);
						Uncompressed_data = NULL;
					}
					memset(tempCh,0,sizeof(tempCh));
#else
					memset(global_buff,0,sizeof(global_buff));
#endif
				sub_string=NULL;//memset(dbTemp,0,sizeof(dbTemp));
				gUpdateDataFlag=0;
				checksumFlag=0;
				global_temp=0;
				Total_packet_length=0;
				Uncompressed_data_length=0;

			}
		}
	}
	//	else           //open when backend is ready with crc
	//	{
	//		CstcLog_printf("\n......CRC Failed........\n ");
	//		ack_flag = 1;
	//
	//		sprintf(Err_MsgAry,"%02d",CRC_CHECK_FAILED);
	//		strcat(Err_Msg,Err_MsgAry);
	//		Err_Len=Err_Len+2;//strlen(Err_MsgAry);
	//		CstcLog_printf("Err_Len = %d Err_Msg =%s ",Err_Len,Err_Msg);
	//		memset(Err_MsgAry,0,3);
	//	}
	if(ack_flag==1)
	{
		CstcLog_printf(">>------ACK1--------<< %d",(sizeof(ack_packet_struct)+Err_Len+1));
		pkt_ptr = (char*)malloc((sizeof(ack_packet_struct)+Err_Len+1));
		memset(pkt_ptr,0,(sizeof(ack_packet_struct)+Err_Len+1));

		CstcLog_printf(">>------ACK2--------<<");

		UART_compose_ack_packet(pkt_ptr,&length);

		pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length+1));
		memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length+1));

		CstcLog_printf("%d>>------ACK3--------<<%d",length,(sizeof(packet_format_struct)+length+1));
		UART_final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

		CstcLog_printf("Sent bytes are : ");
		//		CstcLog_HexDump_1(pkt_format_ptr,length);  //Swap Enable this after chk for the download crash
		CstcLog_printf_2("\n .......Send...........\n ");
		CstcLog_HexDump_2(pkt_format_ptr,length);
		CstcLog_printf("Sent %d bytes",length);
		CstcLog_printf(">>-----------------<<");

		if(write(iclient_socket, pkt_format_ptr, length) < 0)
		{
			CstcLog_printf("ACK Connection was broken!!!");
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			Err_Len=0;
			return 0;
		}
		else
		{
			CstcLog_printf("ACK Connection was Not broken!!!");
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			CstcLog_printf("1. ACK Connection was Not broken!!!");
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			CstcLog_printf("2. ACK Connection was Not broken!!!");
			memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			Err_Len=0;
			CstcLog_printf("3. ACK Connection was Not broken!!!");
			return 1;
		}
	}
	memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
	memset(global_buff,0,sizeof(global_buff));
	checksumFlag=0;
	return 0;
}

#if COMPRESSION_DECOMPRESSION
int UART_received_packet_lan(int indx, char *tempCh, int iclient_socket,char * bTemp,int updateDataFlag,int *j)
#else
int UART_received_packet_lan(int indx, char tempCh, int iclient_socket,char * bTemp,int updateDataFlag,int *j)
#endif
{
	int length = 0;
	int error=0;
	int ack_flag=0,rowCount=0 , count_header =0 ,count_master = 0, download_rowCount = 0;  //Swap Enable this after chk for the download crash

	int retval = 0;
	char * pkt_ptr = NULL;
	char * pkt_format_ptr  = NULL;
	char temp_dsply[50] = {'\0'};

	char Err_MsgAry[3]={'\0'};
	char *sql=NULL;
	int len = 150;
	time_t t = time(NULL);
	struct tm ptm = *localtime(&t);
	FILE *gprs_upload_file = NULL;

#if CSV_FILE_AUDIT
	int temp = 0;
	char tempstr[200]={'\0'};
#endif
	/*-----------------------------CheckSum------------------------------------*/
	/*------------------------------------------------------------------------*/

	//	char recived_CRC_Data[32]={'\0'};
	//	char recived_CRC[8]={'\0'};
	//	char Calculated_crc[9]={'\0'};

	//	memcpy(recived_CRC_Data,g_checksumbuffer,32);
	//	CstcLog_printf("recived_CRC_Data = %s  %d \n\n",recived_CRC_Data,strlen(recived_CRC_Data));

	//	strcat(recived_CRC_Data,"00000000");
	//	CstcLog_printf(" recived_CRC_Data after concat = %s %d \n\n ",recived_CRC_Data,strlen(recived_CRC_Data));

	//	CstcLog_printf("CRC : %" PRIX32 "\n\n", rc_crc32(0, recived_CRC_Data , strlen(recived_CRC_Data)));

	//	snprintf(Calculated_crc, sizeof Calculated_crc, "%" PRIX32, rc_crc32(0, recived_CRC_Data , strlen(recived_CRC_Data)));
	//	CstcLog_printf("Calculated_crc = %s \n\n",Calculated_crc);

	//	memcpy(recived_CRC,(g_checksumbuffer+32),8);
	//	CstcLog_printf("recived_CRC = %s  %d \n\n",recived_CRC,strlen(recived_CRC));

	//if(strcmp(recived_CRC,Calculated_crc) == 0)     //open when backend is ready with crc
	{
		//CstcLog_printf(".........CRC Check Success..........");
		/*------------------------------------------------------------------------*/
		/*--------------------------------CheckSum---------------------------------*/

#if COMPRESSION_DECOMPRESSION
		CstcLog_printf("tempCh is %s ", tempCh);

		if(strcmp(tempCh,"CTS") == 0)
#else

			CstcLog_printf("tempCh is %c ", tempCh);

			if(tempCh == ';')
#endif
		{
			CstcLog_printf("global_temp/indx %d ", global_temp);
			switch(global_temp)
			{
			case 1:

				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
				if(strlen(global_waybill) < WAYBILL_NO_LEN)
				{
					ack_flag=1;
					CstcLog_printf("INVALID_WAYBILL_NO ");
					sprintf(Err_Msg,"%02d",INVALID_WAYBILL_NO);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					break;
				}
#endif
				if(updateDataFlag==0)//For Normal waybill entry
				{
					if(DbInterface_Get_Row_Count("waybill_master",strlen("waybill_master"),0)>0)
					{
						ack_flag=1;
						CstcLog_printf("WAYBILL MASTER parsed ");
						sprintf(Err_Msg,"%02d",WAYBILL_PRESENT);
						Err_Len=Err_Len+2;//strlen(Err_Msg);
						CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
						Err_Msg[Err_Len]='\0';

						break;
					}
				}

				if(master_status_struct_var.insert_waybill==0)
				{
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(DbInterface_Update_Master_Status_Data("update master_status set insert_waybill ='1'"))
					{
						master_status_struct_var.insert_waybill=1;
						UART_check_master_status();
						//	DbInterface_transaction_commit("COMMIT;");

						pthread_mutex_lock(&upload_semaphore_waybill);
						gprs_upload_file= NULL;
						gprs_upload_file= fopen(WAYBILL_RESPONSE_FILE_ADDR,"w");
						fflush(gprs_upload_file);
						fclose(gprs_upload_file);
						pthread_mutex_unlock(&upload_semaphore_waybill);

					}
					//				else
					//					DbInterface_transaction_commit("ROLLBACK;",0);
				}
				CstcLog_printf("WAYBILL MASTER RECEIVED ");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				CstcLog_printf("WAYBILL MASTER parsed1 ");
				if(retval)
				{
					CstcLog_printf("WAYBILL MASTER parsed2 ");
					ack_flag=1;
					CstcLog_printf("WAYBILL MASTER parsed ");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
					Err_Len=0;
					get_waybill_details();
					//					waybill_details_in_file();
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("WAYBILL MASTER parsing failed");

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",WAYBILL_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg=%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';

					beep(2800,1000);
				}
				break;

			case 2:
				CstcLog_printf("GET SCHEDULE DETAILS RECEIVED ");
				retval =UART_packet_parser_nresponder(bTemp);
				if(retval)
				{
					rowCount=DbInterface_Get_Row_Count("duty_master",strlen("duty_master"),0);
					CstcLog_printf("rowCount = %d ",rowCount);
					if(rowCount == 0)
					{
						pkt_ptr= (char *)malloc((sizeof(duty_master_struct))+1);
						CstcLog_printf("(sizeof(duty_master_struct))+1= %d ",(sizeof(duty_master_struct))+1);
						memset(pkt_ptr,0,(sizeof(duty_master_struct))+1);
					}
					else
					{
						pkt_ptr= (char *)malloc((rowCount*(sizeof(duty_master_struct))+1+rowCount));
						CstcLog_printf("(rowCount*(sizeof(duty_master_struct))+1+rowCount) = %d ",(rowCount*(sizeof(duty_master_struct))+1+rowCount));
						memset(pkt_ptr,0,(rowCount*(sizeof(duty_master_struct))+1+rowCount));
					}

					UART_compose_schedule_response_packet(pkt_ptr,&length);

					pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
					UART_final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

					CstcLog_printf("Sent bytes are : ");
					CstcLog_HexDump_1(pkt_format_ptr,length);

					CstcLog_printf_2("\n .......Send.........\n");
					CstcLog_HexDump_2(pkt_format_ptr,length);

					CstcLog_printf("Sent %d bytes ", length);
					error = write(iclient_socket,pkt_format_ptr, length);
					CstcLog_printf("error %d bytes ", error);
					if (error <= 0)
					{
						CstcLog_printf("SCHEDULE DETAILS Connection was broken !!");

						Show_Error_Msg_UART("   SCHEDULE DETAILS FAILED  ");
						beep(2800,1000);
						if(pkt_ptr!=NULL)
						{
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}
						return 0;
					}
					else
					{
						CstcLog_printf("SCHEDULE DETAILS Connection was Not broken !!");
						if(pkt_ptr!=NULL)
						{
							CstcLog_printf("pkt_ptr_free");
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							CstcLog_printf("pkt_format_ptr_free");
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}
						return 1;
					}
					CstcLog_printf("GET SCHEDULE DETAILS parsed ");
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("GET SCHEDULE DETAILS parsing failed");

					Show_Error_Msg_UART("FETCHING \nSCHEDULE DETAILS FAILED");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SCHEDULE_DETAILS_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg=%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';

					beep(2800,1000);
				}
				break;


			case 3 :
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("SCHEDULE MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER");

				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				CstcLog_printf("case 3 retval = %d ",retval);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("SCHEDULE MASTER parsed ");
					Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER SUCCESS");
					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("SCHEDULE MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER FAILED");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SCHEDULE_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg=%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';

					beep(2800,1000);
				}
				break;
			case 5 :
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("DUTY MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nDUTY MASTER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("DUTY MASTER parsed ");
					Show_Error_Msg_UART("UPLOADING \nDUTY MASTER SUCCESS");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
					if(master_status_struct_var.insert_waybill==1)
					{
						//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
						if(DbInterface_Update_Master_Status_Data("update master_status set insert_waybill ='2'"))
						{
							master_status_struct_var.insert_waybill=2;
							*j=0;
							//	DbInterface_transaction_commit("COMMIT;");
						}
						else
						{ //Swap send failure ack here
							//					DbInterface_transaction_commit("ROLLBACK;",0);
						}
					}
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("DUTY MASTER parsing failed");

					Show_Error_Msg_UART("UPLOADING \nDUTY MASTER FAILED");

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",DUTY_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg=%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';

					beep(2800,1000);
				}

				break;

			case 6 :
				if(master_status_struct_var.start_duty==0)
				{
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(DbInterface_Update_Master_Status_Data("update master_status set start_duty ='1'"))
					{
						master_status_struct_var.start_duty=1;
						//	DbInterface_transaction_commit("COMMIT;");
					}
					//				else
					//					DbInterface_transaction_commit("ROLLBACK;",0);
				}
				UART_check_master_status();
				CstcLog_printf("START DUTY RECEIVED ");
				retval =UART_packet_parser_nresponder(bTemp);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("START DUTY parsed ");
					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(DbInterface_Update_Master_Status_Data("update master_status set start_duty ='2'"))
					{
						// Swap time update needs to be added
						sql=(char*)malloc(len);
						memset(sql,0,len);
						t = time(NULL);
						ptm = *localtime(&t);
						sprintf(sql,"update waybill_master set duty_start_date = \'%d-%d-%d\', duty_start_time = \'%d:%d:%d\'",ptm.tm_mday, ptm.tm_mon+1, ptm.tm_year + 1900, ptm.tm_hour, ptm.tm_min, ptm.tm_sec);
						sql[strlen(sql)]='\0';
						DbInterface_Update_Master_Status_Data(sql);//"update waybill_master set duty_start_date =date('now') and duty_start_time = time('now')");
						master_status_struct_var.start_duty=2;
						*j=0;
						if(sql != NULL)
						{
							free(sql);
							sql = NULL;
						}
						//DbInterface_transaction_commit("COMMIT;");
						//					master_status_struct_var.startDutyFlag=1;
						//						waybill_details_in_file(); comment divya
					}
					else
					{ //Swap send failure ack here
						//					DbInterface_transaction_commit("ROLLBACK;",0);
					}
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("START DUTY parsing failed");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",START_DUTY_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg=%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';

					beep(2800,1000);
				}

				break;
			case 7 :
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				/*	if(master_status_struct_var.insert_route==0)
				{
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(DbInterface_Update_Master_Status_Data("update master_status set insert_route ='1'"))
					{
						master_status_struct_var.insert_route=1;
						//DbInterface_transaction_commit("COMMIT;");
					}
					//				else
					//					DbInterface_transaction_commit("ROLLBACK;",0);
				}*/ //for CSTC route is static
				UART_check_master_status();
				CstcLog_printf("ROUTE MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nROUTE MASTER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("ROUTE MASTER parsed ");
					Show_Error_Msg_UART("UPLOADING \nROUTE MASTER SUCCESS");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("ROUTE MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nROUTE MASTER FAILED");

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ROUTE_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg=%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';

					beep(2800,1000);
				}

				break;

			case 8 :
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("FARE CHART RECORD COUNT Recvd updateDataFlag = %d",updateDataFlag);
				//			if(updateDataFlag==0)
				//			{
				//sudhakar commented
									/*
				if(master_status_struct_var.ticket_fare == 0)
				{
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='1'"))
					{
						master_status_struct_var.ticket_fare=1;
						//DbInterface_transaction_commit("COMMIT;");
					}
					//					else
					//						DbInterface_transaction_commit("ROLLBACK;",0);
					CstcLog_printf("1. FARE CHART RECORD COUNT updateDataFlag = %d",updateDataFlag);
					UART_check_master_status();
				}
				//			}*/
				if(updateDataFlag==2)
				{
					/*if(master_status_struct_var.ticket_fare ==1)
					{
						//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

						//sudhakar Cmmt
						if(DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'"))
						{
							master_status_struct_var.ticket_fare=2;
							*j=0;
							//						DbInterface_transaction_commit("COMMIT;",0);
						}
						else
						{ //Swap send failure ack here
							//					DbInterface_transaction_commit("ROLLBACK;",0);
						}
					}
					CstcLog_printf("2. FARE CHART RECORD COUNT updateDataFlag = %d",updateDataFlag);*/
				}
				CstcLog_printf("FARE CHART RECORD COUNT RECEIVED ");

				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("FARE CHART RECORD COUNT parsed ");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));

					Err_Len=0;
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("FARE CHART RECORD COUNT parsing failed");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",FARE_CHART_COUNT_FAILED);

					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg=%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';

					beep(2800,1000);
				}
				break;
			case 9 :
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("FARE CHART RECEIVED ");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					CstcLog_printf("FARE MASTER RECEIVED ");
					Show_Error_Msg_UART("UPLOADING \nFARE MASTER");

					ack_flag=1;
					TotalRecordCntRecvd +=rt_count;
					CstcLog_printf("rt_count = %d",rt_count);
					CstcLog_printf("FARE CHART parsed TotalRecordCntRecvd =%d and RecordCntToRecv =%d",TotalRecordCntRecvd,RecordCntToRecv);

					if(TotalRecordCntRecvd>=RecordCntToRecv)
					{
						CstcLog_printf("FARE CHART parsed 1.swap");
						sprintf(Err_Msg,"%02d",SUCCESS);
						Err_Len=Err_Len+2;//strlen(Err_Msg);
						CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
						Err_Msg[Err_Len]='\0';
						memset(Err_Msg,0,sizeof(Err_Msg));

						Err_Len=0;
						rt_count=0;
					}
					else
					{
						//					strcat(Err_Msg,"FARE CHART Partial Success");
						//					Err_Len = Err_Len + strlen("FARE CHART Partial Success");
						//					CstcLog_printf("Err_Len = %d",Err_Len);
						CstcLog_printf("FARE MASTER RECEIVED ");
						Show_Error_Msg_UART("UPLOADING \nFARE MASTER");

						CstcLog_printf("FARE CHART parsed 2.swap");
						CstcLog_printf("Err_Len = %d ",Err_Len);
						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",FARE_CHART_PARTIAL_SUCCESS);
						CstcLog_printf("Err_Len = %d Err_Msg =%s ",Err_Len,Err_Msg);
						CstcLog_printf("Err_MsgAry_Len = %d Err_MsgAry =%s ",strlen(Err_MsgAry),Err_MsgAry);

						strcat(Err_Msg,Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s ",Err_Len,Err_Msg);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s ",Err_Len,Err_Msg);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);
						Err_Msg[Err_Len]='\0';
						rt_count=0;
						//beep(2800,1000);
					}

				}
				else
				{
					CstcLog_printf("FARE MASTER RECEIVED ");
					Show_Error_Msg_UART("UPLOADING \nFARE MASTER FAILED");

					ack_flag=1;

					//				strcat(Err_Msg,"FARE CHART failed");
					//				Err_Len = Err_Len + strlen("FARE CHART failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",FARE_CHART_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}

				break;
			case 10 :
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			/*	if(master_status_struct_var.upload_master==0)   //sudhakar commented
				{
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='1'"))
					{
						master_status_struct_var.upload_master=1;
						//DbInterface_transaction_commit("COMMIT;");
					}
					//				else
					//					DbInterface_transaction_commit("ROLLBACK;",0);
				}*/

				CstcLog_printf("TICKET TYPE MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nTICKET TYPE MASTER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("TICKET TYPE MASTER parsed ");
					Show_Error_Msg_UART("UPLOADING \nTICKET TYPE MASTER SUCCESS");
					//				strcat(Err_Msg,"TICKET TYPE MASTER Success");
					//				Err_Len = Err_Len + strlen("TICKET TYPE MASTER Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("TICKET TYPE MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nTICKET TYPE MASTER FAILED");
					//				strcat(Err_Msg,"TICKET TYPE MASTER failed");
					//				Err_Len = Err_Len + strlen("TICKET TYPE MASTER failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TICKET_TYPE_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}

				//UART_check_master_status();
				break;
			case 11:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("TICKET SUB TYPE MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nTICKET SUB TYPE MASTER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("TICKET SUB TYPE parsed ");

					Show_Error_Msg_UART("UPLOADING \nTICKET SUB TYPE MASTER SUCCESS");
					//				strcat(Err_Msg,"TICKET SUB TYPE MASTER Success");
					//				Err_Len = Err_Len + strlen("TICKET SUB TYPE MASTER Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg); //need modification for the length
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("TICKET SUB TYPE MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nTICKET SUB TYPE MASTER FAILED");
					//				strcat(Err_Msg,"TICKET SUB TYPE MASTER failed");
					//				Err_Len = Err_Len + strlen("TICKET SUB TYPE MASTER failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TICKET_SUB_TYPE_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				//UART_check_master_status();
				break;
			case 12:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("BUS SERVICE MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nBUS SERVICE MASTER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("BUS SERVICE MASTER parsed ");
					Show_Error_Msg_UART("UPLOADING \nBUS SERVICE MASTER SUCCESS");
					//				strcat(Err_Msg,"BUS SERVICE MASTER Success");
					//				Err_Len = Err_Len + strlen("BUS SERVICE MASTER Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("BUS SERVICE MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nBUS SERVICE MASTER FAILED");
					//				strcat(Err_Msg,"BUS SERVICE MASTER failed");
					//				Err_Len = Err_Len + strlen("BUS SERVICE MASTER failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",BUS_SERVICE_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				//UART_check_master_status();
				break;
			case 13:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("BUS BRAND MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nBUS BRAND MASTER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("BUS BRAND MASTER parsed ");
					Show_Error_Msg_UART("UPLOADING \nBUS BRAND MASTER SUCCESS");
					//				strcat(Err_Msg,"BUS BRAND MASTER Success");
					//				Err_Len = Err_Len + strlen("BUS BRAND MASTER Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("BUS BRAND MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nBUS BRAND MASTER FAILED");
					//				strcat(Err_Msg,"BUS BRAND MASTER failed");
					//				Err_Len = Err_Len + strlen("BUS BRAND MASTER failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",BUS_BRAND_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				//UART_check_master_status();
				break;
			case 14:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("BUS DEPOT MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nBUS DEPOT MASTER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("BUS DEPOT MASTER parsed ");
					Show_Error_Msg_UART("UPLOADING \nBUS DEPOT MASTER SUCCESS");
					//				strcat(Err_Msg,"BUS DEPOT MASTER Success");
					//				Err_Len = Err_Len + strlen("BUS DEPOT MASTER Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("BUS DEPOT MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nBUS DEPOT MASTER FAILED");
					//				strcat(Err_Msg,"BUS DEPOT MASTER failed");
					//				Err_Len = Err_Len + strlen("BUS DEPOT MASTER failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",DEPOT_MASTER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				//UART_check_master_status();
				break;
			case 15:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("Route Header RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nROUTE HEADER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("Route Header parsed ");
					Show_Error_Msg_UART("UPLOADING \nROUTE HEADER SUCCESS");
					//				strcat(Err_Msg,"Route Header Success");
					//				Err_Len = Err_Len + strlen("Route Header Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("Route Header parsing failed");
					Show_Error_Msg_UART("UPLOADING \nROUTE HEADER FAILED");
					//				strcat(Err_Msg,"Route Header failed");
					//				Err_Len = Err_Len + strlen("Route Header failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ROUTE_HEADER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				//UART_check_master_status();
				break;

			case 16:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				//			UART_check_master_status();
				CstcLog_printf("ETM READ WRITE KEY RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nETM READ WRITE KEY");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("ETM READ WRITE KEY parsed ");
					Show_Error_Msg_UART("UPLOADING \nETM READ WRITE KEY SUCCESS");
					//				strcat(Err_Msg,"ETM READ WRITE KEY Success");
					//				Err_Len = Err_Len + strlen("ETM READ WRITE KEY Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

					// sudhakar Commented
					/*if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
					{
						master_status_struct_var.upload_master=2;
						*j=0;
						//	DbInterface_transaction_commit("COMMIT;");
					}
					else
					{ //Swap send failure ack here
						//					DbInterface_transaction_commit("ROLLBACK;",0);
					}*/
					//				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
					//					master_status_struct_var.upload_master=2;
					//UART_check_master_status();
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("ETM READ WRITE KEY parsing failed");
					Show_Error_Msg_UART("UPLOADING \nETM READ WRITE KEY FAILED");
					//				strcat(Err_Msg,"ETM READ WRITE KEY failed");
					//				Err_Len = Err_Len + strlen("ETM READ WRITE KEY failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ETM_READ_WRITE_KEY_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				//			UART_check_master_status();
				//			return 1;
				break;


			case 17:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("TRUNK AND FEEDER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nTRUNK AND FEEDER");
				retval =UART_packet_parser_input_master(tempCh,global_temp, updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("TRUNK AND FEEDER parsed ");
					Show_Error_Msg_UART("UPLOADING \nTRUNK AND FEEDER SUCCESS");
					//				strcat(Err_Msg,"TRUNK AND FEEDER Success");
					//				Err_Len = Err_Len + strlen("TRUNK AND FEEDER Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("TRUNK AND FEEDER failed");
					Show_Error_Msg_UART("UPLOADING \nTRUNK AND FEEDER FAILED");
					//				strcat(Err_Msg,"TRUNK AND FEEDER failed");
					//				Err_Len = Err_Len + strlen("TRUNK AND FEEDER failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TRUNK_FEEDER_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';

					beep(2800,1000);
				}
				//UART_check_master_status();
				break;


			case 18:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("SERVICE_OP RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nSERVICE_OP");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("SERVICE_OP parsed ");
					Show_Error_Msg_UART("UPLOADING \nSERVICE_OP SUCCESS");
					//				strcat(Err_Msg,"SERVICE_OP Success");
					//				Err_Len = Err_Len + strlen("SERVICE_OP Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

					//				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
					//					master_status_struct_var.upload_master=2;
					//UART_check_master_status();
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("SERVICE_OP parsing failed");
					Show_Error_Msg_UART("UPLOADING \nSERVICE_OP FAILED");
					//				strcat(Err_Msg,"SERVICE_OP failed");
					//				Err_Len = Err_Len + strlen("SERVICE_OP failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					sprintf(Err_Msg,"%02d",SERVICE_OP_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case 19:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("RATE MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nRATE MASTER ");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("RATE MASTER  parsed ");
					Show_Error_Msg_UART("UPLOADING \nRATE MASTER SUCCESS");
					//				strcat(Err_Msg,"SERVICE_OP Success");
					//				Err_Len = Err_Len + strlen("SERVICE_OP Success");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

					/*if(DbInterface_Update_Master_Status_Data("update master_status set insert_route ='2'"))
					{
						master_status_struct_var.insert_route=2;
					 *j=0;
					}
					else
					{ //Swap send failure ack here
						//					DbInterface_transaction_commit("ROLLBACK;",0);
					}*/ // For CSTC route is static

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("RATE MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nRATE MASTER FAILED");
					//				strcat(Err_Msg,"SERVICE_OP failed");
					//				Err_Len = Err_Len + strlen("SERVICE_OP failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					sprintf(Err_Msg,"%02d",RATE_MASTER_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case 22:
				CstcLog_printf("CHECK ROUTE_ID ");

				count_header = get_route_id_from_route_header(global_Route_Id);
				count_master = get_route_id_from_route_master(global_Route_Id);

				CstcLog_printf("count_header = %d count_master =  %d ",count_header ,count_master);

				if((count_header > 0 ) && (count_master > 0))
				{
					ack_flag=1;
					CstcLog_printf("ROUTE_ID present ");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("ROUTE_ID_ABSENT");

					sprintf(Err_Msg,"%02d",ROUTE_ID_ABSENT);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);

				}
				break;

			case 23:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("LC_LOGIN RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nLC_LOGIN");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("LC_LOGIN parsed ");
					Show_Error_Msg_UART("UPLOADING \nLC_LOGIN SUCCESS");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("LC_LOGIN parsing failed");
					Show_Error_Msg_UART("UPLOADING \nLC_LOGIN FAILED");

					sprintf(Err_Msg,"%02d",LC_LOGIN_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;


			case 24:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("GPRS_APN RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nGPRS_TABLE");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("GPRS_TABLE parsed ");
					Show_Error_Msg_UART("UPLOADING \nGPRS_TABLE SUCCESS");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("nGPRS_TABLE parsing failed");
					Show_Error_Msg_UART("UPLOADING \nGPRS_TABLE FAILED");

					sprintf(Err_Msg,"%02d",GPRS_TABLE_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case 25:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("ROUTE_CARD RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nROUTE_CARD");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("ROUTE_CARD parsed ");
					Show_Error_Msg_UART("UPLOADING \nROUTE_CARD SUCCESS");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("nROUTE_CARD parsing failed");
					Show_Error_Msg_UART("UPLOADING \nROUTE_CARD FAILED");

					sprintf(Err_Msg,"%02d",ROUTE_CARD_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case 26:

#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("SHIFT_TYPE RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nSHIFT_TYPE");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("SHIFT_TYPE parsed ");
					Show_Error_Msg_UART("UPLOADING \nSHIFT_TYPE SUCCESS");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("nSHIFT_TYPE parsing failed");
					Show_Error_Msg_UART("UPLOADING \nSHIFT_TYPE FAILED");

					sprintf(Err_Msg,"%02d",SHIFT_TYPE_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case 27:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("CONCESSION_TYPE RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nCONCESSION_TYPE");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("CONCESSION_TYPE parsed ");
					Show_Error_Msg_UART("UPLOADING \nCONCESSION_TYPE SUCCESS");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("CONCESSION_TYPE parsing failed");
					Show_Error_Msg_UART("UPLOADING \nCONCESSION_TYPE FAILED");

					sprintf(Err_Msg,"%02d",CONCESSION_TYPE_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case 28:
#if !COMPRESSION_DECOMPRESSION
				memset(global_buff,0,sizeof(global_buff));
#endif
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("ACTIVITY_MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nACTIVITY_MASTER");
				retval =UART_packet_parser_input_master(tempCh,global_temp,updateDataFlag);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("ACTIVITY_MASTER parsed ");
					Show_Error_Msg_UART("UPLOADING \nACTIVITY_MASTER SUCCESS");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("ACTIVITY_MASTER parsing failed");
					Show_Error_Msg_UART("UPLOADING \nACTIVITY_MASTER FAILED");

					sprintf(Err_Msg,"%02d",ACTIVITY_MASTER_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case 45:
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
				prev_end_tkt_count =0;
				if(waybill_master_struct_var.collection_status == 'D')
				{
					retval =UART_packet_parser_nresponder(bTemp);
					if(retval)
					{
						pkt_ptr= (char *)malloc((sizeof(download_response_struct)+length));
						memset(pkt_ptr,0,(sizeof(download_response_struct)+length));
						UART_compose_download_response_packet(pkt_ptr,&length);
						CstcLog_printf("*****************************************");
						memset(temp_dsply,0,50);
						sprintf(temp_dsply,"UPLOADING DATA\nRECORDS = %d/%d",(GDownload_pktCnt * DOWNLOAD_TICKET_RANGE),Global_rowCount);
						Show_Error_Msg_UART(temp_dsply);
						CstcLog_printf("Sending Download Response Packet");
						//				length += 26 ;
						CstcLog_printf("pkt_ptr bytes are : ");
						CstcLog_HexDump(pkt_ptr,length);
						if(master_status_struct_var.download_ticket==0)
						{
							//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
							if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='1'"))
							{
								master_status_struct_var.download_ticket=1;
								//DbInterface_transaction_commit("COMMIT;");
							}
							//					else
							//						DbInterface_transaction_commit("ROLLBACK;",0);
						}
						UART_check_master_status();
						//	DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
						if(DbInterface_Update_Master_Status_Data("update waybill_master set collection_status = 'D'"))
						{
							//						get_waybill_details();
							CstcLog_printf("waybill_master shift_status ==> %c ",waybill_master_struct_var.shift_status);
							//DbInterface_transaction_commit("COMMIT;");
							//						check_collection_status();
						}
						//				else
						//					DbInterface_transaction_commit("ROLLBACK;",0);

						//					if(download_response_struct_var.start_tkt_cnt==0)
						//					{
						//						if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='2'"))
						//							UART_check_master_status();
						//					}
						pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
						memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
						UART_final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

						CstcLog_printf("Sent bytes are : ");
						//						CstcLog_HexDump(pkt_format_ptr,length);  //Swap Enable this after chk for the download crash
						CstcLog_printf_2("\n .......Send...........\n ");
						CstcLog_HexDump_2(pkt_format_ptr,length);

						CstcLog_printf("Sent %d bytes ", length);
						error = write(iclient_socket,pkt_format_ptr, length);
						CstcLog_printf("error %d bytes ", error);
						if (error <= 0)
						{
							CstcLog_printf("Download Response Packet 46 Connection was broken !!");
							//					strcat(Err_Msg,"Download Response Packet 46 Connection was broken !!");
							//					Err_Len = Err_Len + strlen("Download Response Packet 46 Connection was broken !!");
							//					CstcLog_printf("Err_Len = %d",Err_Len);

							sprintf(Err_Msg,"%02d",CONNECTION_WAS_BROKEN);
							Err_Len=Err_Len+2;//strlen(Err_Msg);
							CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
							Err_Msg[Err_Len]='\0';
							//					memset(pkt_ptr,0,(sizeof(download_response_struct)+1));
							//					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
							Show_Error_Msg_UART("DOWNLOADING FAILED");
							beep(2800,1000);
							if(pkt_ptr!=NULL)
							{
								free(pkt_ptr);
								pkt_ptr=NULL;
							}
							if(pkt_format_ptr!=NULL)
							{
								free(pkt_format_ptr);
								pkt_format_ptr=NULL;
							}
							return 0;
						}
						else
						{
							CstcLog_printf("Download Response Packet 46 Connection was Not broken !!");
							//					strcat(Err_Msg,"Download Response Packet 46 Connection was Not broken !!");
							//					Err_Len = Err_Len + strlen("Download Response Packet 46 Connection was Not broken !!");
							CstcLog_printf("Err_Len = %d",Err_Len);

							//					memset(pkt_ptr,0,(sizeof(download_response_struct)+1));
							//					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
							if(pkt_ptr!=NULL)
							{
								free(pkt_ptr);
								pkt_ptr=NULL;
							}
							if(pkt_format_ptr!=NULL)
							{
								free(pkt_format_ptr);
								pkt_format_ptr=NULL;
							}
							return 1;
						}
					}
					else
					{
						ack_flag=1;
						CstcLog_printf("Download Request failed");
						Show_Error_Msg_UART("DOWNLOADING FAILED");
						//				strcat(Err_Msg,"Download Request failed");
						//				Err_Len = Err_Len + strlen("Download Request failed");
						//				CstcLog_printf("Err_Len = %d",Err_Len);

						sprintf(Err_Msg,"%02d",DOWNLOAD_REQUEST_FAILED);
						Err_Len=Err_Len+2;//strlen(Err_Msg);
						CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);

						beep(2800,1000);
					}
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("DUTY_NOT_COMPLETED");
					Show_Error_Msg_UART("DUTY NOT CLOSED ");

					sprintf(Err_Msg,"%02d",DUTY_NOT_CLOSED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);

					beep(2800,1000);
				}
				break;

			case 47:
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("---------After DB_DATA_FILE_ENABLE---------------");
				retval =UART_packet_parser_nresponder(bTemp);
				CstcLog_printf("retval = %d",retval);
				if(retval)
				{
#if CSV_FILE_AUDIT
					CstcLog_printf("Retval is 1 ..........");
					memset(tempstr,0,200);
					sprintf(tempstr,"sqlite3 -csv %s \"select * from transaction_log;\">\"%s\";",DB_ADDR,TRANSACTION_CSV_FILE_ADDR);
					temp=system(tempstr);
					//system("sqlite3 -csv cstc_new.db \"select * from transaction_log;\">test.csv");

					CstcLog_printf("system command fired as  %s ...........%d..........",tempstr,temp);
					while(!system(NULL)){
						CstcLog_printf("Shell is unavailable");
						sleep(2);
					}
					CstcLog_printf("system command sleeping ..........");
					sleep(1);
					if(temp!=0)
					{
						ack_flag=1;
						CstcLog_printf("Download Request failed");
						Show_Error_Msg_UART("DOWNLOADING FAILED");

						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",DOWNLOAD_REQUEST_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);
						Err_Msg[Err_Len]='\0';
						beep(2800,1000);
						prev_end_tkt_count=0;
						break;
					}
					length = (int)get_file_size (TRANSACTION_CSV_FILE_ADDR);
					CstcLog_printf("file_size= %d",length);
					pkt_ptr= (char *)read_whole_file(TRANSACTION_CSV_FILE_ADDR); //(char *)malloc(length*sizeof(char));
#else
					if(Global_rowCount>DOWNLOAD_TICKET_RANGE)  //Swap Enable this after chk for the download crash
					{  //Swap Enable this after chk for the download crash
						pkt_ptr= (char *)malloc(((DOWNLOAD_TICKET_RANGE+1)*sizeof(transaction_log_struct))+1+DOWNLOAD_TICKET_RANGE);
						CstcLog_printf("(DOWNLOAD_TICKET_RANGE*sizeof(transaction_log_struct))+1+DOWNLOAD_TICKET_RANGE = %d",((DOWNLOAD_TICKET_RANGE+1)*sizeof(transaction_log_struct))+1+DOWNLOAD_TICKET_RANGE);
						memset(pkt_ptr,0,(((DOWNLOAD_TICKET_RANGE+1)*sizeof(transaction_log_struct))+1+DOWNLOAD_TICKET_RANGE));
					}  //Swap Enable this after chk for the download crash
					else  //Swap Enable this after chk for the download crash
					{  //Swap Enable this after chk for the download crash

						download_rowCount = Global_rowCount;  //Swap Enable this after chk for the download crash
						pkt_ptr= (char *)malloc(((download_rowCount+1)*sizeof(transaction_log_struct))+1+download_rowCount);  //Swap Enable this after chk for the download crash
						CstcLog_printf("(download_rowCount*sizeof(transaction_log_struct))+1+download_rowCount = %d",((download_rowCount+1)*sizeof(transaction_log_struct))+1+download_rowCount);  //Swap Enable this after chk for the download crash
						memset(pkt_ptr,0,(((download_rowCount+1)*sizeof(transaction_log_struct))+1+download_rowCount));  //Swap Enable this after chk for the download crash
					}  //Swap Enable this after chk for the download crash
					if(0==UART_compose_transaction_data_packets(pkt_ptr,&length))
						length=0;
#endif
					if(length==0) //Download crash handling
					{
						ack_flag=1;
						CstcLog_printf("Download Request failed");
						Show_Error_Msg_UART("DOWNLOADING FAILED");

						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",DOWNLOAD_REQUEST_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);
						Err_Msg[Err_Len]='\0';
						beep(2800,1000);
						prev_end_tkt_count=0;
						break;
					}
					CstcLog_printf("*****************************************");
					memset(temp_dsply,0,50);
					if(download_rowCount>DOWNLOAD_TICKET_RANGE)  //Swap Enable this after chk for the download crash
						sprintf(temp_dsply,"UPLOADING DATA\nRECORDS = %d/%d",(GDownload_pktCnt * DOWNLOAD_TICKET_RANGE),(Global_rowCount+(GDownload_pktCnt * DOWNLOAD_TICKET_RANGE)));
					else  //Swap Enable this after chk for the download crash
						sprintf(temp_dsply,"UPLOADING DATA\nRECORDS = %d/%d",download_rowCount,download_rowCount);  //Swap Enable this after chk for the download crash
					Show_Error_Msg_UART(temp_dsply);
					//					Show_Error_Msg_UART("UPLOADING DATA\nRECORDS = %d/%d",(GDownload_pktCnt * DOWNLOAD_TICKET_RANGE),(Global_rowCount+(GDownload_pktCnt * DOWNLOAD_TICKET_RANGE)));
					CstcLog_printf("Sending Download Response Packet");
					//				length += 26 ;
					CstcLog_printf("pkt_ptr bytes are : ");
					//					CstcLog_HexDump(pkt_ptr,length);  //Swap Enable this after chk for the download crash
					//					if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='1'"))
					//						UART_check_master_status();
					//				if(strlen(download_response_struct_var.terminal_id)> 0)
					//					if(strcmp(download_response_struct_var.terminal_id,"00000000")== 0)
					//					{
					//						DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='2'");
					//					}
					//UART_check_master_status();
					if(strcmp(download_response_struct_var.start_tkt_cnt,"00000") == 0)
					{
						CstcLog_printf("inside strcmp");
						//	DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
						if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='2'"))
						{
							master_status_struct_var.download_ticket=2;//UART_check_master_status();
							*j=0;
							//	DbInterface_transaction_commit("COMMIT;");
						}
						else
						{
							//Swap Add fail condition here and send fail ack
							//						DbInterface_transaction_commit("ROLLBACK;",0);
						}
						CstcLog_printf("after update download_ticket 2");
						ack_flag=1;
						CstcLog_printf("Download Completed Successfully");
						length =0;
						sprintf(Err_Msg,"%02d",SUCCESS);
						Err_Len=Err_Len+2;//strlen(Err_Msg);
						CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
						Err_Msg[Err_Len]='\0';
						memset(Err_Msg,0,sizeof(Err_Msg));
						Err_Len=0;

						//					strcat(Err_Msg,"Download Completed Successfully");
						//					Err_Len = Err_Len + strlen("Download Completed Successfully");
						//					CstcLog_printf("Err_Len = %d",Err_Len);
						GDownload_pktCnt=0;
						prev_end_tkt_count=0;

						if(pkt_ptr!=NULL)
						{
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}

					}
					else
					{
						//						if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='1'"))
						//							UART_check_master_status();
						//					}

						pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
						memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
						UART_final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

						CstcLog_printf("Sent bytes are : ");
						CstcLog_printf_2("\n .......Send..........\n ");

						CstcLog_HexDump_2(pkt_format_ptr,length);

						//						CstcLog_HexDump(pkt_format_ptr,length);  //Swap Enable this after chk for the download crash
						//				for(j=0 ; j< length ; j++)
						//					CstcLog_printf("%2x, ", pkt_format_ptr[j]);
						CstcLog_printf("Sent %d bytes ", length);
						error = write(iclient_socket,pkt_format_ptr, length);
						CstcLog_printf("error %d bytes ", error);
						if (error <= 0)
						{
							CstcLog_printf("Download Response Packet 47 Connection was broken !!");
							//memset(Err_Msg,0,sizeof(Err_Msg));
							//memcpy(Err_Msg,"Download Response Packet 47 Connection was broken !!",52);
							//						strcat(Err_Msg,"Download Response Packet 47 Connection was broken !!");
							//						Err_Len = Err_Len + strlen("Download Response Packet 47 Connection was broken !!");
							//						CstcLog_printf("Err_Len = %d",Err_Len);
							//					memset(pkt_ptr,0,(sizeof(transaction_log_struct)+1));
							//					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
							Show_Error_Msg_UART("DOWNLOADING FAILED");
							beep(2800,1000);
							if(pkt_ptr!=NULL)
							{
								free(pkt_ptr);
								pkt_ptr=NULL;
							}
							if(pkt_format_ptr!=NULL)
							{
								free(pkt_format_ptr);
								pkt_format_ptr=NULL;
							}
							prev_end_tkt_count=0;
							return 0;
						}
						else
						{
							GDownload_pktCnt++;
							CstcLog_printf("Download Response Packet 47 Connection was Not broken !!");

							//						strcat(Err_Msg,"Download Response Packet 47 Connection was Not broken !!");
							//						Err_Len = Err_Len + strlen("Download Response Packet 47 Connection was Not broken !!");
							//						CstcLog_printf("Err_Len = %d",Err_Len);
							//					memset(pkt_ptr,0,(sizeof(transaction_log_struct)+1));
							//					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
							if(pkt_ptr!=NULL)
							{
								free(pkt_ptr);
								pkt_ptr=NULL;
							}
							if(pkt_format_ptr!=NULL)
							{
								free(pkt_format_ptr);
								pkt_format_ptr=NULL;
							}
							prev_end_tkt_count =0;
							return 1;
						}
					}
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("Download Request failed");
					Show_Error_Msg_UART("DOWNLOADING FAILED");
					//				strcat(Err_Msg,"Download Request failed");
					//				Err_Len = Err_Len + strlen("Download Request failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",DOWNLOAD_REQUEST_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case ERASE_DATA_PKT_ID:
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("ERASE PACKET RECEIVED");
				if(master_status_struct_var.erase_ticket==0)
				{
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(DbInterface_Update_Master_Status_Data("update master_status set erase_ticket ='1'"))
					{
						master_status_struct_var.erase_ticket=1;
						//DbInterface_transaction_commit("COMMIT;");
					}
					//				else
					//					DbInterface_transaction_commit("ROLLBACK;",0);
				}
				UART_check_master_status();
				retval =UART_packet_parser_nresponder(bTemp);
				if(retval)
				{
					ack_flag=1;
					//					if(strlen(download_response_struct_var.terminal_id)> 0)
					//						if(strcmp(download_response_struct_var.terminal_id,"00000000")!= 0)
					//						{
					DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(backup_to_transaction_restore()) // redbug : transaction_log_restore
					{
						ack_flag=1;
						CstcLog_printf("Erase Request failed");
						Show_Error_Msg_UART("ERASE FAILED");
						//					strcat(Err_Msg,"Erase Request failed");
						//					Err_Len = Err_Len + strlen("Erase Request failed");
						//					CstcLog_printf("Err_Len = %d",Err_Len);
						DbInterface_transaction_commit("ROLLBACK;",0);

						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",ERASE_DATA_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);
						Err_Msg[Err_Len]='\0';
						beep(2800,1000);
						break;

					}
					if(!DbInterface_Delete_From_All_Tables(updateDataFlag,global_schedule))
					{
						ack_flag=1;
						CstcLog_printf("Erase Request failed");
						Show_Error_Msg_UART("ERASE FAILED");
						//					strcat(Err_Msg,"Erase Request failed");
						//					Err_Len = Err_Len + strlen("Erase Request failed");
						//					CstcLog_printf("Err_Len = %d",Err_Len);
						DbInterface_transaction_commit("ROLLBACK;",0);

						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",ERASE_DATA_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);
						Err_Msg[Err_Len]='\0';
						beep(2800,1000);
						break;
					}

					if(!DbInterface_Update_Master_Status_Data("update master_status set erase_ticket ='2'"))
					{
						ack_flag=1;
						CstcLog_printf("Erase Request failed");
						Show_Error_Msg_UART("ERASE FAILED");
						DbInterface_transaction_commit("ROLLBACK;",0);
						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",ERASE_DATA_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);
						Err_Msg[Err_Len]='\0';
						beep(2800,1000);
						break;
					}
					//					else
					//					{
					master_status_struct_var.erase_ticket=2;
					*j=0;
					DbInterface_transaction_commit("COMMIT;",0);
					//					}
					//				UART_check_master_status();
					//				get_waybill_details();
					//				DbInterface_Delete_Table("Delete from master_status");
					//				DbInterface_Insert_In_All_Tables1("master_status","'0','0','2','0','0','0','0'",27);
					//memset(Err_Msg,0,sizeof(Err_Msg));
					//memcpy(Err_Msg,"Data Deleted",12);
					//				strcat(Err_Msg,"Data Deleted");
					//				Err_Len = Err_Len + strlen("Data Deleted");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len = Err_Len + 2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

					//						}
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("Erase Request failed");
					Show_Error_Msg_UART("ERASE FAILED");
					//				strcat(Err_Msg,"Erase Request failed");
					//				Err_Len = Err_Len + strlen("Erase Request failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERASE_DATA_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
				break;

			case 54 :
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				CstcLog_printf("******************case 54***********************");
				if(master_status_struct_var.machine_register==0)
				{
					//	DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(DbInterface_Update_Master_Status_Data("update master_status set machine_register ='1'"))
					{
						master_status_struct_var.machine_register=1;
						UART_check_master_status();
		//		DbInterface_transaction_commit("COMMIT;",0);
					}
					//				else
					//					DbInterface_transaction_commit("ROLLBACK;",0);
				}
				CstcLog_printf("======case 54======");
				retval =UART_packet_parser_nresponder(bTemp);
				if(retval)
				{
					pkt_ptr= (char *)malloc(sizeof(registration_response_struct));
					memset(pkt_ptr,0,sizeof(registration_response_struct));
					UART_compose_registration_response_packet(pkt_ptr,&length);
					CstcLog_printf("*****************************************");
					CstcLog_printf("Sending Registration Response Packet");

					if(strlen(registration_response_struct_var.terminal_id)> 0)
						if(strcmp(registration_response_struct_var.terminal_id,"00000000")!= 0)
						{
							if(master_status_struct_var.machine_register!=2)
							{
								//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
								if(DbInterface_Update_Master_Status_Data("update master_status set machine_register ='2'"))
								{
									master_status_struct_var.machine_register=2;
									*j=0;
									//DbInterface_transaction_commit("COMMIT;");
								}
								//							else
								//								DbInterface_transaction_commit("ROLLBACK;",0);
							}
						}

					pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
					UART_final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

					CstcLog_printf("Sent bytes are : ");
					CstcLog_printf_2("\n .......Send.........\n");
					CstcLog_HexDump(pkt_format_ptr,length);
					//				for(j=0 ; j< length ; j++)
					//					CstcLog_printf("%2x, ", pkt_format_ptr[j]);
					CstcLog_printf("Sent %d bytes ", length);
					error = write(iclient_socket,pkt_format_ptr, length);

					if (error <= 0)
					{
						CstcLog_printf("Registration Response Packet 55 Connection was broken !!");

						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",CONNECTION_WAS_BROKEN);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);
						Err_Msg[Err_Len]='\0';
						//					strcat(Err_Msg,"Registration Response Packet 55 Connection was broken !!");
						//					Err_Len = Err_Len + strlen("Registration Response Packet 55 Connection was broken !!");
						//					CstcLog_printf("Err_Len = %d",Err_Len);
						Show_Error_Msg_UART("REGISTRATION CHECK FAILED");
						beep(2800,1000);
						if(pkt_ptr!=NULL)
						{
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}
						return 0;
					}
					else
					{
						CstcLog_printf("Registration Response Packet 55 Connection was Not broken !!");

						//					strcat(Err_Msg,"Registration Response Packet 55 Connection was Not broken !!");
						//					Err_Len = Err_Len + strlen("Registration Response Packet 55 Connection was Not broken !!");
						//					CstcLog_printf("Err_Len = %d",Err_Len);
						if(pkt_ptr!=NULL)
						{
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}
						return 1;
					}
				}
				else
				{
					ack_flag=1;
					CstcLog_printf("ETM Request failed");
					//				strcat(Err_Msg,"ETM Request failed");
					//				Err_Len = Err_Len + strlen("ETM Request failed");
					//				CstcLog_printf("Err_Len = %d",Err_Len);
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",REGISTRATION_REQUEST_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					Err_Msg[Err_Len]='\0';
					Show_Error_Msg_UART("ETM REGISTRATION CHECK FAILED");
					beep(2800,1000);
				}
				UART_check_master_status();
				break;
			case 56 :
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);

				{
					CstcLog_printf("Machine Registration RECEIVED ");
					if(master_status_struct_var.machine_register==0)
					{
						//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
						if(DbInterface_Update_Master_Status_Data("update master_status set machine_register ='1'"))
						{
							master_status_struct_var.machine_register=1;
							UART_check_master_status();
							//DbInterface_transaction_commit("COMMIT;");
						}
						//				else
						//					DbInterface_transaction_commit("ROLLBACK;",0);
					}
					retval = UART_packet_parser_nresponder(bTemp);
					if(retval)
					{
						CstcLog_printf("*****************************************");
						pkt_ptr= (char *)malloc(sizeof(registration_response_struct));
						memset(pkt_ptr,0,sizeof(registration_response_struct));
						UART_compose_registration_response_packet(pkt_ptr,&length);
						CstcLog_printf("Sending Registration Response Packet");
						//						length += 26 ;

						if(strlen(registration_response_struct_var.terminal_id)> 0)
						{
							if(master_status_struct_var.machine_register!=2)
							{
								//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
								if(DbInterface_Update_Master_Status_Data("update master_status set machine_register ='2'"))
								{
									master_status_struct_var.machine_register=2;
									*j=0;
									//	DbInterface_transaction_commit("COMMIT;");
								}
								//						else
								//							DbInterface_transaction_commit("ROLLBACK;",0);
							}
						}

						pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
						memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
						UART_final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

						CstcLog_printf("Sent bytes are : ");
						CstcLog_printf_2("\n .......Send.........\n ");

						CstcLog_HexDump_2(pkt_format_ptr,length);

						CstcLog_printf("Sent %d bytes ", length);
						error = write(iclient_socket,pkt_format_ptr, length);

						if (error <= 0)
						{
							CstcLog_printf("Registration Response Packet 56 Connection was broken !!");

							memset(Err_MsgAry,0,3);
							sprintf(Err_MsgAry,"%02d",CONNECTION_WAS_BROKEN);
							strcat(Err_Msg,Err_MsgAry);
							Err_Len=Err_Len+2;//strlen(Err_MsgAry);
							CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
							memset(Err_MsgAry,0,3);


							//					strcat(Err_Msg,"Registration Response Packet 56 Connection was broken !!");
							//					Err_Len = Err_Len + strlen("Registration Response Packet 56 Connection was broken !!");
							//					CstcLog_printf("Err_Len = %d",Err_Len);
							UART_check_master_status();
							Show_Error_Msg_UART("REGISTRATION FAILED");
							beep(2800,1000);
							if(pkt_ptr!=NULL)
							{
								free(pkt_ptr);
								pkt_ptr=NULL;
							}
							if(pkt_format_ptr!=NULL)
							{
								free(pkt_format_ptr);
								pkt_format_ptr=NULL;
							}
							return 0;
						}
						else
						{
							//strcat(Err_Msg,"Registration Response Packet 56 Connection was Not broken !!");
							//Err_Len = Err_Len + strlen("Registration Response Packet 56 Connection was Not broken !!");
							CstcLog_printf("Err_Len = %d",Err_Len);

							UART_check_master_status();
							if(pkt_ptr!=NULL)
							{
								free(pkt_ptr);
								pkt_ptr=NULL;
							}
							if(pkt_format_ptr!=NULL)
							{
								free(pkt_format_ptr);
								pkt_format_ptr=NULL;
							}
							return 1;
						}

					}
					else
					{
						ack_flag=1;
						CstcLog_printf("ETM REGISTRATION failed");

						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",REGISTRATION_REQUEST_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);
						Err_Msg[Err_Len]='\0';
						//				strcat(Err_Msg,"ETM Request failed");
						//				Err_Len = Err_Len + strlen("ETM Request failed");
						//				CstcLog_printf("Err_Len = %d",Err_Len);
						Show_Error_Msg_UART("REGISTRATION FAILED");
						beep(2800,1000);
					}
				}
				UART_check_master_status();
				break;
			case 58 :
			{
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
				retval = UART_packet_parser_nresponder(bTemp);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("PKG FILE NAME REQ Success");
					Show_Error_Msg_UART("DOWNLOADING \nPKG NAME RECEIVED");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("PKG FILE NAME REQ failed");
					Show_Error_Msg_UART("DOWNLOADING \nPKG NAME FAILED");

					sprintf(Err_Msg,"%02d",PKG_FILE_NAME_REQ_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
			}
			break;
			case 60 :
			{
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
				retval = UART_packet_parser_nresponder(bTemp);
				if(retval)
				{
					ack_flag=1;
					CstcLog_printf("PKG FILE DOWNLOAD REQ Success");
					Show_Error_Msg_UART("DOWNLOADING \nPKG FILE RECEIVED");

					sprintf(Err_Msg,"%02d",SUCCESS);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					memset(Err_Msg,0,sizeof(Err_Msg));
					Err_Len=0;

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("PKG FILE DOWNLOAD REQ failed");
					Show_Error_Msg_UART("DOWNLOADING \nPKG FILE FAILED");

					sprintf(Err_Msg,"%02d",PKG_FILE_NAME_REQ_FAILED);
					Err_Len=Err_Len+2;//strlen(Err_Msg);
					CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
					Err_Msg[Err_Len]='\0';
					beep(2800,1000);
				}
			}
			break;
			default :
				break;
			}
		}
	}
	//	else           //open when backend is ready with crc
	//	{
	//		CstcLog_printf("\n......CRC Failed........\n ");
	//		ack_flag = 1;
	//
	//		sprintf(Err_MsgAry,"%02d",CRC_CHECK_FAILED);
	//		strcat(Err_Msg,Err_MsgAry);
	//		Err_Len=Err_Len+2;//strlen(Err_MsgAry);
	//		CstcLog_printf("Err_Len = %d Err_Msg =%s ",Err_Len,Err_Msg);
	//		memset(Err_MsgAry,0,3);
	//	}
	if(ack_flag)
	{
		//length=100;
		CstcLog_printf("Err_Len=%d>>------ACK1--------<<%d",Err_Len,(sizeof(ack_packet_struct)+Err_Len+1+1));
		pkt_ptr = (char*)malloc((sizeof(ack_packet_struct)+Err_Len+1+1));
		memset(pkt_ptr,0,(sizeof(ack_packet_struct)+Err_Len+1+1));

		CstcLog_printf(">>------ACK2--------<<");
		//pkt_ptr = (char*)malloc(sizeof(ack_packet_struct));
		UART_compose_ack_packet(pkt_ptr,&length);

		pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length+1+1));
		memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length+1+1));

		CstcLog_printf("%d>>------ACK3--------<<%d",length,(sizeof(packet_format_struct)+length+1+1));
		UART_final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

		CstcLog_printf("Sent bytes are : ");
		//		CstcLog_HexDump(pkt_format_ptr,length);  //Swap Enable this after chk for the download crash
		CstcLog_printf_2("\n .......Send.........\n ");

		CstcLog_HexDump_2(pkt_format_ptr,length);
		//	for(j=0 ; j< 100 ; j++)
		//		CstcLog_printf("%2x, ",pkt_format_ptr[j]);
		CstcLog_printf("Sent %d bytes",length);
		CstcLog_printf(">>-----------------<<");

		if(write(iclient_socket, pkt_format_ptr, length) < 0)
		{
			CstcLog_printf("ACK Connection was broken!!!");
			Show_Error_Msg_UART("ACKNOWLEDGE FAILED");
			beep(2800,1000);
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			//			memset(Err_Msg1,0,500);
			Err_Len=0;
			return 0;
		}
		else
		{
			CstcLog_printf("ACK Connection was Not broken!!!");
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			CstcLog_printf("1. ACK Connection was Not broken!!!");
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			CstcLog_printf("2. ACK Connection was Not broken!!!");
			memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			//			memset(Err_Msg1,0,500);
			CstcLog_printf("3. ACK Connection was Not broken!!!");
			Err_Len=0;
			CstcLog_printf("4. ACK Connection was Not broken!!!");
			return 1;
		}
	}
	if(pkt_ptr!=NULL)
	{
		free(pkt_ptr);
		pkt_ptr=NULL;
	}
	if(pkt_format_ptr!=NULL)
	{
		free(pkt_format_ptr);
		pkt_format_ptr=NULL;
	}
	memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);
	return 0;
}

//----------------------------------------WAYBILL MASTER PACKET------------------------------------
void UART_compose_waybill_packet(char * pkt_ptr)
{
}

//----------------------------------------SCHEDULE MASTER PACKET------------------------------------
void UART_compose_schedule_packet(char * pkt_ptr)
{
}

//-------------------------------------------DUTY MASTER PACKET----------------------------------------
void UART_compose_duty_packet(char * pkt_ptr)
{
	int temp = DUTY_MASTER_PKT_ID;
	temp = htons(temp);
}

//----------------------------------------ROUTE MASTER PACKET-----------------------------------------
void UART_compose_route_packet(char * pkt_ptr)
{
}

//-----------------------------------------FARE CHART PACKET-------------------------------------------
void UART_compose_fare_chart_packet(char * pkt_ptr)
{}

#if 0
#else
int UART_packet_parser_nresponder(char * bTemp)
{
	char * pkt_ptr;
	//	char *mac_ID = NULL;
	char *etim_no=NULL;
	FILE* tempFile= NULL;
	FILE* tempFile2= NULL;
	int temp = 0;
	char recvPktID[3]={'\0'};
	int len = 0,pkt_data_len=0;
	//	char tempName[100]={'\0'};
	//	char *tempStr; //[1024];
	//	int tempStrLen=0;
	char *sn = NULL;
	char Err_MsgAry[3]={'\0'};
	struct stat sb;
	char tempStr[300] = {'\0'}; //[1024];
	int dbretval=0;

	//	int update_data_flag=0;
#if COMPRESSION_DECOMPRESSION

	char tempCh[4]={'\0'};
	memset(tempCh,0,3);
	memcpy(tempCh,bTemp,3);
	tempCh[strlen(tempCh)]='\0';

	if(strcmp(tempCh,"CTS") == 0)
#else
		if(bTemp[0] == ';')
#endif
	{
		CstcLog_printf("Found start indicator......");
		memset_packet_format();
		memcpy(&(packet_format_struct_var.start_indicator), bTemp,START_INDICATOR_LEN);
		CstcLog_printf("1......");
		memcpy((packet_format_struct_var.data_length),(bTemp+START_INDICATOR_LEN) ,DATA_LENGTH_LEN);
		CstcLog_printf("2......");
		packet_format_struct_var.data_length[DATA_LENGTH_LEN]='\0';
		packet_format_struct_var.update_data_flag = bTemp[(START_INDICATOR_LEN+DATA_LENGTH_LEN)];
		CstcLog_printf("3......");
		len = atoi(packet_format_struct_var.data_length);
		CstcLog_printf("data_length in packet %s = %d",packet_format_struct_var.data_length, len);
		pkt_data_len = len-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+CHECKSUM_LEN);
		packet_format_struct_var.data = (char*)malloc((pkt_data_len+1));
		memset(packet_format_struct_var.data,0,pkt_data_len+1);
		CstcLog_printf("pkt_data_len %d......",pkt_data_len);
		memcpy(packet_format_struct_var.data,(bTemp+START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),pkt_data_len);
		packet_format_struct_var.data[pkt_data_len]='\0';
		memcpy(packet_format_struct_var.checksum,(bTemp+START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+pkt_data_len),CHECKSUM_LEN);
		packet_format_struct_var.checksum[CHECKSUM_LEN]='\0';
		//		pkt_ptr = malloc(len);
		//		memcpy(pkt_ptr,packet_format_struct_var.data,len);
		pkt_ptr=packet_format_struct_var.data;

		recvPktID[0] = pkt_ptr[0];
		recvPktID[1] = pkt_ptr[1];

		memcpy(recvPktID, pkt_ptr, 2);
		//memcpy(&temp, recvPktID, 2);
		////////////////
		////////////////
		temp = atoi(recvPktID);
		CstcLog_printf("packet data length = %d  & Id = %d",pkt_data_len, temp);
		CstcLog_printf("pkt_ptr %s ",pkt_ptr);

		switch(temp)
		{
		case 2:/*----------------------Schedule Details-------------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN/*+SEC_KEY_LEN+WAYBILL_NO_LEN*/))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				//Err_Msg[Err_Len]='\0';

				//				strcat(Err_Msg1,Err_Msg);
				//				Err_Len=Err_Len+strlen(Err_Msg1);
				//CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_Msg1);

				//				strcat(Err_Msg,"Packet length is less than actual required");//LESS_PACKET_LENGTH
				//				Err_Len=Err_Len+strlen("Packet length is less than actual required");
				//				CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				UART_get_machine_info(sn, (MACHINE_SN_LEN+1));

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				//memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				//memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN/*+SEC_KEY_LEN*/), WAYBILL_NO_LEN);

				CstcLog_printf("machine_no  %s ", download_response_struct_var.machine_no);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);
				CstcLog_printf("terminal_id  %s ", download_response_struct_var.terminal_id);
				//				CstcLog_printf("sec_key_ack  %s ", download_response_struct_var.sec_key_ack);
				//				CstcLog_printf("waybill_no  %s ", download_response_struct_var.waybill_no);
#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(Err_Msg,0,sizeof(Err_Msg));
					//memcpy(Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					Err_Len=Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(Err_Msg,"MAC Address : No match found");
					Err_Len=Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//Err_Msg[Err_Len]='\0';

					//					strcat(Err_Msg1,Err_Msg);
					//					Err_Len=Err_Len+strlen(Err_Msg1);
					//CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_Msg1);
					//					strcat(Err_Msg,"Machine Number : No match found");
					//					Err_Len=Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//Err_Msg[Err_Len]='\0';

					//					strcat(Err_Msg1,Err_Msg);
					//					Err_Len=Err_Len+strlen(Err_Msg1);
					//CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_Msg1);

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					CstcLog_printf("TERMINAL_ID_NOT_FOUND");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//Err_Msg[Err_Len]='\0';

					//					strcat(Err_Msg1,Err_Msg);
					//					Err_Len=Err_Len+strlen(Err_Msg1);
					//CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_Msg1);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				/*if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
						{
							CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
							//					strcat(Err_Msg,"Security Key verified");
							//					Err_Len=Err_Len+strlen("Security Key verified");
							//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
						}
						else
						{
							sprintf(Err_Msg,"%02d",SECURITY_KEY_NOT_FOUND);
							strcat(Err_Msg1,Err_Msg);
							Err_Len=strlen(Err_Msg1);
							//Err_Msg[Err_Len]='\0';

		//					strcat(Err_Msg1,Err_Msg);
		//					Err_Len=Err_Len+strlen(Err_Msg1);
							CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_Msg1);
							if(sn!=NULL)
								free(sn);
							if(etim_no!=NULL)
								free(etim_no);
		#if MAC_ID_VERIFY_DISABLED
							if(mac_ID!=NULL)
								free(mac_ID);
		#endif
							return 0;
						}
				if((atoi(download_response_struct_var.waybill_no))==(atoi(waybill_master_struct_var.waybill_no)))
				{
					CstcLog_printf("waybill number  %s verified", download_response_struct_var.waybill_no);
					//					strcat(Err_Msg,"Waybill number verified");
					//					Err_Len=Err_Len+strlen("Waybill number verified");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);

					return 1;
				}

				else
				{
					sprintf(Err_Msg,"%02d",WAYBILL_NUMBER_NOT_FOUND);
					strcat(Err_Msg1,Err_Msg);
					Err_Len=strlen(Err_Msg1);
					//Err_Msg[Err_Len]='\0';

					//					strcat(Err_Msg1,Err_Msg);
					//					Err_Len=Err_Len+strlen(Err_Msg1);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_Msg1);
					//					strcat(Err_Msg,"Waybill Number : No match found");
					//					Err_Len=Err_Len+strlen("Waybill Number : No match found");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}*/
			}
			break;
		case 6:/*----------------------Start Duty-------------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+WAYBILL_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				//				strcat(Err_Msg,"Packet length is less than actual required");//LESS_PACKET_LENGTH
				//				Err_Len=Err_Len+strlen("Packet length is less than actual required");
				//				CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg)
				return 0;
			}
			else
			{
				if(((master_status_struct_var.insert_waybill != 2) /*|| (master_status_struct_var.ticket_fare != 2)*/
						|| (master_status_struct_var.machine_register != 2) /*|| (master_status_struct_var.upload_master != 2)*/
				/*|| (master_status_struct_var.insert_route != 2)*/))
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MASTER_NOT_INSERT_START_DUTY_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				UART_get_machine_info(sn, (MACHINE_SN_LEN+1));

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN), WAYBILL_NO_LEN);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);
#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(Err_Msg,0,sizeof(Err_Msg));
					//memcpy(Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					Err_Len=Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(Err_Msg,"MAC Address : No match found");
					Err_Len=Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(Err_Msg,"Machine Number : No match found");
					//					Err_Len=Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					CstcLog_printf("TERMINAL_ID_NOT_FOUND");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
				{
					CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
					//					strcat(Err_Msg,"Security Key verified");
					//					Err_Len=Err_Len+strlen("Security Key verified");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SECURITY_KEY_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.waybill_no))==(atoi(waybill_master_struct_var.waybill_no)))
				{
					CstcLog_printf("waybill number  %s verified", download_response_struct_var.waybill_no);
					//					strcat(Err_Msg,"Waybill number verified");
					//					Err_Len=Err_Len+strlen("Waybill number verified");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
#if 0
					DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					//					if(0<(DbInterface_Get_Row_Count(" (select distinct C.route_id from waybill_master A inner join duty_master B on A.duty_no=B.duty_no and B.schedule_status='ACTIVE' inner join schedule_master C on C.schedule_no=B.schedule_no left join route_master D on C.route_id=D.route_id left join rate_master E on E.rate_id=C.rate_id where D.route_id is null or E.rate_id is null) AA",335)))
					if(0<(DbInterface_Get_Row_Count(" (select distinct C.route_id from waybill_master A inner join duty_master B on A.duty_no=B.duty_no and B.schedule_status='ACTIVE' inner join schedule_master C on C.schedule_id=B.schedule_id left join route_master D on C.route_id=D.route_id where D.route_id is null) AA",268,0)))
					{
						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",REC_NOT_FND_START_DUTY_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);

						//						strcat(Err_Msg,"Start Duty failed");
						//						Err_Len=Err_Len+strlen("Start Duty failed");
						//						CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
						if(sn!=NULL)
						{
							free(sn);
							sn=NULL;
						}
						if(etim_no!=NULL)
						{
							free(etim_no);
							etim_no=NULL;
						}
#if MAC_ID_VERIFY_DISABLED
						if(mac_ID!=NULL)
							free(mac_ID);
#endif
						DbInterface_transaction_commit("ROLLBACK;",0);
						return 0;

					}

					//					if(0<(DbInterface_Get_Row_Count(" (select distinct C.rate_id from waybill_master A inner join duty_master B on A.duty_no=B.duty_no and B.schedule_status='ACTIVE' inner join schedule_master C on C.schedule_no=B.schedule_no left join route_master D on C.route_id=D.route_id left join rate_master E on E.rate_id=C.rate_id where D.route_id is null or E.rate_id is null) AA",334)))
					//					if(0<(DbInterface_Get_Row_Count(" (select distinct C.rate_id from waybill_master A inner join duty_master B on A.duty_no=B.duty_no and B.schedule_status='ACTIVE' inner join schedule_master C on C.schedule_id=B.schedule_id left join route_master D on C.route_id=D.route_id left join rate_master E on E.rate_id=C.rate_id where D.route_id is null or E.rate_id is null) AA",334,0)))
					if(0<(DbInterface_Get_Row_Count(" (select distinct C.rate_id from waybill_master A inner join duty_master B on A.duty_no=B.duty_no and B.schedule_status='ACTIVE' inner join schedule_master C on C.schedule_id=B.schedule_id left join rate_master E on E.rate_id=C.rate_id where (E.rate_id is null AND C.rate_id !='0')) AA",285,0)))
					{
						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",REC_NOT_FND_START_DUTY_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);

						//						strcat(Err_Msg,"Start Duty failed");
						//						Err_Len=Err_Len+strlen("Start Duty failed");
						//						CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
						if(sn!=NULL)
						{
							free(sn);
							sn=NULL;
						}
						if(etim_no!=NULL)
						{
							free(etim_no);
							etim_no=NULL;
						}
#if MAC_ID_VERIFY_DISABLED
						if(mac_ID!=NULL)
							free(mac_ID);
#endif
						DbInterface_transaction_commit("ROLLBACK;",0);
						return 0;

					}
#endif
					if(DbInterface_Update_Master_Status_Data("update waybill_master set shift_status ='Y'"))
					{
						//						strcat(Err_Msg,"Start Duty Done");
						//						Err_Len=Err_Len+strlen("Start Duty Done");
						//						CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
						CstcLog_printf("Start Duty Done");
						if(sn!=NULL)
						{
							free(sn);
							sn=NULL;
						}
						if(etim_no!=NULL)
						{
							free(etim_no);
							etim_no=NULL;
						}
#if MAC_ID_VERIFY_DISABLED
						if(mac_ID!=NULL)
							free(mac_ID);
#endif
						DbInterface_transaction_commit("COMMIT;",0);
						return 1;
					}
					else
					{
						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",START_DUTY_FAILED);
						strcat(Err_Msg,Err_MsgAry);
						Err_Len=Err_Len+2;//strlen(Err_MsgAry);
						CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);

						//						strcat(Err_Msg,"Start Duty failed");
						//						Err_Len=Err_Len+strlen("Start Duty failed");
						//						CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
						if(sn!=NULL)
						{
							free(sn);
							sn=NULL;
						}
						if(etim_no!=NULL)
						{
							free(etim_no);
							etim_no=NULL;
						}
#if MAC_ID_VERIFY_DISABLED
						if(mac_ID!=NULL)
							free(mac_ID);
#endif
						DbInterface_transaction_commit("ROLLBACK;",0);
						return 0;
					}
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",WAYBILL_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(Err_Msg,"Waybill Number : No match found");
					//					Err_Len=Err_Len+strlen("Waybill Number : No match found");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
			}
			break;
		case 45:/*----------------------Download data-------------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+WAYBILL_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				//				strcat(Err_Msg,"Packet length is less than actual required");//LESS_PACKET_LENGTH
				//				Err_Len=Err_Len+strlen("Packet length is less than actual required");
				//				CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
				return 0;
			}
			else
			{
				//DbInterface_Update_Master_Status_Data("update waybill_master set collection_status ='D'");
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				UART_get_machine_info(sn, (MACHINE_SN_LEN+1));

				memset(download_response_struct_var.machine_no, 0, MACHINE_SN_LEN);
				memset(download_response_struct_var.mac_address, 0, MAC_ADDR_LEN);
				memset(download_response_struct_var.terminal_id, 0, TERMINAL_ID_LEN);
				memset(download_response_struct_var.sec_key_ack, 0, SEC_KEY_LEN);
				memset(download_response_struct_var.waybill_no, 0,WAYBILL_NO_LEN );

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+ SEC_KEY_LEN),WAYBILL_NO_LEN);


				CstcLog_printf("download_response_struct_var.machine_no = %s",download_response_struct_var.machine_no);
				CstcLog_printf("download_response_struct_var.mac_address = %s",download_response_struct_var.mac_address);
				CstcLog_printf("download_response_struct_var.terminal_id = %s",download_response_struct_var.terminal_id);
				CstcLog_printf("download_response_struct_var.sec_key_ack = %s",download_response_struct_var.sec_key_ack);
				CstcLog_printf("download_response_struct_var.waybill_no = %s",download_response_struct_var.waybill_no);

				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);


#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(Err_Msg,0,sizeof(Err_Msg));
					//memcpy(Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					Err_Len=Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(Err_Msg,"MAC Address : No match found");
					Err_Len=Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Machine Number : No match found");
					//					Err_Len=Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}

#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"ERROR OPENING FILE etim_no.txt");
					//					Err_Len=Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Terminal Id : No match found");
					//					Err_Len=Err_Len+strlen("Terminal Id : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.sec_key_ack))==(atoi(waybill_master_struct_var.waybill_lock_code)))
				{
					CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
					//					strcat(Err_Msg,"Security Key verified");
					//					Err_Len=Err_Len+strlen("Security Key verified");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					return 1;
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SECURITY_KEY_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);


					//					strcat(Err_Msg,"Security Key : No match found");
					//					Err_Len=Err_Len+strlen("Security Key : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
#if 0
#endif
			}
			break;

		case 47:
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+TICKET_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				UART_get_machine_info(sn, (MACHINE_SN_LEN+1));

				memset(download_response_struct_var.machine_no, 0, MACHINE_SN_LEN);
				memset(download_response_struct_var.mac_address, 0, MAC_ADDR_LEN);
				memset(download_response_struct_var.terminal_id, 0, TERMINAL_ID_LEN);
				memset(download_response_struct_var.sec_key_ack, 0, SEC_KEY_LEN);
				memset(download_response_struct_var.start_tkt_cnt, 0,TICKET_NO_LEN );
				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.start_tkt_cnt, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN),TICKET_NO_LEN );
				download_response_struct_var.start_tkt_cnt[TICKET_NO_LEN]='\0';

				CstcLog_printf("\n--------------------------- Check Request  ---------------------------------\n");
				CstcLog_printf("download_response_struct_var.machine_no %s ",download_response_struct_var.machine_no);
				CstcLog_printf("download_response_struct_var.mac_address %s ",download_response_struct_var.mac_address);
				CstcLog_printf("download_response_struct_var.terminal_id %s ",download_response_struct_var.terminal_id);
				CstcLog_printf("download_response_struct_var.sec_key_ack %s ",download_response_struct_var.sec_key_ack);
				CstcLog_printf("download_response_struct_var.start_tkt_cnt %s ",download_response_struct_var.start_tkt_cnt);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);

#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(Err_Msg,0,sizeof(Err_Msg));
					//memcpy(Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					Err_Len=Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(Err_Msg,"MAC Address : No match found");
					Err_Len=Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(Err_Msg,"Machine Number : No match found");
					//					Err_Len=Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_Msg,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"ERROR OPENING FILE etim_no.txt");
					//					Err_Len=Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Terminal Id : No match found");
					//					Err_Len=Err_Len+strlen("Terminal Id : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
				{
					CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
					//					strcat(Err_Msg,"Security Key verified");
					//					Err_Len=Err_Len+strlen("Security Key verified");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					return 1;
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SECURITY_KEY_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Security Key : No match found");
					//					Err_Len=Err_Len+strlen("Security Key : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}

#if 0
#endif
			}
			break;

		case 50:/*--------------------ERASE MASTER DATA-------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+WAYBILL_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				UART_get_machine_info(sn, (MACHINE_SN_LEN+1));

				memset(download_response_struct_var.machine_no, 0, MACHINE_SN_LEN);
				memset(download_response_struct_var.mac_address, 0, MAC_ADDR_LEN);
				memset(download_response_struct_var.terminal_id, 0, TERMINAL_ID_LEN);
				memset(download_response_struct_var.sec_key_ack, 0, SEC_KEY_LEN);
				memset(download_response_struct_var.waybill_no, 0,WAYBILL_NO_LEN );

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN), WAYBILL_NO_LEN);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);

#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(Err_Msg,0,sizeof(Err_Msg));
					//memcpy(Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					Err_Len=Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(Err_Msg,"MAC Address : No match found");
					Err_Len=Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Machine Number : No match found");
					//					Err_Len=Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}

#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"ERROR OPENING FILE etim_no.txt");
					//					Err_Len=Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Terminal Id : No match found");
					//					Err_Len=Err_Len+strlen("Terminal Id : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
				{
					CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
					//					strcat(Err_Msg,"Security Key verified");
					//					Err_Len=Err_Len+strlen("Security Key verified");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SECURITY_KEY_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Security Key : No match found");
					//					Err_Len=Err_Len+strlen("Security Key : No match found");
					//					CstcLog_printf("Err_Len=%d , Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.waybill_no))==(atoi(waybill_master_struct_var.waybill_no)))
				{
					CstcLog_printf("waybill number  %s verified", download_response_struct_var.waybill_no);
					//					strcat(Err_Msg,"Waybill number verified");
					//					Err_Len=Err_Len+strlen("Waybill number verified");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					//					if(waybill_master_struct_var.collection_status=='D')
					//					{
					//						strcat(Err_Msg,"Start Duty Done");
					//						Err_Len=Err_Len+strlen("Start Duty Done");
					//						CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 1;
					//					}
					//					else
					//					{
					//						strcat(Err_Msg,"Start Duty failed");
					//						Err_Len=Err_Len+strlen("Start Duty failed");
					//						CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					//						if(sn!=NULL)
					//							free(sn);
					//						if(etim_no!=NULL)
					//							free(etim_no);
					//						if(mac_ID!=NULL)
					//							free(mac_ID);
					//						return 0;
					//					}
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",WAYBILL_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Waybill Number : No match found");
					//					Err_Len=Err_Len+strlen("Waybill Number : No match found");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
			}
#if 0
#endif
			break;

		case 54:/*--------------------GET ETIM NO------------------------*/
			CstcLog_printf("******************GET ETIM NO case 54***********************");
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				if(strncmp((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN),"000000000000",MAC_ADDR_LEN)== 0)
				{
					CstcLog_printf("mac_ID :- %s", (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN));
				}
				else
				{
					CstcLog_printf("mac_ID :- %sFailed to verify", (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN));
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MAC_ADDRESS_NO_MATCH_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(Err_Msg,"MAC Address : No match found");
					//					Err_Len=Err_Len+strlen("MAC Address : No match found");
					//					CstcLog_printf("Err_Len =%d Err_Msg=%s",Err_Len,Err_Msg);
					return 0;
				}
				if(strncmp((pkt_ptr + PKT_ID_LEN),"00000000",MACHINE_SN_LEN)== 0)
				{
					CstcLog_printf("machine_no = %s ", (pkt_ptr + PKT_ID_LEN));
				}
				else
				{
					CstcLog_printf("machine_no = %s Failed to verify", (pkt_ptr + PKT_ID_LEN));
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(Err_Msg,"Machine Number : No match found");
					//					Err_Len=Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					return 0;
				}
				return 1;

			}
			break;
		case 56:/*--------------------ETIM NO REG------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				//				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				//				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				UART_get_machine_info(sn, (MACHINE_SN_LEN+1));

				//				tempStr = malloc(1024*sizeof(char));
				//				memset(tempStr,0,1024);
				//				strcpy(tempStr,"./etim_no W ");

				memset(registration_response_struct_var.machine_no, 0, MACHINE_SN_LEN);
				memset(registration_response_struct_var.mac_address, 0, MAC_ADDR_LEN);
				memset(registration_response_struct_var.terminal_id, 0, TERMINAL_ID_LEN);
				memcpy(registration_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(registration_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(registration_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				CstcLog_printf("mac_address  %s ", registration_response_struct_var.mac_address);

#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile = NULL;
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					strcat(Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					Err_Len=Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("Err_Len =%d Err_Msg=%s",Err_Len,Err_Msg);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
						free(sn);
					//					if(etim_no!=NULL)
					//						free(etim_no);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile = NULL;

				if(strcmp(registration_response_struct_var.mac_address,mac_ID)!=0)//"00257E019279")==0)
				{
					//memset(Err_Msg,0,sizeof(Err_Msg));
					//memcpy(Err_Msg,"MAC Address : No match found",28);
					strcat(Err_Msg,"MAC Address : No match found");
					Err_Len=Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("Err_Len =%d Err_Msg=%s",Err_Len,Err_Msg);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
						free(sn);
					//					if(etim_no!=NULL)
					//						free(etim_no);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				else
					CstcLog_printf("mac_address  %s verified", registration_response_struct_var.mac_address);
#endif
				if(strcmp(registration_response_struct_var.machine_no,sn)!=0)
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"Machine Number : No match found");
					//					Err_Len=Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}

					//					if(etim_no!=NULL)
					//						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				else
					CstcLog_printf("machine_no  %s verified", registration_response_struct_var.machine_no);
				tempFile = NULL;
				tempFile= fopen(ETIM_NO_FILE_ADDR,"w");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(Err_Msg,"ERROR OPENING FILE etim_no.txt");
					//					Err_Len=Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
					//					CstcLog_printf("Err_Len=%d Err_Msg=%s",Err_Len,Err_Msg);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}

					//					if(etim_no!=NULL)
					//						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				else
				{
					CstcLog_printf("Tempfile open success :%d",tempFile);
					//					strcat(Err_Msg,"Tempfile open success");
					//					Err_Len=Err_Len+strlen("Tempfile open success");
					//					CstcLog_printf("Err_Len =%d Err_Msg=%s",Err_Len,Err_Msg);
				}
				fputs(registration_response_struct_var.terminal_id,tempFile);
				//				fprintf(tempFile,"%s",registration_response_struct_var.terminal_id);
				//				fscanf(tempFile,"%s",etim_no);
				//				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile);
				fflush(tempFile);
				CstcLog_printf("terminal_id :- %s", registration_response_struct_var.terminal_id);
				//				CstcLog_printf("etim_no :- %s",etim_no );
				fclose(tempFile);
				tempFile = NULL;
				//									memcpy(tempStr+strlen("./etim_no W "),registration_response_struct_var.terminal_id, TERMINAL_ID_LEN);
				//									CstcLog_printf("system cmd  %s ", tempStr);
				//									system(tempStr);
				if(strlen(registration_response_struct_var.terminal_id)>0)//					if(system(tempStr)>0)
				{
					return 1;
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}

					//					if(etim_no!=NULL)
					//						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
				}
				else
				{
					memset(registration_response_struct_var.terminal_id,'0',TERMINAL_ID_LEN);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}

					//					if(etim_no!=NULL)
					//						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
			}
			break;
		case 58:/*----------------------PKG FILE NAME-------------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				UART_get_machine_info(sn, (MACHINE_SN_LEN+1));

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				//				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				//				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);

				CstcLog_printf("machine_no  %s ", download_response_struct_var.machine_no);
				//				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);
				//				CstcLog_printf("terminal_id  %s ", download_response_struct_var.terminal_id);
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					return 0;
				}
			}
			break;
		case 60:/*----------------------PKG FILE NAME-------------------------------*/
			if(len<(PKT_ID_LEN))//+MACHINE_SN_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
#if 0
#endif
				if (stat (UPDATED_PKG_FILE_ADDR, & sb) == 0)
				{
					if(strstr(bTemp,"NO FIRMWARE UPDATE REQUIRED")==0)
					{
						CstcLog_printf("NO FIRMWARE UPDATE REQUIRED", download_response_struct_var.machine_no);
						//						Show_Error_Msg("NO FIRMWARE UPDATE REQUIRED");
						display_popup("NO FIRMWARE UPDATE REQUIRED");
						dbretval=0;
					}
					else
					{
						dbretval=0;
						memset(tempStr,0,300);
						sprintf(tempStr,"chmod -R 777 %s",UPDATED_PKG_FILE_ADDR);
						CstcLog_printf("System chmod command \"%s\" ", tempStr);
						dbretval=system(tempStr);
						CstcLog_printf("System chmod command retval \"%d\" ", dbretval);



						while(!system(NULL)){
							CstcLog_printf("Shell is unavailable");
							sleep(2);
						}
						//					sleep(2);
						dbretval=-1;
						dbretval=sys_software_update(UPDATED_PKG_FILE_ADDR);
						CstcLog_printf("sys_software_update retval \"%d\" ", dbretval);
					}
				}
				if(dbretval != 0)
				{
					CstcLog_printf("PKG UPDATE failed");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",PKG_FILE_DOWNLOAD_REQ_FAILED);
					strcat(Err_Msg,Err_MsgAry);
					Err_Len=Err_Len+2;//strlen(Err_MsgAry);
					CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					return 0;
				}

			}
			break;
		default:
			return 0;
			break;
		}
		sprintf(Err_Msg,"%02d",SUCCESS);
		Err_Len = Err_Len + 2;// strlen(Err_Msg);
		CstcLog_printf("Err_Len = %d Err_Msg =%s",Err_Len,Err_Msg);
		Err_Msg[Err_Len]='\0';
		memset(Err_Msg,0,sizeof(Err_Msg));
		Err_Len=0;
		//		strcat(Err_Msg,"Parsing Successful");
		//		Err_Len=Err_Len+strlen("Parsing Successful");
		//		CstcLog_printf("Err_Len=%d Err_Msg =%s",Err_Len,Err_Msg);
		CstcLog_printf("Parsing Successful");

		//		if(!(DbInterface_Insert_In_All_Tables1(tempName, tempStr,tempStrLen)))
		//		{
		//			CstcLog_printf("Insertion in table \'%s\' failed", tempName);
		//			//memset(Err_Msg,0,sizeof(Err_Msg));
		//			//memcpy(Err_Msg,"Data Insertion in table failed",30);
		//			strcat(Err_Msg,"Data Insertion in table failed");
		//			Err_Len =Err_Len+strlen("Data Insertion in table failed");
		//			CstcLog_printf("Err_Len =%d Err_Msg =%s",Err_Len,Err_Msg);
		//			memset_packet_format();
		//			if(tempStr!=NULL)
		//				free(tempStr);
		//			return 0;
		//		}
		//		else
		//		{
		//			CstcLog_printf("Insertion in table \'%s\' success", tempName);
		//			//memset(Err_Msg,0,sizeof(Err_Msg));
		//			//memcpy(Err_Msg,"Data Insertion successful",25);
		//			strcat(Err_Msg,"Data Insertion successful");
		//			Err_Len =Err_Len+strlen("Data Insertion successful");
		//			CstcLog_printf("Err_Len =%d Err_Msg =%s",Err_Len, Err_Msg);
		//			memset_packet_format();
		//			if(tempStr!=NULL)
		//				free(tempStr);
		//			return 1;
		//		}
		memset_packet_format();
		//		memset(tempStr,0,tempStrLen);
		//		if(tempStr!=NULL)
		//			free(tempStr);
		return 1;
	}
	else
	{
		CstcLog_printf("Parsing Fail Resend Packet");
		memset(Err_MsgAry,0,3);
		sprintf(Err_MsgAry,"%02d",RESEND_PACKETS);
		strcat(Err_Msg,Err_MsgAry);
		Err_Len=Err_Len+2;//strlen(Err_MsgAry);
		CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);
		memset(Err_MsgAry,0,3);
		//		strcat(Err_Msg,"Parsing Fail Resend Packet");
		//		Err_Len =Err_Len+strlen("Parsing Fail Resend Packet");
		//		CstcLog_printf("Err_Len =%d Err_Msg =%s",Err_Len , Err_Msg);
	}
	return 0;
}
#endif

#if COMPRESSION_DECOMPRESSION
int UART_packet_parser_input_master(char *tempCh,int recvPktID,int updateDataFlag)
#else
int UART_packet_parser_input_master(char tempCh,int recvPktID,int updateDataFlag)
#endif
{
	//	char *mac_ID = NULL;
	//	char *etim_no=NULL;
	//FILE* dbTblDataFile= NULL;
	//FILE* dbDataFile= NULL;
	//	int temp = 0;
	//	int len = 0,pkt_data_len=0,recLen=0,notFirstRecFlag=0;
	char *sub_string=NULL;
	char *waybill_sub_str1=NULL, *waybill_sub_str2=NULL;
//	char tempName[100]={'\0'};
	char tempStr[100] = {'\0'}; //[1024];
	char sysCmdStr[100] = {'\0'}; //[1024];
	int dbretval=0,retrycnt =0,count=0 ;//tempStrLen=0,
//	char QArr[50] = {'\0'};
	//	char *sn = NULL;
	char Err_MsgAry[3]={'\0'};
	unsigned char *inputData=NULL;
	//Bytef uncompData[1050]={'\0'};
//	uLongf uncompDataLen = 1050 ,iTemp = 0;

	struct stat sb;
	int flexi_fare_count = 0, rowcount = 0 ;
	char *sql =NULL;
	int sql_len = 150;

	//	typedef unsigned char Bytef;
	//	typedef unsigned long uLongf;

#if COMPRESSION_DECOMPRESSION
	CstcLog_printf("tempCh isssssssss %s",tempCh);
	if(strcmp(tempCh,"CTS") == 0)
#else
		CstcLog_printf("tempCh isssssssss %c",tempCh);
		if(tempCh == ';')
#endif
	{
#if 0
#endif


		//			fputs(bTemp+7,dbDataFile);
		//			fprintf(dbDataFile,"%s",bTemp);
		CstcLog_printf("recvPktID %d updateDataFlag %d",recvPktID,updateDataFlag);

		switch(recvPktID)
		{
		case 1 :/*-------------------WAYBILL----------------------*/
			//			tempStrLen = sizeof(waybill_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "waybill_master",14);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from waybill_master"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 3: /*----------------- FOR CSTC Schedule is not there -----------------------*/
			count=duty_master_schedule_check(global_schedule);
			CstcLog_printf("duty_master_schedule_check global_schedule = %s ",global_schedule);
			if(count > 0)
			{
				CstcLog_printf("Schedule is same..........");
			}
			else
			{
				/*if(!MultipleScheduleFlag)
				{
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					DbInterface_Update_Master_Status_Data("update master_status set insert_route ='0'");
					DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='0'");
					//DbInterface_transaction_commit("COMMIT;");
				}*/ // For CSTC Route is Static
			}
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			//			tempStrLen = sizeof(schedule_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "schedule_master", 15);
//			if(updateDataFlag==1)
				if(0)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				CstcLog_printf("global_schedule = %s ", global_schedule);

				if(!DbInterface_Delete_Table_active_schedule("Delete from schedule_master where schedule_no = ",global_schedule))
					//				if(!DbInterface_Delete_Table_active_schedule("Delete from schedule_master where schedule_id = ",global_schedule))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);

				//				if(!DbInterface_Delete_Table("Delete from schedule_master"))
				//					return 0;
#if MULTIPLE_SCHEDULE_ENABLE
				if(!DbInterface_Delete_Table_active_schedule("Delete from route_master where schedule_no = ",global_schedule))
					//				if(!DbInterface_Delete_Table_active_schedule("Delete from route_master where schedule_id = ",global_schedule))
					//if(!DbInterface_Delete_Table("Delete from route_master"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);

				if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart where schedule_no = ",global_schedule))
					//				if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart where schedule_id = ",global_schedule))
					//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);

				if(!DbInterface_Delete_Table_active_schedule("Delete from route_header where schedule_no = ",global_schedule))
					//				if(!DbInterface_Delete_Table_active_schedule("Delete from route_header where schedule_id = ",global_schedule))
					//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
				/*if(!MultipleScheduleFlag)
				{
					DbInterface_Update_Master_Status_Data("update master_status set insert_route ='0'");
					DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='0'");
				}*/ // For CSTC Route is Static
#endif

			}
			break;

		case 5:/*--------------------DUTY------------------------*/
			//			tempStrLen = sizeof(duty_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "duty_master",11);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				insert_duty_master_in_restore();
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from duty_master"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 7: /*-------------------ROUTE------------------------*/
			//			tempStrLen = sizeof(route_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "route_master",12);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				CstcLog_printf("global_Route_Id = %s ", global_Route_Id);
				sql=(char*)malloc(sql_len*sizeof(char));
				//				sprintf(sql,"route_master where schedule_no = '%s'",global_schedule);
				sprintf(sql,"route_master where route_id = '%s'",global_Route_Id);
				rowcount = DbInterface_Get_Row_Count(sql,strlen(sql),0);//check rowcount here if (rowcount==0) do not allow to delete

				if(rowcount > 0)
				{
					//				if(!DbInterface_Delete_Table_active_schedule("Delete from route_master where schedule_no = ",global_schedule))
					if(!DbInterface_Delete_Table_active_schedule("Delete from route_master where route_id = ",global_Route_Id))
						//if(!DbInterface_Delete_Table("Delete from route_master"))
					{
						//					DbInterface_transaction_commit("ROLLBACK;",0);
						//					if(tempStr!=NULL)
						//					{
						//						free(tempStr);
						//						tempStr=NULL;
						//					}
						if(sql != NULL)
						{
							free(sql);
							sql = NULL ;
						}
						return 0;
					}
				}
				if(sql != NULL)
				{
					free(sql);
					sql = NULL ;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 8:
		case 9:/*--------------------FARE------------------------*/
			//tempStrLen =sizeof(fare_chart_struct)+40;
			//			tempStrLen =sizeof(fare_chart_struct)+(VERSION_NO_LEN+1)+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)
			//			+(BUS_SRVC_ID_LEN+1)+(BUS_STOP_SEQ_LEN+1)+(BUS_STOP_CODE_LEN+1)
			//			+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)+(BUS_STOP_SEQ_LEN+1)
			//			+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)
			//			+1+(DATE_LEN+1)+(TIME_LEN+1)+(DATE_LEN+1)+(TIME_LEN+1)+1+1+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "fare_chart",10);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				//				CstcLog_printf("global_schedule = %s ", global_schedule);
				//
				//				sql=(char*)malloc(sql_len*sizeof(char));
				//				//				sprintf(sql,"fare_chart where schedule_no = '%s'",global_schedule);
				//				sprintf(sql,"fare_chart where schedule_id = '%s'",global_schedule);
				//				rowcount = DbInterface_Get_Row_Count(sql,strlen(sql),0);//check rowcount here if (rowcount==0) do not allow to delete
				//				if(rowcount > 0)
				{
					//				if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart where schedule_id = ",global_schedule))
					//if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart where schedule_id = ",global_schedule))
					if(!DbInterface_Delete_Table("Delete from fare_chart"))
					{
						//					DbInterface_transaction_commit("ROLLBACK;",0);
						//					if(tempStr!=NULL)
						//					{
						//						free(tempStr);
						//						tempStr=NULL;
						//					}
						if(sql != NULL)
						{
							free(sql);
							sql=NULL;
						}
						return 0;
					}
				}
				if(sql != NULL)
				{
					free(sql);
					sql=NULL;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 10:/*-----------------Ticket Type-------------------------*/
			//			tempStrLen = sizeof(ticket_type_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "ticket_type",11);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from ticket_type"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 11:/*---------------------Ticket sub Type------------------------*/
			//			tempStrLen = sizeof(ticket_sub_type_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "ticket_sub_type",15);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from ticket_sub_type"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 12:/*--------------------Bus Services------------------------------*/
			//			tempStrLen = sizeof(bus_service_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "bus_service",11);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from bus_service"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 13:/*----------------------Bus Brand-------------------------------*/
			//			tempStrLen = sizeof(bus_brand_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "bus_brand",9);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from bus_brand"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);

			}
			break;

		case 14:/*----------------------Depot-------------------------------*/
			CstcLog_printf("Inside Depot Parser function");
			//			tempStrLen = sizeof(depot_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "depot",5);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from depot"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 15:/*-----------------Route Header------------------------*/
			//			tempStrLen = sizeof(route_header_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "route_header",12);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
			if(updateDataFlag==1)
			{
				sql=(char*)malloc(sql_len*sizeof(char));
				//				sprintf(sql,"route_header where schedule_no = '%s'",global_schedule);
				sprintf(sql,"route_header where route_id = '%s'",global_Route_Id);
				rowcount = DbInterface_Get_Row_Count(sql,strlen(sql),0);//check rowcount here if (rowcount==0)do not allow to delete

				if(rowcount > 0)
				{
					//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
					if(!DbInterface_Delete_Table_active_schedule("Delete from route_header where route_id = ",global_Route_Id))
						//if(!DbInterface_Delete_Table_active_schedule("Delete from route_header where schedule_id = ",global_schedule))
						//if(!DbInterface_Delete_Table("Delete from route_header"))
					{
						//					DbInterface_transaction_commit("ROLLBACK;",0);
						//					if(tempStr!=NULL)
						//					{
						//						free(tempStr);
						//						tempStr=NULL;
						//					}
						if(sql != NULL)
						{
							free(sql);
							sql=NULL;
						}
						return 0;
					}
				}
				if(sql != NULL)
				{
					free(sql);
					sql=NULL;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 16:/*--------------------ETM RW key---------------------------*/
			//			tempStrLen = sizeof(etim_rw_keys_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "etim_rw_keys",13);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from etim_rw_keys"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 17:/*--------------------TRUNK AND FEEDER---------------------------*/
			//tempStrLen = sizeof(trunk_feeder_master_struct)+40;

			//			tempStrLen = 2+1+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)
			//			+(BUS_STOP_SEQ_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)
			//			+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "trunk_feeder_master",19);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from trunk_feeder_master"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);

			}
			break;

		case 18:/*-------------------SERVICE OP-----------------------*/
			//			tempStrLen = sizeof(service_op_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "service_op",10);

			//			tempStrLen = 3+(NAME_ENG_LEN+1)+(NAME_KAN_LEN+1)+1+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "service_op",10);
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);

			if(updateDataFlag==1)
			{
				//DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
				if(!DbInterface_Delete_Table("Delete from service_op"))
				{
					//					DbInterface_transaction_commit("ROLLBACK;",0);
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_transaction_commit("COMMIT;",0);
			}
			break;

		case 19:
			if(updateDataFlag==1)
			{
				sql=(char*)malloc(sql_len*sizeof(char));
				sprintf(sql,"rate_master where rate_id = '%s'",global_Rate_Id);
				rowcount = DbInterface_Get_Row_Count(sql,strlen(sql),0);//check rowcount here if (rowcount==0)do not allow to delete
				CstcLog_printf("rowcount = %d ", rowcount);
				if(rowcount > 0)
				{
					//if(!DbInterface_Delete_Table("Delete from rate_master"))
					if(!DbInterface_Delete_Table_active_schedule("Delete from rate_master where rate_id = ",global_Rate_Id))
					{
						if(sql != NULL)
						{
							free(sql);
							sql=NULL;
						}
						return 0;
					}
				}
				if(sql != NULL)
				{
					free(sql);
					sql=NULL;
				}
			}          //still need to discuss

			/***--------------------New Change---------------------------------------**/
			if(!MultipleScheduleFlag)
			{
				sql=(char*)malloc(sql_len*sizeof(char));

				CstcLog_printf("duty_master_struct_var.schedule_id = %s",duty_master_struct_var.schedule_id);
				memset(sql,0,sql_len);
				sprintf(sql,"schedule_master where is_flexi_fare = 'Y' and schedule_id =  '%s' ",duty_master_struct_var.schedule_id);
				flexi_fare_count = DbInterface_Get_Row_Count(sql,strlen(sql),0);

				CstcLog_printf("flexi_fare_count = %d  rowcount = %d ",flexi_fare_count,rowcount);

				if(flexi_fare_count > 0 ) //(flexi_fare_count != rowcount )
				{
					DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='0'");
				}
				else
				{
					memset(sql,0,sql_len);
					flexi_fare_count = -1;

					sprintf(sql,"route_header where is_flexi_fare = 'Y'");
					flexi_fare_count =DbInterface_Get_Row_Count(sql,strlen(sql),0);
					if(flexi_fare_count > 0 )
					{
						//sudhakar cmt
						/*if(master_status_struct_var.ticket_fare == 2)
						{
							DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'");
						}
						else
						{
							DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='0'");
						}*/
					}
					/*else
					{
						DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'");
					}*/

				}

				if(sql != NULL)
				{
					free(sql);
					sql=NULL;
				}
			}
			/***--------------------New Change---------------------------------------**/
			//DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'");
			break;

		case 23:/*-------------------LC LOGIN----------------------*/
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from lc_login"))
				{
					return 0;
				}

			}
			break;

		case 24:/*-----------------GPRS APN ---------------------*/
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from gprs_table"))
				{
					return 0;
				}

			}
			break;

		case 25:/*-----------------ROUTE_CARD ---------------------*/
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from route_card"))
				{
					return 0;
				}

			}
			break;

		case 26:/*-----------------shift_type ---------------------*/
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from shift_type"))
				{
					return 0;
				}

			}
			break;

		case 27:/*-----------------concession_type ---------------------*/
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from concession_type"))
				{
					return 0;
				}

			}
			break;

		case 28:/*-----------------activity_master ---------------------*/
				if(updateDataFlag==1)
				{
					if(!DbInterface_Delete_Table("Delete from activity_master"))
					{
						return 0;
					}

				}
				break;

		default:
			return 0;
			break;
		}
//		CstcLog_printf("reading file tempstr allocated with required memory of length = %d",(tempStrLen+1));
		//			do
		//			{
		//				fread(dbDataFile,"%c",tempStr);
		//			}while(bTemp!=EOF);
#if 0
#endif

		if(recvPktID!=8)
		{
			if (1==1) //This is for going to older system cmd code which is currently not being used.
			{
				if (stat (DB_INPUT_FILE_ADDR, & sb) == 0)
				{
					//					inputData= (unsigned char*)malloc(sb.st_size*sizeof(char));
#if COMPRESSION_DECOMPRESSION
					CstcLog_printf("................Time to insert...............");
					CstcLog_printf("Uncompressed_data = %s %d",Uncompressed_data,strlen(Uncompressed_data));

					inputData = (unsigned char *)malloc((strlen(Uncompressed_data)+strlen("@END#"))*sizeof(char));
					memset(inputData,0,strlen(Uncompressed_data)+strlen("@END#"));

					memcpy(inputData,Uncompressed_data,(strlen(Uncompressed_data)+strlen("@END#")));
					CstcLog_printf("inputData = %s",inputData);

#else
					inputData = read_whole_file (DB_INPUT_FILE_ADDR);
#endif
					CstcLog_printf("..********..........********........");
#if COMPRESSION_DECOMPRESSION
					if((sub_string=strstr((const char *)inputData,"@END#")) != NULL)
#else
						if((sub_string=strstr((const char *)inputData,";")) != NULL)

#endif
					{
#if COMPRESSION_DECOMPRESSION
							*(sub_string)='\0';

#else
						*(sub_string+1)='\0';
#endif
					}
					CstcLog_printf("inputData = %s\n",inputData);
#if COMPRESSION_DECOMPRESSION
					CstcLog_HexDump(inputData,Uncompressed_data_length);
#endif


					//					ret = ZEXPORT uncompress OF((uncompData,&uncompDataLen,(Bytef *)inputData,iTemp));
					//					CstcLog_printf("ret = %d \n%s \n%d \n%s \n%d\n",ret,uncompData,uncompDataLen,inputData,iTemp);
					//					uncompData[uncompDataLen]='\0';
					//					    if(ret!=Z_OK){
					//					    	CstcLog_printf("Error while Uncompressing\n");
					//					    }
					//
					//						  if(ret == Z_BUF_ERROR){
					//							  CstcLog_printf("Buffer was too small!\n");
					//						    return 1;
					//						  }
					//						  if(ret ==  Z_MEM_ERROR){
					//							  CstcLog_printf("Not enough memory for compression!\n");
					//						    return 2;
					//						  }
					//						  CstcLog_printf_2("uncompData=%s\n uncompDataLen = %d\n",uncompData,strlen((char *)uncompData));

					CstcLog_printf("Query to be executed:: %s ", inputData);

					if(recvPktID==1)
					{
						CstcLog_printf("waybill query fetched from file ");
						//if((waybill_sub_str1=strstr((const char *)inputData,"\'")) != NULL)
						if((waybill_sub_str1=strstr((const char *)inputData,"\'")) != NULL)
						{
							CstcLog_printf("waybill query search 1 ");
							if((waybill_sub_str2=strstr((const char *)(waybill_sub_str1+1),"\'")) != NULL)
							{
								CstcLog_printf("waybill query search 2 ");
								if((waybill_sub_str2-(waybill_sub_str1+1))<WAYBILL_NO_LEN)
								{
									CstcLog_printf("waybill is Invalid:: %s ", waybill_sub_str1);
									*(waybill_sub_str2+1)='\0';
								}
								CstcLog_printf("waybill_sub_str length:: %u-(%u+1)=%u ", waybill_sub_str2,waybill_sub_str1,(waybill_sub_str2-(waybill_sub_str1+1)));
							}
						}
					}
					//					sql=(char*)malloc(sql_len*sizeof(char)*strlen((char *)uncompData));
					//
					//					sprintf(sql,"%s %s",QArr,uncompData);
					//
					//					dbretval=0;
					//					dbretval=DbInterface_exec_sql((unsigned char *)sql,Err_Msg,&Err_Len);
					dbretval=0;
					dbretval=DbInterface_exec_sql((unsigned char *)inputData,Err_Msg,&Err_Len);
					//					if (Err_Len > 0)
					if (dbretval!=1)
					{
						memset(sysCmdStr,0,100);
						sprintf(sysCmdStr,"cp %s /home/user0/CSTC/err%d.txt", DB_INPUT_FILE_ADDR, recvPktID);
						system(sysCmdStr);
						//dbretval = 1;
						//						CstcLog_printf("exec sql \"%s\" ", Err_Msg);
					}
					if(inputData != NULL)
					{
						free (inputData);
						inputData=NULL;
					}
					/*if(uncompData != NULL)
					{
						free (uncompData);
						uncompData=NULL;
					}*/
				}
			}
			else
			{
				//			sleep(5);
				memset(tempStr,0,100);
				sprintf(tempStr,"chmod -R 777 %s",DB_INPUT_FILE_ADDR);
				CstcLog_printf("System chmod command \"%s\" ", tempStr);
				dbretval=system(tempStr);
				CstcLog_printf("System chmod command retval \"%d\" ", dbretval);
				memset(sysCmdStr,0,100);
				sprintf(sysCmdStr,"sqlite3 %s < %s",DB_ADDR,DB_INPUT_FILE_ADDR);
				//						memcpy(tempStr,"sqlite3 ",strlen("sqlite3 "));
				//						memcpy(tempStr+strlen("sqlite3 "),DB_ADDR,strlen(DB_ADDR));
				//						memcpy(tempStr+strlen("sqlite3 ")+strlen(DB_ADDR),"sqlite3 ",strlen("sqlite3 "));
				CstcLog_printf("Insertion in table command \"%s\" failed", sysCmdStr);


				//			if(recvPktID == 9)
				//			{
				//				sleep(4);
				//			}
				//			else
				//			{
				//				sleep(1);
				//			}
				//			sprintf(Err_Msg,"%02d",SQLITE_DB_INSERTION_FAILED);
				//			strcat(Err_Msg1,Err_Msg);
				//			Err_Len=Err_Len+strlen(Err_Msg1);
				//			CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_Msg1);

				while(!system(NULL)){
					CstcLog_printf("Shell is unavailable");
					sleep(2);
				}
				sleep(2);
				dbretval=0;
				dbretval=system(sysCmdStr);//DbInterface_Insert_In_All_Tables1(tempName, tempStr,recLen);
				retrycnt =0;
				//			while(retrycnt<5)
				//			{
				//				dbretval=system(tempStr);
				CstcLog_printf("System command retval \"%d\" ", dbretval);
			}
			//				if(dbretval==0)
			//					break;
			//				retrycnt++;
			//
			//			}
			//			if(dbretval == 0)
			if(dbretval == 1)
			{
				CstcLog_printf("Insertion in table  success");

				sprintf(Err_Msg,"%02d",SUCCESS);

				Err_Len=Err_Len+2;//strlen(Err_Msg);
				CstcLog_printf("Err_Msg = %s Err_Len = %d",Err_Msg,Err_Len);
				Err_Msg[Err_Len]='\0';
				memset(Err_Msg,0,ERROR_MSG_BUFFER_LEN);            /*May be need to change...*/
				Err_Len=0;
				//memcpy(Err_Msg,"Data Insertion successful",25);
				//				strcat(Err_Msg,"Data Insertion successful");
				//				Err_Len =Err_Len+strlen("Data Insertion successful");
				CstcLog_printf("Err_Len =%d ",Err_Len);
				//				DbInterface_transaction_commit("COMMIT;",0);

				if(recvPktID == 5)
				{
					DbInterface_Delete_Table("Delete from duty_master_restore");
				}
				return 1;
			}
			else
			{
				CstcLog_printf("Insertion in table  failed");
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",SQLITE_DB_INSERTION_FAILED);
				strcat(Err_Msg,Err_MsgAry);
				Err_Len=Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("Err_Len = %d Err_Msg =%s Err_Msg1 = %s",Err_Len,Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				//				strcat(Err_Msg,"Data Insertion in table failed");
				//				Err_Len =Err_Len+strlen("Data Insertion in table failed");
				//				CstcLog_printf("Err_Len =%d Err_Msg =%s",Err_Len,Err_Msg);

				//							break;
				//				DbInterface_transaction_commit("ROLLBACK;",0);
				if(recvPktID == 5)
				{
					DbInterface_Delete_Table("Delete from duty_master");
					if(reinsert_duty_master_restore_in_duty_master())
						DbInterface_Delete_Table("Delete from duty_master_restore");
				}
				return 0;
			}
		}
	}
	else
	{
		CstcLog_printf("Parsing Fail Resend Packet");
		sprintf(Err_MsgAry,"%02d",RESEND_PACKETS);
		strcat(Err_Msg,Err_MsgAry);
		Err_Len=Err_Len+2;//strlen(Err_MsgAry);
		CstcLog_printf("Err_Len = %d Err_Msg =%s Err_MsgAry = %s",Err_Len,Err_Msg,Err_MsgAry);

		return 0;
	}

	return 1;
}

unsigned get_file_size (const char * file_name)
{
	struct stat sb;
	if (stat (file_name, & sb) != 0) {
		fprintf (stderr, "'stat' failed for '%s': %s.\n",
				file_name, strerror (errno));
		exit (EXIT_FAILURE);
	}
	CstcLog_printf("In get file size %s",file_name);
	return sb.st_size;
}

/* This routine reads the entire file into memory. */

unsigned char* read_whole_file (const char * file_name)
{
	unsigned s;
	unsigned char * contents;
	FILE * f;
	size_t bytes_read;
	int status;

	s = get_file_size (file_name);
	contents = malloc (s + 1);
	if (! contents) {
		CstcLog_printf ( "Not enough memory.\n");
		exit (EXIT_FAILURE);
	}

	f = fopen (file_name, "r");
	if (! f) {
		CstcLog_printf ( "Could not open '%s': %s.\n", file_name,
				strerror (errno));
		exit (EXIT_FAILURE);
	}
	bytes_read = fread (contents, sizeof (unsigned char), s, f);
	CstcLog_printf ("bytes_read = %u",bytes_read);
	if (bytes_read != s) {
		CstcLog_printf ( "Short read of '%s': expected %d bytes "
				"but got %d: %s.\n", file_name, s, bytes_read,
				strerror (errno));
		exit (EXIT_FAILURE);
	}
	fflush(f);
	status = fclose (f);
	f = NULL;
	if (status != 0) {
		CstcLog_printf ( "Error closing '%s': %s.\n", file_name,
				strerror (errno));
		exit (EXIT_FAILURE);
	}
	//    contents[(Total_packet_length+33-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+CHECKSUM_LEN))]='\0';
	contents[bytes_read]='\0';
	return contents;
}


int my_system (const char *command)
{
	int status;
	pid_t pid;

	pid = fork ();
	if (pid == 0)
	{
		/* This is the child process.  Execute the shell command. */
		execl (SHELL, SHELL, "-c", command, NULL);
		_exit (EXIT_FAILURE);
	}
	else if (pid < 0)
		/* The fork failed.  Report failure.  */
		status = -1;
	else
		/* This is the parent process.  Wait for the child to complete.  */
		if (waitpid (pid, &status, 0) != pid)
			status = -1;
	return status;
}



void UART_get_machine_info(char * sn, int nbytes)
{
	CstcLog_printf("In UART_get_machine_info");
	sys_get_sn(sn, nbytes);
	CstcLog_printf("%s", sn);
}

int UART_check_master_status()
{
	int checkFlag = 0;
	int resetonce = 0;

	//	{
	if(master_status_struct_var.machine_register == 0)
	{
		checkFlag = 1;
		Show_Error_Msg_UART("    MACHINE NOT    \n    REGISTERED     ");
		//                goto tryagain;
		return 0;
	}
	if(master_status_struct_var.machine_register == 1 /* && (!machineRegisterFlag)*/)
	{
		Show_Error_Msg_UART("   REGISTERING ETM  \n    PLEASE WAIT..!   ");
		master_status_struct_var.machineRegisterFlag = 1;
		//usleep(100);
		//                goto tryagain;
		return 0;
	}
	if((master_status_struct_var.machine_register == 2) && (master_status_struct_var.machineRegisterFlag))
	{
		Show_Error_Msg_UART("   REGISTERING ETM  \n       COMPLETE     ");
		master_status_struct_var.machineRegisterFlag = 0;
		beep(2800,1000);
		//usleep(100);
		//                goto tryagain;
		return 0;
	}

	/*--------------------- insert waybill-------------------------------*/
	if(master_status_struct_var.insert_waybill == 0)
	{
		checkFlag = 1;
		Show_Error_Msg_UART(" NO WAYBILL IN ETM ");
		//                goto tryagain;
		return 0;
	}
	if(master_status_struct_var.insert_waybill == 1/* && (!insertWaybillFlag)*/)
	{
		Show_Error_Msg_UART("  INSERTING WAYBILL \n     PLEASE WAIT!   ");
		master_status_struct_var.insertWaybillFlag = 1;
		//usleep(100);
		//                goto tryagain;
		return 0;
	}
	if((master_status_struct_var.insert_waybill == 2) && (master_status_struct_var.insertWaybillFlag))
	{
		Show_Error_Msg_UART("NEW WAYBILL RECEIVED");
		master_status_struct_var.insertWaybillFlag = 0;
		beep(2800,1000);
		//usleep(100);
		if(!resetonce){
			get_waybill_details();
			set_waybill_time();
			get_schedule_type_info();        // New Change

			resetonce =1;
		}
		//			goto tryagain;
		return 0;
	}
	/*-----------------------------------------------------------------*/
	/*--------------------- insert route-------------------------------*/
	/*	if(master_status_struct_var.insert_route == 0)
	{
		checkFlag = 1;
		Show_Error_Msg_UART(" NO ROUTE IN ETM ");
		return 0;
	}
	if(master_status_struct_var.insert_route == 1)
	{
		Show_Error_Msg_UART("  INSERTING ROUTE \n     PLEASE WAIT!   ");
		master_status_struct_var.insertrouteFlag = 1;
		return 0;
	}
	if((master_status_struct_var.insert_route == 2) && (master_status_struct_var.insertrouteFlag))
	{
		Show_Error_Msg_UART("  UPLOADING ROUTE \n       COMPLETE     ");
		master_status_struct_var.insertrouteFlag = 0;
		beep(2800,1000);
		return 0;
	}*/

	/*-----------------------------------------------------------------*/
	/*---------------------upload master-------------------------------*/


#if 0 // sudhakar cmmt
	if(master_status_struct_var.upload_master == 0)
	{
		checkFlag = 1;
		Show_Error_Msg_UART("   NO MASTER DATA   ");
		//                goto tryagain;
		return 0;
	}
	if(master_status_struct_var.upload_master == 1 /*&& (!uploadMasterFlag)*/)
	{
		Show_Error_Msg_UART("  UPLOADING MASTER  \n     PLEASE WAIT!   ");
		master_status_struct_var.uploadMasterFlag = 1;
		//usleep(100);
		//                goto tryagain;
		return 0;
	}
	if((master_status_struct_var.upload_master == 2) && (master_status_struct_var.uploadMasterFlag))
	{
		Show_Error_Msg_UART("  UPLOADING MASTER  \n       COMPLETE     ");
		master_status_struct_var.uploadMasterFlag = 0;
		beep(2800,1000);
		//usleep(100);

		//                goto tryagain;
		return 0;
	}

	/*-----------------------------------------------------------------*/
	/*---------------------Ticket Fare--------------------------------*/
	if(master_status_struct_var.ticket_fare == 0)
	{
		checkFlag = 1;
		Show_Error_Msg_UART("   NO FARE CHART DATA   ");
		//                goto tryagain;
		return 0;
	}
	if(master_status_struct_var.ticket_fare == 1 /*&& (!uploadMasterFlag)*/)
	{
		Show_Error_Msg_UART("  UPLOADING FARE CHART  \n     PLEASE WAIT!   ");
		master_status_struct_var.ticketFareFlag = 1;
		//usleep(100);
		//                goto tryagain;
		return 0;
	}
	if((master_status_struct_var.ticket_fare == 2) && (master_status_struct_var.ticketFareFlag))
	{
		Show_Error_Msg_UART("  UPLOADING FARE CHART  \n       COMPLETE     ");
		master_status_struct_var.ticketFareFlag = 0;
		beep(2800,1000);
		//usleep(100);

		//                goto tryagain;
		return 0;
	}
#endif
	if(master_status_struct_var.start_duty == 0)
	{
		checkFlag = 1;
		Show_Error_Msg_UART("  DUTY NOT STARTED  ");
		//                goto tryagain;
		return 0;
	}
	if(master_status_struct_var.start_duty == 1/* UART_check_master_status&& (!startDutyFlag)*/)
	{

		Show_Error_Msg_UART("   ALLOCATING DUTY  \n     PLEASE WAIT    ");
		master_status_struct_var.startDutyFlag = 1;
		//usleep(100);
		//                goto tryagain;
		return 0;
	}
	if((master_status_struct_var.start_duty == 2) && (master_status_struct_var.startDutyFlag))
	{
		Show_Error_Msg_UART("  DUTY ALLOCATION  \n     COMPLETE      ");
		master_status_struct_var.startDutyFlag = 0;
		beep(2800,1000);
		//usleep(100);

		//                goto tryagain;
		get_schedule_type_info();
		all_flags_struct_var.waybill_upload_Flag= 1;
		return 1;
	}
	if(master_status_struct_var.download_ticket == 0)
	{
		checkFlag = 1;
		Show_Error_Msg_UART("  DOWNLOAD DATA  ");
		//			usleep(100);
		//                goto tryagain;
		return 0;
	}

	if((master_status_struct_var.download_ticket == 1) /*&& (!master_status_struct_var.downloadTicketFlag)*/)
	{
		Show_Error_Msg_UART("  DOWNLOADING DATA  \n    PLEASE WAIT     ");
		master_status_struct_var.downloadTicketFlag = 1;
		//usleep(100);
		//                goto tryagain;
		return 0;
	}
	if((master_status_struct_var.download_ticket == 2) && (master_status_struct_var.downloadTicketFlag))
	{

		Show_Error_Msg_UART("  DOWNLOADING DATA  \n      COMPLETE      ");
		master_status_struct_var.downloadTicketFlag = 0;
		//usleep(100);
		beep(2800,1000);
		//                goto tryagain;
		return 1;
		//			return 0;
	}

	if(master_status_struct_var.erase_ticket == 0)
	{
		checkFlag = 1;
		Show_Error_Msg_UART("  PLEASE ERASE \n MACHINE DATA  ");
		//			usleep(100);
		//                goto tryagain;
		return 0;
	}
	if((master_status_struct_var.erase_ticket == 1) /*&& (!master_status_struct_var.eraseTicketFlag)*/)
	{
		Show_Error_Msg_UART("    ERASING DATA    \n    PLEASE WAIT...!   ");
		master_status_struct_var.eraseTicketFlag = 1;
		resetonce =0;
		//usleep(100);
		//                goto tryagain;
		return 0;
	}
	if((master_status_struct_var.erase_ticket == 2) && (master_status_struct_var.eraseTicketFlag))
	{
		Show_Error_Msg_UART("     ERASING DATA   \n       COMPLETE     ");
		master_status_struct_var.eraseTicketFlag = 0;
		beep(2800,1000);
		resetonce =0;

		DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
		if(!DbInterface_Delete_Table("Delete from master_status"))
		{
			DbInterface_transaction_commit("ROLLBACK;",0);
			return 0;
		}
		//DbInterface_transaction_commit("COMMIT;");

		if(gUpdateDataFlag)
		{
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
			if(!DbInterface_Insert_In_All_Tables1("master_status","'0','0','2','0','0','0','0','0'",31))
			{
				DbInterface_transaction_commit("ROLLBACK;",0);
				return 0;
			}
			//			DbInterface_transaction_commit("COMMIT;",0);
		}
		else
		{
			//			DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
			if(!DbInterface_Insert_In_All_Tables1("master_status","'2','0','2','2','0','0','0','2'",31))
			{
				DbInterface_transaction_commit("ROLLBACK;",0);
				return 0;
			}
			//			DbInterface_transaction_commit("COMMIT;",0);
		}
		DbInterface_transaction_commit("COMMIT;",0);
		//usleep(100);
		//                goto tryagain;
		return 1;
	}
	//	}
	return 0;
}

void memset_packet_format(void)
{
	memset(&(packet_format_struct_var.start_indicator), 0,START_INDICATOR_LEN);
	memset(packet_format_struct_var.data_length,0,DATA_LENGTH_LEN);
	//		if(packet_format_struct_var.data!=NULL)
	//			free(packet_format_struct_var.data);
	//		packet_format_struct_var.data=NULL;
	memset(packet_format_struct_var.checksum,0,CHECKSUM_LEN);
}

void remove_stars(const char *input, char *result)
{
	int i, j = 0;
	for (i = 0; input[i] != '\0'; i++)
	{
		if (input[i] != '*')
		{
			CstcLog_printf("In remove =%d\n",i);
			result[j++] = input[i];
		}
	}
	CstcLog_printf("result = %s \n",result);
	result[j] = '\0';
}

void remove_space(const char *input, char *result_waybill)
{
	int i, j = 0;
	for (i = 0; input[i] != '\0'; i++)
	{
		if(!isspace((unsigned char)input[i]))
		{
			CstcLog_printf("In remove =%d\n",i);
			result_waybill[j++] = input[i];
		}
	}
	CstcLog_printf("result_waybill = %s \n",result_waybill);
	result_waybill[j] = '\0';
}


int wc(char* file_path, char* word){
	FILE *fp;
	int count = 0;
	int ch, len;

	if(NULL==(fp=fopen(DB_INPUT_FILE_ADDR, "r")))
		return -1;
	len = strlen(word);
	for(;;){
		int i;
		if(EOF==(ch=fgetc(fp))) break;
		if((char)ch != *word) continue;
		for(i=1;i<len;++i){
			if(EOF==(ch = fgetc(fp))) goto end;
			if((char)ch != word[i]){
				fseek(fp, 1-i, SEEK_CUR);
				goto next;
			}
		}
		++count;
		next: ;
	}
	end:
	fflush(fp);
	fclose(fp);
	fp =NULL;
	return count;
}

uint32_t rc_crc32(uint32_t crc, const char *buf, size_t len)
{
	static uint32_t table[256];
	static int have_table = 0;
	uint32_t rem;
	uint8_t octet;
	int i, j;
	const char *p, *q;

	/* This check is not thread safe; there is no mutex. */
	if (have_table == 0) {
		/* Calculate CRC table. */
		for (i = 0; i < 256; i++) {
			rem = i;  /* remainder from polynomial division */
			for (j = 0; j < 8; j++) {
				if (rem & 1) {
					rem >>= 1;
					rem ^=0xedb88320;// 0x12345678;//0xFFFFFFFF;//0xedb88320;
				} else
					rem >>= 1;
			}
			table[i] = rem;
		}
		have_table = 1;
	}

	crc = ~crc;
	q = buf + len;
	for (p = buf; p < q; p++) {
		octet = *p;  /* Cast to unsigned octet. */
		crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
	}
	return ~crc;
}

int decompression(char *source,int sourceLen)
{
	int ret = -1,iTemp = 0;
	char *Temp_Uncom_Data = NULL;

//	Uncompressed_data = (char *)malloc((Uncompressed_data_length+strlen(DB_Insert_str)+2)*sizeof(char));
//	memset(Uncompressed_data,0,(Uncompressed_data_length+strlen(DB_Insert_str)+2));

	Uncompressed_data = (char *)malloc((Uncompressed_data_length+strlen(DB_Insert_str)+strlen("@END#"))*sizeof(char));
	memset(Uncompressed_data,0,(Uncompressed_data_length+strlen(DB_Insert_str)+strlen("@END#")));
	memcpy(Uncompressed_data,DB_Insert_str,strlen(DB_Insert_str));
	iTemp = strlen(DB_Insert_str) ;
	Temp_Uncom_Data = ( Uncompressed_data+iTemp) ;
	CstcLog_printf("source = %s Uncompressed_data_length =%d sourceLen =%d",source,(Uncompressed_data_length+strlen(DB_Insert_str)+2),sourceLen);
	CstcLog_HexDump(source,sourceLen);

	CstcLog_printf("Uncompressed_data = %s",Uncompressed_data);
//	ret = ZEXPORT uncompress OF(((Bytef *)(Uncompressed_data+iTemp),&Uncompressed_data_length,(Bytef *)source,sourceLen));
	ret = ZEXPORT uncompress OF(((Bytef *)(Temp_Uncom_Data),&Uncompressed_data_length,(Bytef *)source,sourceLen));
	CstcLog_HexDump(Temp_Uncom_Data,Uncompressed_data_length);

	CstcLog_printf("ret = %d \nUncompressed_data = %s \nUncompressed_data_length = %d ",ret,(Uncompressed_data+iTemp),Uncompressed_data_length);
//	Uncompressed_data[Uncompressed_data_length]='\0';
	if(ret!=Z_OK){
		CstcLog_printf("Error while Uncompressing\n");
	}

	if(ret == Z_BUF_ERROR){
		CstcLog_printf("Buffer was too small!\n");
		//return 1;
	}
	if(ret ==  Z_MEM_ERROR){
		CstcLog_printf("Not enough memory for compression!\n");
		//return 2;
	}
	CstcLog_printf_2("uncompData=%s",Uncompressed_data);

	memcpy((Uncompressed_data+Uncompressed_data_length+iTemp),"@END#",strlen("@END#"));
	//Uncompressed_data[Uncompressed_data_length+iTemp]='END';
	Uncompressed_data[Uncompressed_data_length+iTemp+strlen("@END#")]='\0';
	CstcLog_printf("Uncompressed_data = %s",Uncompressed_data);
	CstcLog_HexDump(Uncompressed_data,(Uncompressed_data_length+iTemp+strlen("@END#")));
	//						fprintf(dbDataFile,"%s;",Uncompressed_data);//dbTemp

	return 0;
}


//**************************************************************************************************
#if 0
#endif
#if 0
#endif



