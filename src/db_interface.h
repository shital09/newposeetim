/*
 * db_interface.h
 *
 *  Created on: 09-Apr-2014
 *      Author: root
 */

#ifndef DB_INTERFACE_H_
#define DB_INTERFACE_H_
#include "cstc_defines.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include "sqlite3.h"
#include "lcd_drvr.h"
#include "cstclog.h"
#include <malloc.h>

#define SDK_OK                       (1)
#define SDK_EQU                      (0)        //比较大小相等时用这个
#define SDK_ERR                      (-1)
#define SDK_TIME_OUT                 (-2)       //超时
#define SDK_ESC                      (-3)
#define SDK_PARA_ERR                 (-4)

#define   SDK_SQL_ERR   (-102)

int DbInterface_Init(void);
int DbInterface_Create_Db_File(void);
int DbInterface_Create_Database(char* DB_dir);
#if 0
int DbInterface_Open_Database(char* DB_dir);
int DbInterface_Close_Database(void);
#else
int DbInterface_Open_Database(char* DB_dir,int db_flag);
int DbInterface_Close_Database(int db_flag);
#endif


#if 0
int Close_db(void);
#endif
int DbInterface_Create_thread_Database(char* DB_dir);
int DbInterface_Create_thread_Transaction_Table(char *sql);

int DbInterface_Create_Table(char *sql);
int DbInterface_Create_All_Tables(void);
int DbInterface_Create_Tranxn_Table(void);

int DbInterface_Insert_Data(char* tableName,char* columnNames,const char *dataValues, int DataLength);
int DbInterface_Insert_In_All_Tables(char* tableName,const char *dataValues);
int DbInterface_Update_Master_Status_Data(char* sql);
int DbInterface_Insert_In_Waybill_Tables(void);
int DbInterface_Insert_In_Schedule_Tables(void);
int DbInterface_Insert_In_Duty_Tables(void);
int DbInterface_Insert_In_Route_Tables(void);
int DbInterface_Insert_In_Fare_Tables(void);
int DbInterface_Insert_In_Transaction_Tables(void);
//int DbInterface_Delete_From_All_Tables(int updateDataFlag);
int DbInterface_Update_Data(const char *PAscOldCardbin , const char *pAscNewCardbin);
int DbInterface_Delete_Table(char *sql);
int DbInterface_Insert_In_All_Tables1(char* tableName,const char *dataValues, int tempDataLen);

int get_waybill_details(void);
int get_waybill_wise_info(void);
int get_service_op_details(void);
int get_selected_trip_info(void);

int scheduled_trip_selection(current_selected_trip_stops_struct *current_selected_trip_stops_struct_var);
int select_scheduled_trip(selected_trip_info_struct *temp_selected_trip_info_struct_var);

int select_route_for_scheduled_trip(selected_trip_info_struct *temp_selected_trip_info_struct_var);
int update_newly_selected_trip_info(selected_trip_info_struct *temp_selected_trip_info_struct_var);
int create_trip(selected_trip_info_struct *temp_selected_trip_info_struct_var);
int update_ticket_flag(void);

int check_for_amount_locking(void);
int update_waybill_wise_info_details(int rollBackFlag);

int update_trip_details(void);
int get_current_trip_details(trip_details_struct *tripDetailsBoarding, trip_details_struct *tripDetailsAlighting);
int update_current_trip_values(trip_details_struct *tripDetailsBoarding, trip_details_struct *tripDetailsAlighting);
int update_current_trip_details(trip_details_struct *tripDetailsBoarding, trip_details_struct *tripDetailsAlighting);
int calculate_passenger(void);
int assign_values_to_transaction_struct(int stage_count);
int update_transaction_log(void);
int get_stops_info(char* selected_stop_seq_no_qstr, int inspectorNormalModeInt);

int lc_login_stuct_clear(lc_login_struct *lc_login_struct_var_clear);
int waybill_master_struct_clear(waybill_master_struct *waybill_master_struct_var_clear);//int waybill_master_struct_clear(void);
int schedule_master_struct_clear(schedule_master_struct *schedule_master_struct_var_clear);
int duty_master_struct_clear(duty_master_struct *duty_master_struct_var_clear);
int route_master_struct_clear(route_master_struct *route_master_struct_var_clear);
int fare_chart_struct_clear(fare_chart_struct *fare_chart_struct_var_clear);
int transaction_log_struct_clear(transaction_log_struct *transaction_log_struct_var_clear);
int shift_type_struct_clear(shift_type_struct *shift_type_struct_var_clear);
int route_card_struct_clear(route_card_struct *route_card_struct_var_clear);
int waybill_wise_info_struct_clear(waybill_wise_info_struct *waybill_wise_info_struct_var_clear);
int trip_details_struct_clear(trip_details_struct *trip_details_struct_var_clear);
int get_selected_trip_info_struct_clear(selected_trip_info_struct *selected_trip_info_struct_var_clear);
int current_passengers_details_struct_clear(current_passengers_details_struct *current_passengers_details_struct_var_clear);
#if 0
int DbInterface_Get_Row_Count(char* partStmt,int partStmtLen);
#else
int DbInterface_Get_Row_Count(char* partStmt,int partStmtLen,int db_flag);

#endif

int get_sequence_number(int stage_number);
int DbInterface_Get_first_seq_number(char* partStmt,int partStmtLen);
int view_last_ticket_details(void);

int update_changed_start_stage(int selectedStartStageCountInt);
int last_ticket(void);
int collection_report(void);
int DbInterface_Insert_In_All_Tables1(char* tableName,const char *dataValues, int tempDataLen);
int cancel_trip_full(void);
int cancel_trip_partial(void);

int view_last_ticket_details(void);
int show_ticket(void);

int line_checking_status_report(void);

int end_trip(void);
int DbInterface_Get_Master_Status_Data(int* ticket_fare, int* download_ticket, int* machine_register, int* upload_master, int* erase_ticket, int* start_duty, int* insert_waybill/*, int* insert_route*/);
int repeat_ticket(void);
int current_trip_report(void);
int vehicle_exchange_ticket_report(void);
int all_trip_details(int reportFlag);
int view_ticket(void);

int selected_route_schedule_details(void);
int book_case_report(int caseFlag);
int other_case_report(int caseFlag);
int get_not_printed_data(char *ticket_date, char* ticket_time, char * total_ticket, char *ticket_no);

int selected_route_details(void);
int inspector_report(void);

int end_duty(void);

int not_printed_tkt(void);
int selected_route_fare_chart(void);
int selected_route_flexi_chart(void);

int penalty_ticket(int penaltyFlag);
int inspection_report(void);
int line_checking_status_report(void);
int stage_details(void);
int get_fare_data(int LC_Flag, int *stage_count);
int get_flexi_fare_data(int LC_Flag, int *stage_count);

int get_previous_stops(void);
int get_stops_reports(char* seq_no, char * stop_name,char *sub_stage, int j, char *distance);

int get_adult_fare(char *adultfare, int from_seq_no, int till_seq_no);
int get_flexi_adult_fare(char *adultfare, int from_seq_no, int till_seq_no);

int get_ticket_type(ticket_type_struct *ticket_type_struct_var, int type_id);
int get_ticket_sub_type(ticket_sub_type_struct *ticket_sub_type_struct_var,int sub_type_id);

//-----------------------------------------------
int get_transaction_log_data(int Transaction_no,char * pkt_ptr);
int get_start_end_db_data(char *start_tkt_cnt,char *end_tkt_cnt);

int gprs_get_transaction_log_data(transaction_log_struct *gprs_transaction_log_struct_var,int Transaction_No);
int get_gprs_start_end_db_data(char *start_tkt_cnt,char *end_tkt_cnt);
int update_upload_flag(int till_txn_no, int tkt_flag);

int get_service_op_details_header(char *hdrmsg_1_eng,char *hdrmsg_1_kan,char *hdrmsg_1_pflag);
//int get_service_op_details_footer(char *ftrmsg_1_eng,char *ftrmsg_1_kan,char *ftrmsg_1_pflag,char *ftrmsg_2_eng,char *ftrmsg_2_kan,char *ftrmsg_2_pflag,char *ftrmsg_3_eng,char *ftrmsg_3_kan ,char *ftrmsg_3_pflag);

int get_service_op_details_footer(service_op_struct *service_op_struct_var);


int get_stages(current_selected_trip_stops_struct *stage_current_selected_trip_stops_struct_var, int *rowCount);
int get_schedule_type_info(void);

void free_current_trip_details_ptrs(char *sql,char * val,char * startTicketNo,char * endTicketNo,char * totalPxcount,char * adultcnt,char * studentCnt,char *adultamt,char * studentAmt,char * tollAmt);
//int create_erase_rollback_db_savepoint(int toDoFlag);

int rollback_from_transaction_log(void);
int rollback_from_trip_details(void);

int DbInterface_Open_Database_thread(char* DB_dir);//,int GPRS_BREAK);
int DbInterface_Get_Row_Count_thread(char* partStmt,int partStmtLen);//,int GPRS_BREAK);
int DbInterface_Close_Database_thread(void);

int get_registration_log(void);
int get_key_function(int sector_id);
int update_manual_duty_flag(void);
int conductor_pass_registration_struct_clear(conductor_pass_registration_struct *conductor_pass_registration_struct_var);
int all_flags_struct_clear(all_flags_struct *all_flags_struct_var);
int assign_values_to_transaction_struct_LC(void);
//int DbInterface_Insert_In_Transaction_Tables_LC(void);
int DbInterface_Insert_In_Transaction_Tables_LC(char *pass_no);

int update_report_flag(void);

int all_trip_status_report(void);
int get_all_schedule(void);
int change_active_schedule(int indx, char *username);

int get_duty_master_data(duty_master_struct *pduty_master_struct_var);

void free_all_trip_details_ptrs(char *sql,char *mintripNo,char *maxtripNo,char *temptrip,char *startTicketNo,char *endTicketNo,char *TripStartDt,char *TripStartTm,char *totalPxamt/*,char *totalPxcnt*/,char *adultcnt,char *childcnt,char *adultamt,char *childamt,char *tollamt,char *pass_tkt_count,char *cancelled_cnt,char *cancelled_amt,char *concession_cnt,char *concession_amt,char *e_purse_cnt,char *e_purse_amt);
int free_inspector_report_ptrs(char *sql,char* chkstg,char* totalPxamt,char* totalPxcnt,char* adultcnt,char* childcnt,char* srcitizencnt,char* luggcnt,char* adultamt,char* childamt,char* srcitizenamt,char* luggamt,char* adultcntPen,char* childcntPen,char* srcitizencntPen,char* luggcntPen,char* adultamtPen,char* childamtPen,char* srcitizenamtPen,char* luggamtPen,char* startTicketNo,char* endTicketNo ,char *totalPxamtPen);//,int penaltyFlag);
int gprs_minmax_txn(char *max_tkt_cnt, char *min_tkt_cnt, int tkt_flag);

int copy_data_from_regular_to_thread_db(char *start_tkt_cnt);
int get_gprs_max_tkt_cnt_thread_db(char *max_tkt_cnt_thread);

int DbInterface_Delete_Table_active_schedule(char *sql,const char *global_schedule);
int DbInterface_Delete_From_All_Tables(int updateDataFlag,char *global_schedule);
int duty_master_schedule_check(char *schedule);
int insert_duty_master_in_restore(void);
int reinsert_duty_master_restore_in_duty_master(void);

int DbInterface_exec_sql(unsigned char *sql, char* ErrMsg, int* ErrLen);
#if 0
int DbInterface_transaction_commit(char *sql);
int DbInterface_transaction_begin(char *sql);
#else

int DbInterface_transaction_begin(char *sql,int db_flag);
int DbInterface_transaction_commit(char *sql,int db_flag);
#endif

int get_flexi_fare_route_card(int seq_no, char* fare);
int gprs_get_txn(void);

int get_route_id_from_route_header(char *Route_Id);
int get_route_id_from_route_master(char *Route_Id);

int trip_status_check(unsigned int search_trip_id,char *route_id,char* search_Shift);
int get_luggage_pass_tkt_count(char *lugg_tkt_count,char *pass_tkt_count);
int gprs_get_schedule_data(char *trip_id, char *schedule_no,char shift,char *stage_id);
int get_current_stage_GPRS(char *current_stage_id);
int get_Ip(char *HOST_IP,char *FIELD_Name);
int get_waybill_string(char * databuff);
int assign_values_to_transaction_struct_Trip(void);
int DbInterface_Insert_In_Transaction_Trip(void);
int check_lc_login(char *username,char *password);
int vehicle_ex_reason(void);
int vehicle_ex_cancel(void);
int trip_wise_ticket_details(void);
int get_total_count( char *ticket_no, char *full_count, char *half_count,char *lugg_count);
int stage_for_status_report(char *from_seq_no,char *till_seq_no,char *from_stage,char *till_stage);
int alter_trip_details(void);
int get_flexi_fare_show_ticket(void);
int get_fare_data_for_show_ticket(void);
int get_lc_name(void);
//int extract_mismatched_ids(char Selection,char route_id[][100],char rate_id[][100], int noUpdateFlag);
int extract_mismatched_ids(char Selection,char route_id[][100],char rate_id[][100], int noUpdateFlag,int index);

int check_schedule(int Selection);
int DbInterface_Get_Table_columns(char *sql);
//int insert_schedule_master_in_restore(void);
//int reinsert_schedule_master_restore_in_schedule_master(void);
int Insert_restore_table_in_main_table(char* sql);
int update_schedule_master_restore_with_trip_performed(void);
int get_depot_details(depot_struct *depot_struct_var);
int get_transaction_no_LC(void);
int getUpdateDateTime(char *updateDate, char *updateTime,int selection,char *schedule_id,char *selected_id);
int DbInterface_distinct_rc(char *sql);

int gprs_get_tkt_data(char * data_buff, char * last_tkt);
int get_schedule_data_for_upload(char * data_buff,char * last_txn);

int get_apn_details(char *service_provider);
void clear_apn_struct(void);
int create_extra_trip(void);
int get_all_trips(void);
//int select_shift(schedule_master_struct * schedule_master_struct_ptr);
int get_card_details(schedule_master_struct *schedule_master_struct_var);
int get_route_details(schedule_master_struct *schedule_master_struct_var);
int select_shift(void);
int get_trip_id(schedule_master_struct *schedule_master_struct_var);
int get_start_date_time(schedule_master_struct *schedule_master_struct_var);
int refund_ticket(void);
int cancel_ticket(void);
int concession_ticket(void);
int get_all_concession_types(void);
int toll_function(void);
int Insert_toll_details_in_transaction_table(char *receipt_no,char *toll_amt);
int get_fare_type(schedule_master_struct *schedule_master_struct_var);
int get_toll_details(char *receipt_no,char* toll_amt);
int record_activity_log(int actKey);
int activity_log_function(void);
int get_all_ids(char route_id[][100],char rate_id[][100],char fare_chart_id[][100],int id_flag);
int update_trip_details_count(void);
int update_tkt_type_n_tkt_sub_type(void);
int cancelled_ticket_report(void);
int backup_to_transaction_restore(void);
//-----------------------------------------------
#endif /* DB_INTERFACE_H_ */

