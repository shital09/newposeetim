/*
 * gprs_interface.c
 *
 *  Created on: 09-Apr-2014
 *      Author: root
 */

#include "gprs_interface.h"
#include "lcd_drvr.h"
#include "gprs_drvr.h"

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <malloc.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netif.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <curl/curl.h> // Swap this is code for CURL

#include <posapi.h>
#include <wnet.h>
#include <ppp.h>
#include <ped.h>
#include <sys/time.h>
#include <sys/stat.h>
#include "cstc_defines.h"
#include "db_interface.h"
#include "cstclog.h"
#include "display_interface.h"
#include "uart_interface.h"
#include "cstc_error.h"


/*
char *HttpReq1 ="POST/log.php ";
char *Header ="HTTP/1.1\n";
char *HttpReq2 ="Host:223.30.102.30\r\n";
char *HttpReq3 ="User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36\r\n";
char *HttpReq4 ="Accept: text/html,application/xhtml+xml,application/xml;q=0.9,\r\n";
char *HttpReq5 ="Accept-Language: en-US\r\n";
char *HttpReq6 ="Accept-Encoding: gzip, deflate\r\n";
char *HttpReq7 ="Connection: keep-alive\r\n";
char *HttpReq8 ="Content-Type: application/x-www-form-urlencoded\r\n";
char *HttpReq9 ="Content-Length: 33";
char *HttpReq10 ="deviceid=123&data=abhishek&mode=1";*/

//char *HttpReq8 ="accept: application/xml";

//deviceid=123&data=abhishek&mode=1;

extern void socket_client_demo_1(void);


//extern int gprs_get_transaction_log_data(transaction_log_struct gprs_transaction_log_struct_var);
packet_format_struct packet_format_struct_var;
waybill_mas_packet_id_struct waybill_mas_packet_id_struct_var;
sched_mas_packet_id_struct sched_mas_packet_id_struct_var;
route_mas_packet_id_struct route_mas_packet_id_struct_var;
fare_chart_packet_id_struct fare_chart_packet_id_struct_var;
duty_mas_packet_id_struct duty_mas_packet_id_struct_var;
req_for_wb_struct req_for_wb_struct_var;
req_wb_packet_id_struct req_wb_packet_id_struct_var;
waybill_master_struct gprs_waybill_master_struct_var;
duty_master_struct duty_master_struct_var;
route_master_struct route_master_struct_var;
//fare_chart_struct fare_chart_struct_var;
registration_response_struct registration_response_struct_var;
reg_response_packet_id_struct reg_response_packet_id_struct_var;
bs_sync_req_struct bs_sync_req_struct_var;
transaction_log_struct gprs_transaction_log_struct_var;
download_response_struct download_response_struct_var;
extern conductor_pass_registration_struct conductor_pass_registration_struct_var;
extern all_flags_struct all_flags_struct_var;
extern duty_master_struct duty_master_struct_var; //needed for OTA update
extern waybill_master_struct waybill_master_struct_var; //needed for OTA update
extern duty_master_struct * full_duty_master_struct_var; //for lc schedule update
extern apn_name_struct apn_name_struct_var;

extern char username[CONDR_NAME_LEN+1];
extern char pswd[PSWRD_LEN+1];
char Data_Buff_Auth[1000] ={0};

extern char *Databuff;

int signal_num_1 =0 , imsi_len=50 ; // Swap this is code for CURL
char imsi_no[50]={'\0'}; // Swap this is code for CURL
battery_info_t battery_info;

//static unsigned
//get_file_size_gprs (const char * file_name);
//static unsigned char *
//void read_whole_file_gprs (const char * file_name, unsigned s, unsigned char * contents);

/*-----------------------------New Requirement------------------------------------*/

extern master_status_struct master_status_struct_var;
ack_packet_struct gprs_ack_packet_struct_var;

char GPRS_schedule[SCHEDULE_NO_LEN+1]={'\0'};
char GPRS_global_buff[2048],GPRS_checksumbuffer[50];

int GPRS_checksumFlag = 0, GPRS_global_iTemp = 0, GPRS_New_pkt_recv_flag = 0 , GPRS_Total_packet_length = 0;
static int GPRS_global_pkt_id ; //Received packet ID

char GPRS_Err_Msg[ERROR_MSG_BUFFER_LEN];
int GPRS_Err_Len=0;
static int gprs_UpdateDataFlag=0;
int GPRS_rt_count=0;

int GPRS_RecordCntToRecv =0,GPRS_TotalRecordCntRecvd =0;
char GPRS_global_schedule[SCHEDULE_NO_LEN+1];
char GPRS_global_Rate_Id[RATE_ID_LEN+1];
char GPRS_global_Route_Id[ROUTE_ID_LEN+1];
char GPRS_global_waybill[WAYBILL_NO_LEN+1];
char *data_to_be_send = NULL;

extern unsigned char *
read_whole_file (const char * file_name);
extern volatile char upload_semaphore;
extern pthread_mutex_t upload_semaphore_waybill;
extern pthread_mutex_t upload_semaphore_schedule ;
//extern pthread_mutex_t printing_mutex;
extern pthread_mutex_t GPRS_init_deinit_mutex;

/*-----------------------------New Requirement------------------------------------*/

/*******************************************STRUCTURES****************************************/
int Gprs_Interface_Init()
{
	int signal_num=0;
	int retval = -1;

#if  Service_Provider_ENABLE
	int fd = -1 , retry_cmd = 0 ,i=0,j=0;
	char send_AT_CMD_INIT[]="ATE0\r\n";
	char send_AT_CMD_INIT_1[]="AT\r\n";
	char Check_SIM[]="AT+CPIN?\r\n";
	char send_AT_CMD[] = "AT+COPS?\r\n";  //"AT+CSPN?\r\n"; //
	char recv_AT_Resp[500]={'\0'};
	uint32_t recv_len = 500;
	char *sub_string=NULL;
	char result[50]={'\0'};
	char service_provider[50]={'\0'};
#endif

	CstcLog_printf("THREAD::Power On device ");
	//	all_flags_struct_var.gprs_on_flag = 1;

	//	pthread_mutex_lock(&GPRS_init_deinit_mutex); 12/06/15 temp solution for 10 min hang

	retval = Set_Gprs_Power_On();
	if (0 != retval)
	{
		CstcLog_printf("THREAD::Power On Failed");

		//		all_flags_struct_var.gprs_on_flag = 0;
		//pthread_mutex_unlock(&GPRS_init_deinit_mutex); 12/06/15 temp solution for 10 min hang

		return 0;
	}
	else
	{
		CstcLog_printf("THREAD::Init wnet.. ");
#if  Service_Provider_ENABLE
		/*-----------------------------------Service Provider-----------------------------------------------*/
		if(strlen(apn_name_struct_var.apn_name) == 0)
		{

			fd = wlan_module_commport_open("/dev/ttyS1",115200);
			CstcLog_printf("Snehal  %d ",fd);

			memset(recv_AT_Resp,0,500);
			sleep(2);
			retval = -1;
			retval = wlan_module_exec_atcmd(fd, send_AT_CMD_INIT, &recv_len,recv_AT_Resp, GPRS_MODULE_TYPE, 500 ,500);
			sleep(3);
			CstcLog_printf("Snehal 1::::::: send_AT_CMD_INIT = %s \n recv_AT_Resp = %s \n fd = %d retval = %d",send_AT_CMD_INIT , recv_AT_Resp ,fd,retval);

			memset(recv_AT_Resp,0,500);
			recv_len = 500;
			sleep(2);
			retval = -1;
			retval = wlan_module_exec_atcmd(fd, send_AT_CMD_INIT_1, &recv_len,recv_AT_Resp, GPRS_MODULE_TYPE, 500 ,500);
			sleep(3);
			CstcLog_printf("INIT1::::::: send_AT_CMD_INIT_1 = %s \n recv_AT_Resp = %s \n fd = %d retval = %d",send_AT_CMD_INIT_1 , recv_AT_Resp ,fd,retval);

			memset(recv_AT_Resp,0,500);
			recv_len = 500;
			sleep(2);
			retval = -1;
			retval = wlan_module_exec_atcmd(fd, Check_SIM, &recv_len,recv_AT_Resp, GPRS_MODULE_TYPE, 500 ,500);
			sleep(3);
			CstcLog_printf("Checking SIM::::::: Check_SIM = %s \n recv_AT_Resp = %s \n fd = %d retval = %d",Check_SIM , recv_AT_Resp ,fd,retval);

			if(strstr(recv_AT_Resp,"+CPIN: READY"))
			{
				do
				{
					memset(recv_AT_Resp,0,500);
					memset(result,0,sizeof(result));
					memset(service_provider,0,sizeof(service_provider));
					recv_len = 500;
					sleep(2);
					retval = -1;
					retval = wlan_module_exec_atcmd(fd, send_AT_CMD, &recv_len,recv_AT_Resp, GPRS_MODULE_TYPE, 500 ,500);
					sleep(3);
					CstcLog_printf("Snehal %d::::::: send_AT_CMD = %s \n recv_AT_Resp = %s \n fd = %d retval = %d",retry_cmd,send_AT_CMD , recv_AT_Resp ,fd,retval);

					if(strstr(recv_AT_Resp,"""") == NULL)
					{
						retry_cmd++;
						CstcLog_printf("retry_cmd = %d",retry_cmd);
					}
					else
					{
						CstcLog_printf("Coming............");
						if((sub_string =strstr(recv_AT_Resp,"\"")) != NULL)
						{
							memset(result,0,sizeof(result));
							memcpy(result,(sub_string+1),strlen(sub_string));
							CstcLog_printf("service_provider for first time = %s ",result);

							for (i = 0; result[i] != '\"'; i++)
							{
								CstcLog_printf("In remove =%d\n",i);
								service_provider[j++] = result[i];
							}
							CstcLog_printf("service_provider for 2nd time = %s ",service_provider);
							service_provider[strlen(service_provider)]='\0';
							if(strlen(service_provider) != 0)
							{
								get_apn_details(service_provider);
								break;
							}
							else
								retry_cmd++;
						}
						else
							retry_cmd++;
					}

				}while(retry_cmd  < 5);

				CstcLog_printf("Snehal ............retry finished");

				retval = -1;
				retval = wlan_module_commport_close(fd);
				CstcLog_printf("Snehal port is closed ::::: %d %d",fd,retval);
				//			get_apn_details(service_provider);
			}
			else
			{
				CstcLog_printf("check sim failed.......I am returning");
				memset(recv_AT_Resp,0,500);

				retval = -1;
				retval = wlan_module_commport_close(fd);
				CstcLog_printf("Snehal port is closed ::::: %d %d",fd,retval);
				Gprs_Interface_Deinit();
				return 0;
			}
		}

		/*-----------------------------------Service Provider-----------------------------------------------*/
#endif
		retval =-1;
		retval = Gprs_Initialise();
		CstcLog_printf("THREAD::retval = %d",retval);
		if (0 != retval){
			CstcLog_printf("THREAD::Init wnet failed.. ");
			Gprs_Interface_Deinit();
			return 0;
		}
	}

	CstcLog_printf("THREAD::pink elephant.......");

	//	if(0==wnet_checksim())
	retval = -1;
	retval = wnet_checksim();
	if(retval == 0)
	{
		CstcLog_printf("THREAD::pink panther.......");
		if(0==wnet_signal(&signal_num))
		{
			CstcLog_printf("THREAD::Signal strength: %d",signal_num);

			//			wnet_read_simcardIMSI(imsi_no,imsi_len);
			//			CstcLog_printf("imsi_no = %s imsi_len = %d",imsi_no,imsi_len);
		}
//		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
//		{
//			CstcLog_printf("poor signal...");
//			display_popup("POOR SIGNAL");
//			Gprs_Interface_Deinit();
//			return 0;
//		}

	}
	else
	{
		CstcLog_printf("THREAD::INSERT SIM CARD");
		//		show_sim_error(retval);
		Gprs_Interface_Deinit();
		return 0;
	}
	return 1;
}

int Gprs_Interface_Deinit()
{
	Set_Gprs_Power_Off();
	//	all_flags_struct_var.gprs_on_flag = 0;
	//pthread_mutex_unlock(&GPRS_init_deinit_mutex);  12/06/15 temp solution for 10 min hang

	return 1;
}

int Gprs_Set_PPP_Connection(int index)
{
	int ret = 0,i=0,isUpdatePresent=0;
	char route_id[30][100]={{'\0'}},rate_id[30][100]={{'\0'}},fare_chart_id[30][100]={{'\0'}};

	//	char *route_id = NULL;

	char id_string[100]={'\0'};
	//	char delete_sql_string[400]={'\0'};
	int update_type = 0,id_Cnt = 0,id_indx= 0 ;
	int last_update_type_cnt = 4;
	int routeCnt=0,rateCnt=0,fareCnt=0;
	char *sql = NULL;
	FILE *dbDataFile=NULL;
	int updated_scheduleCnt= 0,updated_routeCnt= 0,updated_rateCnt= 0,updated_fareCnt=0;
	//	for(i = 0;i<30;i++)
	//	{
	//		memset(route_id[i],0,100);
	//		memset(rate_id[i],0,100);
	//	}
	ret = -1;
	//lcd_printf(ALG_LEFT,16, "set attached...");
	// lcd_flip();
	CstcLog_printf("THREAD::Gprs_Set_PPP_Connection...");

	//	pthread_mutex_lock(&printing_mutex);
	ret = Gprs_ISP();
	//	pthread_mutex_unlock(&printing_mutex);


	CstcLog_printf("THREAD::Gprs_ISP ret = %d", ret);

	if (0 != ret)
	{
		CstcLog_printf("THREAD::wnet_set_attached() failed %d", ret);
		if(ret<0)
			return ret;
		else
			return 0;

	}
	if (0 == ret)
	{
		CstcLog_printf("THREAD::PPP logon...");

		//		Gprs_Close_PPP_Connection();
		//		get_apn_details();

		ret = Gprs_Open_PPP_Connection();

		CstcLog_printf("THREAD::Gprs_Open_PPP_Connection ret = %d", ret);


		if (0 != ret)
		{
			CstcLog_printf("THREAD::PPP open failed %d", ret);

			Gprs_Close_PPP_Connection();
			if(ret<0)
				return ret;
			else
				return 0;
		}
		else
		{
			CstcLog_printf("THREAD::PPP check...");

			//pthread_mutex_lock(&printing_mutex);
			ret = Gprs_Check_PPP_Connection();
			//			pthread_mutex_unlock(&printing_mutex);

			CstcLog_printf("THREAD::Gprs_Check_PPP_Connection ret = %d", ret);

			if (0 == ret)
			{
				CstcLog_printf("THREAD::PPP logon Ok");

				//				all_flags_struct_var.ppp_on_flag = 1;
				if(all_flags_struct_var.online_schedule_update_flag == 0)
				{

					/************************* TICKET UPLOAD OTA*************************/
					if(all_flags_struct_var.ticket_upload_flag==1 && (all_flags_struct_var.online_schedule_update_flag == 0))
					{
						if(socket_client_demo())  //ticket data upload is done in this func.
						{
							CstcLog_printf("THREAD:: ticket upload is done");
							all_flags_struct_var.ticket_upload_flag = 0;
							CstcLog_printf("THREAD:: ticket upload = 0");
							Gprs_Close_PPP_Connection();
							return 1;
						}
						else
						{
							all_flags_struct_var.ticket_upload_flag = 0;
							Gprs_Close_PPP_Connection();
							return 0;
						}
					}

					/************************* WAYBILL UPLOAD*************************/

					if((all_flags_struct_var.waybill_upload_Flag == 1)&&(all_flags_struct_var.online_schedule_update_flag == 0))
						//				if((all_flags_struct_var.waybill_upload_Flag == 1)&&(all_flags_struct_var.online_schedule_update_flag == 0) && (all_flags_struct_var.tc_online_schedule_update_flag == 0))
					{
						CstcLog_printf("Waybill uploading");
						if(upload_waybill_string())
							all_flags_struct_var.waybill_upload_Flag= 0 ;
						CstcLog_printf("Waybill uploading complete");
					}

					/************************* SCHEDULE UPLOAD*************************/

					if((all_flags_struct_var.schedule_upload_flag == 1) && (all_flags_struct_var.online_schedule_update_flag == 0))
						//				if((all_flags_struct_var.schedule_upload_flag == 1) && (all_flags_struct_var.online_schedule_update_flag == 0) && (all_flags_struct_var.tc_online_schedule_update_flag == 0))
					{
						CstcLog_printf("Schedule uploading....");
						if(send_schedule_string())
						{
							CstcLog_printf("Schedule flag = 0 ");
							all_flags_struct_var.schedule_upload_flag= 0 ;
						}
						CstcLog_printf("Schedule uploading complete");
					}

					/************************* ONLINE AUTH*************************/

					if((all_flags_struct_var.online_auth==1) && (all_flags_struct_var.online_schedule_update_flag == 0))
					{

						CstcLog_printf("THREAD:: In auth");
						if(socket_client_demo_auth())
						{
							CstcLog_printf("THREAD:: snehal.................snehal");
							all_flags_struct_var.online_auth = 0;
							CstcLog_printf("THREAD:: online_auth = 0");
							Gprs_Close_PPP_Connection();
							return 1;
						}
						else
						{
							all_flags_struct_var.online_auth = 0;
							Gprs_Close_PPP_Connection();
							return 0;
						}
					}
				}

				/************************* SCHEDULE UPDATE*************************/

				else if(all_flags_struct_var.online_schedule_update_flag == 1)
					//				if((all_flags_struct_var.online_schedule_update_flag == 1) || (all_flags_struct_var.tc_online_schedule_update_flag == 1))
				{
//					Show_Error_Msg("Updating Schedule \n  Please Wait....");
					CstcLog_printf("OTA::Schedule uploading");
					id_Cnt = 1;
					CstcLog_printf("OTA::Inserting from RESTORE");
					DbInterface_Delete_Table("Delete from schedule_master_restore");
					DbInterface_Delete_Table("DELETE FROM route_header_restore");
					DbInterface_Delete_Table("DELETE FROM route_master_restore");
					DbInterface_Delete_Table("DELETE FROM rate_master_restore");
					//					insert_schedule_master_in_restore();
					update_type= 4; //starts with route_header
					updated_scheduleCnt= 0;
					updated_routeCnt= 0;
					updated_rateCnt= 0;
					updated_fareCnt=0;
					memset(id_string,0,100);

					routeCnt = get_all_ids(route_id,rate_id,fare_chart_id,1);
					rateCnt = get_all_ids(route_id,rate_id,fare_chart_id,3);
					fareCnt = get_all_ids(route_id,rate_id,fare_chart_id,4);

					while(update_type<=last_update_type_cnt)
					{

						dbDataFile = NULL;
						dbDataFile= fopen(DB_INPUT_FILE_ADDR,"w");

						if(dbDataFile!=NULL)
						{
							fclose(dbDataFile);
							fflush(dbDataFile);
							dbDataFile = NULL;
						}

						if(update_type == 0)
						{
							Show_Error_Msg("Checking Update for \nSchedule Master");
						}
						else if(update_type == 1)
						{
							Show_Error_Msg("Checking Update for \nRoute Header");
							id_Cnt=routeCnt;
						}
						else if(update_type == 2)
						{
							Show_Error_Msg("Checking Update for \nRoute Master");
							id_Cnt=routeCnt;

						}
						else if(update_type == 3)
						{
							Show_Error_Msg("Checking Update for \nRate Master");
							id_Cnt=rateCnt;

						}
						else if(update_type == 4)
						{
							Show_Error_Msg("Checking Update for \nFare Chart");
						}
						id_indx = 0;



#if 0
						if(update_type==1)
						{
							/***********check if routes for updating present in schedule_master. If not, check in route_master.***********/
							routeCnt = DbInterface_Get_Row_Count("schedule_master", strlen("schedule_master"),0);
							if(routeCnt!=0)
							{
								CstcLog_printf("Route present in schedule_master..routeCnt = %d",routeCnt);
							}
							else
							{
								routeCnt = DbInterface_Get_Row_Count("route_master", strlen("route_master"),0);
								CstcLog_printf("Route present in reoute_master..routeCnt = %d",routeCnt);
							}
							if(routeCnt==0)
							{
								Show_Error_Msg("NO ROUTE FOUND FOR UPDATE");
								return 0;
							}
							route_id= (char *)malloc((sizeof(char )*(ROUTE_ID_LEN+1))*routeCnt);
							if(route_id==NULL)
							{
								CstcLog_printf("Error: Unable to allocate memory");
								route_id=NULL;
								return 0;
							}

							// store routes to be updated in allocated memory.

							/*****************************************************/

							get_all_route_details(route_id);


							/*****************************************************/

							//free allocated memory for route_id storage....
							if(route_id !=NULL)
							{
								free(route_id);
								route_id=NULL;
							}



							if(yes_no("CONTINUE DOWNLOADING..."))
							{

							}
							else
							{
								Show_Error_Msg("DOWNLOAD FINISHED");
							}


							/***********************************************************************************************/
						}
#endif


						CstcLog_printf("before while loop check...::id_indx = %d  id_Cnt = %d",id_indx,id_Cnt);

						while(id_indx < id_Cnt)
						{
							CstcLog_printf("OTA::start while......id_indx = %d  id_Cnt = %d",id_indx,id_Cnt);
							memset(id_string,0,100);
							if(update_type == 1 || update_type == 2)
							{
								strcpy(id_string,route_id[id_indx]);
							}
							if(update_type == 3)
							{
								strcpy(id_string,rate_id[id_indx]);
							}
							if(update_type == 4)
							{
								strcpy(id_string,fare_chart_id[id_indx]);
							}

							isUpdatePresent=0;
							CstcLog_printf("Before curl_online_schedule_update function......");
							CstcLog_printf("update_type = %d id_string = %s index = %d",update_type,id_string,index);

							//							isUpdatePresent= online_schedule_update(update_type,id_string,index);  //index= -1

							isUpdatePresent= curl_online_schedule_update(update_type,id_string,index);  //index= -1

							CstcLog_printf("curl_online_schedule_update function returned isUpdatePresent = %d...for update_type=%d , id_string=%s",isUpdatePresent,update_type,id_string);


							//							if(!online_schedule_update(update_type,id_string))
							if(isUpdatePresent==0)  // update_failed
							{
								CstcLog_printf("OTA::reinserting from RESTORE");
								//									DbInterface_Delete_Table("Delete from schedule_master_restore");
								CstcLog_printf("OTA::Deleting Changes");
								//									DbInterface_Delete_Table("DELETE FROM route_header_restore");
								//									DbInterface_Delete_Table("DELETE FROM route_master_restore");
								//									DbInterface_Delete_Table("DELETE FROM rate_master_restore");

								switch(update_type)
								{
								case 0: Show_Error_Msg("Updating Schedule Master\n  Failed");
								CstcLog_printf("Updating Schedule Master Failed");
								break;
								case 1:Show_Error_Msg("Updating Route Header\n  Failed");
								CstcLog_printf("Updating Route Header Failed");
								break;
								case 2:Show_Error_Msg("Updating Route Master\n  Failed");
								CstcLog_printf("Updating Route Master Failed");
								break;
								case 3:Show_Error_Msg("Updating Rate Master\n  Failed");
								CstcLog_printf("Updating Rate Master Failed");
								break;
								case 4:Show_Error_Msg("Updating Fare Chart\n  Failed");
								CstcLog_printf("Updating Fare Chart Failed");
								break;
								default:
									break;
								}
								beep(2800,1000);
								CstcLog_printf("OTA::Failed id_indx = %d  updated_routeCnt = %d",id_indx,updated_routeCnt);
								break;
							}


							// here update Not Found
							if(update_type == 1 || update_type == 2)
							{
								if(update_type == 1)
								{
									if(isUpdatePresent==2)
									{
										Show_Error_Msg("Route Header \nUpdate Not Found");
										CstcLog_printf("No Update for Route Header");
									}
									else
									{
										Show_Error_Msg("Route Header \nUpdate Received");
										CstcLog_printf("Route Header Update Received");
									}
								}
								else
								{
									if(isUpdatePresent==2)
									{
										Show_Error_Msg("Route Master \nUpdate Not Found");
										CstcLog_printf("Route Master Update Not Found");

									}
									else
									{
										Show_Error_Msg("Route Master \nUpdate Received");
										CstcLog_printf("Route Master Update Received");
										updated_routeCnt+= 1;

									}
								}
								CstcLog_printf("OTA::id_indx = %d   total no. of routes updated  = %d",id_indx,updated_routeCnt);
							}
							else if(update_type == 3)
							{
								if(isUpdatePresent==2)
								{
									Show_Error_Msg("Rate Master \nUpdate Not Found");
									CstcLog_printf("Rate Master Update Not Found");
								}
								else
								{
									Show_Error_Msg("Rate Master \nUpdate Received");
									CstcLog_printf("Rate Master Update Received");
									updated_rateCnt+= 1;
								}
								CstcLog_printf("OTA::id_indx = %d  total no. of rate_id updated = %d",id_indx,updated_rateCnt);
							}
							else if(update_type == 4)
							{
								if(isUpdatePresent==2)
								{
									Show_Error_Msg("Fare Chart \nUpdate Not Found");
									CstcLog_printf("Fare Chart Update Not Found");
								}
								else
								{
									Show_Error_Msg("Fare Chart \nUpdate Received");
									CstcLog_printf("Fare Chart Update Received");
									updated_fareCnt+= 1;
								}
								CstcLog_printf("OTA::id_indx = %d  total no. of fare_data updated = %d",id_indx,updated_fareCnt);
							}

							id_indx++;  // increment indx to get next id
						}// all_id update check while loop ends here

						CstcLog_printf("OTA:: Outside inner while loop :D");


						if(id_indx < id_Cnt)
						{
							CstcLog_printf("OTA::coming here...id_indx = %d  id_Cnt = %d",id_indx,id_Cnt);
							CstcLog_printf("OTA:: Error - Couldn't update all records..");
							isUpdatePresent=0;
							break;
						}


						if(update_type == 0)
						{
							if(isUpdatePresent==3)    // upto date
							{
								Show_Error_Msg("Schedule Data \n is upto date");
								updated_scheduleCnt= 0;
							}
							else if(isUpdatePresent==2) // update not found
							{
								Show_Error_Msg("Schedule Master \nUpdate Not Found");
								id_Cnt = extract_mismatched_ids(ROUTE_ID_OTA,route_id,rate_id,isUpdatePresent,index);
								updated_scheduleCnt= 0;
							}
							else if(isUpdatePresent==1) // update received
							{
								//update the schedule_master_rstore with the performed trip before this
								Show_Error_Msg("Schedule Master Received\n Successfully");
								if(update_schedule_master_restore_with_trip_performed())
								{
									// Check only of those trips who's trips are not performed
									id_Cnt = extract_mismatched_ids(ROUTE_ID_OTA,route_id,rate_id,isUpdatePresent,index);
								}
								else
									id_Cnt = -1;
								updated_scheduleCnt= 1;
							}
							else
							{
								updated_scheduleCnt= 0;
							}
							isUpdatePresent=0;
							beep(2800,500);
						}
						else if(update_type == 1)
						{
							if((isUpdatePresent==2))//||(updated_scheduleCnt==0))
							{
								Show_Error_Msg("Route Header \nUpdate Not Found");
								CstcLog_printf("OTA::Route Header .... Update Not Found...........");

							}
							else if(isUpdatePresent==1)
							{
								Show_Error_Msg("Route Header Received\n Successfully");
								CstcLog_printf("OTA:: RouteH Recieved Successful...id_indx = %d  updated_routeCnt = %d",id_indx,updated_routeCnt);
							}
							updated_routeCnt= 0;  //This will be zero coz Route Master is to be inserted then only it is complete
							CstcLog_printf("OTA::Outside id_indx = %d  updated_routeCnt = %d",id_indx,updated_routeCnt);
							isUpdatePresent=0;
							beep(2800,500);
						}
						else if(update_type == 2)
						{
							if((isUpdatePresent==2)/*||(updated_scheduleCnt==0)*/)
							{
								Show_Error_Msg("Route Master \nUpdate Not Found");
//								id_Cnt = extract_mismatched_ids(RATE_ID_OTA,route_id,rate_id,isUpdatePresent,index);
								if(updated_routeCnt>0)
									updated_routeCnt= 1;
								else
									updated_routeCnt= 0;
							}
							else if(isUpdatePresent==1)
							{
								Show_Error_Msg("Route Master Received\n Successfully");
								// Check only of those trips who's trips are not performed
//								id_Cnt = extract_mismatched_ids(RATE_ID_OTA,route_id,rate_id,isUpdatePresent,index);
								CstcLog_printf("OTA::id_indx = %d  updated_routeCnt = %d",id_indx,updated_routeCnt);
								if(updated_routeCnt>0)//<id_indx)
									updated_routeCnt= 1;//0;
								else
									updated_routeCnt= 0;//1;
							}
							else
							{
								updated_routeCnt= 0;
								CstcLog_printf("OTA::Update route count is zero");
							}
							CstcLog_printf("OTA::id_indx = %d  updated_routeCnt = %d",id_indx,updated_routeCnt);
							isUpdatePresent=0;
							beep(2800,500);
						}
						else if(update_type == 3)
						{
							if((isUpdatePresent==2)/*||(updated_scheduleCnt==0)*/)
							{
								Show_Error_Msg("Rate Master \nUpdate Not Found");
								if(updated_rateCnt>0)//<id_indx)
									updated_rateCnt= 1;//0;
								else
									updated_rateCnt= 0;//1;
							}
							else if(isUpdatePresent==1)
							{
								Show_Error_Msg("Rate Master Received\n Successfully");
								CstcLog_printf("OTA::Rate Master Received Successfully id_indx = %d  updated_rateCnt = %d",id_indx,updated_rateCnt);
								if(updated_rateCnt>0)//<id_indx)
									updated_rateCnt= 1;//0;
								else
									updated_rateCnt= 0;//1;
							}
							else
							{
								updated_rateCnt= 0;
							}
							CstcLog_printf("OTA::id_indx = %d  updated_rateCnt = %d",id_indx,updated_rateCnt);
							isUpdatePresent=0;
							beep(2800,500);

						}
						else if(update_type == 4)
						{
							if((isUpdatePresent==2)/*||(updated_scheduleCnt==0)*/)
							{
								Show_Error_Msg("Fare Chart \nUpdate Not Found");
								if(updated_rateCnt>0)//<id_indx)
									updated_rateCnt= 1;//0;
								else
									updated_rateCnt= 0;//1;
							}
							else if(isUpdatePresent==1)
							{
								Show_Error_Msg("Fare Chart  Received\n Successfully");
								CstcLog_printf("OTA::Rate Master Received Successfully id_indx = %d  updated_fareCnt = %d",id_indx,updated_fareCnt);
								if(updated_fareCnt>0)//<id_indx)
									updated_fareCnt= 1;//0;
								else
									updated_fareCnt= 0;//1;
							}
							else
							{
								updated_fareCnt= 0;
							}
							CstcLog_printf("OTA::id_indx = %d  updated_rateCnt = %d",id_indx,updated_fareCnt);
							isUpdatePresent=0;
							beep(2800,500);

						}
						if(id_Cnt == -1)
						{
							CstcLog_printf("OTA::UPDATE ERROR");
							break;
						}
						update_type++;
					}  // end of while(update_type<4) loop


					CstcLog_printf("OTA::Outside outter while loop :O update_type %d",update_type);

					if((update_type < last_update_type_cnt) || (id_Cnt  == -1))
					{
						CstcLog_printf("OTA::Error In Update..");
						display_popup("Error In Update..");
						Gprs_Close_PPP_Connection();
						all_flags_struct_var.online_schedule_update_flag= 0 ;
						return 0;
					}
					else //if((update_type == last_update_type_cnt) && (id_Cnt  > -1))
					{
						CstcLog_printf("OTA::Before updating updated_routeCnt = %d // updated_rateCnt = %d",updated_routeCnt, updated_rateCnt);

						if(updated_scheduleCnt==1||updated_routeCnt==1||updated_rateCnt==1||updated_fareCnt==1)
						{
							Show_Error_Msg("Inserting Masters \n Please wait.....");
							CstcLog_printf("OTA::Inserting Masters \n Please wait.....");
							//copy received data to main table
							DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
							if(updated_routeCnt==1)
							{
								if(0<DbInterface_Get_Row_Count("route_master_restore", strlen("route_master_restore"),0))
								{
									CstcLog_printf("OTA::Processing route_master_restore id_Cnt = %d", id_Cnt);

//									id_Cnt = 1;
//									for(i = 0;i<30;i++)
//									{
//										memset(route_id[i],0,100);
//									}
//									id_Cnt = extract_mismatched_ids(ROUTE_ID_OTA,route_id,rate_id,3,index);


									if(id_Cnt>0)
									{
										sql = (char*)malloc(100*sizeof(char));
										for(i = 0;i<id_Cnt;i++)
										{
											memset(sql,0,100);
/*											if(index>=0)
											{
												sprintf(sql,"Delete from route_master where route_id='%s'and schedule_id='%s'",route_id[i],(full_duty_master_struct_var+index)->schedule_id);
											}
											else
												sprintf(sql,"Delete from route_master where route_id='%s'and schedule_id='%s'",route_id[i],duty_master_struct_var.schedule_id);*/

											sprintf(sql,"Delete from route_master where route_id='%s'",route_id[i]);

											if(!DbInterface_Delete_Table(sql))
											{
												DbInterface_transaction_commit("ROLLBACK;",0);
												display_popup("Err: Update Route Master Fail");
												Gprs_Close_PPP_Connection();
												all_flags_struct_var.online_schedule_update_flag= 0 ;
												if(sql!=NULL)
												{
													free(sql);
													sql = NULL;
												}
												return 0;
											}
											CstcLog_printf("OTA::sql = %s",sql);
											usleep(100);
											memset(sql,0,100);
/*											if(index>=0)
											{
												sprintf(sql,"Delete from route_header where route_id='%s'and schedule_id='%s'",route_id[i],(full_duty_master_struct_var+index)->schedule_id);
											}
											else
												sprintf(sql,"Delete from route_header where route_id='%s'and schedule_id='%s'",route_id[i],duty_master_struct_var.schedule_id);*/

											sprintf(sql,"Delete from route_header where route_id='%s'",route_id[i]);

											if(!DbInterface_Delete_Table(sql))
											{
												DbInterface_transaction_commit("ROLLBACK;",0);
												display_popup("Err: Update Route Master Fail");
												Gprs_Close_PPP_Connection();
												all_flags_struct_var.online_schedule_update_flag= 0 ;
												if(sql!=NULL)
												{
													free(sql);
													sql = NULL;
												}
												return 0;
											}

											CstcLog_printf("OTA::sql = %s",sql);
											usleep(100);
										}
										if(sql!=NULL)
										{
											free(sql);
											sql = NULL;
										}
									}
									if(!Insert_restore_table_in_main_table("insert into route_master select * from route_master_restore"))
									{
										CstcLog_printf("OTA::Route Master Insert Fail ");
										DbInterface_transaction_commit("ROLLBACK;",0);
										display_popup("Error In Update..");
										Gprs_Close_PPP_Connection();
										all_flags_struct_var.online_schedule_update_flag= 0 ;
										return 0;
									}
								}


								if(0<DbInterface_Get_Row_Count("route_header_restore", strlen("route_header_restore"),0))
								{
									CstcLog_printf("OTA::Processing route_header_restore");

									if(!Insert_restore_table_in_main_table("insert into route_header select * from route_header_restore"))
									{
										CstcLog_printf("OTA::Route Header Insert Fail ");
										DbInterface_transaction_commit("ROLLBACK;",0);
										display_popup("Error In Update..");
										Gprs_Close_PPP_Connection();
										all_flags_struct_var.online_schedule_update_flag= 0 ;
										return 0;
									}
								}
							}
							if(updated_rateCnt==1)
							{
								if(0<DbInterface_Get_Row_Count("rate_master_restore", strlen("rate_master_restore"),0))
								{
									CstcLog_printf("OTA::Processing rate_master_restore");

//									id_Cnt = 0;
//									for(i = 0;i<30;i++)
//									{
//										memset(rate_id[i],0,100);
//									}
//									id_Cnt = extract_mismatched_ids(RATE_ID_OTA,route_id,rate_id,3,index);
									if(id_Cnt>0)
									{
										sql = (char*)malloc(100*sizeof(char));
										for(i = 0;i<id_Cnt;i++)
										{
											memset(sql,0,100);
											sprintf(sql,"Delete from rate_master where rate_id='%s'",rate_id[i]);
											if(!DbInterface_Delete_Table(sql))
											{
												DbInterface_transaction_commit("ROLLBACK;",0);
												display_popup("Err: Update Rate Master Fail");
												Gprs_Close_PPP_Connection();
												all_flags_struct_var.online_schedule_update_flag= 0 ;
												if(sql!=NULL)
												{
													free(sql);
													sql = NULL;
												}
												return 0;
											}
											CstcLog_printf("OTA::sql = %s",sql);
											usleep(100);
										}
										if(sql!=NULL)
										{
											free(sql);
											sql = NULL;
										}
									}

									CstcLog_printf("OTA::Rate Master restore timeeeeeeeeee ");

									if(!Insert_restore_table_in_main_table("insert into rate_master select * from rate_master_restore"))
									{
										CstcLog_printf("OTA::Rate Master Insert Fail ");
										DbInterface_transaction_commit("ROLLBACK;",0);
										display_popup("Error In Update..");
										Gprs_Close_PPP_Connection();
										all_flags_struct_var.online_schedule_update_flag= 0 ;
										return 0;
									}
								}
							}

							if(updated_fareCnt==1)
										{
											if(0<DbInterface_Get_Row_Count("fare_chart_restore", strlen("fare_chart_restore"),0))
											{
												CstcLog_printf("OTA::Processing fare_chart_restore");

			//									id_Cnt = 0;
			//									for(i = 0;i<30;i++)
			//									{
			//										memset(rate_id[i],0,100);
			//									}
			//									id_Cnt = extract_mismatched_ids(RATE_ID_OTA,route_id,rate_id,3,index);
												if(id_Cnt>0)
												{
													sql = (char*)malloc(100*sizeof(char));
													for(i = 0;i<id_Cnt;i++)
													{
														memset(sql,0,100);
														sprintf(sql,"Delete from fare_chart where fare_chart_id='%s'",fare_chart_id[i]);
														if(!DbInterface_Delete_Table(sql))
														{
															DbInterface_transaction_commit("ROLLBACK;",0);
															display_popup("Err: Update Fare Chart Fail");
															Gprs_Close_PPP_Connection();
															all_flags_struct_var.online_schedule_update_flag= 0 ;
															if(sql!=NULL)
															{
																free(sql);
																sql = NULL;
															}
															return 0;
														}
														CstcLog_printf("OTA::sql = %s",sql);
														usleep(100);
													}
													if(sql!=NULL)
													{
														free(sql);
														sql = NULL;
													}
												}

												CstcLog_printf("OTA::Fare Chart restore timeeeeeeeeee ");

												if(!Insert_restore_table_in_main_table("insert into fare_chart select * from fare_chart_restore"))
												{
													CstcLog_printf("OTA::Fare Chart Insert Fail ");
													DbInterface_transaction_commit("ROLLBACK;",0);
													display_popup("Error In Update..");
													Gprs_Close_PPP_Connection();
													all_flags_struct_var.online_schedule_update_flag= 0 ;
													return 0;
												}
											}
										}



							if(updated_scheduleCnt==1)
							{
								if(0<DbInterface_Get_Row_Count("schedule_master_restore", strlen("schedule_master_restore"),0))
								{
									sql = (char*)malloc(100*sizeof(char));
									memset(sql,0,100);
									if(index>=0)
									{
										sprintf(sql,"Delete from schedule_master where schedule_id='%s'",(full_duty_master_struct_var+index)->schedule_id);
									}
									else
										sprintf(sql,"Delete from schedule_master where schedule_id='%s'",duty_master_struct_var.schedule_id);

									CstcLog_printf("sql ========== %s",sql);
									if(!DbInterface_Delete_Table(sql))
									{
										DbInterface_transaction_commit("ROLLBACK;",0);
										display_popup("Err: Update Schedule Master Fail");
										Gprs_Close_PPP_Connection();
										all_flags_struct_var.online_schedule_update_flag= 0 ;
										if(sql!=NULL)
										{
											free(sql);
											sql = NULL;
										}
										return 0;
									}
									CstcLog_printf("OTA::sql = %s",sql);
									if(sql!=NULL)
									{
										free(sql);
										sql = NULL;
									}
									if(!Insert_restore_table_in_main_table("insert into schedule_master select * from schedule_master_restore"))
									{
										CstcLog_printf("OTA::Schedule Master Insert Fail ");
										DbInterface_transaction_commit("ROLLBACK;",0);
										display_popup("Error In Update..");
										Gprs_Close_PPP_Connection();
										all_flags_struct_var.online_schedule_update_flag= 0 ;
										return 0;
									}
								}
							}
							DbInterface_transaction_commit("COMMIT;",0);
							Show_Error_Msg("Insertion of Masters \n Successfull");
						}
						else
							Show_Error_Msg("DATA UPTO DATE");
						beep(2800,500);
						//					all_flags_struct_var.online_schedule_update_flag= 0 ;
						//					all_flags_struct_var.tc_online_schedule_update_flag=0;
						CstcLog_printf("OTA::Schedule updating complete");
//						Show_Error_Msg("Schedule Update \nCompleted");
					}
				}
				else
				{
					CstcLog_printf("THREAD::No Operation Performed");

					CstcLog_printf("THREAD::3.....................3");

					Gprs_Close_PPP_Connection();
					return 0;
				}
				//				socket_client_demo();
			}
			else
			{
				CstcLog_printf("THREAD::PPP logon failed");

				CstcLog_printf("THREAD::1.....................1");

				Gprs_Close_PPP_Connection();
				return 0;
			}
		}
		CstcLog_printf("THREAD::2.....................2");

		Gprs_Close_PPP_Connection();
	}
	return 1; //retval;
}

#if 1
int Gprs_Set_PPP_Connection_LC(void)
{
	int retval = 0;
	retval = 0;//lcd_printf(ALG_LEFT,16, "set attached...");	lcd_flip();

	CstcLog_printf("THREAD::Gprs_Set_PPP_Connection...");

	//	pthread_mutex_lock(&printing_mutex);
	retval = Gprs_ISP();
	//	pthread_mutex_unlock(&printing_mutex);

	CstcLog_printf("THREAD::Gprs_ISP retval = %d", retval);

	if (0 != retval)
	{
		CstcLog_printf("THREAD::wnet_set_attached() failed %d", retval);
		Gprs_Close_PPP_Connection();
		return 0;

	}
	if (0 == retval)
	{
		CstcLog_printf("THREAD::PPP logon...");

		//		Gprs_Close_PPP_Connection();
		//		get_apn_details();
		retval = Gprs_Open_PPP_Connection();

		CstcLog_printf("THREAD::Gprs_Open_PPP_Connection retval = %d", retval);


		if (0 != retval)
		{
			CstcLog_printf("THREAD::PPP open failed %d", retval);
			Gprs_Close_PPP_Connection();
			return 0;

		}
		else
		{
			CstcLog_printf("THREAD::PPP check...");
			//pthread_mutex_lock(&printing_mutex);
			retval = Gprs_Check_PPP_Connection();
			//			pthread_mutex_unlock(&printing_mutex);			CstcLog_printf("THREAD::Gprs_Check_PPP_Connection retval = %d", retval);

			if (0 == retval)
			{
				CstcLog_printf("THREAD::PPP logon Ok");

				//				all_flags_struct_var.ppp_on_flag = 1;

				if(all_flags_struct_var.online_auth)
				{
					CstcLog_printf("THREAD:: In auth");
					if(socket_client_demo_auth())
					{
						CstcLog_printf("THREAD:: snehal.................snehal");
						all_flags_struct_var.online_auth = 0;
						CstcLog_printf("THREAD:: online_auth = 0");
						Gprs_Close_PPP_Connection();
						return 1;
					}
					else
					{
						Gprs_Close_PPP_Connection();
						return 0;
					}
				}
			}
			else
			{
				CstcLog_printf("THREAD::PPP logon failed");

				Gprs_Close_PPP_Connection();
				return 0;
			}
		}
		CstcLog_printf("THREAD::1.....................2");

		Gprs_Close_PPP_Connection();
	}
	return 0; //retval;
}
#endif

/****************************************************************************************/

int Gprs_Set_PPP_Connection_Curl()
{
	int retval = -1;
	CstcLog_printf("Curl::Gprs_Set_PPP_Connection...");

	//	pthread_mutex_lock(&printing_mutex);
	retval = Gprs_ISP();
	//	pthread_mutex_lock(&printing_mutex);
	if (retval)  // '0'=success/ '1'=failed
	{
		CstcLog_printf("Curl Gprs_ISP Failed..");
		return 0;
	}

	CstcLog_printf("Curl Gprs_ISP successful..");

	//	get_apn_details();
	if(Gprs_Open_PPP_Connection()) // '0'=success/ '1'=failed
	{
		CstcLog_printf("Curl::PPP Open failed...");
		return 0;
	}
	CstcLog_printf("Curl::PPP Open successful...");

	retval = -1;
	//	pthread_mutex_lock(&printing_mutex);
	retval = Gprs_Check_PPP_Connection();
	//	pthread_mutex_unlock(&printing_mutex);
	if (retval) // '0'=success/ '1'=failed
	{
		CstcLog_printf("Curl::PPP logon failed");
		Gprs_Close_PPP_Connection();
		return 0;
	}
	CstcLog_printf("Curl::PPP logon Ok");
	return 1;
}

/****************************************************************************************/

void show_sim_error(int retval)
{
	lcd_clean();
	if(retval == (-4010))
	{
		CstcLog_printf("No SIM card ....%d",retval);
		//		lcd_printf(ALG_LEFT,16, "No SIM card");
		lcd_printf_ex(ALG_CENTER,20,70,"INSERT SIM CARD");
	}
	else if(retval == (-4011))
	{
		CstcLog_printf("SIM PIN Error ....%d",retval);
		//		lcd_printf(ALG_LEFT,16, "SIM PIN Error");
		lcd_printf_ex(ALG_CENTER,20,70,"SIM PIN ERROR");
	}
	else if(retval == (-4012))
	{
		CstcLog_printf("SIM Card Busy OR Key Error....%d",retval);
		//		lcd_printf(ALG_LEFT,16, "SIM Card Busy OR Key Error");
		lcd_printf_ex(ALG_CENTER,20,70,"SIM CARD BUSY OR KEY ERROR");
	}
	lcd_flip();
}

unsigned
get_file_size_gprs (const char * file_name)
{
	struct stat sb;

	if (stat (file_name, & sb) != 0) {
		fprintf (stderr, "'stat' failed for '%s': %s.\n",
				file_name, strerror (errno));
		exit (EXIT_FAILURE);
	}

	CstcLog_printf("THREAD::sb.st_size %d", sb.st_size);
	return sb.st_size;
}



//static unsigned char *
//void
int read_whole_file_gprs (const char * file_name, unsigned s, unsigned char * contents, long int *seekPtr)
{
	//	unsigned s;
	//	unsigned char * contents;
	FILE * f;
	size_t bytes_read;
	int status;
#if 0
	int total_chunk_length =0,chunk_data_buff_len=0;
	unsigned char * pcontent = contents;
	long int seekPtr = 0;
#endif
	//	s = get_file_size_gprs(file_name);
	//	contents = malloc (s + 1);
	if (! contents) {
		CstcLog_printf ( "Not enough memory.\n");
		//		exit (EXIT_FAILURE);
		return 0;
	}
	CstcLog_printf ( "Enough memory.\n");
	//
	//	if(strcmp(file_name,GPRS_DATA_UPLOAD_WAYBILL_FILE_ADDR) == 0)
	//	{
	//		pthread_mutex_lock(&upload_semaphore_waybill);
	//	}
	//	else if(strcmp(file_name,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
	//	{
	//		pthread_mutex_lock(&upload_semaphore_schedule);
	//	}

	f = fopen (file_name, "r");
	if (! f) {
		CstcLog_printf ( "Could not open '%s': %s.\n", file_name,strerror (errno));
		//		exit (EXIT_FAILURE);
		return 0;
	}
	CstcLog_printf ( "open file.\n");
#if CHUNK_GPRS_TKT_DATA
	fseek(f,*seekPtr,SEEK_SET);
	bytes_read = fread (contents, sizeof (unsigned char), s, f);
	*seekPtr=ftell(f);
	CstcLog_printf ( "bytes_read from file.\n");
#else
	bytes_read = fread (contents, sizeof (unsigned char), s, f);
#endif
#if 0
	do{
		if(chunk_data_buff_len>0)
			pcontent=pcontent+chunk_data_buff_len;  // given pointer is incremented with the received data length
		chunk_data_buff_len=0;                  // received data length is cleared.
		bytes_read = getdelim(pcontent,&chunk_data_buff_len,'|',f); // read data upto delimiter
		CstcLog_printf ("bytes_read = %u",bytes_read);
		CstcLog_printf ("chunk_data_buff_len = %u : %s",chunk_data_buff_len,pcontent);
		total_chunk_length += chunk_data_buff_len;   // chunk read size is added to total data read size.
		CstcLog_printf ("total_chunk_length = %u",total_chunk_length);
		seekPtr=ftell(f);
		if((s-total_chunk_length)<chunk_data_buff_len)
		{
			pcontent='\0';
			break;
		}
	}while(total_chunk_length<s);
#endif
	CstcLog_printf ("bytes_read = %u",bytes_read);
	if (bytes_read != s) {
		CstcLog_printf ( "Short read of '%s': expected %d bytes "
				"but got %d: %s.\n", file_name, s, bytes_read,
				strerror (errno));
		//		exit (EXIT_FAILURE);
		return 0;
	}
	fflush(f);
	status = fclose (f);
	//
	//	if(strcmp(file_name,GPRS_DATA_UPLOAD_WAYBILL_FILE_ADDR) == 0)
	//	{
	//		pthread_mutex_unlock(&upload_semaphore_waybill);
	//	}
	//	else if(strcmp(file_name,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
	//	{
	//		pthread_mutex_unlock(&upload_semaphore_schedule);
	//	}

	f = NULL;
	CstcLog_printf ( "file closed.\n");
	if (status != 0) {
		CstcLog_printf ( "Error closing '%s': %s.\n", file_name,
				strerror (errno));
		//		exit (EXIT_FAILURE);
		return 0;
	}
	//    contents[(Total_packet_length+33-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+CHECKSUM_LEN))]='\0';
	//	contents[bytes_read]='\0';
	CstcLog_printf ( "contents %s", contents);

	return 1; // contents;
}

int socket_client_demo(void)
{
	CURL *curl;
	CURLcode res;
	FILE *check_file =  NULL;
	char curl_url_string[50]={'\0'};
	char *bTemp=NULL;
	char response_read[20]={'\0'};
	int /*ret=0,*/ val=-1;
	int data_len =0;
	int misc_length=0, start_length=0;
	unsigned int data_buff_len =0;
	char *sn = NULL;
	int nbytes = 10;
	uint32_t tamper_status=-1;
	int funcRet =0,test = -1;
	int signal_num=0;
	depot_struct depot_struct_local_var;
	int temp_test=-1;
	int last_read_byte=0;

	CstcLog_printf("THREAD::***************** SOCKET CLIENT DEMO **************************");

	// if gprs_ip is present in waybill_master take that else gprs_ip will be fetched from depot table.
	if(!get_depot_details(&depot_struct_local_var))
	{
		CstcLog_printf("OTA::Failed to get depot details");
		if(waybill_master_struct_var.GPRS_IP!=NULL)   // check whether ip present in waybill_master
		{
			if(0<strlen(waybill_master_struct_var.GPRS_IP))
			{
				memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
				memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
				depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
				CstcLog_printf("OTA::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
			}
			else
			{
				CstcLog_printf("OTA::GPRS IP has 0 length in Waybill");
				memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
			}
		}
		else
		{
			CstcLog_printf("OTA::GPRS IP is NULL in Waybill");
			memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
		}
	}

	data_buff_len = strlen(Databuff);
	CstcLog_printf("THREAD::data_buff_len = %d",data_buff_len);

	if(data_buff_len > 0)
	{
		CstcLog_printf("THREAD::content size = %d",data_buff_len);
		test = sys_battery_info(&battery_info);
		funcRet = ped_get_status(&tamper_status);
		if(funcRet==PED_RET_OK)
		{
			CstcLog_printf("THREAD::Success funcRet = %d",funcRet);
			CstcLog_printf("THREAD::Success tamper_status = %d",tamper_status); //possible bug
		}
		else
		{
			CstcLog_printf("THREAD::Failed funcRet = %d",funcRet);
			CstcLog_printf("THREAD::Failed tamper_status = %d",tamper_status);
		}
		CstcLog_printf("test = %d",test);

		if(0==wnet_signal(&signal_num))
		{
			CstcLog_printf("THREAD::Signal strength: %d",signal_num);
		}
		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
		{
			CstcLog_printf("poor signal...");
//			return 0;
		}

		sn =(char *)malloc(nbytes*sizeof(char));
		memset(sn,0,nbytes);

		UART_get_machine_info(sn,nbytes);
		CstcLog_printf("THREAD::sn = %s",sn);
		CstcLog_printf("THREAD::nbytes = %d",nbytes);

		CstcLog_printf("THREAD::signal_strength = %d battery_status=%d %\n",signal_num,battery_info.percent);

		misc_length = get_formatted_string_length("&bstat=%d&tampstat=%d&gprssig=%d\r\n",battery_info.percent,tamper_status,signal_num);
		start_length = get_formatted_string_length("deviceid=%s&data=",sn);

		CstcLog_printf("THREAD::misc_length: %d , start_length %d ",misc_length, start_length);

		data_len = strlen(Databuff) + strlen("&mode=1") + start_length + misc_length;

		CstcLog_printf("THREAD::data_len: %d", data_len);

		bTemp = (char*)malloc((data_len+1)*sizeof(char));
		memset(bTemp,0,(data_len+1));

		dynamic_printf(bTemp,"deviceid=%s&data=%s",sn,Databuff);
		dynamic_strcat(bTemp,"&mode=1&bstat=%d&tampstat=%d&gprssig=%d\r\n",battery_info.percent,tamper_status,signal_num);

		CstcLog_printf("THREAD::content size = %d",data_buff_len);
		CstcLog_printf("THREAD::content tp be sent = %s",bTemp);

		/**********curl initializing******/

		curl = curl_easy_init();
		if(curl==NULL)
		{
			CstcLog_printf("CURL::Failed to initialise curl");

			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}
		CstcLog_printf("CURL::Initialised curl successfully");

//		sprintf(curl_url_string,"http://%s:8080/its/ticketUpload.action",depot_struct_local_var.GPRS_IP);
		sprintf(curl_url_string,"http://%s/log.php",depot_struct_local_var.GPRS_IP);


		CstcLog_printf_1("CURL::CONNECTING TO %s", curl_url_string);

		curl_easy_setopt(curl, CURLOPT_URL, curl_url_string);

		CstcLog_printf("CURL::Sending Data...");

		/*****************curl post***************/

		CstcLog_printf_1("CURL::String %s StringLen :%d",bTemp,strlen(bTemp));
		//		display_popup("Going to send data");
		val= curl_easy_setopt(curl, CURLOPT_POSTFIELDS,bTemp);
		CstcLog_printf("CURL::Posting finished...returned val = %d",val);

		/*******************check response********************/

		check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");

		CstcLog_printf("in file write mode");

		temp_test = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);

		CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test  %d",temp_test);

		if(temp_test != 0)  //Curl returns 0 on success
		{
			CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test in Fail %d",temp_test);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		temp_test=-1;
		temp_test = curl_easy_setopt(curl, CURLOPT_WRITEDATA,check_file);
		CstcLog_printf("THREAD::CURLOPT_WRITEDATA temp_test: %d",temp_test);

		if(temp_test != 0)
		{
			CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test in Fail %d",temp_test);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;

		}

		if(0==wnet_signal(&signal_num))
		{
			CstcLog_printf("THREAD::Signal strength 2: %d",signal_num);
		}
//		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
//		{
//			CstcLog_printf("poor signal...2");
//			if(curl !=NULL)
//			{
//				curl_easy_cleanup(curl);
//				curl=NULL;
//			}
//			if(bTemp!=NULL)
//			{
//				free(bTemp);
//				bTemp=NULL;
//			}
//			if(sn!=NULL)
//			{
//				free(sn);
//				sn=NULL;
//			}
//			return 0;
//		}

		/* complete within 20 seconds */
		  curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);

		res = curl_easy_perform(curl);

		if(res != CURLE_OK)
		{
			fprintf(stderr, "\nCURL::curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		CstcLog_printf("CURL::curl_easy_perform() succeeded");

		fflush(check_file);
		fclose(check_file);

		/*****************************************/

		/* Read data */
		check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"r");

		last_read_byte = get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR);
		CstcLog_printf("CURL::response_read = %s", response_read);

		if((last_read_byte > 0) && (last_read_byte < lAST_READ_COUNT))
		{
			fread(response_read,last_read_byte, 1, check_file);

			fflush(check_file);
			fclose(check_file);
		}
		else
		{
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			fflush(check_file);
			fclose(check_file);

			check_file = NULL;
			/****delete contents***/
			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
			fflush(check_file);
			fclose(check_file);
			check_file = NULL;
			/*********************/
			return 0;
		}

		CstcLog_printf("CURL::response_read redbug= %s", response_read);

		CstcLog_printf("CURL::last_read_byte = %d", last_read_byte);

		if(last_read_byte < lAST_READ_COUNT)
		{
			if(strstr(response_read,"DATA RECEIVED"))
			{
				CstcLog_printf("CURL::DATA RECEIVED");
				DbInterface_transaction_begin("BEGIN TRANSACTION;",1);
				CstcLog_printf("CURL::download_response_struct_var.end_tkt_cnt = %s", download_response_struct_var.end_tkt_cnt);
				CstcLog_printf("CURL:: ticket data string sent successfully......");
				if(update_upload_flag(atoi(download_response_struct_var.end_tkt_cnt),0))
					DbInterface_transaction_commit("COMMIT;",1);
				else
					DbInterface_transaction_commit("ROLLBACK;",1);

				memset(response_read,0,20);

				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;
				}
				if(bTemp!=NULL)
				{
					free(bTemp);
					bTemp=NULL;
				}
				if(sn!=NULL)
				{
					free(sn);
					sn=NULL;
				}
				/****delete contents***/
				check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
				fflush(check_file);
				fclose(check_file);
				check_file = NULL;
				/*********************/
				return 1;
			}
			else
			{
				CstcLog_printf("CURL::Fail To match");
				memset(response_read,0,20);
				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;
				}
				if(bTemp!=NULL)
				{
					free(bTemp);
					bTemp=NULL;
				}
				if(sn!=NULL)
				{
					free(sn);
					sn=NULL;
				}
				/****delete contents***/
				check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
				fflush(check_file);
				fclose(check_file);
				check_file = NULL;
				/*********************/
				return 0;
			}
		}
		else
		{
			CstcLog_printf("CURL::Error From Server");
			memset(response_read,0,20);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			/****delete contents***/
			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
			fflush(check_file);
			fclose(check_file);
			check_file = NULL;
			/*********************/
			return 0;
		}

		CstcLog_printf("CURL::curl_easy_perform() heererer....");

		if(curl !=NULL)
		{
			curl_easy_cleanup(curl);
			curl=NULL;
		}
	}
	else
	{
		CstcLog_printf("THREAD::BUFFER IS EMPTY");

		if(bTemp!=NULL)
		{
			free(bTemp);
			bTemp=NULL;
		}
		if(sn!=NULL)
		{
			free(sn);
			sn=NULL;
		}
		return 0;
	}
	if(bTemp!=NULL)
	{
		free(bTemp);
		bTemp=NULL;
	}
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	CstcLog_printf("THREAD::returning ...........");

	return 0;
}

int upload_waybill_string(void)
{
	CURL *curl;
	CURLcode res;
	FILE *check_file =  NULL;
	char curl_url_string[50]={'\0'};
	char *bTemp=NULL;
	char response_read[20]={'\0'};
	int /*ret=0,*/ val=-1,data_len =0;
	int misc_length=0, start_length=0;
	unsigned int data_buff_len =0;
	char *sn = NULL;
	int nbytes = 10;
	uint32_t tamper_status=-1;
	int funcRet =0,test = -1;
	int signal_num=0;
	depot_struct depot_struct_local_var;
	int temp_test=-1;
	int last_read_byte=0;

	CstcLog_printf("THREAD::***************** upload_waybill_string **************************");

	if(!get_depot_details(&depot_struct_local_var))
	{
		CstcLog_printf("THREAD::Failed to get depot details");
		if(waybill_master_struct_var.GPRS_IP!=NULL)
		{
			if(0<strlen(waybill_master_struct_var.GPRS_IP))
			{
				memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
				memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
				depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
				CstcLog_printf("THREAD::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
			}
			else
			{
				CstcLog_printf("THREAD::GPRS IP has 0 length in Waybill");
				memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
			}
		}
		else
		{
			CstcLog_printf("THREAD::GPRS IP is NULL in Waybill");
			memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
		}
	}

	data_buff_len = strlen(Databuff);

	if(data_buff_len > 0)
	{
		CstcLog_printf("THREAD::content size = %d",data_buff_len);

		test = sys_battery_info(&battery_info);
		funcRet = ped_get_status(&tamper_status);
		if(funcRet==PED_RET_OK)
		{
			CstcLog_printf("THREAD::Success funcRet = %d",funcRet);
			CstcLog_printf("THREAD::Success tamper_status = %d",tamper_status); //possible bug
		}
		else
		{
			CstcLog_printf("THREAD::Failed funcRet = %d",funcRet);
			CstcLog_printf("THREAD::Failed tamper_status = %d",tamper_status);
		}
		CstcLog_printf("test = %d",test);
		if(0==wnet_signal(&signal_num))
		{
			CstcLog_printf("THREAD::Signal strength: %d",signal_num);
		}
//		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
//		{
//			CstcLog_printf("poor signal....");
//			return 0;
//		}

		sn =(char *)malloc(nbytes*sizeof(char));
		memset(sn,0,nbytes);

		UART_get_machine_info(sn,nbytes);
		CstcLog_printf("THREAD::sn = %s",sn);
		CstcLog_printf("THREAD::nbytes = %d",nbytes);

		CstcLog_printf("THREAD::signal_strength = %d battery_status=%d %\n",signal_num,battery_info.percent);

		misc_length = get_formatted_string_length("&bstat=%d&tampstat=%d&gprssig=%d\r\n",battery_info.percent,tamper_status,signal_num);
		start_length = get_formatted_string_length("deviceid=%s&data=",sn);

		CstcLog_printf("THREAD::misc_length: %d , start_length %d ",misc_length, start_length);

		data_len = data_buff_len + strlen("&mode=2") + start_length + misc_length;

		CstcLog_printf("THREAD::data_len: %d", data_len);

		bTemp = (char*)malloc((data_len+1)*sizeof(char));
		memset(bTemp,0,(data_len+1));

		dynamic_printf(bTemp,"deviceid=%s&data=%s",sn,Databuff);
		dynamic_strcat(bTemp,"&mode=2&bstat=%d&tampstat=%d&gprssig=%d\r\n",battery_info.percent,tamper_status,signal_num);

		CstcLog_printf("THREAD::content size = %d content =  %s",data_buff_len,bTemp);

		/**********curl initializing******/

		curl = curl_easy_init();
		if(curl==NULL)
		{
			CstcLog_printf("CURL::Failed to initialise curl");
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}
		CstcLog_printf("CURL::Initialised curl successfully");

//		sprintf(curl_url_string,"http://%s:8080/its/ticketUpload.action",depot_struct_local_var.GPRS_IP);
		sprintf(curl_url_string,"http://%s/log.php",depot_struct_local_var.GPRS_IP);


		CstcLog_printf("CURL::CONNECTING TO %s", curl_url_string);

		curl_easy_setopt(curl, CURLOPT_URL, curl_url_string);

		CstcLog_printf("CURL::Sending Data...");

		/*****************curl post***************/

		CstcLog_printf("CURL::String %s StringLen :%d",bTemp,strlen(bTemp));

		val= curl_easy_setopt(curl, CURLOPT_POSTFIELDS,bTemp);
		CstcLog_printf("CURL::Posting finished...returned val = %d",val);

		CstcLog_printf("CURL::curl_easy_perform() succeeded");

		/*******************check response********************/

		check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");

		CstcLog_printf("in file write mode");

		temp_test = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);

		if(temp_test != 0)  //Curl returns 0 on success
		{
			CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test in Fail %d",temp_test);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		temp_test = -1;
		temp_test = curl_easy_setopt(curl, CURLOPT_WRITEDATA,check_file);
		if(temp_test != 0)  //Curl returns 0 on success
		{
			CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test in Fail %d",temp_test);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}
		if(0==wnet_signal(&signal_num))
		{
			CstcLog_printf("THREAD::Signal strength 2.: %d",signal_num);
		}
//		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
//		{
//			CstcLog_printf("poor signal....2");
//			if(curl !=NULL)
//			{
//				curl_easy_cleanup(curl);
//				curl=NULL;
//			}
//			if(bTemp!=NULL)
//			{
//				free(bTemp);
//				bTemp=NULL;
//			}
//			if(sn!=NULL)
//			{
//				free(sn);
//				sn=NULL;
//			}
//			return 0;
//		}

		/* complete within 20 seconds */
		  curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);

		res = curl_easy_perform(curl);

		if(res != CURLE_OK)
		{
			fprintf(stderr, "\nCURL::curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		CstcLog_printf("CURL::curl_easy_perform() succeeded");

		fflush(check_file);
		fclose(check_file);

		/*****************************************/

		/* Read data */
		check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"r");
		//fread(response_read, get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR), 1, check_file);

		last_read_byte = get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR);
		CstcLog_printf("CURL::response_read = %s", response_read);

		if((last_read_byte > 0) && (last_read_byte < lAST_READ_COUNT))
		{
			fread(response_read, last_read_byte, 1, check_file);

			fflush(check_file);
			fclose(check_file);
		}
		else
		{
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			fflush(check_file);
			fclose(check_file);

			check_file = NULL;
			/****delete contents***/
			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
			fflush(check_file);
			fclose(check_file);
			check_file = NULL;
			/*********************/

			return 0;
		}

		CstcLog_printf("CURL::response_read = %s", response_read);

		if(last_read_byte < lAST_READ_COUNT)
		{
			if(strstr(response_read,"DATA RECEIVED"))
			{
				CstcLog_printf("CURL::DATA RECEIVED");

				pthread_mutex_lock(&upload_semaphore_waybill);
				check_file = fopen(WAYBILL_RESPONSE_FILE_ADDR,"w");
				fprintf(check_file, "Y");
				fflush(check_file);
				fclose(check_file);
				pthread_mutex_unlock(&upload_semaphore_waybill);
				check_file=NULL;

				memset(response_read,0,20);

				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;
				}
				if(bTemp!=NULL)
				{
					free(bTemp);
					bTemp=NULL;
				}
				if(sn!=NULL)
				{
					free(sn);
					sn=NULL;
				}
				/****delete contents***/
				check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
				fflush(check_file);
				fclose(check_file);
				check_file = NULL;
				/*********************/
				return 1;
			}
			else
			{
				CstcLog_printf("CURL::Failed To match");
				memset(response_read,0,20);
				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;
				}
				if(bTemp!=NULL)
				{
					free(bTemp);
					bTemp=NULL;
				}
				if(sn!=NULL)
				{
					free(sn);
					sn=NULL;
				}
				/****delete contents***/
				check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
				fflush(check_file);
				fclose(check_file);
				check_file = NULL;
				/*********************/
				return 0;
			}
		}
		else
		{

			CstcLog_printf("CURL::Error From Server");
			memset(response_read,0,20);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			/****delete contents***/
			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
			fflush(check_file);
			fclose(check_file);
			check_file = NULL;
			/*********************/
			return 0;
		}
		CstcLog_printf("CURL::curl_easy_perform() heererer....");

		if(curl !=NULL)
		{
			curl_easy_cleanup(curl);
			curl=NULL;
		}
	}
	else
	{
		CstcLog_printf("THREAD::BUFFER IS EMPTY");
		if(bTemp!=NULL)
		{
			free(bTemp);
			bTemp=NULL;
		}
		if(sn!=NULL)
		{
			free(sn);
			sn=NULL;
		}
		return 0;
	}
	if(bTemp!=NULL)
	{
		free(bTemp);
		bTemp=NULL;
	}
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	CstcLog_printf("THREAD::returning ...........");

	return 0;
}

int send_schedule_string(void)
{
	CURL *curl;
	CURLcode res;
	FILE *check_file =  NULL;
	char curl_url_string[50]={'\0'};
	char *bTemp=NULL;
	char response_read[20] = {'\0'};
	int /*ret=0,*/ val=-1,data_len =0;
	uint32_t tamper_status=-1;
	int funcRet =0,test = -1;
	char *sn = NULL;
	int nbytes = 10;
	int misc_length=0, start_length=0;
	int signal_num=0;

	depot_struct depot_struct_local_var;
	int temp_test=-1;
	int last_read_byte=0;

	CstcLog_printf("THREAD::*************In send_schedule_string****************");

	if(!get_depot_details(&depot_struct_local_var))
	{
		CstcLog_printf("THREAD::Failed to get depot details");
		if(waybill_master_struct_var.GPRS_IP!=NULL)
		{
			if(0<strlen(waybill_master_struct_var.GPRS_IP))
			{
				memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
				memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
				depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
				CstcLog_printf("THREAD::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
			}
			else
			{
				CstcLog_printf("THREAD::GPRS IP has 0 length in Waybill");
				memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
			}
		}
		else
		{
			CstcLog_printf("THREAD::GPRS IP is NULL in Waybill");
			memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
		}
	}
	CstcLog_printf("THREAD::sn = %s",sn);
	CstcLog_printf("THREAD::nbytes = %d",nbytes);

	CstcLog_printf("THREAD::Databuff..... = %s",Databuff);

	if(strlen(Databuff) > 0)
	{
		test = sys_battery_info(&battery_info);
		funcRet = ped_get_status(&tamper_status);
		if(funcRet==PED_RET_OK)
		{
			CstcLog_printf("THREAD::Success funcRet = %d",funcRet);
			CstcLog_printf("THREAD::Success tamper_status = %d",tamper_status); //possible bug
		}
		else
		{
			CstcLog_printf("THREAD::Failed funcRet = %d",funcRet);
			CstcLog_printf("THREAD::Failed tamper_status = %d",tamper_status);
		}
		CstcLog_printf("test = %d",test);
		if(0==wnet_signal(&signal_num))
		{
			CstcLog_printf("THREAD::Signal strength: %d",signal_num);
		}
//		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
//		{
//			CstcLog_printf("poor signal......");
//			return 0;
//		}

		sn =(char *)malloc(nbytes*sizeof(char));
		memset(sn,0,nbytes);

		UART_get_machine_info(sn,nbytes);
		CstcLog_printf("THREAD::sn = %s",sn);
		CstcLog_printf("THREAD::nbytes = %d",nbytes);

		CstcLog_printf("THREAD::signal_strength = %d battery_status=%d %\n",signal_num,battery_info.percent);

		misc_length = get_formatted_string_length("&bstat=%d&tampstat=%d&gprssig=%d\r\n",battery_info.percent,tamper_status,signal_num);
		start_length = get_formatted_string_length("deviceid=%s&data=",sn);

		CstcLog_printf("THREAD::misc_length: %d , start_length %d ",misc_length, start_length);

		data_len = strlen(Databuff) + strlen("&mode=3") + start_length + misc_length;

		CstcLog_printf("THREAD::data_len: %d", data_len);

		bTemp = (char*)malloc((data_len+1)*sizeof(char));
		memset(bTemp,0,(data_len+1));

		dynamic_printf(bTemp,"deviceid=%s&data=%s",sn,Databuff);
		dynamic_strcat(bTemp,"&mode=3&bstat=%d&tampstat=%d&gprssig=%d\r\n",battery_info.percent,tamper_status,signal_num);

		CstcLog_printf("THREAD::content tp be sent = %s",bTemp);

		/**********curl initializing******/

		curl = curl_easy_init();
		if(curl==NULL)
		{
			CstcLog_printf("CURL::Failed to initialise curl");
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}
		CstcLog_printf("CURL::Initialised curl successfully");

//		sprintf(curl_url_string,"http://%s:8080/its/ticketUpload.action",depot_struct_local_var.GPRS_IP);
		sprintf(curl_url_string,"http://%s/log.php",depot_struct_local_var.GPRS_IP);


		CstcLog_printf("CURL::CONNECTING TO %s", curl_url_string);

		curl_easy_setopt(curl, CURLOPT_URL, curl_url_string);

		CstcLog_printf("CURL::Sending Data...");

		/*****************curl post***************/

		CstcLog_printf("CURL::String %s StringLen :%d",bTemp,strlen(bTemp));

		val= curl_easy_setopt(curl, CURLOPT_POSTFIELDS,bTemp);
		CstcLog_printf("CURL::Posting finished...returned val = %d",val);

		CstcLog_printf("CURL::curl_easy_perform() succeeded");

		/*******************check response********************/

		check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");

		CstcLog_printf("in file write mode");
		//temp_test = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,write_data_curl);

		temp_test = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);
		if(temp_test != 0)  //Curl returns 0 on success
		{
			CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test in Fail %d",temp_test);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		temp_test=-1;
		temp_test = curl_easy_setopt(curl, CURLOPT_WRITEDATA,check_file);
		if(temp_test != 0)  //Curl returns 0 on success
		{
			CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test in Fail %d",temp_test);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		if(0==wnet_signal(&signal_num))
		{
			CstcLog_printf("THREAD::Signal strength.. %d",signal_num);
		}
//		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
//		{
//			CstcLog_printf("poor signal........");
//			curl_easy_cleanup(curl);
//			if(curl !=NULL)
//			{
//				curl_easy_cleanup(curl);
//				curl=NULL;
//			}
//			if(bTemp!=NULL)
//			{
//				free(bTemp);
//				bTemp=NULL;
//			}
//			if(sn!=NULL)
//			{
//				free(sn);
//				sn=NULL;
//			}
//			return 0;
//		}

		/* complete within 20 seconds */
		  curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);

		res = curl_easy_perform(curl);

		if(res != CURLE_OK)
		{
			fprintf(stderr, "\nCURL::curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		CstcLog_printf("CURL::curl_easy_perform() succeeded");

		fflush(check_file);
		fclose(check_file);

		/*****************************************/

		/* Read data */
		check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"r");

		//fread(response_read, get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR), 1, check_file);
		last_read_byte = get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR);
		CstcLog_printf("CURL::response_read = %s", response_read);

		if((last_read_byte > 0) && (last_read_byte < lAST_READ_COUNT))
		{
			fread(response_read, last_read_byte, 1, check_file);

			fflush(check_file);
			fclose(check_file);
		}
		else
		{
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			fflush(check_file);
			fclose(check_file);

			check_file = NULL;
			/****delete contents***/
			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
			fflush(check_file);
			fclose(check_file);
			check_file = NULL;
			/*********************/
			return 0;
		}

		CstcLog_printf("CURL::response_read = %s", response_read);
		if(last_read_byte < lAST_READ_COUNT)
		{
			if(strstr(response_read,"DATA RECEIVED"))
			{
				CstcLog_printf("CURL::DATA RECEIVED");
				DbInterface_transaction_begin("BEGIN TRANSACTION;",1);
				CstcLog_printf("CURL:: Schedule string sent successfully......");
				if(update_upload_flag(atoi(download_response_struct_var.end_tkt_cnt),1))
					DbInterface_transaction_commit("COMMIT;",1);
				else
					DbInterface_transaction_commit("ROLLBACK;",1);

				memset(response_read,0,20);

				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;
				}

				if(sn!=NULL)
				{
					free(sn);
					sn=NULL;
				}
				if(bTemp!=NULL)
				{
					free(bTemp);
					bTemp=NULL;
				}
				/****delete contents***/
				check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
				fflush(check_file);
				fclose(check_file);
				check_file = NULL;
				/*********************/

				return 1;
			}
			else
			{
				CstcLog_printf("CURL::Fail To match");
				memset(response_read,0,20);
				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;
				}
				if(bTemp!=NULL)
				{
					free(bTemp);
					bTemp=NULL;
				}
				if(sn!=NULL)
				{
					free(sn);
					sn=NULL;
				}
				/****delete contents***/
				check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
				fflush(check_file);
				fclose(check_file);
				check_file = NULL;
				/*********************/

				return 0;
			}
		}
		else
		{
			CstcLog_printf("CURL::Error From Server");
			memset(response_read,0,20);
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			/****delete contents***/
			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
			fflush(check_file);
			fclose(check_file);
			check_file = NULL;
			/*********************/
			return 0;

		}
	}
	else
	{
		CstcLog_printf("THREAD::BUFFER IS EMPTY");
		if(bTemp!=NULL)
		{
			free(bTemp);
			bTemp=NULL;
		}
		if(sn!=NULL)
		{
			free(sn);
			sn=NULL;
		}
		return 0;
	}
	if(bTemp!=NULL)
	{
		free(bTemp);
		bTemp=NULL;
	}
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	CstcLog_printf("THREAD::returning ...........");

	return 0;
}

int socket_client_demo_auth()
{
	int iclient_socket = -1;
	char bTemp[1024];
	unsigned int  iTemp=0;
	//    int error=0;
	//    int j = 0;
	//    int k = 0;
	//	int RowCount=0;
	//    char tempCh;
	//    char tempChID[2];
	//    int req_flag = 1;
	//    int Transaction_No=0;
	char *sn = NULL;
	int signal_num=0;
	uint32_t tamper_status=-1;
	int nbytes = 10,tamper_status_len=1,battery_info_len=3,gprs_sig_len=3,funcRet =0,test = -1;
	depot_struct depot_struct_local_var;
	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);

	CstcLog_printf("THREAD:: AUTH > \n\n***************** socket_client_demo_auth *******************\n\n");
	//	lcd_flip();


	//   	RowCount=DbInterface_Get_Row_Count("transaction_log where upload_flag='N'",37);
	//   	if(RowCount>0)
	//{
	//   		get_gprs_start_end_db_data(download_response_struct_var.start_tkt_cnt,download_response_struct_var.end_tkt_cnt);
	//   		Transaction_No=atoi(download_response_struct_var.start_tkt_cnt);
	//   		CstcLog_printf("Transaction_No = %d , Start ticket = %s , End Ticket = %s",Transaction_No,download_response_struct_var.start_tkt_cnt,download_response_struct_var.end_tkt_cnt);

	while(1)
	{
		CstcLog_printf("THREAD:: AUTH > Inside while 1");
		//   			if(Transaction_No > atoi(download_response_struct_var.end_tkt_cnt))
		//   				break;

		if(!get_depot_details(&depot_struct_local_var))
		{
			CstcLog_printf("OTA::Failed to get depot details");
			if(waybill_master_struct_var.GPRS_IP!=NULL)
			{
				if(0<strlen(waybill_master_struct_var.GPRS_IP))
				{
					memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
					memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
					depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
					CstcLog_printf("OTA::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
				}
				else
				{
					CstcLog_printf("OTA::GPRS IP has 0 length in Waybill");
					memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
				}
			}
			else
			{
				CstcLog_printf("OTA::GPRS IP is NULL in Waybill");
				memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
			}
		}
		//		strcpy(SERVER_ADDR,depot_struct_local_var.GPRS_IP);
		//		SERVER_ADDR[strlen(depot_struct_local_var.GPRS_IP)]='\0';


		//	CstcLog_printf("THREAD:: AUTH > \nconnect to %s:%d...", SERVER_ADDR, SERVER_PORT);

		iclient_socket = tcp_connect(depot_struct_local_var.GPRS_IP, SERVER_PORT);
		if (iclient_socket < 0)
		{
			CstcLog_printf("THREAD:: AUTH > \nConnect failed.");
			Show_Error_Msg("Connecting To Server\n Failed");
		}
		else
		{
			Show_Error_Msg("Authenticating User...\n Please wait....");
			sprintf(Data_Buff_Auth,"%s,%s",username,pswd);
			CstcLog_printf("THREAD:: AUTH > Data_Buff_Auth %s length =%d", Data_Buff_Auth,strlen(Data_Buff_Auth));

			CstcLog_printf("THREAD:: AUTH > violet gazelle");
			UART_get_machine_info(sn,nbytes);
			CstcLog_printf("THREAD:: AUTH > sn = %s",sn);
			CstcLog_printf("THREAD:: AUTH > nbytes = %d",nbytes);

			test = sys_battery_info(&battery_info);
			funcRet = ped_get_status(&tamper_status);
			if(funcRet==PED_RET_OK)
			{
				CstcLog_printf("THREAD:: AUTH > Success funcRet = %d",funcRet);
				CstcLog_printf("THREAD:: AUTH > Success tamper_status = %d",tamper_status); //possible bug
			}
			else
			{
				CstcLog_printf("THREAD:: AUTH > Failed funcRet = %d",funcRet);
				CstcLog_printf("THREAD:: AUTH > Failed tamper_status = %d",tamper_status);
			}
			CstcLog_printf("THREAD:: AUTH > test = %d",test);
			if(0==wnet_signal(&signal_num))
			{
				CstcLog_printf("THREAD:: AUTH > Current Signal strength: %d",signal_num);
			}
#if 0
			if(!get_depot_details(&depot_struct_local_var))
			{
				CstcLog_printf("OTA::Failed to get depot details");
				if(waybill_master_struct_var.GPRS_IP!=NULL)
				{
					if(0<strlen(waybill_master_struct_var.GPRS_IP))
					{
						memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
						memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
						depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
						CstcLog_printf("OTA::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
					}
					else
					{
						CstcLog_printf("OTA::GPRS IP has 0 length in Waybill");
						memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
					}
				}
				else
				{
					CstcLog_printf("OTA::GPRS IP is NULL in Waybill");
					memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
				}
			}
#endif
			//			sprintf((char*)bTemp,"POST /log.php HTTP/1.1\r\nHost: 223.30.102.30\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&data=%s&mode=4&bstat=%d&tampstat=%d&gprssig=%d\r\n",(strlen(Data_Buff_Auth)+nbytes+20+tamper_status_len+battery_info_len+gprs_sig_len+23),sn,Data_Buff_Auth,battery_info.percent,tamper_status,signal_num);
			sprintf((char*)bTemp,"POST /its/ticketUpload.action HTTP/1.1\r\nHost: %s\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&data=%s&mode=4&bstat=%d&tampstat=%d&gprssig=%d\r\n",depot_struct_local_var.GPRS_IP,(strlen(Data_Buff_Auth)+nbytes+20+tamper_status_len+battery_info_len+gprs_sig_len+23),sn,Data_Buff_Auth,battery_info.percent,tamper_status,signal_num);
			//sprintf((char*)bTemp,"POST /log.php HTTP/1.1\r\nHost: 223.30.102.30\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 32\r\n\r\ndeviceid=123&data=swapnil&mode=1\r\n");
			iTemp = strlen((char*)bTemp);
			CstcLog_printf("THREAD:: AUTH > bTemp = %s iTemp = %d",bTemp,iTemp);
			if (tcp_write(iclient_socket , bTemp, iTemp) < 0)
			{
				CstcLog_printf("THREAD:: AUTH > write connection was broken");
				//lcd_printf(ALG_LEFT, "write connection was broken");
				//lcd_flip();
				break;
			}
			else
			{
				usleep(500000);
				iTemp = sizeof(bTemp);
				if (tcp_read(iclient_socket, bTemp, &iTemp) < 0)
				{
					CstcLog_printf("THREAD:: AUTH > read connection was broken");
					//lcd_printf(ALG_LEFT,"read connection was broken");
					//lcd_flip();
					break;
				}
				else if (iTemp > 0)
				{
					CstcLog_printf("THREAD:: AUTH > Readed bTemp = %s iTemp = %d",bTemp,iTemp);
					if(strstr(bTemp,"DATA VERIFIED"))
					{
						CstcLog_printf("THREAD:: AUTH > String DATA VERIFIED received");
						Show_Error_Msg("User Authenticated");
						tcp_close(iclient_socket);
						if(sn!=NULL)
						{
							free(sn);
							sn=NULL;
						}
						return 1;
					}
					else
					{
						CstcLog_printf("THREAD:: AUTH > Fail To match");
						Show_Error_Msg("User Authentication\n Failed");
						tcp_close(iclient_socket);
						if(sn!=NULL)
						{
							free(sn);
							sn=NULL;
						}
						return 0;
					}
				}
			}
		}
	}
	tcp_close(iclient_socket);
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	Show_Error_Msg("User Authentication\n Failed");
	return 0;
}


//int online_schedule_update(int selection,char *selected_id)

#if 0
int online_schedule_update(int update_type,char *id_string,int index)
{

	CURL *curl;
	CURLcode res;
	FILE *check_file =  NULL;
	char curl_url_string[50]={'\0'};
	int temp_test=-1;
	int last_read_byte=0,val=0;


	int iclient_socket = -1;
	char bTemp[1024]={'\0'};
	char rbTemp[6000]={'\0'};
	unsigned int  iTemp=0;
	int j = 0,hd_len=0;
	char *sn = NULL;
	int nbytes = 10;
	int header_flag = 0;//,value_in_itemp = 0;
	int updated_flag = 0;
//	int content_lengths = 0;
	int key=0, ret =0;
	depot_struct depot_struct_local_var;
	char tempUpdateDate[(DATE_LEN+1)];
	char tempUpdateTime[(TIME_LEN+1)]={'\0'};

	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);

	CstcLog_printf("OTA::\n\n***************** SOCKET CLIENT **************************\n\n");

	get_schedule_type_info();
	get_waybill_details();
	if(!get_depot_details(&depot_struct_local_var))
	{
		CstcLog_printf("OTA::Failed to get depot details");
		if(waybill_master_struct_var.GPRS_IP!=NULL)
		{
			if(0<strlen(waybill_master_struct_var.GPRS_IP))
			{
				memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
				memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
				depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
				CstcLog_printf("OTA::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
			}
			else
			{
				CstcLog_printf("OTA::GPRS IP has 0 length in Waybill");
				memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
				depot_struct_local_var.GPRS_IP[7]='\0';
				display_popup("Err: Server IP\n Not Found");
				tcp_close(iclient_socket);
				if(sn!=NULL)
				{
					free(sn);
					sn =NULL;
				}
				return 0;
			}
		}
		else
		{
			CstcLog_printf("OTA::GPRS IP is NULL in Waybill");
			memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
			display_popup("Err: Server IP\n Not Found");
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			return 0;
		}
	}














/*	iclient_socket = tcp_connect(depot_struct_local_var.GPRS_IP, SERVER_PORT);
	if (iclient_socket < 0)
	{
		display_popup("Err: Could Not Connect\n To Server");
		CstcLog_printf("OTA::\nConnect failed.");
		tcp_close(iclient_socket);
		if(sn!=NULL)
		{
			free(sn);
			sn =NULL;
		}
		return 0;
	}*/
//	else
	{
		CstcLog_printf("CURL::Initialised curl successfully");

		//		display_popup("Connected To Server");
		UART_get_machine_info(sn,nbytes);
		CstcLog_printf("OTA::sn = %s",sn);
		CstcLog_printf("OTA::nbytes = %d",nbytes);


		ret = getUpdateDateTime(tempUpdateDate,tempUpdateTime,update_type,duty_master_struct_var.schedule_id,id_string); //error : ret=0
		CstcLog_printf("OTA::Update Date time present : %s %s    ret=%d",tempUpdateDate,tempUpdateTime,ret);


		switch(update_type)
		{
		case 0:
			CstcLog_printf("OTA::Case 0 : schedule_master update...");
			if(ret)
			{
				if((index >= 0) )//&& all_flags_struct_var.online_schedule_update_flag )
					//if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
//					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&scheduleid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->schedule_id,tempUpdateDate,tempUpdateTime,(full_duty_master_struct_var+index)->schedule_id,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
//					sprintf(bTemp,"POST /wget.php HTTP/1.1Host: 223.30.102.30Content-Type: application/x-www-form-urlencodedContent-Length: %ddeviceid=%s&depotid=%s&scheduleid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/content_lengths,sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->schedule_id,tempUpdateDate,tempUpdateTime,(full_duty_master_struct_var+index)->schedule_id,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);

					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
//					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
//					sprintf(bTemp,"POST /wget.php HTTP/1.1Host: %sContent-Type: application/x-www-form-urlencodedContent-Length: %ddeviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/depot_struct_local_var.GPRS_IP,content_lengths,sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);

					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}
			else
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//	if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}

			break;
		case 1:
			CstcLog_printf("OTA::Case 1 : route_header update...");

			if(ret)
			{
				if((index >= 0) )//&& all_flags_struct_var.online_schedule_update_flag )
					//				if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);

//					sprintf(bTemp,"deviceid=82245758&depotid=42&scheduleid=73&timestamp=18-06-2015 19:19:53&type=ROUTEH&data=5661&waybillid=T1115061800004&dutytimestamp=18-06-2015 19:19:53");

				}
			}
			else
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//				if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}
			break;
		case 2:
			CstcLog_printf("OTA::Case 2 : route_master update...");

			if(ret)
			{
				if((index >= 0) )//&& all_flags_struct_var.online_schedule_update_flag )
					//	if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}
			else
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//	if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}

			break;
		case 3:
			CstcLog_printf("OTA::Case 3 : rate_master update...");

			if(ret)
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//				if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}
			else
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//	if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}

			break;




		case 4:  // fare_chart - added.  //check it later.
			CstcLog_printf("OTA::Case 4 : fare_chart update...");

					if(ret)
					{
						if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
							//				if(all_flags_struct_var.tc_online_schedule_update_flag)
						{
							// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
							sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
						}
						else
						{
							// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
							sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
						}
					}
					else
					{
						if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
							//	if(all_flags_struct_var.tc_online_schedule_update_flag)
						{
							// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
							sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
						}
						else
						{
							// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
							sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
						}
					}

					break;

		}

//		strcpy(bTemp,"deviceid=82245757&depotid=42&timestamp=09-10-2014 12:04:20&type=ROUTEH&data=30&waybillid=4555&dutytimestamp=23-10-2015 12:36:18");


		iTemp = strlen((char*)bTemp);

		CstcLog_printf("OTA::iTemp = %d",iTemp);
		CstcLog_printf("OTA::data string to be posted on server for update check(bTemp)= %s",bTemp);
		//CstcLog_HexDump_1(bTemp,(iTemp));



		/**********curl initializing******/

		curl = curl_easy_init();
		if(curl==NULL)
		{
			CstcLog_printf("CURL::Failed to initialise curl");

			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		/*****************set the url of server to be connected with  ***************/

		sprintf(curl_url_string,"http://%s:8080/its/wget.action",depot_struct_local_var.GPRS_IP);

		CstcLog_printf("CURL::CONNECTING TO %s", curl_url_string);

		curl_easy_setopt(curl, CURLOPT_URL, curl_url_string);

		CstcLog_printf("CURL::Sending Data...");

		/*****************send the check string to server ***************/

		CstcLog_printf("CURL::String %s StringLen :%d",bTemp,strlen(bTemp));

		val= curl_easy_setopt(curl, CURLOPT_POSTFIELDS,bTemp);
		CstcLog_printf("CURL::Posting finished...returned val = %d",val);

		CstcLog_printf("CURL::curl_easy_perform() succeeded");







/*		if (tcp_write(iclient_socket , bTemp, iTemp) < 0)
		{
			display_popup("Err: Server Connection\n was broken");
			CstcLog_printf("OTA::write connection was broken");
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			return 0;
		}*/
		if(val!=0)
		{

			CstcLog_printf("CURL:: sending data to server failed..");
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}

			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;

		}
		else
		{
			display_popup("Request Sent To Server");
			CstcLog_printf("OTA::write successful....");
			iTemp = sizeof(rbTemp);
			header_flag = 0;
			//			puts("\n");
			do
			{

				memset(rbTemp,0,6000);
				iTemp = 6000;
				//value_in_itemp = OTA_CHUNK_BYTES;
				if(header_flag ==1)
				{
					hd_len = 0;
					//					usleep(10000);
				}
				else
				{
					usleep(700000);
				}
				CstcLog_printf("OTA::ITEMP BEFORE READING = %d",iTemp);

				/*******************get the respose string from a server and save it in a .txt file ********************/

						check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");

						CstcLog_printf("in file write mode");

						temp_test = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);

						if(temp_test != 0)  //Curl returns 0 on success
						{
							CstcLog_printf("CURL::storing response from server in txt file failed... ",temp_test);
							if(curl !=NULL)
							{
								curl_easy_cleanup(curl);
								curl=NULL;
							}

							if(sn!=NULL)
							{
								free(sn);
								sn=NULL;
							}
							return 0;
						}

						temp_test = -1;
						temp_test = curl_easy_setopt(curl, CURLOPT_WRITEDATA,check_file);
						if(temp_test != 0)  //Curl returns 0 on success
						{
							CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test in Fail %d",temp_test);
							if(curl !=NULL)
							{
								curl_easy_cleanup(curl);
								curl=NULL;
							}

							if(sn!=NULL)
							{
								free(sn);
								sn=NULL;
							}
							return 0;
						}



				//		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
				//		{
				//			CstcLog_printf("poor signal....2");
				//			if(curl !=NULL)
				//			{
				//				curl_easy_cleanup(curl);
				//				curl=NULL;
				//			}
				//			if(bTemp!=NULL)
				//			{
				//				free(bTemp);
				//				bTemp=NULL;
				//			}
				//			if(sn!=NULL)
				//			{
				//				free(sn);
				//				sn=NULL;
				//			}
				//			return 0;
				//		}

						res = curl_easy_perform(curl);

						if(res != CURLE_OK)
						{
							fprintf(stderr, "\nCURL::curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
							if(curl !=NULL)
							{
								curl_easy_cleanup(curl);
								curl=NULL;
							}

							if(sn!=NULL)
							{
								free(sn);
								sn=NULL;
							}
							return 0;
						}

						CstcLog_printf("CURL::curl_easy_perform() succeeded");

				//		fflush(check_file);
						fclose(check_file);


						/************************ Read .txt file data (i.e. data received from the server strored in txt file) ************************/

						check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"r");
						if(check_file==NULL)
						{
							CstcLog_printf("OTA: Error - file opening failed");
							if(curl !=NULL)
							{
								curl_easy_cleanup(curl);
								curl=NULL;
							}

							if(sn!=NULL)
							{
								free(sn);
								sn=NULL;
							}
							return 0;
						}
						//fread(rbTemp, get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR), 1, check_file);

						last_read_byte = get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR);

						CstcLog_printf("CURL::last_read_byte = %d", last_read_byte);

//						if((last_read_byte > 0) && (last_read_byte < lAST_READ_COUNT))
						{
							fread(rbTemp, last_read_byte, 1, check_file);

							fflush(check_file);
							fclose(check_file);
						}

						iTemp=strlen(rbTemp);
						CstcLog_printf("CURL::rbTemp = %s len=%d", rbTemp,iTemp);

	/*			if (tcp_read(iclient_socket, rbTemp, &iTemp) < 0)
				{

					display_popup("Err: Server Connection\n was broken");
					CstcLog_printf("OTA::read connection was broken");
					updated_flag = 0;
					break;

				}
				else*/
						if (iTemp > 0)
				{
					//CstcLog_HexDump_1(rbTemp,iTemp);
					//					puts(rbTemp);
					if(iTemp<6000)
						rbTemp[iTemp]='\0';
					else
						rbTemp[5999]='\0';
					CstcLog_printf("OTA::ITEMP AFTER READING = %d",iTemp);
					if(header_flag == 0)
					{
						hd_len = get_http_header_len(rbTemp);
						CstcLog_HexDump_1(&rbTemp[hd_len],(iTemp-hd_len));
						CstcLog_printf("OTA::HEADER LEN = %d " ,hd_len);
						//						if(hd_len==iTemp)
						//							iTemp_minus_hd_len=iTemp;
						//						else
						//							iTemp_minus_hd_len=iTemp-hd_len;

					}


					CstcLog_printf("OTA::Readed rbTemp = %s ",rbTemp);
					CstcLog_printf("OTA::Readed iTemp = %d",(iTemp-hd_len));//iTemp_minus_hd_len);

					CstcLog_HexDump_1(rbTemp,(iTemp));
					//					usleep(10000);


					ret = GPRS_compose_response_packet((char*)rbTemp, iclient_socket,(iTemp-hd_len),&j,header_flag);
					if(ret==3)
					{
						CstcLog_printf("OTA::Response done iTemp : %d",iTemp);
						display_popup("ALL DATA UPTO DATE");
						updated_flag = 3;
						break;
					}
					else if(ret==2)
					{
						CstcLog_printf("OTA::Response done iTemp : %d",iTemp);
						display_popup("No Update Found");
						updated_flag = 2;
						break;
					}
					else if(ret==1)
					{
						CstcLog_printf("OTA::Response done iTemp : %d",iTemp);
						display_popup("Update Found");
						updated_flag = 1;
						break;
					}
					else
					{
						CstcLog_printf("OTA::Response done iTemp : %d",iTemp);
						updated_flag = 0;
						if(ret==0)
						{
							display_popup("Err: Received Data \nWas Not Proper");
							break;
						}
					}

					CstcLog_printf("OTA::2. size of bTemp : %d",sizeof(bTemp));
					//printf("2. rbTemp : %s",rbTemp);
					memset(rbTemp,0,6000);
					memset(bTemp,0,sizeof(bTemp));
					header_flag = 1;
					//iTemp=0;
				}

				key = 0;
				key = Kb_Get_Key_Sym_New_uart();
				if(key==KEY_SYM_ESCAPE)
					break;
				key = 0;
			}
			while(GPRS_global_iTemp < GPRS_Total_packet_length);//OTA_CHUNK_BYTES);
			//			while(iTemp>=OTA_CHUNK_BYTES);//OTA_CHUNK_BYTES);
		}
		//}
		memset(rbTemp,0,6000);
		memset(bTemp,0,sizeof(bTemp));
		header_flag = 0;
		if(updated_flag == 3)
		{
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			return 3;
		}
		else if(updated_flag == 2)
		{
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			return 2;
		}
		else if(updated_flag == 1)
		{
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			return 1;
		}
		else
		{
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			return 0;
		}
	}
	memset(rbTemp,0,6000);
	memset(bTemp,0,sizeof(bTemp));
	header_flag = 1;
	tcp_close(iclient_socket);
	if(sn!=NULL)
	{
		free(sn);
		sn =NULL;
	}
	return 0;

}


#endif

int get_http_header_len(char *temp_arr)
{
	int byte_cnt = 0;
	byte_cnt = 0;
	while((temp_arr[byte_cnt] != ';' || temp_arr[byte_cnt+1] == ' ')&&temp_arr[byte_cnt] != '\0')
	{
		byte_cnt++;
	}
	if(temp_arr[byte_cnt] == '\0')
		return 0;
	return byte_cnt;
}

void online_waybill_update(void)
{
	int iclient_socket = 1;//-1;
	char bTemp[1024];
	char SERVER_ADDR[50]="223.30.102.30";
	unsigned int  iTemp=0;
	int j = 0;
	char *sn = NULL;
	int nbytes = 10;
	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);

	CstcLog_printf("THREAD::\n\n***************** SOCKET CLIENT **************************\n\n");
	CstcLog_printf("THREAD::\nconnect to %s:%d...", SERVER_ADDR, SERVER_PORT);

	iclient_socket = tcp_connect(SERVER_ADDR, SERVER_PORT);
	if (iclient_socket < 0)
	{
		CstcLog_printf("THREAD::\nConnect failed.");
	}
	else
	{
		CstcLog_printf("inside if............");
		//		UART_get_machine_info(sn,nbytes);
		//		CstcLog_printf("sn = %s",sn);
		//		CstcLog_printf("nbytes = %d",nbytes);
		//		get_schedule_type_info();
		//
		//		sprintf(Data_Buff,"%s,%s",duty_master_struct_var.schedule_no,duty_master_struct_var.schedule_id);
		//		sprintf((char *)bTemp,"POST /log.php HTTP/1.1\r\nHost: 223.30.102.30\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&data=%s&mode=1\r\n",(strlen(Data_Buff)+nbytes+20),sn,Data_Buff);
		//
		//		iTemp = strlen((char*)bTemp);
		//
		//		CstcLog_printf("THREAD::iTemp = %d",iTemp);
		//		CstcLog_printf("THREAD::bTemp = %s",bTemp);
		//		if (tcp_write(iclient_socket , bTemp, iTemp) < 0)
		//		{
		//			CstcLog_printf("THREAD::write connection was broken");
		//
		//		}
		//		else
		{
			CstcLog_printf("THREAD::write successful....");

			if (tcp_read(iclient_socket, bTemp, &iTemp) < 0)
			{
				CstcLog_printf("THREAD::read connection was broken");

			}
			else if (iTemp > 0)
			{
				CstcLog_printf("THREAD::Readed bTemp = %s iTemp = %d",bTemp,iTemp);
				if(GPRS_compose_response_packet((char*)bTemp, iclient_socket,iTemp,&j,0))
				{
					CstcLog_printf("Response done iTemp : %d",iTemp);
				}
				CstcLog_printf("2. size of bTemp : %d",sizeof(bTemp));
				memset(bTemp,0,sizeof(bTemp));
				iTemp=0;

			}
		}
		//}

	}

}

int GPRS_compose_response_packet(char * rbTemp, int iclient_socket, int iTemp,int *j,int header_flag)
{
	char tempCh;// = rbTemp[158];
	char recvPktID[(PKT_ID_LEN+1)]={'\0'};
	char recvPktLen[(DATA_LENGTH_LEN+1)] ={'\0'};
	char recvSchedule[(SCHEDULE_NO_LEN+1)]={'\0'};
	char recvRateId[(SCHEDULE_NO_LEN+1)]={'\0'};
	char result[(SCHEDULE_NO_LEN+1)]={'\0'};
	char recvWaybillNo[(WAYBILL_NO_LEN+1)]={'\0'};
	char result_waybill[(WAYBILL_NO_LEN+1)]={'\0'};
	char recvRouteId[(ROUTE_ID_LEN + 1)]={'\0'};
	char recvCount[5]={'\0'};


	char *tempInitBuff=NULL;//[50]={'\0'};
	char *tempName=NULL;//[100]={'\0'};

	FILE* dbDataFile= NULL;
	int ptr_inc=0,g_ptr_inc=0;//i=0,

	char temp_cmd[100];
	char *sub_string=NULL;
	char *sub_string_1 = NULL;
	//char my_temp_buff[5000] = {'\0'};
	char *my_temp_buff = NULL;
	int hd_length;
	int checksumFlag=0, retval=0;

/*	if(!header_flag)
		hd_length= get_http_header_len(rbTemp);
	else*/
	hd_length = 0;
	my_temp_buff = &rbTemp[hd_length];

	//	if((sub_string_1 = strstr(rbTemp,"\r\n\r\n")) != NULL)
	{
		CstcLog_printf("OTA:: ********************** In GPRS_compose_response_packet ****************************");


		/************checking end heree***********/
		if((sub_string=strstr(my_temp_buff,"|")) != NULL) // data before | copied in my_temp_buff
		{
			checksumFlag=1;
			CstcLog_printf("OTA::checksumFlag = %d",checksumFlag);
			memset(GPRS_checksumbuffer,0,sizeof(GPRS_checksumbuffer));

			CstcLog_printf("OTA::(sub_string+1) = %d",strlen((sub_string+1)));
			memcpy(GPRS_checksumbuffer,(sub_string+1),strlen((sub_string+1)));  //checksum copied
			CstcLog_printf("OTA::(sub_string+1) = %s",(sub_string+1));
			*(sub_string)=';';
			*(sub_string+1)='\0';
			//	CstcLog_printf_2("iTemp = %d",iTemp);
			iTemp=strlen(my_temp_buff)+40; //swap avoided the trailing data if attached
			CstcLog_printf("modified iTemp = %d",iTemp);
		}
		else  // when data sent in parts
		{
			if(GPRS_checksumFlag)
			{
				CstcLog_printf("OTA::GPRS_checksumbuffer = %s",GPRS_checksumbuffer);
				//				strcat(GPRS_checksumbuffer,sub_string);//dbTemp
				if((GPRS_global_iTemp+iTemp) == GPRS_Total_packet_length)
					strncat(GPRS_checksumbuffer,rbTemp,iTemp);//dbTemp
				else
					strncat(GPRS_checksumbuffer,rbTemp,(GPRS_Total_packet_length - GPRS_global_iTemp));//dbTemp
			}
		}
		//		memset(my_temp_buff,0,sizeof(my_temp_buff));
		//		memcpy(my_temp_buff,(sub_string_1+strlen("\r\n\r\n")),strlen((sub_string_1)));
	}

	//	CstcLog_printf("OTA::GPRS_checksumFlag outside = %d",GPRS_checksumFlag);
//	CstcLog_printf("OTA::g_checksumbuffer = %s \n %d",GPRS_checksumbuffer,strlen(GPRS_checksumbuffer));


	tempCh = my_temp_buff[0];     //tempCh = rbTemp[hd_len];

	CstcLog_printf("OTA::Before checking packet:: tempCh =%c rbTemp[158]=%c ,my_temp_buff[0]=%c, my_temp_buff=%s",tempCh,rbTemp[hd_length],my_temp_buff[0],my_temp_buff);
	if(tempCh== ';')
	{
		CstcLog_printf("OTA::Yes, start indicator found....!");
		if(iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3))
		{
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			if(iTemp<2047)
			{
				memcpy(GPRS_global_buff,my_temp_buff,iTemp);//dbTemp,iTemp);
				//memcpy(GPRS_global_buff,(rbTemp+158),iTemp);//dbTemp,iTemp);
				GPRS_global_buff[(iTemp)]='\0';
			}
			else
			{
				memcpy(GPRS_global_buff,my_temp_buff,2047);//dbTemp,iTemp);
				GPRS_global_buff[2047]='\0';
			}
			GPRS_global_iTemp=(iTemp);//-159);
			CstcLog_printf("OTA::GPRS_global_iTemp(%d) in iTemp(%d) != GPRS_Total_packet_length condition  %d", GPRS_global_iTemp,iTemp,GPRS_Total_packet_length);
			GPRS_New_pkt_recv_flag=1;
			return -1; //-1 means still need to receive data
		}
		else if(iTemp > (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3))
		{
			memset(recvPktLen,0,DATA_LENGTH_LEN);
			memcpy(recvPktLen,(my_temp_buff+START_INDICATOR_LEN),DATA_LENGTH_LEN);

			CstcLog_printf("OTA::recvPktLen = %s bytes", recvPktLen);
			GPRS_Total_packet_length = 0;
			GPRS_Total_packet_length = atoi(recvPktLen);
			CstcLog_printf("OTA::GPRS_Total_packet_length is equal to %d ", GPRS_Total_packet_length);

			memcpy(recvPktID,my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),PKT_ID_LEN);//dbTemp
			CstcLog_printf("OTA::recvPktID =%s rbTemp[%d]=%s bytes", recvPktID,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN), my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN));

			memset(temp_cmd,0,100);
			memcpy(temp_cmd,my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN),1);//dbTemp

			gprs_UpdateDataFlag=0;
			gprs_UpdateDataFlag=atoi(temp_cmd);

			CstcLog_printf("OTA::gprs_UpdateDataFlag =%d rbTemp[%d]=%c bytes", gprs_UpdateDataFlag,(START_INDICATOR_LEN+DATA_LENGTH_LEN), my_temp_buff[(START_INDICATOR_LEN+DATA_LENGTH_LEN)]);
			GPRS_global_pkt_id=0;
			GPRS_global_pkt_id=atoi(recvPktID);//temp;
			CstcLog_printf("OTA::GPRS_global_pkt_id (which is packet ID) %d ", GPRS_global_pkt_id);

			if((GPRS_global_pkt_id == SCHEDULE_MASTER_PKT_ID) || (GPRS_global_pkt_id == ROUTE_MASTER_PKT_ID) || (GPRS_global_pkt_id == FARE_CHART_PKT_ID) || (GPRS_global_pkt_id == ROUTE_HEADER_PKT_ID) || (GPRS_global_pkt_id == GPRS_SCHEDULE_MASTER_PKT_ID))
			{
				CstcLog_printf("OTA::if GPRS_global_pkt_id == GPRS_SCHEDULE_MASTER_PKT_ID");
				memcpy(recvSchedule,(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),SCHEDULE_NO_LEN);//dbTemp
				CstcLog_printf("OTA::recvSchedule =%s rbTemp[%d]=%s bytes", recvSchedule,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
				remove_stars(recvSchedule,result);
				CstcLog_printf("OTA::after remove_stars result (Route/rate/fare ID) %s ", result);
				memset(GPRS_global_schedule,0,SCHEDULE_NO_LEN);
				strcpy(GPRS_global_schedule,result);
				GPRS_global_schedule[strlen(result)]='\0';
				CstcLog_printf("OTA::GPRS_global_schedule assigned (Route/rate/fare ID) = %s ", GPRS_global_schedule);
			}


			if(GPRS_global_pkt_id == WAYBILL_MASTER_PKT_ID)
			{
				CstcLog_printf("OTA::if global_temp == WAYBILL_MASTER_PKT_ID");

				memcpy(recvWaybillNo,(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+1)),WAYBILL_NO_LEN);//dbTemp
				recvWaybillNo[WAYBILL_NO_LEN]='\0';

				CstcLog_printf("OTA::recvWaybillNo =%s rbTemp[%d]=%s bytes", recvWaybillNo,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+1), (my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
				remove_space(recvWaybillNo,result_waybill);
				CstcLog_printf("OTA::after remove spaces result %s ", result_waybill);
				memset(GPRS_global_waybill,0,WAYBILL_NO_LEN);
				strcpy(GPRS_global_waybill,result_waybill);
				GPRS_global_waybill[strlen(result_waybill)]='\0';
				CstcLog_printf("OTA::global_waybill = %s ", GPRS_global_waybill);
			}
			if(GPRS_global_pkt_id == RATE_MASTER_PKT_ID)
			{
				CstcLog_printf("OTA::if global_temp == RATE_MASTER_PKT_ID");
				memcpy(recvRateId,(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),RATE_ID_LEN);//dbTemp
				recvRateId[RATE_ID_LEN]='\0';

				CstcLog_printf("OTA::recvRateId =%s rbTemp[%d]=%s bytes", recvRateId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
				remove_stars(recvRateId,result);
				CstcLog_printf("OTA::after remove_stars result %s ", result);
				memset(GPRS_global_Rate_Id,0,RATE_ID_LEN+1);
				strcpy(GPRS_global_Rate_Id,result);
				CstcLog_printf("OTA::global_Rate_Id = %s ", GPRS_global_Rate_Id);
			}

			if(GPRS_global_pkt_id == FARE_CHART_PKT_ID)
			{
				CstcLog_printf("OTA::if global_temp == 9");
				memcpy(recvCount,(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)),3);//dbTemp
				recvCount[3]='\0';
				GPRS_rt_count=0;
				GPRS_rt_count=atoi(recvCount);
				CstcLog_printf("OTA::recvCount =%s rbTemp[%d]=%s bytes rt_count=%d", recvCount,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN), my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),GPRS_rt_count);
				CstcLog_printf("OTA::recvCount =%s rt_count=%d", recvCount,GPRS_rt_count);
			}

			if(GPRS_global_pkt_id == CHECK_ROUTE_PKT_ID)
			{
				CstcLog_printf("OTA::if global_temp == CHECK_ROUTE_PKT_ID");
				memcpy(recvRouteId,(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),ROUTE_ID_LEN);//dbTemp
				CstcLog_printf("OTA::recvRouteId =%s rbTemp[%d]=%s bytes", recvRouteId,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
				remove_stars(recvRouteId,result);
				CstcLog_printf("OTA::after remove_stars result %s ", result);
				memset(GPRS_global_Route_Id,0,ROUTE_ID_LEN);
				strcpy(GPRS_global_Route_Id,result);
				GPRS_global_Route_Id[strlen(result)]='\0';
				CstcLog_printf("OTA::global_Rate_Id = %s ", GPRS_global_Route_Id);
			}

		}
		//		iTemp -= 9;
		if((iTemp) != GPRS_Total_packet_length)
		{
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			if(iTemp<2047)
			{
				memcpy(GPRS_global_buff,my_temp_buff,(iTemp));//dbTemp,iTemp);
				GPRS_global_buff[(iTemp)]='\0';
			}
			else
			{
				memcpy(GPRS_global_buff,my_temp_buff,2047);//dbTemp,iTemp);
				GPRS_global_buff[2047]='\0';
			}

			CstcLog_printf("OTA::GPRS_global_buff %s", GPRS_global_buff);
			dbDataFile = NULL;
			dbDataFile= fopen(DB_INPUT_FILE_ADDR,"a");
			if(dbDataFile == NULL)
			{
				CstcLog_printf("OTA::ERROR_OPENING_FILE_db_input");

				GPRS_checksumFlag=0;
			}
			else
			{
				CstcLog_printf("OTA::DB_INPUT_FILE_ADDR open success :%d and ID receieved = %d",dbDataFile, GPRS_global_pkt_id);

				if((GPRS_global_pkt_id != GET_DETAILS_PKT_ID) || (GPRS_global_pkt_id != REGISTRATION_REQUEST_PKT_ID) || (GPRS_global_pkt_id !=45) || (GPRS_global_pkt_id != 47) || (GPRS_global_pkt_id !=50)|| (GPRS_global_pkt_id !=02) ||(GPRS_global_pkt_id !=START_DUTY_PKT_ID) )
				{
					tempName=(char*)malloc(100*sizeof(char));
					memset(tempName,0,100);
					switch(GPRS_global_pkt_id)
					{
					CstcLog_printf("OTA::before insert table atoi(recvPktID) =%s ",GPRS_global_pkt_id);

					case WAYBILL_MASTER_PKT_ID://-------------------WAYBILL----------------------
						memcpy(tempName, "waybill_master",14);
						break;

					case GPRS_SCHEDULE_MASTER_PKT_ID: /*-----------------GPRS SCHEDULE-----------------------*/
						memcpy(tempName, "schedule_master", 15);
						break;

					case SCHEDULE_MASTER_PKT_ID: /*----------------- SCHEDULE-----------------------*/
						//						memcpy(tempName, "schedule_master", 15);
						memcpy(tempName, "schedule_master_restore", 23);
						break;

					case DUTY_MASTER_PKT_ID://--------------------DUTY------------------------
						memcpy(tempName, "duty_master",11);
						break;

					case ROUTE_MASTER_PKT_ID: //-------------------ROUTE------------------------
						//						memcpy(tempName, "route_master",12);
						memcpy(tempName, "route_master_restore",20);
						break;

					case FARE_CHART_RECORD_PKT_ID://--------------------FARE RECORD LENGTH-----------------------
						GPRS_RecordCntToRecv = atoi((my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
						CstcLog_printf("OTA::GPRS_RecordCntToRecv %d = int(tempStr) %d ",GPRS_RecordCntToRecv, atoi((my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN))));//dbTemp
						GPRS_TotalRecordCntRecvd=0;
						GPRS_rt_count=0;
						CstcLog_printf("OTA:: In case 8 GPRS_TotalRecordCntRecvd is %d ",GPRS_TotalRecordCntRecvd);
						break;

					case FARE_CHART_PKT_ID://--------------------FARE------------------------
						//						memcpy(tempName, "fare_chart",10);
						memcpy(tempName, "fare_chart_restore",18);
						//CstcLog_printf("OTA::GPRS_TotalRecordCntRecvd = %d ",GPRS_TotalRecordCntRecvd);
						//CstcLog_printf("OTA::Incr GPRS_TotalRecordCntRecvd = %d ",GPRS_TotalRecordCntRecvd);
						break;

					case TICKET_TYPE_PKT_ID://-----------------Ticket Type-------------------------
						memcpy(tempName, "ticket_type",11);
						break;

					case TICKET_SUB_TYPE_PKT_ID://---------------------Ticket sub Type-----------------------
						memcpy(tempName, "ticket_sub_type",15);
						break;

					case BUS_SERVICE_PKT_ID://--------------------Bus Services-----------------------------
						memcpy(tempName, "bus_service",11);
						break;

					case BUS_BRAND_PKT_ID://----------------------Bus Brand------------------------------
						memcpy(tempName, "bus_brand",9);
						break;

					case DEPOT_MASTER_PKT_ID://----------------------Depot------------------------------
						memcpy(tempName, "depot",5);
						break;

					case ROUTE_HEADER_PKT_ID://-----------------Route Header------------------------
						//						memcpy(tempName, "route_header",12);
						memcpy(tempName, "route_header_restore",20);
						break;

					case ETM_RW_KEY_PKT_ID://--------------------ETM RW key---------------------------
						memcpy(tempName, "etim_rw_keys",13);
						break;

					case TRUNK_FEEDER_PKT_ID://--------------------TRUNK AND FEEDER---------------------------
						memcpy(tempName, "trunk_feeder_master",19);
						break;

					case SERVICE_OP_PKT_ID://-------------------SERVICE OP-----------------------
						memcpy(tempName, "service_op",10);
						break;

					case RATE_MASTER_PKT_ID://-------------------RATE MASTER-----------------------
						//						memcpy(tempName, "rate_master",11);
						memcpy(tempName, "rate_master_restore",19);
						break;

					case LC_LOGIN_PKT_ID:/*-------------------LC Login-----------------------*/
						memcpy(tempName, "lc_login",8);
						break;

					default:
						break;
					}
					if((GPRS_global_pkt_id != FARE_CHART_RECORD_PKT_ID)&&(GPRS_global_pkt_id >0))
					{
						CstcLog_printf("OTA::Its not packet id 8.............");
						fprintf(dbDataFile,"%s","insert into ");
						CstcLog_printf("OTA::insert into");
						fprintf(dbDataFile,"%s",tempName);
						CstcLog_printf("OTA::%s",tempName);
						fprintf(dbDataFile,"%s"," select ");
						CstcLog_printf("OTA::select");
					}
					free(tempName);
					tempName=NULL;
				}


				if((GPRS_global_pkt_id == SCHEDULE_MASTER_PKT_ID) || (GPRS_global_pkt_id == 7) || (GPRS_global_pkt_id == 15) || (GPRS_global_pkt_id == 19) || (GPRS_global_pkt_id == GPRS_SCHEDULE_MASTER_PKT_ID))//|| (GPRS_global_pkt_id == 1))
				{
					CstcLog_printf("OTA::Found packet ID !!!!!!!! Now copying : %s", my_temp_buff);
					fprintf(dbDataFile,"%s",(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)));//dbTemp
				}

				else if(GPRS_global_pkt_id == FARE_CHART_PKT_ID )
				{
					CstcLog_printf("OTA::Inside if(GPRS_global_pkt_id == SCHEDULE_MASTER_PKT_ID)");
					fprintf(dbDataFile,"%s",(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)));//dbTemp
				}
				else if(GPRS_global_pkt_id > 0 ) //if((GPRS_global_pkt_id == 54) || (GPRS_global_pkt_id == 56) || (GPRS_global_pkt_id ==45) || (GPRS_global_pkt_id == 47) || (GPRS_global_pkt_id ==50)|| (GPRS_global_pkt_id ==02) ||(GPRS_global_pkt_id ==06) )//
				{
					fprintf(dbDataFile,"%s",(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
				}


				fflush(dbDataFile);
				fclose(dbDataFile);
			}
			dbDataFile = NULL;
			//			memset(dbTemp,0,sizeof(dbTemp));
			GPRS_global_iTemp=(iTemp);//-159);
			CstcLog_printf("OTA::GPRS_global_iTemp(%d) in iTemp(%d) != GPRS_Total_packet_length condition  %d", GPRS_global_iTemp,iTemp,GPRS_Total_packet_length);
			GPRS_New_pkt_recv_flag=1;
			//CstcLog_HexDump_2(rbTemp,iTemp);
			//			sleep(1);
			return  -1; //-1 means still need to receive data
		}
		else
		{
			dbDataFile = NULL;
			dbDataFile= fopen(DB_INPUT_FILE_ADDR,"w");
			if(dbDataFile == NULL)
			{
				CstcLog_printf("OTA::ERROR_OPENING_FILE_db_input");

			}
			else
			{
				CstcLog_printf("OTA::dbDataFile open success 22222 :%d",dbDataFile);
				if((GPRS_global_pkt_id != GET_DETAILS_PKT_ID) || (GPRS_global_pkt_id != REGISTRATION_REQUEST_PKT_ID) || (GPRS_global_pkt_id !=45) || (GPRS_global_pkt_id != 47) || (GPRS_global_pkt_id !=50) || (GPRS_global_pkt_id !=02) ||(GPRS_global_pkt_id !=START_DUTY_PKT_ID) )
				{
					tempName=(char*)malloc(100*sizeof(char));
					memset(tempName,0,100);
					switch(GPRS_global_pkt_id)
					{
					CstcLog_printf("OTA::before insert table atoi(recvPktID) =%s ",GPRS_global_pkt_id);

					case GPRS_SCHEDULE_MASTER_PKT_ID: /*-----------------GPRS SCHEDULE-----------------------*/
						memcpy(tempName, "schedule_master", 15);
						break;


					case WAYBILL_MASTER_PKT_ID ://-------------------WAYBILL----------------------
						memcpy(tempName, "waybill_master",14);
						break;

					case SCHEDULE_MASTER_PKT_ID: /*-----------------SCHEDULE-----------------------*/
						//						memcpy(tempName, "schedule_master", 15);
						memcpy(tempName, "schedule_master_restore", 23);
						break;

					case DUTY_MASTER_PKT_ID://--------------------DUTY------------------------
						memcpy(tempName, "duty_master",11);
						break;

					case ROUTE_MASTER_PKT_ID: //-------------------ROUTE------------------------
						//						memcpy(tempName, "route_master",12);
						memcpy(tempName, "route_master_restore",20);
						break;

					case FARE_CHART_RECORD_PKT_ID://--------------------FARE RECORD LENGTH-----------------------
						GPRS_RecordCntToRecv = atoi((rbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
						CstcLog_printf("OTA::GPRS_RecordCntToRecv %d = int(tempStr) %d ",GPRS_RecordCntToRecv, atoi((rbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN))));//dbTemp
						GPRS_TotalRecordCntRecvd=0;
						GPRS_rt_count=0;
						CstcLog_printf("OTA:: In case 8 GPRS_TotalRecordCntRecvd is %d ",GPRS_TotalRecordCntRecvd);
						break;

					case FARE_CHART_PKT_ID://--------------------FARE------------------------
						//						memcpy(tempName, "fare_chart",10);
						memcpy(tempName, "fare_chart_restore",18);
						CstcLog_printf("OTA::GPRS_TotalRecordCntRecvd = %d ",GPRS_TotalRecordCntRecvd);
						CstcLog_printf("OTA::Incr GPRS_TotalRecordCntRecvd = %d ",GPRS_TotalRecordCntRecvd);
						break;

					case TICKET_TYPE_PKT_ID://-----------------Ticket Type-------------------------
						memcpy(tempName, "ticket_type",11);
						break;

					case TICKET_SUB_TYPE_PKT_ID://---------------------Ticket sub Type-----------------------
						memcpy(tempName, "ticket_sub_type",15);
						break;

					case BUS_SERVICE_PKT_ID://--------------------Bus Services-----------------------------
						memcpy(tempName, "bus_service",11);
						break;

					case BUS_BRAND_PKT_ID://----------------------Bus Brand------------------------------
						memcpy(tempName, "bus_brand",9);
						break;

					case DEPOT_MASTER_PKT_ID://----------------------Depot------------------------------
						memcpy(tempName, "depot",5);
						break;

					case ROUTE_HEADER_PKT_ID://-----------------Route Header------------------------
						//						memcpy(tempName, "route_header",12);
						memcpy(tempName, "route_header_restore",20);
						break;

					case ETM_RW_KEY_PKT_ID://--------------------ETM RW key---------------------------
						memcpy(tempName, "etim_rw_keys",13);
						break;

					case TRUNK_FEEDER_PKT_ID://--------------------TRUNK AND FEEDER---------------------------
						memcpy(tempName, "trunk_feeder_master",19);
						break;

					case SERVICE_OP_PKT_ID://-------------------SERVICE OP-----------------------
						memcpy(tempName, "service_op",10);
						break;

					case RATE_MASTER_PKT_ID://-------------------SERVICE OP-----------------------
						//						memcpy(tempName, "rate_master",11);
						memcpy(tempName, "rate_master_restore",19);
						break;

					case LC_LOGIN_PKT_ID:/*-------------------LC Login-----------------------*/
						memcpy(tempName, "lc_login",8);
						break;


					default:
						break;
					}
					if(GPRS_global_pkt_id != FARE_CHART_RECORD_PKT_ID)
					{
						fprintf(dbDataFile,"%s","insert into ");
						CstcLog_printf("OTA::insert into");
						fprintf(dbDataFile," %s ",tempName);
						CstcLog_printf("OTA::%s",tempName);
						fprintf(dbDataFile,"%s"," select ");
						CstcLog_printf("OTA::select");
					}
					free(tempName);
					tempName=NULL;
				}
				if((GPRS_global_pkt_id == SCHEDULE_MASTER_PKT_ID) || (GPRS_global_pkt_id == 7) || (GPRS_global_pkt_id == 15) || (GPRS_global_pkt_id == 19) || (GPRS_global_pkt_id == GPRS_SCHEDULE_MASTER_PKT_ID))// || (GPRS_global_pkt_id == 1))
				{
					CstcLog_printf("OTA::Some packet ID found now copying my_temp_buff data");
					fprintf(dbDataFile,"%s",(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)));//dbTemp
				}

				else if(GPRS_global_pkt_id == FARE_CHART_PKT_ID )
				{
					CstcLog_printf("OTA::Found fare chart packet ID....");
					fprintf(dbDataFile,"%s",(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)));//dbTemp
				}
				else if(GPRS_global_pkt_id > 0 ) //if((GPRS_global_pkt_id == 54) || (GPRS_global_pkt_id == 56) || (GPRS_global_pkt_id ==45) || (GPRS_global_pkt_id == 47) || (GPRS_global_pkt_id ==50)|| (GPRS_global_pkt_id ==02) ||(GPRS_global_pkt_id ==06) )//
				{
					CstcLog_printf("OTA:: else if(GPRS_global_pkt_id > 0 )");
					fprintf(dbDataFile,"%s",(my_temp_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
				}

				fflush(dbDataFile);
				fclose(dbDataFile);
			}
			dbDataFile = NULL;
			GPRS_global_iTemp=(iTemp);//-158);
			CstcLog_printf("OTA::Before performing  GPRS_received_packet [ GPRS_global_iTemp = %d", GPRS_global_iTemp);
			retval = GPRS_received_packet(GPRS_global_pkt_id, tempCh, iclient_socket,my_temp_buff, gprs_UpdateDataFlag,j);//dbTemp
			CstcLog_printf("OTA::GPRS_received_packet returned -> %d", retval);

			memset(temp_cmd,0,100);
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(rbTemp,0,sizeof(rbTemp));
			sub_string=NULL;//memset(dbTemp,0,sizeof(dbTemp));
			sub_string_1=NULL;
			memset(my_temp_buff,0,sizeof(my_temp_buff));
			memset(GPRS_global_schedule,0,sizeof(GPRS_global_schedule));
			GPRS_Total_packet_length=0;
			gprs_UpdateDataFlag=0;
			GPRS_checksumFlag=0;
			if(GPRS_global_pkt_id == FARE_CHART_PKT_ID)
				sleep(2);
			else
				usleep(500000);
			GPRS_global_pkt_id=0;
			//			sleep(2);	// 	sleep(1);  modified on 14_oct
			CstcLog_printf("OTA::Resetting the variables and returning....");

			return retval;//1;
			//}
		}
	}
	else
	{
		if(GPRS_New_pkt_recv_flag)
		{
			CstcLog_printf("OTA::Inside (iTemp(%d)+GPRS_global_iTemp(%d)) == GPRS_Total_packet_length(%d)",iTemp,GPRS_global_iTemp,GPRS_Total_packet_length);
			if((iTemp+GPRS_global_iTemp)<2047)
			{
				CstcLog_printf("OTA::5 iTemp = %d",iTemp);
				//				strncat(GPRS_global_buff,rbTemp,iTemp);//dbTemp,iTemp);
				memcpy(GPRS_global_buff+GPRS_global_iTemp,rbTemp,iTemp);//dbTemp,iTemp);
				GPRS_global_buff[(iTemp+GPRS_global_iTemp)]='\0';
			}
			else if((GPRS_global_iTemp)<2047)
			{
				CstcLog_printf("OTA::5 GPRS_checksumFlag = %d",GPRS_checksumFlag);
				//				strncat(GPRS_global_buff,rbTemp,(2047-GPRS_global_iTemp-1));//dbTemp,iTemp);
				memcpy(GPRS_global_buff+GPRS_global_iTemp,rbTemp,(2047-GPRS_global_iTemp-1));//dbTemp,iTemp);
				GPRS_global_buff[2047]='\0';
			}

			CstcLog_printf("OTA::6 Inside GPRS_global_iTemp(%d) == GPRS_Total_packet_length(%d)",GPRS_global_iTemp,GPRS_Total_packet_length);
			//			else
			//			{
			////				memcpy(GPRS_global_buff,rbTemp,2048);//dbTemp,iTemp);
			//				GPRS_global_buff[2048]='\0';
			//			}

			CstcLog_printf("OTA::6 GPRS_global_buff %s", GPRS_global_buff);


			if(((iTemp+GPRS_global_iTemp) >= (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3))
					&&(GPRS_global_iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)))
			{
				CstcLog_printf("OTA::GPRS_global_iTemp < 30 Refill all data from GPRS_global_buff");
				memset(recvPktLen,0,DATA_LENGTH_LEN);
				memcpy(recvPktLen,GPRS_global_buff+START_INDICATOR_LEN,DATA_LENGTH_LEN);

				CstcLog_printf("OTA::recvPktLen =%s bytes", recvPktLen);
				GPRS_Total_packet_length = 0;
				GPRS_Total_packet_length = atoi(recvPktLen);
				CstcLog_printf("OTA::GPRS_Total_packet_length %d ", GPRS_Total_packet_length);

				memcpy(recvPktID,GPRS_global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),PKT_ID_LEN);//dGPRS_global_buff
				CstcLog_printf("OTA::recvPktID =%s GPRS_global_buff[%d]=%s bytes", recvPktID,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN), GPRS_global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN));

				memset(temp_cmd,0,100);
				memcpy(temp_cmd,GPRS_global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN),1);//dGPRS_global_buff
				gprs_UpdateDataFlag=0;
				gprs_UpdateDataFlag=atoi(temp_cmd);
				CstcLog_printf("OTA::updateDataFlag =%d GPRS_global_buff[%d]=%c bytes", gprs_UpdateDataFlag,(START_INDICATOR_LEN+DATA_LENGTH_LEN), GPRS_global_buff[(START_INDICATOR_LEN+DATA_LENGTH_LEN)]);
				GPRS_global_pkt_id=0;
				GPRS_global_pkt_id=atoi(recvPktID);//temp;
				CstcLog_printf("OTA::GPRS_global_pkt_id %d ", GPRS_global_pkt_id);

				if((GPRS_global_pkt_id == SCHEDULE_MASTER_PKT_ID) || (GPRS_global_pkt_id == ROUTE_MASTER_PKT_ID) || (GPRS_global_pkt_id == FARE_CHART_PKT_ID) || (GPRS_global_pkt_id == ROUTE_HEADER_PKT_ID) || (GPRS_global_pkt_id == GPRS_SCHEDULE_MASTER_PKT_ID))
				{
					CstcLog_printf("OTA::if GPRS_global_pkt_id == SCHEDULE_MASTER_PKT_ID");
					memcpy(recvSchedule,(GPRS_global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),SCHEDULE_NO_LEN);//dGPRS_global_buff
					CstcLog_printf("OTA::recvSchedule =%s GPRS_global_buff[%d]=%s bytes", recvSchedule,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), (GPRS_global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN)));
					remove_stars(recvSchedule,result);
					CstcLog_printf("OTA::after remove_stars result %s ", result);
					memset(GPRS_global_schedule,0,SCHEDULE_NO_LEN);
					strcpy(GPRS_global_schedule,result);
					GPRS_global_schedule[strlen(result)]='\0';
					CstcLog_printf("OTA::GPRS_schedule = %s ", GPRS_global_schedule);

				}

			}

			CstcLog_printf("OTA::GPRS_global_iTemp + iTemp  %d", iTemp+GPRS_global_iTemp);
			//CstcLog_printf("OTA::In New_flag GPRS_TotalRecordCntRecvd = %d ",GPRS_TotalRecordCntRecvd);
			if(GPRS_global_pkt_id == FARE_CHART_PKT_ID)
			{
				//				CstcLog_printf("OTA::In New_flag sub_string_len = %d ",sub_string_len);
				//
				if (GPRS_global_iTemp<=30)
				{
					CstcLog_printf("OTA::2 GPRS_checksumFlag = %d",GPRS_checksumFlag);
					ptr_inc=0;
					ptr_inc=(30-GPRS_global_iTemp);
				}
				else
					ptr_inc=0;
				//CstcLog_printf("OTA::Incr in New_flag GPRS_TotalRecordCntRecvd = %d ",GPRS_TotalRecordCntRecvd);
			}
			CstcLog_printf("OTA::1 GPRS_checksumFlag = %d",GPRS_checksumFlag);
			if ( (GPRS_global_iTemp<=27) && //Swap:This needs to be look into for GPRS_rt_count
					((GPRS_global_pkt_id == SCHEDULE_MASTER_PKT_ID) || (GPRS_global_pkt_id == 7) || (GPRS_global_pkt_id == 15)|| (GPRS_global_pkt_id == 19) || (GPRS_global_pkt_id == GPRS_SCHEDULE_MASTER_PKT_ID))
			)
			{
				CstcLog_printf("OTA::2 GPRS_checksumFlag = %d",GPRS_checksumFlag);
				ptr_inc=0;
				ptr_inc=(27-GPRS_global_iTemp);
			}
			else
				ptr_inc=0;

			CstcLog_printf("OTA::ptr_inc %d ,GPRS_schedule  %s",ptr_inc,GPRS_schedule);
			CstcLog_printf("OTA::In New_flag dbTemp = %s ",rbTemp);//dbTemp
			if(!GPRS_checksumFlag)
			{
				CstcLog_printf("OTA::3 GPRS_checksumFlag = %d",GPRS_checksumFlag);
				dbDataFile = NULL;
				dbDataFile= fopen(DB_INPUT_FILE_ADDR,"a+");
				if(dbDataFile == NULL)
				{
					CstcLog_printf("OTA::ERROR_OPENING_FILE_db_input");

					memset(temp_cmd,0,100);
					strcpy(temp_cmd,"rm -f ");
					strcat(temp_cmd,DB_INPUT_FILE_ADDR);
					CstcLog_printf("OTA::temp_cmd :%s",temp_cmd);
					system(temp_cmd);
					memset(temp_cmd,0,100);
					memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
					memset(rbTemp,0,sizeof(rbTemp));
					sub_string=NULL;//memset(dbTemp,0,sizeof(dbTemp));
					sub_string_1=NULL;
					memset(my_temp_buff,0,sizeof(my_temp_buff));
					memset(GPRS_schedule,0,sizeof(GPRS_schedule));
					gprs_UpdateDataFlag=0;
				}
				else
				{
					CstcLog_printf("OTA::1.Tempfile open success :%d",dbDataFile);
					g_ptr_inc=0;
					if(((iTemp+GPRS_global_iTemp) >= (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3))
							&&(GPRS_global_iTemp < (START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3)))
					{
						tempName=(char*)malloc(100*sizeof(char));
						memset(tempName,0,100);
						CstcLog_printf("OTA::GPRS_global_iTemp < 30 Refill all data in db_input.txt file");
						if((GPRS_global_pkt_id != GET_DETAILS_PKT_ID) || (GPRS_global_pkt_id != REGISTRATION_REQUEST_PKT_ID) || (GPRS_global_pkt_id !=45) || (GPRS_global_pkt_id != 47) || (GPRS_global_pkt_id !=50) || (GPRS_global_pkt_id !=02) ||(GPRS_global_pkt_id !=START_DUTY_PKT_ID) )
						{
							switch(GPRS_global_pkt_id)
							{
							CstcLog_printf("OTA::before insert table atoi(recvPktID)=%s ",GPRS_global_pkt_id);

							case WAYBILL_MASTER_PKT_ID :/*-------------------WAYBILL----------------------*/
								memcpy(tempName, "waybill_master",14);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case GPRS_SCHEDULE_MASTER_PKT_ID: /*----------------- SCHEDULE-----------------------*/
								memcpy(tempName, "schedule_master", 15);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
								break;

							case SCHEDULE_MASTER_PKT_ID: /*-----------------SCHEDULE-----------------------*/
								//								memcpy(tempName, "schedule_master", 15);
								memcpy(tempName, "schedule_master_restore", 23);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
								break;

							case DUTY_MASTER_PKT_ID:/*--------------------DUTY------------------------*/
								memcpy(tempName, "duty_master",11);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case ROUTE_MASTER_PKT_ID: /*-------------------ROUTE------------------------*/
								//								memcpy(tempName, "route_master",12);
								memcpy(tempName, "route_master_restore",20);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
								break;

							case FARE_CHART_RECORD_PKT_ID:/*--------------------FARE RECORD LENGTH------------------------*/
								GPRS_RecordCntToRecv = atoi((GPRS_global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));//dbTemp
								CstcLog_printf("OTA::GPRS_RecordCntToRecv %d = int(tempStr) %d ",GPRS_RecordCntToRecv, atoi((GPRS_global_buff+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN))));//dbTemp
								GPRS_TotalRecordCntRecvd=0;
								GPRS_rt_count=0;
								CstcLog_printf("OTA:: In case 8 GPRS_TotalRecordCntRecvd is %d ",GPRS_TotalRecordCntRecvd);
								break;

							case FARE_CHART_PKT_ID:/*--------------------FARE------------------------*/
								//								memcpy(tempName, "fare_chart",10); /*--search for union all select in dbTemp add that count in GPRS_TotalRecordCntRecvd--*/
								memcpy(tempName, "fare_chart_restore",18);
								//								CstcLog_printf("OTA::GPRS_TotalRecordCntRecvd = %d ",GPRS_TotalRecordCntRecvd);
								//								CstcLog_printf("OTA::Incr GPRS_TotalRecordCntRecvd = %d ",GPRS_TotalRecordCntRecvd);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN+3;
								break;

							case TICKET_TYPE_PKT_ID:/*-----------------Ticket Type-------------------------*/
								memcpy(tempName, "ticket_type",11);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case TICKET_SUB_TYPE_PKT_ID:/*---------------------Ticket sub Type------------------------*/
								memcpy(tempName, "ticket_sub_type",15);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case BUS_SERVICE_PKT_ID:/*--------------------Bus Services------------------------------*/
								memcpy(tempName, "bus_service",11);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case BUS_BRAND_PKT_ID:/*----------------------Bus Brand-------------------------------*/
								memcpy(tempName, "bus_brand",9);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case DEPOT_MASTER_PKT_ID:/*----------------------Depot-------------------------------*/
								memcpy(tempName, "depot",5);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case ROUTE_HEADER_PKT_ID:/*-----------------Route Header------------------------*/
								//								memcpy(tempName, "route_header",12);
								memcpy(tempName, "route_header_restore",20);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
								break;

							case ETM_RW_KEY_PKT_ID:/*--------------------ETM RW key---------------------------*/
								memcpy(tempName, "etim_rw_keys",13);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case TRUNK_FEEDER_PKT_ID:/*--------------------TRUNK AND FEEDER---------------------------*/
								memcpy(tempName, "trunk_feeder_master",19);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case SERVICE_OP_PKT_ID:/*-------------------SERVICE OP-----------------------*/
								memcpy(tempName, "service_op",10);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;

							case RATE_MASTER_PKT_ID:/*------------------Rate Master-----------------------*/
								//								memcpy(tempName, "rate_master",11);
								memcpy(tempName, "rate_master_restore",19);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN;
								break;

							case LC_LOGIN_PKT_ID:/*-------------------LC Login-----------------------*/
								memcpy(tempName, "lc_login",8);
								g_ptr_inc=START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN;
								break;


							default:
								break;
							}
							if(GPRS_global_pkt_id != FARE_CHART_RECORD_PKT_ID)
							{
								fprintf(dbDataFile,"%s","insert into ");
								CstcLog_printf("OTA::insert into");
								fprintf(dbDataFile," %s ",tempName);
								CstcLog_printf("OTA::%s",tempName);
								fprintf(dbDataFile,"%s"," select ");
								CstcLog_printf("OTA::select");
							}
							free(tempName);
							tempName=NULL;
						}
						if(GPRS_global_iTemp>g_ptr_inc)
						{
							tempInitBuff=(char*)malloc(50*sizeof(char));
							memset(tempInitBuff,0,50);
							memcpy(tempInitBuff,(GPRS_global_buff+g_ptr_inc),(GPRS_global_iTemp-g_ptr_inc));
							tempInitBuff[(GPRS_global_iTemp-g_ptr_inc)]='\0';
							CstcLog_printf("OTA::tempInitBuff :%s",tempInitBuff);
							fprintf(dbDataFile,"%s",tempInitBuff);//dbTemp
							free(tempInitBuff);
							tempInitBuff=NULL;
						}
					}
					CstcLog_printf("OTA::Tempfile open success :%d",dbDataFile);
					//fprintf(dbDataFile,"%s",dbTemp);
					fprintf(dbDataFile,"%s",(rbTemp+ptr_inc));//dbTemp
				}
			}
			if(checksumFlag)
			{
				CstcLog_printf("OTA::4 GPRS_checksumFlag = %d",GPRS_checksumFlag);
				GPRS_checksumFlag =1;
				CstcLog_printf("OTA::5 GPRS_checksumFlag = %d",GPRS_checksumFlag);
			}

			GPRS_global_iTemp=iTemp+GPRS_global_iTemp;
			CstcLog_printf("OTA::Inside GPRS_global_iTemp(%d) == GPRS_Total_packet_length(%d)",GPRS_global_iTemp,GPRS_Total_packet_length);
			//			else
			//			{
			////				memcpy(GPRS_global_buff,rbTemp,2048);//dbTemp,iTemp);
			//				GPRS_global_buff[2048]='\0';
			//			}

			CstcLog_printf("OTA::GPRS_global_buff %s", GPRS_global_buff);

			if(GPRS_global_iTemp == GPRS_Total_packet_length)
			{
				CstcLog_printf("OTA::Done Inside iTemp(%d),GPRS_global_iTemp(%d) == GPRS_Total_packet_length(%d)",iTemp,GPRS_global_iTemp,GPRS_Total_packet_length);
				//				fprintf(dbDataFile,"%s",(char*)";");
				CstcLog_printf("OTA::Before checking dbDataFile ");
				if(dbDataFile != NULL)
				{
					CstcLog_printf("OTA::After checking dbDataFile ");
					fflush(dbDataFile);
					fclose(dbDataFile);
					dbDataFile = NULL;
					CstcLog_printf("OTA::After closing dbDataFile ");
				}
				CstcLog_printf("OTA::For closed dbDataFile  ");

				retval = GPRS_received_packet(GPRS_global_pkt_id, GPRS_global_buff[0],iclient_socket,GPRS_global_buff, gprs_UpdateDataFlag,j);

				memset(temp_cmd,0,100);
				CstcLog_printf("OTA::8. If fail ack then crashes");
				strcpy(temp_cmd,"rm -f ");
				strcat(temp_cmd,DB_INPUT_FILE_ADDR);
				CstcLog_printf("OTA::9. If fail ack then crashes");
				CstcLog_printf("OTA::temp_cmd :%s",temp_cmd);
				system(temp_cmd);
				memset(temp_cmd,0,100);
				CstcLog_printf("OTA::10. If fail ack then crashes");
				CstcLog_HexDump_2(rbTemp,iTemp);

				GPRS_New_pkt_recv_flag=0;
				memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
				memset(rbTemp,0,sizeof(rbTemp));
				CstcLog_printf("OTA::11. If fail ack then crashes");
				memset(GPRS_checksumbuffer,0,sizeof(GPRS_checksumbuffer));
				sub_string_1=NULL;
				memset(my_temp_buff,0,sizeof(my_temp_buff));
				sub_string=NULL;

				gprs_UpdateDataFlag=0;
				GPRS_Total_packet_length=0;
				GPRS_checksumFlag=0;
				CstcLog_printf("OTA::12. If fail ack then crashes");
				if(GPRS_global_pkt_id == FARE_CHART_PKT_ID)		//Fare Chart
					sleep(2);
				else
					usleep(500000);
				GPRS_global_pkt_id=0;
				CstcLog_printf("OTA::13. If fail ack then crashes");
				//				sleep(2); //sleep(1); modified on 14_oct
				return retval;//1;
			}
			else if(GPRS_global_iTemp > GPRS_Total_packet_length)
			{
				CstcLog_HexDump_2(rbTemp,iTemp);
				Show_Error_Msg_UART("Received Packet size is larger than \ntotal packet length in the packet");
				CstcLog_printf("OTA::Packet size is larger:Before checking dbDataFile ");
				if(dbDataFile != NULL)
				{
					CstcLog_printf("OTA::Packet size is larger:After checking dbDataFile ");
					fflush(dbDataFile);
					fclose(dbDataFile);
					dbDataFile = NULL;
					CstcLog_printf("OTA::Packet size is larger:After closing dbDataFile ");
				}
				CstcLog_printf("OTA::Packet size is larger:For closed dbDataFile  ");

				//				memset(temp_cmd,0,100);
				//				sprintf(temp_cmd,"cp %s /home/user0/BMTC/err_larger.txt", DB_INPUT_FILE_ADDR);
				//				system(temp_cmd);
				//				while(!system(NULL)){
				//					CstcLog_printf("Shell is unavailable");
				//					sleep(2);
				//				}
				//				sleep(2);
				//				memset(temp_cmd,0,100);
				//				sprintf(temp_cmd,"cp %s /home/user0/BMTC/err_largerbuff.txt", GPRS_global_buff);
				//				system(temp_cmd);

				while(!system(NULL)){
					CstcLog_printf("Shell is unavailable");
					sleep(2);
				}
				sleep(2);
				memset(temp_cmd,0,100);
				strcpy(temp_cmd,"rm -f ");
				strcat(temp_cmd,DB_INPUT_FILE_ADDR);
				CstcLog_printf("OTA::temp_cmd :%s",temp_cmd);
				system(temp_cmd);
				memset(temp_cmd,0,100);
				memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
				memset(rbTemp,0,sizeof(rbTemp));
				memset(GPRS_checksumbuffer,0,sizeof(GPRS_checksumbuffer));

				sub_string=NULL;
				sub_string_1=NULL;
				memset(my_temp_buff,0,sizeof(my_temp_buff));

				gprs_UpdateDataFlag=0;
				checksumFlag=0;
				GPRS_global_pkt_id=0;
				GPRS_Total_packet_length=0;
				GPRS_New_pkt_recv_flag=0;

			}
			else
			{
				CstcLog_printf("OTA::default Before checking dbDataFile ");
				if(dbDataFile != NULL)
				{
					CstcLog_printf("OTA::default After checking dbDataFile ");
					fflush(dbDataFile);
					fclose(dbDataFile);
					dbDataFile = NULL;
					CstcLog_printf("OTA::default After closing dbDataFile ");
				}
				CstcLog_printf("OTA::default For closed dbDataFile  ");
				CstcLog_HexDump_2(rbTemp,iTemp);
				//				sleep(1);
				return  -1; //-1 means still need to receive data
			}
			memset(rbTemp,0,sizeof(rbTemp));
			sub_string=NULL;
		}
		else
		{
			CstcLog_printf("OTA::Checking for NO UPDATE FOUND");
			if(strstr(rbTemp,"NO UPDATE FOUND"))
			{
				CstcLog_printf("OTA::String Found is NO UPDATE FOUND");
				memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
				memset(rbTemp,0,sizeof(rbTemp));
				sub_string=NULL;
				checksumFlag=0;
				//				sleep(4);
				return 2;
			}
			if(strstr(rbTemp,"ALL DATA UPTO DATE"))
			{
				CstcLog_printf("OTA::String Found is ALL DATA UPTO DATE");
				memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
				memset(rbTemp,0,sizeof(rbTemp));
				sub_string=NULL;
				checksumFlag=0;
				//				sleep(4);
				return 3;
			}
		}
	}
	//memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
	memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
	checksumFlag=0;
	return 0;

}


int GPRS_received_packet(int indx, char tempCh, int iclient_socket,char * bTemp,int updateDataFlag,int *j)
{
	int length = 0;
	int error=0,rowCount=0 , count_header =0 ,count_master = 0;
	int ack_flag=0;

	int retval = 0;
	char * pkt_ptr = NULL;
	char * pkt_format_ptr  = NULL;
	char temp_Ary[20] = {'\0'};
	char GPRS_Err_MsgAry[3]={'\0'};

	char *ack_post = NULL;
	char *sn = NULL;
	//int nbytes = 10,data_len=0;
	//time_t t = time(NULL);
	//struct tm ptm = *localtime(&t);

	CstcLog_printf("OTA::********Inside GPRS_received_packet********** ");

	if(tempCh == ';')
	{
		CstcLog_printf("OTA::Found a start indicator :) ");
		switch(GPRS_global_pkt_id)
		{
		case WAYBILL_MASTER_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			if(strlen(GPRS_global_waybill) < WAYBILL_NO_LEN)
			{
				ack_flag=1;
				CstcLog_printf("OTA::INVALID_WAYBILL_NO ");
				sprintf(GPRS_Err_Msg,"%02d",INVALID_WAYBILL_NO);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				break;
			}
			if(updateDataFlag==0)//For Normal waybill entry
			{
				if(DbInterface_Get_Row_Count("waybill_master",strlen("waybill_master"),0)>0)
				{
					ack_flag=1;
					CstcLog_printf("OTA::WAYBILL MASTER parsed ");
					sprintf(GPRS_Err_Msg,"%02d",WAYBILL_PRESENT);
					GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
					CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
					GPRS_Err_Msg[GPRS_Err_Len]='\0';

					break;
				}
			}

			//			if(master_status_struct_var.insert_waybill==0)
			//			{
			//				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
			//				if(DbInterface_Update_Master_Status_Data("update master_status set insert_waybill ='1'"))
			//				{
			//					master_status_struct_var.insert_waybill=1;
			//					GPRS_check_master_status();
			//					//	DbInterface_Delete_Table("COMMIT;");
			//				}
			//				//				else
			//				//					DbInterface_Delete_Table("ROLLBACK;");
			//			}
			CstcLog_printf("OTA::WAYBILL MASTER RECEIVED ");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			CstcLog_printf("OTA::WAYBILL MASTER parsed1 ");
			if(retval)
			{
				CstcLog_printf("OTA::WAYBILL MASTER parsed2 ");
				ack_flag=1;
				CstcLog_printf("OTA::WAYBILL MASTER parsed ");

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
				GPRS_Err_Len=0;
				get_waybill_details();
				//				waybill_details_in_file(); //divya comment
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::WAYBILL MASTER parsing failed");

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",WAYBILL_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg=%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}
			break;

		case SCHEDULE_DETAILS_PKT_ID:
			CstcLog_printf("OTA::GET SCHEDULE DETAILS RECEIVED ");
			//	retval =UART_packet_parser_nresponder(bTemp);
			if(retval)
			{
				rowCount=DbInterface_Get_Row_Count("duty_master",strlen("duty_master"),0);
				CstcLog_printf("OTA::rowCount = %d ",rowCount);
				if(rowCount == 0)
				{
					pkt_ptr= (char *)malloc((sizeof(duty_master_struct))+1);
					CstcLog_printf("OTA::(sizeof(duty_master_struct))+1= %d ",(sizeof(duty_master_struct))+1);
					memset(pkt_ptr,0,(sizeof(duty_master_struct))+1);
				}
				else
				{
					pkt_ptr= (char *)malloc((rowCount*(sizeof(duty_master_struct))+1+rowCount));
					CstcLog_printf("OTA::(rowCount*(sizeof(duty_master_struct))+1+rowCount) = %d ",(rowCount*(sizeof(duty_master_struct))+1+rowCount));
					memset(pkt_ptr,0,(rowCount*(sizeof(duty_master_struct))+1+rowCount));
				}

				UART_compose_schedule_response_packet(pkt_ptr,&length);

				pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
				memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
				UART_final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

				CstcLog_printf("OTA::Sent bytes are : ");
				CstcLog_HexDump_1(pkt_format_ptr,length);

				CstcLog_printf("OTA::Sent %d bytes", length);


				error = write(iclient_socket,pkt_format_ptr, length);



				CstcLog_printf("OTA::error %d bytes ", error);
				if (error <= 0)
				{
					CstcLog_printf("OTA::SCHEDULE DETAILS Connection was broken !!");

					Show_Error_Msg_UART("   SCHEDULE DETAILS FAILED  ");
					beep(2800,1000);
					if(pkt_ptr!=NULL)
					{
						free(pkt_ptr);
						pkt_ptr=NULL;
					}
					if(pkt_format_ptr!=NULL)
					{
						free(pkt_format_ptr);
						pkt_format_ptr=NULL;
					}
					return 0;
				}
				else
				{
					CstcLog_printf("OTA::SCHEDULE DETAILS Connection was Not broken !!");
					if(pkt_ptr!=NULL)
					{
						CstcLog_printf("OTA::pkt_ptr_free");
						free(pkt_ptr);
						pkt_ptr=NULL;
					}
					if(pkt_format_ptr!=NULL)
					{
						CstcLog_printf("OTA::pkt_format_ptr_free");
						free(pkt_format_ptr);
						pkt_format_ptr=NULL;
					}
					return 1;
				}
				CstcLog_printf("OTA::GET SCHEDULE DETAILS parsed ");
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::GET SCHEDULE DETAILS parsing failed");

				Show_Error_Msg_UART("FETCHING \nSCHEDULE DETAILS FAILED");
				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",SCHEDULE_DETAILS_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg=%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}
			break;

		case GPRS_SCHEDULE_MASTER_PKT_ID :
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::GPRS_SCHEDULE_MASTER_PKT_ID RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER");

			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			CstcLog_printf("OTA::case 3 retval = %d ",retval);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::GPRS_SCHEDULE_MASTER_PKT_ID parsed ");
				Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER SUCCESS");
				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::GPRS_SCHEDULE_MASTER_PKT_ID parsing failed");
				Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER FAILED");
				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",SCHEDULE_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg=%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}
			break;

		case SCHEDULE_MASTER_PKT_ID :
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::SCHEDULE MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER");

			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			CstcLog_printf("OTA::case 3 retval = %d ",retval);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::SCHEDULE MASTER parsed ");
				Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER SUCCESS");
				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::SCHEDULE MASTER parsing failed");
				Show_Error_Msg_UART("UPLOADING \nSCHEDULE MASTER FAILED");
				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",SCHEDULE_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg=%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}
			break;
		case DUTY_MASTER_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::DUTY MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nDUTY MASTER");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::DUTY MASTER parsed ");
				Show_Error_Msg_UART("UPLOADING \nDUTY MASTER SUCCESS");

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
				//				if(master_status_struct_var.insert_waybill==1)
				//				{
				//					//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				//					if(DbInterface_Update_Master_Status_Data("update master_status set insert_waybill ='2'"))
				//					{
				//						master_status_struct_var.insert_waybill=2;
				//						*j=0;
				//						//	DbInterface_Delete_Table("COMMIT;");
				//					}
				//					//					else
				//					//						DbInterface_Delete_Table("ROLLBACK;");
				//				}
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::DUTY MASTER parsing failed");

				Show_Error_Msg_UART("UPLOADING \nDUTY MASTER FAILED");

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",DUTY_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg=%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}
			break;

		case START_DUTY_PKT_ID:
			//			if(master_status_struct_var.start_duty==0)
			//			{
			//				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
			//				if(DbInterface_Update_Master_Status_Data("update master_status set start_duty ='1'"))
			//				{
			//					master_status_struct_var.start_duty=1;
			//					//	DbInterface_Delete_Table("COMMIT;");
			//				}
			//				//				else
			//				//					DbInterface_Delete_Table("ROLLBACK;");
			//			}
			//			GPRS_check_master_status();
			CstcLog_printf("OTA::START DUTY RECEIVED ");
			retval =UART_packet_parser_nresponder(bTemp);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::START DUTY parsed ");
				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				//				if(DbInterface_Update_Master_Status_Data("update master_status set start_duty ='2'"))
				//				{
				//					master_status_struct_var.start_duty=2;
				//					*j=0;
				//					//DbInterface_Delete_Table("COMMIT;");
				//					//					master_status_struct_var.startDutyFlag=1;
				//				}
				//				else
				//					DbInterface_Delete_Table("ROLLBACK;");
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::START DUTY parsing failed");
				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",START_DUTY_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg=%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}

			break;
		case ROUTE_MASTER_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			//			if(master_status_struct_var.insert_route==0)
			//			{
			//				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
			//				if(DbInterface_Update_Master_Status_Data("update master_status set insert_route ='1'"))
			//				{
			//					master_status_struct_var.insert_route=1;
			//					//DbInterface_Delete_Table("COMMIT;");
			//				}
			//				//				else
			//				//					DbInterface_Delete_Table("ROLLBACK;");
			//			}
			//			GPRS_check_master_status();
			CstcLog_printf("OTA::ROUTE MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nROUTE MASTER");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id,updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::ROUTE MASTER parsed ");
				Show_Error_Msg_UART("UPLOADING \nROUTE MASTER SUCCESS");

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;

			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::ROUTE MASTER parsing failed");
				Show_Error_Msg_UART("UPLOADING \nROUTE MASTER FAILED");

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",ROUTE_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg=%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}

			break;

		case FARE_CHART_RECORD_PKT_ID :
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::FARE CHART RECORD COUNT Recvd updateDataFlag = %d",updateDataFlag);
			//			if(updateDataFlag==0)
			//			{
			//			if(master_status_struct_var.ticket_fare == 0)
			//			{
			//				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
			//				if(DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='1'"))
			//				{
			//					master_status_struct_var.ticket_fare=1;
			//					//DbInterface_Delete_Table("COMMIT;");
			//				}
			//				//					else
			//				//						DbInterface_Delete_Table("ROLLBACK;");
			//				CstcLog_printf("OTA::1. FARE CHART RECORD COUNT updateDataFlag = %d",updateDataFlag);
			////				GPRS_check_master_status();
			//			}
			//			}
			if(updateDataFlag==2)
			{
				//				if(master_status_struct_var.ticket_fare ==1)
				//				{
				//					//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				//					if(DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'"))
				//					{
				//						master_status_struct_var.ticket_fare=2;
				//						*j=0;
				//						//						DbInterface_Delete_Table("COMMIT;");
				//					}
				//					//					else
				//					//						DbInterface_Delete_Table("ROLLBACK;");
				//				}
				CstcLog_printf("OTA::2. FARE CHART RECORD COUNT updateDataFlag = %d",updateDataFlag);
			}
			CstcLog_printf("OTA::FARE CHART RECORD COUNT RECEIVED ");

			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::FARE CHART RECORD COUNT parsed ");

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));

				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::FARE CHART RECORD COUNT parsing failed");
				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",FARE_CHART_COUNT_FAILED);

				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg=%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}
			break;
		case FARE_CHART_PKT_ID :
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::FARE CHART RECEIVED ");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				CstcLog_printf("OTA::FARE MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nFARE MASTER");

				ack_flag=1;
				GPRS_TotalRecordCntRecvd +=GPRS_rt_count;
				CstcLog_printf("OTA::GPRS_rt_count = %d",GPRS_rt_count);
				CstcLog_printf("OTA::FARE CHART parsed GPRS_TotalRecordCntRecvd =%d and GPRS_RecordCntToRecv =%d",GPRS_TotalRecordCntRecvd,GPRS_RecordCntToRecv);

				if(GPRS_TotalRecordCntRecvd>=GPRS_RecordCntToRecv)
				{
					CstcLog_printf("OTA::FARE CHART parsed 1.swap");
					sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
					GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
					CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
					GPRS_Err_Msg[GPRS_Err_Len]='\0';
					memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));

					GPRS_Err_Len=0;
					GPRS_rt_count=0;
				}
				else
				{
					//					strcat(GPRS_Err_Msg,"FARE CHART Partial Success");
					//					GPRS_Err_Len = GPRS_Err_Len + strlen("FARE CHART Partial Success");
					//					CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);
					CstcLog_printf("OTA::FARE MASTER RECEIVED ");
					Show_Error_Msg_UART("UPLOADING \nFARE MASTER");

					CstcLog_printf("OTA::FARE CHART parsed 2.swap");
					CstcLog_printf("OTA::GPRS_Err_Len = %d ",GPRS_Err_Len);
					memset(GPRS_Err_MsgAry,0,3);
					sprintf(GPRS_Err_MsgAry,"%02d",FARE_CHART_PARTIAL_SUCCESS);
					CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s ",GPRS_Err_Len,GPRS_Err_Msg);
					CstcLog_printf("OTA::Err_MsgAry_Len = %d GPRS_Err_MsgAry =%s ",strlen(GPRS_Err_MsgAry),GPRS_Err_MsgAry);

					strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
					CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s ",GPRS_Err_Len,GPRS_Err_Msg);
					GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
					CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s ",GPRS_Err_Len,GPRS_Err_Msg);
					CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
					memset(GPRS_Err_MsgAry,0,3);
					GPRS_Err_Msg[GPRS_Err_Len]='\0';
					GPRS_rt_count=0;
					//beep(2800,1000);
				}

			}
			else
			{
				CstcLog_printf("OTA::FARE MASTER RECEIVED ");
				Show_Error_Msg_UART("UPLOADING \nFARE MASTER FAILED");

				ack_flag=0;

				//				strcat(GPRS_Err_Msg,"FARE CHART failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("FARE CHART failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",FARE_CHART_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s Err_Msg1 = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			break;

		case TICKET_TYPE_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			//			if(master_status_struct_var.upload_master==0)
			//			{
			//				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
			//				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='1'"))
			//				{
			//					master_status_struct_var.upload_master=1;
			//					//DbInterface_Delete_Table("COMMIT;");
			//				}
			//				//				else
			//				//					DbInterface_Delete_Table("ROLLBACK;");
			//			}

			CstcLog_printf("OTA::TICKET TYPE MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nTICKET TYPE MASTER");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::TICKET TYPE MASTER parsed ");
				Show_Error_Msg_UART("UPLOADING \nTICKET TYPE MASTER SUCCESS");
				//				strcat(GPRS_Err_Msg,"TICKET TYPE MASTER Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("TICKET TYPE MASTER Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::TICKET TYPE MASTER parsing failed");
				Show_Error_Msg_UART("UPLOADING \nTICKET TYPE MASTER FAILED");
				//				strcat(GPRS_Err_Msg,"TICKET TYPE MASTER failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("TICKET TYPE MASTER failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",TICKET_TYPE_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}

			//GPRS_check_master_status();
			break;
		case TICKET_SUB_TYPE_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::TICKET SUB TYPE MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nTICKET SUB TYPE MASTER");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::TICKET SUB TYPE parsed ");

				Show_Error_Msg_UART("UPLOADING \nTICKET SUB TYPE MASTER SUCCESS");
				//				strcat(GPRS_Err_Msg,"TICKET SUB TYPE MASTER Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("TICKET SUB TYPE MASTER Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg); //need modification for the length
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::TICKET SUB TYPE MASTER parsing failed");
				Show_Error_Msg_UART("UPLOADING \nTICKET SUB TYPE MASTER FAILED");
				//				strcat(GPRS_Err_Msg,"TICKET SUB TYPE MASTER failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("TICKET SUB TYPE MASTER failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",TICKET_SUB_TYPE_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			//GPRS_check_master_status();
			break;

		case BUS_SERVICE_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::BUS SERVICE MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nBUS SERVICE MASTER");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::BUS SERVICE MASTER parsed ");
				Show_Error_Msg_UART("UPLOADING \nBUS SERVICE MASTER SUCCESS");
				//				strcat(GPRS_Err_Msg,"BUS SERVICE MASTER Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("BUS SERVICE MASTER Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;

			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::BUS SERVICE MASTER parsing failed");
				Show_Error_Msg_UART("UPLOADING \nBUS SERVICE MASTER FAILED");
				//				strcat(GPRS_Err_Msg,"BUS SERVICE MASTER failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("BUS SERVICE MASTER failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);
				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",BUS_SERVICE_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			//GPRS_check_master_status();
			break;
		case BUS_BRAND_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::BUS BRAND MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nBUS BRAND MASTER");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::BUS BRAND MASTER parsed ");
				Show_Error_Msg_UART("UPLOADING \nBUS BRAND MASTER SUCCESS");
				//				strcat(GPRS_Err_Msg,"BUS BRAND MASTER Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("BUS BRAND MASTER Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::BUS BRAND MASTER parsing failed");
				Show_Error_Msg_UART("UPLOADING \nBUS BRAND MASTER FAILED");
				//				strcat(GPRS_Err_Msg,"BUS BRAND MASTER failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("BUS BRAND MASTER failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);
				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",BUS_BRAND_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			//GPRS_check_master_status();
			break;

		case DEPOT_MASTER_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::BUS DEPOT MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nBUS DEPOT MASTER");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::BUS DEPOT MASTER parsed ");
				Show_Error_Msg_UART("UPLOADING \nBUS DEPOT MASTER SUCCESS");
				//				strcat(GPRS_Err_Msg,"BUS DEPOT MASTER Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("BUS DEPOT MASTER Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);
				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::BUS DEPOT MASTER parsing failed");
				Show_Error_Msg_UART("UPLOADING \nBUS DEPOT MASTER FAILED");
				//				strcat(GPRS_Err_Msg,"BUS DEPOT MASTER failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("BUS DEPOT MASTER failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);
				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",DEPOT_MASTER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s Err_Msg1 = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			//GPRS_check_master_status();
			break;

		case ROUTE_HEADER_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::Route Header RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nROUTE HEADER");
			CstcLog_printf("OTA::Before GPRS_packet_parser_input_master....tempCh =%c , GPRS_global_pkt_id = %d ,updateDataFlag= %d ",tempCh,GPRS_global_pkt_id,updateDataFlag);
			retval = GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			CstcLog_printf("OTA::After GPRS_packet_parser_input_master returned = %d ",retval);
			CstcLog_printf("OTA::After tempCh =%c , GPRS_global_pkt_id = %d ,updateDataFlag= %d ",tempCh,GPRS_global_pkt_id,updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::Route Header parsed ");
				Show_Error_Msg_UART("UPLOADING \nROUTE HEADER SUCCESS");
				//				strcat(GPRS_Err_Msg,"Route Header Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("Route Header Success");
				CstcLog_printf("UPLOADING ROUTE HEADER SUCCESS....");
				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::Route Header parsing failed");
				Show_Error_Msg_UART("UPLOADING \nROUTE HEADER FAILED");
				//				strcat(GPRS_Err_Msg,"Route Header failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("Route Header failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",ROUTE_HEADER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			//GPRS_check_master_status();
			break;

		case ETM_RW_KEY_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			//			GPRS_check_master_status();
			CstcLog_printf("OTA::ETM READ WRITE KEY RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nETM READ WRITE KEY");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id,updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::ETM READ WRITE KEY parsed ");
				Show_Error_Msg_UART("UPLOADING \nETM READ WRITE KEY SUCCESS");
				//				strcat(GPRS_Err_Msg,"ETM READ WRITE KEY Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("ETM READ WRITE KEY Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);
				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				//				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
				//				{
				//					master_status_struct_var.upload_master=2;
				//					*j=0;
				//					//	DbInterface_Delete_Table("COMMIT;");
				//				}
				//				else
				//					DbInterface_Delete_Table("ROLLBACK;");

				//				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
				//					master_status_struct_var.upload_master=2;
				//GPRS_check_master_status();
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::ETM READ WRITE KEY parsing failed");
				Show_Error_Msg_UART("UPLOADING \nETM READ WRITE KEY FAILED");
				//				strcat(GPRS_Err_Msg,"ETM READ WRITE KEY failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("ETM READ WRITE KEY failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",ETM_READ_WRITE_KEY_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s GPRS_Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			//			GPRS_check_master_status();
			//			return 1;
			break;


		case TRUNK_FEEDER_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::TRUNK AND FEEDER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nTRUNK AND FEEDER");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::TRUNK AND FEEDER parsed ");
				Show_Error_Msg_UART("UPLOADING \nTRUNK AND FEEDER SUCCESS");
				//				strcat(GPRS_Err_Msg,"TRUNK AND FEEDER Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("TRUNK AND FEEDER Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;

			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::TRUNK AND FEEDER failed");
				Show_Error_Msg_UART("UPLOADING \nTRUNK AND FEEDER FAILED");
				//				strcat(GPRS_Err_Msg,"TRUNK AND FEEDER failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("TRUNK AND FEEDER failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				memset(GPRS_Err_MsgAry,0,3);
				sprintf(GPRS_Err_MsgAry,"%02d",TRUNK_FEEDER_FAILED);
				strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s Err_Msg1 = %s",GPRS_Err_Len,GPRS_Err_Msg,GPRS_Err_MsgAry);
				memset(GPRS_Err_MsgAry,0,3);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';

				beep(2800,1000);
			}
			//GPRS_check_master_status();
			break;


		case SERVICE_OP_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::SERVICE_OP RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nSERVICE_OP");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id,updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::SERVICE_OP parsed ");
				Show_Error_Msg_UART("UPLOADING \nSERVICE_OP SUCCESS");
				//				strcat(GPRS_Err_Msg,"SERVICE_OP Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("SERVICE_OP Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;

				//				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
				//					master_status_struct_var.upload_master=2;
				//GPRS_check_master_status();
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::SERVICE_OP parsing failed");
				Show_Error_Msg_UART("UPLOADING \nSERVICE_OP FAILED");
				//				strcat(GPRS_Err_Msg,"SERVICE_OP failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("SERVICE_OP failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);
				sprintf(GPRS_Err_Msg,"%02d",SERVICE_OP_FAILED);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			break;

		case RATE_MASTER_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::RATE MASTER RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nRATE MASTER ");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id,updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::RATE MASTER  parsed ");
				Show_Error_Msg_UART("UPLOADING \nRATE MASTER SUCCESS");
				//				strcat(GPRS_Err_Msg,"SERVICE_OP Success");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("SERVICE_OP Success");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;

				//				if(DbInterface_Update_Master_Status_Data("update master_status set insert_route ='2'"))
				//				{
				//					master_status_struct_var.insert_route=2;
				//					*j=0;
				//				}

			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::RATE MASTER parsing failed");
				Show_Error_Msg_UART("UPLOADING \nRATE MASTER FAILED");
				//				strcat(GPRS_Err_Msg,"SERVICE_OP failed");
				//				GPRS_Err_Len = GPRS_Err_Len + strlen("SERVICE_OP failed");
				//				CstcLog_printf("OTA::GPRS_Err_Len = %d",GPRS_Err_Len);
				sprintf(GPRS_Err_Msg,"%02d",RATE_MASTER_FAILED);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			break;

		case 22:
			CstcLog_printf("OTA::CHECK ROUTE_ID ");

			count_header = get_route_id_from_route_header(GPRS_global_Route_Id);
			count_master = get_route_id_from_route_master(GPRS_global_Route_Id);

			CstcLog_printf("OTA::count_header = %d count_master =  %d ",count_header ,count_master);

			if((count_header > 0 ) && (count_master > 0))
			{
				ack_flag=1;
				CstcLog_printf("OTA::ROUTE_ID present ");

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;
			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::ROUTE_ID_ABSENT");

				sprintf(GPRS_Err_Msg,"%02d",ROUTE_ID_ABSENT);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			break;

		case LC_LOGIN_PKT_ID:
			memset(GPRS_global_buff,0,sizeof(GPRS_global_buff));
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			CstcLog_printf("OTA::LC_LOGIN RECEIVED ");
			Show_Error_Msg_UART("UPLOADING \nLC_LOGIN");
			retval =GPRS_packet_parser_input_master(tempCh,GPRS_global_pkt_id,updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("OTA::LC_LOGIN parsed ");
				Show_Error_Msg_UART("UPLOADING \nLC_LOGIN SUCCESS");

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,sizeof(GPRS_Err_Msg));
				GPRS_Err_Len=0;

			}
			else
			{
				ack_flag=0;
				CstcLog_printf("OTA::LC_LOGIN parsing failed");
				Show_Error_Msg_UART("UPLOADING \nLC_LOGIN FAILED");

				sprintf(GPRS_Err_Msg,"%02d",LC_LOGIN_FAILED);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("OTA::GPRS_Err_Len = %d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				beep(2800,1000);
			}
			break;

		default :
			break;
		}
	}

	if(ack_flag)
	{
		//		//length=100;
		//		CstcLog_printf("OTA::GPRS_Err_Len=%d>>------ACK1--------<<%d",GPRS_Err_Len,(sizeof(ack_packet_struct)+GPRS_Err_Len+1+1));
		//		pkt_ptr = (char*)malloc((sizeof(ack_packet_struct)+GPRS_Err_Len+1+1));
		//		memset(pkt_ptr,0,(sizeof(ack_packet_struct)+GPRS_Err_Len+1+1));
		//
		//		CstcLog_printf("OTA::>>------ACK2--------<<");
		//		//pkt_ptr = (char*)malloc(sizeof(ack_packet_struct));
		//		GPRS_compose_ack_packet(pkt_ptr,&length);
		//
		//		pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length+1+1));
		//		memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length+1+1));
		//
		//		CstcLog_printf("OTA::%d>>------ACK3--------<<%d",length,(sizeof(packet_format_struct)+length+1+1));
		//		GPRS_final_packet(pkt_format_ptr, pkt_ptr, &length);
		//
		//		CstcLog_printf("OTA::Sent bytes are : ");
		//		//CstcLog_HexDump_1(pkt_format_ptr,length);
		//
		//		CstcLog_printf("OTA:: >> -------- ACK4  -------------<<");
		//		sn =(char *)malloc(nbytes*sizeof(char));
		//		memset(sn,0,nbytes);
		//		UART_get_machine_info(sn,nbytes);
		//		CstcLog_printf("OTA::THREAD::sn = %s",sn);
		//		data_len = length+nbytes+20+157+1+1+200;
		//
		//		CstcLog_printf("OTA::data_len = %d ",data_len);
		//		ack_post = (char*)malloc(data_len*sizeof(char));
		//		CstcLog_printf("OTA::bTemp = %d ",ack_post);
		//		memset(ack_post,0,data_len);
		//
		//		CstcLog_printf("OTA:: >> -------- ACK5 -------------<<");
		//		//		sprintf(ack_post,"POST /log.php HTTP/1.1\r\nHost: 223.30.102.30\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&data=%s&mode=1\r\n",(length+nbytes+20),sn,pkt_format_ptr);  //Snehal: Need to change this URL in future
		//		//		ack_post[strlen(ack_post)]='\0';
		//
		//		/*if(strcmp(GPRS_Err_Msg,"01") == 0)
		//		{
		//			strcpy(temp_Ary,"SUCCESS");
		//			temp_Ary[strlen(temp_Ary)]='\0';
		//		}
		//		else
		//		{
		//			strcpy(temp_Ary,"FAILURE");
		//			temp_Ary[strlen(temp_Ary)]='\0';
		//		}*/
		//
		//		//sprintf(ack_post,"POST /wget.php HTTP/1.1\r\nHost: 223.30.102.30\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&scheduleid=%s&timestamp=%04d-%02d-%02d %02d:%02d:%02d&type=%s\r\n",(strlen(duty_master_struct_var.schedule_id)+nbytes+55+strlen(temp_Ary)),sn,duty_master_struct_var.schedule_id,ptm.tm_year + 1900, ptm.tm_mon + 1, ptm.tm_mday, ptm.tm_hour, ptm.tm_min, ptm.tm_sec,temp_Ary);
		//
		//		sprintf(ack_post,"POST /wget.php HTTP/1.1\r\nHost: 223.30.102.30\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&depotid=%s&scheduleid=%s&timestamp=%04d-%02d-%02d %02d:%02d:%02d&type=%s\r\n",(strlen(duty_master_struct_var.schedule_id)+nbytes+64+length+(strlen(waybill_master_struct_var.depot_id))),sn,waybill_master_struct_var.depot_id,duty_master_struct_var.schedule_id,ptm.tm_year + 1900, ptm.tm_mon + 1, ptm.tm_mday, ptm.tm_hour, ptm.tm_min, ptm.tm_sec,pkt_format_ptr);
		//		ack_post[strlen(ack_post)]='\0';
		//
		//		CstcLog_printf("OTA::Sent %s %d bytes",ack_post,data_len);
		//		CstcLog_printf("OTA::>>------iclient_socket = %d-----------<<",iclient_socket);
		//
		//		if(tcp_write(iclient_socket, ack_post, strlen(ack_post)) < 0)
		//		{
		//			CstcLog_printf("OTA::ACK Connection was broken!!!");
		//			Show_Error_Msg_UART("ACKNOWLEDGE FAILED");
		//			beep(2800,1000);
		//			if(pkt_ptr!=NULL)
		//			{
		//				free(pkt_ptr);
		//				pkt_ptr=NULL;
		//			}
		//			if(pkt_format_ptr!=NULL)
		//			{
		//				free(pkt_format_ptr);
		//				pkt_format_ptr=NULL;
		//			}
		//			if(ack_post!=NULL)
		//			{
		//				free(ack_post);
		//				ack_post=NULL;
		//			}
		//			if(sn!=NULL)
		//			{
		//				free(sn);
		//				sn=NULL;
		//			}
		//			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
		//			memset(temp_Ary,0,sizeof(temp_Ary));
		//			//			memset(GPRS_Err_Msg1,0,500);
		//			GPRS_Err_Len=0;
		//			return 0;
		//		}
		//		else
		{
			CstcLog_printf("OTA::Freeing gprs pointers.....!!!");
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			if(ack_post!=NULL)
			{
				free(ack_post);
				ack_post=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			memset(temp_Ary,0,sizeof(temp_Ary));
			//			memset(GPRS_Err_Msg1,0,500);
			GPRS_Err_Len=0;
			return 1;
		}
	}
	if(pkt_ptr!=NULL)
	{
		free(pkt_ptr);
		pkt_ptr=NULL;
	}
	if(pkt_format_ptr!=NULL)
	{
		free(pkt_format_ptr);
		pkt_format_ptr=NULL;
	}
	if(ack_post!=NULL)
	{
		free(ack_post);
		ack_post=NULL;
	}
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
	memset(temp_Ary,0,sizeof(temp_Ary));
	return 0;

}

int GPRS_packet_parser_input_master(char tempCh,int recvPktID,int updateDataFlag)
{
	//	char *mac_ID = NULL;
	//	char *etim_no=NULL;
	//FILE* dbTblDataFile= NULL;
	//FILE* dbDataFile= NULL;
	//	int temp = 0;
	//	int len = 0,pkt_data_len=0,recLen=0,notFirstRecFlag=0;
	char *sub_string=NULL;
	char *waybill_sub_str1=NULL, *waybill_sub_str2=NULL;
	char tempName[100]={'\0'};
	char tempStr[100] = {'\0'}; //[1024];
	char sysCmdStr[100] = {'\0'}; //[1024];
	int dbretval=0,retrycnt =0;//tempStrLen=0,count=0;
	//	char *sn = NULL;
	char Err_MsgAry[3]={'\0'};
	unsigned char *inputData=NULL;
	struct stat sb;
	//	int flexi_fare_count = 0, rowcount = 0;
	char *sql =NULL;
	int sql_len = 150;

	if(tempCh == ';')
	{

		CstcLog_printf("recvPktID %d updateDataFlag %d",recvPktID,updateDataFlag);
		memset(tempName,0,100);

		switch(recvPktID)
		{
		case 1 :/*-------------------WAYBILL----------------------*/
			//			tempStrLen = sizeof(waybill_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "waybill_master",14);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");
			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from waybill_master"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case GPRS_SCHEDULE_MASTER_PKT_ID:
			if(updateDataFlag==1)
			{
				sql=(char*)malloc(sql_len*sizeof(char));
				CstcLog_printf("GPRS_SCHEDULE_MASTER_PKT_ID duty_master_struct_var.schedule_id = %s",duty_master_struct_var.schedule_id);
				//				sprintf(sql,"delete from schedule_master where schedule_id = '%s' and trip_end_condition = 'N' and trip_status = 'N'",duty_master_struct_var.schedule_id);
				sprintf(sql,"delete from schedule_master_restore");
				if(!DbInterface_Delete_Table(sql))
				{
					return 0;
				}

				if(sql != NULL)
				{
					free(sql);
					sql = NULL;
				}
				//				if(!DbInterface_Delete_Table_active_schedule("Delete from route_master where schedule_no = ",global_schedule))
				//					return 0;
				//				if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart where schedule_no = ",global_schedule))
				//					return 0;
				//				if(!DbInterface_Delete_Table_active_schedule("Delete from route_header where schedule_no = ",global_schedule))
				//					return 0;
				//
			}
			break;



		case 3: /*-----------------SCHEDULE-----------------------*/
			//			count=duty_master_schedule_check(GPRS_global_schedule);
			CstcLog_printf("SCHEDULE_MASTER duty_master_schedule_check GPRS_global_schedule = %s ",GPRS_global_schedule);
			//			if(count > 0)
			//			{
			CstcLog_printf("Schedule is same..........");
			//			}
			//			else
			//			{
			//if(!MultipleScheduleFlag)
			//				{
			//					//DbInterface_Delete_Table("BEGIN TRANSACTION;");
			//					DbInterface_Update_Master_Status_Data("update master_status set insert_route ='0'");
			//					DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='0'");
			//					//DbInterface_Delete_Table("COMMIT;");
			//				}
			//			}
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			//			tempStrLen = sizeof(schedule_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "schedule_master", 15);
			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				CstcLog_printf("GPRS_global_schedule = %s ", GPRS_global_schedule);

				//				if(!DbInterface_Delete_Table_active_schedule("Delete from schedule_master where schedule_no = ",GPRS_global_schedule))
				if(!DbInterface_Delete_Table("Delete from schedule_master_restore"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");

				//				if(!DbInterface_Delete_Table("Delete from schedule_master"))
				//					return 0;
#if 0 //MULTIPLE_SCHEDULE_ENABLE
				//				if(!DbInterface_Delete_Table_active_schedule("Delete from route_master where schedule_no = ",GPRS_global_schedule))
				//					//if(!DbInterface_Delete_Table("Delete from route_master"))
				//				{
				//					//					DbInterface_Delete_Table("ROLLBACK;");
				//					//					if(tempStr!=NULL)
				//					//					{
				//					//						free(tempStr);
				//					//						tempStr=NULL;
				//					//					}
				//					return 0;
				//				}
				//				//				else
				//				//					DbInterface_Delete_Table("COMMIT;");
				//
				//				if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart where schedule_no = ",GPRS_global_schedule))
				//					//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				//				{
				//					//					DbInterface_Delete_Table("ROLLBACK;");
				//					//					if(tempStr!=NULL)
				//					//					{
				//					//						free(tempStr);
				//					//						tempStr=NULL;
				//					//					}
				//					return 0;
				//				}
				//				//				else
				//				//					DbInterface_Delete_Table("COMMIT;");
				//
				//				if(!DbInterface_Delete_Table_active_schedule("Delete from route_header where schedule_no = ",GPRS_global_schedule))
				//					//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				//				{
				//					//					DbInterface_Delete_Table("ROLLBACK;");
				//					//					if(tempStr!=NULL)
				//					//					{
				//					//						free(tempStr);
				//					//						tempStr=NULL;
				//					//					}
				//					return 0;
				//				}
				//				//				else
				//				//					DbInterface_Delete_Table("COMMIT;");
				//				//if(!MultipleScheduleFlag)
				//				//				{
				//				//					DbInterface_Update_Master_Status_Data("update master_status set insert_route ='0'");
				//				//					DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='0'");
				//				//				}
#endif

			}
			break;

		case 5:/*--------------------DUTY------------------------*/
			//			tempStrLen = sizeof(duty_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "duty_master",11);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				insert_duty_master_in_restore();
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from duty_master"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 7: /*-------------------ROUTE------------------------*/
			//			tempStrLen = sizeof(route_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "route_master",12);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				CstcLog_printf("GPRS_global_schedule = %s ", GPRS_global_schedule);

				if(!DbInterface_Delete_Table_active_schedule("Delete from route_master_restore where schedule_id = ",GPRS_global_schedule))
					//				if(!DbInterface_Delete_Table("Delete from route_master"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 8:
		case 9:/*--------------------FARE------------------------*/
			//tempStrLen =sizeof(fare_chart_struct)+40;
			//			tempStrLen =sizeof(fare_chart_struct)+(VERSION_NO_LEN+1)+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)
			//			+(BUS_SRVC_ID_LEN+1)+(BUS_STOP_SEQ_LEN+1)+(BUS_STOP_CODE_LEN+1)
			//			+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)+(BUS_STOP_SEQ_LEN+1)
			//			+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)
			//			+1+(DATE_LEN+1)+(TIME_LEN+1)+(DATE_LEN+1)+(TIME_LEN+1)+1+1+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "fare_chart",10);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				CstcLog_printf("GPRS_global_schedule = %s ", GPRS_global_schedule);

				if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart_restore where schedule_id = ",GPRS_global_schedule))
					//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 10:/*-----------------Ticket Type-------------------------*/
			//			tempStrLen = sizeof(ticket_type_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "ticket_type",11);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from ticket_type"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 11:/*---------------------Ticket sub Type------------------------*/
			//			tempStrLen = sizeof(ticket_sub_type_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "ticket_sub_type",15);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from ticket_sub_type"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 12:/*--------------------Bus Services------------------------------*/
			//			tempStrLen = sizeof(bus_service_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "bus_service",11);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from bus_service"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 13:/*----------------------Bus Brand-------------------------------*/
			//			tempStrLen = sizeof(bus_brand_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "bus_brand",9);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from bus_brand"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");

			}
			break;

		case 14:/*----------------------Depot-------------------------------*/
			CstcLog_printf("Inside Depot Parser function");
			//			tempStrLen = sizeof(depot_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "depot",5);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from depot"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 15:/*-----------------Route Header------------------------*/
			//			tempStrLen = sizeof(route_header_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "route_header",12);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");
			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table_active_schedule("Delete from route_header_restore where route_id = ",GPRS_global_schedule))
					//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 16:/*--------------------ETM RW key---------------------------*/
			//			tempStrLen = sizeof(etim_rw_keys_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "etim_rw_keys",13);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from etim_rw_keys"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 17:/*--------------------TRUNK AND FEEDER---------------------------*/
			//tempStrLen = sizeof(trunk_feeder_master_struct)+40;

			//			tempStrLen = 2+1+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)
			//			+(BUS_STOP_SEQ_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)
			//			+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "trunk_feeder_master",19);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from trunk_feeder_master"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");

			}
			break;

		case 18:/*-------------------SERVICE OP-----------------------*/
			//			tempStrLen = sizeof(service_op_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "service_op",10);

			//			tempStrLen = 3+(NAME_ENG_LEN+1)+(NAME_KAN_LEN+1)+1+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "service_op",10);
			//			DbInterface_Delete_Table("BEGIN TRANSACTION;");

			if(updateDataFlag==1)
			{
				//DbInterface_Delete_Table("BEGIN TRANSACTION;");
				if(!DbInterface_Delete_Table("Delete from service_op"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
			}
			break;

		case 19:
			if(updateDataFlag==1)
			{
				//if(!DbInterface_Delete_Table("Delete from rate_master"))
				if(!DbInterface_Delete_Table_active_schedule("Delete from rate_master_restore where rate_id = ",GPRS_global_Rate_Id))
				{
					return 0;
				}
			}          //still need to discuss

			/***--------------------New Change---------------------------------------**/
			//if(!MultipleScheduleFlag)
			{
				//				sql=(char*)malloc(sql_len*sizeof(char));
				//
				//				CstcLog_printf("duty_master_struct_var.schedule_no = %s",duty_master_struct_var.schedule_no);
				//				sprintf(sql,"schedule_master where is_flexi_fare = 'Y' and schedule_id =  '%s' ",duty_master_struct_var.schedule_id);
				//				flexi_fare_count =DbInterface_Get_Row_Count(sql,strlen(sql));
				//
				//				//			memset(sql,0,sql_len);
				//				//			sprintf(sql,"schedule_master where schedule_no =  '%s' ",duty_master_struct_var.schedule_no);
				//				//			rowcount = DbInterface_Get_Row_Count(sql,strlen(sql));
				//
				//				CstcLog_printf("flexi_fare_count = %d  rowcount = %d ",flexi_fare_count,rowcount);

				//				if(flexi_fare_count > 0 ) //(flexi_fare_count != rowcount )
				//				{
				//					DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='0'");
				//				}
				//				else
				//				{
				//					DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'");
				//				}

				if(sql != NULL)
				{
					free(sql);
					sql=NULL;
				}
			}
			/***--------------------New Change---------------------------------------**/
			//DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'");
			break;

		case 23:/*-------------------LC LOGIN----------------------*/
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from lc_login"))
				{
					return 0;
				}

			}
			break;

		default:
			return 0;
			break;
		}
		//		CstcLog_printf("reading file tempstr allocated with required memory of length = %d",(tempStrLen+1));


		if(recvPktID!=8)
		{
			if (1==1)
			{
				if (stat (DB_INPUT_FILE_ADDR, & sb) == 0)
				{
					//					inputData= (unsigned char*)malloc(sb.st_size*sizeof(char));
					inputData = read_whole_file (DB_INPUT_FILE_ADDR);
					if((sub_string=strstr((const char *)inputData,";")) != NULL)
					{
						*(sub_string+1)='\0';
					}
					CstcLog_printf("Query to be executed:: %s ", inputData);

					if(recvPktID==1)
					{
						CstcLog_printf("waybill query fetched from file ");
						if((waybill_sub_str1=strstr((const char *)inputData,"\'")) != NULL)
						{
							CstcLog_printf("waybill query search 1 ");
							if((waybill_sub_str2=strstr((const char *)(waybill_sub_str1+1),"\'")) != NULL)
							{
								CstcLog_printf("waybill query search 2 ");
								if((waybill_sub_str2-(waybill_sub_str1+1))<WAYBILL_NO_LEN)
								{
									CstcLog_printf("waybill is Invalid:: %s ", waybill_sub_str1);
									*(waybill_sub_str2+1)='\0';
								}
								CstcLog_printf("waybill_sub_str length:: %u-(%u+1)=%u ", waybill_sub_str2,waybill_sub_str1,(waybill_sub_str2-(waybill_sub_str1+1)));
							}
						}
					}
					dbretval=0;
					dbretval=DbInterface_exec_sql((unsigned char *)inputData,GPRS_Err_Msg,&GPRS_Err_Len);

					//					if (GPRS_Err_Len > 0)
					if (dbretval!=1)
					{
						CstcLog_printf("fail %s ", inputData);
						//						memset(sysCmdStr,0,100);
						//						sprintf(sysCmdStr,"cp %s /home/user0/BMTC/err%d.txt", DB_INPUT_FILE_ADDR, recvPktID);
						//						system(sysCmdStr);
						//dbretval = 1;
						//						CstcLog_printf("exec sql \"%s\" ", GPRS_Err_Msg);
					}
					free (inputData);
					inputData=NULL;
				}
			}
			else
			{
				//			sleep(5);
				memset(tempStr,0,100);
				sprintf(tempStr,"chmod -R 777 %s",DB_INPUT_FILE_ADDR);
				CstcLog_printf("System chmod command \"%s\" ", tempStr);
				dbretval=system(tempStr);
				CstcLog_printf("System chmod command retval \"%d\" ", dbretval);
				memset(sysCmdStr,0,100);
				sprintf(sysCmdStr,"sqlite3 %s < %s",DB_ADDR,DB_INPUT_FILE_ADDR);
				//						memcpy(tempStr,"sqlite3 ",strlen("sqlite3 "));
				//						memcpy(tempStr+strlen("sqlite3 "),DB_ADDR,strlen(DB_ADDR));
				//						memcpy(tempStr+strlen("sqlite3 ")+strlen(DB_ADDR),"sqlite3 ",strlen("sqlite3 "));
				CstcLog_printf("Insertion in table command \"%s\" failed", sysCmdStr);


				//			if(recvPktID == 9)
				//			{
				//				sleep(4);
				//			}
				//			else
				//			{
				//				sleep(1);
				//			}
				//			sprintf(GPRS_Err_Msg,"%02d",SQLITE_DB_INSERTION_FAILED);
				//			strcat(Err_Msg1,GPRS_Err_Msg);
				//			GPRS_Err_Len=GPRS_Err_Len+strlen(Err_Msg1);
				//			CstcLog_printf("GPRS_Err_Len = %d GPRS_Err_Msg =%s Err_Msg1 = %s",GPRS_Err_Len,GPRS_Err_Msg,Err_Msg1);

				while(!system(NULL)){
					CstcLog_printf("Shell is unavailable");
					sleep(2);
				}
				sleep(2);
				dbretval=0;
				dbretval=system(sysCmdStr);//DbInterface_Insert_In_All_Tables1(tempName, tempStr,recLen);
				retrycnt =0;
				while(!system(NULL)){
					CstcLog_printf("Shell is unavailable");
					sleep(2);
				}
				sleep(2);
				//			while(retrycnt<5)
				//			{
				//				dbretval=system(tempStr);
				CstcLog_printf("System command retval \"%d\" ", dbretval);
			}
			//				if(dbretval==0)
			//					break;
			//				retrycnt++;
			//
			//			}
			//			if(dbretval == 0)
			if(dbretval == 1)
			{
				CstcLog_printf("Insertion in table  success");

				sprintf(GPRS_Err_Msg,"%02d",SUCCESS);

				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(GPRS_Err_Msg);
				CstcLog_printf("GPRS_Err_Msg = %s GPRS_Err_Len = %d",GPRS_Err_Msg,GPRS_Err_Len);
				GPRS_Err_Msg[GPRS_Err_Len]='\0';
				memset(GPRS_Err_Msg,0,ERROR_MSG_BUFFER_LEN);            /*May be need to change...*/
				GPRS_Err_Len=0;
				//memcpy(GPRS_Err_Msg,"Data Insertion successful",25);
				//				strcat(GPRS_Err_Msg,"Data Insertion successful");
				//				GPRS_Err_Len =GPRS_Err_Len+strlen("Data Insertion successful");
				//				DbInterface_Delete_Table("COMMIT;");

				if(recvPktID == 5)
				{
					DbInterface_Delete_Table("Delete from duty_master_restore");
				}

				CstcLog_printf("Retruning succesfully........../");

				return 1;
			}
			else
			{
				CstcLog_printf("Insertion in table  failed");
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",SQLITE_DB_INSERTION_FAILED);
				strcat(GPRS_Err_Msg,Err_MsgAry);
				GPRS_Err_Len=GPRS_Err_Len+2;//strlen(Err_MsgAry);
				CstcLog_printf("GPRS_Err_Len = %d GPRS_Err_Msg =%s Err_Msg1 = %s",GPRS_Err_Len,GPRS_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				//				strcat(GPRS_Err_Msg,"Data Insertion in table failed");
				//				GPRS_Err_Len =GPRS_Err_Len+strlen("Data Insertion in table failed");
				//				CstcLog_printf("GPRS_Err_Len =%d GPRS_Err_Msg =%s",GPRS_Err_Len,GPRS_Err_Msg);

				//							break;
				//				DbInterface_Delete_Table("ROLLBACK;");
				if(recvPktID == 5)
				{
					DbInterface_Delete_Table("Delete from duty_master");
					if(reinsert_duty_master_restore_in_duty_master())
						DbInterface_Delete_Table("Delete from duty_master_restore");
				}
				return 0;
			}
		}
	}
	else
	{
		CstcLog_printf("Parsing Fail Resend Packet");
		sprintf(Err_MsgAry,"%02d",RESEND_PACKETS);
		strcat(GPRS_Err_Msg,Err_MsgAry);
		GPRS_Err_Len=GPRS_Err_Len+2;//strlen(Err_MsgAry);
		CstcLog_printf("GPRS_Err_Len = %d GPRS_Err_Msg =%s Err_MsgAry = %s",GPRS_Err_Len,GPRS_Err_Msg,Err_MsgAry);

		return 0;
	}

	return 1;
}

void GPRS_compose_ack_packet(char *pkt_ptr,int *length)
{
	char *sn = NULL;
	char *etim_no = NULL;
	//	char *mac_ID = NULL;
	FILE* tempFile= NULL;
	int nbytes = 10;
	int temp = ACK_PKT_ID;

	char GPRS_Err_MsgAry[3]={'\0'};

	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);
	UART_get_machine_info(sn, nbytes);

	etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
	memset(etim_no,0,(TERMINAL_ID_LEN+1));
	tempFile= fopen(ETIM_NO_FILE_ADDR,"r+");
	if(tempFile == NULL)
	{
		CstcLog_printf("ERROR OPENING FILE etim_no.txt");
		sprintf(GPRS_Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
		strcat(GPRS_Err_Msg,GPRS_Err_MsgAry);
		GPRS_Err_Len = GPRS_Err_Len+2;//strlen(GPRS_Err_MsgAry);
		memset(GPRS_Err_MsgAry,0,3);
	}
	else
	{
		fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile);
		fflush(tempFile);
		CstcLog_printf("etim_no :- %s", etim_no);
		fclose(tempFile);
	}

	sprintf(gprs_ack_packet_struct_var.pkt_id_ack_packet, "%d", temp);

	memcpy(gprs_ack_packet_struct_var.machine_no, sn, MACHINE_SN_LEN);

	strcpy(gprs_ack_packet_struct_var.mac_address,"000000000000");

	if(strlen(etim_no)==0)
		memset(gprs_ack_packet_struct_var.terminal_id,'0', TERMINAL_ID_LEN);
	else
		strcpy(gprs_ack_packet_struct_var.terminal_id, etim_no);

	gprs_ack_packet_struct_var.err_ack=(char *)malloc((GPRS_Err_Len+1)*sizeof(char));
	memset(gprs_ack_packet_struct_var.err_ack,0,(GPRS_Err_Len+1));

	if(GPRS_Err_Len==0)
	{
		sprintf(GPRS_Err_Msg,"%02d",SUCCESS);
		GPRS_Err_Len=2;//strlen(GPRS_Err_Msg);
		CstcLog_printf("GPRS_Err_Len =%d ,GPRS_Err_Msg = %s",GPRS_Err_Len,GPRS_Err_Msg);
	}
	else
	{
		CstcLog_printf("GPRS_Err_Len =%d ,GPRS_Err_Msg = %s",GPRS_Err_Len,GPRS_Err_Msg);
	}


	//CstcLog_HexDump_1(GPRS_Err_Msg,GPRS_Err_Len);

	memcpy(gprs_ack_packet_struct_var.err_ack,GPRS_Err_Msg,GPRS_Err_Len);
	gprs_ack_packet_struct_var.err_ack[GPRS_Err_Len]='\0';

	CstcLog_printf("GPRS_Err_Len =%d",GPRS_Err_Len);

	sprintf(gprs_ack_packet_struct_var.err_length,"%02d",GPRS_Err_Len);

	memcpy(pkt_ptr, gprs_ack_packet_struct_var.pkt_id_ack_packet, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),gprs_ack_packet_struct_var.terminal_id, TERMINAL_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN), gprs_ack_packet_struct_var.machine_no, MACHINE_SN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN), gprs_ack_packet_struct_var.mac_address, MAC_ADDR_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), gprs_ack_packet_struct_var.err_length,ERR_LENGTH_LEN );
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+ERR_LENGTH_LEN), gprs_ack_packet_struct_var.err_ack,GPRS_Err_Len );

	CstcLog_printf("ack pkt_ptr %s ",pkt_ptr);
	*length += PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+ERR_LENGTH_LEN+GPRS_Err_Len;
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	if(etim_no!=NULL)
	{
		free(etim_no);
		etim_no=NULL;
	}
	//	if(mac_ID!=NULL)
	//	{
	//		free(mac_ID);
	//		mac_ID=NULL;
	//	}
	if(gprs_ack_packet_struct_var.err_ack!=NULL)
	{
		free(gprs_ack_packet_struct_var.err_ack);
		gprs_ack_packet_struct_var.err_ack=NULL;
	}

}

void GPRS_final_packet(char * pkt_format_ptr, char * pkt_ptr, int *length)
{
	int temp = 0;
	packet_format_struct_var.start_indicator = ';';
	temp = *length + START_INDICATOR_LEN+DATA_LENGTH_LEN+CHECKSUM_LEN; // (length - 45);

	sprintf((packet_format_struct_var.data_length), "%08d", temp);
	packet_format_struct_var.data = pkt_ptr;

	memset(packet_format_struct_var.checksum,'0', CHECKSUM_LEN);

	memcpy(pkt_format_ptr, &(packet_format_struct_var.start_indicator), START_INDICATOR_LEN);
	memcpy((pkt_format_ptr + START_INDICATOR_LEN) , (packet_format_struct_var.data_length), DATA_LENGTH_LEN);
	memcpy((pkt_format_ptr + START_INDICATOR_LEN+DATA_LENGTH_LEN), packet_format_struct_var.data, *length);
	memcpy((pkt_format_ptr + START_INDICATOR_LEN+DATA_LENGTH_LEN + *length), packet_format_struct_var.checksum,CHECKSUM_LEN);
	*length += START_INDICATOR_LEN+DATA_LENGTH_LEN+CHECKSUM_LEN;
}

int get_formatted_string_length(char *Formatted_string,...)
{
	int length = 0;
	char buffer[1000];
	va_list args;
	va_start (args, Formatted_string);
	length = vsprintf(buffer,Formatted_string, args);
	va_end (args);
	return length;
}
int dynamic_printf(char *Dest_string,char *Formatted_string,...)
{
	int length = 0;
	va_list args;
	va_start (args, Formatted_string);
	length = vsprintf(Dest_string,Formatted_string, args);
	va_end (args);
	return length;
}
int dynamic_strcat(char *Dest_string,char *Formatted_string,...)
{
	int length = 0;
	va_list args;
	va_start (args, Formatted_string);
	length = vsprintf(&Dest_string[strlen(Dest_string)],Formatted_string, args);
	va_end (args);
	return length;
}


/*************************************sudhakar_online update using curl****************************************************/

int online_update(void)
{
	int ret=0;

	if(!Gprs_Interface_Init())
	{
		CstcLog_printf("failed to start gprs...");
		return 0;
	}
	CstcLog_printf("gprs started..");
	if(! Gprs_Set_PPP_Connection_Curl())   //need to check good quality signal
	{
		//swap display wnet error here
		CstcLog_printf("failed hereeeee...");
		Gprs_Interface_Deinit();
		return 0;
	}


	CstcLog_printf("gprs ppp connection is set...");

	ret= file_download_curl(); //pkg file is being downloaded here.. '1'=success/ '0'=failed
	Gprs_Close_PPP_Connection();
	Gprs_Interface_Deinit();
	if(ret==0)
	{
		Show_Error_Msg("Downloading Failed..");
		CstcLog_printf("file_download_curl failed hereeeee");
		return 0;
	}
	else if(ret==2)
	{
		CstcLog_printf("version upto date..");
		return 0;
	}

	Show_Error_Msg("Downloading Successful..");
	CstcLog_printf("updated pkg file download successful..");

	/**************************************************/

	Gprs_Close_PPP_Connection();
	CstcLog_printf("gprs ppp closed..");
	Gprs_Interface_Deinit();
	CstcLog_printf("gprs closed..");

	return 1;
}


int file_download_curl(void)
{
	CURL *curl;
	CURLcode res;
	char *bTemp = NULL;
	char sn[MACHINE_SN_LEN+1]={'\0'};

	int val=-1/*,ret=0*/;
	unsigned int new_update=1,size=0,data_len=0;

	char rbTemp[5000]={'\0'};
	char temp_url[250]={'\0'};
	char curl_url_string[100]={0};

	static FILE *temp_pkg_file;
	static FILE *check_file;

	if(strlen(waybill_master_struct_var.waybill_no)==0)
		strcpy(waybill_master_struct_var.waybill_no,"0");

	curl = curl_easy_init();
	if(curl==NULL)
	{
		CstcLog_printf("UPDATEAPP::Failed to initialise curl.... :(");

		return 0;
	}
	CstcLog_printf("UPDATEAPP::Initialised curl...");

	sys_get_sn(sn, (MACHINE_SN_LEN+1));
	CstcLog_printf("UPDATEAPP::serial_no = %s",sn);

	data_len=MACHINE_SN_LEN+ APP_VER_NO_LEN+ APP_VER_DATETIME_LEN +WAYBILL_NO_LEN+3+6; // 3 for three commas(,) used in string,6 for " data=".
	CstcLog_printf("UPDATEAPP::data_len = %d",data_len);


	sprintf(curl_url_string,"%s","http://180.92.175.103:8080/its/fota.action");
	curl_easy_setopt(curl, CURLOPT_URL,curl_url_string);

	CstcLog_printf("UPDATEAPP::Posting version info to server:%s",curl_url_string);

	bTemp = (char*)malloc((data_len+1)*sizeof(char));
	memset(bTemp,0,(data_len+1));
	sprintf(bTemp,"data=%s,%s,%s,%s",sn,APP_VERSION,APP_VER_DATETIME,waybill_master_struct_var.waybill_no);
//	bTemp[strlen(bTemp)]='\0';

	CstcLog_printf("UPDATEAPP::i.e.: %s\n Len :%d Bytes",bTemp,strlen(bTemp));

	val= curl_easy_setopt(curl, CURLOPT_POSTFIELDS,bTemp);
	CstcLog_printf("UPDATEAPP::Posting info finished...val = %d",val);

	if(val!=0)
		CstcLog_printf("UPDATEAPP::curl_easy_setopt failed..");


	res = curl_easy_perform(curl);

	if(res != CURLE_OK)
	{
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));

		if(bTemp!=NULL)
		{
			free(bTemp);
			bTemp=NULL;
		}
		if(curl !=NULL)
		{
			curl_easy_cleanup(curl);
			curl=NULL;
		}

		return 0;
	}

	CstcLog_printf("UPDATEAPP::curl_easy_perform() successful:...............");

	/********************************************************************************/

	check_file = fopen(ONLINE_UDATE_FILE_ADDR,"w+");
	if (check_file == NULL)
	{
		CstcLog_printf("UPDATEAPP::Could not open temp .txt file....");
		if(bTemp!=NULL)
		{
			free(bTemp);
			bTemp=NULL;
		}
		if(curl !=NULL)
		{
			curl_easy_cleanup(curl);
			curl=NULL;

		}

		return 0;
	}

	CstcLog_printf("UPDATEAPP:: temp .txt File opened successfully.....");

	/* Define our callback to get called when there's data to be written */
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);

	/* Set a pointer to dummy_file ptr to pass to the callback */
	curl_easy_setopt(curl, CURLOPT_WRITEDATA,check_file);

	res = curl_easy_perform(curl);

	if(res != CURLE_OK)
	{
		fprintf(stderr, "\ncurl_easy_perform() failed: %s\n",curl_easy_strerror(res));
		if(bTemp!=NULL)
		{
			free(bTemp);
			bTemp=NULL;
		}
		if(check_file!=NULL)
		{
			fclose(check_file);
			check_file=NULL;
		}
		if(curl !=NULL)
		{
			curl_easy_cleanup(curl);
			curl=NULL;

		}

		remove(ONLINE_UDATE_FILE_ADDR);  //remove the temp .txt file

		CstcLog_printf("UPDATEAPP::curl_easy_perform() failed:...............");
		return 0;

	}

	CstcLog_printf("UPDATEAPP::curl_easy_perform() successful:...............");

	if(curl !=NULL)
	{
		curl_easy_cleanup(curl);
		curl=NULL;
	}

	/* Get file size */
	fseek(check_file, 0L, SEEK_END);
	size = ftell(check_file);

	CstcLog_printf("UPDATEAPP::txt file size = %d",size);

	fseek(check_file, 0L, SEEK_SET);

	/* Read data */
	fread(rbTemp, size, 1, check_file);
	CstcLog_printf("UPDATEAPP::rbTemp = %s", rbTemp);

	if(check_file!=NULL)
	{
		fclose(check_file);
		check_file=NULL;
	}

	if(strstr(rbTemp,"NO UPGRADE REQUIRED"))
	{
		CstcLog_printf("UPDATEAPP::version is upto date");
		Show_Error_Msg("No Upgrade Required");
		new_update=0;
	}

	CstcLog_printf("UPDATEAPP::new_update available");

	/****************************************************update the the app **********************************/

	if(new_update)   //update available
	{
		CstcLog_printf("UPDATEAPP::new_update available");
		CstcLog_printf("UPDATEAPP::Part for Downloading pkg file from Server : ");

		Show_Error_Msg("Downloading Updates...");


		curl = curl_easy_init();
		if(curl ==NULL)
		{

			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}

			if(check_file!=NULL)
			{
				fclose(check_file);
				check_file=NULL;
			}

			remove(ONLINE_UDATE_FILE_ADDR);  //remove the temp .txt file
			return 0;
		}

		curl_easy_setopt(curl, CURLOPT_URL, "http://180.92.175.103:8080/its/fota.action");
		val= curl_easy_setopt(curl, CURLOPT_POSTFIELDS,rbTemp);

		CstcLog_printf("UPDATEAPP::Posting info finished...val = %d",val);

		res = curl_easy_perform(curl);

		if(res != CURLE_OK)
		{
			fprintf(stderr, "\n\ncurl_easy_perform() failed: %s\n",
					curl_easy_strerror(res));

			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}

			if(check_file!=NULL)
			{
				fclose(check_file);
				check_file=NULL;
			}

			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}

			CstcLog_printf("UPDATEAPP::curl_easy_perform() failed:...............");
			return 0;
		}
		CstcLog_printf("UPDATEAPP::curl_easy_perform() successful:...............");

		if(curl!=NULL)
		{
			curl_easy_cleanup(curl);
			curl=NULL;
		}

		CstcLog_printf("UPDATEAPP::Creating a temp pkg file..");

		temp_pkg_file = fopen(UPDATED_PKG_FILE_ADDR,"wb");

		if (temp_pkg_file == NULL)
		{
			CstcLog_printf("UPDATEAPP::Could not open Dummy pkg file....");
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(check_file!=NULL)
			{
				fclose(check_file);
				check_file=NULL;
			}

			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}

			remove(ONLINE_UDATE_FILE_ADDR);  //remove the temp .txt file
			return 0;
		}

		CstcLog_printf("UPDATEAPP::Dummy pkg File opened successfully.....");

		curl = curl_easy_init();
		if(curl ==NULL)
		{
			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}
			if(check_file!=NULL)
			{
				fclose(check_file);
				check_file=NULL;
			}
			if(temp_pkg_file!=NULL)
			{
				fclose(temp_pkg_file);
				temp_pkg_file=NULL;
			}


			remove(ONLINE_UDATE_FILE_ADDR);  //remove the temp .txt file
			remove(UPDATED_PKG_FILE_ADDR);

			return 0;
		}

		CstcLog_printf("UPDATEAPP::setting url for downloading latest .pkg file...");

		sprintf(temp_url,"http://180.92.175.103/ota/%s",rbTemp);

		curl_easy_setopt(curl, CURLOPT_URL, temp_url);

		CstcLog_printf("UPDATEAPP::pkg download url is set : %s",temp_url);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);

		curl_easy_setopt(curl, CURLOPT_WRITEDATA,temp_pkg_file);

		CstcLog_printf("UPDATEAPP::before curl_easy_perform......pkg file");

		res = curl_easy_perform(curl);


		if(res != CURLE_OK)
		{
			CstcLog_printf("UPDATEAPP::curl_easy_perform() failed:...............");

			fprintf(stderr, "curl_easy_perform() failed: %s\n",curl_easy_strerror(res));

			if(bTemp!=NULL)
			{
				free(bTemp);
				bTemp=NULL;
			}

			if(check_file!=NULL)
			{
				fclose(check_file);
				check_file=NULL;
			}


			if(temp_pkg_file!=NULL)
			{
				fclose(temp_pkg_file);
				temp_pkg_file=NULL;
			}

			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}

			remove(ONLINE_UDATE_FILE_ADDR);  //remove the temp .txt file
			remove(UPDATED_PKG_FILE_ADDR);
			return 0;
		}

		CstcLog_printf("UPDATEAPP::curl_easy_perform() successful:...............");

		if(temp_pkg_file!=NULL)
		{
			fclose(temp_pkg_file);
			temp_pkg_file=NULL;
		}

		if(curl !=NULL)
		{
			curl_easy_cleanup(curl);
			curl=NULL;
		}
		CstcLog_printf("UPDATEAPP::curl cleaned up..");
	}
	else
	{
		CstcLog_printf("UPDATEAPP::version is upto date....!!!!");
		Show_Error_Msg("VERSION IS UPTO DATE");
		remove(ONLINE_UDATE_FILE_ADDR);  //remove the temp .txt file

		if(bTemp!=NULL)
		{
			free(bTemp);
			bTemp=NULL;
		}

		return 2;
	}


	if(bTemp!=NULL)
	{
		free(bTemp);
		bTemp=NULL;
	}
	CstcLog_printf("UPDATEAPP::lib curl cleaned up...");
	remove(ONLINE_UDATE_FILE_ADDR);  //remove the temp .txt file
	//	remove(UPDATED_PKG_FILE_ADDR);
	return 1;
}

size_t  write_data_curl(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	size_t written = size * nmemb;

	CstcLog_printf("url file size = %d Bytes",nmemb);
	if(stream == NULL)
	{
		CstcLog_printf("file do not exist ...return");
		return 0;
	}
	if(nmemb == 0)
	{
		CstcLog_printf("number of memory bytes are 0...return");
		return 0;
	}
	fwrite(ptr,size,nmemb,stream);
	CstcLog_printf("Data copied to Dummy file : %d Bytes",written);

	return written;
}


#if 0
int make_copy_of_upload_file(void)
{
	FILE *gprs_upload_file = NULL;
	FILE *gprs_upload_copy_file = NULL;
	char read_buffer[2001],bytes_readed = 0;
	CstcLog_printf("\n*********** IN make_copy_of_upload_file **************\n");
	bytes_readed = 0;

	while(upload_semaphore_ticket)
	{
		sleep(1);
	}
	upload_semaphore_ticket++;

	gprs_upload_file= fopen(GPRS_DATA_UPLOAD_FILE_ADDR,"r");
	gprs_upload_copy_file = fopen(GPRS_DATA_COPY_UPLOAD_FILE_ADDR,"w");

	if(gprs_upload_file != NULL)
	{
		while((!feof( gprs_upload_file )))
		{
			memset(read_buffer,0,2001);
			bytes_readed = fread(read_buffer,sizeof(char),2000,gprs_upload_file);

			if(bytes_readed)
			{
				fputs(read_buffer,gprs_upload_copy_file);
			}
		}

		fflush(gprs_upload_file);
		fclose(gprs_upload_file);

		fflush(gprs_upload_copy_file);
		fclose(gprs_upload_copy_file);

		/***delete contents***/
		gprs_upload_file= fopen(GPRS_DATA_UPLOAD_FILE_ADDR,"w");
		fflush(gprs_upload_file);
		fclose(gprs_upload_file);

		upload_semaphore_ticket--;

		puts("\nWriting Finished\n");
		return 1;
	}
	else
	{
		CstcLog_printf("File Open Error");
		puts("File Open Error");

	}

	return 0;


}


int get_chunk_size_in_bytes_and_fill(FILE *gprs_file_ptr,char *CHK_FILENAME,depot_struct *depot_struct_local_var)
{

	int line_count = 0,bytes_count = 0,j = 0,start_length = 0;
	int mode_length = 0,data_length = 0,misc_length = 0,device_id_length = 0,signal_num=0;
	//	char line_data[3000] = {0x00};
	//	char *replace_ptr;
	int calculated_length = 0;
	//	char replace_string[100];
	char ch = 0;
	char sn[10];
	char File_Type = 0 ;
	int initial_file_pointer = 0/*,final_file_pointer = 0*/;

	uint32_t tamper_status=-1;
	line_count = 0;
	bytes_count = 0;
	j = 0;
	//	i = 0 ;
	CstcLog_printf("THREAD::reading file\n");
	CstcLog_printf("READING FILE...");
	UART_get_machine_info(sn,10);
	sys_battery_info(&battery_info);
	ped_get_status(&tamper_status);
	wnet_signal(&signal_num);

	if(strcmp(CHK_FILENAME,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
	{
		File_Type = 3;
	}

	initial_file_pointer = ftell(gprs_file_ptr);

	while(ch != EOF && line_count < NO_OF_CHUNK_LINES)
	{
		ch = getc(gprs_file_ptr);
		j++;

		switch(File_Type)
		{
		case 1:if((j == sizeof(transaction_log_struct)) && (ch != '|'))
			return -2;

		case 2:if((j == sizeof(waybill_master_struct)) && (ch != '|'))
			return -2;

		case 3:if((j == sizeof(schedule_master_struct)) && (ch != '|'))
			return -2;

		}

		if(ch == '|')
		{

			calculated_length = j;
			//			data_length+= (calculated_length);
			//			printf("data_length=&%d  calculated length = %d",data_length,calculated_length);
			j = 0;
			line_count++;
		}

		bytes_count++;

	}
	if(ch!= EOF || calculated_length != 0)
	{
		//		data_to_be_send[start_length+i] = '\0';
		//		strcat(data_to_be_send,"&mode=1");
		//		replace_ptr = strstr(data_to_be_send,"Content-Length: ");
		//		memset(replace_string,0,100);
		//		sprintf(replace_string,"Content-Length: %d",data_length+mode_length+misc_length+device_id_length);
		//		memcpy(replace_ptr,replace_string,strlen(replace_string));
		//		dynamic_strcat(data_to_be_send,"&bstat=%d&tampstat=%d&gprssig=%d\r\n",battery_info.percent,tamper_status,signal_num);

		mode_length = strlen("&mode=")+1;
		misc_length = get_formatted_string_length("&bstat=%d&tampstat=%d&gprssig=%d",battery_info.percent,tamper_status,signal_num);
		device_id_length = get_formatted_string_length("deviceid=%s&data=",sn);
		//		start_length = get_formatted_string_length("POST /log.php HTTP/1.1\r\nHost: 223.30.102.30\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&data=",mode_length+bytes_count+misc_length+device_id_length,sn);
		start_length = get_formatted_string_length("POST /log.php HTTP/1.1\r\nHost: %s\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&data=",depot_struct_local_var->GPRS_IP,(mode_length+bytes_count+misc_length+device_id_length),sn);
		data_to_be_send = (char *)malloc((start_length+(bytes_count+1)+mode_length+misc_length/*+device_id_length*/+1) * sizeof(char));
		memset(data_to_be_send,0,start_length+(bytes_count+1)+mode_length+misc_length/*+device_id_length*/+1);
		//		dynamic_printf(data_to_be_send,"POST /log.php HTTP/1.1\r\nHost: 223.30.102.30\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&data=",(mode_length+bytes_count+misc_length+device_id_length),sn);
		dynamic_printf(data_to_be_send,"POST /log.php HTTP/1.1\r\nHost: %s\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\ndeviceid=%s&data=",depot_struct_local_var->GPRS_IP,(mode_length+bytes_count+misc_length+device_id_length),sn);
		fseek(gprs_file_ptr,initial_file_pointer,SEEK_SET);
		fgets(&data_to_be_send[start_length],(bytes_count+1),gprs_file_ptr);
		dynamic_strcat(data_to_be_send,"&mode=%d&bstat=%d&tampstat=%d&gprssig=%d\r\n",File_Type,battery_info.percent,tamper_status,signal_num);
		CstcLog_printf("THREAD::data_to_be_send");
		data_length= 0;
	}

	return ch;
}


int get_chunk_size_and_fill_curl(FILE *gprs_file_ptr,char *CHK_FILENAME)
{

	int line_count = 0,bytes_count = 0,j = 0,start_length = 0;
	int mode_length = 0,data_length = 0,misc_length = 0,device_id_length = 0,signal_num=0;
	//	char line_data[3000] = {0x00};
	//	char *replace_ptr;
	int calculated_length = 0;
	//	char replace_string[100];
	char ch = 0;
	char sn[10];
	char File_Type = 0 ;
	int initial_file_pointer = 0/*,final_file_pointer = 0*/;

	uint32_t tamper_status=-1;
	line_count = 0;
	bytes_count = 0;
	j = 0;
	//	i = 0 ;
	CstcLog_printf("THREAD::reading file\n");
	CstcLog_printf("READING FILE...");
	UART_get_machine_info(sn,10);
	sys_battery_info(&battery_info);
	ped_get_status(&tamper_status);
	wnet_signal(&signal_num);

	if(strcmp(CHK_FILENAME,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
	{
		File_Type = 3;
	}

	initial_file_pointer = ftell(gprs_file_ptr);

	while(ch != EOF && line_count < NO_OF_CHUNK_LINES)
	{
		ch = getc(gprs_file_ptr);
		j++;

		switch(File_Type)
		{
		case 1:if((j == sizeof(transaction_log_struct)) && (ch != '|'))
			return -2;

		case 2:if((j == sizeof(waybill_master_struct)) && (ch != '|'))
			return -2;

		case 3:if((j == sizeof(schedule_master_struct)) && (ch != '|'))
			return -2;

		}

		if(ch == '|')
		{

			calculated_length = j;
			//			data_length+= (calculated_length);
			//			printf("data_length=&%d  calculated length = %d",data_length,calculated_length);
			j = 0;
			line_count++;
		}

		bytes_count++;

	}
	if(ch!= EOF || calculated_length != 0)
	{

		mode_length = strlen("&mode=")+1;
		misc_length = get_formatted_string_length("&bstat=%d&tampstat=%d&gprssig=%d",battery_info.percent,tamper_status,signal_num);

		device_id_length = get_formatted_string_length("deviceid=%s&data=",sn);

		start_length = get_formatted_string_length("deviceid=%s&data=",sn);

		data_to_be_send = (char *)malloc((start_length+(bytes_count+1)+mode_length+misc_length/*+device_id_length*/+1) * sizeof(char));
		memset(data_to_be_send,0,start_length+(bytes_count+1)+mode_length+misc_length/*+device_id_length*/+1);

		dynamic_printf(data_to_be_send,"deviceid=%s&data=",sn);

		fseek(gprs_file_ptr,initial_file_pointer,SEEK_SET);

		fgets(&data_to_be_send[start_length],(bytes_count+1),gprs_file_ptr);

		dynamic_strcat(data_to_be_send,"&mode=%d&bstat=%d&tampstat=%d&gprssig=%d\r\n",File_Type,battery_info.percent,tamper_status,signal_num);
		CstcLog_printf("THREAD::data_to_be_send");

		data_length= 0;
	}

	return ch;
}


int GPRS_set_chunk_ppp_connection(void)
{
	int retval = 0;
	retval = 0;//lcd_printf(ALG_LEFT,16, "set attached...");	lcd_flip();

	CstcLog_printf("THREAD::Gprs_Set_PPP_Connection...");

	//	pthread_mutex_lock(&printing_mutex);
	retval = Gprs_ISP();
	//	pthread_mutex_unlock(&printing_mutex);

	CstcLog_printf("THREAD::Gprs_ISP retval = %d", retval);

	if (0 != retval)
	{
		CstcLog_printf("THREAD::wnet_set_attached() failed %d", retval);
		return 0;

	}
	if (0 == retval)
	{
		CstcLog_printf("THREAD::PPP logon...");

		retval = Gprs_Open_PPP_Connection();

		CstcLog_printf("THREAD::Gprs_Open_PPP_Connection retval = %d", retval);


		if (0 != retval)
		{
			CstcLog_printf("THREAD::PPP open failed %d", retval);
			Gprs_Close_PPP_Connection();
			return 0;
		}
		else
		{
			CstcLog_printf("THREAD::PPP check...");

			//pthread_mutex_lock(&printing_mutex);
			retval = Gprs_Check_PPP_Connection();
			//pthread_mutex_unlock(&printing_mutex);
			CstcLog_printf("THREAD::Gprs_Check_PPP_Connection retval = %d", retval);

			if (0 == retval)
			{
				CstcLog_printf("THREAD::PPP logon Ok");

				//				all_flags_struct_var.ppp_on_flag = 1;
				return 1;
			}
			else
			{
				Gprs_Close_PPP_Connection();
				return 0;
			}
		}
	}
	else
	{
		return 0;
	}
	return 0;
}
int send_curl_data_chunkwise(char *p_Filename)
{
	CURL *curl;
	CURLcode res;
	int chunk_counter = 0,retry_count = 0,/*ret=0,*/val=-1;
	char curl_url_string[31]={'\0'};
	long int current_chunk_pointer = 0; //Vaibhav added Change
	char response_read[20] = {'\0'};
	//	unsigned int response_bytes;
	char exit_flag = 0,start_flag =0 ,data_send_incomplete = 0;
	FILE *gprs_file_ptr = NULL;
	FILE * check_file = NULL;
	int return_value = 0;
	depot_struct depot_struct_local_var;

	//	if(strcmp(p_Filename,GPRS_DATA_UPLOAD_WAYBILL_FILE_ADDR) == 0)
	//	{
	//		pthread_mutex_lock(&upload_semaphore_waybill);
	//	}
	//	else if(strcmp(p_Filename,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
	//	{
	//		pthread_mutex_lock(&upload_semaphore_schedule);
	//	}

	gprs_file_ptr = fopen(p_Filename,"r");
	exit_flag = 0;

	if(gprs_file_ptr == NULL)
	{
		CstcLog_printf("CURL::\nFile Open Error\n");
		return 0;
	}

	chunk_counter = 0;
	start_flag =0 ;
	data_send_incomplete = 0;
	if(!get_depot_details(&depot_struct_local_var))
	{
		CstcLog_printf("CURL::Failed to get depot details");
		if(waybill_master_struct_var.GPRS_IP!=NULL)
		{
			if(0<strlen(waybill_master_struct_var.GPRS_IP))
			{
				memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
				memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
				depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
				CstcLog_printf("CURL::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
			}
			else
			{
				CstcLog_printf("CURL::GPRS IP has 0 length in Waybill");
				memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
			}
		}
		else
		{
			CstcLog_printf("CURL::GPRS IP is NULL in Waybill");
			memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
		}
	}

	do
	{
		// ARRANGE THE BUFFER          //
		current_chunk_pointer = ftell(gprs_file_ptr);//Vaibhav added Change
		if((return_value = get_chunk_size_and_fill_curl(gprs_file_ptr,p_Filename)) != EOF)
		{
			if(return_value == -2)
			{
				return -2;
			}
			CstcLog_printf("CURL::Readed\n");

		}
		else
		{
			CstcLog_printf("CURL::EOF OCCURED\n");
			exit_flag = 1;
		}

		retry_count = 0;
		while((retry_count < 3) && (data_to_be_send != NULL))
		{
			CstcLog_printf("CURL::retries = %d\n",retry_count);
			if(start_flag == 0)
			{
				if(Gprs_Interface_Init())
				{
					CstcLog_printf("GPRS initializes Success");
					CstcLog_printf("CURL::GPRS initializes Success\n");

					if(GPRS_set_chunk_ppp_connection())
					{
						CstcLog_printf("YUP..GPRS Connection is established..");
						CstcLog_printf("CURL::YUP..GPRS Connection is established..\n");
					}
					else
					{
						CstcLog_printf("CURL::GPRS ppp failed....\n");
						start_flag = 0;
						retry_count++;
						Gprs_Interface_Deinit();
						continue;
					}
				}
				else
				{
					CstcLog_printf("... GPRS init Failure");
					start_flag = 0;
					retry_count++;
					Gprs_Interface_Deinit();
					continue;
				}

			}

			/**********curl initializing******/

			curl = curl_easy_init();
			if(curl==NULL)
			{
				CstcLog_printf("CURL::Failed to initialise curl");

				retry_count++;
				Gprs_Interface_Deinit();
				CstcLog_printf("CURL::RETRYING 2");
				continue;
			}
			CstcLog_printf("CURL::Initialised curl successfully");

			sprintf(curl_url_string,"http://%s/log.php",depot_struct_local_var.GPRS_IP);

			CstcLog_printf("CURL::CONNECTING TO %s", curl_url_string);

			curl_easy_setopt(curl, CURLOPT_URL, curl_url_string);

			start_flag = 1;
			CstcLog_printf("CURL::Sending Data...");

			/*****************curl post***************/
			CstcLog_printf("CURL::Posting to server...");
			CstcLog_printf("CURL::String %s StringLen :%d",data_to_be_send,strlen(data_to_be_send));

			val= curl_easy_setopt(curl, CURLOPT_POSTFIELDS,data_to_be_send);
			CstcLog_printf("CURL::Posting finished...returned val = %d",val);

			/*******************check response********************/

			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
			if (check_file == NULL)
			{
				CstcLog_printf("Could not open temp .txt file....");
				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;

				}


				retry_count++;
				Gprs_Interface_Deinit();
				CstcLog_printf("CURL::RETRYING 3");
				continue;
			}

			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,write_data_curl);

			curl_easy_setopt(curl, CURLOPT_WRITEDATA,check_file);

			res = curl_easy_perform(curl);

			if(res != CURLE_OK)
			{
				fprintf(stderr, "\nCURL::curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;

				}


				CstcLog_printf("CURL::curl_easy_perform() failed.....");
				retry_count++;
				CstcLog_printf("CURL::RETRYING 4");
				Gprs_Interface_Deinit();
				continue;
			}

			CstcLog_printf("CURL::curl_easy_perform() succeeded");
			fflush(check_file);
			fclose(check_file);


			/*****************************************/

			/* Read data */

			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"r");

			fread(response_read, get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR), 1, check_file);

			fflush(check_file);
			fclose(check_file);

			CstcLog_printf("CURL::response_read = %s", response_read);

			if(strstr(response_read,"DATA RECEIVED"))
			{
				CstcLog_printf("CURL::DATA RECEIVED");
				Gprs_Interface_Deinit();
			}
			else
			{
				CstcLog_printf("CURL::Fail To match");
				memset(response_read,0,20);
				if(curl !=NULL)
				{
					curl_easy_cleanup(curl);
					curl=NULL;
				}

				retry_count++;
				CstcLog_printf("CURL::RETRYING 5");
				Gprs_Interface_Deinit();
				continue;
			}

			CstcLog_printf("CURL::curl_easy_perform() successful....");

			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}


			CstcLog_printf("CURL::cleanup");

			/****delete contents***/
			check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");
			fflush(check_file);
			fclose(check_file);
			check_file = NULL;
			/*********************/

			if(data_to_be_send != NULL)
			{
				data_to_be_send = NULL;
				free(data_to_be_send);
			}
			break;
		}
		chunk_counter++;
		if(retry_count >= 3)
		{

			data_send_incomplete  = 1;
			CstcLog_printf("CURL::\nFailsafe started....\n");
			//			while(upload_semaphore_ticket)
			//			{
			//				sleep(1);
			//			}
			//			upload_semaphore_ticket++;
			fseek(gprs_file_ptr,current_chunk_pointer,SEEK_SET);//Vaibhav added Change
			write_to_failsafe_file(gprs_file_ptr,p_Filename);
			if(gprs_file_ptr != NULL)
			{

				fclose(gprs_file_ptr);
				gprs_file_ptr = NULL;

			}
			CstcLog_printf("Restoring...");
			CstcLog_printf("CURL::\nRestoring....\n");
			restore_failed_data_to_file(p_Filename);
			//			upload_semaphore_ticket--;
			CstcLog_printf("CURL::\nRestored....\n");
			exit_flag = 1;
		}

		if(exit_flag == 0)
			CstcLog_printf("CURL::Chunk Sent: %d",chunk_counter);
		else
			CstcLog_printf("CURL::Chunk failed: %d",chunk_counter);

	}
	while(!exit_flag);

	if(data_to_be_send != NULL)
	{
		free(data_to_be_send);
	}

	if(gprs_file_ptr != NULL)
	{
		fclose(gprs_file_ptr);
		gprs_file_ptr = NULL;
		//
		//		if(strcmp(p_Filename,GPRS_DATA_UPLOAD_WAYBILL_FILE_ADDR) == 0)
		//		{
		//			pthread_mutex_unlock(&upload_semaphore_waybill);
		//		}
		//		else if(strcmp(p_Filename,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
		//		{
		//			pthread_mutex_unlock(&upload_semaphore_schedule);
		//		}
	}

	if(check_file != NULL)
	{
		fclose(check_file);
		check_file = NULL;
	}

	if(data_send_incomplete == 0)
	{
		//		while(upload_semaphore_ticket)
		//		{
		//			sleep(1);
		//		}
		//		upload_semaphore_ticket++;
		//		if(strcmp(p_Filename,GPRS_DATA_UPLOAD_WAYBILL_FILE_ADDR) == 0)
		//		{
		//			pthread_mutex_lock(&upload_semaphore_waybill);
		//		}
		//		else if(strcmp(p_Filename,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
		//		{
		//			pthread_mutex_lock(&upload_semaphore_schedule);
		//		}



		gprs_file_ptr = fopen(p_Filename,"w");
		fflush(gprs_file_ptr);
		fclose(gprs_file_ptr);
		gprs_file_ptr = NULL;

		if(strcmp(p_Filename,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
			gprs_file_ptr = fopen(GPRS_SCHEDULE_FAILSAFE_FILE_ADDR,"w");
		fflush(gprs_file_ptr);
		fclose(gprs_file_ptr);

		//		upload_semaphore_ticket--;
		//		if(strcmp(p_Filename,GPRS_DATA_UPLOAD_WAYBILL_FILE_ADDR) == 0)
		//		{
		//			pthread_mutex_unlock(&upload_semaphore_waybill);
		//		}
		//		else if(strcmp(p_Filename,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
		//		{
		//			pthread_mutex_unlock(&upload_semaphore_schedule);
		//		}

		return 1;
	}
	else
	{
		return 0;
	}


}

int send_gprs_data_chunkwise(char *p_Filename)
{
	int iclient_socket = -1,chunk_counter = 0,retry_count = 0;
	long int current_chunk_pointer = 0; //Vaibhav added Change
	char response_read[20] = {'\0'};
	unsigned int response_bytes;
	char exit_flag = 0,start_flag =0 ,data_send_incomplete = 0;
	FILE *gprs_file_ptr = NULL;
	int return_value = 0;
	depot_struct depot_struct_local_var;
	gprs_file_ptr = fopen(p_Filename,"r");
	exit_flag = 0;
	if(gprs_file_ptr == NULL)
		//		CstcLog_printf("THREAD::\nFILE IS OPENED\n");
		//	else
	{
		CstcLog_printf("THREAD::\nFile Open Error\n");
		return 0;
	}

	chunk_counter = 0;
	start_flag =0 ;
	data_send_incomplete = 0;
	if(!get_depot_details(&depot_struct_local_var))
	{
		CstcLog_printf("THREAD::Failed to get depot details");
		if(waybill_master_struct_var.GPRS_IP!=NULL)
		{
			if(0<strlen(waybill_master_struct_var.GPRS_IP))
			{
				memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
				memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
				depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
				CstcLog_printf("THREAD::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
			}
			else
			{
				CstcLog_printf("THREAD::GPRS IP has 0 length in Waybill");
				memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
			}
		}
		else
		{
			CstcLog_printf("THREAD::GPRS IP is NULL in Waybill");
			memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
		}
	}

	do
	{
		// ARRANGE THE BUFFER          //
		current_chunk_pointer = ftell(gprs_file_ptr);//Vaibhav added Change
		if((return_value = get_chunk_size_in_bytes_and_fill(gprs_file_ptr,p_Filename,&depot_struct_local_var)) != EOF)
		{

			if(return_value == -2)
			{
				return -2;
			}
			CstcLog_printf("THREAD::Readed\n");

		}
		else
		{
			CstcLog_printf("THREAD::EOF OCCURED\n");
			exit_flag = 1;
		}

		//		//    Initialize GPRS and All th sh....t
		retry_count = 0;           // should be 0, but it is done 3 for testing
		while((retry_count < 3) && (data_to_be_send != NULL))
		{
			CstcLog_printf("THREAD::retries = %d\n",retry_count);
			if(start_flag == 0)
			{
				//Swap check if GPRS is working outside the thread if yes add wait here also check UART
				//				while((all_flags_struct_var.online_schedule_update_flag||all_flags_struct_var.uart_comm_flag)==1){
				//					sleep(5);
				//				} //swap added to avoid gprs while downloading ticket data through UART.
				if(Gprs_Interface_Init())
				{
					CstcLog_printf("GPRS initializes Success");
					CstcLog_printf("THREAD::GPRS initializes Success\n");
					//					if(Gprs_Set_PPP_Connection())
					//					if(Gprs_Set_PPP_Connection(0))
					if(GPRS_set_chunk_ppp_connection())
					{
						CstcLog_printf("YUP..GPRS Connection is established..");
						CstcLog_printf("THREAD::YUP..GPRS Connection is established..\n");
					}
					else
					{
						CstcLog_printf("GPRS Gonna....");
						CstcLog_printf("THREAD::GPRS Gonna....\n");
						start_flag = 0;
						retry_count++;
						Gprs_Interface_Deinit();
						continue;
					}


				}
				else
				{
					CstcLog_printf("Ohh   MYGOD... GPRS Failure");
					CstcLog_printf("THREAD::Ohh   MYGOD... GPRS Failure\n");
					start_flag = 0;
					retry_count++;
					Gprs_Interface_Deinit();
					continue;
				}





			}


			iclient_socket = tcp_connect(depot_struct_local_var.GPRS_IP, SERVER_PORT);
			if (iclient_socket < 0)
			{
				CstcLog_printf("\nConnect failed.");
				CstcLog_printf("THREAD::\nConnect failed.");
				CstcLog_printf("THREAD::\n");
				start_flag = 0;
				retry_count++;
				tcp_close(iclient_socket);
				Gprs_Interface_Deinit();
				continue;
			}
			CstcLog_printf("THREAD::Connection Done...");
			CstcLog_printf("Connect DONE.");//Vaibhav added Change
			start_flag = 1;
			//else
			{
				//       Now Send The Data     //

				CstcLog_printf("THREAD::Sending Data...");
				CstcLog_printf("data_to_be_send: %s ",data_to_be_send);//Vaibhav added Change
				if (tcp_write(iclient_socket , data_to_be_send, strlen(data_to_be_send)) < 0)
				{
					CstcLog_printf("write connection was broken");
					CstcLog_printf("THREAD::write connection was broken\n");
					CstcLog_printf("THREAD::\n");
					retry_count++;
					tcp_close(iclient_socket);
					Gprs_Interface_Deinit();
					continue;



				}
				else
				{

					usleep(700000);
					CstcLog_printf("THREAD::Waiting For response...\n");
					CstcLog_printf("Waiting For response...");//Vaibhav added Change
					response_bytes = 319;//sizeof(response_read);
					memset(response_read,0,sizeof(response_read));
					if (tcp_read(iclient_socket, response_read,&response_bytes ) < 0)
					{

						CstcLog_printf("THREAD::response read Error\n");
						CstcLog_printf("THREAD::\n");
						retry_count++;
						tcp_close(iclient_socket);
						Gprs_Interface_Deinit();
						continue;

					}
					else
					{
						CstcLog_printf("THREAD::Response Bytes = %d\n",response_bytes);
						if(strstr(response_read,"DATA RECEIVED") != NULL)
						{
							CstcLog_printf("THREAD::response_read");
							CstcLog_printf("THREAD::\n");
							CstcLog_printf("THREAD::Chunk Sent Successfully...\n");
							CstcLog_printf("Chunk Sent Successfully...");//Vaibhav added Change
							tcp_close(iclient_socket);
							Gprs_Interface_Deinit();
						}
						else
						{
							CstcLog_printf("THREAD::response_read");
							CstcLog_printf(response_read);//Vaibhav added Change
							CstcLog_printf("THREAD::\n");
							retry_count++;
							tcp_close(iclient_socket);
							Gprs_Interface_Deinit();
							continue;
						}
					}


					if(data_to_be_send != NULL)
					{
						data_to_be_send = NULL;
						free(data_to_be_send);
					}
					break;
				}

			}
		}
		chunk_counter++;
		if(retry_count >= 3)
		{

			data_send_incomplete  = 1;
			CstcLog_printf("THREAD::\nFailsafe started....\n");
			while(upload_semaphore_ticket)
			{
				sleep(1);
			}
			upload_semaphore_ticket++;
			fseek(gprs_file_ptr,current_chunk_pointer,SEEK_SET);//Vaibhav added Change
			write_to_failsafe_file(gprs_file_ptr,p_Filename);
			if(gprs_file_ptr != NULL)
			{
				fclose(gprs_file_ptr);
				gprs_file_ptr = NULL;
			}
			CstcLog_printf("Restoring...");
			CstcLog_printf("THREAD::\nRestoring....\n");
			restore_failed_data_to_file(p_Filename);
			upload_semaphore_ticket--;
			CstcLog_printf("THREAD::\nRestored....\n");
			exit_flag = 1;
		}

		if(exit_flag == 0)
			CstcLog_printf("THREAD::Chunk Sent: %d",chunk_counter);
		else
			CstcLog_printf("THREAD::Chunk failed: %d",chunk_counter);

	}
	while(!exit_flag);

	tcp_close(iclient_socket);
	Gprs_Interface_Deinit();

	if(data_to_be_send != NULL)
	{
		free(data_to_be_send);
	}

	if(gprs_file_ptr != NULL)
	{
		fclose(gprs_file_ptr);
		gprs_file_ptr = NULL;
	}
	if(data_send_incomplete == 0)
	{
		while(upload_semaphore_ticket)
		{
			sleep(1);
		}
		upload_semaphore_ticket++;
		gprs_file_ptr = fopen(p_Filename,"w");
		fflush(gprs_file_ptr);
		fclose(gprs_file_ptr);
		gprs_file_ptr = NULL;

		if(strcmp(p_Filename,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
			gprs_file_ptr = fopen(GPRS_SCHEDULE_FAILSAFE_FILE_ADDR,"w");
		fflush(gprs_file_ptr);
		fclose(gprs_file_ptr);

		upload_semaphore_ticket--;
		return 1;
	}
	else
	{
		return 0;
	}


}


int restore_failed_data_to_file(char *P_FILENAME)
{
	FILE *updated_failsafe_fp = NULL,*failsafe_fp = NULL;
	char read_buffer[2001] = {'\0'};
	int bytes_readed = 0;

	if(strcmp(P_FILENAME,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
		failsafe_fp = fopen(GPRS_SCHEDULE_FAILSAFE_FILE_ADDR,"r");

	updated_failsafe_fp = fopen(P_FILENAME,"w");
	if(failsafe_fp == NULL)
	{
		return 0;
	}
	if(updated_failsafe_fp == NULL)
	{
		return 0;
	}

	while((!feof( failsafe_fp )))
	{
		memset(read_buffer,0,2001);
		bytes_readed = fread(read_buffer,sizeof(char),2000,failsafe_fp);

		if(bytes_readed)
		{
			fputs(read_buffer,updated_failsafe_fp);
		}

	}
	fflush(updated_failsafe_fp);
	fflush(failsafe_fp);
	fclose(updated_failsafe_fp);
	fclose(failsafe_fp);
	//	if(strcmp(P_FILENAME,GPRS_DATA_UPLOAD_WAYBILL_FILE_ADDR) == 0)
	//	{
	//		pthread_mutex_unlock(&upload_semaphore_waybill);
	//	}
	//	else if(strcmp(P_FILENAME,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
	//	{
	//		pthread_mutex_unlock(&upload_semaphore_schedule);
	//	}
	return 1;
}

int write_to_failsafe_file(FILE *failed_fp,char *P_FILENAME)
{
	FILE *failsafe_fp = NULL;
	char read_buffer[2001],bytes_readed = 0;
	CstcLog_printf("\n*********** IN write_to_failsafe_file **************\n");
	bytes_readed = 0;

	if(strcmp(P_FILENAME,GPRS_DATA_UPLOAD_SCHEDULE_FILE_ADDR) == 0)
		failsafe_fp = fopen(GPRS_SCHEDULE_FAILSAFE_FILE_ADDR,"w");

	if(failsafe_fp != NULL)
	{
		while((!feof( failed_fp )))
		{
			memset(read_buffer,0,2001);
			bytes_readed = fread(read_buffer,sizeof(char),2000,failed_fp);

			if(bytes_readed)
			{
				fputs(read_buffer,failsafe_fp);
			}

		}
		fflush(failsafe_fp);
		fclose(failsafe_fp);

		puts("\nWriting Finished\n");
		return 1;
	}
	else
	{
		CstcLog_printf("File Open Error");
		puts("File Open Error");

	}
	fclose(failsafe_fp);
	return 0;

}
#endif
/********************************************************************************/


int curl_online_schedule_update(int update_type,char *id_string,int index)
{

	CURL *curl;
	CURLcode res;
	FILE *check_file =  NULL;
	char curl_url_string[50]={'\0'};
	int temp_test=-1;
	int response_file_data_len=0,val=0,bytes_read=0;


	int iclient_socket = -1;
	char bTemp[1024]={'\0'};
	char rbTemp[20000]={'\0'};
	unsigned int  iTemp=0;
	int j = 0,hd_len=0;
	char *sn = NULL;
	int nbytes = 10;
	int header_flag = 0;//,value_in_itemp = 0;
	int updated_flag = 0;
//	int content_lengths = 0;
	int key=0, ret =0;
	depot_struct depot_struct_local_var;
	char tempUpdateDate[(DATE_LEN+1)];
	char tempUpdateTime[(TIME_LEN+1)]={'\0'};

	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);

	CstcLog_printf("OTA::\n\n***************** SOCKET CLIENT **************************\n\n");
	CstcLog_printf("OTA::size of rbTemp = %d",sizeof(rbTemp));

	get_schedule_type_info();
	get_waybill_details();
	if(!get_depot_details(&depot_struct_local_var))
	{
		CstcLog_printf("OTA::Failed to get depot details");
		if(waybill_master_struct_var.GPRS_IP!=NULL)
		{
			if(0<strlen(waybill_master_struct_var.GPRS_IP))
			{
				memset(depot_struct_local_var.GPRS_IP,0,(SERVER_IP_LEN+1));
				memcpy(depot_struct_local_var.GPRS_IP,waybill_master_struct_var.GPRS_IP,strlen(waybill_master_struct_var.GPRS_IP));
				depot_struct_local_var.GPRS_IP[strlen(waybill_master_struct_var.GPRS_IP)]='\0';
				CstcLog_printf("OTA::Copied GPRS IP from Waybill (%s) to depot (%s)",waybill_master_struct_var.GPRS_IP,depot_struct_local_var.GPRS_IP);
			}
			else
			{
				CstcLog_printf("OTA::GPRS IP has 0 length in Waybill");
				memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
				depot_struct_local_var.GPRS_IP[7]='\0';
				display_popup("Err: Server IP\n Not Found");
				tcp_close(iclient_socket);
				if(sn!=NULL)
				{
					free(sn);
					sn =NULL;
				}
				return 0;
			}
		}
		else
		{
			CstcLog_printf("OTA::GPRS IP is NULL in Waybill");
			memcpy(depot_struct_local_var.GPRS_IP,"0.0.0.0",7);
			display_popup("Err: Server IP\n Not Found");
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			return 0;
		}
	}




/*	iclient_socket = tcp_connect(depot_struct_local_var.GPRS_IP, SERVER_PORT);
	if (iclient_socket < 0)
	{
		display_popup("Err: Could Not Connect\n To Server");
		CstcLog_printf("OTA::\nConnect failed.");
		tcp_close(iclient_socket);
		if(sn!=NULL)
		{
			free(sn);
			sn =NULL;
		}
		return 0;
	}*/
//	else
	{

		//		display_popup("Connected To Server");
		UART_get_machine_info(sn,nbytes);
		CstcLog_printf("OTA::sn = %s",sn);
		CstcLog_printf("OTA::nbytes = %d",nbytes);


		ret = getUpdateDateTime(tempUpdateDate,tempUpdateTime,update_type,duty_master_struct_var.schedule_id,id_string); //error : ret=0
		CstcLog_printf("OTA::Update Date time present : %s %s    ret=%d",tempUpdateDate,tempUpdateTime,ret);


		switch(update_type)
		{
		case 0:
			CstcLog_printf("OTA::Case 0 : schedule_master update...");
			if(ret)
			{
				if((index >= 0) )//&& all_flags_struct_var.online_schedule_update_flag )
					//if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
//					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&scheduleid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->schedule_id,tempUpdateDate,tempUpdateTime,(full_duty_master_struct_var+index)->schedule_id,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
//					sprintf(bTemp,"POST /wget.php HTTP/1.1Host: 223.30.102.30Content-Type: application/x-www-form-urlencodedContent-Length: %ddeviceid=%s&depotid=%s&scheduleid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/content_lengths,sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->schedule_id,tempUpdateDate,tempUpdateTime,(full_duty_master_struct_var+index)->schedule_id,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);

					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
//					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
//					sprintf(bTemp,"POST /wget.php HTTP/1.1Host: %sContent-Type: application/x-www-form-urlencodedContent-Length: %ddeviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/depot_struct_local_var.GPRS_IP,content_lengths,sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);

					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}
			else
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//	if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=SCH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen(duty_master_struct_var.schedule_id)+nbytes+67+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}

			break;
		case 1:
			CstcLog_printf("OTA::Case 1 : route_header update...");

			if(ret)
			{
				if((index >= 0) )//&& all_flags_struct_var.online_schedule_update_flag )
					//				if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);

//					sprintf(bTemp,"deviceid=82245758&depotid=42&scheduleid=73&timestamp=18-06-2015 19:19:53&type=ROUTEH&data=5661&waybillid=T1115061800004&dutytimestamp=18-06-2015 19:19:53");

				}
			}
			else
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//				if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTEH&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}
			break;
		case 2:
			CstcLog_printf("OTA::Case 2 : route_master update...");

			if(ret)
			{
				if((index >= 0) )//&& all_flags_struct_var.online_schedule_update_flag )
					//	if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}
			else
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//	if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=ROUTED&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}

			break;
		case 3:
			CstcLog_printf("OTA::Case 3 : rate_master update...");

			if(ret)
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//				if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}
			else
			{
				if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
					//	if(all_flags_struct_var.tc_online_schedule_update_flag)
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
				else
				{
					// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
					sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=RATEM&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
				}
			}

			break;




		case 4:  // fare_chart - added.  //check it later.
			CstcLog_printf("OTA::Case 4 : fare_chart update...");

					if(ret)
					{
						if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
							//				if(all_flags_struct_var.tc_online_schedule_update_flag)
						{
							// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
							sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
						}
						else
						{
							// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
							sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,tempUpdateDate,tempUpdateTime,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
						}
					}
					else
					{
						if((index >= 0))// && all_flags_struct_var.online_schedule_update_flag )
							//	if(all_flags_struct_var.tc_online_schedule_update_flag)
						{
							// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
							sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+70+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,(full_duty_master_struct_var+index)->updated_date,(full_duty_master_struct_var+index)->updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
						}
						else
						{
							// content_lengths = get_formatted_string_length("deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
							sprintf(bTemp,"deviceid=%s&depotid=%s&timestamp=%s %s&type=FAREC&data=%s&waybillid=%s&dutytimestamp=%s %s",/*(strlen("&data=")+strlen(id_string)+strlen(duty_master_struct_var.schedule_id)+nbytes+69+(strlen(waybill_master_struct_var.depot_id)))*/sn,waybill_master_struct_var.depot_id,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time,id_string,waybill_master_struct_var.waybill_no,duty_master_struct_var.updated_date,duty_master_struct_var.updated_time);
						}
					}

					break;

		}

//		strcpy(bTemp,"deviceid=82245757&depotid=42&timestamp=09-10-2014 12:04:20&type=ROUTEH&data=30&waybillid=4555&dutytimestamp=23-10-2015 12:36:18");


		iTemp = strlen((char*)bTemp);

		CstcLog_printf("OTA::iTemp = %d",iTemp);
		CstcLog_printf("OTA::data string to be posted on server for update check(bTemp)= %s",bTemp);
		//CstcLog_HexDump_1(bTemp,(iTemp));



		/**********curl initializing******/

		curl = curl_easy_init();
		if(curl==NULL)
		{
			CstcLog_printf("CURL::Failed to initialise curl");

			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;
		}

		CstcLog_printf("CURL::Initialised curl successfully");

		/*****************set the url of server to be connected with  ***************/

		sprintf(curl_url_string,"http://%s:8080/its/wget.action",depot_struct_local_var.GPRS_IP);

		CstcLog_printf("CURL::CONNECTING TO SERVER %s", curl_url_string);

		curl_easy_setopt(curl, CURLOPT_URL, curl_url_string);

		CstcLog_printf("CURL::Sending Data...");

		/*****************send the check string to server ***************/

		CstcLog_printf("CURL::String %s StringLen :%d",bTemp,strlen(bTemp));

		val= curl_easy_setopt(curl, CURLOPT_POSTFIELDS,bTemp);
		if(val!= CURLE_OK)
		{
			CstcLog_printf("Error: CURL::Posting data failed...");
			if(curl!=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			if(sn!=NULL)
			{
				free(sn);
				sn = NULL;
			}
			return 0;
		}
		CstcLog_printf("CURL::data sent to server successfully...");



/*		if (tcp_write(iclient_socket , bTemp, iTemp) < 0)
		{
			display_popup("Err: Server Connection\n was broken");
			CstcLog_printf("OTA::write connection was broken");
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			return 0;
		}*/
		if(val!=0)
		{

			CstcLog_printf("CURL:: sending data to server failed..");
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}

			if(sn!=NULL)
			{
				free(sn);
				sn=NULL;
			}
			return 0;

		}
		else
		{
			display_popup("Request Sent To Server");
			CstcLog_printf("OTA::write successful....");
			iTemp = sizeof(rbTemp);
			header_flag = 0;
			//			puts("\n");
			do
			{

				memset(rbTemp,0,sizeof(rbTemp));
				iTemp = 6000;
				//value_in_itemp = OTA_CHUNK_BYTES;
				if(header_flag ==1)
				{
					hd_len = 0;
					//					usleep(10000);
				}
				else
				{
					usleep(700000);
				}
				/*******************get the respose string from a server and save it in a .txt file ********************/

				check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"w");

				CstcLog_printf("Storing the received data from server to RESPONSE_CHECK_FILE_ADDR.txt file....");

//				temp_test = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,write_data_curl);
				temp_test = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);

				if(temp_test != 0)  //Curl returns 0 on success
				{
					CstcLog_printf("CURL::storing response from server in txt file failed... ",temp_test);
					if(curl !=NULL)
					{
						curl_easy_cleanup(curl);
						curl=NULL;
					}

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					return 0;
				}

				temp_test = -1;
				temp_test = curl_easy_setopt(curl, CURLOPT_WRITEDATA,check_file);
				if(temp_test != 0)  //Curl returns 0 on success
				{
					CstcLog_printf("THREAD:::CURLOPT_WRITEFUNCTION temp_test in Fail %d",temp_test);
					if(curl !=NULL)
					{
						curl_easy_cleanup(curl);
						curl=NULL;
					}

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					return 0;
				}



				//		if(signal_num < 20) // abort download in case of poor signal(i.e < 20%).
				//		{
				//			CstcLog_printf("poor signal....2");
				//			if(curl !=NULL)
				//			{
				//				curl_easy_cleanup(curl);
				//				curl=NULL;
				//			}
				//			if(bTemp!=NULL)
				//			{
				//				free(bTemp);
				//				bTemp=NULL;
				//			}
				//			if(sn!=NULL)
				//			{
				//				free(sn);
				//				sn=NULL;
				//			}
				//			return 0;
				//		}

				res = curl_easy_perform(curl);

				if(res != CURLE_OK)
				{
					fprintf(stderr, "\nCURL::curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
					if(curl !=NULL)
					{
						curl_easy_cleanup(curl);
						curl=NULL;
					}

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					return 0;
				}

				CstcLog_printf("CURL::curl_easy_perform() succeeded");

				//		fflush(check_file);
				if(check_file!=NULL)
				{
				fclose(check_file);
				}
				CstcLog_printf("Divya reading file .....waittttttttt...");

  /*            if(update_type==3)
				sleep(100);*/


				/************************ Read .txt file data (i.e. data received from the server strored in txt file) ************************/

				check_file = fopen(RESPONSE_CHECK_FILE_ADDR,"r");
				if(check_file==NULL)
				{
					CstcLog_printf("OTA: Error - file opening failed");
					if(curl !=NULL)
					{
						curl_easy_cleanup(curl);
						curl=NULL;
					}

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					return 0;
				}
				//fread(rbTemp, get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR), 1, check_file);


				CstcLog_printf("OTA: file opened successfully for reading ....");
				response_file_data_len = get_file_size_gprs(RESPONSE_CHECK_FILE_ADDR);

				CstcLog_printf("CURL::response_file_data_len = %d", response_file_data_len);

				/*************** copy server respose data into buffer(rbTemp)*********/



				// read data in parts


				if((response_file_data_len > 0)/* && (response_file_data_len < lAST_READ_COUNT)*/)
				{
					bytes_read=fread(rbTemp, (response_file_data_len+1), 1, check_file);
					CstcLog_printf("CURL::bytes readed from server response file = %d", bytes_read);
				}

				if(check_file!=NULL)
				{
					fflush(check_file);
					fclose(check_file);
					check_file=NULL;
				}
				CstcLog_printf("CURL::Data Copied to temp buffer ......wow");

				iTemp=strlen(rbTemp);
				CstcLog_printf("CURL::rbTemp = %s len=%d", rbTemp,iTemp);

				/*			if (tcp_read(iclient_socket, rbTemp, &iTemp) < 0)
				{

					display_popup("Err: Server Connection\n was broken");
					CstcLog_printf("OTA::read connection was broken");
					updated_flag = 0;
					break;

				}
				else*/
				if (iTemp > 0)
				{

/*					if(iTemp<6000)
						rbTemp[iTemp]='\0';
					else
						rbTemp[5999]='\0';*/

					CstcLog_printf("OTA::ITEMP AFTER READING = %d",iTemp);
/*					if(header_flag == 0)
					{
						hd_len = get_http_header_len(rbTemp);
						CstcLog_HexDump_1(&rbTemp[hd_len],(iTemp-hd_len));
						CstcLog_printf("OTA::HEADER LEN = %d " ,hd_len);
						//						if(hd_len==iTemp)
						//							iTemp_minus_hd_len=iTemp;
						//						else
						//							iTemp_minus_hd_len=iTemp-hd_len;

					}*/


					CstcLog_printf("OTA::Readed rbTemp wow fantastic= %s ",rbTemp);
					CstcLog_printf("OTA::Readed iTemp wow fantastic = %d",(iTemp-hd_len));//iTemp_minus_hd_len);

//					CstcLog_HexDump_1(rbTemp,(iTemp));
					//					usleep(10000);


					ret = GPRS_compose_response_packet((char*)rbTemp, iclient_socket,(iTemp/*-hd_len*/),&j,header_flag);

					CstcLog_printf("OTA::rGPRS_compose_response_packet().... ret = %d",ret);

					if(ret==3)
					{
						CstcLog_printf("OTA::Response done iTemp 1: %d",iTemp);
						display_popup("ALL DATA UPTO DATE");
						updated_flag = 3;
						break;
					}
					else if(ret==2)
					{
						CstcLog_printf("OTA::Response done iTemp 2: %d",iTemp);
						display_popup("No Update Found");
						updated_flag = 2;
						break;
					}
					else if(ret==1)
					{
						CstcLog_printf("OTA::Response done iTemp 3: %d",iTemp);
						display_popup("Update Found");
						updated_flag = 1;
						break;
					}
					else
					{
						CstcLog_printf("OTA::Response done iTemp 4: %d",iTemp);
						updated_flag = 0;
						if(ret==0)
						{
							display_popup("Err: Received Data \nWas Not Proper");
							break;
						}
					}

					CstcLog_printf("OTA::2. size of bTemp : %d",sizeof(bTemp));
					//printf("2. rbTemp : %s",rbTemp);
					memset(rbTemp,0,6000);
					memset(bTemp,0,sizeof(bTemp));
//					header_flag = 1;  //temporary commented -- sudhakar
					//iTemp=0;
				}
				else
				{
					CstcLog_printf("OTA::Not received any data");
				}

				key = 0;
				key = Kb_Get_Key_Sym_New_uart();
				if(key==KEY_SYM_ESCAPE)
					break;
				key = 0;

				CstcLog_printf("OTA::GPRS_global_iTemp = %d    GPRS_Total_packet_length=%d",GPRS_global_iTemp,GPRS_Total_packet_length);

			}
			while(GPRS_global_iTemp < GPRS_Total_packet_length);//OTA_CHUNK_BYTES);
			//			while(iTemp>=OTA_CHUNK_BYTES);//OTA_CHUNK_BYTES);
		}
		//}

		CstcLog_printf("OTA::Outside while loop...updated_flag = %d",updated_flag);

		memset(rbTemp,0,6000);
		memset(bTemp,0,sizeof(bTemp));
		header_flag = 0;
		if(updated_flag == 3)
		{
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			return 3;
		}
		else if(updated_flag == 2)
		{
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			return 2;
		}
		else if(updated_flag == 1)
		{
//			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			return 1;
		}
		else
		{
			tcp_close(iclient_socket);
			if(sn!=NULL)
			{
				free(sn);
				sn =NULL;
			}
			if(curl !=NULL)
			{
				curl_easy_cleanup(curl);
				curl=NULL;
			}
			return 0;
		}
	}
	memset(rbTemp,0,6000);
	memset(bTemp,0,sizeof(bTemp));
	header_flag = 1;
//	tcp_close(iclient_socket);
	if(sn!=NULL)
	{
		free(sn);
		sn =NULL;
	}
	if(curl !=NULL)
	{
		curl_easy_cleanup(curl);
		curl=NULL;
	}
	return 0;

}
