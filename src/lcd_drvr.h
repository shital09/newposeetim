#ifndef lcd_drvr_h_
#define lcd_drvr_h_
#include <directfb.h>
#include "keyboard_drvr.h"
#include "cstc_defines.h"
#include <time.h>
extern IDirectFB *dfb;
#define RGBA(R, G, B, A) ((((unsigned int)(unsigned char)R)<< 16) | \
						  (((unsigned int)(unsigned char)G)<< 8) | \
						  (((unsigned int)(unsigned char)B)<< 0)  | \
						  (((unsigned int)(unsigned char)A)<< 24))

#define COLOR_RED   RGBA(0xFF, 0x00, 0x00, 0xFF)
#define COLOR_GREEN RGBA(0x00, 0xFF, 0x00, 0xFF)
#define COLOR_BLUE  RGBA(0x00, 0x00, 0xFF, 0xFF)
#define COLOR_WHITE  RGBA(0xFF, 0xFF, 0xFF, 0xFF)
#define COLOR_BLACK RGBA(0x00, 0x00, 0x00, 0xFF)
#define COLOR_GREY RGBA(192, 192, 192, 0xFF)
#define COLOR_YELLOW RGBA(0xFF, 0xFF, 0x00, 0xFF)


typedef enum {
	ALG_CENTER,
		ALG_LEFT,
		ALG_RIGHT
}LCD_ALG;


int  lcd_init(int * argc, char **argv[]);
void init_popup(void);
void popup_deinit(void);
void lcd_uninit(void);
void lcd_set_bk_color(unsigned int color);
void lcd_set_font_color(unsigned int color);
void lcd_clean(void);
void popup_clean(void);
int lcd_printf(LCD_ALG alg, int font_height, const char * pszFmt,...);
int popup_printf(LCD_ALG alg, int font_height, const char * pszFmt,...);
void lcd_printf_ex(LCD_ALG alg, unsigned int height, unsigned int y, const char * pszFmt,...);
void popup_printf_ex(LCD_ALG alg, unsigned int height, unsigned y, const char * pszFmt,...);
void lcd_draw_rectangle(unsigned int x, unsigned int y, unsigned int width, unsigned int height);
void popup_draw_rectangle(unsigned int x, unsigned int y, unsigned int width, unsigned int height);
void lcd_set_background_picture(const char * pszPictureFileName);
void lcd_clear_background_picture(void);
void lcd_show_picture(const char * pszPictureFileName);
void lcd_draw_point(unsigned int x, unsigned int y, unsigned int color);
void lcd_save(void);
void lcd_restore(void);
void lcd_flip(void);
void popup_flip(void);
int lcd_menu(const char * pszTitle, int font_height, const char menu[][30], unsigned int count, int select);
int lcd_menu_bangla(const char * pszTitle, int font_height, const char menu[][300], unsigned int count, int select);
int lcd_menu_schedule(const char * pszTitle, int font_height, schedule_master_struct *schedule_master_struct_var, unsigned int count, int select);
int  Kb_Hit(void);
int  Kb_Get_Key_Sym(void);
int  Kb_Get_Key_Sym1(void);
int  Kb_Get_Key_Code(void);
int Kb_Get_Key_Sym_new(void);
int Kb_Get_Key_Sym_old(void);
int Kb_Get_Key_Sym_New_uart(void);
int lcd_menu_trip(const char * pszTitle, int font_height, const char menu[][50], unsigned int count, int select);
int lcd_menu_details(const char * pszTitle,char * schedNo, int font_height, waybill_master_struct waybill_master_struct_var,schedule_master_struct *schedule_master_struct_var, unsigned int count, int select);
int lcd_menu_stop_list(const char * pszTitle, int font_height, current_selected_trip_stops_struct *lcd_current_selected_trip_stops_struct_var,unsigned int count, int select, int stopFlag,current_selected_trip_stops_struct *select_stg_current_selected_trip_stops_struct_var, int stageRowCount, int fromcount);
void lcd_printf_x( int font_height,unsigned x, const char * pszFmt,...);
void popup_printf_x( int font_height,unsigned x, const char * pszFmt,...);
void lcd_printf_xy(unsigned int height, unsigned x,unsigned y, const char * pszFmt,...);
void popup_printf_xy(unsigned int height, unsigned x,unsigned y, const char * pszFmt,...);
int lcd_menu_TC(const char * pszTitle, int font_height, duty_master_struct *full_duty_master_struct_var, unsigned int count, int select);
int lcd_all_trip_status_report(const char * pszTitle, char schedNo[][SCHEDULE_NO_LEN+1],char trip_no[][TRIP_NO_LEN+1],char shift_no[][SHIFT_ID_LEN+1],char ticket_count[][TICKET_NO_LEN+1],char pass_cnt[][PASSG_COUNT_LEN+1],char pass_amt[][AMOUNT_LEN+1], int font_height,unsigned int count, int select,char * grandTotal,int endflag,char toll_amt[][AMOUNT_LEN+1]);
void return_substring(char *scanned_string,char break_character,char * ret_string,int length);
int lcd_menu_trips(const char * pszTitle, int font_height, schedule_master_struct *full_schedule_master_struct_var, unsigned int count, int select);
int lcd_menu_shifts(const char * pszTitle, int font_height, shift_type_struct *shift_type_struct_var, unsigned int count, int select);
int lcd_menu_concession(const char * pszTitle, int font_height, concession_type_struct *concession_type_struct_var, unsigned int count, int select);
#endif
