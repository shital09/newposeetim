#include <posapi.h>
#include <stdio.h>
#include <stdarg.h>
#include "cstclog.h"

#if DEBUG_LOG_ENABLE || THREAD_DEBUG_LOG_ENABLE
static FILE* logFile= NULL;
pthread_mutex_t work_mutex;
#endif

char LOG_FILE_ADDR[70] = {'\0'};

int CstcLog_Init()
{
#if DEBUG_LOG_ENABLE || THREAD_DEBUG_LOG_ENABLE

	int retval = -1;
	time_t t = time(NULL);
	struct tm ptm;

	//ptm.tm_year + 1900, ptm.tm_mon + 1, ptm.tm_mday, ptm.tm_hour, ptm.tm_min, ptm.tm_sec

	retval = pthread_mutex_init(&work_mutex, NULL);
	if (retval != 0) {
		return 0;
	}

	if(logFile != NULL)
	{
		fclose(logFile);
		logFile = NULL;
	}
	if(logFile == NULL)
	{
		ptm = *localtime(&t);

/*		sprintf(LOG_FILE_ADDR,"/home/user0/httpd/html/CSTCLog%d%02d%02d%02d%02d.txt",
				ptm.tm_year + 1900,ptm.tm_mon + 1,ptm.tm_mday,ptm.tm_hour,ptm.tm_min);*/

		sprintf(LOG_FILE_ADDR,"/home/user0/httpd/html/CSTC %02d-%02d-%d %02d:%02d.txt",
				ptm.tm_mday,ptm.tm_mon + 1,ptm.tm_year + 1900,ptm.tm_hour,ptm.tm_min);

		logFile = fopen(LOG_FILE_ADDR,"w+");
	}
	if(logFile == NULL)
	{
		return 0;
	}
	fputs("<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1252\"></head><body>",logFile);
	fputs("\n****************************************** CSTC Debug Log ******************************************\n",logFile);
	fprintf(logFile,"\t\t\t\t\tCREATED DATE: %02d/%02d/%d\n",ptm.tm_mday,ptm.tm_mon + 1,ptm.tm_year + 1900);
	fprintf(logFile,"\t\t\t\t\tCREATED TIME: %02d:%02d:%02d",ptm.tm_hour,ptm.tm_min,ptm.tm_sec);
	fputs("\n****************************************************************************************************\n",logFile);
	fputs("</body></html>",logFile);
	CstcLog_Deinit(/*logFile*/);

#endif
	return 1;
}

int CstcLog_Open(/*FILE* logFile*/)

{
#if DEBUG_LOG_ENABLE
	pthread_mutex_lock(&work_mutex);
	if(logFile == NULL)
	{
		logFile = fopen(LOG_FILE_ADDR,"a");
	}
	if(logFile == NULL)
	{
		return 0;
	}
#endif
	return 1;
}

void CstcLog_Deinit(/*FILE* logFile*/)
{
#if DEBUG_LOG_ENABLE
	fflush(logFile);
	if(logFile != NULL)
	{
		fclose(logFile);
		logFile = NULL;
	}
	else
	{
		printf("\nIn CstcLog_Deinit : logFile was NULL");
	}
	pthread_mutex_unlock(&work_mutex);
#endif
}

void CstcLog_printf(const char * pszFmt,...)
{
#if DEBUG_LOG_ENABLE
	char textBuf[2048];
	char prntBuf[2048];
	va_list arg;

	time_t now;
	char* pTime = NULL;
	time(&now);
	pTime = ctime(&now);
	pTime[strlen(pTime)-1] = '\0';
	sprintf(prntBuf,"[ %s ] [%d]:: ", pTime,(int)getpid);

	va_start(arg, pszFmt);
	vsnprintf(textBuf, sizeof(textBuf), pszFmt, arg);
	strcat(prntBuf,textBuf);
	va_end (arg);

	CstcLog_Open();

	if(NULL != logFile)
	{
		fputs("\n", logFile);
		fputs(prntBuf,logFile);
//		fputs("</body></html>",logFile);

		fflush(logFile);
	}
	CstcLog_Deinit(/*logFile*/);

#endif
}
int CstcLog_HexDump(void* pBuffer, int Count)
{
#if DEBUG_LOG_ENABLE

	unsigned int Offset;
	unsigned int Dlen;
	unsigned int i;
	unsigned char ch;
	unsigned char* pData = (unsigned char *) pBuffer;
	char Temp[80];

	for (Offset = 0; Count > 0; Count -= Dlen)
	{
		char* pCp = Temp;
		pCp += sprintf(pCp, "%08x: ", Offset);
		Dlen = (Count >= 16) ? 16 : Count;
		for (i = 0; i < 16; i++)
		{
			if (i < Dlen)
				pCp += sprintf(pCp, "%02x ", pData[Offset+i]);
			else
				pCp += sprintf(pCp, "   ");
		}

		*pCp++ = ' ';
		for (i = 0; i < Dlen; i++)
		{
			ch = pData[Offset+i];
			if (ch < 0x20 || ch > 0x7f)
				ch = '.';

			*pCp++ = ch;
		}
		Offset += Dlen;
		*pCp++ = 0;
		CstcLog_printf( "%s", Temp);
	}
#endif
	return 0;

}

void CstcLog_printf_1(const char * pszFmt,...)
{

#if THREAD_DEBUG_LOG_ENABLE
	char textBuf[4096];
	char prntBuf[4096];
	va_list arg;

	time_t now;
	char* pTime = NULL;
	time(&now);
	pTime = ctime(&now);
	pTime[strlen(pTime)-1] = '\0';

	sprintf(prntBuf,"[ %s ] [%d]:: ", pTime,(int)getpid);

	va_start(arg, pszFmt);
	vsnprintf(textBuf, sizeof(textBuf), pszFmt, arg);
	strcat(prntBuf,textBuf);
	va_end (arg);

	CstcLog_Open();

	if(NULL != logFile)
	{

		if(0 != (fseek(logFile,-14, SEEK_END)))
		{
			return ;
		}

		fputs("\n<br>", logFile);
		fputs(prntBuf,logFile);
		fputs("</body></html>",logFile);
		fflush(logFile);
	}
	CstcLog_Deinit(/*logFile*/);

#endif
}


int CstcLog_Init_1()
{
#if THREAD_DEBUG_LOG_ENABLE

	if(logFile != NULL)
	{
		fclose(logFile);
		logFile = NULL;
	}
	if(logFile == NULL)
	{
		logFile = fopen(LOG_FILE_ADDR,"w+");
	}
	if(logFile == NULL)
	{
		return 0;
	}
	CstcLog_printf("In CstcLog_Init : FILE created successfully");
	fputs("<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1252\"></head><body>",logFile);
	fputs("\n****************************** CSTC Debug Log ******************************* \n",logFile);
	fputs("</body></html>",logFile);
	CstcLog_Deinit(/*logFile*/);
#endif
	return 1;
}
int CstcLog_HexDump_2(void* pBuffer, int Count)
{
#if 0//THREAD_DEBUG_LOG_ENABLE

	unsigned int Offset;
	unsigned int Dlen;
	unsigned int i;
	unsigned char ch;
	unsigned char* pData = (unsigned char *) pBuffer;
	char Temp[80];

	for (Offset = 0; Count > 0; Count -= Dlen)
	{
		char* pCp = Temp;
		pCp += sprintf(pCp, "%08x: ", Offset);
		Dlen = (Count >= 16) ? 16 : Count;
		for (i = 0; i < 16; i++)
		{
			if (i < Dlen)
				pCp += sprintf(pCp, "%02x ", pData[Offset+i]);
			else
				pCp += sprintf(pCp, "   ");
		}

		*pCp++ = ' ';
		for (i = 0; i < Dlen; i++)
		{
			ch = pData[Offset+i];
			if (ch < 0x20 || ch > 0x7f)
				ch = '.';

			*pCp++ = ch;
		}
		Offset += Dlen;
		*pCp++ = 0;
		CstcLog_printf_2( "%s", Temp);
	}
#endif
	return 0;

}
int CstcLog_HexDump_1(void* pBuffer, int Count)
{
#if THREAD_DEBUG_LOG_ENABLE

	unsigned int Offset;
	unsigned int Dlen;
	unsigned int i;
	unsigned char ch;
	unsigned char* pData = (unsigned char *) pBuffer;
	char Temp[80];

	for (Offset = 0; Count > 0; Count -= Dlen)
	{
		char* pCp = Temp;
		pCp += sprintf(pCp, "%08x: ", Offset);
		Dlen = (Count >= 16) ? 16 : Count;
		for (i = 0; i < 16; i++)
		{
			if (i < Dlen)
				pCp += sprintf(pCp, "%02x ", pData[Offset+i]);
			else
				pCp += sprintf(pCp, "   ");
		}

		*pCp++ = ' ';
		for (i = 0; i < Dlen; i++)
		{
			ch = pData[Offset+i];
			if (ch < 0x20 || ch > 0x7f)
				ch = '.';

			*pCp++ = ch;
		}
		Offset += Dlen;
		*pCp++ = 0;
		CstcLog_printf_1( "%s", Temp);
	}
#endif
	return 0;

}

void CstcLog_printf_2(const char * pszFmt,...)
{

#if 0//THREAD_DEBUG_LOG_ENABLE
	char textBuf[4096];
	char prntBuf[4096];
	va_list arg;

	time_t now;
	char* pTime = NULL;
	time(&now);
	pTime = ctime(&now);
	pTime[strlen(pTime)-1] = '\0';

	sprintf(prntBuf,"[ %s ] [%d]:: ", pTime,(int)getpid);

	va_start(arg, pszFmt);
	vsnprintf(textBuf, sizeof(textBuf), pszFmt, arg);
	strcat(prntBuf,textBuf);
	va_end (arg);

	CstcLog_Open();

	if(NULL != logFile)
	{

		if(0 != (fseek(logFile,-14, SEEK_END)))
		{
			return ;
		}

		fputs("\n<br>", logFile);
		fputs(prntBuf,logFile);
		fputs("</body></html>",logFile);
		fflush(logFile);

	}
	CstcLog_Deinit(/*logFile*/);

#endif
}
