#ifndef keyboard_drvr_h_
#define keyboard_drvr_h_
#include <time.h>

#define KEY_CODE_0                0x0B
#define KEY_CODE_1                0x02
#define KEY_CODE_2                0x03
#define KEY_CODE_3                0x04
#define KEY_CODE_4                0x05
#define KEY_CODE_5                0x06
#define KEY_CODE_6                0x07
#define KEY_CODE_7                0x08
#define KEY_CODE_8                0x09
#define KEY_CODE_9                0x0A
#define KEY_CODE_ESCAPE           0x01
#define KEY_CODE_ENTER            0x1C
#define KEY_CODE_CURSOR_UP        0x67
#define KEY_CODE_CURSOR_DOWN      0x6C
#define KEY_CODE_BACKSPACE        0x0E         //Clear
#define KEY_CODE_PRINT            0x63         //Paper Feed
#define KEY_CODE_ALT              0x38         //Menu or Left ALT
#define KEY_CODE_CONTROL          0x1D         //Func or Left CRTL
#define KEY_CODE_SHIFT            0x2A         //ALPHA or Left Shift
#define KEY_CODE_F1               0x3B
#define KEY_CODE_F2               0x3C
#define KEY_CODE_F3               0x3D
#define KEY_CODE_F4               0x3E

#define KEY_SYM_0                0x30
#define KEY_SYM_1                0x31
#define KEY_SYM_2                0x32
#define KEY_SYM_3                0x33
#define KEY_SYM_4                0x34
#define KEY_SYM_5                0x35
#define KEY_SYM_6                0x36
#define KEY_SYM_7                0x37
#define KEY_SYM_8                0x38
#define KEY_SYM_9                0x39
#define KEY_SYM_ESCAPE           0x1B
#define KEY_SYM_ENTER            0x0D
#define KEY_SYM_CURSOR_UP        0xF002
#define KEY_SYM_CURSOR_DOWN      0xF003
#define KEY_SYM_BACKSPACE        0x08         //Clear
#define KEY_SYM_PRINT            0xF009       //Paper Feed
//#define KEY_SYM_ALT              0xF203       //Menu or Left ALT
#define KEY_SYM_ALT              0xF204       //Menu or Left ALT
#define KEY_SYM_CONTROL          0xF202       //Func or Left CRTL
#define KEY_SYM_SHIFT            0xF201       //ALPHA or Left Shift
#define KEY_SYM_F1               0xF101
#define KEY_SYM_F2               0xF102
#define KEY_SYM_F3               0xF103
#define KEY_SYM_F4               0xF104
#define KEY_SYM_POWEROFF         0xF00A

//int  kb_hit(void);
//int  kb_getkey(void);
unsigned short int getKeySymbol(unsigned short int key, int shiftKeyLevel,int preKeyFlag);
int changeStrUpperToLower(char *str);


#endif
