/*
 * serialcomm_interface.h
 *
 *  Created on: 09-Apr-2014
 *      Author: root
 */

#ifndef UART_INTERFACE_H_
#define UART_INTERFACE_H_
#include "lcd_drvr.h"
#include "tcp_drvr.h"
#include "db_interface.h"
#include "display_interface.h"
#include "cstc_defines.h"

void uart_demo(void);
int UART_Manual_Duty_Assignment_server(int DownloadEnableFlag);
int UART_Manual_Init_Assignment_server(void);
int UART_Do_duty_data_exchange(int iclient_socket);
int UART_Download_Data(int ifd);

int UART_compose_response_packet(char * bTemp, int iclient_socket, int iTemp,int *j);
void UART_final_packet_lan(char * pkt_format_ptr, char * pkt_ptr, int* length);
#if COMPRESSION_DECOMPRESSION
int UART_received_packet_lan(int i, char *tempCh, int iclient_socket,char * bTemp,int updateDataFlag,int *j);
#else
int UART_received_packet_lan(int i, char tempCh, int iclient_socket,char * bTemp,int updateDataFlag,int *j);
#endif
void UART_get_machine_info(char * sn, int nbytes);
//#ifndef DB_DATA_FILE_ENABLE
int UART_packet_parser_nresponder(char * bTemp);

#if COMPRESSION_DECOMPRESSION
int UART_packet_parser_input_master(char *tempCh,int recvPktID,int updateDataFlag);
#else
int UART_packet_parser_input_master(char tempCh,int recvPktID,int updateDataFlag);
#endif

int UART_check_master_status(void);
int UART_Schedule_Upload_server(void);

void remove_stars(const char *input, char *result);
void memset_packet_format(void);
void UART_compose_schedule_response_packet(char * pkt_ptr,int *length);
int wc(char* file_path, char* word);
int my_system (const char *command);
uint32_t rc_crc32(uint32_t crc, const char *buf, size_t len);
void remove_space(const char *input, char *result_waybill);


unsigned get_file_size (const char * file_name);
unsigned char* read_whole_file (const char * file_name);

int decompression(char *source,int sourceLen);

#endif /* SERIALCOMM_INTERFACE_H_ */
