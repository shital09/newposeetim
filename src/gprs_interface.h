/*
 * gprs_interface.h
 *
 *  Created on: 08-Apr-2014
 *      Author: root
 */

#ifndef GPRS_INTERFACE_H_
#define GPRS_INTERFACE_H_

#include "gprs_drvr.h"
#include "tcp_drvr.h"
#include "db_interface.h"

int Gprs_Interface_Init(void);
//int Gprs_Set_ISP(void);
//int Gprs_Set_PPP_Connection(void);
int Gprs_Set_PPP_Connection(int index);
int Gprs_Interface_Deinit(void);

int socket_client_demo(void);
void socket_client_curl(void);
size_t  write_data(void *ptr, size_t size, size_t nmemb, FILE *stream);
int socket_client_demo_auth(void);

int compose_request_packet(int iclient_socket);
int received_packet(int temp, char tempCh, int iclient_socket);
void packet_parser(char * bTemp);
void final_packet(char * pkt_format_ptr, char * pkt_ptr, int length);
unsigned get_file_size_gprs (const char * file_name);
//void
int read_whole_file_gprs (const char * file_name, unsigned s, unsigned char * contents, long int *seekPtr);

int upload_waybill_string(void);
int send_schedule_string(void);

int online_schedule_update(int,char*,int) ;
int curl_online_schedule_update(int update_type,char *id_string,int index);

void online_waybill_update(void);
void GPRS_compose_ack_packet(char *pkt_ptr,int *length);
void GPRS_final_packet(char * pkt_format_ptr, char * pkt_ptr, int *length);
int GPRS_check_master_status(void);
int GPRS_received_packet(int indx, char tempCh, int iclient_socket,char * bTemp,int updateDataFlag,int *j);
//int GPRS_compose_response_packet(char * bTemp, int iclient_socket, int iTemp,int *j);
int GPRS_compose_response_packet(char * bTemp, int iclient_socket, int iTemp,int *j,int header_flag);
int GPRS_packet_parser_input_master(char tempCh,int recvPktID,int updateDataFlag);
int get_http_header_len(char *temp_arr);
void show_sim_error(int retval);
int get_chunk_size_in_bytes_and_fill(FILE *gprs_file_ptr,char *,depot_struct *depot_struct_local_var);
int get_chunk_size_and_fill_curl(FILE *gprs_file_ptr,char *CHK_FILENAME);
int GPRS_set_chunk_ppp_connection(void);
int get_formatted_string_length(char *Formatted_string,...);
int dynamic_printf(char *Dest_string,char *Formatted_string,...);
int dynamic_strcat(char *Dest_string,char *Formatted_string,...);
int Gprs_Set_PPP_Connection_LC(void);

// curl
int online_update(void);     							//sudhakar
size_t  write_data_online(void *ptr, size_t size, size_t nmemb, FILE *stream); // Swap this is code for CURL
int Gprs_Set_PPP_Connection_Curl(void);
size_t  write_data_curl(void *ptr, size_t size, size_t nmemb, FILE *stream); // Swap this is code for CURL
int file_download_curl(void);

#endif /* GPRS_INTERFACE_H_ */


