#ifndef CSTCLOG_H
#define CSTCLOG_H
#include <stdarg.h>
#include "cstc_defines.h"
#include <pthread.h>

int CstcLog_Init(void);
int CstcLog_Init_1(void);
int CstcLog_Open(/*(FILE* logFile*/void);
void CstcLog_Deinit(/*FILE* logFile*/void);
void CstcLog_printf(const char * pszFmt,...);
int CstcLog_HexDump(void* pBuffer, int Count);
void CstcLog_printf_1(const char * pszFmt,...);


int CstcLog_Open_1(/*(FILE* logFile*/void);
void CstcLog_Deinit_1(/*FILE* logFile*/void);
void CstcLog_printf_1(const char * pszFmt,...);
int CstcLog_HexDump_1(void* pBuffer, int Count);
void CstcLog_printf_2(const char * pszFmt,...);
int CstcLog_HexDump_2(void* pBuffer, int Count);

#endif // CSTCLOG_H
