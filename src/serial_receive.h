/*
 * serial_receive.h
 *
 *  Created on: 05-Feb-2015
 *      Author: root
 */

#ifndef SERIAL_RECEIVE_H_
#define SERIAL_RECEIVE_H_

int send_data(const char *send_string);
int send_ack(char *start_string);
int wait_for_start(void);
int read_data(char *read_buff,int len);
int configure_serial(void);
int serial_receive(void);
int  progress_percentage(void);


#endif /* SERIAL_RECEIVE_H_ */
