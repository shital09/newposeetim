/*
 * display_interface.h
 *
 *  Created on: 08-Apr-2014
 *      Author: root
 */

#ifndef DISPLAY_INTERFACE_H_
#define DISPLAY_INTERFACE_H_
#include "lcd_drvr.h"
#include "cstc_defines.h"
#include <time.h>

int Init_Display(int * argc, char **argv[]);
void Deinit_Display(void);
int Display_Home_Screen(void);
void display_popup(char *message);
int Display_Download_Duty(void);
int Display_Connect_To_Local(void);
int Display_Connect_To_GPRS(void);
int Display_ConnectionFailed_To_GPRS(void);
int Display_Conductor_Main_Screen(selected_trip_info_struct* selected_trip_info_struct_var, waybill_wise_info_struct* waybill_wise_info_struct_var,all_flags_struct* all_flags_struct_var,current_passengers_details_struct* current_passengers_details_struct_var);//char* startPoint, char* endPoint, char* routeNo, char* busType);
int Display_Pass_Details(void);
int Display_Enter_Pass_No(void);
int Display_Last_Ticket(void);
int Main_Menu_Page1(void);
int Main_Menu_Page2(void);
int Main_Menu_Page3(void);
int Passanger_Count_Report_One(void);
int passanger_Count_Report_Two(void);
int Display_Toll_Payment(void);
int Recharge_screen(void);
int Recharge_Screen1(void);
int Recharge_Screen2(void);
int Check_Card_Balance(void);
int Check_Card_Balance1(void);
int Pass_Details_Insert(void);
int Pass_Details_Insert1(void);
int Show_Error_Msg(char* errMsg);

int Display_Time_On_Screen(void);
int Display_Login_Screen123(char* username, char *pswd,int key,int *cursrFlag,int *shiftKeyLevel,int LC_chk_flag);
int display_last_ticket1(void);
int display_last_screen1(char* ticket_no,int key);
int display_last_screen2(char* ticket_no);

int Display_Issue_Ticket_Screen1(char* fromStop, char* tillStop,char* fromStopName, char* tillStopName, char* fromStopStage, char* tillStopStage,all_flags_struct* all_flags_struct_var,int key,int *shiftKeyLevel);
int Display_Issue_Ticket_Screen_LC(char* fromStop, char* tillStop,char* fromStopName, char* tillStopName, char* fromStopStage, char* tillStopStage,all_flags_struct* all_flags_struct_var,int key,int *shiftKeyLevel,int *cursrFlag);

int Display_Issue_Ticket_Screen_Two1(char* fromStop, char* tillStop,char* fromStopName, char* tillStopName, ticket_display_struct* ticket_display_struct_var,all_flags_struct* all_flags_struct_var,int *cursrFlag,int key, int *ticketInit);
//void last_ticket_display(transaction_log_struct transaction_log_struct_var,ticket_display_struct* ticket_display_struct_var);

int Display_Change_Stage_Screen(char *selectedseqno, char *selectedname,int key,int *shiftKeyLevel);
int display_conductor_entry_screen(char* conductor_id, char *conductor_name,char *conductor_pwrd,char* conductor_type,char* licence_expiry_date,int key,int *cursrFlag,int *shiftKeyLevel);
int view_ticket_display(int key, char * ticketNo);
void view_schedule_details(schedule_master_struct schedule_master_struct_var, waybill_master_struct waybill_master_struct_var,int casekey);
int Issue_Pass_Ticket_screen(void);
int Display_Issue_Pass_Screen1(char* fromStop,char* fromStopName,all_flags_struct* all_flags_struct_var,int key,int *shiftKeyLevel);
//19june14
int duty_machine_not_regi(void);
int duty_machine_regi(void);
int duty_machine_regi_complete(void);
int duty_machine_no_waybill(void);
int duty_machine_no_waybill(void);
int duty_machine_no_waybill(void);
int duty_machine_new_waybill(void);
int duty_machine_new_waybill(void);
int duty_machine_uploading_master(void);
int duty_machine_uploading_master_complete(void);
int duty_machine_no_ticket_fare(void);
int duty_machine_receiving_fare(void);
int duty_machine_receiving_fare_complete(void);
int duty_machine_duty_not_started(void);
int duty_machine_allocating_duty(void);
int duty_machine_allocating_duty_complete(void);
int duty_machine_downloading_data(void);
int duty_machine_downloading_data_complete(void);
int duty_machine_erasing_data(void);
int duty_machine_downloading_data_complete(void);
int Display_Lugg_Screen(void);
int repeat_display_screen(char* fromStop, char* tillStop, ticket_display_struct* ticket_display_struct_var,all_flags_struct* all_flags_struct_var,int *cursrFlag,int key);
int Display_week_day(void);
const char *wd(int year, int month, int day);
int Display_Issue_Ticket_Lugg(char* fromStop, char* tillStop, char* fromStopName, char* tillStopName,ticket_display_struct* ticket_display_struct_var,int key, int * cursrFlag);
int status_report(int no_of_tickets,char *lugg_tkt_count, char*pass_tkt_count);
int display_current_trip_report(char *schedule_no,char *startTicketNo,char *endTicketNo,char * TripStartDt,char * TripStartTm,int tempTotal,char *adultcnt,char * studentCnt,char *adultamt,char *studentAmt,char *toll_Amt);
int display_all_trip_report(char *schedule_no,char *shift,char *temptrip,char *startTicketNo,char *endTicketNo,char * TripStartDt,char * TripStartTm,int tempTotal,char *adultcnt,char * childcnt,/*char *srcitizencnt,char *luggcnt,*/char *adultamt,char * childamt,/*char *srcitizenamt,*/char *tollamt,/*char *lugg_tkt_count,*/char *pass_tkt_count,int reportFlag,int endflag,int grandTotal);
int display_collection_report(void);
int Show_Error_Msg_UART(char* errMsg);
//int display_all_trip_status_report(char *start_date,char *start_time,char *schedule_no,char *trip_no,char *pass_cnt,char *pass_amt,int endflag);
int display_all_trip_status_report  (int j,char *scheduleNo, char *shift,char *tripNo,char *temptrip,char *ticketCount,char *passCnt,char *passAmt,char *totalPxcnt,char *tempTotal,int scheduleflag,int startflag,int endflag,char *grandTotal);
int Display_Pass_No(char* username, int key,int *cursrFlag,int *shiftKeyLevel);
int Display_Get_TicketNo_Screen(char* ticket_no,int key);
int Display_Get_RouteId_Screen(char* route_id,int key);
int Display_Get_CardNo_Screen(char* card_no,int key);
void Show_Ticket_display(transaction_log_struct transaction_log_struct_var,ticket_display_struct* ticket_display_struct_var);
void Display_Ticket_data(char **result_data,int y,int screen,char first_time_flag);
int Display_Update_App_Screen(char* password,int key);
int Display_Get_Time_Screen(char* s_time,int key);
int Display_Toll_Entry(char* receipt_no, char *toll_amount,int key,int *cursrFlag,int *shiftKeyLevel,int LC_chk_flag);
int Display_Get_RouteNo_Screen(char* route_no,int key,int *shiftKeyLevel);
#endif /* DISPLAY_INTERFACE_H_ */
