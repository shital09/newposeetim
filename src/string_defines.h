/*
 * string_defines.h
 *
 *  Created on: 17-May-2014
 *      Author: root
 */

#ifndef STRING_DEFINES_H_
#define STRING_DEFINES_H_


#endif /* STRING_DEFINES_H_ */

//const char Trip_Change[]={0xE0 ,0xB2 ,0x9F ,0xE0 ,0xB3 ,0x8D ,0xE0 ,0xB2 ,0xB0 ,0xE0 ,0xB2 ,0xBF ,0xE0 ,0xB2 ,0xAA ,0xE0 ,0xB3 ,0x8D ,0x20 ,0xE0 ,0xB2 ,0x9A ,0xE0 ,0xB3,0x87 ,0xE0,0xB2 ,0x82 ,0xE0 ,0xB2 ,0x9C ,0xE0 ,0xB3 ,0x8D ,0x20};
//const char  End_Trip[]={E0 ,0xB2 ,0x8E ,0xE0,0xB2 ,0x82 ,0xE0 ,0xB2 ,0xA1 ,0xE0 ,0xB3 ,0x8D ,0x20 ,0xE0 ,0xB2 ,0x9F ,0xE0 ,0xB3,0x8D ,0xE0 ,0xB2 ,0xB0 ,0xE0 ,0xB2 ,0xBF ,0xE0 ,0xB2 ,0xAA ,0xE0 ,0xB3 ,0x8D};
//const char  Sync_Data []={0xE0 ,0xB2 ,0xB8 ,0xE0 ,0xB2 ,0xBF ,0xE0 ,0xB2 ,0x82 ,0xE0 ,0xB2 ,0x95 ,0xE0 ,0xB3 ,0x8D ,0x20 ,0xE0 ,0xB2 ,0xA1 ,0xE0 ,0xB3,0x86 ,0xE0 ,0xB3 ,0x95 ,0xE0 ,0xB2 ,0x9F ,0xE0 ,0xB2 ,0xBE ,0xE0 ,0xB2 ,0xB5 ,0xE0 ,0xB2 ,0xA8 ,0xE0 ,0xB3 ,0x8D ,0xE0 ,0xB2 ,0xA8 ,0xE0 ,0xB3 ,0x81};
//const char Line_Checking[]={E0 B2 86 E0 B2 A8 E0 B3 8D E0 B2 B2 E0 B3 86 E0 B3 96 E0 B2 A8 E0 B3 8D 20 E0 B2 AA E0 B2 B0 E0 B2 BF E0 B2 B6 E0 B2 BF E0 B3 95 E0 B2 B2 E0 B2 BF E0 B2 B8 E0 B3 81 E0 B2 A4 E0 B3 8D E0 B2 A4 E0 B2 BF E0 B2 A6 E0 B3 86};
//const char Toll_Payment[]={E0 B2 9F E0 B3 86 E0 B3 82 E0 B3 95 E0 B2 B2 E0 B3 8D 20 E0 B2 AA E0 B2 BE E0 B2 B5 E0 B2 A4 E0 B2 BF};
//const char Report[]={E0 B2 B0 E0 B2 BF E0 B2 AA E0 B3 86 E0 B3 82 E0 B3 95 E0 B2 B0 E0 B3 8D E0 B2 9F E0 B3 8D};
//const char Duty_End[]={};
//const char Recharge[]={};
//const char Card_Balance[]={};
//const char Pass_Details[]={};
//const char Last_Ticket_Details[]={};
//const char Status[]={};


#define WELCOME "ಸ್ವಾಗತ"
#define TO "ಗೆ"
#define BMTC  "ಬಿಎಂಟಿಸಿ"
#define MAIN_MENU "ಮುಖ್ಯ ಮೆನು"                           //MAIN_MENU
#define ROUTE_DETAILS_MENU "ಮಾರ್ಗ ಮೆನು ವಿವರಗಳನ್ನು"         //ROUTE_DETAILS_MENU
#define TRIP_MENU "ಟ್ರಿಪ್ ಮೆನು"                           //TRIP_MENU
//# define KORAMAGALA  ""

// MAIN MENU

#define CHANGE_LANGUAGE "1.ಭಾಷೆಯನ್ನು ಬದಲಾಯಿಸಿ"     //CHANGE_LANGUAGE
#define TRIP_CHANGE "2.ಎಂಡ್ ಟ್ರಿಪ್"              //TRIP_CHANGE
#define END_TRIP  "3.ಎಂಡ್ ಟ್ರಿಪ್"               //END_TRIP
#define SYNC_DATA "4.ಸಿಂಕ್ ಡೇಟಾವನ್ನು"            //SYNC_DATA
#define LINE_CHECKING "5.ಸಿಂಕ್ ಡೇಟಾವನ್ನು"            //LINE_CHECKING

#define TOLL_PAYMENT "6.ಆನ್ಲೈನ್ ಪರಿಶೀಲಿಸುತ್ತಿದೆ"           //TOLL_PAYMENT
#define REPORT "7.ಟೋಲ್ ಪಾವತಿ"         //REPORT
#define DUTY_END "8.ಡ್ಯೂಟಿ ಎಂಡ್"		//DUTY_END
#define RECHARGE "9.ರೀಚಾರ್ಜ್"	//RECHARGE
#define CARD_BALANCE "10.ಕಾರ್ಡ್ ಬ್ಯಾಲೆನ್ಸ್"               //CARD_BALANCE

#define PASS_DETAILS "11.ಪಾಸ್ ವಿವರಗಳು"                 //PASS_DETAILS
#define LAST_TICKET_DETAILS "12.ಕೊನೆಯ ಟಿಕೆಟ್ ವಿವರಗಳು"       //LAST_TICKET_DETAILS
#define STATUS "13.ಸ್ಥಿತಿ"                              //STATUS


// Route_N_Schedule_Function()
#define ROUTE_DETAILS "1. ಮಾರ್ಗ ವಿವರಗಳು"                       //ROUTE_DETAILS
#define FARE_CHART "2. ಶುಲ್ಕ ಚಾರ್ಟ್"                         //FARE_CHART
#define STAGE_DETAILS "3. ಹಂತ ವಿವರಗಳು"                         //STAGE_DETAILS
#define SCHEDULE_DETAILS "4. ವೇಳಾಪಟ್ಟಿ ವಿವರಗಳು"                  //SCHEDULE_DETAILS


//Passenger_Type_Function()
#define CHILD_TICKET "1. ಮಕ್ಕಳ ಟಿಕೆಟ್"                         // child ticket
#define SR_CITIZEN_TICKET "2. ಸೀನಿಯರ್ ನಾಗರಿಕ ಟಿಕೆಟ್"                    // sr. citizen ticket
#define LUGGAGE_TICKET "3. ಸಾಮಾನು ಟಿಕೆಟ್"                   // luggage ticket
#define REPEAT_LAST_TICKET "1. ಕೊನೆಯ ಟಿಕೆಟ್ ಪುನರಾವರ್ತಿಸಿ"           // repeat last ticket
#define TRUCK_FEEDER "2. ಟ್ರಂಕ್ / ಫೀಡರ್ ಸ್ಟಾಪ್ಸ್"    //trunk / feeder

//Enter_Pass_Type_Details_Function()
#define STUDENT "1.ವಿದ್ಯಾರ್ಥಿ"
#define BLIND "2.ಕುರುಡ"
#define PHYSICALLY_CHALLENGED "3.ಅಂಗವಿಕಲ"
#define FREEDOM_FIGHTER "4.ಸ್ವಾತಂತ್ರ್ಯ ಹೋರಾಟಗಾರ"
#define POLICE_DUTY "5.ಪೊಲೀಸ್ ಡ್ಯೂಟಿ"
#define POLICE_WARRANT "6.ಪೊಲೀಸ್ ವಾರಂಟ್"

//Select_Trip()
#define FROM_STOP " ಬಸ್ ಸ್ಟಾಪ್ ಗೆ "     // from stop
#define TILL_STOP "ಕುರುಡ"                // to stop
#define ADULT "ವಯಸ್ಕರ     ="                  // adult
#define CHILD  "ಮಕ್ಕಳ         ="                 //child
#define TOTAL   "         (ಒಟ್ಟು)"           // total


//ERRORS
#define NO_ROUTE_SLCT "ಆಯ್ದ ಯಾವುದೇ ಮಾರ್ಗ"   // no route selected
#define NO_TKTS "ಇ.ಟಿ.ನಾನು.ಎಂ. ಯಾವುದೇ ಟಿಕೆಟ್"       // no tickets in etim

#define ETIM_LOCKED  "ಇ.ಟಿ.ನಾನು.ಎಂ. ಲಾಕ್"           // etim locked



#define CONCT_DEV_LOCAL "ಸ್ಥಳೀಯ ಸಾಧನವನ್ನು ಸಂಪರ್ಕಿಸಲು"  //Connect the Device to the Local
#define PRS_ENTR "ಪಿಸಿ ಮತ್ತು ನಂತರ ಪ್ರೆಸ್ ಕೀಲಿ ನಮೂದಿಸಿ"//PC and then Press “Enter Key”
#define TO_DWN_DUTY "ಡ್ಯೂಟಿ ಡೌನ್ಲೋಡ್."        //"    to Download the Duty.      "
#define DWNG_SCHDLE "ವೇಳಾಪಟ್ಟಿ ಡೌನ್ಲೋಡ್ ......."  //Downloading Schedule.......
#define TK_TIME "ಈ ಕೆಲವು ನಿಮಿಷಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳಬಹುದು"  //This may take few minutes

#define PLZ_WAIT "ದಯವಿಟ್ಟು ಕಾಯಿರಿ ......." //Please wait.......
#define DWN_FAILD "ಡೌನ್ಲೋಡ್ ವಿಫಲವಾಗಿದೆ. ಪ್ರಯತ್ನಿಸಿ"  //Download failed. Please try
#define CNTCT_DPT "ಮತ್ತೆ ಅಥವಾ ಡಿಪೋ ಸಂಪರ್ಕಿಸಿ." //   again or contact Depot.
#define ENTR_TO_RTRY "ಪ್ರೆಸ್ ಮರುಪ್ರಯತ್ನಿಸಲು ಕೀ ಯನ್ನು."//Press “Enter Key “ to retry.

#define SLCT_FRM_BUS_STP "ಬಸ್ ಸ್ಟಾಪ್ ಮತ್ತು ಆಯ್ಕೆ"  //Select From Bus Stop &
#define ENTR_PRCD "ಪ್ರೆಸ್ ಮುಂದುವರೆಯಲು ಯನ್ನು"  //Press Enter to Proceed
#define PASS_NO "ನಂ ಪಾಸ್"  //pass no.

#define LGT_WT "ಲೈಟ್ ತೂಕ" //Light Weight
#define HVY_WT "ಹೆವಿ ತೂಕ"  //Heavy Weight

#define TV_ELECT  "ಟಿವಿ / ಎಲೆಕ್ಟ್" //TV/Elect
#define PET_DOG "ಪೆಟ್ ಅನಿಮಲ್ಸ್-ಡಾಗ್"  //Pet Animals –Dog
#define PET_BRD "ಪೆಟ್ ಅನಿಮಲ್ಸ್-ಬರ್ಡ್ಸ್" //Pet Animals –Birds
#define PAPER "ನ್ಯೂಸ್ ಪೇಪರ್"  //News Paper
#define ENTR_TOLL "ಟೋಲ್ ಪ್ರಮಾಣ ಯನ್ನು" //Enter Toll Amount
#define STR_TXN "TXN ಶೇಖರಿಸಿಡಲು ಯನ್ನು"  // “Enter” to store the Txn
#define AMT_CARD "ಆಮ್ಟ್ ಕಾರ್ಡ್ ಗೆ ಸಲ್ಲುತ್ತದೆ"   //Amt Credited to the card



#define EXIT "ಮತ್ತು ನಿರ್ಗಮಿಸಿ"  //and Exit
#define SHOW_RFID "ನಿಮ್ಮ ಆರ್ ಎಫ್ ನಾನು ಡಿ ಕಾರ್ಡನ್ನು ತೋರಿಸಿ ದಯವಿಟ್ಟು" //Please Show your RFID card
#define OR "ಅಥವಾ"//OR
#define ENTR_ID "ID ಅನ್ನು ನಮೂದಿಸಿ:" //Enter ID :
#define INSN_REPRT "1. ತಪಾಸಣೆ ವರದಿ"  //1.Inspection Report
#define INPR_REPRT "2. ಇನ್ಸ್ಪೆಕ್ಟರ್ ರಿಪೋರ್ಟ್" //"2.Inspector Report"
#define STG_REPRT "3. ಬುದ್ಧಿವಂತ ವರದಿ ಹಂತ" //3.Stage wise Report
#define BK_CS "4. ಪುಸ್ತಕ ಕೇಸ್" //4.Book Case


#define PAGE_1_2 "ಪುಟ 1/2" //Page 1/2
#define PSNR_CNT "ಯಾತ್ರಿಗಳಿಗೆ" //Passenger Count
#define NINC_OUT "1.NINC ಮಹೋನ್ನತ" //NINC Outstanding
#define NINC_RD "2.NINC ಕೆಂಪು ಮಾರ್ಕ್" //NINC Red Mark
#define NINC_ORD "3.ಆರ್ಡಿನರಿ NINC"   //NINC Ordinary
#define NIAC_OUT "4.NIAC ಮಹೋನ್ನತ"    //NIAC Outstanding
#define NIAC_RD "5.NIAC ಕೆಂಪು ಮಾರ್ಕ್" //NIAC Red Mark
#define NIAC_ORD "6.ಆರ್ಡಿನರಿ NIAC"  ////NIAC Ordinary
#define CHK_OK "7.ಪರೀಕ್ಷಿಸಿದ್ದು ಸರಿ"  //Checked OK


// Duty_End()
#define NORMAL "1.ಸಾಧಾರಣ"       //Normal
#define BRK_DWN "2.ಡೌನ್ ಬ್ರೇಕ್"                 //Break Down
#define TRIP_CANCEL "3.ಟ್ರಿಪ್ ರದ್ದು"             //Trip Cancel
#define ACCIDENT "4.ಅಪಘಾತ"       //Accident

//Recharge_screen()
#define INSRT_CARD "ಕಾರ್ಡ್ ಸೇರಿಸಿ"  //Please insert card

//Recharge_Screen1()
#define CARD_NO "ಕಾರ್ಡ್ ಸಂಖ್ಯೆ:     "   //  card no.
#define NAME "ಹೆಸರು:         " //Name     :
#define BALANCE "ಬ್ಯಾಲೆನ್ಸ್:"          //Balance  :
#define ENTR_AMT "ಕ್ರೆಡಿಟ್ ಗೆ ಆಮ್ಟ್ ಯನ್ನು: ರಾತ್ರಿ"          //Enter the Amt to Credit:nnn
#define CUR_BLNCE "ಪ್ರಸ್ತುತ ಬ್ಯಾಲೆನ್ಸ್:"  //Current Balance  :
#define CANCEL_EXIT "ಪ್ರೆಸ್ ನಿರ್ಗಮಿಸಿ  ರದ್ದು ಮಾಡು"     //Press “Cancel” to Exit

#define INSRT_CARD "ಕಾರ್ಡ್ ಸೇರಿಸಿ"  //Please insert card
#define CARD_TYPE  "ಕಾರ್ಡ್ ಪ್ರಕಾರ:"       //Card Type   :
#define VALID_TILL "ಮಾನ್ಯ ಟಿಲ್:"       //Valid Till  :
#define PAGE_2_2 "ಪುಟ 2/2" //Page 2/2
#define PAGE_1_N "ಪುಟ 1/N" //Page 2/2



#define STRT_TIME "ಸಮಯ ಪ್ರಾರಂಭಿಸಿ"        //Start time
#define END_TIME  "ಎಂಡ್ ಸಮಯ"     //End time

#define INVALID_CRD "ಅಮಾನ್ಯವಾದ ಕಾರ್ಡ್"
#define CRD_AUTHENT "ಕಾರ್ಡ್ ದೃಢೀಕರಿಸಬಹುದು"

#define INVALID_USRNM "ಸರಿಯಾದ ಬಳಕೆದಾರ ಹೆಸರನ್ನು ಸೇರಿಸಿ"  //Please insert correct user name
#define INVALID PASSWD "ಸರಿಯಾದ ಪಾಸ್ವರ್ಡ್ ಸೇರಿಸಿ"    //Please insert correct password
#define INVLD_USR_PAS "ಅಮಾನ್ಯವಾದ ಬಳಕೆದಾರ ಹೆಸರು / ಪಾಸ್ವರ್ಡ್ದೆ"
#define INVLD_CRD "ಅಮಾನ್ಯವಾಗಿದೆ"
//#define TO "ಗೆ"
//#define TO "ಗೆ"
















