#include "lcd_drvr.h"
#include <directfb.h>
#include <posapi.h>
#include <unistd.h>
#include <stdarg.h>
#include <led.h>
#include "string_convert.h"
#include "cstclog.h"
#include "db_interface.h"
#include "display_interface.h"

#define ANIMATE
#define font_file_xx  ENGLISH_FONT_FILE_ADDR  //"/usr/share/fonts/wqy-microhei.ttf"
#define font_file_02  BANGLA_FONT_FILE_ADDR  //"/home/user1/fonts/Shonarb.ttf"
IDirectFB				*dfb 	= NULL;
static IDirectFBDisplayLayer	*layer	= NULL;
static IDirectFBWindow			*window	= NULL;
static IDirectFBWindow			*popup_window = NULL;
static IDirectFBEventBuffer 	*events	= NULL;
IDirectFBSurface               *main_surface = NULL;
IDirectFBSurface			   *popup_surface = NULL;
static IDirectFBFont		    *font_16 = NULL;
static IDirectFBFont		    *font_17 = NULL;
static IDirectFBFont		    *font_18 = NULL;

static IDirectFBFont		    *font_24 = NULL;
static IDirectFBFont		    *font_32 = NULL;

//IDirectFBFont		    *font_16 = NULL;
static IDirectFBFont		    *font_20 = NULL;
static IDirectFBFont		    *font_21 = NULL;
//IDirectFBFont		    *font_24 = NULL;
static IDirectFBFont		    *font_26 = NULL;
static IDirectFBFont		    *font_27 = NULL;
static IDirectFBFont		    *font_28 = NULL;
static IDirectFBFont		    *font_30 = NULL;
static IDirectFBFont		    *font_31 = NULL;
//IDirectFBFont		    *font_32 = NULL;
static IDirectFBFont		    *font_34 = NULL;
static IDirectFBFont		    *font_35 = NULL;
static IDirectFBFont		    *font_40 = NULL;
static IDirectFBFont		    *font_45 = NULL;
static IDirectFBFont		    *font_60 = NULL;

static unsigned int              back_color = COLOR_WHITE;
static unsigned int              font_color = COLOR_BLACK;

struct tm	pretm;
struct timeval	pretmvl, key_pretmvl;
unsigned long long pretmvl_micro_sec=0;
extern selected_trip_info_struct selected_trip_info_struct_var;
extern current_selected_trip_stops_struct *line_checking_current_selected_trip_stops_struct_var;
extern current_selected_trip_stops_struct *current_selected_trip_stops_struct_var; //for penalty stage selection
extern all_flags_struct all_flags_struct_var;
extern int temp_offset;
char key_released = 1;
int backlit_flag = 1,standby_mode = 0;
extern char enter_key_lock;
int lcd_init(int * argc, char **argv[]){
	int retval = 0;
	int width = 0, height = 0;



	DFBWindowDescription	desc;
	DFBDisplayLayerConfig	config;
	DFBFontDescription	    fdesc;

	key_pretmvl.tv_usec=time(NULL);
	key_pretmvl.tv_sec=time(NULL);
	gettimeofday(&key_pretmvl, NULL);


	printf("lcd_init() >>\r\n");
	if (0 != DirectFBInit(argc, argv)){
		printf("DirectFInit Failed\r\n");
		retval = -1;
	}else if (0 != DirectFBCreate(&dfb)){
		printf("DirectFBCreate Failed\r\n");
		retval = -1;
	}
	else if (0 != dfb->GetDisplayLayer(dfb, DLID_PRIMARY, &layer)){
		printf("GetDisplayLayer Failed\r\n");
		retval = -1;			
	}else if (0 != layer->GetConfiguration(layer, &config)){
		printf("GetConfiguration Failed\r\n");
		retval = -1;
	}
	else {
		desc.flags  = DWDESC_POSX | DWDESC_POSY | DWDESC_WIDTH | DWDESC_HEIGHT | DWDESC_CAPS;
		desc.posx	= 0;
		desc.posy	= sys_get_status_bar_height();
		desc.width  = config.width;
		desc.height = config.height - desc.posy;
		printf("Screen size %d X %d\r\n", desc.width, desc.height);
		desc.caps	= DWCAPS_ALPHACHANNEL;
		desc.surface_caps = DSCAPS_TRIPLE;
	}

	if (0 == retval){
		if (0 !=  layer->CreateWindow(layer, &desc, &window)){
			printf("Create Window Failed\r\n");	
			retval = -1;
		}
		else if (0 !=  window->CreateEventBuffer(window, &events)){
			printf("GetConfiguration Failed\r\n");
			retval = -1;
		}
		else if (0 != window->GetSurface(window, &main_surface)){						//MAIN SURFACE
			printf("GetSurface Failed\r\n");
			retval = -1;
		}else if (0 != main_surface->GetSize(main_surface, &width, &height)){
			printf("GetSize Failed\r\n");
			retval = -1;		
		}	
	}
	init_popup();
	if (0 == retval){
		fdesc.flags  = DFDESC_HEIGHT;
		fdesc.height = 16;	
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_16);
		if (NULL == font_16){
			printf("create font16 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 17;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_17);
		if (NULL == font_17){
			printf("create font17 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 18;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_18);
		if (NULL == font_18){
			printf("create font18 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 20;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_20);
		if (NULL == font_20){
			printf("create font20 failed.\r\n");
			retval = -1;
		}
		fdesc.height = 21;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_21);
		if (NULL == font_21){
			printf("create font21 failed.\r\n");
			retval = -1;
		}
		fdesc.height = 24;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_24);
		if (NULL == font_24){
			printf("create font24 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 26;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_26);
		if (NULL == font_26){
			printf("create font26 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 27;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_27);
		if (NULL == font_27){
			printf("create font27 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 28;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_28);
		if (NULL == font_28){
			printf("create font28 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 30;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_30);
		if (NULL == font_30){
			printf("create font30 failed.\r\n");
			retval = -1;
		}

		// bangla font font_31
		fdesc.height = 31;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_31);
		if (NULL == font_31){
			printf("create font31 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 32;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_32);
		if (NULL == font_32){
			printf("create font32 failed.\r\n");
			retval = -1;
		}

		// bangla font font_34
		fdesc.height = 34;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_34);
		if (NULL == font_34){
			printf("create font34 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 35;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_35);
		if (NULL == font_35){
			printf("create font35 failed.\r\n");
			retval = -1;
		}

		fdesc.height = 40;
		dfb->CreateFont(dfb, font_file_xx, &fdesc, &font_40);
		if (NULL == font_40){
			printf("create font40 failed.\r\n");
			retval = -1;
		}



		fdesc.height = 45;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_45);
		if (NULL == font_45){
			printf("create font45 failed.\r\n");
			retval = -1;
		}


		fdesc.height = 60;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_60);
		if (NULL == font_60){
			printf("create font60 failed.\r\n");
			retval = -1;
		}



	}

	if (0 == retval){
		window->SetOptions(window, DWOP_ALPHACHANNEL);
		window->SetOpacity(window, 0xFF);
		main_surface->SetFont(main_surface, font_16);
		lcd_set_font_color(COLOR_BLACK);
		lcd_clean();
	}

	//if (NULL != popup_window){
	popup_window->Close(popup_window);
	//}
	//	sys_backlight_set_time(LCD_BACKLIGHT_TIME);
	//	key_pretmvl.tv_sec = KEYPAD_LOCK_TIME;
	printf("lcd_init() <<\r\n");
	return retval;
}
//---------------------------------------------------------------------------------------------
void init_popup(void)
{
	int width = 0, height = 0,retval = 0;
	DFBWindowDescription	desc;
	DFBDisplayLayerConfig	config;
	//	DFBFontDescription	    fdesc;
	/*  INITIALIZE POPUP WINDOW HERE BECAUSE CAUSING KEY DELAYS AT MAIN SCREEN */

	if (0 != layer->GetConfiguration(layer, &config)){
		printf("GetConfiguration Failed\r\n");
		retval = -1;
	}
	else {
		desc.flags  = DWDESC_POSX | DWDESC_POSY | DWDESC_WIDTH | DWDESC_HEIGHT | DWDESC_CAPS;
		desc.posx	= 40;
		desc.posy	= 60;//sys_get_status_bar_height();
		desc.width  = 240;
		desc.height = 120;
		//printf("Screen size %d X %d\r\n", desc.width, desc.height);
		desc.caps	= DWCAPS_SUBWINDOW;//DWCAPS_ALPHACHANNEL;
		desc.surface_caps = DSCAPS_TRIPLE;
	}



	if (0 !=  layer->CreateWindow(layer, &desc, &popup_window)){							//POPUP WINDOW
		printf("Create Window Failed\r\n");
		retval = -1;
	}
	//		else if (0 !=  popup_window->CreateEventBuffer(popup_window, &events)){
	//					printf("GetConfiguration Failed\r\n");
	//					retval = -1;
	//				}
	else if (0 !=  popup_window->DetachEventBuffer(popup_window, events)){
		printf("GetConfiguration Failed\r\n");
		retval = -1;
	}
	else if (0 != popup_window->GetSurface(popup_window, &popup_surface)){						//POPUP SURFACE
		printf("GetSurface Failed\r\n");
		retval = -1;
	}else if (0 != popup_surface->GetSize(popup_surface, &width, &height)){
		printf("GetSize Failed\r\n");
		retval = -1;
	}



	if (0 == retval){													//popup window
		popup_window->SetOptions(popup_window, DWOP_ALPHACHANNEL);
		popup_window->SetOptions(popup_window, DWOP_GHOST);

		popup_window->SetOpacity(popup_window, 0x00);
		popup_surface->SetFont(popup_surface, font_16 );
		lcd_set_font_color(COLOR_BLACK);
		//lcd_set_bk_color(COLOR_BLACK);
		popup_clean();
		//lcd_clean();
	}
}
//--------------------------------------------------------------
void popup_deinit(void)
{
	//DFBWindowEvent  windowEvent;
	if (NULL != popup_window){
		popup_window->Close(popup_window);

	}
	if (NULL != popup_surface)
		popup_surface->Release(popup_surface);
	//
	if (NULL != popup_window)
		popup_window->Release(popup_window);
	if (NULL != events)
		events->Release(events);

}
//---------------------------------------------------------------------------------------------

void lcd_uninit(void){
	DFBWindowEvent  windowEvent;
	//DFBResult ret;

	if (NULL != window){
		window->Close(window);
		while(1){			
			if (events->HasEvent(events) == DFB_OK){
				if (events->PeekEvent(events, DFB_EVENT(&windowEvent)) == DFB_OK) {
					events->GetEvent(events, DFB_EVENT(&windowEvent));
					if (windowEvent.type == DWET_KEYUP){
						window->Close(window);
						printf("key up********************%02x\r\n", windowEvent.key_symbol);
					}else if (windowEvent.type == DWET_CLOSE){
						window->Destroy(window);
						break;
					}					
				}
			} 
		}
	}

	if (NULL != events)
		events->Release(events);

	if (NULL != main_surface)
		main_surface->Release(main_surface);

	if (NULL != window)
		window->Release(window);

	if (NULL != layer)
		layer->Release(layer);




	if (NULL != font_16)
		font_16->Release(font_16);

	if (NULL != font_17)
		font_17->Release(font_17);

	if (NULL != font_18)
		font_18->Release(font_18);

	if (NULL != font_20)
		font_20->Release(font_20);

	if (NULL != font_21)
		font_21->Release(font_21);

	if (NULL != font_24)
		font_24->Release(font_24);

	if (NULL != font_26)
		font_26->Release(font_26);

	if (NULL != font_27)
		font_27->Release(font_27);

	if (NULL != font_28)
		font_28->Release(font_28);

	if (NULL != font_30)
		font_30->Release(font_30);

	// bangla
	if (NULL != font_31)
		font_31->Release(font_31);

	if (NULL != font_32)
		font_32->Release(font_32);

	// bangla
	if (NULL != font_34)
		font_34->Release(font_34);

	if (NULL != font_35)
		font_35->Release(font_35);

	if (NULL != font_40)
		font_40->Release(font_40);

	if (NULL != font_45)
		font_45->Release(font_45);

	if (NULL != font_60)
		font_60->Release(font_60);


	if (NULL != dfb){
		dfb->WaitForSync(dfb);
		dfb->Release(dfb);	
	}
}

void lcd_set_bk_color(unsigned int color){
	back_color = color;
}

void lcd_set_font_color(unsigned int color){
	font_color = color;	
}

static int current_y = 1;
void lcd_clean(void){	
	main_surface->Clear(main_surface, 
			0xFF & (back_color>> 16),
			0xFF & (back_color >> 8),
			0xFF & (back_color >> 0),
			0xFF & (back_color)>>24);
	current_y = 1;	
}
void popup_clean(void){
#ifdef ANIMATE
	u8 opacity_value,i;
#endif

	int popup_back_color = COLOR_GREY;
	popup_surface->Clear(popup_surface,
			0xFF & (popup_back_color>> 16),
			0xFF & (popup_back_color >> 8),
			0xFF & (popup_back_color >> 0),
			0xFF & (popup_back_color)>>24);
#ifdef ANIMATE
	popup_window->GetOpacity(popup_window,&opacity_value);
	for(i = opacity_value ; i >= 0 ;i-=52)
	{
		popup_window->SetOpacity(popup_window, i);
		if((int)(i-52)<0)
		{
			popup_window->SetOpacity(popup_window, 0);
			break;
		}

	}
	popup_window->SetOpacity(popup_window, 0);
#else
	popup_window->SetOpacity(popup_window, 0x01);
#endif
	current_y = 1;
}

int lcd_printf(LCD_ALG alg, int font_height, const char * pszFmt,...){
	int width, height;
	//	int font_height;
	IDirectFBSurface * sub_surface = NULL;
	IDirectFBSurface * store_surface = NULL;
	DFBRectangle rect;
	DFBSurfaceDescription	surfdesc;
	IDirectFBFont * font = NULL;

	char textbuf[2048];
	const char *utf8text;
	//const char *pnewline;
	int str_length;//, temp_width;
	va_list arg;

	int old_y =0;
	//memset(pnewline,0,100);
	va_start(arg, pszFmt);
	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);	
	va_end (arg);
	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;
	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;
	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}
	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &width, &height);
	main_surface->SetColor(main_surface,
			0xFF & (font_color>> 16),
			0xFF & (font_color >> 8),
			0xFF & (font_color >> 0),
			0xFF & (font_color>>24));

	if (current_y + font_height > height){
		rect.x = 0;
		rect.y = current_y + font_height - height;
		rect.w = width;
		rect.h = current_y - rect.y;
		main_surface->GetSubSurface (main_surface, &rect, &sub_surface);

		surfdesc.flags = DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT;
		surfdesc.caps  = DSCAPS_SYSTEMONLY;
		surfdesc.width = width;
		surfdesc.height= height;
		dfb->CreateSurface(dfb, &surfdesc, &store_surface);
		store_surface->SetBlittingFlags(store_surface, 	DSBLIT_BLEND_ALPHACHANNEL);
		store_surface->Blit(store_surface, sub_surface, NULL, 0, 0);
		lcd_clean();
		main_surface->SetBlittingFlags(main_surface, 	DSBLIT_BLEND_ALPHACHANNEL);
		rect.y = 0;
		main_surface->Blit(main_surface, store_surface, &rect, 0, 0);

		store_surface->Release(store_surface);
		sub_surface->Release(sub_surface);
		current_y = rect.h;

	}

	//	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	//		font->GetStringBreak(font, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//	else if(font_height==45)
	//	font_45->GetStringBreak(font_45, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//	else if(font_height==60)
	//	font_60->GetStringBreak(font_60, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//if((font_height!=45)&&(font_height!=60))
	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		str_length = strlen(textbuf);
		utf8text = string_covert(textbuf, str_length);
		if (ALG_CENTER == alg)
			main_surface->DrawString(main_surface, utf8text, string_len(utf8text), width/2, current_y, DSTF_TOPCENTER);
		else if (ALG_LEFT == alg)
			main_surface->DrawString(main_surface, utf8text, string_len(utf8text), 2, current_y, DSTF_TOPLEFT);
		else
			main_surface->DrawString(main_surface, utf8text, string_len(utf8text), width, current_y, DSTF_TOPRIGHT);
	}
	else
	{
		if (ALG_CENTER == alg)
			main_surface->DrawString(main_surface, pszFmt, string_len(pszFmt), width/2, current_y, DSTF_TOPCENTER);
		else if (ALG_LEFT == alg)
			main_surface->DrawString(main_surface, pszFmt, string_len(pszFmt), 2, current_y, DSTF_TOPLEFT);
		else
			main_surface->DrawString(main_surface, pszFmt, string_len(pszFmt), width, current_y, DSTF_TOPRIGHT);
	}

	old_y	= current_y;
	current_y = current_y + font_height + 1;


	//	if (NULL != pnewline)
	//	{
	//		lcd_printf(alg,16, pnewline);
	//
	//	}

	return old_y;
}
//--------------------------------------------------------------------------------------------------------
int popup_printf(LCD_ALG alg, int font_height, const char * pszFmt,...){
	int width, height;
	//	int font_height;
	IDirectFBSurface * sub_surface = NULL;
	IDirectFBSurface * store_surface = NULL;
	DFBRectangle rect;
	DFBSurfaceDescription	surfdesc;
	IDirectFBFont * font = NULL;

	char textbuf[2048];
	const char *utf8text;
	//	const char * pnewline = NULL;
	int str_length;//, temp_width;
	va_list arg;

	int old_y =0;

	va_start(arg, pszFmt);
	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);
	va_end (arg);
	switch(font_height)
	{
	case 16:
		popup_surface->SetFont(popup_surface, font_16);
		break;
	case 17:
		popup_surface->SetFont(popup_surface, font_17);
		break;
	case 18:
		popup_surface->SetFont(popup_surface, font_18);
		break;
	case 20:
		popup_surface->SetFont(popup_surface, font_20);
		break;
	case 21:
		popup_surface->SetFont(popup_surface, font_21);
		break;
	case 24:
		popup_surface->SetFont(popup_surface, font_24);
		break;
	case 26:
		popup_surface->SetFont(popup_surface, font_26);
		break;
	case 27:
		popup_surface->SetFont(popup_surface, font_27);
		break;
	case 28:
		popup_surface->SetFont(popup_surface, font_28);
		break;
	case 30:
		popup_surface->SetFont(popup_surface, font_30);
		break;
	case 31:
		popup_surface->SetFont(popup_surface, font_31);
		break;
	case 32:
		popup_surface->SetFont(popup_surface, font_32);
		break;
	case 34:
		popup_surface->SetFont(popup_surface, font_34);
		break;
	case 35:
		popup_surface->SetFont(popup_surface, font_35);
		break;
	case 40:
		popup_surface->SetFont(popup_surface, font_40);
		break;
	case 45:
		popup_surface->SetFont(popup_surface, font_45);
		break;
	case 60:
		popup_surface->SetFont(popup_surface, font_60);
		break;
	default:
		break;
	}
	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		popup_surface->GetFont(popup_surface, &font);
		font->GetHeight(font, &font_height);
	}
	popup_surface->GetSize(popup_surface, &width, &height);
	popup_surface->SetColor(popup_surface,
			0xFF & (font_color>> 16),
			0xFF & (font_color >> 8),
			0xFF & (font_color >> 0),
			0xFF & (font_color>>24));

	if (current_y + font_height > height){
		rect.x = 0;
		rect.y = current_y + font_height - height;
		rect.w = width;
		rect.h = current_y - rect.y;
		popup_surface->GetSubSurface (popup_surface, &rect, &sub_surface);

		surfdesc.flags = DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT;
		surfdesc.caps  = DSCAPS_SYSTEMONLY;
		surfdesc.width = width;
		surfdesc.height= height;
		dfb->CreateSurface(dfb, &surfdesc, &store_surface);
		store_surface->SetBlittingFlags(store_surface, 	DSBLIT_BLEND_ALPHACHANNEL);
		store_surface->Blit(store_surface, sub_surface, NULL, 0, 0);
		lcd_clean();
		popup_surface->SetBlittingFlags(popup_surface, 	DSBLIT_BLEND_ALPHACHANNEL);
		rect.y = 0;
		popup_surface->Blit(popup_surface, store_surface, &rect, 0, 0);

		store_surface->Release(store_surface);
		sub_surface->Release(sub_surface);
		current_y = rect.h;

	}

	//	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	//		font->GetStringBreak(font, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//	else if(font_height==45)
	//	font_45->GetStringBreak(font_45, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//	else if(font_height==60)
	//	font_60->GetStringBreak(font_60, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//if((font_height!=45)&&(font_height!=60))
	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		str_length = strlen(textbuf);
		utf8text = string_covert(textbuf, str_length);
		if (ALG_CENTER == alg)
			popup_surface->DrawString(popup_surface, utf8text, string_len(utf8text), width/2, current_y, DSTF_TOPCENTER);
		else if (ALG_LEFT == alg)
			popup_surface->DrawString(popup_surface, utf8text, string_len(utf8text), 2, current_y, DSTF_TOPLEFT);
		else
			popup_surface->DrawString(popup_surface, utf8text, string_len(utf8text), width, current_y, DSTF_TOPRIGHT);
	}
	else
	{
		if (ALG_CENTER == alg)
			popup_surface->DrawString(popup_surface, pszFmt, string_len(pszFmt), width/2, current_y, DSTF_TOPCENTER);
		else if (ALG_LEFT == alg)
			popup_surface->DrawString(popup_surface, pszFmt, string_len(pszFmt), 2, current_y, DSTF_TOPLEFT);
		else
			popup_surface->DrawString(popup_surface, pszFmt, string_len(pszFmt), width, current_y, DSTF_TOPRIGHT);
	}

	old_y	= current_y;
	current_y = current_y + font_height + 1;


	//	if (NULL != pnewline)
	//	{
	//		lcd_printf(alg,16, pnewline);
	//	}

	return old_y;
}
void return_substring(char *scanned_string,char break_character,char * ret_string,int length)
{
	int i,j;
	char char_found = 0;
	char_found = 0;
	for(i = 0,j = 0;i<length && scanned_string[i]!= 0x00;i++)
	{
		if(scanned_string[i] == break_character || char_found  == 1)
		{
			if(char_found == 1)
			{
				ret_string[j] = scanned_string[i];
				scanned_string[i] = 0x00;
				j++;
			}
			else
				scanned_string[i] = 0x00;
			char_found = 1;
		}

	}

}
void lcd_printf_ex(LCD_ALG alg, unsigned int height, unsigned y, const char * pszFmt,...){

	char textbuf[2048];

	char *new_line =NULL;
	va_list arg;
	va_start(arg, pszFmt);
	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);
	va_end (arg);
	current_y = y;
	new_line = (char *)malloc(100);
	memset(new_line,0,100);
	return_substring((char *)textbuf,'\n',new_line,100);

	if((height!=17)&&(height!=21)&&(height!=27)&&(height!=31)&&(height!=34)&&(height!=45)&&(height!=60))
	{
		lcd_printf(alg, height, textbuf);
	}
	else
	{
		lcd_printf(alg, height, pszFmt);
	}
	if(strlen(new_line))
	{

		if((height!=17)&&(height!=21)&&(height!=27)&&(height!=31)&&(height!=34)&&(height!=45)&&(height!=60))
		{
			lcd_printf(alg, 16, new_line);
		}
		else
		{
			lcd_printf(alg, 16, new_line);
		}
	}
	if(new_line != NULL)
		free(new_line);
}
void popup_printf_ex(LCD_ALG alg, unsigned int height, unsigned y, const char * pszFmt,...){

	char textbuf[2048];

	char *new_line =NULL;
	va_list arg;

	va_start(arg, pszFmt);
	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);
	va_end (arg);
	current_y = y;
	new_line = (char *)malloc(100);
	memset(new_line,0,100);
	return_substring((char *)textbuf,'\n',new_line,100);


	//	strcpy(new_line,strstr(pszFmt,"\n"));

	if((height!=17)&&(height!=21)&&(height!=27)&&(height!=31)&&(height!=34)&&(height!=45)&&(height!=60))
	{
		popup_printf(alg, height, textbuf);
	}
	else
	{
		popup_printf(alg, height, pszFmt);
	}
	if(new_line != NULL)
	{
		if((height!=17)&&(height!=21)&&(height!=27)&&(height!=31)&&(height!=34)&&(height!=45)&&(height!=60))
		{
			popup_printf(alg, height, new_line);
		}
		else
		{
			popup_printf(alg, height, new_line);
		}
	}
	if(new_line != NULL)
		free(new_line);
}

void lcd_draw_rectangle(unsigned int x, unsigned int y, unsigned int width, unsigned int height){
	int screen_width, screen_height;
	main_surface->GetSize(main_surface, &screen_width, &screen_height);
	if ((x+1 < screen_width) && (y +1 < screen_height)){
		if (x + 1 + width  > (unsigned int)screen_width)
			width = screen_width - x -1;
		if (x + 1 + height  > (unsigned int)screen_height)
			height = screen_height - y -1;

		main_surface->SetColor(main_surface,
				0xFF & (font_color >> 24),
				0xFF & (font_color >> 16),
				0xFF & (font_color >> 8),
				0xFF & (font_color));
		main_surface->DrawRectangle(main_surface, x, y, width, height);
	}	
}
void popup_draw_rectangle(unsigned int x, unsigned int y, unsigned int width, unsigned int height){
	int screen_width, screen_height;
	int rect_color = COLOR_BLACK;
	popup_surface->GetSize(popup_surface, &screen_width, &screen_height);
	if ((x+1 < screen_width) && (y +1 < screen_height)){
		if (x + 1 + width  > (unsigned int)screen_width)
			width = screen_width - x -1;
		if (x + 1 + height  > (unsigned int)screen_height)
			height = screen_height - y -1;

		popup_surface->SetColor(popup_surface,
				0xFF & (rect_color >> 16),
				0xFF & (rect_color >> 8),
				0xFF & (rect_color >> 0),
				0xFF & (rect_color)>>24);
		popup_surface->DrawRectangle(popup_surface, x, y, width, height);
	}
}


static IDirectFBWindow	*back_ground_window	= NULL;

void lcd_set_background_picture(const char * pszPictureFileName){
	DFBWindowDescription	desc;
	DFBDisplayLayerConfig	config;
	IDirectFBImageProvider *provider;
	IDirectFBSurface		*imgsurf = NULL;
	IDirectFBSurface		*windowSurf = NULL;
	DFBSurfaceDescription	 dsc;
	DFBResult				 err;
	if (NULL == back_ground_window){
		if (0 != layer->GetConfiguration(layer, &config)){
			printf("GetConfiguration Failed\r\n");
		}
		else {
			desc.flags  = DWDESC_POSX | DWDESC_POSY | DWDESC_WIDTH | DWDESC_HEIGHT | DWDESC_CAPS;
			desc.posx	= 0;
			desc.posy	= sys_get_status_bar_height();
			desc.width  = config.width;
			desc.height = config.height - desc.posy;
			desc.caps	= DWCAPS_ALPHACHANNEL;
			if (0 !=  layer->CreateWindow(layer, &desc, &back_ground_window)){
				printf("Create Window Failed\r\n");	
			}else{
				back_ground_window->SetOptions(back_ground_window, DWOP_ALPHACHANNEL);
				back_ground_window->SetOpacity(back_ground_window, 0xFF);
				back_ground_window->GetSurface(back_ground_window, &windowSurf);
			}
		}

	}

	if (NULL != back_ground_window){
		err = dfb->CreateImageProvider(dfb, pszPictureFileName, &provider);
		if (err != DFB_OK) {
			printf( "Couldn't load image from file '%s': %s\n",
					pszPictureFileName, DirectFBErrorString( err ));
		}else{
			provider->GetSurfaceDescription(provider, &dsc);
			dsc.flags = DSDESC_WIDTH | DSDESC_HEIGHT | DSDESC_PIXELFORMAT;
			dsc.pixelformat = DSPF_ARGB;
			if (dfb->CreateSurface(dfb, &dsc, &imgsurf) == DFB_OK)
				provider->RenderTo(provider, imgsurf, NULL);

			windowSurf->StretchBlit (windowSurf,imgsurf, NULL, NULL);

			imgsurf->Release(imgsurf);
			imgsurf = NULL;
			provider->Release(provider);
			provider = NULL;
		}
	}

	if (NULL != windowSurf){
		windowSurf->Release(windowSurf);
		windowSurf = NULL;
	}
}

void lcd_clear_background_picture(void){
	window->SetOpacity(window, 0xFF);
	window->RaiseToTop(window);
	lcd_flip();
	if (back_ground_window != NULL){
		back_ground_window->Release(back_ground_window);
		back_ground_window = NULL;
	}
}


IDirectFBSurface * __SCREEN_SAVED = NULL;
void lcd_save(void){
	DFBSurfaceDescription	surfdesc;

	if (NULL == __SCREEN_SAVED){
		surfdesc.flags = DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT;
		surfdesc.caps  = DSCAPS_SYSTEMONLY;
		main_surface->GetSize(main_surface, &surfdesc.width, &surfdesc.height);
		dfb->CreateSurface(dfb, &surfdesc, &__SCREEN_SAVED);
	}

	if (NULL != __SCREEN_SAVED)
		__SCREEN_SAVED->Blit(__SCREEN_SAVED, main_surface, NULL, 0, 0);
}

void lcd_restore(void){
	if (NULL != __SCREEN_SAVED)
		main_surface->Blit(main_surface, __SCREEN_SAVED, NULL, 0, 0);
}


void lcd_show_picture(const char * pszPictureFileName){
	IDirectFBImageProvider *provider;
	IDirectFBSurface		*imgsurf = NULL;

	DFBSurfaceDescription	 dsc;
	DFBResult				 err;



	err = dfb->CreateImageProvider(dfb, pszPictureFileName, &provider);

	if (err != DFB_OK) {
		printf( "Couldn't load image from file '%s': %s\n",
				pszPictureFileName, DirectFBErrorString( err ));
		lcd_printf_ex(ALG_CENTER,35,20,"WELCOME");
		lcd_printf_ex(ALG_CENTER,35,70,"TO");
		lcd_printf_ex(ALG_CENTER,35,110,"CSTC");
		lcd_flip();
	}else{
		provider->GetSurfaceDescription(provider, &dsc);
		dsc.flags = DSDESC_WIDTH | DSDESC_HEIGHT | DSDESC_PIXELFORMAT;
		dsc.height = 225;
		dsc.width = 226;
		dsc.pixelformat = DSPF_ARGB;
		if (dfb->CreateSurface(dfb, &dsc, &imgsurf) == DFB_OK)
			provider->RenderTo(provider, imgsurf, NULL);

		main_surface->FillRectangle(main_surface,0,0,226,225);
		main_surface->StretchBlit (main_surface,imgsurf, NULL, NULL);
		main_surface->Flip(main_surface,NULL,DSFLIP_WAITFORSYNC);
		//main_surface->Flip(main_surface, NULL, 0);
		//usleep(100);
		imgsurf->Release(imgsurf);
		imgsurf = NULL;
		provider->Release(provider);
		provider = NULL;
	}

}

void lcd_draw_point(unsigned int x, unsigned int y, unsigned int color){
	main_surface->SetColor(main_surface,
			0xFF & (color >> 24),
			0xFF & (color >> 16),
			0xFF & (color >> 8),
			0xFF & (font_color));
	y = y - sys_get_status_bar_height();
	main_surface->DrawLine(main_surface, x, y, x+1, y+1);

}



void lcd_flip(void){
	main_surface->Flip(main_surface, NULL, 0);
}
void popup_flip(void){
#ifdef ANIMATE
	u8 opacity_value,i;
#endif

	popup_surface->Flip(popup_surface, NULL, 0);
#ifdef ANIMATE
	popup_window->GetOpacity(popup_window,&opacity_value);
	for(i = opacity_value ; i < 0xE5 ;i+=52)
	{
		popup_window->SetOpacity(popup_window, i);
		if((unsigned int)(i+52)>255)
		{
			popup_window->SetOpacity(popup_window, 0xE5);
			break;
		}

	}
#else
	popup_window->SetOpacity(popup_window, 0xE5);
#endif

}


int lcd_menu_bangla(const char * pszTitle, int font_height, const char menu[][300], unsigned int count, int select){
	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0;
	int prevKey = 0;


	unsigned int     max_lines = 0;
	unsigned int     istart = 0;

	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));

	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=500000; //micrseconds

	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	//	CstcLog_printf( "In lcd_menu");
	//	CstcLog_printf( "Count=%d",count);

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	//	main_surface->GetFont(main_surface, &font);
	//	font->GetHeight(font, &font_height);

	//	CstcLog_printf( "Count=%d,Select=%d",count,select);
	if (select < 0)
	{
		select = 0;
	}
	//	CstcLog_printf( "select=%d",select);

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		//		CstcLog_printf( "In lcd_menu else");
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height, pszTitle);
			lcd_printf(ALG_CENTER,20, "************************");

			max_lines = (screen_height - current_y - 1) / font_height;


			istart = (select / max_lines) * max_lines;
			//			CstcLog_printf( "istart=%d ,max_lines=%d",istart,max_lines);
			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						//						CstcLog_printf( "istart=%d ,select=%d",istart,select);
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height +1);
						lcd_printf(ALG_LEFT,font_height ,menu[istart + i]);
						lcd_set_font_color(oldcolor);
					}
					else
						lcd_printf(ALG_LEFT,font_height, menu[istart + i]);


				}
				else
				{
					//					CstcLog_printf("End of for loop:istart:%d,i=%d,count=%d",istart,i,count);
					break;
				}
			}
			lcd_flip();

			LOOP:
			if (1)//Kb_Hit())
			{
				//				key = Kb_Get_Key_Sym();
				while(1)
				{
					key=0;
					if((key = Kb_Get_Key_Sym())>1)
					{
						break;
					}
					else
						key=0;
				}
				//				CstcLog_printf("key:%02x\r\n", key);
				switch((unsigned int)key)
				{
				case KEY_SYM_CURSOR_UP:
					select --;
					if (select < 0)
					{
						select = count -1;
					}
					prevKey = 0;
					break;
				case KEY_SYM_CURSOR_DOWN:
					select ++;
					if (select >= count)
					{
						select = 0;

					}
					prevKey = 0;
					break;
				case KEY_SYM_ESCAPE:
					select = -1;
					prevKey = 0;
					bLoop = 0;
					break;
				case KEY_SYM_ENTER:
					enter_key_lock = 1;

					bLoop = 0;
					prevKey = 0;
					break;
				default:
					if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
					{
						//						ti.tv_sec=time(NULL);
						//						nowtm =localtime(&(ti.tv_sec));
						//						ti.tv_usec=ti.tv_sec*1000;

						ti.tv_usec=time(NULL);
						ti.tv_sec=time(NULL);
						gettimeofday(&ti, NULL);

						total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_usec,ti.tv_usec);
						//						CstcLog_printf("%ld:::%ld",(ti.tv_sec-pretmvl.tv_sec),tmsec.tv_usec);

						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
						CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
						CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);


						//						if((ti.tv_sec-pretmvl.tv_sec)>=tmsec.tv_sec)
						if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))

						{
							prevKey = 0;
							pretmvl.tv_sec=ti.tv_sec;
							pretmvl.tv_usec=ti.tv_usec;
							pretmvl_micro_sec = total_micro_seconds;
							CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
						}
						select = (prevKey * 10) + (key & 0x0F) - 1;
						prevKey = select + 1;
						if (select >= count)
						{
							select = 0;
							prevKey = 0;
						}
						CstcLog_printf("Selected = %d", key);
						break;
					}
					else
					{
						prevKey = 0;
						goto LOOP;
					}
				}
			}
			else
			{
				goto LOOP;
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	//	CstcLog_printf( "End of  lcd_menu");
	retval = select;
	lcd_clean();
	return retval;
}


int lcd_menu(const char * pszTitle, int font_height, const char menu[][30], unsigned int count, int select)
{
	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0;
	int prevKey = 0;
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=500000; //micrseconds
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;

	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	//	CstcLog_printf( "In lcd_menu");
	//	CstcLog_printf( "Count=%d",count);

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	//	main_surface->GetFont(main_surface, &font);
	//	font->GetHeight(font, &font_height);

//	CstcLog_printf( "screen_height=%d    screen_width=%d",screen_height,screen_width);
	if (select < 0)
	{
		select = 0;
	}
	//	CstcLog_printf( "select=%d",select);

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		//		CstcLog_printf( "In lcd_menu else");
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);	
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height, pszTitle);
			lcd_printf(ALG_CENTER,20, "************************");

			max_lines = (screen_height - current_y - 1) / font_height;


			istart = (select / max_lines) * max_lines; 
			//			CstcLog_printf( "istart=%d ,max_lines=%d",istart,max_lines);
			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						//						CstcLog_printf( "istart=%d ,select=%d",istart,select);
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height +1);
						lcd_printf(ALG_LEFT,font_height ,menu[istart + i]);
						lcd_set_font_color(oldcolor);
					}
					else
						lcd_printf(ALG_LEFT,font_height, menu[istart + i]);


				}
				else
				{
					//					CstcLog_printf("End of for loop:istart:%d,i=%d,count=%d",istart,i,count);
					break;
				}
			}
			lcd_flip();	

			LOOP:
			//			if (Kb_Hit())
			//			{
			//								key = Kb_Get_Key_Sym();
			while(1)
			{
				key=0;
				key_released = 1;
				if((key = Kb_Get_Key_Sym())>1)
				{
					break;
				}
				else
					key=0;
			}
			//				CstcLog_printf("key:%02x\r\n", key);
			switch((unsigned int)key)
			{
			case KEY_SYM_CURSOR_UP:
				select --;
				if (select < 0)
				{
					select = count -1;
				}
				prevKey = 0;
				break;
			case KEY_SYM_CURSOR_DOWN:
				select ++;
				if (select >= count)
				{
					select = 0;

				}
				prevKey = 0;
				break;
			case KEY_SYM_ESCAPE:
				select = -1;
				prevKey = 0;
				bLoop = 0;
				break;
			case KEY_SYM_ENTER:
				enter_key_lock = 1;
				bLoop = 0;
				prevKey = 0;
				break;
			default:
				if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
				{
					//						ti.tv_sec=time(NULL);
					//						nowtm =localtime(&(ti.tv_sec));
					//						ti.tv_usec=ti.tv_sec*1000;

					ti.tv_usec=time(NULL);
					ti.tv_sec=time(NULL);
					gettimeofday(&ti, NULL);

					total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

					//						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
					//						CstcLog_printf("%ld:::%ld", pretmvl.tv_usec,ti.tv_usec);
					//						CstcLog_printf("%ld:::%ld",(ti.tv_sec-pretmvl.tv_sec),tmsec.tv_usec);

					CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
					CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
					CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);


					//						if((ti.tv_sec-pretmvl.tv_sec)>=tmsec.tv_sec)
					if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))

					{
						prevKey = 0;
						pretmvl.tv_sec=ti.tv_sec;
						pretmvl.tv_usec=ti.tv_usec;
						pretmvl_micro_sec = total_micro_seconds;
						CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
					}
					select = (prevKey * 10) + (key & 0x0F) - 1;
					prevKey = select + 1;
					if (select >= count)
					{
						select = 0;
						prevKey = 0;
					}
					CstcLog_printf("Selected = %d", key);
					break;
				}
				else
				{
					prevKey = 0;
					goto LOOP;
				}
			}
			//			}
			//			else
			//			{
			//				goto LOOP;
			//			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);	
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	//	CstcLog_printf( "End of  lcd_menu");
	retval = select;
	lcd_clean();
	return retval;
}


int lcd_menu_TC(const char * pszTitle, int font_height, duty_master_struct *full_duty_master_struct_var, unsigned int count, int select)
{
	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0;
	int prevKey = 0;
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=500000; //micrseconds
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;

	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	CstcLog_printf( "In lcd_menu_TC");

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	if (select < 0)
	{
		select = 0;
	}

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height, pszTitle);
			lcd_printf(ALG_CENTER,20, "************************");

			max_lines = (screen_height - current_y - 1) / font_height;


			istart = (select / max_lines) * max_lines;
			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height +1);

						lcd_printf(ALG_LEFT,font_height,"%d. %s ", istart+i+1, (full_duty_master_struct_var+(istart + i))->schedule_no);
						lcd_set_font_color(oldcolor);
					}
					else
						lcd_printf(ALG_LEFT,font_height,"%d. %s ", istart+i+1, (full_duty_master_struct_var+(istart + i))->schedule_no);

				}
				else
					break;
			}
			lcd_flip();

			LOOP:
			while(1)
			{
				key=0;
				if((key = Kb_Get_Key_Sym())>1)
				{
					break;
				}
				else
					key=0;
			}
			//				CstcLog_printf("key:%02x\r\n", key);
			switch((unsigned int)key)
			{
			case KEY_SYM_CURSOR_UP:
				select --;
				if (select < 0)
				{
					select = count -1;
				}
				prevKey = 0;
				break;
			case KEY_SYM_CURSOR_DOWN:
				select ++;
				if (select >= count)
				{
					select = 0;

				}
				prevKey = 0;
				break;
			case KEY_SYM_ESCAPE:
				select = -1;
				prevKey = 0;
				bLoop = 0;
				break;
			case KEY_SYM_ENTER:
				bLoop = 0;
				prevKey = 0;
				break;
			default:
				if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
				{
					//						ti.tv_sec=time(NULL);
					//						nowtm =localtime(&(ti.tv_sec));
					//						ti.tv_usec=ti.tv_sec*1000;

					ti.tv_usec=time(NULL);
					ti.tv_sec=time(NULL);
					gettimeofday(&ti, NULL);

					total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

					CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
					CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
					CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);

					if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))

					{
						prevKey = 0;
						pretmvl.tv_sec=ti.tv_sec;
						pretmvl.tv_usec=ti.tv_usec;
						pretmvl_micro_sec = total_micro_seconds;
						CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
					}
					select = (prevKey * 10) + (key & 0x0F) - 1;
					prevKey = select + 1;
					if (select >= count)
					{
						select = 0;
						prevKey = 0;
					}
					CstcLog_printf("Selected = %d", key);
					break;
				}
				else
				{
					prevKey = 0;
					goto LOOP;
				}
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	retval = select;
	lcd_clean();
	return retval;
}


int lcd_menu_trip(const char * pszTitle, int font_height, const char menu[][50], unsigned int count, int select)
{
	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0;
	int prevKey = 0;
	//	char tripNo[TRIP_NO_LEN+1]={'\0'};
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=500000; //micrseconds
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;

	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	//	CstcLog_printf( "In lcd_menu");
	//	CstcLog_printf( "Count=%d",count);

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	//	main_surface->GetFont(main_surface, &font);
	//	font->GetHeight(font, &font_height);

	//	CstcLog_printf( "Count=%d,Select=%d",count,select);
	if (select < 0)
	{
		select = 0;
	}
	//	CstcLog_printf( "select=%d",select);

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		//		CstcLog_printf( "In lcd_menu else");
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height, pszTitle);
			lcd_printf(ALG_CENTER,20, "************************");

			max_lines = (screen_height - current_y - 1) / font_height;


			istart = (select / max_lines) * max_lines;
			//			CstcLog_printf( "istart=%d ,max_lines=%d",istart,max_lines);
			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						//						CstcLog_printf( "istart=%d ,select=%d",istart,select);
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height);
						lcd_printf(ALG_LEFT,font_height ,menu[istart + i]);
						lcd_set_font_color(oldcolor);
					}
					else
						lcd_printf(ALG_LEFT,font_height, menu[istart + i]);


				}
				else
				{
					//					CstcLog_printf("End of for loop:istart:%d,i=%d,count=%d",istart,i,count);
					break;
				}
			}
			lcd_flip();

			LOOP:
			if (Kb_Hit())
			{
				//				key = Kb_Get_Key_Sym();
				while(1)
				{
					key=0;
					if((key = Kb_Get_Key_Sym())>1)
					{
						break;
					}
					else
						key=0;
				}
				//				CstcLog_printf("key:%02x\r\n", key);
				switch((unsigned int)key)
				{
				case KEY_SYM_CURSOR_UP:
					select --;
					if (select < 0)
					{
						select = count -1;
					}
					prevKey = 0;
					break;
				case KEY_SYM_CURSOR_DOWN:
					select ++;
					if (select >= count)
					{
						select = 0;
					}
					prevKey = 0;
					break;
				case KEY_SYM_ESCAPE:
					select = -1;
					bLoop = 0;
					prevKey = 0;
					break;
				case KEY_SYM_ENTER:
					bLoop = 0;
					prevKey = 0;
					break;
				default:
					if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
					{
						//						ti.tv_sec=time(NULL);
						//						nowtm =localtime(&(ti.tv_sec));
						//						ti.tv_usec=ti.tv_sec*1000;

						ti.tv_usec=time(NULL);
						ti.tv_sec=time(NULL);
						gettimeofday(&ti, NULL);

						total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_usec,ti.tv_usec);
						//						CstcLog_printf("%ld:::%ld",(ti.tv_sec-pretmvl.tv_sec),tmsec.tv_usec);

						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
						CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
						CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);


						//						if((ti.tv_sec-pretmvl.tv_sec)>=tmsec.tv_sec)
						if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))

						{
							prevKey = 0;
							pretmvl.tv_sec=ti.tv_sec;
							pretmvl.tv_usec=ti.tv_usec;
							pretmvl_micro_sec = total_micro_seconds;
							CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
						}
						select = (prevKey * 10) + (key & 0x0F) - 1;
						prevKey = select + 1;
						if (select >= count)
						{
							select = 0;
							prevKey = 0;
						}
						CstcLog_printf("Selected = %d", key);
						break;
					}
					else
					{
						prevKey = 0;
						goto LOOP;
					}
				}
			}
			else
			{
				goto LOOP;
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	//	CstcLog_printf( "End of  lcd_menu");
	retval = select;
	lcd_clean();
	return retval;
}

int lcd_menu_schedule(const char * pszTitle, int font_height, schedule_master_struct *schedule_master_struct_var, unsigned int count, int select)
{

	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0;
	int prevKey = 0;

	unsigned int     max_lines = 0;
	unsigned int     istart = 0;
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=500000; //micrseconds
	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	//	CstcLog_printf( "In lcd_menu");
	//	CstcLog_printf( "Count=%d",count);

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	//	main_surface->GetFont(main_surface, &font);
	//	font->GetHeight(font, &font_height);

	//	CstcLog_printf( "Count=%d,Select=%d",count,select);
	if (select < 0)
	{
		select = 0;
	}
	//	CstcLog_printf( "select=%d",select);

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		//		CstcLog_printf( "In lcd_menu else");
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height, pszTitle);
			lcd_printf(ALG_CENTER,20, "************************");

			max_lines = (screen_height - current_y - 1) / font_height;


			istart = (select / max_lines) * max_lines;
			//			CstcLog_printf( "istart=%d ,max_lines=%d",istart,max_lines);
			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						//						CstcLog_printf( "istart=%d ,select=%d",istart,select);
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height);
						//						%s : %s - %s    %s - %s ",(schedule_master_struct_var+i)->trip_id,(schedule_master_struct_var+i)->start_bus_stop_code,(schedule_master_struct_var+i)->end_bus_stop_code,(schedule_master_struct_var+i)->start_time, (schedule_master_struct_var+i)->end_time);
						//						lcd_printf(ALG_LEFT,font_height ,menu[istart + i]);
						lcd_printf(ALG_LEFT,font_height,"%s : %s - %s  %s (%s)", (schedule_master_struct_var+(istart + i))->trip_id, (schedule_master_struct_var+(istart + i))->start_bus_stop_code,(schedule_master_struct_var+(istart + i))->end_bus_stop_code, (schedule_master_struct_var+(istart + i))->start_time,/*(schedule_master_struct_var+(istart + i))->end_time,*/(schedule_master_struct_var+(istart + i))->route_no);//""schedule_master_struct_var+(istart + i));
						lcd_set_font_color(oldcolor);
					}
					else
					{
						lcd_printf(ALG_LEFT,font_height,"%s : %s - %s  %s (%s)",(schedule_master_struct_var+(istart + i))->trip_id, (schedule_master_struct_var+(istart + i))->start_bus_stop_code,(schedule_master_struct_var+(istart + i))->end_bus_stop_code, (schedule_master_struct_var+(istart + i))->start_time,/*(schedule_master_struct_var+(istart + i))->end_time,*/(schedule_master_struct_var+(istart + i))->route_no);//""schedule_master_struct_var+(istart + i));
						//						lcd_printf(ALG_LEFT,font_height, menu[istart + i]);
					}

				}
				else
				{
					//					CstcLog_printf("End of for loop:istart:%d,i=%d,count=%d",istart,i,count);
					break;
				}
			}
			lcd_flip();

			LOOP:
			if (Kb_Hit())
			{
				//				key = Kb_Get_Key_Sym();
				while(1)
				{
					key=0;
					if((key = Kb_Get_Key_Sym())>1)
					{
						break;
					}
					else
						key=0;
				}
				//				CstcLog_printf("key:%02x\r\n", key);
				switch((unsigned int)key)
				{
				case KEY_SYM_CURSOR_UP:
					select --;
					if (select < 0)
					{
						select = count -1;
					}
					prevKey = 0;
					break;
				case KEY_SYM_CURSOR_DOWN:
					select ++;
					if (select >= count)
					{
						select = 0;
					}
					prevKey = 0;
					break;
				case KEY_SYM_ESCAPE:
					select = -1;
					bLoop = 0;
					prevKey = 0;
					break;
				case KEY_SYM_ENTER:
					bLoop = 0;
					prevKey = 0;
					break;
				default:
					if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
					{
						//						ti.tv_sec=time(NULL);
						//						nowtm =localtime(&(ti.tv_sec));
						//						ti.tv_usec=ti.tv_sec*1000;

						ti.tv_usec=time(NULL);
						ti.tv_sec=time(NULL);
						gettimeofday(&ti, NULL);

						total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_usec,ti.tv_usec);
						//						CstcLog_printf("%ld:::%ld",(ti.tv_sec-pretmvl.tv_sec),tmsec.tv_usec);

						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
						CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
						CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);


						//						if((ti.tv_sec-pretmvl.tv_sec)>=tmsec.tv_sec)
						if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))

						{
							prevKey = 0;
							pretmvl.tv_sec=ti.tv_sec;
							pretmvl.tv_usec=ti.tv_usec;
							pretmvl_micro_sec = total_micro_seconds;
							CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
						}
						select = (prevKey * 10) + (key & 0x0F) - 1;
						prevKey = select + 1;
						if (select >= count)
						{
							select = 0;
							prevKey = 0;
						}
						CstcLog_printf("Selected = %d", key);
						break;
					}
					else
					{
						prevKey = 0;
						goto LOOP;
					}
				}
			}
			else
			{
				goto LOOP;
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	//	CstcLog_printf( "End of  lcd_menu");
	retval = select;
	lcd_clean();
	return retval;

}
int Kb_Hit(void)
{
	DFBWindowEvent  windowEvent;
	DFBResult ret;

	while(1){
		if (events->HasEvent(events) == DFB_OK){
			if (events->PeekEvent(events, DFB_EVENT(&windowEvent)) == DFB_OK) {
				if (/*windowEvent.type == DWET_KEYDOWN || */ windowEvent.type == DWET_KEYUP)
					return windowEvent.type;

				ret = events->GetEvent(events, DFB_EVENT(&windowEvent));
				if(ret){
					DirectFBError("IDirectFBEventBuffer::GetEvent() failed", ret);
				}
			}
		}
		else {
			break;
		}
	}

	return 0;
}

int Kb_Get_Key_Sym_new(void)
{
	DFBWindowEvent windowEvent;
	DFBResult ret;
	//	while (1){
	//	events->WaitForEvent(events);
	events->WaitForEventWithTimeout(events,0,200);
	if ((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK) {
		//		if (windowEvent.type == DWET_KEYDOWN)
		if (windowEvent.type == DWET_KEYDOWN && (key_released == 1))
		{
			if((windowEvent.key_symbol!=KEY_SYM_CURSOR_DOWN)&&(windowEvent.key_symbol!=KEY_SYM_CURSOR_UP))
				key_released = 0;
			return windowEvent.key_symbol;
		}
		else if (windowEvent.type == DWET_KEYUP && (key_released == 0))
		{
			key_released = 1;
			events->Reset(events);
			return 0;
		}

		return 0;
	}
	else
	{
		key_released = 1;
	}
	//		else {
	//			break;
	//		}
	//	}
	return 0;
}

int Kb_Get_Key_Sym_New_uart(void)
{
	DFBWindowEvent windowEvent;
	DFBResult ret;
	//	while (1){
	//		events->WaitForEvent(events);
	if ((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK) {
		if (windowEvent.type == DWET_KEYDOWN)
			return windowEvent.key_symbol;
		//				return 1;
	}
	//		else {
	//			break;
	//		}
	//	}
	return 0;
}

int Kb_Get_Key_Sym_old(void)
{
	DFBWindowEvent windowEvent;
	DFBResult ret;
	while (1){
		events->WaitForEvent(events);
		if ((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK) {
			if (windowEvent.type == DWET_KEYDOWN)
				//			return windowEvent.key_symbol;
				return 1;
		}
		else {
			break;
		}
	}
	return 0;
}
int Kb_Get_Key_Sym(void)
{
	DFBWindowEvent windowEvent;
	DFBResult ret;

	uint32_t value = 0;//NULL;
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	//	int temp_seconds = 62;
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	key_pretmvl.tv_sec=ti.tv_sec;
	key_pretmvl.tv_usec=ti.tv_usec;
	tmsec.tv_sec = KEYPAD_LOCK_TIME;
	tmsec.tv_usec=KEYPAD_LOCK_TIME*1000;

	while (1){
		//led_get_brightness(LED_BACKLIGHT,&value);
		if((enter_key_lock == 1))                                           //Manually clearing the Buffer Since Some Events are needed to set Key_released
		{																	//flag
			while((ret = events->HasEvent(events)) == DFB_OK)
			{
				if((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK)
					if(windowEvent.type == DWET_KEYUP)
					{
						key_released = 1;
					}
			}
			enter_key_lock = 0;


		}


		//		else
		//			backlit_flag = 1;
		//		if(backlit_flag == 0)
		events->WaitForEvent(events);
		//events->WaitForEventWithTimeout(events,t.tv_sec,t.tv_usec);
		//events->WaitForEventWithTimeout(events,0,300);
		if ((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK) {

			if (windowEvent.type == DWET_KEYDOWN && (key_released == 1))
				//if (windowEvent.type == DWET_KEYUP)
			{

				ti.tv_sec=time(NULL);
				nowtm =localtime(&(ti.tv_sec));
				//		if(nowtm->tm_sec != temp_seconds)
				//		{
				//			temp_seconds=nowtm->tm_sec;
				//			value++;
				//			if(value == KEYPAD_LOCK_TIME && backlit_flag == 1)
				//			{
				//				backlit_flag = 0;
				//				value = 0;
				//				display_popup("KEYPAD LOCKED");
				//
				//
				//			}
				//		}
				ti.tv_usec=ti.tv_sec*1000;
				CstcLog_printf("%ld:::%ld", key_pretmvl.tv_sec,ti.tv_sec);
				CstcLog_printf("%ld:::%ld", key_pretmvl.tv_usec,ti.tv_usec);
				CstcLog_printf("%ld:::%ld",(ti.tv_sec-key_pretmvl.tv_sec),tmsec.tv_usec);
				if((ti.tv_sec-key_pretmvl.tv_sec)>=tmsec.tv_sec)
				{
					//					events->Reset(events);
					key_pretmvl.tv_sec=ti.tv_sec;
					key_pretmvl.tv_usec=ti.tv_usec;
					CstcLog_printf("KEYPAD LOCKED");
					CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
					if(backlit_flag == 1)
					{
						backlit_flag = 0;
						value = 0;
						//						display_popup("KEYPAD LOCKED");
					}
				}
				else
				{
					CstcLog_printf("KEYPAD SHOULD NOT BE LOCKED");
					key_pretmvl.tv_sec=ti.tv_sec;
					key_pretmvl.tv_usec=ti.tv_usec;
					CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
				}

				if((windowEvent.key_symbol!=KEY_SYM_CURSOR_DOWN)&&(windowEvent.key_symbol!=KEY_SYM_CURSOR_UP))
					key_released =0;
				if(!backlit_flag)
					key_released =0;

				if(backlit_flag )
				{
					//backlit_flag = 1;

					//events->Reset(events);
					if(enter_key_lock == 1)
						return 0;
					return windowEvent.key_symbol;
				}
				if((backlit_flag == 0) && (windowEvent.key_symbol != KEY_SYM_BACKSPACE))
				{
					//led_set_brightness(LED_BACKLIGHT,0);
					//events->Reset(events);
					display_popup("KEYPAD LOCKED");
					//Show_Error_Msg("KEYPAD LOCKED");
					backlit_flag = 0;
					return 0;
				}
				else
				{

					//events->Reset(events);
					display_popup("KEYPAD UNLOCKED");
					//Show_Error_Msg("KEYPAD UNLOCKED");
					backlit_flag = 1;
					standby_mode = 0;
					return 0;
				}
			}
			else if (windowEvent.type == DWET_KEYUP && (key_released == 0))
			{
				key_released = 1;
				events->Reset(events);
				if(standby_mode)
				{
					led_set_brightness(LED_BACKLIGHT,0);
					standby_mode = 0;
				}
			}


			//		return 1;
		}
		//		else {
		//			break;
		//		}
	}
	if(nowtm!=NULL)
		free(nowtm);
	return 0;
}


#if 0
int Kb_Get_Key_Sym(void)
{
	DFBWindowEvent windowEvent;
	DFBResult ret;
	//	int backlit_flag = 1;
	//	uint32_t value = 0;//NULL;
	//	struct timeval	t;
	//	t.tv_sec  = 0;
	//	t.tv_usec = 100;
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl
	while (1){
		//		led_get_brightness(LED_BACKLIGHT,&value);
		//		if(value == 0)
		//		{
		//			backlit_flag = 0;
		//		}
		//		else
		//			backlit_flag = 1;
		ti.tv_sec=time(NULL);
		nowtm =localtime(&(ti.tv_sec));
		ti.tv_usec=ti.tv_sec*1000;
		CstcLog_printf("%ld:::%ld", key_pretmvl.tv_sec,ti.tv_sec);
		CstcLog_printf("%ld:::%ld", key_pretmvl.tv_usec,ti.tv_usec);
		CstcLog_printf("%ld:::%ld",(ti.tv_sec-key_pretmvl.tv_sec),tmsec.tv_usec);
		if((ti.tv_sec-key_pretmvl.tv_sec)>=tmsec.tv_sec)
		{
			events->Reset(events);
			key_pretmvl.tv_sec=ti.tv_sec;
			key_pretmvl.tv_usec=ti.tv_usec;
			CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
		}
		events->WaitForEvent(events);
		//		events->WaitForEventWithTimeout(events,t.tv_sec,t.tv_usec);
		if ((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK) {
			if (windowEvent.type == DWET_KEYDOWN)
			{
				//				if(backlit_flag)
				//				{
				//					backlit_flag = 1;
				return windowEvent.key_symbol;
				//				}
			}
			//		return 1;
		}
		else {
			break;
		}
	}
	return 0;
}
#endif

int Kb_Get_Key_Sym1(void)
{
	DFBWindowEvent windowEvent;
	DFBResult ret;
	while (1){
		events->WaitForEvent(events);
		if ((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK) {
			if (windowEvent.type == DWET_KEYUP)
				return windowEvent.key_symbol;
			//			return 1;
		}
		else {
			break;
		}
	}
	return 0;
}

int Kb_Get_Key_Code(void)
{
	DFBWindowEvent windowEvent;
	DFBResult ret;
	while (1){
		events->WaitForEvent(events);
		if ((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK) {
			if (windowEvent.type == DWET_KEYUP)
				return windowEvent.key_code;
		} else {
			break;
		}
	}

	return 0;
}



int lcd_menu_details(const char * pszTitle, char * schedNo, int font_height, waybill_master_struct waybill_master_struct_var,schedule_master_struct *schedule_master_struct_var, unsigned int count, int select)
{

	int retval = -1;

	int key;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int     i = 0;
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;
	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	int curnt_y =0;
	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	if (select < 0)
	{
		select = 0;
	}

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,16, "*****************************");
			lcd_printf(ALG_CENTER,20, "TRIP DETAILS");
//			curnt_y = lcd_printf(ALG_LEFT,16,"Sch. No. : %s",schedNo);
//			curnt_y = lcd_printf(ALG_LEFT,16,"");

//			lcd_printf_xy(16,150,curnt_y,"Con.Tno. : %s",waybill_master_struct_var.conductor_id);

			curnt_y = 0;

			curnt_y = lcd_printf(ALG_LEFT,16,"V No. : %s",waybill_master_struct_var.vehicle_no);
//			lcd_printf_xy(font_height,180,curnt_y,"Dr.Tno. : %s",waybill_master_struct_var.driver_id);
			lcd_printf_xy(16,170,curnt_y,"Con.Tno. : %s",waybill_master_struct_var.conductor_id);


			lcd_printf(ALG_LEFT,16,"Trip   Origin      Dest        StrtTime  EndTime");
			lcd_printf(ALG_CENTER,16, "*****************************");

			max_lines = (screen_height - current_y - 1) / font_height;

			curnt_y = 0;
			istart = (select / max_lines) * max_lines;

			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					curnt_y = lcd_printf(ALG_LEFT,16," %s",(schedule_master_struct_var+(istart + i))->trip_id);
					lcd_printf_xy(16,40,curnt_y,"%s",(schedule_master_struct_var+(istart + i))->start_bus_stop_code);
					lcd_printf_xy(16,100,curnt_y,"%s",(schedule_master_struct_var+(istart + i))->end_bus_stop_code);
					lcd_printf_xy(16,170,curnt_y,"%s",(schedule_master_struct_var+(istart + i))->start_time);
					lcd_printf_xy(16,240,curnt_y,"%s",(schedule_master_struct_var+(istart + i))->end_time);

				}
				else
				{
					break;
				}
			}

			lcd_printf_ex(ALG_CENTER,16,195,"PRESS CANCEL KEY TO EXIT");
			lcd_flip();

			LOOP:
			if (Kb_Hit())
			{
				while(1)
				{
					key=0;
					if((key = Kb_Get_Key_Sym())>1)
					{
						break;
					}
					else
						key=0;
				}
				switch((unsigned int)key)
				{
				case KEY_SYM_CURSOR_UP:
					select -= 3;//4;
					if (select < 0)
					{
						select = count -1;
					}
					break;
				case KEY_SYM_CURSOR_DOWN:
					select += 3;//4;
					if (select >= count)
					{
						select = 0;
					}
					break;
				case KEY_SYM_ESCAPE:
					select = -1;
					bLoop = 0;
					break;
				default:
					break;
				}
			}
			else
			{
				goto LOOP;
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	retval = select;
	lcd_clean();
	CstcLog_printf( "End of  lcd_menu_details select = %d retval %d", select, retval);

	return retval;

}

#if 1 //original 09/08/2014
int lcd_menu_stop_list(const char * pszTitle, int font_height, current_selected_trip_stops_struct *lcd_current_selected_trip_stops_struct_var,unsigned int count, int select, int stopFlag,current_selected_trip_stops_struct *select_stg_current_selected_trip_stops_struct_var, int stageRowCount, int fromcount)
{
	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0,last_stage_flag = 0;
	int isStagePresent =0;
	int curnt_y =0,current_stage_index=0;
	int prevKey = 0;
	//	char *tempName = NULL;
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=3000000; //micrseconds
	unsigned int ChrgCnt = 1;//0;
	int maxSelect = 0;
	//	char * chrstage = NULL;
	char **tempStage;
	char **tempStop;
	char* sql = NULL;

	int j  =0, sql_len = 280;
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;
	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	//	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl
	CstcLog_printf("In lcd_menu_stop_list");
	sql=(char*)malloc(sql_len);
	memset(sql,0,sql_len);
//	sprintf(sql,"route_master where cast(bus_stop_seq_no as number) >= '%d' and cast(bus_stop_seq_no as number) <= '%d' and (sub_stage = 'S') and route_id = '%s' and schedule_id = '%s'",atoi(selected_trip_info_struct_var.current_selected_start_stop_seq_no_qstr), atoi(selected_trip_info_struct_var.end_bus_stop_seq_no_qstr),selected_trip_info_struct_var.route_id_qstr, selected_trip_info_struct_var.schedule_id);
	sprintf(sql,"route_master where cast(bus_stop_seq_no as number) >= '%d' and cast(bus_stop_seq_no as number) <= '%d' and (sub_stage = 'S') and cast(route_id as number) = '%s'",atoi(selected_trip_info_struct_var.current_selected_start_stop_seq_no_qstr), atoi(selected_trip_info_struct_var.end_bus_stop_seq_no_qstr),selected_trip_info_struct_var.route_id_qstr);

	isStagePresent = DbInterface_Get_Row_Count(sql, strlen(sql),0);

	if(isStagePresent > 0)
		isStagePresent = 1;
	else
		isStagePresent = 0;
	CstcLog_printf("1. isStagePresent received : %d",isStagePresent);

	memset(sql,0,sql_len);

	if(!all_flags_struct_var.penalty_ticket_flag)
	{
//		sprintf(sql,"route_master where cast(bus_stop_seq_no as number) >= '%d' and cast(bus_stop_seq_no as number) < '%d' and (sub_stage = 'S') and route_id = '%s' and  schedule_id = '%s'",atoi(selected_trip_info_struct_var.start_stop_seq_no_qstr),atoi(selected_trip_info_struct_var.current_selected_start_stop_seq_no_qstr), selected_trip_info_struct_var.route_id_qstr,selected_trip_info_struct_var.schedule_id);
		sprintf(sql,"route_master where cast(bus_stop_seq_no as number) >= '%d' and cast(bus_stop_seq_no as number) < '%d' and (sub_stage = 'S') and cast(route_id as number)= '%s'",atoi(selected_trip_info_struct_var.start_stop_seq_no_qstr),atoi(selected_trip_info_struct_var.current_selected_start_stop_seq_no_qstr), selected_trip_info_struct_var.route_id_qstr);

		ChrgCnt = DbInterface_Get_Row_Count(sql, strlen(sql),0); //so charge count doesnt reset to 0

		ChrgCnt++; //count must start from one so +1 to show it correctly even after stage closed

		if(atoi(selected_trip_info_struct_var.start_stop_seq_no_qstr) != atoi(selected_trip_info_struct_var.current_selected_start_stop_seq_no_qstr))
		{ //this means first stage was closed
			memset(sql,0,sql_len);
//			sprintf(sql,"route_master where cast(bus_stop_seq_no as number) = '%d' and (sub_stage = 'S') and route_id = '%s' and schedule_id = '%s'",atoi(selected_trip_info_struct_var.start_stop_seq_no_qstr),selected_trip_info_struct_var.route_id_qstr, selected_trip_info_struct_var.schedule_id);
			sprintf(sql,"route_master where cast(bus_stop_seq_no as number) = '%d' and (sub_stage = 'S') and cast(route_id as number) = '%s'",atoi(selected_trip_info_struct_var.start_stop_seq_no_qstr),selected_trip_info_struct_var.route_id_qstr);


			if(DbInterface_Get_Row_Count(sql, strlen(sql),0) == 0) //first stop was not stage
				ChrgCnt++; //add to count so that current stage onwards correct stage number is displayed
		}
	}

	CstcLog_printf("isStagePresent %d count %d", isStagePresent, count);


	if(!all_flags_struct_var.penalty_ticket_flag)
	{
		if(stageRowCount > 0)
		{
			if(select_stg_current_selected_trip_stops_struct_var!=NULL)
			{
				for(j = 0; j < count; j++)
				{
					if(atoi((select_stg_current_selected_trip_stops_struct_var+0)->stop_seq_no_QStrLst)== atoi((lcd_current_selected_trip_stops_struct_var+j)->stop_seq_no_QStrLst))
					{
						maxSelect = j;
						break;
					}
				}
			}
		}
	}

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}


	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);
	CstcLog_printf("Total Stops = %d", count);

	tempStage = (char **)malloc(count * sizeof(char *)); // Allocate row pointers
	tempStage[0] = (char *)malloc(count * 3* sizeof(char)); // Allocate all the elements

	for (j = 0; j <count ; j++)
	{
		tempStage[j] = tempStage[0] + (j * 3);
	}

	for(j =0;j < count; j++)
	{
		memset(tempStage[j],0,3); //charge stage list

		sprintf(tempStage[j], "%d", ChrgCnt);
		CstcLog_printf("tempStage[%d]= %s ChrgCnt = %d",j,tempStage[j], ChrgCnt);

		if((lcd_current_selected_trip_stops_struct_var+j)->fare_stage_QStrLst=='Y')
		{
			ChrgCnt++;
		}

		if(j == 0)//first stop is not stage
		{
			if((lcd_current_selected_trip_stops_struct_var+0)->fare_stage_QStrLst!='Y')
				ChrgCnt++;
		}
	}


	tempStop = (char **)malloc(count * sizeof(char *)); // Allocate row pointers
	tempStop[0] = (char *)malloc(count * (NAME_ENG_LEN+1)* sizeof(char)); // Allocate all the elements

	for (j = 0; j <count ; j++)
	{
		tempStop[j] = tempStop[0] + (j * (NAME_ENG_LEN+1));
	}

	for(j =0;j < count; j++)
	{
		memset(tempStop[j],0,NAME_ENG_LEN+1);
		memcpy(tempStop[j],(lcd_current_selected_trip_stops_struct_var+j)->stop_name_english_QStrLst,15);
	}

	//	prevKey = select;
	//	ti.tv_usec=time(NULL);
	//	ti.tv_sec=time(NULL);
	//	gettimeofday(&ti, NULL);
	//
	//	total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

	CstcLog_printf("0.5. select received : %d",select);
	// This is the input before list display
	/*****************/
	prevKey = select-1;
	ti.tv_usec=time(NULL);
	ti.tv_sec=time(NULL);
	gettimeofday(&ti, NULL);

	total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;
	pretmvl.tv_sec=ti.tv_sec;
	pretmvl.tv_usec=ti.tv_usec;
	pretmvl_micro_sec = total_micro_seconds;
	CstcLog_printf("4. select received : %d",select);
	/*****************/
	//Here "select" is stage number so get its seq no.
	last_stage_flag = 0;
	if(isStagePresent==1)
	{
		CstcLog_printf("STAGE IS PRESENT!!!!");

		for(j =0;j < count; j++)
		{
			if(atoi(tempStage[j])==(select))
			{
				select = j;
				CstcLog_printf("1.select = %d",select);
				break;
			}
			else if(atoi(tempStage[(count-1)])==(select-1))
			{
				select = count;
				CstcLog_printf("3.select = %d",select);
				break;
			}
			else if((j+1) == count)
			{
				select = 0;
				CstcLog_printf("4.select = %d",select);
				break;
			}
			//
			//			if((atoi(tempStage[count-1]))==(atoi(tempStage[j])))
			//			{
			//				if((atoi(tempStage[j])+1) == select )
			//				{
			//					select = j;  // When Last stage is selected in list
			//					CstcLog_printf("1.select = %d",select);
			//					last_stage_flag = 1;  //very imp
			//					break;
			//				}
			//
			//			}

		}
		//		if(j == count)
		//		{
		//			select = 1;
		//		}
		CstcLog_printf("2.5. select received : %d",select);
	}
	else
		select =0;


	CstcLog_printf("2. select received : %d",select);

	//Now "select" is sequence number so verify as seq no.
	if(select > (count-1)) // if select is greater than the greatest stg count
		select = count-1;
	else if(select <= 0)
		select = 0;
	else //if(last_stage_flag == 0)
		select -= 1;
	//	last_stage_flag =0;
	CstcLog_printf("3. select received : %d",select);

	if(all_flags_struct_var.penalty_ticket_flag)
	{
		/*******current stage index********/
		for(j=0;j<(selected_trip_info_struct_var.totalfetchedstops);j++)
		{
			if(atoi((current_selected_trip_stops_struct_var+0)->stop_seq_no_QStrLst)==atoi((line_checking_current_selected_trip_stops_struct_var+j)->stop_seq_no_QStrLst))
			{
				current_stage_index = j;
				CstcLog_printf("current_stage_index = %d",current_stage_index);
				break;
			}
		}
		/*********************************/
	}

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		//		chrstage = (char *)malloc(3*sizeof(char));
		//		memset(chrstage,0,3);

		CstcLog_printf("In else lcd_menu_stop_list");

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			//			lcd_printf(ALG_CENTER,20, "************************");
			//			lcd_printf(ALG_CENTER,24,"CSTC");
			lcd_printf(ALG_CENTER,20, pszTitle);
			lcd_printf(ALG_LEFT,16, "Route Number: %s", selected_trip_info_struct_var.route_no_qstr);
			if(!stopFlag) // from stop
				lcd_printf(ALG_LEFT,16, "From - %s",(lcd_current_selected_trip_stops_struct_var+select)->stop_name_english_QStrLst);
			else // till stop
				lcd_printf(ALG_LEFT,16, "To - %s",(lcd_current_selected_trip_stops_struct_var+select)->stop_name_english_QStrLst);
			//			lcd_printf(ALG_CENTER,20, "From Bus Stop - English");
			lcd_printf(ALG_LEFT,16,"ChrStg  Seq No    Stop Name            Stage");
			lcd_printf(ALG_CENTER,16,"----------------------------------------------------");

			max_lines = (screen_height - current_y - 1) / font_height;

			istart = (select / max_lines) * max_lines;

			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height +1);

						curnt_y = lcd_printf(ALG_LEFT,16," %s",tempStage[(istart + i)]);
						lcd_printf_xy(16,60,curnt_y,"%d",(atoi((lcd_current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst) - temp_offset));
						//						lcd_printf_xy(16,60,curnt_y,"%s",(current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst);
						//						CstcLog_printf("%d-%d=%d",atoi((current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst),temp_offset,(atoi((current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst) - temp_offset));
						lcd_printf_xy(16,110,curnt_y,"%s",tempStop[istart+i]);
						lcd_printf_xy(16,280,curnt_y,"%s",(lcd_current_selected_trip_stops_struct_var+(istart + i))->sub_stage_QStrLst);
						lcd_set_font_color(oldcolor);
					}
					else
					{
						//						lcd_printf(ALG_LEFT,16," %s           %s          %s             %s",tempStage[(istart + i)],(current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst, tempStop[istart+i], (current_selected_trip_stops_struct_var+(istart + i))->sub_stage_QStrLst);
						curnt_y = lcd_printf(ALG_LEFT,16," %s",tempStage[(istart + i)]);
						lcd_printf_xy(16,60,curnt_y,"%d",(atoi((lcd_current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst) - temp_offset));
						//						lcd_printf_xy(16,60,curnt_y,"%s",(current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst);
						//						CstcLog_printf("%d-%d=%d",atoi((current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst),temp_offset,(atoi((current_selected_trip_stops_struct_var+(istart + i))->stop_seq_no_QStrLst) - temp_offset));
						lcd_printf_xy(16,110,curnt_y,"%s",tempStop[istart+i]);
						lcd_printf_xy(16,280,curnt_y,"%s",(lcd_current_selected_trip_stops_struct_var+(istart + i))->sub_stage_QStrLst);

					}
				}
				else
					break;
			}
			lcd_flip();

			LOOP:
			if (Kb_Hit())
			{
				while(1)
				{
					key=0;
					if((key = Kb_Get_Key_Sym())>1)
					{
						break;
					}
					else
						key=0;
				}
				switch((unsigned int)key)
				{
				case KEY_SYM_CURSOR_UP:
					select --;
					CstcLog_printf("UP Select ::: %d", select);

					if (select < 0)
					{
						select = count -1;
					}
					prevKey = 0;
					break;
				case KEY_SYM_CURSOR_DOWN:
					select++;
					CstcLog_printf("DN Select ::: %d", select);
					if (select >= count)
					{
						select = 0;
					}
					prevKey = 0;
					break;
				case KEY_SYM_ESCAPE:
					select = -2;
					bLoop = 0;
					prevKey = 0;
					break;
				case KEY_SYM_SHIFT:
					select = -1;
					bLoop = 0;
					prevKey = 0;
					break;
				case KEY_SYM_ENTER:
					bLoop = 0;
					prevKey = 0;
					if(!all_flags_struct_var.penalty_ticket_flag)
					{
						if(!stopFlag)
						{
							if(stageRowCount > 0)
							{
								CstcLog_printf("select %d, maxSelect-1 %d", select,maxSelect-1);
								if(select <= maxSelect-1)
									break;
								else
								{
									CstcLog_printf("goto LOOOOP");
									key =0;
									select = 0;
									lcd_clean();
									lcd_printf_ex(ALG_CENTER,20,70,"FROM STOP CANNOT BE GREATER");
									lcd_printf_ex(ALG_CENTER,20,90,"THAN NEXT STAGE");
									lcd_flip();
									sleep(1);
									beep(2800,1200);
									continue;
								}
							}
						}
					}
					else if(all_flags_struct_var.penalty_ticket_flag)
					{
						CstcLog_printf("Penalty ENTER pressed");
						CstcLog_printf("select %d", select);
						CstcLog_printf("stop_seq_no_QStrLst %s", (current_selected_trip_stops_struct_var+0)->stop_seq_no_QStrLst);
						CstcLog_printf("current_stage_index %d", current_stage_index);

						if(!stopFlag)
						{
							//from stop
							CstcLog_printf("--------from stop------");

							if(select >= current_stage_index)
							{ //if selected is >= current stage show error
								CstcLog_printf(">=");
								key =0;
								select = -2;
								lcd_clean();
								lcd_printf_ex(ALG_CENTER,20,70,"FROM STOP SHOULD BE LESS");
								lcd_printf_ex(ALG_CENTER,20,90,"THAN CURRENT STAGE");
								lcd_flip();
								sleep(1);
								beep(2800,1200);
							}
						}
						else if(stopFlag)
						{
							//till stop
							CstcLog_printf("--------till stop------");
							CstcLog_printf("select %d", select);
							CstcLog_printf("current_stage_index %d", current_stage_index);

							if(select < current_stage_index)
							{ //if selected is <= current stage show error
								CstcLog_printf("<=");

								key =0;
								select = -2;
								lcd_clean();
								lcd_printf_ex(ALG_CENTER,20,70,"TO STOP SHOULD BE GREATER");
								lcd_printf_ex(ALG_CENTER,20,90,"THAN CURRENT STAGE");
								lcd_flip();
								sleep(1);
								beep(2800,1200);
							}
						}
					}
					break;
				default:
					if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
					{
						//						ti.tv_sec=time(NULL);
						//						nowtm =localtime(&(ti.tv_sec));
						//						ti.tv_usec=ti.tv_sec*1000;

						ti.tv_usec=time(NULL);
						ti.tv_sec=time(NULL);
						gettimeofday(&ti, NULL);

						total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_usec,ti.tv_usec);
						//						CstcLog_printf("%ld:::%ld",(ti.tv_sec-pretmvl.tv_sec),tmsec.tv_usec);

						//						CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
						//						CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
						//						CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);

						//						if((ti.tv_sec-pretmvl.tv_sec)>=tmsec.tv_sec)
						CstcLog_printf("Select => %d/10 = %ld", select,(select/10));
						if(((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))||((select/10)>0))
						{
							prevKey = 0;
							pretmvl.tv_sec=ti.tv_sec;
							pretmvl.tv_usec=ti.tv_usec;
							pretmvl_micro_sec = total_micro_seconds;
							//							CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
						}
						select = (prevKey * 10) + (key & 0x0F);
						CstcLog_printf("Select ::: %d - %d", select,atoi((lcd_current_selected_trip_stops_struct_var+0)->stop_seq_no_QStrLst));
						prevKey = select;

#if 0 // to stop should go to the pressed no. STAGE (not seq no)
						if(isStagePresent==0)
						{
							for(i=0;i<count;i++)
							{
								CstcLog_printf("i seq no ::: %d ", atoi((lcd_current_selected_trip_stops_struct_var+i)->stop_seq_no_QStrLst));

								if(atoi((lcd_current_selected_trip_stops_struct_var+i)->stop_seq_no_QStrLst)==select)
								{
									CstcLog_printf("Selected seq no ::: %d ", atoi((lcd_current_selected_trip_stops_struct_var+i)->stop_seq_no_QStrLst));
									select = i;
									CstcLog_printf("select = i ::: %d = %d", select,i);
									break;
								}
								else if(select < atoi((lcd_current_selected_trip_stops_struct_var+0)->stop_seq_no_QStrLst))
									select = 0;

								//							else
								//								select = 0;
							}
						}
#endif
						if(isStagePresent==1)// stage is present
						{
							CstcLog_printf("Checking for charge count....");
							if ((select > atoi(tempStage[count-1])))//||(select < atoi(tempStage[0])))
							{
								Show_Error_Msg("Invalid Stage Number");
								beep(2800,1500);
								usleep(100);
							}

							for(j =0;j < count; j++)
							{
								//								if(atoi(tempStage[j])==select)
								//								{
								//									select = j;
								//									CstcLog_printf("select chr stg = %d",select);
								//									break;
								//								}
								//								else if(select > atoi(tempStage[count-1])) // if select is greater than the greatest stg count
								//									select = 0;
								//								else if(select < atoi(tempStage[0]))
								//									select = 0;


								if(atoi(tempStage[j])==(select+1))  //  added 1 for last stop of the selected stage
								{
									select = j;
									CstcLog_printf("1.select chr stg = %d",select);
									break;
								}
								else if(atoi(tempStage[(count-1)])<=(select))
								{
									select = count;
									CstcLog_printf("3.select chr stg = %d",select);
									break;
								}
								else if(select < atoi(tempStage[0])) //if((j+1) == count)
								{
									select = 0;
									CstcLog_printf("4.select chr stg = %d",select);
									break;
								}
							}
						}
						//////////////////////////////////////

						if (select >= count)
						{
							CstcLog_printf("making select zero..");
							//							select = 0; // commented
							prevKey = 0;
						}
						if(select > (count-1)) // if select is greater than the greatest stg count
							select = count-1;
						else if(select <= 0)
							select = 0;
						else //if(last_stage_flag == 0)
							select -= 1;
						CstcLog_printf("Selected = %d", key);
						break;
					}
					else
					{
						prevKey = 0;
						goto LOOP;
					}
				}
			}
			else
			{
				goto LOOP;
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}


	if(select >= 0)
	{
		if(stopFlag)
		{
			if (select < fromcount)
				select = fromcount;
		}
	}


	retval = select;
	CstcLog_printf("retval %d select %d",retval, select);
	lcd_clean();
	//	for (j = 0; j <count ; j++)
	//	{
	//		if(tempStage[j]!=NULL)
	//			free(tempStage[j]);
	//	}
	//	if(tempStage!=NULL)
	free(tempStage[0]);
	free(tempStage);

	free(tempStop[0]);
	free(tempStop);

	return retval;
}
#endif
void lcd_printf_xy(unsigned int height, unsigned x,unsigned y, const char * pszFmt,...){

	char textbuf[2048];

	//	va_list arg;
	//	va_start(arg, pszFmt);
	//	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);
	//	va_end (arg);
	//	current_y = y;


	char *new_line =NULL;
	va_list arg;

	va_start(arg, pszFmt);
	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);
	va_end (arg);
	current_y = y;
	new_line = (char *)malloc(100);
	memset(new_line,0,100);
	return_substring((char *)textbuf,'\n',new_line,100);

	lcd_printf_x(height,x,textbuf);

	if(strlen(new_line))
	{

		if((height!=17)&&(height!=21)&&(height!=27)&&(height!=31)&&(height!=34)&&(height!=45)&&(height!=60))
		{
			lcd_printf_x(16, x, new_line);
		}
		else
		{
			lcd_printf_x(16, x, new_line);
		}
	}
	if(new_line != NULL)
		free(new_line);



}
void popup_printf_xy(unsigned int height, unsigned x,unsigned y, const char * pszFmt,...){

	char textbuf[2048];

	va_list arg;
	va_start(arg, pszFmt);
	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);
	va_end (arg);
	current_y = y;
	//	lcd_printf(alg, height, textbuf);
	popup_printf_x(height,x,textbuf);
}


void lcd_printf_x( int font_height,unsigned x, const char * pszFmt,...){
	int width, height;
	//	int font_height;
	IDirectFBSurface * sub_surface = NULL;
	IDirectFBSurface * store_surface = NULL;
	DFBRectangle rect;
	DFBSurfaceDescription	surfdesc;
	IDirectFBFont * font = NULL;

	char textbuf[2048];
	const char *utf8text;
	//	const char * pnewline = NULL;
	int str_length;//, temp_width;
	va_list arg;



	va_start(arg, pszFmt);
	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);
	va_end (arg);
	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;
	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;
	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}
	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &width, &height);
	main_surface->SetColor(main_surface,
			0xFF & (font_color>> 16),
			0xFF & (font_color >> 8),
			0xFF & (font_color >> 0),
			0xFF & (font_color>>24));

	if (current_y + font_height > height){
		rect.x = 0;
		rect.y = current_y + font_height - height;
		rect.w = width;
		rect.h = current_y - rect.y;
		main_surface->GetSubSurface (main_surface, &rect, &sub_surface);

		surfdesc.flags = DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT;
		surfdesc.caps  = DSCAPS_SYSTEMONLY;
		surfdesc.width = width;
		surfdesc.height= height;
		dfb->CreateSurface(dfb, &surfdesc, &store_surface);
		store_surface->SetBlittingFlags(store_surface, 	DSBLIT_BLEND_ALPHACHANNEL);
		store_surface->Blit(store_surface, sub_surface, NULL, 0, 0);
		lcd_clean();
		main_surface->SetBlittingFlags(main_surface, 	DSBLIT_BLEND_ALPHACHANNEL);
		rect.y = 0;
		main_surface->Blit(main_surface, store_surface, &rect, 0, 0);

		store_surface->Release(store_surface);
		sub_surface->Release(sub_surface);
		current_y = rect.h;

	}

	//	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	//		font->GetStringBreak(font, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//	else if(font_height==45)
	//	font_45->GetStringBreak(font_45, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//	else if(font_height==60)
	//	font_60->GetStringBreak(font_60, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//if((font_height!=45)&&(font_height!=60))
	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		str_length = strlen(textbuf);
		utf8text = string_covert(textbuf, str_length);
		//		if (ALG_CENTER == alg)
		//			main_surface->DrawString(main_surface, utf8text, string_len(utf8text), width/2, current_y, DSTF_TOPCENTER);
		//		else if (ALG_LEFT == alg)
		//			main_surface->DrawString(main_surface, utf8text, string_len(utf8text), 2, current_y, DSTF_TOPLEFT);
		//		else
		main_surface->DrawString(main_surface, utf8text, string_len(utf8text), x, current_y, DSTF_TOPLEFT);
	}
	else
	{
		//		if (ALG_CENTER == alg)
		//			main_surface->DrawString(main_surface, textbuf, string_len(textbuf), width/2, current_y, DSTF_TOPCENTER);
		//		else if (ALG_LEFT == alg)
		//			main_surface->DrawString(main_surface, textbuf, string_len(textbuf), 2, current_y, DSTF_TOPLEFT);
		//		else
		main_surface->DrawString(main_surface, textbuf, string_len(textbuf), x, current_y, DSTF_TOPLEFT);
	}

	current_y = current_y + font_height + 1;


	//	if (NULL != pnewline)
	//	{
	//		lcd_printf_x(x,16, pnewline);
	//	}

}
//----------------------------------------------------------------------------------------------------------------
void popup_printf_x( int font_height,unsigned x, const char * pszFmt,...){
	int width, height;
	//	int font_height;
	IDirectFBSurface * sub_surface = NULL;
	IDirectFBSurface * store_surface = NULL;
	DFBRectangle rect;
	DFBSurfaceDescription	surfdesc;
	IDirectFBFont * font = NULL;

	char textbuf[2048];
	const char *utf8text;
	const char * pnewline = NULL;
	int str_length, temp_width;
	va_list arg;



	va_start(arg, pszFmt);
	vsnprintf(textbuf, sizeof(textbuf), pszFmt, arg);
	va_end (arg);
	switch(font_height)
	{
	case 16:
		popup_surface->SetFont(popup_surface, font_16);
		break;
	case 17:
		popup_surface->SetFont(popup_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		popup_surface->SetFont(popup_surface, font_20);
		break;
	case 21:
		popup_surface->SetFont(popup_surface, font_21);
		break;
	case 24:
		popup_surface->SetFont(popup_surface, font_24);
		break;
	case 26:
		popup_surface->SetFont(popup_surface, font_26);
		break;
	case 27:
		popup_surface->SetFont(popup_surface, font_27);
		break;
	case 28:
		popup_surface->SetFont(popup_surface, font_28);
		break;
	case 30:
		popup_surface->SetFont(popup_surface, font_30);
		break;
	case 31:
		popup_surface->SetFont(popup_surface, font_31);
		break;
	case 32:
		popup_surface->SetFont(popup_surface, font_32);
		break;
	case 34:
		popup_surface->SetFont(popup_surface, font_34);
		break;
	case 35:
		popup_surface->SetFont(popup_surface, font_35);
		break;
	case 40:
		popup_surface->SetFont(popup_surface, font_40);
		break;
	case 45:
		popup_surface->SetFont(popup_surface, font_45);
		break;
	case 60:
		popup_surface->SetFont(popup_surface, font_60);
		break;
	default:
		break;
	}
	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		popup_surface->GetFont(popup_surface, &font);
		font->GetHeight(font, &font_height);
	}
	popup_surface->GetSize(popup_surface, &width, &height);
	popup_surface->SetColor(popup_surface,
			0xFF & (font_color>> 16),
			0xFF & (font_color >> 8),
			0xFF & (font_color >> 0),
			0xFF & (font_color>>24));

	if (current_y + font_height > height){
		rect.x = 0;
		rect.y = current_y + font_height - height;
		rect.w = width;
		rect.h = current_y - rect.y;
		popup_surface->GetSubSurface (popup_surface, &rect, &sub_surface);

		surfdesc.flags = DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT;
		surfdesc.caps  = DSCAPS_SYSTEMONLY;
		surfdesc.width = width;
		surfdesc.height= height;
		dfb->CreateSurface(dfb, &surfdesc, &store_surface);
		store_surface->SetBlittingFlags(store_surface, 	DSBLIT_BLEND_ALPHACHANNEL);
		store_surface->Blit(store_surface, sub_surface, NULL, 0, 0);
		lcd_clean();
		popup_surface->SetBlittingFlags(popup_surface, 	DSBLIT_BLEND_ALPHACHANNEL);
		rect.y = 0;
		popup_surface->Blit(popup_surface, store_surface, &rect, 0, 0);

		store_surface->Release(store_surface);
		sub_surface->Release(sub_surface);
		current_y = rect.h;

	}

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
		font->GetStringBreak(font, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//	else if(font_height==45)
	//	font_45->GetStringBreak(font_45, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//	else if(font_height==60)
	//	font_60->GetStringBreak(font_60, textbuf, strlen(textbuf), width -2, &temp_width, &str_length, &pnewline);
	//if((font_height!=45)&&(font_height!=60))
	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		utf8text = string_covert(textbuf, str_length);
		//		if (ALG_CENTER == alg)
		//			main_surface->DrawString(main_surface, utf8text, string_len(utf8text), width/2, current_y, DSTF_TOPCENTER);
		//		else if (ALG_LEFT == alg)
		//			main_surface->DrawString(main_surface, utf8text, string_len(utf8text), 2, current_y, DSTF_TOPLEFT);
		//		else
		popup_surface->DrawString(popup_surface, utf8text, string_len(utf8text), x, current_y, DSTF_TOPLEFT);
	}
	else
	{
		//		if (ALG_CENTER == alg)
		//			main_surface->DrawString(main_surface, textbuf, string_len(textbuf), width/2, current_y, DSTF_TOPCENTER);
		//		else if (ALG_LEFT == alg)
		//			main_surface->DrawString(main_surface, textbuf, string_len(textbuf), 2, current_y, DSTF_TOPLEFT);
		//		else
		popup_surface->DrawString(popup_surface, textbuf, string_len(textbuf), x, current_y, DSTF_TOPLEFT);
	}

	current_y = current_y + font_height + 1;


	if (NULL != pnewline)
	{
		lcd_printf_x(x,16, pnewline);
	}
}



#if 1
int lcd_all_trip_status_report(const char * pszTitle, char schedNo[][SCHEDULE_NO_LEN+1],char trip_no[][TRIP_NO_LEN+1],char shift_no[][SHIFT_ID_LEN+1],char ticket_count[][TICKET_NO_LEN+1],char pass_cnt[][PASSG_COUNT_LEN+1],char pass_amt[][AMOUNT_LEN+1], int font_height,unsigned int count, int select,char * grandTotal,int endflag,char toll_amt[][AMOUNT_LEN+1])
{

	int retval = -1;

	int key;
	int bLoop;
	IDirectFBFont * font = NULL;
	int prevKey = 0, last_indx=0, last_flag=0;
	unsigned int     i = 0;
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;
	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	int curnt_y =0;
	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	CstcLog_printf( "In lcd_all_trip_status_report...........");
	CstcLog_printf( "Count = %d",count);
	CstcLog_printf( "pass_cnt = %s",pass_cnt[0]);
	CstcLog_printf( "pass_cnt = %s",pass_cnt[1]);
	CstcLog_printf( "pass_cnt = %s",pass_cnt[2]);
	CstcLog_printf( "pass_cnt = %s",pass_cnt[3]);





	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	//	main_surface->GetFont(main_surface, &font);
	//	font->GetHeight(font, &font_height);

	CstcLog_printf( "Count = %d, Select = %d",count,select);
	if (select < 0)
	{
		select = 0;
	}
	CstcLog_printf( "select = %d",select);

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		//		CstcLog_printf( "In lcd_menu else");
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height,pszTitle);
//			CstcLog_printf( "select[%d]:Toll_amt[%s] ",select,toll_amt[select]);
//			CstcLog_printf( "toll_amt[0]=%s",toll_amt);

//			curnt_y = lcd_printf(ALG_LEFT,16,"Toll Amt : %s",toll_amt[select]);
//			curnt_y = lcd_printf(ALG_LEFT,16,"");
//			lcd_printf_xy(font_height,220,curnt_y,"Shift No : %s",shift_no[select]);
			//
			//
			//			curnt_y = lcd_printf(ALG_LEFT,16,"V No.: %s",waybill_master_struct_var.vehicle_no);
			//			lcd_printf_xy(font_height,160,curnt_y,"DrTno.: %s",waybill_master_struct_var.driver_id);
			//

			lcd_printf(ALG_CENTER,16, "*****************************");
			lcd_printf(ALG_LEFT,16," Shift  Trip No.  Tkt Cnt   Px Cnt  Toll   Amount");


			max_lines = (screen_height - current_y - 1) / font_height;
			CstcLog_printf("max_lines= %d font_height= %d",max_lines,font_height);
			//			istart = (select / max_lines) * max_lines;
			istart = select;
			CstcLog_printf( "istart = %d ,max_lines = %d",istart,max_lines);
			for (i = 0;  i < max_lines; i++)
			{
				CstcLog_printf( "-**************- max_lines i = %d -************-", i);
				if (istart + i < count)
				{
					curnt_y = lcd_printf(ALG_LEFT,16,"   %s",shift_no[istart + i]);
					lcd_printf_xy(font_height,60,curnt_y,"%s",trip_no[istart + i]);
					lcd_printf_xy(font_height,115,curnt_y,"%s",ticket_count[istart + i]);
					lcd_printf_xy(font_height,170,curnt_y,"%s",pass_cnt[istart + i]);
					lcd_printf_xy(font_height,225,curnt_y,"%s",toll_amt[istart + i]);
					lcd_printf_xy(font_height,260,curnt_y,"%s",pass_amt[istart + i]);
					last_indx =  istart+i;
					if((strcmp(shift_no[istart + i],shift_no[istart + i+1])!=0)||(strcmp(schedNo[istart + i],schedNo[istart + i+1])!=0))
						break;

				}

				else
				{
					break;
				}
			}

			if((last_indx+1) == count)
				last_flag = 1;

			//		if(endflag==1)
			//			lcd_printf_xy(font_height,100,200,"GRAND TOTAL :  %s",grandTotal);
			//			lcd_printf_xy(font_height,100,190,"GRAND TOTAL :  %s",grandTotal);
//			lcd_set_font_color(COLOR_RED);
			lcd_printf_ex(ALG_CENTER,18,180,"NET TOTAL : Rs.  %s/-",grandTotal);
//			lcd_set_font_color(COLOR_BLACK);

			if(!last_flag)
				lcd_printf_xy(16,90,200,"PRESS ENTER TO PROCEED");
			else if(last_flag)
				lcd_printf_xy(16,90,200,"PRESS CANCEL TO EXIT");

			lcd_flip();

			LOOP:
			if (Kb_Hit())
			{
				//				key = Kb_Get_Key_Sym();
				while(1)
				{
					key=0;
					if((key = Kb_Get_Key_Sym())>1)
					{
						break;
					}
					else
						key=0;
				}
				//				CstcLog_printf("key:%02x\r\n", key);
				switch((unsigned int)key)
				{
				case KEY_SYM_ESCAPE:
					select = -1;
					bLoop = 0;
					prevKey = 0;
					break;
				case KEY_SYM_ENTER:
					if((last_indx+1)==count)
						break;
					select =last_indx+1;

					CstcLog_printf("OUT OF FORRRRRRRR bLoop %d", bLoop);
					break;
				default:
					break;
				}
			}
			else
			{
				goto LOOP;
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	//	CstcLog_printf( "End of  lcd_menu");
	retval = select;
	//	lcd_clean();
	return retval;

}
#endif



int lcd_menu_trips(const char * pszTitle, int font_height, schedule_master_struct *full_schedule_master_struct_var, unsigned int count, int select)
{
	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0;
	int prevKey = 0;
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=500000; //micrseconds
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;

	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	CstcLog_printf( "In lcd_menu_TC");

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	if (select < 0)
	{
		select = 0;
	}

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height, pszTitle);
			lcd_printf(ALG_CENTER,20, "************************");

			max_lines = (screen_height - current_y - 1) / font_height;


			istart = (select / max_lines) * max_lines;
			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height +1);

						lcd_printf(ALG_LEFT,font_height,"%d. %s ", istart+i+1, (full_schedule_master_struct_var+(istart + i))->schedule_no);
						lcd_set_font_color(oldcolor);
					}
					else
						lcd_printf(ALG_LEFT,font_height,"%d. %s ", istart+i+1, (full_schedule_master_struct_var+(istart + i))->schedule_no);

				}
				else
					break;
			}
			lcd_flip();

			LOOP:
			while(1)
			{
				key=0;
				if((key = Kb_Get_Key_Sym())>1)
				{
					break;
				}
				else
					key=0;
			}
			//				CstcLog_printf("key:%02x\r\n", key);
			switch((unsigned int)key)
			{
			case KEY_SYM_CURSOR_UP:
				select --;
				if (select < 0)
				{
					select = count -1;
				}
				prevKey = 0;
				break;
			case KEY_SYM_CURSOR_DOWN:
				select ++;
				if (select >= count)
				{
					select = 0;

				}
				prevKey = 0;
				break;
			case KEY_SYM_ESCAPE:
				select = -1;
				prevKey = 0;
				bLoop = 0;
				break;
			case KEY_SYM_ENTER:
				bLoop = 0;
				prevKey = 0;
				break;
			default:
				if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
				{
					//						ti.tv_sec=time(NULL);
					//						nowtm =localtime(&(ti.tv_sec));
					//						ti.tv_usec=ti.tv_sec*1000;

					ti.tv_usec=time(NULL);
					ti.tv_sec=time(NULL);
					gettimeofday(&ti, NULL);

					total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

					CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
					CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
					CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);

					if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))

					{
						prevKey = 0;
						pretmvl.tv_sec=ti.tv_sec;
						pretmvl.tv_usec=ti.tv_usec;
						pretmvl_micro_sec = total_micro_seconds;
						CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
					}
					select = (prevKey * 10) + (key & 0x0F) - 1;
					prevKey = select + 1;
					if (select >= count)
					{
						select = 0;
						prevKey = 0;
					}
					CstcLog_printf("Selected = %d", key);
					break;
				}
				else
				{
					prevKey = 0;
					goto LOOP;
				}
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	retval = select;
	lcd_clean();
	return retval;
}




int lcd_menu_shifts(const char * pszTitle, int font_height, shift_type_struct *shift_type_struct_var, unsigned int count, int select)
{
	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0;
	int prevKey = 0;
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=500000; //micrseconds
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;

	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	CstcLog_printf( "In lcd_menu_TC");

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	if (select < 0)
	{
		select = 0;
	}

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height, pszTitle);
			lcd_printf(ALG_CENTER,20, "************************");

			max_lines = (screen_height - current_y - 1) / font_height;


			istart = (select / max_lines) * max_lines;
			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height +1);

						lcd_printf(ALG_LEFT,font_height,"%d. %s ", istart+i+1, (shift_type_struct_var+(istart + i))->shift_type_name_english);
						lcd_set_font_color(oldcolor);
					}
					else
						lcd_printf(ALG_LEFT,font_height,"%d. %s ", istart+i+1, (shift_type_struct_var+(istart + i))->shift_type_name_english);

				}
				else
					break;
			}
			lcd_flip();

			LOOP:
			while(1)
			{
				key=0;
				if((key = Kb_Get_Key_Sym())>1)
				{
					break;
				}
				else
					key=0;
			}
			//				CstcLog_printf("key:%02x\r\n", key);
			switch((unsigned int)key)
			{
			case KEY_SYM_CURSOR_UP:
				select --;
				if (select < 0)
				{
					select = count -1;
				}
				prevKey = 0;
				break;
			case KEY_SYM_CURSOR_DOWN:
				select ++;
				if (select >= count)
				{
					select = 0;

				}
				prevKey = 0;
				break;
			case KEY_SYM_ESCAPE:
				select = -2;
				prevKey = 0;
				bLoop = 0;
				break;
			case KEY_SYM_ENTER:
				bLoop = 0;
				prevKey = 0;
				break;
			default:
				if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
				{
					//						ti.tv_sec=time(NULL);
					//						nowtm =localtime(&(ti.tv_sec));
					//						ti.tv_usec=ti.tv_sec*1000;

					ti.tv_usec=time(NULL);
					ti.tv_sec=time(NULL);
					gettimeofday(&ti, NULL);

					total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

					CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
					CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
					CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);

					if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))

					{
						prevKey = 0;
						pretmvl.tv_sec=ti.tv_sec;
						pretmvl.tv_usec=ti.tv_usec;
						pretmvl_micro_sec = total_micro_seconds;
						CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
					}
					select = (prevKey * 10) + (key & 0x0F) - 1;
					prevKey = select + 1;
					if (select >= count)
					{
						select = 0;
						prevKey = 0;
					}
					CstcLog_printf("Selected = %d", key);
					break;
				}
				else
				{
					prevKey = 0;
					goto LOOP;
				}
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}
	retval = select;
	lcd_clean();
	return retval;
}




int lcd_menu_concession(const char * pszTitle, int font_height, concession_type_struct *full_concession_type_struct_var, unsigned int count, int select)
{
	int retval = -1;

	int key;
	unsigned int i;
	int bLoop;
	IDirectFBFont * font = NULL;
	unsigned int oldcolor = 0;
	int prevKey = 0;
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=500000; //micrseconds
	unsigned int     max_lines = 0;
	unsigned int     istart = 0;

	int screen_width, screen_height;//, font_height;
	//	time_t ti = time(NULL);
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=1;
	tmsec.tv_usec=100;//pretmvl

	switch(font_height)
	{
	case 16:
		main_surface->SetFont(main_surface, font_16);
		break;
	case 17:
		main_surface->SetFont(main_surface, font_17);
		break;
	case 18:
		main_surface->SetFont(main_surface, font_18);
		break;
	case 20:
		main_surface->SetFont(main_surface, font_20);
		break;
	case 21:
		main_surface->SetFont(main_surface, font_21);
		break;
	case 24:
		main_surface->SetFont(main_surface, font_24);
		break;
	case 26:
		main_surface->SetFont(main_surface, font_26);
		break;

	case 27:
		main_surface->SetFont(main_surface, font_27);
		break;

	case 28:
		main_surface->SetFont(main_surface, font_28);
		break;
	case 30:
		main_surface->SetFont(main_surface, font_30);
		break;
	case 31:
		main_surface->SetFont(main_surface, font_31);
		break;
	case 32:
		main_surface->SetFont(main_surface, font_32);
		break;
	case 34:
		main_surface->SetFont(main_surface, font_34);
		break;
	case 35:
		main_surface->SetFont(main_surface, font_35);
		break;
	case 40:
		main_surface->SetFont(main_surface, font_40);
		break;
	case 45:
		main_surface->SetFont(main_surface, font_45);
		break;
	case 60:
		main_surface->SetFont(main_surface, font_60);
		break;
	default:
		break;
	}

	CstcLog_printf( "In lcd_menu_concession....");

	if((font_height!=17)&&(font_height!=21)&&(font_height!=27)&&(font_height!=31)&&(font_height!=34)&&(font_height!=45)&&(font_height!=60))
	{
		main_surface->GetFont(main_surface, &font);
		font->GetHeight(font, &font_height);
	}
	main_surface->GetSize(main_surface, &screen_width, &screen_height);

	if (select < 0)
	{
		select = 0;
	}

	if (count <= 0)
	{
		retval = -1;
	}
	else
	{
		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MAX);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MAX);

		bLoop = 1;
		while (1 == bLoop)
		{
			lcd_clean();
			lcd_printf(ALG_CENTER,20, "************************");
			lcd_printf(ALG_CENTER,font_height, pszTitle);
			lcd_printf(ALG_CENTER,20, "************************");

			max_lines = (screen_height - current_y - 1) / font_height;


			istart = (select / max_lines) * max_lines;
			for (i = 0;  i < max_lines; i++)
			{
				if (istart + i < count)
				{
					if (istart + i == select)
					{
						oldcolor = font_color;
						lcd_set_font_color(COLOR_GREEN);
						lcd_draw_rectangle(1, current_y -1, screen_width -1, font_height +1);

						lcd_printf(ALG_LEFT,font_height,"%d. %s ", istart+i+1, (full_concession_type_struct_var+(istart + i))->concession_type_name_english);
						lcd_set_font_color(oldcolor);
					}
					else
						lcd_printf(ALG_LEFT,font_height,"%d. %s ", istart+i+1, (full_concession_type_struct_var+(istart + i))->concession_type_name_english);

				}
				else
					break;
			}
			lcd_flip();

			LOOP:
			while(1)
			{
				key=0;
				if((key = Kb_Get_Key_Sym())>1)
				{
					break;
				}
				else
					key=0;
			}
			//				CstcLog_printf("key:%02x\r\n", key);
			switch((unsigned int)key)
			{
			case KEY_SYM_CURSOR_UP:
				select --;
				if (select < 0)
				{
					select = count -1;
				}
				prevKey = 0;
				break;
			case KEY_SYM_CURSOR_DOWN:
				select ++;
				if (select >= count)
				{
					select = 0;

				}
				prevKey = 0;
				break;
			case KEY_SYM_ESCAPE:
				select = -1;
				prevKey = 0;
				bLoop = 0;
				break;
			case KEY_SYM_ENTER:
				bLoop = 0;
				prevKey = 0;
				break;
			default:
				if(key >= KEY_SYM_0 && key <= KEY_SYM_9)
				{
					//						ti.tv_sec=time(NULL);
					//						nowtm =localtime(&(ti.tv_sec));
					//						ti.tv_usec=ti.tv_sec*1000;

					ti.tv_usec=time(NULL);
					ti.tv_sec=time(NULL);
					gettimeofday(&ti, NULL);

					total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;

					CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
					CstcLog_printf("%lu:::%lu", pretmvl.tv_usec,ti.tv_usec);
					CstcLog_printf("%llu:::%llu",total_micro_seconds,limit_time_micros/1000);

					if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))

					{
						prevKey = 0;
						pretmvl.tv_sec=ti.tv_sec;
						pretmvl.tv_usec=ti.tv_usec;
						pretmvl_micro_sec = total_micro_seconds;
						CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
					}
					select = (prevKey * 10) + (key & 0x0F) - 1;
					prevKey = select + 1;
					if (select >= count)
					{
						select = 0;
						prevKey = 0;
					}
					CstcLog_printf("Selected = %d", key);
					break;
				}
				else
				{
					prevKey = 0;
					goto LOOP;
				}
			}

		}

		led_set_brightness(LED_ARROW_DOWN,LED_BRIGHTNESS_MIN);
		led_set_brightness(LED_ARROW_UP,LED_BRIGHTNESS_MIN);
	}

	retval = select;
	lcd_clean();
	return retval;
}


