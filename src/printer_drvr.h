#ifndef PRINTER__DRVR_H_
#define PRINTER__DRVR_H_

#include <posapi.h>
#include <printer.h>
#include <directfb.h>
#include "lcd_drvr.h"
#include "string_convert.h"
#include "cstc_defines.h"

#define printer_device_name "/dev/printer0"
#define font_file_02  BANGLA_FONT_FILE_ADDR       //"/usr/share/fonts/wqy-microhei.ttf"
#define font_file_01  ENGLISH_FONT_FILE_ADDR
#define font_file_bold ENGLISH_BOLD_FONT_FILE_ADDR

void Printer_Init(void);
void Printer_String_Font16(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font20(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font24(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font26(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font28(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font30(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font32(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font35(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font38(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font40(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font45(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font50(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
void Printer_String_Font60(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign);
int Printer_Start_printing(int y,int pagefeed);
int Printer_Get_Status(void);
void Printer_Deinit(void);
void Printer_String_Alignment(char* dataStr, int bytes, int x, int *y,int txtAlign);
int Printer_Start(void);
void Printer_Stop(void);
void surface_deinit(void);
void surface_init(int retval,int surf_height);

#endif
