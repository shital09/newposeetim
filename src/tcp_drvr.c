#include <posapi.h>
#include <stdarg.h>
#include "tcp_drvr.h"
#include "cstclog.h"


/********************************************************************************
 *  Function Name:  tcp_init
 *  Description:
 *                  Function is used to initialize tcp communication
 *
 * Parameters : -               RETURN VALUE    : -
 *********************************************************************************/
void tcp_init(void)
{
	static int inited = 0;
	if (!inited){
		signal(SIGPIPE, SIG_IGN);
		inited = 1;
	}
}

/********************************************************************************
 *  Function Name:  tcp_server
 *  Description:
 *                  Function is used to create socket, binds the port and server
 *                  address and listens the newsocket connection
 *
 * Parameters : Port            			 RETURN VALUE    : +ve/0 : Success
 * 			  : Max socket to listen							-ve  : Failure
 *********************************************************************************/
int  tcp_server(unsigned int uiPort, unsigned int MaxConnect)
{
#define MAX_SOCKETS 1000
	int isocket = -1;
	struct sockaddr_in  servaddr;
	tcp_init();
	if (MaxConnect == 0)
		MaxConnect = MAX_SOCKETS;
	isocket = socket(AF_INET, SOCK_STREAM, 0);
	if (isocket >= 0){
		memset(&servaddr, 0, sizeof(servaddr));
		servaddr.sin_family = AF_INET;
		servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		servaddr.sin_port = htons(uiPort);
		if( bind(isocket, (struct sockaddr*)&servaddr, sizeof(servaddr)) == -1){
			tcp_close(isocket);
			isocket = -2;
		}else if( listen(isocket,MaxConnect ) == -1){
			tcp_close(isocket);
			isocket = -3;
		}
	}
	return isocket;
}

/********************************************************************************
 *  Function Name:  tcp_accept
 *  Description:
 *                  Function will accept client connection.
 *
 * Parameters : function Descriptor    	 RETURN VALUE    : +ve/0 : Success
 * 			  : port					            		-ve  : Failure
 *			  : Server IP
 *********************************************************************************/
int tcp_accept(int iSvrSocket, char szClientIP[16], unsigned int * puiClientPort)
{
	int clientSocket = -1;
	struct timeval tv;
	struct sockaddr_in addr;
#if defined(WIN32)
	int addrlen;
#else
	unsigned int addrlen;
#endif
	fd_set r;
	tv.tv_sec = 0;
	tv.tv_usec = 1000;
	FD_ZERO(&r);
	FD_SET(iSvrSocket, &r);
	if (select(iSvrSocket + 1, &r, NULL, NULL, &tv)>0)
	{
		addrlen = sizeof(struct sockaddr);

		clientSocket=accept(iSvrSocket,  (struct sockaddr *)&addr, &addrlen);
		if (clientSocket >=0)
		{
			if (szClientIP != NULL)
			{
				sprintf(szClientIP, inet_ntoa( addr.sin_addr));
			}
			if (NULL != puiClientPort)
			{
				*puiClientPort = ntohs(addr.sin_port);
			}
		}
	}
	return clientSocket;
}


/********************************************************************************
 *  Function Name:  tcp_connect
 *  Description:
 *                  Function will connect client connection to Server IP and Port.
 *
 * Parameters : Server IP 	 RETURN VALUE    : +ve/0 : Success
 * 			  : port					      	-ve  : Failure
 *********************************************************************************/
int tcp_connect(const char * pServerAddr, unsigned int port)
{
	int clientsocket = -1;
	struct sockaddr_in addr;
	struct hostent *pHE;
	tcp_init();
	clientsocket = socket(AF_INET,SOCK_STREAM,0);
	if (-1 != clientsocket){

		memset(&addr, 0, sizeof(addr));
		addr.sin_addr.s_addr=inet_addr(pServerAddr);
		if (INADDR_NONE == addr.sin_addr.s_addr)
		{
			pHE=gethostbyname(pServerAddr);
			if(pHE)
			{
				addr.sin_addr.s_addr = *(u_long *) pHE->h_addr_list[0];
			}
		}

		addr.sin_family=AF_INET;
		addr.sin_port=htons(port);

		if(connect(clientsocket,(struct sockaddr *)&addr, sizeof(addr))==0)
		{

		}
		else
		{
			tcp_close(clientsocket);
			clientsocket = -1;
		}

	}
	return clientsocket;
}


/********************************************************************************
 *  Function Name:  tcp_close
 *  Description:
 *                  Function is used to close tcp communication
 *
 * Parameters : function descriptor              RETURN VALUE    : -
 *********************************************************************************/
void   tcp_close(int isocket)
{
	if (isocket >=0)
		close(isocket);
}

/********************************************************************************
 *  Function Name:  tcp_read
 *  Description:
 *                  Function is used to receive data in communication.
 *
 * Parameters : function descriptor         RETURN VALUE   : bytes received
 * 			  : bytes received				         	   : number of bytes read
 * 			  : number of bytes read
 *********************************************************************************/
int tcp_read(int isocket, void * pvbuff, unsigned int * pisize)
{
	int retval = 0;
	int error;
	struct timeval tv;
	fd_set r;


	tv.tv_sec = 3600;
	tv.tv_usec = 1000;

	FD_ZERO(&r);
	FD_SET(isocket, &r);
	error= select(isocket+1,&r,NULL, NULL, &tv);

	if ( error > 0)
	{

		if (*pisize > 0)
		{
			error =  recv(isocket,(char*)pvbuff, *pisize, 0);
			CstcLog_printf("pvbuff %s", pvbuff);
			if (error > 0)
			{
				*pisize = (unsigned int)error;
			}
			else
			{
				retval = -1;
			}
		}
	}
	else if (0 != error)
	{
		retval = -1;
	}
	else
	{
		*pisize = 0;
		retval = 0;
	}

	return retval;
}


/********************************************************************************
 *  Function Name:  tcp_write
 *  Description:
 *                  Function is used to send data in communication.
 *
 * Parameters : function descriptor         RETURN VALUE   : bytes received
 * 			  : bytes received				         	   : number of bytes read
 * 			  : number of bytes read
 *********************************************************************************/
int tcp_write(int isocket, const void * pvdata, unsigned int isize)
{
	int retval = 0;
	struct timeval tv;
	fd_set r;

	tv.tv_sec = 0;
	tv.tv_usec = 1000;
	FD_ZERO(&r);
	FD_SET(isocket, &r);

	if (select(isocket+1,NULL,&r,NULL,&tv) > 0)
	{
		if(isize != (unsigned int)send(isocket,(char*)pvdata, isize, 0))
		{
			retval = -1;
		}
	}
	else
	{
		retval = -1;
	}

	return retval;
}
