/*
 * printer_interface.h
 *
 *  Created on: 08-Apr-2014
 *      Author: root
 */

#ifndef PRINTER_INTERFACE_H_
#define PRINTER_INTERFACE_H_
#include "printer_drvr.h"
#include "lcd_drvr.h"
int Printer_Demo_English(int penalty_type);
int Printer_Demo_Bangla(int penalty_type);
int print_ticket(void);
int route_details(void);
void print_current_trip_report(char *startTicketNo, char *endTicketNo,char *TripStartDt, char *TripStartTm, int tempTotal,char *totalPxcnt,char *adultcnt,char *childcnt,char *srcitizencnt,char *luggcnt,char *adultamt,char *childamt,char *srcitizenamt,char *luggamt);
//void print_current_trip_report(transaction_log_struct transaction_log_struct_var, char *startTicketNo, char *endTicketNo, char *startSchedule, char *endSchedule, char *TripStartDt, char *TripStartTm,/* char *busStartDt, char *busStartTm,*/ char *totalPx);
//void print_all_trip_report(transaction_log_struct transaction_log_struct_var,char *totalPx);
void print_all_trip_report(transaction_log_struct transaction_log_struct_var, char *temptrip,char *startTicketNo,char *endTicketNo, char *scheduleNo, char *TripStartDt, char *TripStartTm, int tempTotal,/*char *totalPxcnt,*/char *adultcnt, char *childcnt,/*char *srcitizencnt,char *luggcnt,*/char *adultamt, char *childamt,/*char *srcitizenamt,*/char *tollamt,int startflag,int endflag, int reportFlag,int scheduleFlag,char *shift, int grandTotal, int tktCount,char *cancelled_cnt, char *cancelled_amt,char *concession_cnt, char *concession_amt,char *e_purse_cnt, char *e_purse_amt);
int print_book_case(/*char *startSchedule, char *endSchedule,char *routeNo,char *servicType ,*/char *type, char *chkstg);
//void print_inspector_report(transaction_log_struct transaction_log_struct_var, char *startSchedule, char *endSchedule, char *totalPx);
//int print_inspection_report(transaction_log_struct transaction_log_struct_var, char *chkstg, int tempTotal,char *totalPxcnt,char *adultcnt,char *childcnt,char *srcitizencnt,char *luggcnt,char *adultamt,char *childamt,char *srcitizenamt,char *luggamt);
int print_inspection_report(char *chkstg, int tempTotal,char *totalPxcnt,char *adultcnt,char *childcnt,char *srcitizencnt,char *luggcnt,char *adultamt,char *childamt,char *srcitizenamt,char *luggamt,int totalPen,char *adultcntPen,char *childcntPen,char *srcitizencntPen,char *luggcntPen,char *adultamtPen,char *childamtPen,char *srcitizenamtPen,char *luggamtPen,int normalFlag,int penaltyFlag);
int print_footer(int y, int report_flag);
int print_header(void);
//int print_line_checking_status_report(transaction_log_struct *transaction_log_struct_var, /*char *startTicketNo, char *endTicketNo, char *startSchedule, char *endSchedule, char *TripStartDt, char *TripStartTm,*/char *totalPxcnt,/*char *luggcnt,*/char *checkstg, int totalAmt, int totalPx);
int print_line_checking_status_report(transaction_log_struct *transaction_log_struct_var, int rowCnt, int totalAmt, int totalPx, int tktCount,char* lugg_tkt_count,char *pass_tkt_count, char* chkstg,char *pass_count);

int print_bangla_stop(char *bangla_string,int y);
int print_all_trip_status_report(char *scheduleNo, char *shift,char *tripNo,char *ticketCount,char *passCnt,char *passAmt,char *totalPxcnt,char *tempTotal,int scheduleflag,int shiftFlag,int startflag,int endflag,char *grandTotal,char *toll_amt);
int print_activity_log(activity_print_struct *temp_activity_print_ptr,int rowCnt);

#endif /* PRINTER_INTERFACE_H_ */
