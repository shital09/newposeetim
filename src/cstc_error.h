
#ifndef BMTC_ERROR_H_
#define BMTC_ERROR_H_

#define SUCCESS 1
#define FAIL  	2

/*---------------ERROR_MESSAGES----------------------*/
#define ERROR_OPENING_FILE_etim_no				3     //etim.txt
#define ERROR_OPENING_FILE_mac_ID 				4      //mac_ID.txt
#define WAYBILL_MASTER_FAILED 					5
#define SCHEDULE_MASTER_FAILED 					6
#define DUTY_MASTER_FAILED						7
#define ROUTE_MASTER_FAILED 					8
#define FARE_CHART_FAILED						9
#define TICKET_TYPE_MASTER_FAILED 				10
#define TICKET_SUB_TYPE_MASTER_FAILED 			11
#define BUS_SERVICE_MASTER_FAILED 				12
#define BUS_BRAND_MASTER_FAILED 				13
#define DEPOT_MASTER_FAILED		 				14
#define ROUTE_HEADER_FAILED 					15
#define ETM_READ_WRITE_KEY_FAILED 				16
#define CONNECTION_WAS_BROKEN 					17
#define DOWNLOAD_REQUEST_FAILED 				18
#define ERASE_DATA_FAILED 						19
#define REGISTRATION_REQUEST_FAILED				20
#define LESS_PACKET_LENGTH 						21
#define NO_MATCH_FOUND 							22
#define WAYBILL_NUMBER_NOT_FOUND 				23
#define SECURITY_KEY_NOT_FOUND 					24
#define TERMINAL_ID_NOT_FOUND 					25
#define MACHINE_NUMBER_NOT_FOUND 				26
#define MAC_ADDRESS_NO_MATCH_FOUND 				27
#define FARE_CHART_PARTIAL_SUCCESS	 			28
#define SQL_DB_PARSING_FAILED 					29
#define SQLITE_DB_INSERTION_FAILED 				30
#define SQL_DB_OPEN_FAILED 		        		31
#define SQL_DB_CLOSE_FAILED		 				32
#define SQL_DB_UPDATE_FAILED 					33
#define SQL_DB_SELECT_FAILED					34
#define SQL_DB_DELETE_FAILED 					35
#define GREATER_PACKET_LENGTH					36
#define RESEND_PACKETS							37
#define WAYBILL_PRESENT							38
#define START_DUTY_FAILED						39
#define FARE_CHART_COUNT_FAILED					40
#define TRUNK_FEEDER_FAILED						41
#define SERVICE_OP_FAILED						42
#define ERROR_OPENING_FILE_db_input				43 		//	db_input.txt
#define ERROR_OPENING_FILE_db_tbl_input			44 		//	db_tbl_input.txt
#define SCHEDULE_DETAILS_FAILED					45
#define PARSING_FAILED							46
#define INSERTION_SUCCESS						47
#define PARSING_SUCCESS							48
#define RATE_MASTER_FAILED						49
#define DUTY_NOT_CLOSED							50
#define ROUTE_ID_ABSENT							51
#define CRC_CHECK_FAILED						52
#define INVALID_WAYBILL_NO						53
#define LC_LOGIN_FAILED							54
#define REC_NOT_FND_START_DUTY_FAILED			55
#define MASTER_NOT_INSERT_START_DUTY_FAILED		56
#define PKG_FILE_NAME_REQ_FAILED          		57
#define PKG_FILE_DOWNLOAD_REQ_FAILED       		58
#define GPRS_TABLE_FAILED						59
#define ROUTE_CARD_FAILED						60
#define SHIFT_TYPE_FAILED						61
#define CONCESSION_TYPE_FAILED					62
#define ACTIVITY_MASTER_FAILED					63
#define ACTIVITY_LOG_FAILED						64


#endif

