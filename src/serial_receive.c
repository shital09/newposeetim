/*
 * serial_recieve1.c
 *
 *  Created on: 05-Feb-2015
 *      Author: root
 */
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <stdlib.h>
#include <uart.h>
#include <malloc.h>
#include <curl/curl.h> // Swap this is code for CURL
#include "cstclog.h"
#include <malloc.h>
#include "display_interface.h"
#include "serial_receive.h"
#include "gprs_interface.h"


 size_t malloc_usable_size (void *ptr);


/*
 * 'open_port()' - Open serial port 1.
 *
 * Returns the file descriptor on success or -1 on error.
 */
int fd; /* File descriptor for the port */
int data_len = 0;
FILE *dest_file_ptr;            			// dest file pointer
char *temp_buff=NULL;
int open_port(void)
{

	fd = open("/dev/ttyS3", O_RDWR);
	if (fd == -1)
	{
		/*
		 * Could not open the port.
		 */

		CstcLog_printf("open_port: Unable to open /dev/ttyS0 - %s",strerror(errno));
		return 0;
	}
	else
		CstcLog_printf("Open Port Successful");
	tty_property_config(fd,115200,8,'n',1,'n');        // configured port

	return (fd);
}

int send_data(const char *send_string)
{
	int n;
	n = write(fd, send_string, strlen(send_string));
	if (n < 0)
	{
		CstcLog_printf("SEND FAILED");
		return 0;
	}
	return 1;
}

int send_ack(char *start_string)
{
	if(!send_data(start_string))
	{
		CstcLog_printf("Start Ack send failed");
		return 0;
	}
	CstcLog_printf("Start Ack send Successful");

	return 1;

}

int wait_for_start(void)
{
	char start_string[100];     //start ack string with containing pkg len and other details
	//char start_string[(VERSION_NO_LEN+APP_VER_DATETIME_LEN+APP_NAME_LEN+MACHINE_SN_LEN+PKG_FILE_LEN)+1]; //8+10+10+8+ 10 =46+1

//    int str_len  =VERSION_NO_LEN+APP_VER_DATETIME_LEN+APP_NAME_LEN+MACHINE_SN_LEN+PKG_FILE_LEN;
	char temp_data[PKG_FILE_LEN+1];    // for finding the size of the new  pkg file
	int i=0;
	memset(start_string,0,100);
	CstcLog_printf("Waiting for start_byte.....");


	for(i = 0;i<46;i++)
	{
		while((read(fd, &start_string[i],1)<=0));
	}
	CstcLog_printf("received start_string = %s",start_string);
	CstcLog_printf("read header finished....");

	memcpy(temp_data,&start_string[0],PKG_FILE_LEN);
	temp_data[PKG_FILE_LEN]='\0';

	data_len=atoi(temp_data);
	CstcLog_printf("data_len = %d",data_len);


	if(!send_ack(start_string))
	{
		CstcLog_printf("SEND ACK FAILED");
		return 0;
	}
	else
	{
		CstcLog_printf("START ACK SEND SUCCESSFUL");

	}
	return 1;

}

int read_data(char *read_buff,int len)
{
	int i;

//	FILE *dest_copy;  //  src copy file pointer
//
//	dest_copy= fopen("/home/user0/CSTC/dest_copy.txt","wb");
//	if(dest_copy==NULL)
//	{
//		CstcLog_printf("dest_copy open failed");
//		return 0;
//	}
//	CstcLog_printf("dest_copy opened successfully");


	CstcLog_printf("reading data of len = %d",len);
	usleep(2000);
	for(i = 0;i<len;i++)
	{
		while((read(fd, &read_buff[i], 1)<=0));

	}
//	fwrite(read_buff,sizeof(char),len,dest_copy);     // copying data to .txt file at m/c end
//	fclose(dest_copy);
//	CstcLog_printf("read data over ");
//	CstcLog_printf("copying data to dummy file");

//	fwrite(temp_buff,sizeof(char),1,dest_file_ptr);
//	CstcLog_printf("data copied to dest file");


	return 1;
}


int close_serial_port(void)
{
	close(fd);
	return 1;
}
int configure_serial(void)
{
#if 0
	struct termios newtio;
	tcgetattr(fd, &newtio);   						// Get the current options for the port

	//fcntl(fd, F_SETFL);
	cfsetispeed(&newtio, B115200);    				// Set the baud rates to 230400
	cfsetospeed(&newtio, B115200);

	newtio.c_cflag |= (CLOCAL | CREAD);    // Enable the receiver and set local mode
	newtio.c_cflag &= ~PARENB;             // No parity bit
	newtio.c_cflag &= ~CSTOPB;             // 1 stop bit
	newtio.c_cflag &= ~CSIZE;              // Mask data size
	newtio.c_cflag |=  CS8;                // Select 8 data bits
	newtio.c_cflag &= ~CRTSCTS;            // Disable hardware flow control

	// Enable data to be processed as raw input
	newtio.c_lflag &= ~(ICANON | ECHO | ISIG);

	// Set the new attributes
	tcsetattr(fd, TCSANOW, &newtio);
	tcflush(fd, TCIFLUSH);

#endif


	struct termios newtio;
	tcgetattr(fd, &newtio);   // Get the current options for the port

	//fcntl(fd, F_SETFL);
	cfsetispeed(&newtio, B115200);    // Set the baud rates to 230400
	cfsetospeed(&newtio, B115200);

	newtio.c_cflag |= (CLOCAL | CREAD);    // Enable the receiver and set local mode
	newtio.c_cflag &= ~PARENB;             // No parity bit
	newtio.c_cflag &= ~CSTOPB;             // 1 stop bit
	newtio.c_cflag &= ~CSIZE;              // Mask data size
	newtio.c_cflag |=  CS8;                // Select 8 data bits
	newtio.c_cflag &= ~CRTSCTS;            // Disable hardware flow control
	newtio.c_iflag &=  ~(IXON | IXOFF | IXANY);
	// Enable data to be processed as raw input
	newtio.c_lflag &= ~(ICANON | ECHO | ISIG);
	newtio.c_oflag |= OPOST; // ?? choosing processed output
//	newtio.c_iflag &= ~(INLCR|ICRNL);
	newtio.c_oflag &=~ ONLCR;

//	newtio.c_cc[VMIN] = 0; // Wait until x bytes read (blocks!)
//	newtio.c_cc[VTIME] = 0; // Wait x * 0.1s for input (unblocks!)

	// Set the new attributes

	tcsetattr(fd, TCSANOW, &newtio);
	tcflush(fd, TCIOFLUSH);

	//bzero(&newtio, sizeof(newtio));
//	newtio.c_cflag = B115200 | CRTSCTS | CS8 | CLOCAL | CREAD;
//	newtio.c_iflag = IGNPAR;
//	newtio.c_oflag = 0;
//
//	newtio.c_lflag = 0;
//
//	newtio.c_cc[VTIME]    = 0;
//	newtio.c_cc[VMIN]     = 255;

	//tcflush(fd, TCIFLUSH);
//	tcsetattr(fd,TCSANOW,&newtio);

	return 1;
}

int serial_receive(void)
{
//	FILE *dest_file_ptr;            			// dest file pointer
//	char *temp_buff=NULL;

	int size_counter = 0;
//	int malloced_size=0;

	CstcLog_printf("Getting Started.. ");
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,20,70,"UPDATING SOFTWARE");
	lcd_flip();

	open_port();

	//configure_serial();

	size_counter = 0;
	if(!wait_for_start())    						// handshaking and getting data length here
		return 0;

	CstcLog_printf("temp_buff size = %d ",data_len);

	temp_buff=(char *)malloc(sizeof(char)*data_len);
	memset(temp_buff,0,data_len);

//	malloced_size=malloc_usable_size(temp_buff);
//	CstcLog_printf("Actual memory allocated by malloc  = %d ",malloced_size);


	dest_file_ptr = fopen("/home/user0/CSTCApp.pkg","wb");

	if(dest_file_ptr==NULL)
	{
		CstcLog_printf("DUMMY.pkg open failed");
		return 0;
	}
	CstcLog_printf("DUMMY.pkg opened successfully");

		if(!read_data(temp_buff,data_len))
			return 0;
		fwrite(temp_buff,sizeof(char),data_len,dest_file_ptr);
//		CstcLog_HexDump_1(temp_buff,data_len);


	if(temp_buff!=NULL)
	{
		free(temp_buff);
		temp_buff=NULL;
	}

	close_serial_port();
	fclose(dest_file_ptr);
	return 1;
}


int  progress_percentage(void)  // to show percentage of pkg file download progress
{
	CstcLog_printf("Under Implementation");
	return 1;
}

