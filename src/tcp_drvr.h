#ifndef printer_drvr_h_
#define printer_drvr_h_

#include <posapi.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netif.h>
void tcp_init(void);
void tcp_close(int isocket);
int tcp_server(unsigned int uiPort, unsigned int MaxConnect);
int tcp_accept(int iSvrSocket, char szClientIP[16], unsigned int * puiClientPort);
int tcp_connect(const char * pServerAddr, unsigned int port);
int tcp_read(int isocket, void * pvbuff, unsigned int * pisize);
int tcp_write(int isocket, const void * pvdata, unsigned int isize);
#endif
