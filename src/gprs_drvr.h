#ifndef gprs_drvr_h_
#define gprs_drvr_h_
#include <posapi.h>
#include <wnet.h>
#include <ppp.h>
#include <sys/time.h>
#include "cstc_defines.h"

#define GPRS_DEVICE_ADDR "/var/mux0"
#define GPRS_PPP_DEVICE_ADDR "/var/mux1"
int Set_Gprs_Power_On(void);
int Set_Gprs_Power_Off(void);
int Gprs_Initialise(void);
int Gprs_ISP(void);
int Gprs_PPP_Connect(void);
int Gprs_Open_PPP_Connection(void);
int Gprs_Check_PPP_Connection(void);
int Gprs_Close_PPP_Connection(void);

#endif
