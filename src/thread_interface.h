/*
 * thread_interface.h
 *
 *  Created on: 17-Apr-2014
 *      Author: root
 */

#ifndef THREAD_INTERFACE_H_
#define THREAD_INTERFACE_H_
#include <pthread.h>
#define NUM_THREADS	5

void* DisplayTime(void* arg);
int startThread_DisplayTime(void);
void stopThread_DisplayTime(void);

#endif /* THREAD_INTERFACE_H_ */
