/*
 * display_interface.c
 *
 *  Created on: 08-Apr-2014
 *      Author: root
 */
//#include "lcd_drvr.h"
#include "display_interface.h"
#include "string_defines.h"
#include <unistd.h>

// extern char* username, *pswd;
//int key;
extern fare_chart_struct fare_chart_struct_var;
extern current_passengers_details_struct current_passengers_details_struct_var;
extern int lang_Bangla_flag;
extern int roundNo(float num);
extern bus_service_struct bus_service_struct_var;
extern schedule_master_struct schedule_master_struct_var;
extern depot_struct depot_struct_var;
extern duty_master_struct duty_master_struct_var;
extern selected_trip_info_struct selected_trip_info_struct_var;
extern waybill_master_struct waybill_master_struct_var;
extern all_flags_struct  all_flags_struct_var;

extern int temp_offset;


/********************************************************************************
 *  Function Name:  Init_Display
 *  Description:
 *                  Initialise display
 *
 *  Parameters: None			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Init_Display(int * argc, char **argv[])
{
	lcd_init(argc, argv);
	lcd_set_bk_color(COLOR_WHITE);
	lcd_set_font_color(COLOR_BLACK);
	return 1;

}


/********************************************************************************
 *  Function Name:  Init_Display
 *  Description:
 *                  Deinitialise display
 *
 *  Parameters: None			    RETURN VALUE    : N/A
 *
 *********************************************************************************/
void Deinit_Display()
{
	lcd_uninit();
}

/********************************************************************************
 *  Function Name:  Display_Home_Screen
 *  Description:
 *                  Welcome to BMTC
 *
 *  Parameters: None			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_Home_Screen()
{
	//	lcd_clean();
	//	//popup_clean();
	//	lcd_printf_ex(ALG_CENTER,35,20,"WELCOME");
	//	lcd_printf_ex(ALG_CENTER,35,70,"TO");
	//	lcd_printf_ex(ALG_CENTER,35,110,"CSTC");
	//	lcd_flip();
	lcd_show_picture(IMAGE_PATH);
	usleep(200);

	return 1;
}


/********************************************************************************
 *  Function Name:  display_popup
 *  Description:
 *                  Display pop-up window with message
 *
 *  Parameters: message			    RETURN VALUE    : 1
 *
 *********************************************************************************/
void display_popup(char *message)
{

	popup_clean();//popup test
	//	popup_printf_xy(20,x_position,45,message);
	popup_printf_ex(ALG_CENTER,20,45,message);
	popup_draw_rectangle(1,1,238,118);
	popup_flip();
	sleep(1);
	popup_clean();
	/*   RELEASE POPUP WINDOW     */
	//popup_deinit();


}

/********************************************************************************
 *  Function Name:  Display_Time_On_Screen
 *  Description:
 *                  Display current date and time on main screen
 *
 *  Parameters: None			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_Time_On_Screen()
{
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
	lcd_printf_ex(ALG_LEFT,20,190,"%02d/%02d/%d %02d:%02d:%02d", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900, tm->tm_hour, tm->tm_min, tm->tm_sec);
	lcd_flip();

	return 1;
}
const char *wd(int year, int month, int day)
{
	return ((const char *[])
			{
		"Monday", "Tuesday", "Wednesday",
		"Thursday", "Friday", "Saturday", "Sunday"})
		[(day + ((153 * (month + 12 * ((14 - month) / 12) - 3) + 2) / 5)+ (365 * (year + 4800 - ((14 - month) / 12)))+ ((year + 4800 - ((14 - month) / 12)) / 4)- ((year + 4800 - ((14 - month) / 12)) / 100)+ ((year + 4800 - ((14 - month) / 12)) / 400)- 32045) % 7];
}


/********************************************************************************
 *  Function Name:  Display_week_day
 *  Description:
 *                  Display day of the week
 *
 *  Parameters: None			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_week_day()
{
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	return 1;
}


/********************************************************************************
 *  Function Name:  Display_Download_Duty
 *  Description:
 *                  Display 'Download duty' message
 *
 *  Parameters: None			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_Download_Duty()
{
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
	lcd_clean();
	if(lang_Bangla_flag)
	{
		lcd_printf_ex(ALG_CENTER,45,10,"ক রা প সং");
	}
	else
	{
		lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	}
	lcd_printf_ex(ALG_CENTER,20,70,"Press \"Enter Key\" to");
	lcd_printf_ex(ALG_CENTER,20,90,"   Download the Duty.          ");

	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();

	return 1;
}

/********************************************************************************
 *  Function Name:  Display_Connect_To_Local
 *  Description:
 *                  Display connect to PC message
 *
 *  Parameters: None			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_Connect_To_Local()
{
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
	lcd_clean();
	if(lang_Bangla_flag)
	{
//		lcd_printf_ex(ALG_CENTER,45,10,"ಬಿ ಎಮ್ ಟಿ ಸಿ");//ক রা প সং
		lcd_printf_ex(ALG_CENTER,45,10,"ক রা প সং");//ক রা প সং

	}
	else
	{
		lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	}
	lcd_printf_ex(ALG_CENTER,20,70,"Connect the Device to the Local ");
	lcd_printf_ex(ALG_CENTER,20,100," PC and then Press \"ENTER Key\"  ");
	lcd_printf_ex(ALG_CENTER,20,130,"    to Download the Duty.      ");
	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();
	return 1;
}


/********************************************************************************
 *  Function Name:  Display_Connect_To_GPRS
 *  Description:
 *                  Display Downloading Schedule message
 *
 *  Parameters: None			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_Connect_To_GPRS()
{
	lcd_clean();
	if(lang_Bangla_flag)
	{
		lcd_printf_ex(ALG_CENTER,45,10,"ক রা প সং");
	}
	else
	{
		lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	}
	lcd_printf_ex(ALG_CENTER,20,70,"Downloading Schedule");
	lcd_printf_ex(ALG_CENTER,20,100,"This may take few minutes.");
	lcd_printf_ex(ALG_CENTER,20,130,"Please wait.......");
	lcd_flip();
	return 1;
}

/********************************************************************************
 *  Function Name:  Display_Connect_To_GPRS
 *  Description:
 *                  Display Download failed message
 *
 *  Parameters: None			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_ConnectionFailed_To_GPRS()
{
	lcd_clean();
	if(lang_Bangla_flag)
	{
		lcd_printf_ex(ALG_CENTER,45,10,"ক রা প সং");
	}
	else
	{
		lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	}
	lcd_printf_ex(ALG_CENTER,20,40,"Download failed. Please try");
	lcd_printf_ex(ALG_CENTER,20,60,"  again or contact Depot.  ");
	lcd_printf_ex(ALG_LEFT,20,80,"Press “Enter Key “ to retry.");
	lcd_flip();
	return 1;
}


/********************************************************************************
 *  Function Name:  Display_Login_Screen123
 *  Description:
 *                  Display login screen
 *
 *  Parameters: UN, PW, key, cursor, shift lvl, LC flag	    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_Login_Screen123(char* username, char *pswd,int key,int *cursrFlag,int *shiftKeyLevel,int LC_chk_flag)
{
	int reqKey=0,preKey=0;
	int i =0, username_limit=0;
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
	char *fpswd;
	fpswd = (char*)malloc(((PSWRD_LEN+1)*sizeof(char)));
	memset(fpswd,0,PSWRD_LEN+1);
	if(LC_chk_flag)
	{
		username_limit = LC_STAFF_USERNAME_LIMIT;
	}
	else
	{
		username_limit = CONDR_NAME_LEN;
	}
	//All display here

	if(key!=0)
	{
		if(*cursrFlag==1)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(username)>1)&&(strlen(username)<=(username_limit+1)))
			{
				if(NULL!=strchr(username,'|'))
				{
					username[strlen(username)-2]='|';
					username[strlen(username)-1]='\0';
				}
				else
					username[strlen(username)-1]='|';
			}
			else if((strlen(username)>0)&&(strlen(username)<=username_limit)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(username,'|'))
					sprintf(username+(strlen(username)-1),"%c|",key);
			}
			else if((strlen(username)>0)&&(strlen(username)<=username_limit)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				CstcLog_printf("1. preKey Alhpa %c",preKey);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				CstcLog_printf("2. reqKey Alhpa %c",reqKey);
				if(NULL!=strchr(username,'|'))
				{
					CstcLog_printf("username Alhpa %s ::%d",username,strlen(username));
					CstcLog_printf("username Alhpa %c",username[(strlen(username)-2)]);
					if(strlen(username)>1)
					{
						if(username[(strlen(username)-2)]==preKey)
							sprintf(username+(strlen(username)-2),"%c|",reqKey);
						else
							sprintf(username+(strlen(username)-1),"%c|",reqKey);
					}
					else
					{
						sprintf(username+(strlen(username)-1),"%c|",reqKey);
					}
					CstcLog_printf("username Alhpa %s",username);
				}
				else if(NULL==strchr(username,'|'))
				{
					CstcLog_printf("No cursor username Alhpa %s ::%d",username,strlen(username));
					CstcLog_printf("No cursor username Alhpa %c",username[(strlen(username)-2)]);
					if(strlen(username)>1)
					{
						if(username[(strlen(username)-1)]==preKey)
							sprintf(username+(strlen(username)-1),"%c",reqKey);
					}

				}
			}
			else if((strlen(username)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				CstcLog_printf("3. reqKey Alhpa %c",reqKey);
				sprintf(username+(strlen(username)),"%c|",reqKey);
			}
			else if((strlen(username)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(username+(strlen(username)),"%c|",key);

/*			if(strlen(username)==username_limit)
			{
				username[strlen(username)-1]='\0';
				if(!LC_chk_flag)
				{
					*cursrFlag=2;
					pswd[strlen(pswd)]='|';
				}
			}*/
		}
		else if(*cursrFlag==2)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(pswd)>1)&&(strlen(pswd)<=PSWRD_LEN+1))
			{
				if(NULL!=strchr(pswd,'|'))
				{
					pswd[strlen(pswd)-2]='|';
					pswd[strlen(pswd)-1]='\0';
				}
				else
					pswd[strlen(pswd)-1]='|';
			}
			else if((strlen(pswd)>0)&&(strlen(pswd)<=PSWRD_LEN)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(pswd,'|'))
					sprintf(pswd+(strlen(pswd)-1),"%c|",key);
			}
			else if((strlen(pswd)>=0)&&(strlen(pswd)<=PSWRD_LEN)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(pswd,'|'))
				{
					if(pswd[(strlen(pswd)-2)]==preKey)
						sprintf(pswd+(strlen(pswd)-2),"%c|",reqKey);
					else
						sprintf(pswd+(strlen(pswd)-1),"%c|",reqKey);
				}
			}
			else if((strlen(pswd)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(pswd+(strlen(pswd)),"%c|",reqKey);
			}

			else if((strlen(pswd)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(pswd+(strlen(pswd)),"%c|",key);
			if(strlen(pswd)==PSWRD_LEN)
			{
				pswd[strlen(pswd)-1]='\0';
			}
		}
	}

	if(NULL!=strchr(pswd,'|'))
		pswd[strlen(pswd)-1]='\0';

	if(strlen(pswd)>0)
	{
		for(i=0;i<strlen(pswd);i++)
		{
			strcat(fpswd,"*");
		}
	}

	if(*cursrFlag==2)
	{
		if(strlen(fpswd)<=PSWRD_LEN)
			strcat(fpswd,"|");
	}

	if(NULL==strchr(pswd,'|'))
		pswd[strlen(pswd)]='|';

	//	lcd_printf_ex(ALG_CENTER,20,120,"USER :");
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	lcd_printf_ex(ALG_CENTER,26,60,"Show RFID Card or");
/*	lcd_printf_xy(20,80,120,"USER ID");
	lcd_printf_xy(20,190,120,":");
	lcd_printf_xy(20,200,120,"%s",username);*/

	lcd_printf_ex(ALG_CENTER,20,100,"USER ID : %s",username);
/*	lcd_printf_xy(20,80,150,"PASSWORD");
	lcd_printf_xy(20,190,150,":");
	lcd_printf_xy(20,200,150,"%s",fpswd);*/
	lcd_printf_ex(ALG_CENTER,20,130,"PASSWORD : %s",fpswd);
	lcd_printf_ex(ALG_CENTER,20,160,"WAYBILL NO : %s",waybill_master_struct_var.waybill_no);

	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();

	free(fpswd);
	return 1;
}

/********************************************************************************
 *  Function Name:  Display_Conductor_Main_Screen
 *  Description:
 *                  Display Main Screen
 *
 *  Parameters: structs			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_Conductor_Main_Screen(selected_trip_info_struct* cms_selected_trip_info_struct_var,
		waybill_wise_info_struct* cms_waybill_wise_info_struct_var,
		all_flags_struct* cms_all_flags_struct_var,
		current_passengers_details_struct* cms_current_passengers_details_struct_var)//char* startPoint, char* endPoint, char* routeNo, char* busType)
{
	time_t t = time(NULL);
	struct tm	*ptm = localtime(&t);
	char tempRoute[50]={0},/*tempRoute1[50]={0},*/tempType[50]={0}, tempStrQStr[12]={0}, tempStrQStr1[12]={0};
	char tempsched[50]={0};
	memset(tempType,0,50);
	memset(tempRoute,0,50);
	memset(tempsched,0,50);
	memset(tempStrQStr,0,12);
	memset(tempStrQStr1,0,12);
	lcd_clean();
	if(lang_Bangla_flag)
	{
//		lcd_printf_ex(ALG_CENTER,31,20,CSTC);
	}
	else
	{
		lcd_printf_ex(ALG_CENTER,35,5,"WBTC");
	}
	if(strlen(selected_trip_info_struct_var.route_no_qstr)==0)
	{
		cms_all_flags_struct_var->routeSelectionFlag = 0;

//		sprintf(tempsched,"SCH NO:%s",duty_master_struct_var.schedule_no);

/*		memcpy(tempsched,"SCH NO:",strlen("SCH NO:"));
		memcpy(tempsched+strlen("SCH NO:"),duty_master_struct_var.schedule_no,strlen(duty_master_struct_var.schedule_no));*/


		//		memcpy(tempType+strlen(duty_master_struct_var.schedule_no)+strlen("     "),"             ",strlen("             "));
		//		memcpy(tempType+strlen(duty_master_struct_var.schedule_no)+strlen("     ")+strlen("             "),bus_service_struct_var.bus_service_name_english,strlen(bus_service_struct_var.bus_service_name_english));
	}
	else
	{
		cms_all_flags_struct_var->routeSelectionFlag = 1;

		//		memcpy(tempRoute,selected_trip_info_struct_var->start_bus_stop_code_english_qstr,strlen(selected_trip_info_struct_var->start_bus_stop_code_english_qstr));
		//		memcpy(tempRoute+strlen(selected_trip_info_struct_var->start_bus_stop_code_english_qstr)," to ",strlen(" to "));
		//		memcpy(tempRoute+strlen(selected_trip_info_struct_var->start_bus_stop_code_english_qstr)+strlen(" to "),selected_trip_info_struct_var->end_bus_stop_code_english_qstr,strlen(selected_trip_info_struct_var->end_bus_stop_code_english_qstr));

		//		memcpy(tempRoute,"T-NO:",strlen("T-NO:"));
		//		memcpy(tempRoute+strlen("T-NO:"),selected_trip_info_struct_var->trip_id_qstr,strlen(selected_trip_info_struct_var->trip_id_qstr));
		//		memcpy(tempRoute+strlen("T-NO:")+strlen(selected_trip_info_struct_var->trip_id_qstr),"(",strlen("("));
		//		memcpy(tempRoute+strlen("T-NO:")+strlen(selected_trip_info_struct_var->trip_id_qstr)+strlen("("),selected_trip_info_struct_var->start_bus_stop_code_english_qstr,strlen(selected_trip_info_struct_var->start_bus_stop_code_english_qstr));
		//		memcpy(tempRoute+strlen("T-NO:")+strlen(selected_trip_info_struct_var->trip_id_qstr)+strlen("(")+strlen(selected_trip_info_struct_var->start_bus_stop_code_english_qstr)," to ",strlen(" to "));
		//		memcpy(tempRoute+strlen("T-NO:")+strlen(selected_trip_info_struct_var->trip_id_qstr)+strlen("(")+strlen(selected_trip_info_struct_var->start_bus_stop_code_english_qstr)+strlen(" to "),selected_trip_info_struct_var->end_bus_stop_code_english_qstr,strlen(selected_trip_info_struct_var->end_bus_stop_code_english_qstr));
		//		memcpy(tempRoute+strlen("T-NO:")+strlen(selected_trip_info_struct_var->trip_id_qstr)+strlen("(")+strlen(selected_trip_info_struct_var->start_bus_stop_code_english_qstr)+strlen(" to ")+strlen(selected_trip_info_struct_var->end_bus_stop_code_english_qstr),")",strlen(")"));

		sprintf(tempRoute, "T.No.: %s  .  %s To %s", selected_trip_info_struct_var.trip_id_qstr,selected_trip_info_struct_var.start_bus_stop_code_english_qstr,selected_trip_info_struct_var.end_bus_stop_code_english_qstr);
		sprintf(tempType,"     %s     %s",selected_trip_info_struct_var.route_no_qstr,bus_service_struct_var.bus_service_name_english);


		/*memcpy(tempType,"     ",strlen("     "));
		memcpy(tempType+strlen("     "),selected_trip_info_struct_var.route_no_qstr,(strlen(selected_trip_info_struct_var.route_no_qstr)));
		memcpy(tempType+strlen(selected_trip_info_struct_var.route_no_qstr)+strlen("     "),"             ",strlen("             "));
		memcpy(tempType+strlen(selected_trip_info_struct_var.route_no_qstr)+strlen("     ")+strlen("             "),bus_service_struct_var.bus_service_name_english,strlen(bus_service_struct_var.bus_service_name_english));*/



/*		memcpy(tempsched,"SCH NO:",strlen("SCH NO:"));
		memcpy(tempsched+strlen("SCH NO:"),duty_master_struct_var.schedule_no,strlen(duty_master_struct_var.schedule_no));
		memcpy(tempsched+strlen("SCH NO:")+strlen(duty_master_struct_var.schedule_no)," ( ",strlen(" ( "));
		memcpy(tempsched+strlen("SCH NO:")+strlen(duty_master_struct_var.schedule_no)+strlen(" ( "),&selected_trip_info_struct_var.shift_qstr,1);
		memcpy(tempsched+strlen("SCH NO:")+strlen(duty_master_struct_var.schedule_no)+strlen(" ( ")+1," ) ",strlen(" ) "));*/

	}
	if(!cms_all_flags_struct_var->routeSelectionFlag)
	{

		if(lang_Bangla_flag)
		{
			lcd_printf_ex(ALG_CENTER,21,60,NO_ROUTE_SLCT);
		}
		else
		{
			lcd_printf_ex(ALG_CENTER,20,60,"NO TRIP SELECTED");
			lcd_printf_ex(ALG_CENTER,20,90,tempsched);
			//			lcd_printf_ex(ALG_CENTER,20,100,tempType);//"<Route no> <Bus Type>   ");
		}

	}
	else
	{
		lcd_printf_ex(ALG_CENTER,20,50,tempRoute);//"Start Point to End Point");
		lcd_printf_ex(ALG_CENTER,20,75,tempsched);
		lcd_printf_ex(ALG_CENTER,20,100,tempType);//"<Route no> <Bus Type>   ");
		lcd_printf_ex(ALG_CENTER,20,170,selected_trip_info_struct_var.current_selected_start_stop_name_english_qstr);//"<Route no> <Bus Type>   ");
	}
	if(strlen(current_passengers_details_struct_var.present_passengers_qstr)!=0)
	{
		sprintf(tempStrQStr,"     B:%03d",atoi(current_passengers_details_struct_var.present_passengers_qstr));
		sprintf(tempStrQStr1,"A:%03d     ",atoi(current_passengers_details_struct_var.to_be_alighted_on_next_stage_qstr));

		lcd_printf_ex(ALG_LEFT,20,150,tempStrQStr);//"B:000             A:000 ");
		lcd_printf_ex(ALG_RIGHT,20,150,tempStrQStr1);//"B:000             A:000 ");
	}
	else
	{
		lcd_printf_ex(ALG_LEFT,20,150,"     B:000");
		lcd_printf_ex(ALG_RIGHT,20,150,"A:000     ");
	}
	if(!((atoi(cms_waybill_wise_info_struct_var->end_ticket_count_qstr)) - (atoi(cms_waybill_wise_info_struct_var->start_ticket_count_qstr))))
	{
		if(lang_Bangla_flag)
		{
			lcd_printf_ex(ALG_CENTER,21,120,NO_TKTS);
		}
		else
		{
			if(!(cms_all_flags_struct_var->etimLockedInt || cms_all_flags_struct_var->waybillExpiredFlagInt || cms_all_flags_struct_var->collectionFlag))
				lcd_printf_ex(ALG_CENTER,20,120,"NO TICKETS DATA");
		}
	}
	if(cms_all_flags_struct_var->etimLockedInt || cms_all_flags_struct_var->waybillExpiredFlagInt || cms_all_flags_struct_var->collectionFlag)
		lcd_printf_ex(ALG_CENTER,20,120,"ETIM LOCKED");

	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", ptm->tm_mday, ptm->tm_mon + 1, ptm->tm_year + 1900,wd(ptm->tm_year + 1900,ptm->tm_mon + 1, ptm->tm_mday));
	lcd_flip();
	//	if(ptm!=NULL)
	//		free(ptm);
	return 1;
}


/********************************************************************************
 *  Function Name:  Display_Change_Stage_Screen
 *  Description:
 *                  Display for close stage
 *
 *  Parameters: seq no, name, shift lvl			    RETURN VALUE    : 1
 *
 *********************************************************************************/
int Display_Change_Stage_Screen(char *selectedseqno, char *selectedname,int key,int *shiftKeyLevel)
{
	int reqKey=0;

	lcd_clean();
	if(key!=0)
	{
		if((key==KEY_SYM_BACKSPACE)&&(strlen(selectedseqno)>1)&&(strlen(selectedseqno)<=4))
		{
			if(NULL!=strchr(selectedseqno,'|'))
			{
				selectedseqno[strlen(selectedseqno)-2]='|';
				selectedseqno[strlen(selectedseqno)-1]='\0';
			}
			else
				selectedseqno[strlen(selectedseqno)-1]='|';
		}
		else if((strlen(selectedseqno)>0)&&(strlen(selectedseqno)<4)&&(*shiftKeyLevel==0))
		{
			if(NULL!=strchr(selectedseqno,'|'))
				sprintf(selectedseqno+(strlen(selectedseqno)-1),"%c|",key);
		}
		else if((strlen(selectedseqno)>=0)&&(strlen(selectedseqno)<4)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5))
		{
			reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
			if(NULL!=strchr(selectedseqno,'|'))
				sprintf(selectedseqno+(strlen(selectedseqno)-1),"%c|",reqKey);
		}
		else if((strlen(selectedseqno)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5))
		{
			reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
			sprintf(selectedseqno+(strlen(selectedseqno)-1),"%c|",reqKey);
		}
		else if((strlen(selectedseqno)==0)&&(*shiftKeyLevel==0))
			sprintf(selectedseqno+(strlen(selectedseqno)),"%c|",key);
		if(strlen(selectedseqno)==4)
		{
			selectedseqno[strlen(selectedseqno)-1]='\0';
		}
	}
	//	lcd_printf_ex(ALG_LEFT,20,10,"SELECTED STOP : %s",selectedseqno);//"From Bus Stop     nnn   ");
	lcd_printf_ex(ALG_LEFT,20,10,"SELECTED STOP : %d",(atoi(selectedseqno)-temp_offset));//"From Bus Stop     nnn   ");
	lcd_printf_ex(ALG_CENTER,16,40,selectedname);//"From Bus Stop     nnn   ");
	lcd_printf_ex(ALG_LEFT,24,140,"Select Default Start Stage &");
	lcd_printf_ex(ALG_LEFT,24,160,"Press Enter to Proceed ");
	lcd_flip();
	return 1;
}


int Display_Issue_Ticket_Screen1(char* fromStop, char* tillStop,char* fromStopName, char* tillStopName, char* fromStopStage, char* tillStopStage,all_flags_struct* all_flags_struct_var,int key,int *shiftKeyLevel)
{
	int reqKey=0;

	lcd_clean();
	if(key!=0)
	{
		if(!all_flags_struct_var->next_selection_Flag)
		{
#if 0 // FROM STOP DISABLED

			if((key==KEY_SYM_BACKSPACE)&&(strlen(fromStop)>1)&&(strlen(fromStop)<=4))
			{
				if(NULL!=strchr(fromStop,'|'))
				{
					fromStop[strlen(fromStop)-2]='|';  //2-1
					fromStop[strlen(fromStop)-1]='\0';
				}
				else
					fromStop[strlen(fromStop)-1]='|';
			}
			else if((strlen(fromStop)>0)&&(strlen(fromStop)<4)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(fromStop,'|'))
					sprintf(fromStop+(strlen(fromStop)-1),"%c|",key);
			}
			else if((strlen(fromStop)>=0)&&(strlen(fromStop)<4)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(fromStop,'|'))
					sprintf(fromStop+(strlen(fromStop)-1),"%c|",reqKey);
			}
			else if((strlen(fromStop)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(fromStop+(strlen(fromStop)-1),"%c|",reqKey);
			}
			else if((strlen(fromStop)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(fromStop+(strlen(fromStop)),"%c|",key);

			if(strlen(fromStop)==4)
			{
				fromStop[strlen(fromStop)-1]='\0';
			}
#endif
		}
		else if(all_flags_struct_var->next_selection_Flag)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(tillStop)>1)&&(strlen(tillStop)<=4))
			{
				//				if(NULL!=strchr(tillStop,'|'))
				//				{
				//					tillStop[strlen(tillStop)-2]='|';  //2-1
				//					tillStop[strlen(tillStop)-1]='\0';
				//				}
				//				else
				//					tillStop[strlen(tillStop)-1]='|';
			}
			else if((strlen(tillStop)>0)&&(strlen(tillStop)<4)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(tillStop,'|'))
					sprintf(tillStop+(strlen(tillStop)-1),"%c|",key);
			}
			else if((strlen(tillStop)>=0)&&(strlen(tillStop)<4)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(tillStop,'|'))
					sprintf(tillStop+(strlen(tillStop)-1),"%c|",reqKey);
			}
			else if((strlen(tillStop)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(tillStop+(strlen(tillStop)-1),"%c|",reqKey);
			}

			else if((strlen(tillStop)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(tillStop+(strlen(tillStop)),"%c|",key);
			if(strlen(tillStop)==4)
			{
				tillStop[strlen(tillStop)-1]='\0';
			}
		}
	}

	//	lcd_printf_ex(ALG_LEFT,20,10,"From Bus Stop : %s",fromStop);//"From Bus Stop     nnn   ");
	lcd_printf_ex(ALG_LEFT,20,10,"From Bus Stop : %d",(atoi(fromStop) - temp_offset));//"From Bus Stop     nnn   ");
	//	CstcLog_printf("%d-%d=%d",atoi(fromStop),temp_offset,(atoi(fromStop) - temp_offset));

	if(strlen(fromStopStage)>0)
		lcd_printf_ex(ALG_RIGHT,20,10,fromStopStage);//"From Bus Stop     nnn   ");
	lcd_printf_ex(ALG_CENTER,16,30,fromStopName);//"From Bus Stop     nnn   ");

	if(!all_flags_struct_var->pass_selection_Flag)
	{
		//		lcd_printf_ex(ALG_LEFT,20,60,"To Bus Stop : %s",tillStop);//"To Bus Stop       nnn   ");
		lcd_printf_ex(ALG_LEFT,20,60,"To Bus Stop : %d|",(atoi(tillStop) - temp_offset));//"To Bus Stop       nnn   ");
		//		CstcLog_printf("%d-%d=%d",atoi(tillStop),temp_offset,(atoi(tillStop) - temp_offset));


		if(strlen(tillStopStage)>0)
			lcd_printf_ex(ALG_RIGHT,20,60,tillStopStage);//"From Bus Stop     nnn   ");
		lcd_printf_ex(ALG_CENTER,16,80,tillStopName);//"To Bus Stop       nnn   ");
	}
#if 0 // FROM STOP DISABLED
	if(!all_flags_struct_var->next_selection_Flag)
	{
		lcd_printf_ex(ALG_LEFT,24,140,"Select From Bus Stop &  ");
	}
	else
#endif
	{

		if(!all_flags_struct_var->pass_selection_Flag)
			lcd_printf_ex(ALG_LEFT,24,140,"Select To Bus Stop &  ");
	}
	lcd_printf_ex(ALG_LEFT,24,160,"Press Enter to Proceed ");
	lcd_flip();
	return 1;
}


int Display_Issue_Ticket_Screen_LC(char* fromStop, char* tillStop,char* fromStopName, char* tillStopName, char* fromStopStage, char* tillStopStage,all_flags_struct* all_flags_struct_var,int key,int *shiftKeyLevel,int *cursrFlag)
{
	int reqKey=0,preKey=0;
	CstcLog_printf("fromStop = %s tillStop = %s",fromStop,tillStop);
	lcd_clean();
	if(key!=0)
	{
		CstcLog_printf("123.....................");
		if(!all_flags_struct_var->next_selection_Flag)
		{
			if(*cursrFlag==1)
			{
				CstcLog_printf("345............");
#if 1
				if((key==KEY_SYM_BACKSPACE)&&(strlen(fromStop)>1)&&(strlen(fromStop)<=4))
				{
					if(NULL!=strchr(fromStop,'|'))
					{
						fromStop[strlen(fromStop)-2]='|';  //2-1
						fromStop[strlen(fromStop)-1]='\0';
					}
					else
						fromStop[strlen(fromStop)-1]='|';
				}
				else if((strlen(fromStop)>0)&&(strlen(fromStop)<4)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				{
					if(NULL!=strchr(fromStop,'|'))
						sprintf(fromStop+(strlen(fromStop)-1),"%c|",key);
				}
				else if((strlen(fromStop)>=0)&&(strlen(fromStop)<4)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
				{
					preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
					reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
					if(NULL!=strchr(fromStop,'|'))
					{
						if(fromStop[(strlen(fromStop)-2)]==preKey)
							sprintf(fromStop+(strlen(fromStop)-2),"%c|",reqKey);
						else
							sprintf(fromStop+(strlen(fromStop)-1),"%c|",reqKey);
					}
				}
				else if((strlen(fromStop)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
				{
					reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
					sprintf(fromStop+(strlen(fromStop)-1),"%c|",reqKey);
				}
				else if((strlen(fromStop)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
					sprintf(fromStop+(strlen(fromStop)),"%c|",key);

				if(strlen(fromStop)==4)
				{
					fromStop[strlen(fromStop)-1]='\0';
					*cursrFlag=2;
					tillStop[strlen(tillStop)]='|';
				}
#endif
			}
		}
		else if(all_flags_struct_var->next_selection_Flag)
		{
			if(*cursrFlag==2)
			{
				if((key==KEY_SYM_BACKSPACE)&&(strlen(tillStop)>1)&&(strlen(tillStop)<=4))
				{
					if(NULL!=strchr(tillStop,'|'))
					{
						tillStop[strlen(tillStop)-2]='|';  //2-1
						tillStop[strlen(tillStop)-1]='\0';
					}
					else
						tillStop[strlen(tillStop)-1]='|';
				}
				else if((strlen(tillStop)>0)&&(strlen(tillStop)<4)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				{
					if(NULL!=strchr(tillStop,'|'))
						sprintf(tillStop+(strlen(tillStop)-1),"%c|",key);
				}
				else if((strlen(tillStop)>=0)&&(strlen(tillStop)<4)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
				{
					preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
					reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
					if(NULL!=strchr(tillStop,'|'))
					{
						if(tillStop[(strlen(tillStop)-2)]==preKey)
							sprintf(tillStop+(strlen(tillStop)-2),"%c|",reqKey);
						else
							sprintf(tillStop+(strlen(tillStop)-1),"%c|",reqKey);
					}
				}
				else if((strlen(tillStop)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
				{
					reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
					sprintf(tillStop+(strlen(tillStop)-1),"%c|",reqKey);
				}

				else if((strlen(tillStop)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
					sprintf(tillStop+(strlen(tillStop)),"%c|",key);
				if(strlen(tillStop)==4)
				{
					tillStop[strlen(tillStop)-1]='\0';
				}
			}
		}
	}

	//	lcd_printf_ex(ALG_LEFT,20,10,"From Bus Stop : %s",fromStop);//"From Bus Stop     nnn   ");

	if(!all_flags_struct_var->next_selection_Flag)  // to solve cursor problem
		lcd_printf_ex(ALG_LEFT,20,10,"From Bus Stop : %d|",(atoi(fromStop) - temp_offset));//"From Bus Stop     nnn   ");
	else if(all_flags_struct_var->next_selection_Flag)
		lcd_printf_ex(ALG_LEFT,20,10,"From Bus Stop : %d",(atoi(fromStop) - temp_offset));//"From Bus Stop     nnn   ");
	//	CstcLog_printf("%d-%d=%d",atoi(fromStop),temp_offset,(atoi(fromStop) - temp_offset));

	if(strlen(fromStopStage)>0)
		lcd_printf_ex(ALG_RIGHT,20,10,fromStopStage);//"From Bus Stop     nnn   ");
	lcd_printf_ex(ALG_CENTER,16,30,fromStopName);//"From Bus Stop     nnn   ");

	if(!all_flags_struct_var->pass_selection_Flag)
	{
		//		lcd_printf_ex(ALG_LEFT,20,60,"To Bus Stop : %s",tillStop);//"To Bus Stop       nnn   ");

		if(all_flags_struct_var->next_selection_Flag) // to solve cursor problem
			lcd_printf_ex(ALG_LEFT,20,60,"To Bus Stop : %d|",(atoi(tillStop) - temp_offset));//"To Bus Stop       nnn
		else if(!all_flags_struct_var->next_selection_Flag)
			lcd_printf_ex(ALG_LEFT,20,60,"To Bus Stop : %d",(atoi(tillStop) - temp_offset));//"To Bus Stop       nnn   ");
		//		CstcLog_printf("%d-%d=%d",atoi(tillStop),temp_offset,(atoi(tillStop) - temp_offset));

		if(strlen(tillStopStage)>0)
			lcd_printf_ex(ALG_RIGHT,20,60,tillStopStage);//"From Bus Stop     nnn   ");
		lcd_printf_ex(ALG_CENTER,16,80,tillStopName);//"To Bus Stop       nnn   ");
	}
#if 1
	if(!all_flags_struct_var->next_selection_Flag)
	{
		lcd_printf_ex(ALG_LEFT,24,140,"Select From Bus Stop &  ");
	}
	else
#endif
	{

		if(!all_flags_struct_var->pass_selection_Flag)
			lcd_printf_ex(ALG_LEFT,24,140,"Select To Bus Stop &  ");
	}
	lcd_printf_ex(ALG_LEFT,24,160,"Press Enter to Proceed ");
	lcd_flip();
	return 1;
}

int Display_Issue_Pass_Screen1(char* fromStop,char* fromStopName,all_flags_struct* all_flags_struct_var,int key,int *shiftKeyLevel)
{
	int reqKey=0;

	lcd_clean();
	if(key!=0)
	{
		if(!all_flags_struct_var->next_selection_Flag)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(fromStop)>1)&&(strlen(fromStop)<=4))
			{
				if(NULL!=strchr(fromStop,'|'))
				{
					fromStop[strlen(fromStop)-2]='|';
					fromStop[strlen(fromStop)-1]='\0';
				}
				else
					fromStop[strlen(fromStop)-1]='|';
			}
			else if((strlen(fromStop)>0)&&(strlen(fromStop)<4)&&(*shiftKeyLevel==0))
			{
				if(NULL!=strchr(fromStop,'|'))
					sprintf(fromStop+(strlen(fromStop)-1),"%c|",key);
			}
			else if((strlen(fromStop)>=0)&&(strlen(fromStop)<4)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(fromStop,'|'))
					sprintf(fromStop+(strlen(fromStop)-1),"%c|",reqKey);
			}
			else if((strlen(fromStop)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(fromStop+(strlen(fromStop)-1),"%c|",reqKey);
			}
			else if((strlen(fromStop)==0)&&(*shiftKeyLevel==0))
				sprintf(fromStop+(strlen(fromStop)),"%c|",key);
			if(strlen(fromStop)==4)
			{
				fromStop[strlen(fromStop)-1]='\0';
			}
		}

		lcd_printf_ex(ALG_LEFT,24,10,"From bus Stop : %s",fromStop);//"From Bus Stop     nnn   ");
		lcd_printf_ex(ALG_CENTER,24,30,fromStopName);//"From Bus Stop     nnn   ");

		if(!all_flags_struct_var->next_selection_Flag)
		{
			lcd_printf_ex(ALG_LEFT,24,140,"Select From Bus Stop &  ");
		}
		else
		{
			;//lcd_printf_ex(ALG_LEFT,24,140,"Select Till Bus Stop &  ");
		}
		lcd_printf_ex(ALG_LEFT,24,160,"Press Enter to Proceed ");
		lcd_flip();

	}
	return 1;
}

int Display_Issue_Ticket_Screen_Two1(char* fromStop, char* tillStop, char* fromStopName, char* tillStopName,ticket_display_struct* ticket_display_struct_var,all_flags_struct* all_flags_struct_var,int *cursrFlag,int key, int *ticketInit)
{
	int totalAmtInt = 0;
	int totalLuggInt = 0;

	CstcLog_printf("In Display_Issue_Ticket_Screen_Two1");

	CstcLog_printf("fromStop %s", fromStop);
	CstcLog_printf("tillStop %s", tillStop);
	CstcLog_printf("fromStopName %s", fromStopName);
	CstcLog_printf("tillStopName %s", tillStopName);
	CstcLog_printf("cursrFlag %d",*cursrFlag);
	CstcLog_printf("key %d",key);
	CstcLog_printf("toll_amt= %s ",fare_chart_struct_var.toll_amt);


	if(key!=0)
	{
		CstcLog_printf("0....................display ticketInit = %d",*ticketInit);

		if(*cursrFlag==1)
		{
			CstcLog_printf("1....................");
			CstcLog_printf("strlen %d ",strlen(ticket_display_struct_var->adultCnt));
			CstcLog_printf("2....................");

			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->adultCnt)>1)&&(strlen(ticket_display_struct_var->adultCnt)<=2))
			{
				CstcLog_printf("backspace........");

				CstcLog_printf("1. adult : %s / key = %c",ticket_display_struct_var->adultCnt, key);
				CstcLog_printf("4....................");
				if(NULL!=strchr(ticket_display_struct_var->adultCnt,'|'))
				{
					CstcLog_printf("5....................");

					ticket_display_struct_var->adultCnt[strlen(ticket_display_struct_var->adultCnt)-2]='|';
					ticket_display_struct_var->adultCnt[strlen(ticket_display_struct_var->adultCnt)-1]='\0';
				}
				else
					ticket_display_struct_var->adultCnt[strlen(ticket_display_struct_var->adultCnt)-1]='|';
			}
			else if((strlen(ticket_display_struct_var->adultCnt)>0)&&(strlen(ticket_display_struct_var->adultCnt)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				CstcLog_printf("2. adult : %s / key = %c",ticket_display_struct_var->adultCnt, key);
				if((NULL!=strstr(ticket_display_struct_var->adultCnt,"1|"))&&(*ticketInit==0))
				{
					sprintf(ticket_display_struct_var->adultCnt+(strlen(ticket_display_struct_var->adultCnt)-2),"%c|",key);
					*ticketInit =1;
				}
				else if(NULL!=strstr(ticket_display_struct_var->adultCnt,"0|"))
					sprintf(ticket_display_struct_var->adultCnt+(strlen(ticket_display_struct_var->adultCnt)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->adultCnt,'|'))
					sprintf(ticket_display_struct_var->adultCnt+(strlen(ticket_display_struct_var->adultCnt)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->adultCnt)==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				CstcLog_printf("3. adult : %s / key = %c",ticket_display_struct_var->adultCnt, key);
				sprintf(ticket_display_struct_var->adultCnt+(strlen(ticket_display_struct_var->adultCnt)),"%c|",key);
			}
			if(strlen(ticket_display_struct_var->adultCnt)>=2)
			{
				CstcLog_printf("4. adult : %s / key = %c",ticket_display_struct_var->adultCnt, key);
				if(strlen(ticket_display_struct_var->adultCnt)==2)
					ticket_display_struct_var->adultCnt[strlen(ticket_display_struct_var->adultCnt)]='\0';
				else
					ticket_display_struct_var->adultCnt[2]='\0';
			}
		}
		else if(*cursrFlag==2)
		{
			CstcLog_printf("cursrFlag_2= %d",cursrFlag);
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->childCnt)>1)&&(strlen(ticket_display_struct_var->childCnt)<=2))
			{
				CstcLog_printf("1. child : %s / key = %c",ticket_display_struct_var->childCnt, key);

				if(NULL!=strchr(ticket_display_struct_var->childCnt,'|'))
				{
					ticket_display_struct_var->childCnt[strlen(ticket_display_struct_var->childCnt)-2]='|';
					ticket_display_struct_var->childCnt[strlen(ticket_display_struct_var->childCnt)-1]='\0';
				}
				else
					ticket_display_struct_var->childCnt[strlen(ticket_display_struct_var->childCnt)-1]='|';
			}
			else if((strlen(ticket_display_struct_var->childCnt)>0)&&(strlen(ticket_display_struct_var->childCnt)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				CstcLog_printf("2. child : %s / key = %c",ticket_display_struct_var->childCnt, key);

				if(NULL!=strstr(ticket_display_struct_var->childCnt,"0|"))
					sprintf(ticket_display_struct_var->childCnt+(strlen(ticket_display_struct_var->childCnt)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->childCnt,'|'))
					sprintf(ticket_display_struct_var->childCnt+(strlen(ticket_display_struct_var->childCnt)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->childCnt)==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				CstcLog_printf("3. child : %s / key = %c",ticket_display_struct_var->childCnt, key);
				sprintf(ticket_display_struct_var->childCnt+(strlen(ticket_display_struct_var->childCnt)),"%c|",key);
			}
			if(strlen(ticket_display_struct_var->childCnt)>=2)
			{
				CstcLog_printf("4. child : %s / key = %c",ticket_display_struct_var->childCnt, key);
				if(strlen(ticket_display_struct_var->childCnt)==2)
					ticket_display_struct_var->childCnt[strlen(ticket_display_struct_var->childCnt)]='\0';
				else
					ticket_display_struct_var->childCnt[2]='\0';
			}
		}
		/*else if(*cursrFlag==3)
		{
			CstcLog_printf("cursrFlag_3= %d",cursrFlag);
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->srCitizenCnt)>1)&&(strlen(ticket_display_struct_var->srCitizenCnt)<=2))
			{
				CstcLog_printf("1. srCitizenCnt : %s / key = %c",ticket_display_struct_var->srCitizenCnt, key);

				if(NULL!=strchr(ticket_display_struct_var->srCitizenCnt,'|'))
				{
					ticket_display_struct_var->srCitizenCnt[strlen(ticket_display_struct_var->srCitizenCnt)-2]='|';
					ticket_display_struct_var->srCitizenCnt[strlen(ticket_display_struct_var->srCitizenCnt)-1]='\0';
				}
				else
					ticket_display_struct_var->srCitizenCnt[strlen(ticket_display_struct_var->srCitizenCnt)-1]='|';
			}
			else if((strlen(ticket_display_struct_var->srCitizenCnt)>0)&&(strlen(ticket_display_struct_var->srCitizenCnt)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				CstcLog_printf("2. srCitizenCnt : %s / key = %c",ticket_display_struct_var->srCitizenCnt, key);

				if(NULL!=strstr(ticket_display_struct_var->srCitizenCnt,"0|"))
					sprintf(ticket_display_struct_var->srCitizenCnt+(strlen(ticket_display_struct_var->srCitizenCnt)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->srCitizenCnt,'|'))
					sprintf(ticket_display_struct_var->srCitizenCnt+(strlen(ticket_display_struct_var->srCitizenCnt)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->srCitizenCnt)==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				CstcLog_printf("3. srCitizenCnt : %s / key = %c",ticket_display_struct_var->srCitizenCnt, key);
				sprintf(ticket_display_struct_var->srCitizenCnt+(strlen(ticket_display_struct_var->srCitizenCnt)),"%c|",key);
			}
			if(strlen(ticket_display_struct_var->srCitizenCnt)>=2)
			{
				CstcLog_printf("4. srCitizenCnt : %s / key = %c",ticket_display_struct_var->srCitizenCnt, key);
				if(strlen(ticket_display_struct_var->srCitizenCnt)==2)
					ticket_display_struct_var->srCitizenCnt[strlen(ticket_display_struct_var->srCitizenCnt)]='\0';
				else
					ticket_display_struct_var->srCitizenCnt[2]='\0';

			}
		}
		else if(*cursrFlag==4)
		{
			CstcLog_printf("cursrFlag_4= %d",cursrFlag);
			key = 0;
		}
		*/


		CstcLog_printf("Before totalAmt");
		CstcLog_printf("Adult_cnt= %s",ticket_display_struct_var->adultCnt);
		CstcLog_printf("Student_cnt= %s",ticket_display_struct_var->childCnt);


		totalAmtInt = 0;
		memset(ticket_display_struct_var->totalAmt,0,(AMOUNT_LEN+1));

		if((ticket_display_struct_var->adultCnt)!=NULL)
		{
			CstcLog_printf("adult_fare= %s",fare_chart_struct_var.adult_fare);

			if(NULL!=strchr(ticket_display_struct_var->adultCnt,'|'))
			{
				ticket_display_struct_var->adultCnt[strlen(ticket_display_struct_var->adultCnt)-1]='\0';

				if(all_flags_struct_var->penalty_ticket_flag == 1)
				{
					CstcLog_printf("penalty ticket..");

					if((atof(fare_chart_struct_var.adult_fare)*atof(ticket_display_struct_var->adultCnt)*10) > atof(ticket_display_struct_var->adultCnt)*500)
						totalAmtInt += roundNo(atof(ticket_display_struct_var->adultCnt)*500);
					else
						totalAmtInt+=roundNo(atof(fare_chart_struct_var.adult_fare)*atof(ticket_display_struct_var->adultCnt)*10);
				}
				else
				{
					CstcLog_printf("Not a penalty ticket..");
					CstcLog_printf("adult_cnt= %s",ticket_display_struct_var->adultCnt);
					CstcLog_printf("adult_fare= %s",fare_chart_struct_var.adult_fare);
					totalAmtInt+=roundNo(atof(fare_chart_struct_var.adult_fare)*atof(ticket_display_struct_var->adultCnt));

				}
				ticket_display_struct_var->adultCnt[strlen(ticket_display_struct_var->adultCnt)]='|';
			}
			else
			{
				if(all_flags_struct_var->penalty_ticket_flag == 1)
				{
					if((atof(fare_chart_struct_var.adult_fare)*atof(ticket_display_struct_var->adultCnt)*10) > atof(ticket_display_struct_var->adultCnt)*500)
						totalAmtInt +=roundNo(atof(ticket_display_struct_var->adultCnt)*500);
					else
						totalAmtInt+=roundNo(atof(fare_chart_struct_var.adult_fare)*atof(ticket_display_struct_var->adultCnt)*10);
				}else
					totalAmtInt+=roundNo(atof(fare_chart_struct_var.adult_fare)*atof(ticket_display_struct_var->adultCnt));
			}
		}
		if((ticket_display_struct_var->childCnt)!=NULL)
		{
			CstcLog_printf("child_fare= %s",fare_chart_struct_var.child_fare);

			if(NULL!=strchr(ticket_display_struct_var->childCnt,'|'))
			{
				ticket_display_struct_var->childCnt[strlen(ticket_display_struct_var->childCnt)-1]='\0';
				if(all_flags_struct_var->penalty_ticket_flag == 1)
				{
					if((atof(fare_chart_struct_var.child_fare)*atof(ticket_display_struct_var->childCnt)*10) > atof(ticket_display_struct_var->childCnt)*500)
						totalAmtInt += roundNo(atof(ticket_display_struct_var->childCnt)*500);
					else
						totalAmtInt+=roundNo(atof(fare_chart_struct_var.child_fare)*atof(ticket_display_struct_var->childCnt)*10);
				}
				else
					totalAmtInt+=roundNo(atof(fare_chart_struct_var.child_fare)*atof(ticket_display_struct_var->childCnt));

				ticket_display_struct_var->childCnt[strlen(ticket_display_struct_var->childCnt)]='|';
			}
			else
			{
				if(all_flags_struct_var->penalty_ticket_flag == 1)
				{
					if((atof(fare_chart_struct_var.child_fare)*atof(ticket_display_struct_var->childCnt)*10) > atof(ticket_display_struct_var->childCnt)*500)
						totalAmtInt += roundNo(atof(ticket_display_struct_var->childCnt)*500);
					else
						totalAmtInt+=roundNo(atof(fare_chart_struct_var.child_fare)*atof(ticket_display_struct_var->childCnt)*10);
				}
				else
					totalAmtInt+=roundNo(atof(fare_chart_struct_var.child_fare)*atof(ticket_display_struct_var->childCnt));
			}
		}


		if((ticket_display_struct_var->srCitizenCnt)!=NULL)
		{
			CstcLog_printf("sr_citizen_fare= %s",fare_chart_struct_var.sr_citizen_fare);
			if(NULL!=strchr(ticket_display_struct_var->srCitizenCnt,'|'))
			{
				ticket_display_struct_var->srCitizenCnt[strlen(ticket_display_struct_var->srCitizenCnt)-1]='\0';
				if(all_flags_struct_var->penalty_ticket_flag == 1)
				{
					if((atof(fare_chart_struct_var.sr_citizen_fare)*atof(ticket_display_struct_var->srCitizenCnt)*10) > atof(ticket_display_struct_var->srCitizenCnt)*500)
						totalAmtInt += roundNo(atof(ticket_display_struct_var->srCitizenCnt)*500);
					else
						totalAmtInt+=roundNo(atof(fare_chart_struct_var.sr_citizen_fare)*atof(ticket_display_struct_var->srCitizenCnt)*10);
				}
				else
					totalAmtInt+=roundNo(atof(fare_chart_struct_var.sr_citizen_fare)*atof(ticket_display_struct_var->srCitizenCnt));

				ticket_display_struct_var->srCitizenCnt[strlen(ticket_display_struct_var->srCitizenCnt)]='|';
			}
			else
			{
				if(all_flags_struct_var->penalty_ticket_flag == 1)
				{
					if((atof(fare_chart_struct_var.sr_citizen_fare)*atof(ticket_display_struct_var->srCitizenCnt)*10) > atof(ticket_display_struct_var->srCitizenCnt)*500)
						totalAmtInt +=roundNo(atof(ticket_display_struct_var->srCitizenCnt)*500);
					else
						totalAmtInt+=roundNo(atof(fare_chart_struct_var.sr_citizen_fare)*atof(ticket_display_struct_var->srCitizenCnt)*10);
				}else
					totalAmtInt+=roundNo(atof(fare_chart_struct_var.sr_citizen_fare)*atof(ticket_display_struct_var->srCitizenCnt));

			}
		}

		if((ticket_display_struct_var->luggtotalAmt)!=NULL)
		{
			totalLuggInt = 0;
			totalLuggInt= atoi(ticket_display_struct_var->luggtotalAmt);
			CstcLog_printf("totalWithLugg= %f",totalLuggInt);
		}
		CstcLog_printf("Adult= %s Child= %s Sr.citizen= %s",fare_chart_struct_var.adult_fare,fare_chart_struct_var.child_fare,fare_chart_struct_var.sr_citizen_fare);
		CstcLog_printf("After totalAmt= %d",totalAmtInt);

		sprintf(ticket_display_struct_var->totalAmt,"%d",totalAmtInt+totalLuggInt);
	}
	lcd_clean();

	//	lcd_printf_ex(ALG_LEFT,20,10,"FROM STOP : %s",fromStop);
	lcd_printf_ex(ALG_LEFT,20,10,"FROM STOP : %d",(atoi(fromStop) - temp_offset));
	//	CstcLog_printf("%d-%d=%d",atoi(fromStop),temp_offset,(atoi(fromStop) - temp_offset));
	lcd_printf_ex(ALG_RIGHT,16,30,fromStopName);
	//	lcd_printf_ex(ALG_LEFT,20,50,"TO STOP : %s",tillStop);
	lcd_printf_ex(ALG_LEFT,20,50,"TO STOP : %d",(atoi(tillStop) - temp_offset));
	//	CstcLog_printf("%d-%d=%d",atoi(tillStop),temp_offset,(atoi(tillStop) - temp_offset));
	lcd_printf_ex(ALG_RIGHT,16,70,tillStopName);

	//	lcd_printf_ex(ALG_LEFT,20,100,"Adult                 = %s   x  %s",ticket_display_struct_var->adultCnt,fare_chart_struct_var.adult_fare);//"Adult         =1    ");

	lcd_printf_ex(ALG_LEFT,20,100,"Adult");
	lcd_printf_xy(20,130,100,"=");
	lcd_printf_xy(20,150,100,"%s",ticket_display_struct_var->adultCnt);
	lcd_printf_xy(20,180,100,"x   %s",fare_chart_struct_var.adult_fare);

	//	lcd_printf_ex(ALG_LEFT,20,120,"Child                  = %s   x  %s",ticket_display_struct_var->childCnt,fare_chart_struct_var.child_fare);//"Child         =2    ");


	/***********redbug:- if concession_ticket don't show student option on Dislay ************/
//  if((atoi(selected_trip_info_struct_var.bus_service_id_qstr)!=2)&&(atoi(selected_trip_info_struct_var.bus_service_id_qstr)!=5)&&(!all_flags_struct_var->concession_flag))
	if(!(all_flags_struct_var->concession_flag))
	{
		lcd_printf_ex(ALG_LEFT,20,120,"Student");
		lcd_printf_xy(20,130,120,"=");
		lcd_printf_xy(20,150,120,"%s",ticket_display_struct_var->childCnt);
		lcd_printf_xy(20,180,120,"x   %s",fare_chart_struct_var.child_fare);
	}

	/*//	lcd_printf_ex(ALG_LEFT,20,140,"Sr. Citizen         = %s   x  %s",ticket_display_struct_var->srCitizenCnt,fare_chart_struct_var.sr_citizen_fare);//"Child         =2    ");
	lcd_printf_ex(ALG_LEFT,20,140,"Sr. Citizen");
	lcd_printf_xy(20,130,140,"=");
	lcd_printf_xy(20,150,140,"%s",ticket_display_struct_var->srCitizenCnt);
	lcd_printf_xy(20,180,140,"x   %s",fare_chart_struct_var.sr_citizen_fare);*/

	//	lcd_printf_ex(ALG_LEFT,20,160,"Luggage            = %s",ticket_display_struct_var->lugg);
//	lcd_printf_ex(ALG_LEFT,20,120,"Luggage");
//	lcd_printf_xy(20,130,120,"=");
//	lcd_printf_xy(20,150,120,"%s",ticket_display_struct_var->lugg);

	//	lcd_printf_ex(ALG_LEFT,20,190,"Total Fare         = %s        ",ticket_display_struct_var->totalAmt);//"(Total)");
	lcd_printf_ex(ALG_LEFT,20,190,"Total Fare");
	lcd_printf_xy(20,130,190,"=");
	lcd_printf_xy(20,150,190,"%d",atoi(ticket_display_struct_var->totalAmt));
	lcd_flip();
	return 1;
}

int Display_Issue_Ticket_Lugg(char* fromStop, char* tillStop, char* fromStopName, char* tillStopName,ticket_display_struct* ticket_display_struct_var,int key, int * cursrFlag)
{
	lcd_clean();
	if(key!=0)
	{
		if(*cursrFlag==1)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->lightWeightKg)>1)&&(strlen(ticket_display_struct_var->lightWeightKg)<=2))
			{
				if(NULL!=strchr(ticket_display_struct_var->lightWeightKg,'|'))
				{
					ticket_display_struct_var->lightWeightKg[strlen(ticket_display_struct_var->lightWeightKg)-2]='|';
					ticket_display_struct_var->lightWeightKg[strlen(ticket_display_struct_var->lightWeightKg)-1]='\0';
				}
				else
					ticket_display_struct_var->lightWeightKg[strlen(ticket_display_struct_var->lightWeightKg)-1]='|';
				CstcLog_printf("Backspace if~");

			}
			else if((strlen(ticket_display_struct_var->lightWeightKg)>0)&&(strlen(ticket_display_struct_var->lightWeightKg)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strstr(ticket_display_struct_var->lightWeightKg,"0|"))
					sprintf(ticket_display_struct_var->lightWeightKg+(strlen(ticket_display_struct_var->lightWeightKg)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->lightWeightKg,'|'))
					sprintf(ticket_display_struct_var->lightWeightKg+(strlen(ticket_display_struct_var->lightWeightKg)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->lightWeightKg)==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(ticket_display_struct_var->lightWeightKg+(strlen(ticket_display_struct_var->lightWeightKg)),"%c|",key);
			if(strlen(ticket_display_struct_var->lightWeightKg)>=2)
			{
				if(strlen(ticket_display_struct_var->lightWeightKg)==2)
					ticket_display_struct_var->lightWeightKg[strlen(ticket_display_struct_var->lightWeightKg)]='\0';
				else
					ticket_display_struct_var->lightWeightKg[2]='\0';
			}

		}
		else if(*cursrFlag==2)
		{
			CstcLog_printf("cursrFlag_2= %d*************",cursrFlag);
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->heavyWeightKg)>1)&&(strlen(ticket_display_struct_var->heavyWeightKg)<=2))
			{
				if(NULL!=strchr(ticket_display_struct_var->heavyWeightKg,'|'))
				{
					ticket_display_struct_var->heavyWeightKg[strlen(ticket_display_struct_var->heavyWeightKg)-2]='|';
					ticket_display_struct_var->heavyWeightKg[strlen(ticket_display_struct_var->heavyWeightKg)-1]='\0';
				}
				else
					ticket_display_struct_var->heavyWeightKg[strlen(ticket_display_struct_var->heavyWeightKg)-1]='|';
				CstcLog_printf("Backspace if 2~");
			}
			else if((strlen(ticket_display_struct_var->heavyWeightKg)>0)&&(strlen(ticket_display_struct_var->heavyWeightKg)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strstr(ticket_display_struct_var->heavyWeightKg,"0|"))
					sprintf(ticket_display_struct_var->heavyWeightKg+(strlen(ticket_display_struct_var->heavyWeightKg)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->heavyWeightKg,'|'))
					sprintf(ticket_display_struct_var->heavyWeightKg+(strlen(ticket_display_struct_var->heavyWeightKg)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->heavyWeightKg)==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(ticket_display_struct_var->heavyWeightKg+(strlen(ticket_display_struct_var->heavyWeightKg)),"%c|",key);
			if(strlen(ticket_display_struct_var->heavyWeightKg)>=2)
			{
				if(strlen(ticket_display_struct_var->heavyWeightKg)==2)
					ticket_display_struct_var->heavyWeightKg[strlen(ticket_display_struct_var->heavyWeightKg)]='\0';
				else
					ticket_display_struct_var->heavyWeightKg[2]='\0';
			}
			CstcLog_printf("**********************************");
		}
		else if(*cursrFlag==3)
		{
			CstcLog_printf("cursrFlag_3= %d*****************",cursrFlag);
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->tvElectKg)>1)&&(strlen(ticket_display_struct_var->tvElectKg)<=2))
			{
				if(NULL!=strchr(ticket_display_struct_var->tvElectKg,'|'))
				{
					ticket_display_struct_var->tvElectKg[strlen(ticket_display_struct_var->tvElectKg)-2]='|';
					ticket_display_struct_var->tvElectKg[strlen(ticket_display_struct_var->tvElectKg)-1]='\0';
				}
				else
					ticket_display_struct_var->tvElectKg[strlen(ticket_display_struct_var->tvElectKg)-1]='|';
				CstcLog_printf("Backspace if 3~");
			}
			else if((strlen(ticket_display_struct_var->tvElectKg)>0)&&(strlen(ticket_display_struct_var->tvElectKg)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strstr(ticket_display_struct_var->tvElectKg,"0|"))
					sprintf(ticket_display_struct_var->tvElectKg+(strlen(ticket_display_struct_var->tvElectKg)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->tvElectKg,'|'))
					sprintf(ticket_display_struct_var->tvElectKg+(strlen(ticket_display_struct_var->tvElectKg)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->tvElectKg)==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(ticket_display_struct_var->tvElectKg+(strlen(ticket_display_struct_var->tvElectKg)),"%c|",key);
			if(strlen(ticket_display_struct_var->tvElectKg)>=2)
			{
				if(strlen(ticket_display_struct_var->tvElectKg)==2)
					ticket_display_struct_var->tvElectKg[strlen(ticket_display_struct_var->tvElectKg)]='\0';
				else
					ticket_display_struct_var->tvElectKg[2]='\0';
			}
			CstcLog_printf("**********************************");
		}
		else if(*cursrFlag==4)
		{
			CstcLog_printf("cursrFlag_4= %d****************",cursrFlag);
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->petAnimalsDogCnt)>1)&&(strlen(ticket_display_struct_var->petAnimalsDogCnt)<=2))
			{
				if(NULL!=strchr(ticket_display_struct_var->petAnimalsDogCnt,'|'))
				{
					ticket_display_struct_var->petAnimalsDogCnt[strlen(ticket_display_struct_var->petAnimalsDogCnt)-2]='|';
					ticket_display_struct_var->petAnimalsDogCnt[strlen(ticket_display_struct_var->petAnimalsDogCnt)-1]='\0';
				}
				else
					ticket_display_struct_var->petAnimalsDogCnt[strlen(ticket_display_struct_var->petAnimalsDogCnt)-1]='|';
				CstcLog_printf("Backspace if 4~");
			}
			else if((strlen(ticket_display_struct_var->petAnimalsDogCnt)>0)&&(strlen(ticket_display_struct_var->petAnimalsDogCnt)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strstr(ticket_display_struct_var->petAnimalsDogCnt,"0|"))
					sprintf(ticket_display_struct_var->petAnimalsDogCnt+(strlen(ticket_display_struct_var->petAnimalsDogCnt)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->petAnimalsDogCnt,'|'))
					sprintf(ticket_display_struct_var->petAnimalsDogCnt+(strlen(ticket_display_struct_var->petAnimalsDogCnt)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->petAnimalsDogCnt)==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(ticket_display_struct_var->petAnimalsDogCnt+(strlen(ticket_display_struct_var->petAnimalsDogCnt)),"%c|",key);
			if(strlen(ticket_display_struct_var->petAnimalsDogCnt)>=2)
			{
				if(strlen(ticket_display_struct_var->petAnimalsDogCnt)==2)
					ticket_display_struct_var->petAnimalsDogCnt[strlen(ticket_display_struct_var->petAnimalsDogCnt)]='\0';
				else
					ticket_display_struct_var->petAnimalsDogCnt[2]='\0';
			}
			CstcLog_printf("**********************************");
		}
		else if(*cursrFlag==5)
		{
			CstcLog_printf("cursrFlag_5= %d*****************",cursrFlag);
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->petAnimalsBirdsCnt)>1)&&(strlen(ticket_display_struct_var->petAnimalsBirdsCnt)<=2))
			{
				if(NULL!=strchr(ticket_display_struct_var->petAnimalsBirdsCnt,'|'))
				{
					ticket_display_struct_var->petAnimalsBirdsCnt[strlen(ticket_display_struct_var->petAnimalsBirdsCnt)-2]='|';
					ticket_display_struct_var->petAnimalsBirdsCnt[strlen(ticket_display_struct_var->petAnimalsBirdsCnt)-1]='\0';
				}
				else
					ticket_display_struct_var->petAnimalsBirdsCnt[strlen(ticket_display_struct_var->petAnimalsBirdsCnt)-1]='|';
				CstcLog_printf("Backspace if 5~");
			}
			else if((strlen(ticket_display_struct_var->petAnimalsBirdsCnt)>0)&&(strlen(ticket_display_struct_var->petAnimalsBirdsCnt)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strstr(ticket_display_struct_var->petAnimalsBirdsCnt,"0|"))
					sprintf(ticket_display_struct_var->petAnimalsBirdsCnt+(strlen(ticket_display_struct_var->petAnimalsBirdsCnt)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->petAnimalsBirdsCnt,'|'))
					sprintf(ticket_display_struct_var->petAnimalsBirdsCnt+(strlen(ticket_display_struct_var->petAnimalsBirdsCnt)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->petAnimalsBirdsCnt)==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(ticket_display_struct_var->petAnimalsBirdsCnt+(strlen(ticket_display_struct_var->petAnimalsBirdsCnt)),"%c|",key);
			if(strlen(ticket_display_struct_var->petAnimalsBirdsCnt)>=2)
			{
				if(strlen(ticket_display_struct_var->petAnimalsBirdsCnt)==2)
					ticket_display_struct_var->petAnimalsBirdsCnt[strlen(ticket_display_struct_var->petAnimalsBirdsCnt)]='\0';
				else
					ticket_display_struct_var->petAnimalsBirdsCnt[2]='\0';
			}
			CstcLog_printf("**********************************");
		}
		else if(*cursrFlag==6)
		{
			CstcLog_printf("cursrFlag_6= %d*********************",cursrFlag);
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_display_struct_var->newsPaperKg)>1)&&(strlen(ticket_display_struct_var->newsPaperKg)<=2))
			{
				if(NULL!=strchr(ticket_display_struct_var->newsPaperKg,'|'))
				{
					ticket_display_struct_var->newsPaperKg[strlen(ticket_display_struct_var->newsPaperKg)-2]='|';
					ticket_display_struct_var->newsPaperKg[strlen(ticket_display_struct_var->newsPaperKg)-1]='\0';
				}
				else
					ticket_display_struct_var->newsPaperKg[strlen(ticket_display_struct_var->newsPaperKg)-1]='|';
				CstcLog_printf("Backspace if 6~");
			}
			else if((strlen(ticket_display_struct_var->newsPaperKg)>0)&&(strlen(ticket_display_struct_var->newsPaperKg)<=2)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strstr(ticket_display_struct_var->newsPaperKg,"0|"))
					sprintf(ticket_display_struct_var->newsPaperKg+(strlen(ticket_display_struct_var->newsPaperKg)-2),"%c|",key);
				else if(NULL!=strchr(ticket_display_struct_var->newsPaperKg,'|'))
					sprintf(ticket_display_struct_var->newsPaperKg+(strlen(ticket_display_struct_var->newsPaperKg)-1),"%c|",key);
			}
			else if((strlen(ticket_display_struct_var->newsPaperKg)==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(ticket_display_struct_var->newsPaperKg+(strlen(ticket_display_struct_var->newsPaperKg)),"%c|",key);
			if(strlen(ticket_display_struct_var->newsPaperKg)>=2)
			{
				if(strlen(ticket_display_struct_var->newsPaperKg)==2)
					ticket_display_struct_var->newsPaperKg[strlen(ticket_display_struct_var->newsPaperKg)]='\0';
				else
					ticket_display_struct_var->newsPaperKg[2]='\0';
			}
			CstcLog_printf("**********************************");
		}

	}

	lcd_printf_ex(ALG_LEFT,20,10,"Light Weight (units)");
	lcd_printf_xy(20,200,10,"=");
	//lcd_printf_xy(20,220,10,"%s  x  %s",ticket_display_struct_var->lightWeightKg,fare_chart_struct_var.luggage_amt_per_unit);
	lcd_printf_xy(20,220,10,"%s",ticket_display_struct_var->lightWeightKg);
	lcd_printf_xy(20,250,10,"x   %s",fare_chart_struct_var.luggage_amt_per_unit);

	lcd_printf_ex(ALG_LEFT,20,35,"Heavy Weight (units)");
	lcd_printf_xy(20,200,35,"=");
	//	lcd_printf_xy(20,220,35,"%s  x  %s",ticket_display_struct_var->heavyWeightKg,fare_chart_struct_var.luggage_amt_per_unit);
	lcd_printf_xy(20,220,35,"%s",ticket_display_struct_var->heavyWeightKg);
	lcd_printf_xy(20,250,35,"x   %s",fare_chart_struct_var.luggage_amt_per_unit);

	lcd_printf_ex(ALG_LEFT,20,60,"Electronics (units)");
	lcd_printf_xy(20,200,60,"=");
	//	lcd_printf_xy(20,220,60,"%s  x  %s",ticket_display_struct_var->tvElectKg,fare_chart_struct_var.luggage_amt_per_unit);
	lcd_printf_xy(20,220,60,"%s",ticket_display_struct_var->tvElectKg);
	lcd_printf_xy(20,250,60,"x   %s",fare_chart_struct_var.luggage_amt_per_unit);

	lcd_printf_ex(ALG_LEFT,20,85,"Pet Dog");
	lcd_printf_xy(20,200,85,"=");
	//	lcd_printf_xy(20,220,85,"%s  x  %s",ticket_display_struct_var->petAnimalsDogCnt,fare_chart_struct_var.adult_fare);
	lcd_printf_xy(20,220,85,"%s",ticket_display_struct_var->petAnimalsDogCnt);
	lcd_printf_xy(20,250,85,"x   %d",atoi(fare_chart_struct_var.adult_fare)-atoi(fare_chart_struct_var.toll_amt));

	lcd_printf_ex(ALG_LEFT,20,110,"Pet Bird");
	lcd_printf_xy(20,200,110,"=");
	//	lcd_printf_xy(20,220,110,"%s  x  %s",ticket_display_struct_var->petAnimalsBirdsCnt,fare_chart_struct_var.child_fare);
	lcd_printf_xy(20,220,110,"%s",ticket_display_struct_var->petAnimalsBirdsCnt);
	lcd_printf_xy(20,250,110,"x   %d",atoi(fare_chart_struct_var.child_fare)-atoi(fare_chart_struct_var.toll_amt));

	lcd_printf_ex(ALG_LEFT,20,135,"Newspaper (units)");
	lcd_printf_xy(20,200,135,"=");
	//	lcd_printf_xy(20,220,135,"%s  x  %s",ticket_display_struct_var->newsPaperKg,fare_chart_struct_var.luggage_amt_per_unit);
	lcd_printf_xy(20,220,135,"%s",ticket_display_struct_var->newsPaperKg);
	lcd_printf_xy(20,250,135,"x   %s",fare_chart_struct_var.luggage_amt_per_unit);

	lcd_flip();
	return 1;
}

int Display_Pass_Details()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,20,40,"1.Student");
	lcd_printf_ex(ALG_LEFT,20,60,"2.Blind");
	lcd_printf_ex(ALG_LEFT,20,140,"3.Physically Challenged");
	lcd_printf_ex(ALG_LEFT,20,160,"4.Freedom Fighter");
	lcd_printf_ex(ALG_LEFT,20,160,"5.Police Duty");
	lcd_printf_ex(ALG_LEFT,20,160,"6.Police Warrant");
	lcd_flip();
	return 1;
}

int Display_Enter_Pass_No()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,20,40,"Pass No.");
	lcd_printf_ex(ALG_CENTER,32,90,"“Enter” to Proceed");
	lcd_flip();
	return 1;
}

int Passanger_Count_Report_One()
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,20,40,"<Current Bus stop>              ");
	lcd_printf_ex(ALG_LEFT,20,60,"Select Bus Stop                nnn");
	lcd_printf_ex(ALG_LEFT,20,100,"Press Enter Key");
	lcd_printf_ex(ALG_LEFT,20,120,"to Proceed");
	lcd_flip();
	return 1;
}

int passanger_Count_Report_Two()
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,20,40,"Total Pass: nn  Tkt: nn   ");
	lcd_printf_ex(ALG_LEFT,20,60,"Bus stop1                   ");
	lcd_printf_ex(ALG_LEFT,20,80,"Pass        : nn, Tkt: nn   ");
	lcd_printf_ex(ALG_LEFT,20,100,"Bus stop2                  ");
	lcd_printf_ex(ALG_LEFT,20,120,"Pass       : nn, Tkt: nn   ");
	lcd_printf_ex(ALG_LEFT,20,150,"Bus stopz  : nnn           ");
	lcd_flip();
	return 1;
}

int Display_Toll_Payment()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,20,40,"Enter Toll Amount");
	lcd_printf_ex(ALG_LEFT,20,90,"“Enter” to store the Txn");
	lcd_printf_ex(ALG_LEFT,20,110,"and Exit");
	lcd_flip();
	return 1;
}

int Recharge_screen()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,32,40,"Please insert card");
	lcd_flip();
	return 1;
}

int Recharge_Screen1()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,20,40,"Card No  :                  ");
	lcd_printf_ex(ALG_LEFT,20,60,"Name     :                  ");
	lcd_printf_ex(ALG_LEFT,20,80,"Balance  :                  ");
	lcd_printf_ex(ALG_LEFT,20,150,"Enter the Amt to Credit:nnn");
	lcd_flip();
	return 1;
}

int Recharge_Screen2()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,20,40,"Amt Credited to the card             ");
	lcd_printf_ex(ALG_LEFT,20,70,"Current Balance  :                   ");
	lcd_printf_ex(ALG_LEFT,20,100,"Press “Cancel” to Exit              ");
	lcd_flip();
	return 1;
}

int Check_Card_Balance()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,32,40,"Please insert card");
	lcd_flip();
	return 1;
}

int Check_Card_Balance1()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,20,40,"Card No  :                  ");
	lcd_printf_ex(ALG_LEFT,20,60,"Name     :                  ");
	lcd_printf_ex(ALG_LEFT,20,80,"Balance  :                  ");
	lcd_printf_ex(ALG_LEFT,20,150,"Press “Cancel” to Exit     ");
	lcd_flip();
	return 1;
}


int Pass_Details_Insert()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,32,40,"Please insert/Show card");
	lcd_flip();
	return 1;
}

int Pass_Details_Insert1()
{
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,20,40,"Card No     :                  ");
	lcd_printf_ex(ALG_LEFT,20,60,"Name        :                  ");
	lcd_printf_ex(ALG_LEFT,20,80,"Card Type   :                  ");
	lcd_printf_ex(ALG_LEFT,20,100,"Valid Till  :                  ");
	lcd_printf_ex(ALG_LEFT,20,150,"Press “Cancel” to Exit     ");
	lcd_flip();
	return 1;
}

int Show_Error_Msg(char* errMsg)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,20,70,errMsg);
	lcd_flip();
	usleep(5000);
	return 1;
}

int Show_Error_Msg_UART(char* errMsg)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,20,70,errMsg);
	lcd_flip();
	return 1;
}


int status_report(int no_of_tickets,char *lugg_tkt_count, char*pass_tkt_count)
{
	int key=0;
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
	CstcLog_printf("In status_report_display");
	lcd_clean();
	lcd_printf(ALG_CENTER,28, "WBTC");
	lcd_printf_ex(ALG_CENTER,20,30,"STATUS REPORT");
	lcd_printf_ex(ALG_CENTER,20,50,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_printf_ex(ALG_LEFT,20,80,"No. Of Pass holder");
	lcd_printf_xy(20,230,80,":");
	lcd_printf_xy(20,270,80,"%s",current_passengers_details_struct_var.pass_count_qstr);
	lcd_printf_ex(ALG_LEFT,20,100,"No. Of Ticket Count");
	lcd_printf_xy(20,230,100,":");
	lcd_printf_xy(20,270,100,"%d",no_of_tickets);//(atoi(current_passengers_details_struct_var.full_count_qstr)+atoi(current_passengers_details_struct_var.half_count_qstr)+atoi(current_passengers_details_struct_var.srctzn_count_qstr)));
	lcd_printf_ex(ALG_LEFT,20,120,"Total No. Of Passenger");
	lcd_printf_xy(20,230,120,":");
	lcd_printf_xy(20,270,120,"%s",current_passengers_details_struct_var.present_passengers_qstr);
/*	lcd_printf_ex(ALG_LEFT,20,140,"Luggage Ticket Count");
	lcd_printf_xy(20,230,140,":");
	lcd_printf_xy(20,270,140,"%s",lugg_tkt_count);
	lcd_printf_ex(ALG_LEFT,20,140,"Passenger Ticket Count ");
	lcd_printf_xy(20,230,140,":");
	lcd_printf_xy(20,270,140,"%s",pass_tkt_count);*/
	lcd_printf_ex(ALG_CENTER,16,190,"PRESS CANCEL KEY TO EXIT ");
	lcd_flip();
	while(1)
	{
		key = Kb_Get_Key_Sym();
		if (key==KEY_SYM_ESCAPE)
		{
			break;
		}
	}
	return 0;
}

#if 0
void last_ticket_display(transaction_log_struct transaction_log_struct_var,ticket_display_struct* ticket_display_struct_var)
{

	int key =0;
#if 1
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,20,5,"From Bus Stop :");
	lcd_printf_ex(ALG_CENTER,16,25,"- %s",transaction_log_struct_var.from_bus_stop_name_english);
	lcd_printf_ex(ALG_LEFT,20,45,"To Bus Stop :");
	lcd_printf_ex(ALG_CENTER,16,65,"- %s",transaction_log_struct_var.till_bus_stop_name_english);

	lcd_printf_ex(ALG_LEFT,20,85,"Adult");//transaction_log_struct_var.px_count
	lcd_printf_xy(20,110,85,":");
	lcd_printf_xy(20,130,85,"%s",ticket_display_struct_var->adultCnt);//transaction_log_struct_var.px_count
	lcd_printf_xy(20,160,85,"x");
	lcd_printf_xy(20,190,85,"%s",fare_chart_struct_var.adult_fare);//transaction_log_struct_var.px_count
	lcd_printf_xy(20,220,85,"=");
	lcd_printf_xy(20,250,85,"%d",roundNo(atof(fare_chart_struct_var.adult_fare)*atof(ticket_display_struct_var->adultCnt)));//transaction_log_struct_var.px_count

	lcd_printf_ex(ALG_LEFT,20,105,"Child");//ticket_display_struct_var.childCnt
	lcd_printf_xy(20,110,105,":");
	lcd_printf_xy(20,130,105,"%s",ticket_display_struct_var->childCnt);//ticket_display_struct_var.childCnt
	lcd_printf_xy(20,160,105,"x");
	lcd_printf_xy(20,190,105,"%s",fare_chart_struct_var.child_fare);//transaction_log_struct_var.px_count
	lcd_printf_xy(20,220,105,"=");
	lcd_printf_xy(20,250,105,"%d",roundNo(atof(fare_chart_struct_var.child_fare)*atof(ticket_display_struct_var->childCnt)));//transaction_log_struct_var.px_count

	lcd_printf_ex(ALG_LEFT,20,125,"Sr Citizen  ");//ticket_display_struct_var.srCitizenCnt       //Correct this by adding total number of concessions and epurse ticket count
	lcd_printf_xy(20,110,125,":");
	lcd_printf_xy(20,130,125,"%s",ticket_display_struct_var->srCitizenCnt);//ticket_display_struct_var.srCitizenCnt       //Correct this by adding total number of concessions and epurse ticket count
	lcd_printf_xy(20,160,125,"x");
	lcd_printf_xy(20,190,125,"%s",fare_chart_struct_var.sr_citizen_fare);//transaction_log_struct_var.px_count
	lcd_printf_xy(20,220,125,"=");
	lcd_printf_xy(20,250,125,"%d",roundNo(atof(fare_chart_struct_var.sr_citizen_fare)*atof(ticket_display_struct_var->srCitizenCnt)));//transaction_log_struct_var.px_count

	lcd_printf_ex(ALG_LEFT,20,145,"Luggage");////transaction_log_struct_var.lugg_units
	lcd_printf_xy(20,110,145,":");
	lcd_printf_xy(20,130,145,"%s",ticket_display_struct_var->lugg);//ticket_display_struct_var.srCitizenCnt       //Correct this by adding total number of concessions and epurse ticket count
	lcd_printf_xy(20,250,145,"%s",ticket_display_struct_var->luggtotalAmt);//transaction_log_struct_var.px_count

	lcd_printf_ex(ALG_LEFT,20,165,"Total");//Correct this by adding total number of concessions and epurse ticket count
	lcd_printf_xy(20,110,165,":");
	lcd_printf_xy(20,250,165,"%s",transaction_log_struct_var.total_ticket_amount);////transaction_log_struct_var.lugg_units
	lcd_printf_ex(ALG_CENTER,16,190,"PRESS CANCEL KEY TO EXIT");
	lcd_flip();

	while(1)
	{
		key = Kb_Get_Key_Sym();
		if (key==KEY_SYM_ESCAPE)
		{
			break;
		}
	}
#endif
}
#endif

int display_conductor_entry_screen(char* conductor_id, char *conductor_name,char *conductor_pwrd,char* conductor_type,char* licence_expiry_date,int key,int *cursrFlag,int *shiftKeyLevel)
{
	int reqKey=0,preKey=0;

	if(key!=0)
	{
		if(*cursrFlag==1)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(conductor_id)>1)&&(strlen(conductor_id)<=(COND_ID_LEN+1)))
			{
				if(NULL!=strchr(conductor_id,'|'))
				{
					conductor_id[strlen(conductor_id)-2]='|';
					conductor_id[strlen(conductor_id)-1]='\0';
				}
				else
					conductor_id[strlen(conductor_id)-1]='|';
			}
			else if((strlen(conductor_id)>0)&&(strlen(conductor_id)<(COND_ID_LEN+1))&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(conductor_id,'|'))
					sprintf(conductor_id+(strlen(conductor_id)-1),"%c|",key);
			}
			else if((strlen(conductor_id)>=0)&&(strlen(conductor_id)<(COND_ID_LEN+1))&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(conductor_id,'|'))
				{
					if(conductor_id[(strlen(conductor_id)-2)]==preKey)
						sprintf(conductor_id+(strlen(conductor_id)-2),"%c|",reqKey);
					else
						sprintf(conductor_id+(strlen(conductor_id)-1),"%c|",reqKey);
				}
			}
			else if((strlen(conductor_id)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(conductor_id+(strlen(conductor_id)-1),"%c|",reqKey);
			}
			else if((strlen(conductor_id)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(conductor_id+(strlen(conductor_id)),"%c|",key);
			//			conductor_id[strlen(conductor_id)]='|';
			if(strlen(conductor_id)==(COND_ID_LEN))
			{
				conductor_id[strlen(conductor_id)]='\0';
				//				*cursrFlag=2;
				//				conductor_name[strlen(conductor_name)]='|';
			}
		}

		else if(*cursrFlag==2)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(conductor_name)>1)&&(strlen(conductor_name)<=(CONDR_NAME_LEN+1)))
			{
				if(NULL!=strchr(conductor_name,'|'))
				{
					conductor_name[strlen(conductor_name)-2]='|';
					conductor_name[strlen(conductor_name)-1]='\0';
				}
				else
					conductor_name[strlen(conductor_name)-1]='|';
			}
			else if((strlen(conductor_name)>0)&&(strlen(conductor_name)<(CONDR_NAME_LEN+1))&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(conductor_name,'|'))
					sprintf(conductor_name+(strlen(conductor_name)-1),"%c|",key);
			}
			else if((strlen(conductor_name)>=0)&&(strlen(conductor_name)<(CONDR_NAME_LEN+1))&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(conductor_name,'|'))
				{
					if(conductor_name[(strlen(conductor_name)-2)]==preKey)
						sprintf(conductor_name+(strlen(conductor_name)-2),"%c|",reqKey);
					else
						sprintf(conductor_name+(strlen(conductor_name)-1),"%c|",reqKey);
				}
			}
			else if((strlen(conductor_name)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(conductor_name+(strlen(conductor_name)-1),"%c|",reqKey);
			}
			//			else
			//				conductor_name[strlen(conductor_name)]='|';
			else if((strlen(conductor_name)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(conductor_name+(strlen(conductor_name)),"%c|",key);
			if(strlen(conductor_name)==(CONDR_NAME_LEN))
			{
				conductor_name[strlen(conductor_name)]='\0';
			}
		}
		else if(*cursrFlag==3)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(conductor_pwrd)>1)&&(strlen(conductor_pwrd)<=(PSWRD_LEN+1)))
			{
				if(NULL!=strchr(conductor_pwrd,'|'))
				{
					conductor_pwrd[strlen(conductor_pwrd)-2]='|';
					conductor_pwrd[strlen(conductor_pwrd)-1]='\0';
				}
				else
					conductor_pwrd[strlen(conductor_pwrd)-1]='|';
			}
			else if((strlen(conductor_pwrd)>0)&&(strlen(conductor_pwrd)<(PSWRD_LEN+1))&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(conductor_pwrd,'|'))
					sprintf(conductor_pwrd+(strlen(conductor_pwrd)-1),"%c|",key);
			}
			else if((strlen(conductor_pwrd)>=0)&&(strlen(conductor_pwrd)<(PSWRD_LEN+1))&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(conductor_pwrd,'|'))
				{
					if(conductor_pwrd[(strlen(conductor_pwrd)-2)]==preKey)
						sprintf(conductor_pwrd+(strlen(conductor_pwrd)-2),"%c|",reqKey);
					else
						sprintf(conductor_pwrd+(strlen(conductor_pwrd)-1),"%c|",reqKey);
				}
			}
			else if((strlen(conductor_pwrd)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(conductor_pwrd+(strlen(conductor_pwrd)-1),"%c|",reqKey);
			}
			//			else
			//				conductor_pwrd[strlen(conductor_pwrd)]='|';
			else if((strlen(conductor_pwrd)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(conductor_pwrd+(strlen(conductor_pwrd)),"%c|",key);
			if(strlen(conductor_pwrd)==(PSWRD_LEN))
			{
				conductor_pwrd[strlen(conductor_pwrd)]='\0';
			}
		}
		//...............sudhakar
		else if(*cursrFlag==4)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(conductor_type)>1)&&(strlen(conductor_type)<=(CONDR_TYPE+1)))
			{
				if(NULL!=strchr(conductor_type,'|'))
				{
					conductor_type[strlen(conductor_type)-2]='|';
					conductor_type[strlen(conductor_type)-1]='\0';
				}
				else
					conductor_type[strlen(conductor_type)-1]='|';
			}
			else if((strlen(conductor_type)>0)&&(strlen(conductor_type)<(CONDR_TYPE+1))&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(conductor_type,'|'))
					sprintf(conductor_type+(strlen(conductor_type)-1),"%c|",key);
			}
			else if((strlen(conductor_type)>=0)&&(strlen(conductor_type)<(CONDR_TYPE+1))&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(conductor_type,'|'))
				{
					if(conductor_type[(strlen(conductor_type)-2)]==preKey)
						sprintf(conductor_type+(strlen(conductor_type)-2),"%c|",reqKey);
					else
						sprintf(conductor_type+(strlen(conductor_type)-1),"%c|",reqKey);
				}
			}
			else if((strlen(conductor_type)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(conductor_type+(strlen(conductor_type)-1),"%c|",reqKey);
			}

			else if((strlen(conductor_type)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(conductor_type+(strlen(conductor_type)),"%c|",key);
			if(strlen(conductor_type)==CONDR_TYPE)
			{
				conductor_type[strlen(conductor_type)]='\0';
			}
		}
		else if(*cursrFlag==5)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(licence_expiry_date)>1)&&(strlen(licence_expiry_date)<=(DATE_LEN+1)))
			{
				if(NULL!=strchr(licence_expiry_date,'|'))
				{
					licence_expiry_date[strlen(licence_expiry_date)-2]='|';
					licence_expiry_date[strlen(licence_expiry_date)-1]='\0';
				}
				else
					licence_expiry_date[strlen(licence_expiry_date)-1]='|';
			}
			else if((strlen(licence_expiry_date)>0)&&(strlen(licence_expiry_date)<(DATE_LEN+1))&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(licence_expiry_date,'|'))
					sprintf(licence_expiry_date+(strlen(licence_expiry_date)-1),"%c|",key);
			}
			else if((strlen(licence_expiry_date)>=0)&&(strlen(licence_expiry_date)<(DATE_LEN+1))&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(licence_expiry_date,'|'))
				{
					if(licence_expiry_date[(strlen(licence_expiry_date)-2)]==preKey)
						sprintf(licence_expiry_date+(strlen(licence_expiry_date)-2),"%c|",reqKey);
					else
						sprintf(licence_expiry_date+(strlen(licence_expiry_date)-1),"%c|",reqKey);
				}
			}
			else if((strlen(licence_expiry_date)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(licence_expiry_date+(strlen(licence_expiry_date)-1),"%c|",reqKey);
			}

			else if((strlen(licence_expiry_date)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(licence_expiry_date+(strlen(licence_expiry_date)),"%c|",key);
			if(strlen(licence_expiry_date)==DATE_LEN)
			{
				licence_expiry_date[strlen(licence_expiry_date)]='\0';
			}
		}
	}
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_LEFT,20,60,"Conductor Id :%s",conductor_id);
	lcd_printf_ex(ALG_LEFT,20,90,"Conductor Name :%s",conductor_name);
	lcd_printf_ex(ALG_LEFT,20,120,"Conductor Password:%s",conductor_pwrd);
	lcd_printf_ex(ALG_LEFT,20,150,"Conductor Type:%s",conductor_type);
	lcd_printf_ex(ALG_LEFT,20,180,"Licence Expy Date:%s",licence_expiry_date);
	lcd_flip();

	return 1;
}

#if 0
int view_ticket_display(int key, char * ticketNo)
{
	CstcLog_printf(" In view_last_ticket_details");
	lcd_clean();
	lcd_printf_ex(ALG_LEFT,26,10,"CSTC");
	lcd_printf_ex(ALG_LEFT,26,35,"Enter ticket no.");

	CstcLog_printf("Key = %d", key);
	if(key!=0)
	{
		if((key==KEY_SYM_BACKSPACE)&&(strlen(ticketNo)>1)&&(strlen(ticketNo)<=7))
		{
			if(NULL!=strchr(ticketNo,'_'))
			{
				ticketNo[strlen(ticketNo)-2]='_';
				ticketNo[strlen(ticketNo)-1]='\0';
				CstcLog_printf("1. TicketNo = %s", ticketNo);
			}
			else
			{
				ticketNo[strlen(ticketNo)-1]='_';
				CstcLog_printf("2. TicketNo = %s", ticketNo);
			}
		}
		else if((strlen(ticketNo)>0)&&(strlen(ticketNo)<7))
		{
			if(NULL!=strchr(ticketNo,'_'))
				sprintf(ticketNo+(strlen(ticketNo)-1),"%c|",key);
			CstcLog_printf("1. Append TicketNo = %s", ticketNo);

		}
		else if((strlen(ticketNo)==0))
		{
			sprintf(ticketNo+(strlen(ticketNo)),"%c|",key);
			CstcLog_printf("2. Append TicketNo = %s", ticketNo);
		}

		if(strlen(ticketNo)==7)
		{
			ticketNo[strlen(ticketNo)-1]='\0';
			CstcLog_printf("3. TicketNo = %s", ticketNo);

		}

	}
	CstcLog_printf("!! TICKET No. : %s", ticketNo);
	lcd_printf_ex(ALG_LEFT,20,80,"TICKET No. : %s", ticketNo);
	lcd_printf_ex(ALG_CENTER,16,180,"Press ENTER to print");
	lcd_flip();

	return 1;
}


void view_schedule_details(schedule_master_struct schedule_master_struct_var, waybill_master_struct waybill_master_struct_var,int casekey)
{
	switch(casekey)
	{
	case 0:
		lcd_clean();
		//lcd_printf_ex(ALG_LEFT,20,20,"Sch. No.: %s",);
		lcd_printf_ex(ALG_CENTER,20,20,"ConTno.: %s",waybill_master_struct_var.conductor_id);

		lcd_printf_ex(ALG_LEFT,20,40,"V No.: %s",waybill_master_struct_var.vehicle_no);
		lcd_printf_ex(ALG_CENTER,20,40,"DrTno.: %s",waybill_master_struct_var.driver_id);
		lcd_flip();
		break;
	case 1:
		lcd_printf_ex(ALG_CENTER,20,60,"%s %s %s %s %s", schedule_master_struct_var.trip_id, schedule_master_struct_var.start_bus_stop_code,schedule_master_struct_var.end_bus_stop_code, schedule_master_struct_var.start_date,schedule_master_struct_var.end_date);
		break;
	default:
		break;
	}

}
#endif

int duty_machine_not_regi(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"MACHINE NOT");
	lcd_printf_ex(ALG_CENTER,24,90,"REGISTERED");
	lcd_flip();

	return 1;
}


int duty_machine_regi(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"REGISTERING ETIM");
	lcd_printf_ex(ALG_CENTER,24,90,"PLEASE WAIT..!");
	lcd_flip();

	return 1;
}

int duty_machine_regi_complete(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"REGISTERING ETIM");
	lcd_printf_ex(ALG_CENTER,24,90,"COMPLETE");
	lcd_flip();

	return 1;
}

int duty_machine_no_waybill(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"NO WAYBILL IN ETIM ");
	lcd_printf_ex(ALG_CENTER,24,90,"              ");
	lcd_flip();

	return 1;
}

int duty_machine_inserting_waybill(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"INSERTING WAYBILL");
	lcd_printf_ex(ALG_CENTER,24,90,"PLEASE WAIT..!");
	lcd_flip();

	return 1;
}

int duty_machine_new_waybill_final(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"NEW WAYBILL RECEIVED");
	lcd_printf_ex(ALG_CENTER,24,90,"              ");
	lcd_flip();

	return 1;
}

int duty_machine_no_master(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"NO MASTER DATA");
	lcd_printf_ex(ALG_CENTER,24,90,"              ");
	lcd_flip();

	return 1;
}

int duty_machine_uploading_master(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"UPLOADING MASTER");
	lcd_printf_ex(ALG_CENTER,24,90,"PLEASE WAIT..!");
	lcd_flip();

	return 1;
}

int duty_machine_uploading_master_complete(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"UPLOADING MASTER");
	lcd_printf_ex(ALG_CENTER,24,90,"COMPLETE");
	lcd_flip();

	return 1;
}

int duty_machine_no_ticket_fare(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"NO TICKET FARE");
	lcd_printf_ex(ALG_CENTER,24,90,"              ");
	lcd_flip();

	return 1;
}

int duty_machine_receiving_fare(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"RECEIVEING TKT FARE");
	lcd_printf_ex(ALG_CENTER,24,90,"PLEASE WAIT..!");
	lcd_flip();

	return 1;
}

int duty_machine_receiving_fare_complete(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"RECEIVEING TKT FARE");
	lcd_printf_ex(ALG_CENTER,24,90,"COMPLETE");
	lcd_flip();

	return 1;
}

int duty_machine_duty_not_started(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"DUTY NOT STARTED");
	lcd_printf_ex(ALG_CENTER,24,90,"              ");
	lcd_flip();

	return 1;
}

int duty_machine_allocating_duty(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"ALLOCATING DUTY");
	lcd_printf_ex(ALG_CENTER,24,90,"PLEASE WAIT..!");
	lcd_flip();

	return 1;
}

int duty_machine_allocating_duty_complete(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"DUTY ALLOCATION ");
	lcd_printf_ex(ALG_CENTER,24,90,"COMPLETE");
	lcd_flip();

	return 1;
}

int duty_machine_downloading_data(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"DOWNLOADING DATA");
	lcd_printf_ex(ALG_CENTER,24,90,"PLEASE WAIT..!");
	lcd_flip();

	return 1;
}

int duty_machine_downloading_data_complete(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"DOWNLOADING DATA");
	lcd_printf_ex(ALG_CENTER,24,90,"COMPLETE");
	lcd_flip();

	return 1;
}

int duty_machine_erasing_data(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"ERASING DATA");
	lcd_printf_ex(ALG_CENTER,24,90,"PLEASE WAIT..!");
	lcd_flip();

	return 1;
}

int duty_machine_erasing_data_complete(void)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"ERASING DATA");
	lcd_printf_ex(ALG_CENTER,24,90,"COMPLETE");
	lcd_flip();

	return 1;
}

#if 0
int display_last_screen1(char* ticket_no,int key)
{
	lcd_printf_ex(ALG_CENTER,32,5,"CSTC");
	if(key!=0)
	{
		if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_no)>1)&&(strlen(ticket_no)<=7))
		{
			if(NULL!=strchr(ticket_no,'_'))
			{
				ticket_no[strlen(ticket_no)-2]='_';
				ticket_no[strlen(ticket_no)-1]='\0';
			}
			else
				ticket_no[strlen(ticket_no)-1]='_';
		}
		else if((strlen(ticket_no)>0)&&(strlen(ticket_no)<7))
		{
			if(NULL!=strchr(ticket_no,'_'))
				sprintf(ticket_no+(strlen(ticket_no)-1),"%c|",key);
		}
		else if((strlen(ticket_no)==0))
		{
			sprintf(ticket_no+(strlen(ticket_no)),"%c|",key);
		}
		//			username[strlen(username)]='_';
		if(strlen(ticket_no)==7)
		{
			ticket_no[strlen(ticket_no)-1]='\0';
		}

	}
	lcd_printf_ex(ALG_CENTER,24,70,"ENTER TICKET No.");
	lcd_printf_ex(ALG_CENTER,24,90,"0000001");
	lcd_flip();

	//	 sleep(2);
	return 1;
}
int display_last_screen2(char* ticket_no)
{
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,5,"CSTC");
	lcd_printf_ex(ALG_CENTER,20,50,"Ticket_no:   0000001");
	lcd_printf_ex(ALG_CENTER,20,70,"SBS   -TO-   BMT-02");
	lcd_printf_ex(ALG_CENTER,20,90,"21/05/2014   -   04.30");
	lcd_printf_ex(ALG_LEFT,20,120,"FULL:     1  *  40   =   40");
	lcd_printf_ex(ALG_LEFT,20,140,"HALF:     0         ");
	lcd_printf_ex(ALG_LEFT,20,160,"LUGG:    0         ");
	lcd_printf_ex(ALG_LEFT,20,175,"------------------------");
	lcd_printf_ex(ALG_LEFT,20,190,"Total :                  40");
	lcd_flip();
	//	sleep(3);
	return 1;
}


int display_last_ticket1()
{
	unsigned short int key=0;
	int retval=0;  //exit =0,

	static char* ticket_no;

	//CstcLog_printf("In User authentication");
	ticket_no = (char*)malloc((30*sizeof(char)));
	memset(ticket_no,0,30);

	while(1)
	{
		key=0;
		display_last_screen2(ticket_no);
		//Kb_Get_Key_Sym1();
		Kb_Get_Key_Sym();
		retval=1;
		//if((key = Kb_Get_Key_Sym1())>1)
		if((key = Kb_Get_Key_Sym())>1)
		{
			if(key==KEY_SYM_ESCAPE)
			{
				//					exit =1;
				retval=0;
				key=0;
				break;
			}
			else if(key==KEY_SYM_ENTER)
			{
				key=0;
				break;
			}

			display_last_screen2(ticket_no);

		}
	}

	//		if(key==KEY_SYM_ENTER)
	//		{
	//			 display_last_screen2(ticket_no);
	//			 Kb_Get_Key_Sym1();
	//				retval=1;
	//
	//		}
	return retval;
}
#endif


int display_collection_report(void)
{
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
	CstcLog_printf("In display_collection_report");
	lcd_clean();
	lcd_printf(ALG_CENTER,28, "WBTC");

	lcd_printf_ex(ALG_CENTER,20,40,"COLLECTION REPORT");
	lcd_printf_ex(ALG_CENTER,20,60,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));

	lcd_printf_ex(ALG_LEFT,20,90,"Adult        : %s",current_passengers_details_struct_var.pass_count_qstr);
	lcd_printf_ex(ALG_LEFT,20,110,"Student      : %d",(atoi(current_passengers_details_struct_var.full_count_qstr)+atoi(current_passengers_details_struct_var.half_count_qstr)+atoi(current_passengers_details_struct_var.srctzn_count_qstr)));
/*	lcd_printf_ex(ALG_LEFT,20,130,"Sr. Citizen  : %s",current_passengers_details_struct_var.present_passengers_qstr);
	lcd_printf_ex(ALG_LEFT,20,150,"Luggage  : %s",current_passengers_details_struct_var.present_passengers_qstr);*/
	lcd_printf_ex(ALG_CENTER,16,180,"PRESS ANY KEY TO EXIT ");
	lcd_flip();
	//Kb_Get_Key_Sym_old();
	Kb_Get_Key_Sym();

	return 0;
}
//int display_current_trip_report(char *startTicketNo,char *endTicketNo,char *scheduleNo,char * TripStartDt,char * TripStartTm,int tempTotal,char *adultcnt,char * childcnt,char *srcitizencnt,char *luggcnt,char *adultamt,char * childamt,char *srcitizenamt,char *luggamt)
#if 0
int display_current_trip_report(char *schedule_no,char *startTicketNo,char *endTicketNo,char * TripStartDt,char * TripStartTm,int tempTotal,char *adultcnt,char * childcnt,char *srcitizencnt,char *luggcnt,char *adultamt,char * childamt,char *srcitizenamt,char *luggamt)
{
	//	time_t t = time(NULL);
	//	struct tm	*tm = localtime(&t);
	CstcLog_printf("In display_collection_report");
	lcd_clean();
	//	lcd_printf(ALG_CENTER,28,"CSTC");
	lcd_printf_ex(ALG_CENTER,20,30,"CURRENT TRIP REPORT");
	//	lcd_printf_ex(ALG_CENTER,16,50,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_printf_ex(ALG_LEFT,16,50,"TICKET NO: %s To %s",startTicketNo,endTicketNo);
	lcd_printf_xy(16,200,50,"TRIP NO: %s",selected_trip_info_struct_var.trip_id_qstr);
	lcd_printf_ex(ALG_LEFT,16,70,"TRIP START: %s   %s",TripStartDt,TripStartTm);
	lcd_printf_ex(ALG_LEFT,16,90,"TICKET             COUNT            AMOUNT");
	lcd_printf_ex(ALG_LEFT,16,110,"Adult :                 %s                         %s",adultcnt,adultamt);
	lcd_printf_ex(ALG_LEFT,16,130,"Child :                 %s                          %s",childcnt,childamt);
	lcd_printf_ex(ALG_LEFT,16,150,"Sr.Citizen:          %s                          %s",srcitizencnt,srcitizenamt);
	lcd_printf_ex(ALG_LEFT,16,170,"Luggage  :          %s                          %s",luggcnt,luggamt);
	lcd_printf_ex(ALG_LEFT,16,190,"TOTAL  :                                         %d",tempTotal);
	lcd_printf_ex(ALG_CENTER,16,210,"PRESS ANY KEY TO EXIT ");

	//	lcd_printf_ex(ALG_LEFT,20,90,"Adult        : %s",adultcnt);
	//	lcd_printf_ex(ALG_LEFT,20,110,"Child      : %d",childcnt);
	//	lcd_printf_ex(ALG_LEFT,20,130,"Sr. Citizen  : %s",srcitizencnt);
	//	lcd_printf_ex(ALG_LEFT,20,150,"Luggage  : %s",luggcnt);
	//	lcd_printf_ex(ALG_CENTER,16,180,"PRESS ANY KEY TO EXIT ");

	lcd_flip();
	Kb_Get_Key_Sym_old();

	return 0;
}
#endif
int display_current_trip_report(char *schedule_no,char *startTicketNo,char *endTicketNo,char * TripStartDt,char * TripStartTm,int tempTotal,char *adultcnt,char * studentCnt,char *adultamt,char *studentAmt,char *toll_Amt)
{
	//	time_t t = time(NULL);
	//	struct tm	*tm = localtime(&t);
	int key=0;
	CstcLog_printf("In display_collection_report");

	lcd_clean();
	//	lcd_printf(ALG_CENTER,28,"CSTC");
	lcd_printf_ex(ALG_CENTER,20,20,"CURRENT TRIP DETAILS");
	//	lcd_printf_ex(ALG_CENTER,16,50,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_printf_ex(ALG_LEFT,16,50," TICKET NO: %s To %s",startTicketNo,endTicketNo);
	lcd_printf_xy(16,200,50,"TRIP NO: %s",selected_trip_info_struct_var.trip_id_qstr);
	lcd_printf_ex(ALG_LEFT,16,70," TRIP START: %s     %s",TripStartDt,TripStartTm);

	lcd_printf_ex(ALG_LEFT,16,95," TICKET             COUNT            AMOUNT");
	lcd_printf_ex(ALG_LEFT,16,115," Adult    :");
	lcd_printf_xy(16,130,115,"%s",adultcnt);
	lcd_printf_xy(16,240,115,"%s",adultamt);

	lcd_printf_ex(ALG_LEFT,16,135," Student     :");
	lcd_printf_xy(16,130,135,"%s",studentCnt);
	lcd_printf_xy(16,240,135,"%s",studentAmt);

//	lcd_printf_ex(ALG_CENTER,20,162," TOTAL  : Rs. %d /-",tempTotal);

	lcd_printf_ex(ALG_LEFT,18,162," TOTAL  :");
	lcd_printf_xy(18,230,162,"Rs. %d /-",tempTotal);

	lcd_printf_ex(ALG_LEFT,16,180," Toll  :");
	lcd_printf_xy(16,230,180,"Rs. %s /-",toll_Amt);

	lcd_printf_ex(ALG_CENTER,16,210,"PRESS CANCEL KEY TO EXIT ");
	lcd_flip();
	while(1)
	{
		key = Kb_Get_Key_Sym();
		if (key==KEY_SYM_ESCAPE)
		{
			break;
		}
	}
	return 1;

}

int display_all_trip_report(char *schedule_no,char *shift,char *temptrip,char *startTicketNo,char *endTicketNo,char * TripStartDt,char * TripStartTm,int tempTotal,char *adultcnt,char * childcnt,/*char *srcitizencnt,char *luggcnt,*/char *adultamt,char * childamt,/*char *srcitizenamt,*/char *tollamt,/*char *lugg_tkt_count,*/char *pass_tkt_count,int reportFlag,int endflag,int grandTotal)
{
	//	time_t t = time(NULL);
	//	struct tm	*tm = localtime(&t);
	int key=0;
	CstcLog_printf("In display_collection_report....");
	lcd_clean();
	//	lcd_printf(ALG_CENTER,28,"CSTC");
	if(reportFlag)
	{
		lcd_printf_ex(ALG_CENTER,20,5,"TRIP REPORT");
	}
	else
	{
		lcd_printf_ex(ALG_CENTER,20,5,"TRIP WISE REVENUE DETAILS");
	}
	//	lcd_printf_ex(ALG_CENTER,16,50,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	//	lcd_printf_ex(ALG_LEFT,16,30,"SCHEDULE NO: %s",schedule_no);
	lcd_printf_ex(ALG_RIGHT,16,30,"D/S: %s    ",shift);


	lcd_printf_ex(ALG_LEFT,16,50," TICKET NO: %s To %s",startTicketNo,endTicketNo);


	lcd_printf_ex(ALG_RIGHT,16,50,"TRIP NO: %s    ",temptrip);


	//		lcd_printf_ex(ALG_LEFT,16,50,"TRIP START: %s   %s",TripStartDt,TripStartTm);



	lcd_printf_ex(ALG_LEFT,16,75," TICKET             COUNT            AMOUNT");
	lcd_printf_ex(ALG_LEFT,16,95," Adult    :");
	lcd_printf_xy(16,130,95,"%s",adultcnt);
	lcd_printf_xy(16,240,95,"%s",adultamt);

	lcd_printf_ex(ALG_LEFT,16,110," Student     :");
	lcd_printf_xy(16,130,110,"%s",childcnt);
	lcd_printf_xy(16,240,110,"%s",childamt);

	/*
	lcd_printf_ex(ALG_LEFT,16,124,"Sr.Citizen :");
	lcd_printf_xy(16,130,124,"%s",srcitizencnt);
	lcd_printf_xy(16,240,124,"%s",srcitizenamt);

	lcd_printf_ex(ALG_LEFT,16,142,"Luggage  :");
	lcd_printf_xy(16,130,142,"%s",lugg_tkt_count);//luggcnt);
	lcd_printf_xy(16,240,142,"%s",luggamt);*/
	//
	//	lcd_printf_ex(ALG_LEFT,16,168,"Luggage Ticket Count  :");
	//	lcd_printf_xy(16,170,168,"%s",lugg_tkt_count);
	//	lcd_printf_ex(ALG_LEFT,16,185,"Passenger Ticket Count  :");
	//	lcd_printf_xy(16,190,185,"%s",pass_tkt_count);


	//	lcd_printf_ex(ALG_LEFT,18,142,"TOLL");
	//	lcd_printf_xy(18,240,12,"%d",luggamt);
	//	lcd_printf(ALG_LEFT,16,"TOLL = %d",luggamt);
	//		lcd_printf_ex(ALG_LEFT,16,142,"TOLL : %s",tollamt);

	lcd_printf_ex(ALG_LEFT,16,142," TOLL");
	lcd_printf_xy(16,220,142,"Rs. %s /-",tollamt);
	lcd_printf_ex(ALG_LEFT,16,162," TOTAL");
	lcd_printf_xy(16,220,162,"Rs. %d /-",tempTotal);
//	lcd_set_font_color(COLOR_RED);
	lcd_printf_ex(ALG_CENTER,20,182,"NET TOTAL : Rs.  %d /-",grandTotal);
//	lcd_set_font_color(COLOR_BLACK);


	if(!endflag)
	{
		lcd_printf_ex(ALG_CENTER,16,204,"PRESS ENTER/CANCEL KEY");
		lcd_flip();
		while(1)
		{
			CstcLog_printf("key..1=%d",key);
			key = Kb_Get_Key_Sym();
			CstcLog_printf("key= %d",key);
			if(key==KEY_SYM_ENTER)
			{
				CstcLog_printf("key2=%d",key);
				break;
			}
			else if (key==KEY_SYM_ESCAPE)
			{
				CstcLog_printf("key3=%d",key);
				return -1;
			}
		}
	}
	else
	{
		lcd_printf_ex(ALG_CENTER,16,204,"PRESS CANCEL KEY TO EXIT ");
		lcd_flip();
		while(1)
		{
			key = Kb_Get_Key_Sym();
			if (key==KEY_SYM_ESCAPE)
			{
				break;
			}
		}
	}
	return 0;
}

#if 0
int display_all_trip_status_report  (char *scheduleNo, char *shift,char *tripNo,char *temptrip,char *ticketCount,char *passCnt,char *passAmt,char *totalPxcnt,char *tempTotal,int scheduleflag,int startflag,int endflag,char *grandTotal)
{
	int key=0;
	CstcLog_printf("In display_collection_report....");
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,20,5,"ALL TRIP STATUS REPORT");
	lcd_printf_ex(ALG_LEFT,16,30,"SCHEDULE NO: %s",scheduleNo);
	lcd_printf_ex(ALG_RIGHT,16,30,"D/S: %s    ",shift);

	lcd_printf_ex(ALG_LEFT,16,65,"  Trip No      Tkt Cnt       P.Cnt     AMOUNT");
	lcd_printf_xy(16,20,85,"%s",tripNo);
	lcd_printf_xy(16,120,85,"%s",ticketCount);
	lcd_printf_xy(16,180,85,"%s",passCnt);
	lcd_printf_xy(16,260,85,"%s",passAmt);
	lcd_printf_xy(16,20,160,"%s","TOTAL");
	//	lcd_printf_xy(16,160,160,"%s",passCnt);
	lcd_printf_xy(16,260,160,"%s",passAmt);

	//	lcd_printf_xy(16,160,160,"%s",totalPxcnt);
	//	lcd_printf_xy(16,240,160,"%s",tempTotal);

	if(!endflag)
	{
		lcd_printf_ex(ALG_CENTER,16,200,"PRESS ENTER/CANCEL KEY");
		lcd_flip();
		while(1)
		{
			CstcLog_printf("key..1=%d",key);
			key=Kb_Get_Key_Sym();
			CstcLog_printf("key=%d",key);
			if(key==KEY_SYM_ENTER)
			{
				CstcLog_printf("key2=%d",key);
				break;
			}
			else if (key==KEY_SYM_ESCAPE)
			{
				CstcLog_printf("key3=%d",key);
				return -1;
			}
		}
	}
	else
	{
		lcd_printf_ex(ALG_CENTER,20,180,"NET TOTAL : Rs. %s",grandTotal);
		//		lcd_printf_ex(ALG_CENTER,16,200,"PRESS ANY KEY TO EXIT ");
		lcd_flip();
		Kb_Get_Key_Sym_old();
	}
	return 0;
}
#endif

int display_all_trip_status_report  (int j,char *scheduleNo, char *shift,char *tripNo,char *temptrip,char *ticketCount,char *passCnt,char *passAmt,char *totalPxcnt,char *tempTotal,int scheduleflag,int startflag,int endflag,char *grandTotal)
{
	int key=0;
	CstcLog_printf("In display_collection_report....");
	if(scheduleflag)
	{
		lcd_clean();
		lcd_printf_ex(ALG_CENTER,20,5,"ALL TRIP STATUS REPORT");
		lcd_printf_ex(ALG_LEFT,16,30,"SCHEDULE NO: %s",scheduleNo);
		lcd_printf_ex(ALG_RIGHT,16,30,"D/S: %s    ",shift);

		lcd_printf_ex(ALG_LEFT,16,65,"  Trip No      Tkt Cnt       P.Cnt     AMOUNT");
	}
	if(j>5)
		j=j%5;
	else if(j==5)
		j=0;

	lcd_printf_xy(16,20,(85+(j*16)),"%s",tripNo);
	lcd_printf_xy(16,120,(85+(j*16)),"%s",ticketCount);
	lcd_printf_xy(16,180,(85+(j*16)),"%s",passCnt);
	lcd_printf_xy(16,260,(85+(j*16)),"%s",passAmt);
	lcd_printf_xy(16,20,160,"%s","TOTAL");
	//	lcd_printf_xy(16,160,160,"%s",passCnt);
	lcd_printf_xy(16,260,160,"%s",passAmt);

	//	lcd_printf_xy(16,160,160,"%s",totalPxcnt);
	//	lcd_printf_xy(16,240,160,"%s",tempTotal);

	if((!endflag)&&(j%5==0))
	{
		lcd_printf_ex(ALG_CENTER,16,200,"PRESS ENTER/CANCEL KEY");
		lcd_flip();
		while(1)
		{
			CstcLog_printf("key..1=%d",key);
			key=Kb_Get_Key_Sym();
			CstcLog_printf("key=%d",key);
			if(key==KEY_SYM_ENTER)
			{
				CstcLog_printf("key2=%d",key);
				break;
			}
			else if (key==KEY_SYM_ESCAPE)
			{
				CstcLog_printf("key3=%d",key);
				return -1;
			}
		}
	}
	else if(endflag)
	{
		lcd_printf_ex(ALG_CENTER,20,180,"NET TOTAL : Rs. %s",grandTotal);
		//		lcd_printf_ex(ALG_CENTER,16,200,"PRESS ANY KEY TO EXIT ");
		lcd_flip();
		Kb_Get_Key_Sym_old();
	}
	else
		lcd_flip();
	return 0;
}

int Display_Pass_No(char* pass_no,int key,int *cursrFlag,int *shiftKeyLevel)
{
	int reqKey=0,preKey=0;
	//int i =0;

	lcd_clean();
	lcd_printf_ex(ALG_CENTER,20,20,"Enter Pass No");
	if(key!=0)
	{
		if(*cursrFlag==1)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(pass_no)>1)&&(strlen(pass_no)<=10))
			{
				if(NULL!=strchr(pass_no,'|'))
				{
					pass_no[strlen(pass_no)-2]='|';
					pass_no[strlen(pass_no)-1]='\0';
				}
				else
					pass_no[strlen(pass_no)-1]='|';
			}
			else if((strlen(pass_no)>0)&&(strlen(pass_no)<=10)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(pass_no,'|'))
					sprintf(pass_no+(strlen(pass_no)-1),"%c|",key);
			}
			else if((strlen(pass_no)>0)&&(strlen(pass_no)<=10)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				if(NULL!=strchr(pass_no,'|'))
				{
					if(pass_no[(strlen(pass_no)-2)]==preKey)
						sprintf(pass_no+(strlen(pass_no)-2),"%c|",reqKey);
					else
						sprintf(pass_no+(strlen(pass_no)-1),"%c|",reqKey);
				}
			}
			else if((strlen(pass_no)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				sprintf(pass_no+(strlen(pass_no)),"%c|",reqKey);
			}
			else if((strlen(pass_no)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(pass_no+(strlen(pass_no)),"%c|",key);

			if(strlen(pass_no)==11)
			{
				pass_no[strlen(pass_no)-1]='\0';
				CstcLog_printf("Snehal You reached here");
			}
		}
	}

	lcd_printf_xy(10,80,100,"PASS No:");
	lcd_printf_xy(20,170,100,"%s",pass_no);

	lcd_flip();
	return 1;
}


int Display_Get_TicketNo_Screen(char* ticket_no,int key)
{

	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);

	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"Please Enter Ticket Number");

	if(key!=0)
	{
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(ticket_no)>1)&&(strlen(ticket_no)<=TICKET_NO_LEN+1))
			{
				if(NULL!=strchr(ticket_no,'|'))
				{
					ticket_no[strlen(ticket_no)-2]='|';
					ticket_no[strlen(ticket_no)-1]='\0';
				}
				else
					ticket_no[strlen(ticket_no)-1]='|';
			}
			else if((strlen(ticket_no)>0)&&(strlen(ticket_no)<TICKET_NO_LEN+1)&&/*(*shiftKeyLevel==0)&&*/(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(ticket_no,'|'))
					sprintf(ticket_no+(strlen(ticket_no)-1),"%c|",key);
			}

			else if((strlen(ticket_no)==0)&&/*(*shiftKeyLevel==0)&&*/(key!=KEY_SYM_BACKSPACE))
				sprintf(ticket_no+(strlen(ticket_no)),"%c|",key);

			if(strlen(ticket_no)==TICKET_NO_LEN+1)
			{
				ticket_no[strlen(ticket_no)-1]='\0';

			}
		}

	}

	lcd_printf_xy(20,80,120,"Ticket No :");
	//	lcd_printf_xy(20,190,120,":");
	lcd_printf_xy(20,200,120,"%s",ticket_no);
	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();
	return 1;
}



int Display_Get_RouteId_Screen(char* route_id,int key)
{

	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);

	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"Please Enter Route Id ");

	if(key!=0)
	{
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(route_id)>1)&&(strlen(route_id)<=ROUTE_ID_LEN+1))
			{
				if(NULL!=strchr(route_id,'|'))
				{
					route_id[strlen(route_id)-2]='|';
					route_id[strlen(route_id)-1]='\0';
				}
				else
					route_id[strlen(route_id)-1]='|';
			}
			else if((strlen(route_id)>0)&&(strlen(route_id)<ROUTE_ID_LEN+1)&&/*(*shiftKeyLevel==0)&&*/(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(route_id,'|'))
					sprintf(route_id+(strlen(route_id)-1),"%c|",key);
			}

			else if((strlen(route_id)==0)&&/*(*shiftKeyLevel==0)&&*/(key!=KEY_SYM_BACKSPACE))
				sprintf(route_id+(strlen(route_id)),"%c|",key);

			if(strlen(route_id)==ROUTE_ID_LEN+1)
			{
				route_id[strlen(route_id)-1]='\0';

			}
		}

	}

	lcd_printf_xy(20,80,120,"Route Id :");
	//	lcd_printf_xy(20,190,120,":");
	lcd_printf_xy(20,200,120,"%s",route_id);
	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();
	return 1;
}


int Display_Get_CardNo_Screen(char* card_no,int key)
{

	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);

	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"Please Enter Card Number");

	if(key!=0)
	{
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(card_no)>1)&&(strlen(card_no)<=TRIP_CARD_NO_LEN+1))
			{
				if(NULL!=strchr(card_no,'|'))
				{
					card_no[strlen(card_no)-2]='|';
					card_no[strlen(card_no)-1]='\0';
				}
				else
					card_no[strlen(card_no)-1]='|';
			}
			else if((strlen(card_no)>0)&&(strlen(card_no)<TRIP_CARD_NO_LEN+1)&&/*(*shiftKeyLevel==0)&&*/(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(card_no,'|'))
					sprintf(card_no+(strlen(card_no)-1),"%c|",key);
			}

			else if((strlen(card_no)==0)&&/*(*shiftKeyLevel==0)&&*/(key!=KEY_SYM_BACKSPACE))
				sprintf(card_no+(strlen(card_no)),"%c|",key);

			if(strlen(card_no)==TRIP_CARD_NO_LEN+1)
			{
				card_no[strlen(card_no)-1]='\0';

			}
		}

	}

	lcd_printf_xy(20,80,120,"Card No :");
	//	lcd_printf_xy(20,190,120,":");
	lcd_printf_xy(20,200,120,"%s",card_no);
	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();
	return 1;
}


void Show_Ticket_display(transaction_log_struct transaction_log_struct_var,ticket_display_struct* ticket_display_struct_var)
{
#if 1
	int key =0;
	lcd_clean();

	lcd_printf_ex(ALG_CENTER,20,5,"TICKET NO : %05d",atoi(transaction_log_struct_var.ticket_no));

	lcd_printf_ex(ALG_LEFT,16,25,"From Bus Stop :");
	lcd_printf_ex(ALG_CENTER,16,45,"- %s",transaction_log_struct_var.from_bus_stop_name_english);
	lcd_printf_ex(ALG_LEFT,16,65,"To Bus Stop :");
	lcd_printf_ex(ALG_CENTER,16,85,"- %s",transaction_log_struct_var.till_bus_stop_name_english);

	lcd_printf_ex(ALG_LEFT,16,105,"Adult");//transaction_log_struct_var.px_count
	lcd_printf_xy(16,110,105,":");
	lcd_printf_xy(16,130,105,"%s",ticket_display_struct_var->adultCnt);//transaction_log_struct_var.px_count
	lcd_printf_xy(16,160,105,"x");
	lcd_printf_xy(16,190,105,"%s",fare_chart_struct_var.adult_fare);//transaction_log_struct_var.px_count
	lcd_printf_xy(16,216,105,"=");
	lcd_printf_xy(16,250,105,"%d",roundNo(atof(fare_chart_struct_var.adult_fare)*atof(ticket_display_struct_var->adultCnt)));//transaction_log_struct_var.px_count

	lcd_printf_ex(ALG_LEFT,16,125,"Student");//ticket_display_struct_var.childCnt
	lcd_printf_xy(16,110,125,":");
	lcd_printf_xy(16,130,125,"%s",ticket_display_struct_var->childCnt);//ticket_display_struct_var.childCnt
	lcd_printf_xy(16,160,125,"x");
	lcd_printf_xy(16,190,125,"%s",fare_chart_struct_var.child_fare);//transaction_log_struct_var.px_count
	lcd_printf_xy(16,216,125,"=");
	lcd_printf_xy(16,250,125,"%d",roundNo(atof(fare_chart_struct_var.child_fare)*atof(ticket_display_struct_var->childCnt)));//transaction_log_struct_var.px_count
	/*
	lcd_printf_ex(ALG_LEFT,16,145,"Sr Citizen  ");//ticket_display_struct_var.srCitizenCnt       //Correct this by adding total number of concessions and epurse ticket count
	lcd_printf_xy(16,110,145,":");
	lcd_printf_xy(16,130,145,"%s",ticket_display_struct_var->srCitizenCnt);//ticket_display_struct_var.srCitizenCnt       //Correct this by adding total number of concessions and epurse ticket count
	lcd_printf_xy(16,160,145,"x");
	lcd_printf_xy(16,190,145,"%s",fare_chart_struct_var.sr_citizen_fare);//transaction_log_struct_var.px_count
	lcd_printf_xy(16,216,145,"=");
	lcd_printf_xy(16,250,145,"%d",roundNo(atof(fare_chart_struct_var.sr_citizen_fare)*atof(ticket_display_struct_var->srCitizenCnt)));//transaction_log_struct_var.px_count

	lcd_printf_ex(ALG_LEFT,16,165,"Luggage");////transaction_log_struct_var.lugg_units
	lcd_printf_xy(16,110,165,":");
	lcd_printf_xy(16,130,165,"%s",ticket_display_struct_var->lugg);//ticket_display_struct_var.srCitizenCnt       //Correct this by adding total number of concessions and epurse ticket count
	lcd_printf_xy(16,250,165,"%s",ticket_display_struct_var->luggtotalAmt);//transaction_log_struct_var.px_count*/

	lcd_printf_ex(ALG_LEFT,20,160,"Total");//Correct this by adding total number of concessions and epurse ticket count
	lcd_printf_xy(20,110,160,":");
	lcd_printf_xy(20,220,160,"Rs. %s /-",transaction_log_struct_var.total_ticket_amount);////transaction_log_struct_var.lugg_units
	lcd_printf_ex(ALG_CENTER,16,190,"PRESS CANCEL TO EXIT");
	lcd_flip();
	while(1)  //swap:test this
	{
		key = Kb_Get_Key_Sym();
		if (key==KEY_SYM_ESCAPE)
		{
			break;
		}
	}
#endif
}

#if 0
int display_all_trip_status_report(char *start_date,char *start_time,char *schedule_no,char *trip_no,char *pass_cnt,char *pass_amt,int endflag)
{


	//	time_t t = time(NULL);
	//	struct tm	*tm = localtime(&t);
	CstcLog_printf("In display_all_trip_status_report");
	lcd_clean();
	//	lcd_printf(ALG_CENTER,28,"CSTC");
	lcd_printf_ex(ALG_CENTER,20,30,"ALL TRIP STATUS REPORT");
	//	lcd_printf_ex(ALG_CENTER,16,50,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	//	lcd_printf_ex(ALG_LEFT,16,50,"TICKET NO: %s To %s",startTicketNo,endTicketNo);
	lcd_printf_ex(ALG_LEFT,16,70,"TRIP START: %s   %s",TripStartDt,TripStartTm);
	lcd_printf_ex(ALG_LEFT,16,90,"TRIP NO         COUNT            AMOUNT");
	lcd_printf_ex(ALG_LEFT,16,110,"Adult :                 %s                         %s",adultcnt,adultamt);
	lcd_printf_ex(ALG_LEFT,16,130,"Child :                 %s                          %s",childcnt,childamt);
	lcd_printf_ex(ALG_LEFT,16,150,"Sr.Citizen:          %s                          %s",srcitizencnt,srcitizenamt);
	lcd_printf_ex(ALG_LEFT,16,170,"Luggage  :          %s                          %s",luggcnt,luggamt);
	lcd_printf_ex(ALG_LEFT,16,190,"TOTAL  :                                         %d",tempTotal);
	lcd_printf_ex(ALG_CENTER,16,210,"PRESS ANY KEY TO EXIT ");

	//	lcd_printf_ex(ALG_LEFT,20,90,"Adult        : %s",adultcnt);
	//	lcd_printf_ex(ALG_LEFT,20,110,"Child      : %d",childcnt);
	//	lcd_printf_ex(ALG_LEFT,20,130,"Sr. Citizen  : %s",srcitizencnt);
	//	lcd_printf_ex(ALG_LEFT,20,150,"Luggage  : %s",luggcnt);
	//	lcd_printf_ex(ALG_CENTER,16,180,"PRESS ANY KEY TO EXIT ");

	lcd_flip();
	Kb_Get_Key_Sym_old();



	return 0;
}
#endif

void Display_Ticket_data(char *result_data[],int y,int screen,char first_time_flag)
{
	char temp_cnt[AMOUNT_LEN+AMOUNT_LEN+4] = {'\0'};


	memset(temp_cnt,0,AMOUNT_LEN+AMOUNT_LEN+4);
	strcpy(temp_cnt,result_data[3]);
	if((result_data[9])!=NULL)
	{
		if(atoi(result_data[9])>0)
			strcat(temp_cnt,"L");
		//	sprintf(temp_cnt+strlen(result_data[3]),"+%sL",result_data[9]);
		//	else
		//		sprintf(temp_cnt+strlen(result_data[3]),"+0L");//result_data[9]);
	}
	//	else
	//		sprintf(temp_cnt+strlen(result_data[3]),"+0L");//result_data[9]);

	if(first_time_flag == 0)
	{
		//first_time_flag = 1;

		lcd_printf_ex(ALG_CENTER,20,5,"TICKET DETAILS");
		lcd_printf_ex(ALG_CENTER,16,35,"WAYBILL NO: %s",result_data[5]);

//		lcd_printf_ex(ALG_CENTER,16,65,"SCHEDULE NO: %s",result_data[6]);
//		lcd_printf_ex(ALG_CENTER,16,65,"TRIP NO   : %s   SHIFT      : %s",result[7],result[8]);

		lcd_printf_ex(ALG_CENTER,16,60,"**********************************");
		lcd_printf_ex(ALG_CENTER,16,75,"T.NO  TRP  FROM  TO   SHFT  CNT     TOTAL");
		lcd_printf_ex(ALG_CENTER,16,95,"**********************************");
		lcd_printf_ex(ALG_CENTER,16,200,"PRESS 'v' or '^'TO SCROLL");
	}

	lcd_printf_xy(16,5,115 + ((y-screen)*14)," %s",result_data[0]);
	lcd_printf_xy(16,45,115 + ((y-screen)*14)," %s",result_data[7]);
	result_data[1][4] = 0x00;
	lcd_printf_xy(16,70,115 + ((y-screen)*14),"  %s",result_data[1]);
	result_data[2][4] = 0x00;
	lcd_printf_xy(16,120,115 + ((y-screen)*14),"  %s",result_data[2]);
	lcd_printf_xy(16,170,115 + ((y-screen)*14),"  %s",result_data[8]);
	lcd_printf_xy(16,210,115 + ((y-screen)*14),"  %s",temp_cnt);
	lcd_printf_xy(16,265,115 + ((y-screen)*14),"  %s",result_data[4]);
}

int Display_Update_App_Screen(char* password,int key)
{
	int i=0;
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
	char *fpswd=NULL;
	fpswd = (char*)malloc(((MASTER_PW_LEN+1)*sizeof(char)));
	memset(fpswd,0,MASTER_PW_LEN+1);

	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	lcd_printf_ex(ALG_CENTER,20,60,"Enter Password");
	lcd_printf_ex(ALG_CENTER,20,80,"To Update Application");


	if(key!=0)
	{
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(password)>1)&&(strlen(password)<=(MASTER_PW_LEN+1)))
			{
				if(NULL!=strchr(password,'|'))
				{
					password[strlen(password)-2]='|';
					password[strlen(password)-1]='\0';
				}
				else
					password[strlen(password)-1]='|';
			}
			else if((strlen(password)>0)&&(strlen(password)<=MASTER_PW_LEN)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(password,'|'))
					sprintf(password+(strlen(password)-1),"%c|",key);
			}
			else if((strlen(password)==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(password+(strlen(password)),"%c|",key);

			if(strlen(password)==MASTER_PW_LEN)
			{
				password[strlen(password)-1]='\0';
			}

		}
	}
	if(NULL!=strchr(password,'|'))
		password[strlen(password)-1]='\0';

	if(strlen(password)>0)
	{
		for(i=0;i<strlen(password);i++)
		{
			strcat(fpswd,"*");
		}
	}

	if(strlen(fpswd)<MASTER_PW_LEN)
		strcat(fpswd,"|");

	if(NULL==strchr(password,'|'))
		password[strlen(password)]='|';


	lcd_printf_xy(20,80,130,"Password");
	lcd_printf_xy(20,190,130,":");
	lcd_printf_xy(20,200,130,"%s",fpswd);


	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();
	if(fpswd!= NULL)
	{
		free(fpswd);
		fpswd=NULL;
	}

	return 1;
}



int Display_Get_Time_Screen(char* s_time,int key)
{

	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);

	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"Please Enter Time");

	if(key!=0)
	{
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(s_time)>1)&&(strlen(s_time)<=(TIME_LEN-3)+1))
			{
				if(NULL!=strchr(s_time,'|'))
				{
					s_time[strlen(s_time)-2]='|';
					s_time[strlen(s_time)-1]='\0';
				}
				else
					s_time[strlen(s_time)-1]='|';
			}
			else if((strlen(s_time)>0)&&(strlen(s_time)<(TIME_LEN-3)+1)&&/*(*shiftKeyLevel==0)&&*/(key!=KEY_SYM_BACKSPACE))
			{
				CstcLog_printf("before s_time=%s,%d",s_time,strlen(s_time));
				if(strlen(s_time)==2)
				{
					if(NULL!=strchr(s_time,'|'));
						sprintf(s_time+(strlen(s_time)-1),"%c:|",key);
					CstcLog_printf("1..s_time=%s",s_time);
				}
				else
				{
					if(NULL!=strchr(s_time,'|'))
						sprintf(s_time+(strlen(s_time)-1),"%c|",key);
					CstcLog_printf("2...s_time=%s",s_time);
				}
			}

			else if((strlen(s_time)==0)&&/*(*shiftKeyLevel==0)&&*/(key!=KEY_SYM_BACKSPACE))
				sprintf(s_time+(strlen(s_time)),"%c|",key);

			if(strlen(s_time)==(TIME_LEN-3)+1)
			{
				s_time[strlen(s_time)-1]='\0';
			}
		}
	}

	lcd_printf_xy(20,80,120,"Time :");
	//	lcd_printf_xy(20,190,120,":");
	lcd_printf_xy(20,150,120,"%s",s_time);
	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();
	return 1;
}



int Display_Toll_Entry(char* receipt_no, char *toll_amt,int key,int *cursrFlag,int *shiftKeyLevel,int LC_chk_flag)
{
	int reqKey=0,preKey=0;
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);
/*	char *fpswd;
	fpswd = (char*)malloc(((PSWRD_LEN+1)*sizeof(char)));
	memset(fpswd,0,PSWRD_LEN+1);*/
//	if(LC_chk_flag)
//	{
//		(TOLL_RECEIPT_NO_LEN-1) = LC_STAFF_USERNAME_LIMIT+1;
//	}
//	else
//	{
//	}
	//All display here

	if(key!=0)
	{
		if(*cursrFlag==1)
		{
			if((key==KEY_SYM_BACKSPACE)&&(strlen(receipt_no)>1)&&(strlen(receipt_no)<=(TOLL_RECEIPT_NO_LEN+1)))
			{
				if(NULL!=strchr(receipt_no,'|'))
				{
					receipt_no[strlen(receipt_no)-2]='|';
					receipt_no[strlen(receipt_no)-1]='\0';
				}
				else
					receipt_no[strlen(receipt_no)-1]='|';
			}
			else if((strlen(receipt_no)>0)&&(strlen(receipt_no)<=(TOLL_RECEIPT_NO_LEN))&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(receipt_no,'|'))
					sprintf(receipt_no+(strlen(receipt_no)-1),"%c|",key);
			}
			else if((strlen(receipt_no)>0)&&(strlen(receipt_no)<=(TOLL_RECEIPT_NO_LEN))&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
				CstcLog_printf("1. preKey Alhpa %c",preKey);
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				CstcLog_printf("2. reqKey Alhpa %c",reqKey);
				if(NULL!=strchr(receipt_no,'|'))
				{
					CstcLog_printf("receipt_no Alhpa %s ::%d",receipt_no,strlen(receipt_no));
					CstcLog_printf("receipt_no Alhpa %c",receipt_no[(strlen(receipt_no)-2)]);
					if(strlen(receipt_no)>1)
					{
						if(receipt_no[(strlen(receipt_no)-2)]==preKey)
							sprintf(receipt_no+(strlen(receipt_no)-2),"%c|",reqKey);
						else
							sprintf(receipt_no+(strlen(receipt_no)-1),"%c|",reqKey);
					}
					else
					{
						sprintf(receipt_no+(strlen(receipt_no)-1),"%c|",reqKey);
					}
					CstcLog_printf("receipt_no Alhpa %s",receipt_no);
				}
				else if(NULL==strchr(receipt_no,'|'))
				{
					CstcLog_printf("No cursor receipt_no Alhpa %s ::%d",receipt_no,strlen(receipt_no));
					CstcLog_printf("No cursor receipt_no Alhpa %c",receipt_no[(strlen(receipt_no)-2)]);
					if(strlen(receipt_no)>1)
					{
						if(receipt_no[(strlen(receipt_no)-1)]==preKey)
							sprintf(receipt_no+(strlen(receipt_no)-1),"%c",reqKey);
					}

				}
			}
			else if((strlen(receipt_no)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
			{
				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
				CstcLog_printf("3. reqKey Alhpa %c",reqKey);
				sprintf(receipt_no+(strlen(receipt_no)),"%c|",reqKey);
			}
			else if((strlen(receipt_no)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(receipt_no+(strlen(receipt_no)),"%c|",key);

//			if(strlen(receipt_no)==(TOLL_RECEIPT_NO_LEN-1))
//			{
//				receipt_no[strlen(receipt_no)-1]='\0';
//				if(!LC_chk_flag)
//				{
//					*cursrFlag=2;
//					toll_amt[strlen(toll_amt)]='|';
//				}
//			}
		}
		else if(*cursrFlag==2)
		{
			*shiftKeyLevel=0;
			if((key==KEY_SYM_BACKSPACE)&&(strlen(toll_amt)>1)&&(strlen(toll_amt)<=(TOLL_AMOUNT_LEN+1)))
			{
				if(NULL!=strchr(toll_amt,'|'))
				{
					toll_amt[strlen(toll_amt)-2]='|';
					toll_amt[strlen(toll_amt)-1]='\0';
				}
				else
					toll_amt[strlen(toll_amt)-1]='|';
			}
			else if((strlen(toll_amt)>0)&&(strlen(toll_amt)<=(TOLL_AMOUNT_LEN))&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			{
				if(NULL!=strchr(toll_amt,'|'))
					sprintf(toll_amt+(strlen(toll_amt)-1),"%c|",key);
			}
//			else if((strlen(toll_amt)>=0)&&(strlen(toll_amt)<(TOLL_AMOUNT_LEN))&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
//			{
//				preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
//				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
//				if(NULL!=strchr(toll_amt,'|'))
//				{
//					if(toll_amt[(strlen(toll_amt)-2)]==preKey)
//						sprintf(toll_amt+(strlen(toll_amt)-2),"%c|",reqKey);
//					else
//						sprintf(toll_amt+(strlen(toll_amt)-1),"%c|",reqKey);
//				}
//			}
//			else if((strlen(toll_amt)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
//			{
//				reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
//				sprintf(toll_amt+(strlen(toll_amt)),"%c|",reqKey);
//			}

			else if((strlen(toll_amt)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
				sprintf(toll_amt+(strlen(toll_amt)),"%c|",key);
/*			if(strlen(toll_amt)==TOLL_AMOUNT_LEN)
			{
				toll_amt[strlen(toll_amt)-1]='\0';
			}*/
		}
	}

//	if(NULL!=strchr(toll_amt,'|'))
//		toll_amt[strlen(toll_amt)-1]='\0';

/*	if(strlen(toll_amt)>0)
	{
		for(i=0;i<strlen(toll_amt);i++)
		{
			strcat(fpswd,"*");
		}
	}*/

//	if(*cursrFlag==2)
//	{
//		if(strlen(fpswd)<8)
//			strcat(fpswd,"|");
//	}

//	if(NULL==strchr(toll_amt,'|'))
//		toll_amt[strlen(toll_amt)]='|';

	//	lcd_printf_ex(ALG_CENTER,20,120,"USER :");
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	lcd_printf_ex(ALG_CENTER,26,60,"Enter Toll Details");
	lcd_printf_xy(16,80,120,"RECEIPT NO");
	lcd_printf_xy(16,170,120," :");
	lcd_printf_xy(16,180,120,"%s",receipt_no);

	lcd_printf_xy(16,90,150,"AMOUNT");
	lcd_printf_xy(16,170,150,":");
	lcd_printf_xy(16,180,150,"%s",toll_amt);

	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();

//	free(fpswd);
	return 1;
}




int Display_Get_RouteNo_Screen(char* route_no,int key,int *shiftKeyLevel)
{

	int reqKey=0,preKey=0;
	time_t t = time(NULL);
	struct tm	*tm = localtime(&t);

	lcd_clean();
	lcd_printf_ex(ALG_CENTER,32,20,"WBTC");
	lcd_printf_ex(ALG_CENTER,24,60,"Please Enter Route No.");


	if(key!=0)
		{

		if((key==KEY_SYM_BACKSPACE)&&(strlen(route_no)>1)&&(strlen(route_no)<=(ROUTE_NO_LEN+1)))
		{
			if(NULL!=strchr(route_no,'|'))
			{
				route_no[strlen(route_no)-2]='|';
				route_no[strlen(route_no)-1]='\0';
			}
			else
				route_no[strlen(route_no)-1]='|';
		}
		else if((strlen(route_no)>0)&&(strlen(route_no)<=(ROUTE_NO_LEN))&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
		{
			if(NULL!=strchr(route_no,'|'))
				sprintf(route_no+(strlen(route_no)-1),"%c|",key);
		}
		else if((strlen(route_no)>0)&&(strlen(route_no)<=(ROUTE_NO_LEN))&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
		{
			preKey = (int)getKeySymbol(key,*shiftKeyLevel,0);
			CstcLog_printf("1. preKey Alhpa %c",preKey);
			reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
			CstcLog_printf("2. reqKey Alhpa %c",reqKey);
			if(NULL!=strchr(route_no,'|'))
			{
				CstcLog_printf("route_no Alhpa %s ::%d",route_no,strlen(route_no));
				CstcLog_printf("route_no Alhpa %c",route_no[(strlen(route_no)-2)]);
				if(strlen(route_no)>1)
				{
					if(route_no[(strlen(route_no)-2)]==preKey)
						sprintf(route_no+(strlen(route_no)-2),"%c|",reqKey);
					else
						sprintf(route_no+(strlen(route_no)-1),"%c|",reqKey);
				}
				else
				{
					sprintf(route_no+(strlen(route_no)-1),"%c|",reqKey);
				}
				CstcLog_printf("route_no Alhpa %s",route_no);
			}
			else if(NULL==strchr(route_no,'|'))
			{
				CstcLog_printf("No cursor route_no Alhpa %s ::%d",route_no,strlen(route_no));
				CstcLog_printf("No cursor route_no Alhpa %c",route_no[(strlen(route_no)-2)]);
				if(strlen(route_no)>1)
				{
					if(route_no[(strlen(route_no)-1)]==preKey)
						sprintf(route_no+(strlen(route_no)-1),"%c",reqKey);
				}

			}
		}
		else if((strlen(route_no)==0)&&(*shiftKeyLevel>0)&&(*shiftKeyLevel<5)&&(key!=KEY_SYM_BACKSPACE))
		{
			reqKey = (int)getKeySymbol(key,*shiftKeyLevel,1);
			CstcLog_printf("3. reqKey Alhpa %c",reqKey);
			sprintf(route_no+(strlen(route_no)),"%c|",reqKey);
		}
		else if((strlen(route_no)==0)&&(*shiftKeyLevel==0)&&(key!=KEY_SYM_BACKSPACE))
			sprintf(route_no+(strlen(route_no)),"%c|",key);

		}


/*	lcd_printf_xy(20,20,120,"Route No :");
	lcd_printf_xy(20,120,120,"%s",route_no);*/
	lcd_printf_ex(ALG_CENTER,20,120,"Route No : %s",route_no);
	lcd_printf_ex(ALG_CENTER,20,190,"%02d/%02d/%d %s", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,wd(tm->tm_year + 1900,tm->tm_mon + 1, tm->tm_mday));
	lcd_flip();
	return 1;
}
