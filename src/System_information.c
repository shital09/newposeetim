/*
 * System_information.c
 *
 *  Created on: 13-Jan-2015
 *      Author: root
 */
#include <sys/sysinfo.h>
#include <string.h>
#include <stdio.h>
#include "System_information.h"
#include <unistd.h>
#include <dirent.h>
#include "lcd_drvr.h"
#include "display_interface.h"

struct sysinfo sys_information;
char sys_info_string[400];
//long double a[4], b[4], loadavg = 0;
char FILE_PATH_SYS_INFO[100];

void set_file_number(void)
{
	DIR *dp;
	char filename_withnum[100];
	int file_num = 0,first_time = 0,file_counter =0,initial_num = 0;
	struct dirent *ent;
	memset(filename_withnum,0,100);
	memset(FILE_PATH_SYS_INFO,0,100);
	sprintf(filename_withnum,"/home/user0/httpd/html/system_information-%d.txt",file_num);
	strcpy(FILE_PATH_SYS_INFO,filename_withnum);
	memset(filename_withnum,0,100);
	sprintf(filename_withnum,"system_information-%d.txt",file_num);
	if ((dp = opendir ("/home/user0/httpd/html/")) != NULL) {
		/* print all the files and directories within directory */
		first_time = 0;
		file_counter =0;
		while ((ent = readdir (dp)) != NULL) {

			if(strstr(ent->d_name,"system_information"))
			{

				if(first_time == 0 )
				{
					sscanf(ent->d_name,"system_information-%d.txt",&file_num);
					sscanf(ent->d_name,"system_information-%d.txt",&initial_num);
					sprintf(filename_withnum,"system_information-%d.txt",file_num);
				}
				first_time = 1;
				//				memset(filename_withnum,0,100);
				//				memset(FILE_PATH_SYS_INFO,0,100);
				//				//sprintf(filename_withnum,"/home/user0/httpd/html/system_information-%d.txt",file_num);
				//				strcpy(FILE_PATH_SYS_INFO,filename_withnum);


				if(strstr(ent->d_name,filename_withnum))//check for the file
				{
					file_num++;
					file_counter++;
					sprintf(filename_withnum,"system_information-%d.txt",file_num);
				}
				else
				{
					memset(filename_withnum,0,100);
					memset(FILE_PATH_SYS_INFO,0,100);
					sprintf(filename_withnum,"/home/user0/httpd/html/system_information-%d.txt",file_num);
					strcpy(FILE_PATH_SYS_INFO,filename_withnum);
					//					sprintf(filename_withnum,"/home/user0/httpd/html/system_information-%d.txt",file_num);
					//					strcpy(FILE_PATH_SYS_INFO,filename_withnum);
				}
				if(file_counter>19)
				{
					sprintf(filename_withnum,"/home/user0/httpd/html/system_information-%d.txt",initial_num);
					remove(filename_withnum);
				}
			}


		}
		memset(filename_withnum,0,100);
		memset(FILE_PATH_SYS_INFO,0,100);
		sprintf(filename_withnum,"/home/user0/httpd/html/system_information-%d.txt",file_num);
		strcpy(FILE_PATH_SYS_INFO,filename_withnum);

		closedir (dp);
	} else {
		/* could not open directory */
		return;
	}


}
int check_and_save_system_usage(void)
{
	long unsigned int prev_free_mem = 0,prev_total_mem = 0,prev_used_mem = 0;
	unsigned long current_free_mem;
//	long double prev_loadavg = 0;//,current_loadavg = 0;
	FILE *file_reader;

	sysinfo(&sys_information);

	current_free_mem = sys_information.freeram;
	//	total_mem = sys_information.totalram;
	memset(sys_info_string,0,400);
	file_reader = fopen(FILE_PATH_SYS_INFO,"r");

	if(file_reader != NULL)
	{
		//fscanf(file_reader,"The current CPU utilization is : %Lf \nTOTAL MEMORY = %lu , FREE MEMORY = %lu , USED MEMORY = %lu",&prev_loadavg,&prev_total_mem,&prev_free_mem,&prev_used_mem);
		//		fscanf(file_reader,"%*s %Lf %*s  %lu %*s  %lu %*s  %lu",&prev_loadavg,&prev_total_mem,&prev_free_mem,&prev_used_mem);
		//		fscanf(file_reader,"The Maximum CPU usage : %Lf %%\nTOTAL MEMORY = %lu , FREE MEMORY = %lu , USED MEMORY = %lu",&prev_loadavg,&prev_total_mem,&prev_free_mem,&prev_used_mem);
		fscanf(file_reader,"TOTAL MEMORY = %lu , FREE MEMORY = %lu , USED MEMORY = %lu",&prev_total_mem,&prev_free_mem,&prev_used_mem);
		fclose(file_reader);
		//		current_loadavg = get_final_cpu_usage();
		//		if(prev_loadavg<(current_loadavg*100))
		//		{
		//			loadavg = current_loadavg;
		//			save_system_status();
		//		}
		//		else
		{
			//if(current_free_mem<prev_free_mem)
			{
//				loadavg = prev_loadavg;
				save_system_status();
			}
		}

	}
	else
	{
		//		if(file_reader != NULL)
		//			fclose(file_reader);
//		loadavg = get_final_cpu_usage();
		save_system_status();
	}
	//	fflush(file_reader);

	return 1;

}
void save_system_status(void)
{
	unsigned long free_mem,total_mem;
	FILE *file_writer;

	sysinfo(&sys_information);

	free_mem = sys_information.freeram;
	total_mem = sys_information.totalram;
	memset(sys_info_string,0,400);
	file_writer = fopen(FILE_PATH_SYS_INFO,"w+");
	//	save_cpu_usage();
	sprintf(sys_info_string+strlen(sys_info_string),"TOTAL MEMORY = %lu , FREE MEMORY = %lu , USED MEMORY = %lu",(total_mem/1024),(free_mem/1024),((total_mem - free_mem)/1024));
	//fprintf(sys_info_string,1,strlen(sys_info_string),file_writer);
	fprintf(file_writer, "%s", sys_info_string);
	//write(file_writer,(char *)sys_info_string,strlen((char *)sys_info_string));
	//fflush(file_writer);
	fclose(file_writer);

}
#if 0
long double get_initial_cpu_usage(void)
{
	//	long double a[4], b[4], loadavg;
	FILE *fp;
	//char dump[50];

	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
	fclose(fp);
	//	sleep(1);

	//	fp = fopen("/proc/stat","r");
	//	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
	//	fclose(fp);
	//
	//	loadavg = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	//printf("The current CPU utilization is : %Lf\n",loadavg);
	//	sprintf(sys_info_string,"The current CPU utilization is : %Lf %\n",loadavg);


	return(loadavg);
}
//---------------------------------------------------------------------------------
long double get_final_cpu_usage(void)
{
	//	long double a[4], b[4], loadavg;
	FILE *fp;
	//char dump[50];


	//	fp = fopen("/proc/stat","r");
	//	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
	//	fclose(fp);
	//	sleep(1);

	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
	fclose(fp);

	//	loadavg = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	//printf("The current CPU utilization is : %Lf\n",loadavg);
	//	sprintf(sys_info_string,"The current CPU utilization is : %Lf %\n",loadavg);


	return(((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3])));
}
int save_cpu_usage(void)
{
	//	long double a[4], b[4], loadavg;
	//	FILE *fp;
	//char dump[50];


	//	fp = fopen("/proc/stat","r");
	//	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
	//	fclose(fp);
	//	sleep(1);
	//
	//	fp = fopen("/proc/stat","r");
	//	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
	//	fclose(fp);

	//loadavg = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	//printf("The current CPU utilization is : %Lf\n",loadavg);
	sprintf(sys_info_string,"The Maximum CPU usage : %Lf %%\n",(loadavg)*100);


	return(0);
}
#endif
