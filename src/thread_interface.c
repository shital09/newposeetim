/*
 * pthread_hello.c
 *
 *  Created on: 15-Apr-2014
 *      Author: root
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include "thread_interface.h"
#include "display_interface.h"
#include "gprs_interface.h"
#include "db_interface.h"

extern waybill_master_struct waybill_master_struct_var;
extern download_response_struct download_response_struct_var;
extern all_flags_struct all_flags_struct_var;
extern pthread_mutex_t upload_semaphore_waybill ;
extern pthread_mutex_t upload_semaphore_schedule ;

char * Databuff = NULL;

pthread_t threads[NUM_THREADS],GPRS_Thread;
extern int manual_thread_enable;
extern int tkt_data_backup_flag;

/**************************************************************************************
 *  Function Name:  DisplayTime
 *  Description:
 *                  This function is used to upload waybill, schedule
 *                  data(i.e. trip details) and transaction data to server.
 *                  1.This function checks data for master is present in database or
 *                    not.
 *                  2.If data is present then it initializes GPRS and sends the data
 *                    to server.
 *                  3.This function sleeps for every 5 minutes(or ticket upload interval
 *                    mentioned in waybill master)
 *
 * Parameters : -				     RETURN VALUE    : -
 ***************************************************************************************/
void* DisplayTime(void* arg)
{

	int Retry_count=0, ticket_count=0, trip_count = 0,counter=0;//, selected_trip_count = 0;
	unsigned file_size_waybill=0;
	char Err_Msg[ERROR_MSG_BUFFER_LEN];
	int Err_Len=0;

	while(1)
	{
		CstcLog_printf("THREAD::In thread function......");

		all_flags_struct_var.online_schedule_update_flag = 0;
		//all_flags_struct_var.tc_online_schedule_update_flag = 0;

		/**************************** Ticket Upload *****************************/

		if(DbInterface_Get_Row_Count("transaction_log_restore",strlen("transaction_log_restore"),1)>0)
			tkt_data_backup_flag=1;


	     if(tkt_data_backup_flag)
	     {
	    	 ticket_count = DbInterface_Get_Row_Count("transaction_log_restore where upload_flag='N' and tkt_sub_type_short_code != 'SL'",81,1);
	         if((ticket_count==0)||(ticket_count>MAX_BACKUP_NO)){
	     		if (DbInterface_exec_sql((unsigned char *)"delete from transaction_log_restore",Err_Msg,&Err_Len)==0)
	     			{
	     				CstcLog_printf("Error: transaction_log_restore clear Failed");
	     				CstcLog_printf("Err_Msg= %s",Err_Msg);
//	     				return -1;
	     			}
	     		tkt_data_backup_flag=0;
	         }
	     }
	     else
		     ticket_count = DbInterface_Get_Row_Count("transaction_log where upload_flag='N' and tkt_sub_type_short_code != 'SL'",73,1);
		CstcLog_printf("THREAD::ticket_count=%d",ticket_count);

		if(ticket_count > 0)
		{
			Databuff = (char *)malloc((NO_OF_CHUNK_LINES*sizeof(transaction_log_struct))+1+NO_OF_CHUNK_LINES);

			CstcLog_printf("Sizeof transaction_log = %d",sizeof(transaction_log_struct));
			CstcLog_printf("Memory Allocated = %d",((NO_OF_CHUNK_LINES*sizeof(transaction_log_struct))+1+NO_OF_CHUNK_LINES));

			Retry_count = 0;
			do
			{
				CstcLog_printf_1("Sending ticket Data...");

				memset(Databuff,0,(NO_OF_CHUNK_LINES*sizeof(transaction_log_struct))+1+NO_OF_CHUNK_LINES);

				/*************fetch ticket data***************/
				memset(download_response_struct_var.end_tkt_cnt,0,TICKET_NO_LEN+1);
				gprs_get_tkt_data(Databuff,download_response_struct_var.end_tkt_cnt);

				CstcLog_printf("THREAD:: download_response_struct_var.end_tkt_cnt=%s",download_response_struct_var.end_tkt_cnt);
				if(atoi(download_response_struct_var.end_tkt_cnt) == 0)
				{
					CstcLog_printf_1("THREAD::BUFFER IS EMPTY");
					Retry_count++;
				}
				/*********************************************/

				all_flags_struct_var.ticket_upload_flag = 1;
				if(Gprs_Interface_Init())
				{
					CstcLog_printf_1("THREAD:: Gprs_Interface_Init()");
					Gprs_Set_PPP_Connection(-1);   //need to check good quality signal
					CstcLog_printf_1("THREAD:: Gprs_Set_PPP_Connection()");
					Gprs_Interface_Deinit();
					all_flags_struct_var.ticket_upload_flag=0;
					CstcLog_printf_1("THREAD:: Gprs_Interface_Deinit() ");
					memset(download_response_struct_var.end_tkt_cnt,0,TICKET_NO_LEN+1);
					break;
				}
				else
				{
					all_flags_struct_var.ticket_upload_flag=0;
					Retry_count++;
				}

			}while(Retry_count<5);

			Retry_count = -1;   //  -1 is used to indicate '|' failure    ( THIS IS JUST NOT TO USE AN EXTRA FLAG)
			all_flags_struct_var.ticket_upload_flag = 0;
			if(Databuff!=NULL)
			{
				free(Databuff);
				Databuff=NULL;
			}
		}

		/**************************** Waybill Upload *****************************/
		file_size_waybill = get_file_size_gprs(WAYBILL_RESPONSE_FILE_ADDR); //check if waybill has been uploaded
		CstcLog_printf_1("file_size_waybill = %d",file_size_waybill);

		if(file_size_waybill != 1)
		{
			if(strlen(waybill_master_struct_var.waybill_no) > 0)
			{
				CstcLog_printf_1("SENDING WAYBILL DETAILS........");

				Databuff = (char*)malloc(sizeof(waybill_master_struct)+NO_OF_CHUNK_LINES+1);
				CstcLog_printf("Sizeof waybill_master_struct= %d",sizeof(waybill_master_struct));
				CstcLog_printf("Memory allocated for waybill upload = %d",(sizeof(waybill_master_struct)+NO_OF_CHUNK_LINES+1));

				Retry_count = 0;
				do
				{
					memset(Databuff,0,(sizeof(waybill_master_struct)+NO_OF_CHUNK_LINES+1));

					/*************fetch waybill data***************/
					get_waybill_string(Databuff);

					if(strlen(Databuff) ==0)
					{
						CstcLog_printf_1("THREAD::WAYBILL BUFFER IS EMPTY");
						Retry_count++;
					}
					/*********************************************/
					all_flags_struct_var.waybill_upload_Flag = 1;

					if(Gprs_Interface_Init())
					{
						CstcLog_printf_1("THREAD:: Gprs_Interface_Init()");
						Gprs_Set_PPP_Connection(-1);   //need to check good quality signal
						CstcLog_printf_1("THREAD:: Gprs_Set_PPP_Connection()");

						Gprs_Interface_Deinit();
						CstcLog_printf_1("THREAD:: Gprs_Interface_Deinit() ");
						break;
					}
					else
					{
						Retry_count++;
					}
				}
				while(Retry_count<5);
				Retry_count = -1;   //  -1 is used to indicate '|' failure    ( THIS IS JUST NOT TO USE AN EXTRA FLAG)

				all_flags_struct_var.waybill_upload_Flag = 0;
				if(Databuff!=NULL)
				{
					free(Databuff);
					Databuff=NULL;
				}
			}
		}

		/**************************** Schedule Upload *****************************/

		trip_count = DbInterface_Get_Row_Count("transaction_log where upload_flag='N' and (tkt_sub_type_short_code = 'SL' or tkt_sub_type_short_code = 'TL' or tkt_sub_type_short_code = 'TC')",142,1);
		CstcLog_printf_1("THREAD::trip_count=%d",trip_count);

/*		selected_trip_count = DbInterface_Get_Row_Count("selected_trip_info",18,1);
		CstcLog_printf_1("THREAD::selected_trip_count=%d",selected_trip_count);*/

		if(trip_count>0 /*|| selected_trip_count>0*/)
		{
			CstcLog_printf_1("SENDING SCHEDULE DETAILS........");

			Databuff = (char *)malloc(NO_OF_CHUNK_LINES*sizeof(schedule_master_struct)+1+NO_OF_CHUNK_LINES);

			CstcLog_printf("Sizeof schedule_master_struct= %d",sizeof(schedule_master_struct));
			CstcLog_printf("Memory allocated for schedule upload = %d",(NO_OF_CHUNK_LINES*sizeof(schedule_master_struct)+1+NO_OF_CHUNK_LINES));

			Retry_count = 0;
			do
			{
				memset(Databuff,0,(NO_OF_CHUNK_LINES*sizeof(schedule_master_struct))+1+NO_OF_CHUNK_LINES);

				/*************fetch ticket data***************/
				memset(download_response_struct_var.end_tkt_cnt,0,TICKET_NO_LEN+1);
				get_schedule_data_for_upload(Databuff,download_response_struct_var.end_tkt_cnt);
				if(atoi(download_response_struct_var.end_tkt_cnt) == 0)
				{
					CstcLog_printf_1("THREAD::BUFFER IS EMPTY");
					Retry_count++;
				}
				/*********************************************/

				all_flags_struct_var.schedule_upload_flag = 1;
				if(Gprs_Interface_Init())
				{
					CstcLog_printf_1("THREAD:: Gprs_Interface_Init()");
					Gprs_Set_PPP_Connection(-1);   //need to check good quality signal
					CstcLog_printf_1("THREAD:: Gprs_Set_PPP_Connection()");
					Gprs_Interface_Deinit();

					all_flags_struct_var.schedule_upload_flag = 0;
					CstcLog_printf_1("THREAD:: Gprs_Interface_Deinit() ");
					memset(download_response_struct_var.end_tkt_cnt,0,TICKET_NO_LEN+1);
					break;
				}
				else
				{
					all_flags_struct_var.schedule_upload_flag = 0;
					Retry_count++;
				}

			}while(Retry_count<5);
			Retry_count = -1;   //  -1 is used to indicate '|' failure    ( THIS IS JUST NOT TO USE AN EXTRA FLAG)

			//all_flags_struct_var.waybill_upload_Flag = 0;
			all_flags_struct_var.schedule_upload_flag = 0;
			if(Databuff!=NULL)
			{
				free(Databuff);
				Databuff=NULL;
			}
		}

		all_flags_struct_var.waybill_upload_Flag = 0;
		all_flags_struct_var.schedule_upload_flag = 0;
		CstcLog_printf_1("THREAD:: Sleeping for %d seconds",atoi(waybill_master_struct_var.tkt_upload_interval)*60);

		//sleep((atoi(waybill_master_struct_var.tkt_upload_interval)*60));

	   counter = atoi(waybill_master_struct_var.tkt_upload_interval)*60;
		while((counter)&&(manual_thread_enable==0))
		{
		   sleep(1);  //((waybill_master_struct_var.tkt_upload_interval)*60) == 5 min
		   counter--;
		}
		CstcLog_printf("manual_flag=%d  counter=%d",manual_thread_enable,counter);
		manual_thread_enable=0;

	}

	return NULL;
}

/********************************************************************************
 *  Function Name:  startThread_DisplayTime
 *  Description:
 *                  Function will create thread
 *
 * Parameters : -				     RETURN VALUE    : 1 : Success
 *                                                     0 : Failure
 *********************************************************************************/
int startThread_DisplayTime(void)
{
	//	pthread_t threads[NUM_THREADS];
	int rc;
	//long t=0;
	//	for(t=0;t<NUM_THREADS;t++){
	//		lcd_printf_ex(ALG_LEFT,16,20,"In main: creating thread %ld", t);

	rc = pthread_create(&GPRS_Thread, NULL, DisplayTime, NULL);
	if (rc){
		//lcd_printf_ex(ALG_LEFT,16,40,"ERROR; return code from pthread_create() is %d\n", rc);
		CstcLog_printf_1("THREAD::ERROR; return code from pthread_create() is %d\n",rc);
		return 0;
	}
	//
	/* Last thing that main() should do */
	//	pthread_exit(NULL);

	return 1;
}


/********************************************************************************
 *  Function Name:  stopThread_DisplayTime
 *  Description:
 *                  Function will wait for thread termination
 *
 * Parameters : -				     RETURN VALUE    : -
 *********************************************************************************/
void stopThread_DisplayTime(void)
{
	pthread_join(0,NULL);
}
