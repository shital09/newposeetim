/*
 * Clock.c
 *
 *  Created on: 14-Sep-2008
 *      Author: root
 */
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include "cstclog.h"
#include "clock.h"
#include "posapi.h"
#include "db_interface.h"
extern waybill_master_struct waybill_master_struct_var;

int settime(unsigned int dd,unsigned int mm,unsigned int yy,unsigned int hr,unsigned int mn,unsigned int sc)
{
    struct tm mytime;

    mytime.tm_sec = sc;    // Seconds
    mytime.tm_min = mn;    // Minutes
    mytime.tm_hour = hr;   // Hours
    mytime.tm_mday = dd;      // Day of Month
    mytime.tm_mon = mm;     // Month
    mytime.tm_year = yy-1900; // Year


    if(sys_set_time(&mytime))
    {
    	CstcLog_printf("The system time is set successfully");
    	return 1;
    }
    else
    {
    	CstcLog_printf("The system time cannot be set");
    	 return 0;
    }





}
//--------------------------------------------------------------------------------------------
void set_waybill_time(void)
{
	unsigned int way_yyyy,way_dd,way_mm,way_hr,way_mn,way_sc;
//	get_waybill_details();

#ifdef USE_WAYBILL_TIME
	sscanf(waybill_master_struct_var.waybill_date,"%0.2d/%0.2d/%d",&way_dd,&way_mm,&way_yyyy);    //Read Formatted Date dd-mm-yyyy
	sscanf(waybill_master_struct_var.waybill_time,"%02d:%02d:%02d",&way_hr,&way_mn,&way_sc);		//Read formatted Time hr:mn:sc
#else
	sscanf(waybill_master_struct_var.duty_start_date,"%2d/%2d/%d",&way_dd,&way_mm,&way_yyyy);    //Read Formatted Date dd-mm-yyyy
	sscanf(waybill_master_struct_var.duty_start_time,"%2d:%2d:%2d",&way_hr,&way_mn,&way_sc);		//Read formatted Time hr:mn:sc
#endif
	settime(way_dd,way_mm-1,way_yyyy,way_hr,way_mn,way_sc);

}
