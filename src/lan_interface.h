/*
 * serialcomm_interface.h
 *
 *  Created on: 09-Apr-2014
 *      Author: root
 */

#ifndef SERIALCOMM_INTERFACE_H_
#define SERIALCOMM_INTERFACE_H_
#include "lcd_drvr.h"
#include "tcp_drvr.h"
#include "db_interface.h"
#include "display_interface.h"
int Manual_Duty_Assignment_server(int DownloadEnableFlag);
int Do_duty_data_exchange(int iclient_socket);

int socket_server_demo(void);
int compose_response_packet(char * bTemp, int iclient_socket ,int iTemp,int *j);
void final_packet_lan(char * pkt_format_ptr, char * pkt_ptr, int* length);
//int received_packet_lan(int i, char tempCh, int iclient_socket,char * bTemp);
int received_packet_lan(int i, char tempCh, int iclient_socket,char * bTemp,int updateDataFlag,int *j);
void get_machine_info(char * sn, int nbytes);
int packet_parser_lan(char * bTemp);
int check_master_status(void);
int packet_parser_input_master(char tempCh,int recvPktID,int updateDataFlag);
void compose_ack_packet(char *pkt_ptr,int *length);
int packet_parser_nresponder(char * bTemp);
void compose_download_response_packet(char * pkt_ptr,int *length);
void compose_transaction_data_packets(char * pkt_ptr,int *length);



#endif /* SERIALCOMM_INTERFACE_H_ */
