/*
 * cstc_defines.h
 *
 *  Created on: 14-Apr-2014
 *      Author: root
 */

#ifndef CSTC_DEFINES_H_
#define CSTC_DEFINES_H_
#include "keyboard_drvr.h"
#include "cstclog.h"

/***** for Live  ******/
#define APP_VERSION "1.0.21"   // powered by trimax//extra trip flag//skip cancel ticket//skip refund ticket//wbtc&reportentry of refund&cancel
#define APP_VER_DATETIME "0309181930"
#define SERVER_PORT 8080
#define SERVER_PORT_LAN 9000

#define UPDATED_PKG_FILE_ADDR "/home/user0/CSTCApp.pkg"
#define DB_ADDR "/home/user0/CSTC/cstc_new.db"
#define DB_ADDR1 "/home/user0/CSTC/cstc_new_thread.db"
//#define LOG_FILE_ADDR "/home/user0/httpd/html/cstc_log.txt"
#define MAC_FILE_ADDR "/home/user0/CSTC/mac_ID.txt"
#define ETIM_NO_FILE_ADDR "/home/user0/etim_no.txt"
#define DB_INPUT_FILE_ADDR "/home/user0/CSTC/db_input.txt"
#define ONLINE_UDATE_FILE_ADDR "/home/user0/CSTC/online_update_file.txt"

#define RESPONSE_CHECK_FILE_ADDR "/home/user0/CSTC/response_check.txt"
#define WAYBILL_RESPONSE_FILE_ADDR "/home/user0/CSTC/waybill_response.txt"

//#define TRANSACTION_CSV_FILE_ADDR "/home/user0/CSTC/test.csv"
#define BANGLA_FONT_FILE_ADDR "/home/user0/fonts/Shonarb.ttf"                       //"/home/user0/fonts/Lohit-Kannada.ttf"
//#define BANGLA_FONT_FILE_ADDR "/home/user0/fonts/Barah_Kan_bold.ttf"
#define ENGLISH_FONT_FILE_ADDR "/usr/share/fonts/wqy-microhei.ttf"
#define ENGLISH_BOLD_FONT_FILE_ADDR "/usr/share/fonts/arialnb.ttf"

#define IMAGE_PATH "/home/user0/CSTC/CSTCLogo.jpg"

#define DEBUG_LOG_ENABLE 0
#define THREAD_DEBUG_LOG_ENABLE 0
#define CSV_FILE_AUDIT 0
#define ERASE_ISSUE_HANDLE 1
#define MULTIPLE_SCHEDULE_ENABLE 1
#define ERROR_MSG_BUFFER_LEN 1024 //500
#define KEYPAD_LOCK_TIME 300  //5 mins lock

#define CHUNK_GPRS_TKT_DATA 0
#define CHUNK_GPRS_TKT_DATA_LEN 5000

#define CORP_NAME "WBTC"
#define MAX_TICKET_NO 99999
#define DOWNLOAD_TICKET_RANGE 600
#define TC_SCHEDULE  0
#define Service_Provider_ENABLE 1

#define COMPRESSION_DECOMPRESSION  0

#define lAST_READ_COUNT 21   // DATA RECEIVED/SAVING FAILED etc.


/*-----------------------Master request Packet ID---------------------------*/
#define	WAYBILL_MASTER_PKT_ID 		 01
#define SCHEDULE_DETAILS_PKT_ID      02
#define	SCHEDULE_MASTER_PKT_ID 		 03
#define	DUTY_MASTER_PKT_ID 			 05
#define START_DUTY_PKT_ID			 06
#define	ROUTE_MASTER_PKT_ID 		 07
#define	FARE_CHART_RECORD_PKT_ID     8
#define	FARE_CHART_PKT_ID 			 9
#define	TICKET_TYPE_PKT_ID 			10
#define	TICKET_SUB_TYPE_PKT_ID 		11
#define	BUS_SERVICE_PKT_ID	 		12
#define	BUS_BRAND_PKT_ID	 		13
#define	DEPOT_MASTER_PKT_ID	 		14
#define	ROUTE_HEADER_PKT_ID	 		15
#define ETM_RW_KEY_PKT_ID			16
#define TRUNK_FEEDER_PKT_ID			17
#define SERVICE_OP_PKT_ID			18
#define RATE_MASTER_PKT_ID			19
#define CHECK_ROUTE_PKT_ID			22
#define LC_LOGIN_PKT_ID				23
#define GPRS_TABLE_PKT_ID			24
#define ROUTE_CARD 					25
#define SHIFT_TYPE 					26
#define CONCESSION_TYPE				27
#define ACTIVITY_MASTER				28
#define ACTIVITY_LOG				29


#define DOWNLOAD_DATA_RECORD_PKT_ID 	 45
#define TRANSACTION_DATA_REQUEST_PKT_ID  47
#define ERASE_DATA_PKT_ID          		 50
#define GET_DETAILS_PKT_ID 				 54
#define REGISTRATION_REQUEST_PKT_ID		 56
#define GPRS_SCHEDULE_MASTER_PKT_ID      24
#define PKG_FILE_NAME_REQ_PKT_ID		 58
#define PKG_FILE_DOWNLOAD_REQ_PKT_ID	 60

//#define	TRANSACTION_LOG_PKT_ID 11

#define	REQ_FOR_WB_PKT_ID 00
#define	DUTY_OFF_PKT_ID 20
#define	DUTY_OFF_CONFIRM_PKT_ID 21
#define	SYNC_REQ_PKT_ID 30
#define	BUS_STOP_SYNC_REQ_PKT_ID 40
#define	BUS_STOP_SYNC_RESPONSE_PKT_ID 41

/*-----------GPRS defines------------*/
//#define OTA_CHUNK_BYTES 5000
#define NO_OF_CHUNK_LINES 30
#define MAX_BACKUP_NO 2000
#define ROUTE_ID_OTA 0
#define RATE_ID_OTA  1

/*----------------------Master response Packet ID---------------------------*/
#define REGISTRATION_RESPONSE_PKT_ID 		 55
#define ACK_PKT_ID 							 51
#define DOWNLOAD_DATA_COUNT_RESPONSE_PCK_ID  46
#define TRANSACTION_DATA 					 48
#define SCHEDULE_RESPONSE_PCK_ID 			 04



/*********************activity_log_master************************/

#define POWER_ON_OFF  								 100
#define LOCK_KEYS									 101
#define REBOOT										 102

//func key menus
#define  ROUTES_DETAILS								150
#define  STAGES_DETAILS								151
#define  TRIP_DETAILS								152

// line_checking
#define LC_STATUS_REPORT						    153
#define PENLTY_TICKET								154
#define INSPECTION_REPORT							155

//main_menu items
#define CLOSE_STAGE            					200
#define TRIP_SELECTION							201
#define TRIP_CANCELLED							202
#define TRIP_CLOSE								203
#define CREATE_EXTRA_TRIP						204
#define CONCESSION_TKT							205
#define REFUND_TKT								206
#define CANCELLED_TKT							207
#define TOLL_ENTRY								208
#define DUTY_ENDING								209
#define LAST_TKT_DETAILS						210
#define SHOW_TKT								211
#define STATUS_REPORT							212

// reports
#define TRIP_WISE_REVENUE_DETAILS 				213
#define CURRENT_TRIP_REPORT                     214
#define ALL_TRIP_STATUS_REPORT					215
#define TRIPWISE_TICKET_DETAILS					216

#define LINE_CHECK								217
#define LOAD_MASTERS							218
#define SYNCING_DATA							219
#define DOWNLOAD_DATA 							220
#define NOT_PRINTED_TKT  						221
#define UPDATE_APP 								222

#define TICKET_ISSUE 							223
#define SPECIAL_TICKET							224



#define POWER_ON_OFF_CODE                           "1"
#define LOCK_KEYS_CODE							    "2"
#define REBOOT_CODE									"3"

//func key menus
#define  ROUTE_DETAILS_CODE							"4"
#define  STAGE_DETAILS_CODE							"5"
#define  TRIP_DETAILS_CODE							"6"

// line_checking
#define LC_STATUS_REPORT_CODE						"7"
#define PENLTY_TICKET_CODE							"8"
#define INSPECTION_REPORT_CODE						"9"

#define CLOSE_STAGE_CODE            				"10"
#define TRIP_SELECTION_CODE							"11"
#define TRIP_CANCEL_CODE							"12"
#define TRIP_CLOSE_CODE								"13"
#define CREATE_EXTRA_TRIP_CODE						"14"

#define CONCESSION_TKT_CODE							"15"
#define REFUND_TKT_CODE								"16"
#define CANCELLED_TKT_CODE							"17"
#define TOLL_ENTRY_CODE								"18"
#define DUTY_END_CODE								"19"
#define LAST_TICKET_DETAILS_CODE					"20"
#define SHOW_TKT_CODE								"21"
#define STATUS_REPORT_CODE							"22"

// reports
#define TRIP_WISE_REVENUE_DETAILS_CODE 				"23"
#define CURRENT_TRIP_REPORT_CODE                    "24"
#define ALL_TRIP_STATUS_REPORT_CODE					"25"
#define TRIPWISE_TICKET_DETAILS_CODE				"26"

#define LINE_CHECKING_CODE							"27"
#define LOAD_MASTERS_CODE							"28"
#define SYNC_DATA_CODE								"29"
#define DOWNLOAD_DATA_CODE 							"30"
#define NOT_PRINTED_TKT_CODE  						"31"
#define UPDATE_APP_CODE 							"32"
#define REPEAT_TICKET								"33"
#define TICKET_ISSUE_CODE 							"34"
#define SPECIAL_TICKET_CODE							"35"



//ota updates
#define UPDATE_FAILED 0
#define UPDATE_FOUND 1
#define NO_UPDATE_FOUND 2
#define UPTO_DATE 3





enum {

	VEHICLE_NO_LEN = 15, SCHEDULE_NO_LEN = 15, DUTY_NO_LEN = 8,ROUTE_NO_LEN = 15,ROUTE_ID_LEN=10,BUS_SRVC_ID_LEN = 2,
	MACHINE_SN_LEN = 8,TRAVELLED_KM_LEN = 6,DEPOT_CODE_LEN = 5,OPERATOR_ID_LEN=2,
	WAYBILL_NO_LEN = 14, NAME_ENG_LEN = 50, NAME_KAN_LEN =150/*74*/, DATE_LEN = 10, TIME_LEN = 8,
	BUS_STOP_CODE_LEN = 5, BUS_STOP_SEQ_LEN = 3,EPURSE_AMT_LEN = 6, AMOUNT_LEN = 7, CURR_COUNT_LEN = 2,
	SHIFT_STATUS_LEN = 1, TRIP_ID_LEN = 4, BUS_STOP_ID_LEN=10,SCHEDULE_ID_LEN = 6,STAGE_CNT_LEN=3,

	/*-----------------------------------WAYBILL---------------------------------*/
	COND_ID_LEN = 10, DRVR_ID_LEN = 15,LOCK_RCPT_LEN = 5, LOCK_AMT_LEN = 16, MASTER_PW_LEN = 6,
	MAC_ADDR_LEN = 12, SOFTWARE_VER_LEN = 5, MAX_ADULT_LEN = 2, MAX_CHILD_LEN = 2, TKT_INTERVAL_LEN = 2,
	COLL_STATUS_LEN = 1,WAYBILL_LOCK_CD_LEN = 6,
	MANUAL_DUTY_EN_FLAG_LEN = 1, FUNCTIONS_LEN = 2, DEPOT_ID_LEN = 5,SERVER_FLD_LEN=25,SERVER_IP_LEN=15,

	/*---------------------------------SCHEDULE----------------------------------*/
	SERIAL_NO_LEN = 4,BRAND_CODE_LEN = 4,SHIFT_LEN = 1,SHIFT_CODE_LEN=3,SHIFT_ID_LEN=3,TRIP_STATUS_LEN = 2, SCHED_STATUS_LEN = 20,
	RATE_ID_LEN = 15,DUTY_END_LEN = 2,

	/*---------------------------------ROUTE-------------------------------------*/
	BUS_STOP_ALIAS_ENG_LEN = 25,FARE_STG_PT_LEN = 1, DISTANCE_LEN = 3, FARE_STAGE_LEN = 1,
	SUB_STAGE_LEN = 2, BUS_BRAND_ID_LEN = 2,

	/*----------------------------------FARE STAGE-------------------------------*/
	VERSION_NO_LEN = 3, ADULT_FARE_LEN = 7, CHILD_FARE_LEN = 7,	SR_CIT_FARE_LEN = 7,
	LUGG_AMT_LEN = 7, TOLL_AMOUNT_LEN= 4,FACTOR_TYPE_LEN = 1,

	/*----------------------------------TRANSACTION------------------------------*/
	TRIP_CARD_NO_LEN=5,
	TRIP_NO_LEN = 2,TRANSACTION_NO_LEN = 5, TICKET_NO_LEN = 5,TICKET_TYPE_LEN = 1,
	FULL_PX_COUNT_LEN = 5, HALF_PX_COUNT_LEN = 5, LUGG_UNITS_LEN = 5,CONCESSION_TYPE_LEN =1,
	GROUP_TICKET_MODE_LEN =1,PAYMENT_MODE_LEN =1,UPLOAD_FLAG_LEN =1,TRIP_DEPARTURE_TIME_LEN = 8,
	TICKET_CODE_LEN = 8, CARD_NO_LEN = 15, CARD_ISSUER_NAME_LEN = 25,EPURSE_LAST_TXN_NO_LEN = 4,
	LINE_CHECK_STATUS_LEN = 8,

	/*------------------------------WAYBILL WISE INFO----------------------------*/
	SR_NO_LEN = 2, START_TKT_COUNT_LEN = 5, END_TKT_COUNT_LEN = 5, TXN_NO_LEN = 5,

	/*----------------------------CURR PASSENGER---------------------------------*/
	PRESENT_PASSG_LEN = 5, TO_BE_ALIGHTED_LEN = 5,

	/*----------------------------SELECTED TRIP---------------------------------*/
	CURR_TRIP_NO_LEN = 2,PASSG_COUNT_LEN = 3, SR_CIT_COUNT_LEN = 3, TRIP_TYPE_LEN = 2, ROUTE_TYPE_LEN =2,
	TRIP_END_CONDN_LEN = 15,

	/*---------------------------------PACKETS----------------------------------*/
	PKT_ID_LEN =2, REGISTR_NO_LEN =10, TERMINAL_ID_LEN = 8,
	SEC_KEY_LEN = 6,ERR_LENGTH_LEN = 2, ERR_LEN = 2,APP_VER_DATETIME_LEN=10,APP_VER_NO_LEN=6,RETURN_LEN=1,
	CHECKSUM_LEN = 40, DATA_LENGTH_LEN = 8, UPDATE_DATA_FLAG_LEN=1,
#if COMPRESSION_DECOMPRESSION
	 START_INDICATOR_LEN=3,
#else
	 START_INDICATOR_LEN=1,
#endif
	/*----------------------Download ACK PACKETS------------------------------*/
	ROW_CNT_LEN =5 ,STR_TKT_LEN = 5, END_TKT_LEN =5,

	/*-----------------------------------mis---------------------------------*/
	CONDR_TYPE=2,PSWRD_LEN=6,CONDR_NAME_LEN=10,CORP_NAME_LEN=4,CARD_ID_LEN =8,PASS_TYPE_NAME_LEN = 15,PASS_NO_LEN=10,
	LC_STAFF_USERNAME_LIMIT=10,CONDUCTOR_USERNAME_LIMIT=10, RFID_KEY_LEN=15,

	/*--------------------------ticket_type---------------------------------*/
	TKT_TYPE_ID_LEN = 2, TKT_TYPE_CODE_LEN = 3,

	/*--------------------------update_pkg_file---------------------------------*/

	PKG_FILE_LEN=10,APP_NAME_LEN =10,


	CONCESSION_ID_LEN=2,CONCESSION_CODE_LEN=2,CONCESSION_RATE_LEN=6,

	TOLL_RECEIPT_NO_LEN=10,



	/*-----------------------------activity_log master-------------------------------------*/
	ACT_CODE_LEN=3


};

enum payment_mode
{
	normal_mode='0',
	epurse_mode='1',
	refund_mode='2',
	cancel_mode='3',
	reissue_mode ='4',
	special_ticket_mode='5'
};




typedef struct conductor_pass_registration
{
	char corp_name[CORP_NAME_LEN+1];
	char depot_id[(DEPOT_ID_LEN+1)];
	char card_id_no[CARD_ID_LEN+1];
	char pass_type_name[PASS_TYPE_NAME_LEN+1];
	char conductor_type[CONDR_TYPE+1];

	char conductor_id[(COND_ID_LEN+1)];
	char conductor_name[(CONDR_NAME_LEN+1)];
	char conductor_password[(PSWRD_LEN+1)];
	char conductor_licn_expy_date[DATE_LEN+1];
	char issuer_conductor_id[(COND_ID_LEN+1)];
	char issuer_conductor_name[(CONDR_NAME_LEN+1)];
	char issuer_waybill_no[WAYBILL_NO_LEN+1];
	char issuer_etim_no[MACHINE_SN_LEN+1];
	char last_update_date[DATE_LEN+1];
	char last_update_time[TIME_LEN+1];
	char validity_from_date[DATE_LEN+1];
	char validity_till_date[DATE_LEN+1];
	char card_status;
}conductor_pass_registration_struct;

typedef struct ack_packet
{
	char pkt_id_ack_packet[PKT_ID_LEN+1];	// Packet ID : 51
	char terminal_id[TERMINAL_ID_LEN+1];
	char machine_no[MACHINE_SN_LEN+1];
	char mac_address[MAC_ADDR_LEN+1];
	char sec_key_ack[SEC_KEY_LEN+1];
	char err_length[ERR_LENGTH_LEN+1];
	char *err_ack;  			// “OK” if successful else error description
	char return_type[RETURN_LEN+1];
}ack_packet_struct;

typedef struct route_master
{
	char route_no[ROUTE_NO_LEN+1];
	char route_id[ROUTE_ID_LEN+1]; //not used //swap 10Nov 11:45
	char bus_stop_seq_no[BUS_STOP_SEQ_LEN+1];
	char bus_stop_code_english[BUS_STOP_CODE_LEN+1];
	char bus_stop_name_english[NAME_ENG_LEN+1];
	char bus_stop_code_bangla[BUS_STOP_CODE_LEN+1];
	char bus_stop_name_bangla[NAME_KAN_LEN+1];
	char bus_stop_id[BUS_STOP_ID_LEN+1];
	char bus_stop_alias_english[NAME_ENG_LEN+1]; //as per new ITS
	char sub_stage[SUB_STAGE_LEN+1]; // Y or N /[SUB_STG_LEN+1]; // not in doc or in NEW table
	char distance[DISTANCE_LEN+1];
	char fare_stage; //Y or N
	char tf_change_point; //Y or N / [FARE_STG_PT_LEN+1]; // not in doc
	/* not used
	 * char schedule_no[SCHEDULE_NO_LEN+1]; */

	char is_toll; // y or n
	char toll_amt[TOLL_AMOUNT_LEN+1];

}route_master_struct;

typedef struct master_status
{

	int ticket_fare;
	int download_ticket;
	int machine_register;
	int upload_master;
	int erase_ticket;
	int start_duty;
	int insert_waybill;
	int downloadTicketFlag;
	int machineRegisterFlag;
	int uploadMasterFlag;
	int ticketFareFlag;
	int eraseTicketFlag;
	int startDutyFlag;
	int insertWaybillFlag;
	int insert_route;
	int insertrouteFlag;
}master_status_struct;

//user_credentials(username  , password );
#if 0 //not used
typedef struct user_credentials
{
	char username[15];
	char password[15];

}user_credentials_struct;
#endif

typedef struct waybill_master
{
	char waybill_no[WAYBILL_NO_LEN+1];
	char depot_id[DEPOT_ID_LEN+1];
	char conductor_id[COND_ID_LEN+1];
	char conductor_name_english[NAME_ENG_LEN+1];
	char conductor_name_bangla[NAME_KAN_LEN+1];
	char driver_id[DRVR_ID_LEN+1];
	char driver_name_english[NAME_ENG_LEN+1];
	char driver_name_bangla[NAME_KAN_LEN+1];
	char vehicle_no[VEHICLE_NO_LEN+1];
	char etim_no[MACHINE_SN_LEN+1];
	char waybill_date[DATE_LEN+1];
	char waybill_time[TIME_LEN+1];
	char duty_start_date[DATE_LEN+1];
	char duty_start_time[TIME_LEN+1];
	char locking_date[DATE_LEN+1];
	char locking_time[TIME_LEN+1];

	char locking_reciept[LOCK_RCPT_LEN+1];
	char locking_amt[LOCK_AMT_LEN+1]; //not in table

	char master_password[MASTER_PW_LEN+1];
	char mac_address[MAC_ADDR_LEN+1];
	char software_ver[SOFTWARE_VER_LEN+1];
	char max_px_cnt[MAX_ADULT_LEN+1];
	char tkt_upload_interval[TKT_INTERVAL_LEN+1];
	char shift_status;
	char collection_status;
	char duty_no[DUTY_NO_LEN+1];
	char waybill_lock_code[WAYBILL_LOCK_CD_LEN+1];
	char manual_duty_enable_flag;
	char functions[FUNCTIONS_LEN+1];
	char waybill_type;
	char conductor_password[MASTER_PW_LEN+1];
	char GPRS_IP[SERVER_IP_LEN+1];
	char GPRS_Field_Name[SERVER_FLD_LEN+1];
}waybill_master_struct;


typedef struct route_header
{
	char route_no[ROUTE_NO_LEN+1];
	char route_id[ROUTE_ID_LEN+1];
	char bus_Service_id[BUS_SRVC_ID_LEN+1];
	char total_stops[2];
	char start_bus_stop_code_english[BUS_STOP_CODE_LEN+1];
	char start_bus_stop_name_english[NAME_ENG_LEN+1];
	char start_bus_stop_code_bangla[BUS_STOP_CODE_LEN+1];
	char start_bus_stop_name_bangla[NAME_ENG_LEN+1];
	char start_bus_stop_id[BUS_STOP_ID_LEN+1];

	char end_bus_stop_code_english[BUS_STOP_CODE_LEN+1];
	char end_bus_stop_name_english[NAME_ENG_LEN+1];
	char end_bus_stop_code_bangla[BUS_STOP_CODE_LEN+1];
	char end_bus_stop_name_bangla[NAME_ENG_LEN+1];
	char end_bus_stop_id[BUS_STOP_ID_LEN+1];

	char via_bus_stop_code_english[BUS_STOP_CODE_LEN+1];
	char via_bus_stop_name_english[NAME_ENG_LEN+1];
	char via_bus_stop_code_bangla[BUS_STOP_CODE_LEN+1];
	char via_bus_stop_name_bangla[NAME_ENG_LEN+1];
	char via_bus_stop_id[BUS_STOP_ID_LEN+1];

	char depot_id [DEPOT_ID_LEN+1];
	char fare_type ;
	char route_type ;
	char schedule_id[SCHEDULE_ID_LEN+1];
	char rate_id[RATE_ID_LEN+1];

}route_header_struct;




typedef struct route_card
{
	char route_id[ROUTE_ID_LEN+1];
	char route_no[ROUTE_NO_LEN+1];
	char shift_id[SHIFT_ID_LEN+1];
	char shift_type[NAME_KAN_LEN+1];
	char start_card_no[TRIP_CARD_NO_LEN+1];
	char end_card_no[TRIP_CARD_NO_LEN+1];
	char bus_Service_id[BUS_SRVC_ID_LEN+1];
	char rate_id[RATE_ID_LEN +1];
	char updated_date[DATE_LEN+1];
	char updated_time[TIME_LEN+1];

}route_card_struct;


typedef struct selected_trip_info//correct
{
	char trip_id_qstr[TRIP_ID_LEN+1];
	char schedule_no_qstr[SCHEDULE_NO_LEN+1];
	char route_no_qstr[ROUTE_NO_LEN+1];
	char route_id_qstr[ROUTE_ID_LEN+1];
	char bus_brand_id_qstr[BUS_BRAND_ID_LEN+1];
	char bus_service_id_qstr[BUS_SRVC_ID_LEN+1];

	char depot_id_qstr[DEPOT_ID_LEN+1];
	char vehicle_no_qstr[VEHICLE_NO_LEN+1];
	char shift_qstr[SHIFT_ID_LEN+1];
	char effective_from_date_qstr[DATE_LEN+1];
	char effective_from_time_qstr[TIME_LEN+1];

	char effective_till_date_qstr[DATE_LEN+1];
	char effective_till_time_qstr[TIME_LEN+1];

	char start_date_qstr[DATE_LEN+1];
	char start_time_qstr[TIME_LEN+1];
	char end_date_qstr[DATE_LEN+1];
	char end_time_qstr[TIME_LEN+1];

	char start_stop_seq_no_qstr[BUS_STOP_SEQ_LEN+1];
	char start_bus_stop_code_english_qstr[BUS_STOP_CODE_LEN+1];
	char start_bus_stop_name_english_qstr[NAME_ENG_LEN+1];
	char start_bus_stop_code_bangla_qstr[BUS_STOP_CODE_LEN+1];
	char start_bus_stop_name_bangla_qstr[NAME_KAN_LEN+1];
	char start_bus_stop_id_qstr[BUS_STOP_ID_LEN+1];

	char end_bus_stop_seq_no_qstr[BUS_STOP_SEQ_LEN+1];
	char end_bus_stop_code_english_qstr[BUS_STOP_CODE_LEN+1];
	char end_bus_stop_name_english_qstr[NAME_ENG_LEN+1];
	char end_bus_stop_code_bangla_qstr[BUS_STOP_CODE_LEN+1];
	char end_bus_stop_name_bangla_qstr[NAME_KAN_LEN+1];
	char end_bus_stop_id_qstr[BUS_STOP_ID_LEN+1];

	char is_flexi_fare;
	char bus_service_name_english_qstr[NAME_ENG_LEN+1];
	char bus_service_name_bangla_qstr[NAME_KAN_LEN+1];

	char current_selected_start_stop_seq_no_qstr[BUS_STOP_SEQ_LEN+1];
	char current_selected_start_stop_code_english_qstr[BUS_STOP_CODE_LEN+1]; //not used
	char current_selected_start_stop_name_english_qstr[NAME_ENG_LEN+1];
	//	char current_selected_start_stop_code_bangla_qstr[BUS_STOP_CODE_LEN+1];
	//	char current_selected_start_stop_name_bangla_qstr[NAME_KAN_LEN+1]; not used
	char current_selected_bus_stop_id_qstr[BUS_STOP_ID_LEN+1];
	char current_trip_no_qstr[TRIP_NO_LEN+1];

	char trip_type_qstr[TRIP_TYPE_LEN+1]; //....
	char route_type_qstr[ROUTE_TYPE_LEN+1];  //..... NR or TR or FR
	//	char gprs_flag_qstr; not used
	int totalfetchedstops;

	char is_peak_hour;
	char rate_id[RATE_ID_LEN +1];
	char serial_no[SERIAL_NO_LEN +1];
	char schedule_id[SCHEDULE_ID_LEN+1];

	char update_date[DATE_LEN+1];
	char update_time[TIME_LEN+1];

	char selected_date[DATE_LEN+1];
	char selected_time[TIME_LEN+1];
	char card_no[TRIP_CARD_NO_LEN+1];

}selected_trip_info_struct;


typedef struct schedule_master
{
	char serial_no[SERIAL_NO_LEN+1];
	char trip_id[TRIP_ID_LEN+1];
	char schedule_no[SCHEDULE_NO_LEN+1];
	char route_no[ROUTE_NO_LEN+1];
	char route_id[ROUTE_ID_LEN+1];
	char bus_brand_id[BRAND_CODE_LEN+1];
	char bus_service_id[BUS_SRVC_ID_LEN+1];
	char depot_id[DEPOT_ID_LEN+1];
	char vehicle_no[VEHICLE_NO_LEN+1];
	char shift[SHIFT_ID_LEN+1];
	char effective_from_date[DATE_LEN+1];
	char effective_from_time[TIME_LEN+1];
	char effective_till_date[DATE_LEN+1];
	char effective_till_time[TIME_LEN+1];
	char start_date[DATE_LEN+1];
	char start_time[TIME_LEN+1];
	char end_date[DATE_LEN+1];
	char end_time[TIME_LEN+1];
	char start_bus_stop_code[BUS_STOP_CODE_LEN+1];
	char start_bus_stop_id[BUS_STOP_ID_LEN+1];
	char end_bus_stop_code[BUS_STOP_CODE_LEN+1];
	char end_bus_stop_id[BUS_STOP_ID_LEN+1];
	char is_flexi_fare;
	char trip_status[TRIP_STATUS_LEN+1];  //O/C
	char trip_type[TRIP_TYPE_LEN+1];   //       //..... up or dn
	char trip_end_condition[TRIP_END_CONDN_LEN+1];
	char is_peak_hour;
	char rate_id[RATE_ID_LEN +1];
	char schedule_id[SCHEDULE_ID_LEN+1];
	char update_date[DATE_LEN+1];
	char update_time[TIME_LEN+1];
	char card_no[TRIP_CARD_NO_LEN+1];

	//	char trip_end_condition[TRIP_END_CONDN_LEN+1];
}schedule_master_struct;

typedef struct waybill_wise_info
{
	char serial_no_qstr[SERIAL_NO_LEN+1];
	char waybill_no_qstr[WAYBILL_NO_LEN+1];
	char start_transaction_no[TXN_NO_LEN+1];
	char end_transaction_no[TXN_NO_LEN+1];
	char start_ticket_count_qstr[START_TKT_COUNT_LEN+1];
	char end_ticket_count_qstr[END_TKT_COUNT_LEN+1];
	char total_collection_amount_qstr[AMOUNT_LEN+1];
	char effective_from_date_qstr[DATE_LEN+1];
	char effective_from_time_qstr[TIME_LEN+1];
	char effective_till_date_qstr[DATE_LEN+1];
	char effective_till_time_qstr[TIME_LEN+1];
}waybill_wise_info_struct;

typedef struct trip_details
{
	char schedule_no_qstr[SCHEDULE_NO_LEN+1] ;
	char route_no_qstr[ROUTE_NO_LEN+1];
	char route_id_qstr[ROUTE_ID_LEN+1];
	char bus_service_id_qstr[BUS_SRVC_ID_LEN+1];
	char trip_no_qstr[TRIP_NO_LEN+1];
	char trip_direction_qstr;
	char bus_stop_seq_no_qstr[BUS_STOP_SEQ_LEN+1];
	char bus_stop_id_qstr[BUS_STOP_ID_LEN+1];
	char board_full_count_qstr[FULL_PX_COUNT_LEN+1];
	char board_luggage_count_qstr[LUGG_UNITS_LEN+1];
	char board_half_count_qstr[HALF_PX_COUNT_LEN+1];
	char board_srctzn_count_qstr[SR_CIT_COUNT_LEN+1];
	char board_pass_count_qstr[PASSG_COUNT_LEN+1];
	char alight_full_count_qstr[FULL_PX_COUNT_LEN+1];
	char alight_luggage_count_qstr[LUGG_UNITS_LEN+1];
	char alight_half_count_qstr[HALF_PX_COUNT_LEN+1];
	char alight_srctzn_count_qstr[SR_CIT_COUNT_LEN+1];
	char alight_pass_count_qstr[PASSG_COUNT_LEN+1];
}trip_details_struct;

#if 0 // not used
typedef struct ticket_fares
{
	char version_id[VERSION_NO_LEN+1];
	char route_no[ROUTE_NO_LEN+1];
	char route_id[ROUTE_ID_LEN+1];
	char bus_service_id[BUS_SRVC_ID_LEN+1];
	char from_stop_seq_no[BUS_STOP_SEQ_LEN+1];
	char from_bus_stop_code_english_qstr[BUS_STOP_CODE_LEN+1];
	char from_bus_stop_name_english_qstr[NAME_ENG_LEN+1];
	char from_bus_stop_code_bangla_qstr[BUS_STOP_CODE_LEN+1];
	char from_bus_stop_name_bangla_qstr[NAME_KAN_LEN+1];
	char from_bus_stop_id_qstr[BUS_STOP_ID_LEN+1];

	char till_stop_seq_no[BUS_STOP_SEQ_LEN+1];
	char till_bus_stop_code_english_qstr[BUS_STOP_CODE_LEN+1];
	char till_bus_stop_name_english_qstr[NAME_ENG_LEN+1];
	char till_bus_stop_code_bangla_qstr[BUS_STOP_CODE_LEN+1];
	char till_bus_stop_name_bangla_qstr[NAME_KAN_LEN+1];
	char till_bus_stop_id_qstr[BUS_STOP_ID_LEN+1];


	char travelled_km[TRAVELLED_KM_LEN+1];
	char adult_fare[ADULT_FARE_LEN+1];
	char child_fare[CHILD_FARE_LEN+1];
	char sr_citizen_fare[SR_CIT_FARE_LEN+1];
	char luggage_amt_per_unit[LUGG_AMT_LEN+1];
	//char toll_amt[TOLL_AMOUNT_LEN+1];
	//char factor_type;
	char fare_type;
	char Effective_from_date[DATE_LEN+1];

	char effective_from_date[DATE_LEN+1];
	char effective_from_time[TIME_LEN+1];
	char effective_till_date[DATE_LEN+1];
	char effective_till_time[TIME_LEN+1];
	char route_type[ROUTE_TYPE_LEN+1];  //NR or TR or FR
	char feeder_route_no[ROUTE_NO_LEN+1];
}ticket_fares_struct;
#endif

typedef struct ticket_type
{
	char tkt_type_id[TKT_TYPE_ID_LEN+1];
	char tkt_type_short_code[TKT_TYPE_CODE_LEN+1];
	char tkt_type_name_english[NAME_ENG_LEN+1];
	char tkt_type_name_bangla[NAME_KAN_LEN+1];

}ticket_type_struct;		//205

typedef struct ticket_sub_type
{
	char tkt_sub_type_id[TKT_TYPE_ID_LEN+1];
	char tkt_sub_type_short_code[TKT_TYPE_CODE_LEN+1];
	char tkt_sub_type_name_english[NAME_ENG_LEN+1];
	char tkt_sub_type_name_bangla[NAME_KAN_LEN+1];

}ticket_sub_type_struct;	//205

typedef struct bus_service
{
	char bus_service_id[BUS_SRVC_ID_LEN+1];
	char bus_short_code[BUS_STOP_CODE_LEN+1];
	char bus_service_name_english[NAME_ENG_LEN+1];
	char bus_service_name_bangla[NAME_KAN_LEN+1];

}bus_service_struct;		//207

typedef struct bus_brand
{
	char bus_brand_id[BUS_BRAND_ID_LEN+1];
	char bus_brand_short_code[3+1];
	char bus_brand_name_english[NAME_ENG_LEN+1];
	char bus_brand_name_bangla[NAME_KAN_LEN+1];
}bus_brand_struct; 	//205

typedef struct etim_rw_keys
{
	char service_id[2+1];
	char  auth_key_a[RFID_KEY_LEN+1];
	char  auth_key_b[RFID_KEY_LEN+1];
}etim_rw_keys_struct;    //32

typedef struct depot
{
	char depot_id[DEPOT_ID_LEN+1];
	char depot_code[DEPOT_CODE_LEN+1];
	char depot_name_english[NAME_ENG_LEN+1];
	char depot_name_bangla[NAME_KAN_LEN+1];
	char GPRS_IP[SERVER_IP_LEN+1];
	char GPRS_File_Name[SERVER_FLD_LEN+1];
}depot_struct;           //237

typedef struct duty_master
{
	char duty_no[DUTY_NO_LEN+1];
	char schedule_no[SCHEDULE_NO_LEN+1];
	char schedule_status[SCHED_STATUS_LEN+1];
	char updated_by[CONDR_NAME_LEN+1];
	char updated_date[DATE_LEN+1];
	char updated_time[TIME_LEN+1];
	char schedule_id[SCHEDULE_ID_LEN+1];

}duty_master_struct;	//76


#if 0 //not used
typedef struct trunk_feeder_master
{
	char tf_id[2+1];
	char trunk_route_no[ROUTE_NO_LEN+1];
	char trunk_route_id[ROUTE_ID_LEN+1];
	char feeder_route_no[ROUTE_NO_LEN+1];
	char feeder_route_id[ROUTE_ID_LEN+1];

	char tf_change_stop_seq_no[BUS_STOP_SEQ_LEN+1];
	char tf_change_stop_code_english[BUS_STOP_CODE_LEN+1];
	char tf_change_stop_name_english[NAME_ENG_LEN+1];
	char tf_change_stop_code_bangla[BUS_STOP_CODE_LEN+1];
	char tf_change_stop_name_bangla[NAME_KAN_LEN+1];
	char tf_change_stop_id[BUS_STOP_ID_LEN+1];

}trunk_feeder_master_struct;
#endif

typedef struct fare_chart
{
	/*	not used
	char version_id[VERSION_NO_LEN+1];
	char route_no[ROUTE_NO_LEN+1];
	char route_id[ROUTE_ID_LEN+1];

	char bus_service_id[BUS_SRVC_ID_LEN+1];
	char from_stop_seq_no[BUS_STOP_SEQ_LEN+1];
	char from_bus_stop_code_english_qstr[BUS_STOP_CODE_LEN+1];
	char from_bus_stop_name_english_qstr[NAME_ENG_LEN+1];
	char from_bus_stop_code_bangla_qstr[BUS_STOP_CODE_LEN+1];
	char from_bus_stop_name_bangla_qstr[NAME_KAN_LEN+1];
	char from_bus_stop_id[BUS_STOP_ID_LEN+1];

	char till_stop_seq_no[BUS_STOP_SEQ_LEN+1];
	char till_bus_stop_code_english_qstr[BUS_STOP_CODE_LEN+1];
	char till_bus_stop_name_english_qstr[NAME_ENG_LEN+1];
	char till_bus_stop_code_bangla_qstr[BUS_STOP_CODE_LEN+1];
	char till_bus_stop_name_bangla_qstr[NAME_KAN_LEN+1];
	char till_bus_stop_id[BUS_STOP_ID_LEN+1];*/

	char travelled_km[TRAVELLED_KM_LEN+1];
	char adult_fare[ADULT_FARE_LEN+1];
	char child_fare[CHILD_FARE_LEN+1];
	char sr_citizen_fare[SR_CIT_FARE_LEN+1];
	char luggage_amt_per_unit[LUGG_AMT_LEN+1];

	char toll_amt[TOLL_AMOUNT_LEN+1]; //toll amount that comes from route_master
	/*	not used
	 * char fare_type;
	char effective_from_date[DATE_LEN+1];
	char effective_from_time[TIME_LEN+1];
	char effective_till_date[DATE_LEN+1];
	char effective_till_time[TIME_LEN+1];
	char route_type ;
	char feeder_route_no;*/
}fare_chart_struct;


typedef struct transaction_log
{
	char waybill_no[WAYBILL_NO_LEN+1];
	char schedule_no[SCHEDULE_NO_LEN+1];
	char trip_no[TRIP_NO_LEN+1];
	char etim_no[TERMINAL_ID_LEN+1];
	char route_no[ROUTE_NO_LEN+1];

	char route_id[ROUTE_ID_LEN+1];
	char transaction_no[TRANSACTION_NO_LEN+1];
	char ticket_no[TICKET_NO_LEN+1];
	char tkt_type_short_code[TKT_TYPE_CODE_LEN+1];   //PT or LT or GP or IT or EP or PS
	char tkt_sub_type_short_code[TKT_TYPE_CODE_LEN+1]; //AD or CH or SC or LW or HW or


	char from_stop_seq_no[BUS_STOP_SEQ_LEN+1];
	char from_bus_stop_code_english[BUS_STOP_CODE_LEN+1];
	char from_bus_stop_name_english[NAME_ENG_LEN+1];
	char from_bus_stop_code_bangla[BUS_STOP_CODE_LEN+1];
	char from_bus_stop_name_bangla[NAME_KAN_LEN+1];

	char from_bus_stop_id[BUS_STOP_ID_LEN+1];
	char till_stop_seq_no[BUS_STOP_SEQ_LEN+1];
	char till_bus_stop_code_english[BUS_STOP_CODE_LEN+1];
	char till_bus_stop_name_english[NAME_ENG_LEN+1];
	char till_bus_stop_code_bangla[BUS_STOP_CODE_LEN+1];

	char till_bus_stop_name_bangla[NAME_KAN_LEN+1];
	char till_bus_stop_id[BUS_STOP_ID_LEN+1];
	char travelled_km[TRAVELLED_KM_LEN+1];
	char px_count[FULL_PX_COUNT_LEN+1];
	char lugg_units[LUGG_UNITS_LEN+1];

	char px_total_amount[AMOUNT_LEN+1];
	char lugg_total_amount[LUGG_AMT_LEN+1];
	char toll_amt[TOLL_AMOUNT_LEN+1];
	char total_ticket_amount[AMOUNT_LEN+1];
	char concession_type;  //0 or 1

	char group_ticket_mode; //0 or 1
	char payment_mode; //0 or 1
	char ticket_date[DATE_LEN+1];
	char ticket_time[TIME_LEN+1];
	char upload_flag; //Y or N

	char bus_service_id[BUS_SRVC_ID_LEN+1];
	char fare_type;
	char line_check_status[LINE_CHECK_STATUS_LEN+1];
	char depot_id[DEPOT_ID_LEN+1];
	char vehicle_no[VEHICLE_NO_LEN+1];

	char ticket_code[TICKET_CODE_LEN+1];
	//char pass_id[CARD_NO_LEN+1];
	char pass_id[PASS_NO_LEN+1];
	char epurse_card_no[CARD_NO_LEN+1];
	char epurse_card_issuer_name [CARD_ISSUER_NAME_LEN+1];
	char epurse_curr_amount_qstr[EPURSE_AMT_LEN+1];

	char epurse_last_amount_qstr[EPURSE_AMT_LEN+1];
	char epurse_last_waybill_no_qstr[WAYBILL_NO_LEN+1];
	char epurse_last_tkt_no[TICKET_NO_LEN+1];
	char epurse_last_tkt_date[DATE_LEN+1]; // not in doc
	char epurse_last_tkt_time[TIME_LEN+1]; // not in doc

	char epurse_last_from_stop_code[BUS_STOP_CODE_LEN+1];
	char epurse_last_to_stop_code[BUS_STOP_CODE_LEN+1];
	char tkt_printed_flag;  // Y or N
	char shift[SHIFT_ID_LEN+1];
	char trip_status[TRIP_STATUS_LEN+1];

	char trip_end_condition[TRIP_END_CONDN_LEN+1];
	char waybill_type;
	char schedule_id[SCHEDULE_ID_LEN+1];
	char conductor_token_id[COND_ID_LEN+1];
	char rate_id[RATE_ID_LEN+1];

	char stage_count[STAGE_CNT_LEN+1];
	char card_no[TRIP_CARD_NO_LEN+1];
	char duty_end[DUTY_END_LEN+1];


}transaction_log_struct;


typedef struct current_passengers_details
{
	char full_count_qstr[FULL_PX_COUNT_LEN+1];
	char half_count_qstr[HALF_PX_COUNT_LEN+1];
	char srctzn_count_qstr[SR_CIT_COUNT_LEN+1];

	char luggage_count_qstr[LUGG_UNITS_LEN+1];
	char pass_count_qstr[PASSG_COUNT_LEN+1];

	char present_passengers_qstr[PRESENT_PASSG_LEN+1];
	char to_be_alighted_on_next_stage_qstr[TO_BE_ALIGHTED_LEN+1];
}current_passengers_details_struct;

typedef struct service_op
{
	char operator_id[OPERATOR_ID_LEN+1] ;
	char operator_name_english[NAME_ENG_LEN+1] ;
	char operator_name_bangla[NAME_KAN_LEN+1] ;
	char print_flag;
}service_op_struct;

typedef struct all_flags
{
	//ETIM status flags
	unsigned short collectionFlag;
	unsigned short waybillExpiredFlagInt;
	unsigned short etimLockedInt;

	//Miscellaneous flags
	//	unsigned short terminal_id_flag; not used
	//	unsigned short e_purse_var; not used
	unsigned short routeSelectionFlag;
	unsigned short stageClosedFlag;
	unsigned short inspector_status_flag;
	unsigned short inspection_report_flag;
	unsigned short tc_auth_flag;
	unsigned short line_check_auth_flag;
	//	unsigned short uart_comm_flag;
	//	unsigned short gprs_on_flag;
	//	unsigned short ppp_on_flag;
	unsigned short ticket_upload_flag;

	//Ticket flags
	unsigned short Adult_Ticket_Flag;
	unsigned short Child_Ticket_Flag;
	unsigned short Sr_Citizen_Ticket_Flag;
	unsigned short Luggage_Ticket_Flag;
	unsigned short Repeat_Last_Ticket_Flag;
	unsigned short next_selection_Flag;
	unsigned short trunk_feeder_flag;
	unsigned short penalty_ticket_flag;
	unsigned short book_case_flag;

	/*not used
	unsigned short Student_pass_flag;
	unsigned short Blind_pass_flag;
	unsigned short Physically_Challenged_pass_flag;
	unsigned short Freedom_Fighter_pass_flag;
	unsigned short Police_Duty_pass_flag;
	unsigned short Police_Warrant_pass_flag;
	unsigned short From_Stop_Fare_Stage_Flag;
	unsigned short Till_Stop_Fare_Stage_Flag;*/

	unsigned short pass_selection_Flag;

	unsigned short epurse_ticket_flag;
	unsigned short online_auth;

	unsigned short waybill_upload_Flag;
	unsigned short schedule_upload_flag;
	unsigned short online_schedule_update_flag;

	unsigned short trip_close_flag;
	unsigned short stage_close_flag;
	unsigned short trip_cancel_flag;
	unsigned short not_printing_flag;
	unsigned short refund_flag;
	unsigned short cancel_ticket_flag;
	unsigned short special_ticket_flag;

	unsigned short concession_flag;
	unsigned short toll_flag;
	unsigned short trip_start_flag;

	unsigned short duty_end_flag;



	//	unsigned short tc_online_schedule_update_flag;
}all_flags_struct;

typedef struct trip_end_flag
{
	unsigned short Accident;
	unsigned short Breakdown;
	unsigned short Cancelled;
	unsigned short Completed;
}trip_end_flag_struct;

typedef struct cancel_menu_flag
{
	unsigned short full_cancellation;
	unsigned short partial_cancellation;
}cancel_menu_flag_struct;

#if 0 //not used
enum pass_types
{
	Student = 1,
	Blind,
	Physically_Challenged,
	Freedom_Fighter,
	Police_Duty,
	Police_Warrant

};

typedef struct
{
	char* Student,
	*Blind,
	*Physically_Challenged,
	*Freedom_Fighter,
	*Police_Duty,
	*Police_Warrant,
	*totalAmt;
	char* frmStp;
}pass_display_struct;

enum ticket_types
{
	Normal=1,
	Concession,
	lugg,
	Warrant,
	toll,
	pass,
	penalty //(inspector ticket)

};

enum duty_off_status
{
	Success=0,
	Failed=1
};

enum payment_mode
{
	Cash=1,
	Epurse=2
};

enum gprs_upload_flag
{
	Uploaded=1,
	Not_uploaded=2
};
#endif

typedef struct
{
	char* adultCnt, *childCnt, *totalAmt, *srCitizenCnt, *lugg;
	char *luggtotalAmt;
	char* lightWeightKg, *heavyWeightKg, *tvElectKg, *petAnimalsDogCnt, *petAnimalsBirdsCnt, *newsPaperKg;
	int adultCntLen, childCntLen,aindex,cindex,srCitizenCntLen,srcindex;
	int lightWeightKgLen, heavyWeightKgLen,lwindex,hwindex;
	int tvElectKgLen, newsPaperKgLen,tveindex,npindex;
	int petAnimalsDogCntLen, petAnimalsBirdsCntLen,pdindex,pbindex,inspector_status_flag,inspection_report_flag;
	//	char* frmStp,*tilStp; not used
}ticket_display_struct;

typedef struct
{
	char stop_seq_no_QStrLst[BUS_STOP_SEQ_LEN+1];
	char stop_code_english_QStrLst[BUS_STOP_CODE_LEN+1];
	char stop_name_english_QStrLst[NAME_ENG_LEN+1];
	char stop_code_bangla_QStrLst[BUS_STOP_CODE_LEN+1];
	char stop_name_bangla_QStrLst[NAME_KAN_LEN+1];
	char stop_id_QStrLst[BUS_STOP_ID_LEN+1];
	char sub_stage_QStrLst[SUB_STAGE_LEN+1]; // Y or N
	char fare_stage_QStrLst;
	char tf_change_point; //Y or N 
}current_selected_trip_stops_struct;


typedef struct packet_format
{
	char start_indicator;
	char data_length[DATA_LENGTH_LEN+1];
	char update_data_flag;
	char *data;
	char checksum[CHECKSUM_LEN+1];
}packet_format_struct;

typedef struct waybill_mas_packet_id
{
	char waybill_mas_pkt_id[PKT_ID_LEN+1];
	waybill_master_struct waybill_master_struct_var;
}waybill_mas_packet_id_struct;

typedef struct sched_mas_packet_id
{
	char sched_mas_pkt_id[PKT_ID_LEN+1];
	schedule_master_struct schedule_master_struct_var;
}sched_mas_packet_id_struct;

typedef struct route_mas_packet_id
{
	char route_mas_pkt_id[PKT_ID_LEN+1];
	route_master_struct route_master_struct_var;
}route_mas_packet_id_struct;

typedef struct fare_chart_packet_id
{
	char fare_chart_pkt_id[PKT_ID_LEN+1];
	fare_chart_struct fare_chart_struct_var;
}fare_chart_packet_id_struct;

typedef struct duty_mas_packet_id
{
	char duty_mas_pkt_id[PKT_ID_LEN+1];
	duty_master_struct duty_master_struct_var;
}duty_mas_packet_id_struct;

typedef struct req_wb_packet_id
{
	char req_for_wb_pkt_id[PKT_ID_LEN+1];
	waybill_master_struct waybill_master_struct_var;
}req_wb_packet_id_struct;


typedef struct req_for_wb
{
	char terminal_id[TERMINAL_ID_LEN+1];
	char  sec_key[SEC_KEY_LEN+1];
}req_for_wb_struct;

typedef struct registration_response
{
	char pkt_id_reg_response[PKT_ID_LEN+1];
	char terminal_id[TERMINAL_ID_LEN+1];
	char machine_no[MACHINE_SN_LEN+1];
	char mac_address[MAC_ADDR_LEN+1];
	char version_no[APP_VER_NO_LEN+1];
	char version_date[APP_VER_DATETIME_LEN+1];
	char waybill_no[WAYBILL_NO_LEN+1];
}registration_response_struct;

typedef struct reg_response_packet_id
{
	char pkt_id_reg_response[PKT_ID_LEN+1];
	registration_response_struct registration_response_struct_var;
}reg_response_packet_id_struct;


//typedef struct bs_sync_req
//{
//	int pkt_id_bs_sync_req;
//	char terminal_id[TERMINAL_ID_LEN+1];
//	char sec_key_bs_sync[SEC_KEY_LEN+1];
//}bs_sync_req_struct;


typedef struct download_response
{
	char terminal_id[TERMINAL_ID_LEN+1];
	char machine_no[MACHINE_SN_LEN+1];
	char mac_address[MAC_ADDR_LEN+1];
	char sec_key_ack[SEC_KEY_LEN+1];
	char waybill_no[WAYBILL_NO_LEN+1];

	char row_cnt[ROW_CNT_LEN+1];
	char start_tkt_cnt[TICKET_NO_LEN+1];
	char end_tkt_cnt[TICKET_NO_LEN+1];
	char version_no[APP_VER_NO_LEN+1];
	char version_date[APP_VER_DATETIME_LEN+1];
}download_response_struct;

typedef struct download_response_packet_id
{
	char pkt_id_download_response[PKT_ID_LEN+1];
	download_response_struct download_response_struct_var;
}download_response_packet_struct;

typedef struct download_transaction_packet
{
	char pkt_id_ack_packet[PKT_ID_LEN+1];
	transaction_log_struct transaction_log_struct_var;
}download_transaction_log_struct;

typedef struct bs_sync_req
{
	int pkt_id_bs_sync_req;
	char terminal_id[TERMINAL_ID_LEN+1];
	char sec_key_bs_sync[SEC_KEY_LEN+1];
}bs_sync_req_struct;

typedef struct pass_select_flag
{
	unsigned short only_pass;
	unsigned short daily_pass;
	unsigned short monthly_pass;
	unsigned short student_pass;
}pass_select_flag_struct;

typedef struct lc_login
{
	char lc_name[NAME_ENG_LEN+1];
	char lc_token_no[COND_ID_LEN+1];
	char lc_passward[MASTER_PW_LEN+1];
}lc_login_struct;


typedef struct apn_name
{
	char service_provider[NAME_ENG_LEN+1];
	char apn_name[NAME_ENG_LEN+1];
	char apn_username[COND_ID_LEN+1];
	char apn_passward[MASTER_PW_LEN+1];
}apn_name_struct;



typedef struct shift_type
{
	char shift_id[SHIFT_ID_LEN+1];
	char shift_code[SHIFT_CODE_LEN+1];
	char shift_type_name_bangla[NAME_KAN_LEN+1];
	char shift_type_name_english[NAME_ENG_LEN+1];
	char updated_date[DATE_LEN+1];
	char updated_time[TIME_LEN+1];

}shift_type_struct;


typedef struct concession_type
{
	char concession_id[CONCESSION_ID_LEN+1];
	char concession_code[CONCESSION_CODE_LEN+1];
	char concession_type_name_bangla[NAME_KAN_LEN+1];
	char concession_type_name_english[NAME_ENG_LEN+1];
	char concession_rate[CONCESSION_RATE_LEN+1];
	char updated_date[DATE_LEN+1];
	char updated_time[TIME_LEN+1];

}concession_type_struct;

/*typedef struct report_struct
{
	char schedule_no[SCHEDULE_NO_LEN+1];

	char startTicketNo[TICKET_NO_LEN+1];
	char endTicketNo[TICKET_NO_LEN+1];
	char start_date[DATE_LEN+1];
	char start_time[TIME_LEN+1];

	char adultcnt[PASSG_COUNT_LEN+1];
	char adultamt[AMOUNT_LEN+1];
	char childcnt[PASSG_COUNT_LEN+1];
	char childamt[AMOUNT_LEN+1];
	char srcitizencnt[PASSG_COUNT_LEN+1];
	char srcitizenamt[AMOUNT_LEN+1];
	char luggcnt[LUGG_UNITS_LEN+1];
	char luggamt[AMOUNT_LEN+1];

	char luggtktcnt;
	char tempTotal[AMOUNT_LEN+1];

}report_struct;*/


typedef struct activity_log
{
	char activity_code[3+1] ;
	char waybill_no[WAYBILL_NO_LEN+1] ;

	char depot_id[DEPOT_ID_LEN+1] ;
	char etim_no[MACHINE_SN_LEN+1] ;
	char conductor_id[COND_ID_LEN+1] ;
	char usage_date[DATE_LEN+1];
	char usage_time[TIME_LEN+1];
}activity_log_struct;


typedef struct activity_print
{
	char activity_code[ACT_CODE_LEN+1] ;
	char activity_name[NAME_ENG_LEN+1];
	char count[4+1];

}activity_print_struct;




#if 0

typedef struct
{
	char csn_no[20];
	QString name_corp_qstr;
	QString depot_code;
	QString card_ID_No_qstr;
	QString Pass_type;
	QString Pass_user;
	// ------------- For Conductor IN-OUT ---------
	QString waybill_no;
	QString etim_no;
	QString valid_from_datetime_qstr;
	QString valid_till_datetime_qstr;
	QString In_time;
	QString temp_route_no;
	QString temp_from_stage_code;
	QString temp_till_stage_code;
	QString temp_trip_id;
	// -------- For Pass -------------
	QString trip_count_qstr;
	QString valid_from_depo;
	QString valid_till_depo;
	QString rfid_deducted_tripcount;
	QString e_purse_amt_qstr;
	QString cur_waybill_no_qstr;
	QString cur_ticket_no_qstr;
	QString cur_transaction_amt_qstr;
	QString cur_trans_DateTime_qstr;
	QString last_waybill_no_qstr;
	QString last_ticket_no_qstr;
	QString updated_card_datetime_qstr;
	QString bus_service_type;
}rfid_smartcard_struct;

#endif

typedef struct
{
	 char csn_no[20+1];
	 char name_corp[CORP_NAME_LEN+1];
	 char depot_code[DEPOT_CODE_LEN+1];
	 char card_ID_No[CARD_ID_LEN+1];
	 char Pass_type[PASS_TYPE_NAME_LEN+1];
	 char Pass_user[NAME_ENG_LEN+1];

	// ------------- For Conductor IN-OUT ---------
	 char waybill_no[WAYBILL_NO_LEN+1];
	 char etim_no[MACHINE_SN_LEN+1];
	 /*char valid_from_datetime[+1];
	 char valid_till_datetime[+1];*/
	 char valid_from_date[DATE_LEN+1];
	 char valid_from_time[TIME_LEN+1];
	 char valid_till_date[DATE_LEN+1];
	 char valid_till_time[TIME_LEN+1];

	 char In_time[TIME_LEN+1];
	 char temp_route_no[ROUTE_NO_LEN+1];
	 char temp_from_stage_code[BUS_STOP_CODE_LEN+1];
	 char temp_till_stage_code[BUS_STOP_CODE_LEN+1];
	 char temp_trip_id[TRIP_ID_LEN+1];

	// -------- For Pass -------------
	 char trip_count[3+1];
	 char valid_from_depot[DEPOT_CODE_LEN+1];
	 char valid_till_depot[DEPOT_CODE_LEN+1];
	 char rfid_deducted_tripcount[1+1];
	 char e_purse_amt[AMOUNT_LEN+1];
	 char cur_waybill_no[WAYBILL_NO_LEN+1];
	 char cur_ticket_no[TICKET_NO_LEN+1];
	 char cur_transaction_amt[AMOUNT_LEN+1];
	 char cur_trans_Date[DATE_LEN+1];
	 char cur_trans_Time[TIME_LEN+1];


	 char last_waybill_no[WAYBILL_NO_LEN+1];
	 char last_ticket_no[TICKET_NO_LEN+1];
	 char updated_card_date[DATE_LEN+1];
	 char updated_card_time[TIME_LEN+1];

	 char bus_service_type[BUS_SRVC_ID_LEN+1];
}rfid_smartcard_struct;









#endif /* CSTC_DEFINES_H_ */
