#include "gprs_drvr.h"
#include "cstclog.h"

extern all_flags_struct all_flags_struct_var;
extern apn_name_struct apn_name_struct_var;

char gprs_chat_file[] = {
	"ABORT          'NO CARRIER'"		"\n"
	"ABORT          'NO DIALTONE'"		"\n"
	"ABORT          'ERROR'"			"\n"
	"ABORT          'NO ANSWER'"		"\n"
	"ABORT          'BUSY'"				"\n"
	"ABORT          'Invalid Login'"	"\n"
	"ABORT          'Login incorrect'"	"\n"
	"TIMEOUT        '60'"				"\n"
	"''             'ATZ'"				"\n"
/*	"'OK'           'AT+CGDCONT=1,\"IP\",\"CMNET\"'"	"\n"*/
	"'OK'           'AT+CGDCONT=1,\"IP\",\"airtelgprs.com\"'"	"\n"
	"'OK'           'ATDT*99***1#'"			"\n"
	"'CONNECT'      ''"					"\n"
};


/********************************************************************************
 *  Function Name:  Set_Gprs_Power_On
 *  Description:
 *                  Function will power ON wnet module.
 *
 * Parameters : - 					   RETURN VALUE    : 		0 : Success
 *                                               		-ve value : Failure
 *********************************************************************************/
int Set_Gprs_Power_On()
{
	return wnet_power_on();
}


/********************************************************************************
 *  Function Name:  Set_Gprs_Power_Off
 *  Description:
 *                  Function will power OFF wnet module.
 *
 * Parameters : - 					   RETURN VALUE    : 		0 : Success
 *                                               		-ve value : Failure
 *********************************************************************************/
int Set_Gprs_Power_Off()
{
	return wnet_power_down();
}

/********************************************************************************
 *  Function Name:  Gprs_Initialise
 *  Description:
 *                  Function will initialize wnet module.
 *
 * Parameters : - 					   RETURN VALUE    : 		0 : Success
 *                                               		-ve value : Failure
 *********************************************************************************/
int Gprs_Initialise()
{

	return wnet_init(GPRS_DEVICE_ADDR);
}

/********************************************************************************
 *  Function Name:  Gprs_ISP
 *  Description:
 *                  Function will set network in attached mode.
 *
 * Parameters : - 					   RETURN VALUE    : 		0 : Success
 *                                               		-ve value : Failure
 *********************************************************************************/
int Gprs_ISP()
{
	struct timeval start,cur, diff, end;
	struct timezone tz;
	int retval = 0;
	gettimeofday(&start, &tz);
	diff.tv_sec = 20;//60;
	diff.tv_usec = 0;
	timeradd(&start, &diff, &end);
	while (1){
		retval = wnet_set_attached(1);
		if (0 != retval){
			gettimeofday(&cur, &tz);

			if (timercmp(&cur, &end, < ))
				usleep(1000);//10000);
			else {
				//				lcd_printf(ALG_LEFT, "wnet_set_attached() failed %d", retval);
				//				lcd_flip();
				//				kb_getkey();
				break;
			}
		}else
			break;
	}

	return retval;
}


/********************************************************************************
 *  Function Name:  Gprs_Open_PPP_Connection
 *  Description:
 *                  Function will set APN for ppp connection.
 *
 * Parameters : - 					   RETURN VALUE    : 		0 : Success
 *                                               		-ve value : Failure
 *********************************************************************************/
int Gprs_Open_PPP_Connection()
{
	int retval = 0;
	char *gprs_chat = NULL;

	CstcLog_printf("THREAD:: Gprs_Set_PPP_Connection...");

	if(strlen(apn_name_struct_var.apn_name) != 0)
	{
		gprs_chat = (char *)malloc(500*(sizeof(char)));
		memset(gprs_chat,0,500);

		CstcLog_printf_1("apn_name  %s",apn_name_struct_var.apn_name);

		sprintf(gprs_chat,
				"ABORT          'NO CARRIER'"		"\n"
				"ABORT          'NO DIALTONE'"		"\n"
				"ABORT          'ERROR'"			"\n"
				"ABORT          'NO ANSWER'"		"\n"
				"ABORT          'BUSY'"				"\n"
				"ABORT          'Invalid Login'"	"\n"
				"ABORT          'Login incorrect'"	"\n"
				"TIMEOUT        '60'"				"\n"
				"''             'ATZ'"				"\n"
				"'OK'           'AT+CGDCONT=1,\"IP\",\"%s\"'"	"\n"
				"'OK'           'ATDT*99***1#'"			"\n"
				"'CONNECT'      ''"					"\n" ,
				apn_name_struct_var.apn_name
		);

		CstcLog_printf_1("gprs_chat :::::::::::::::::%s %d",gprs_chat , strlen(gprs_chat));

		//	retval = ppp_open(GPRS_PPP_DEVICE_ADDR, gprs_chat_file, "wap", "wap", PPP_ALG_PAP, 10);//30);
		//	retval = ppp_open(GPRS_PPP_DEVICE_ADDR, gprs_chat_file, "", "", PPP_ALG_PAP, 30);
		retval = ppp_open(GPRS_PPP_DEVICE_ADDR, gprs_chat, apn_name_struct_var.apn_username, apn_name_struct_var.apn_passward, PPP_ALG_PAP, 10);//30);

		if(gprs_chat != NULL)
		{
			free(gprs_chat);
			gprs_chat = NULL;
		}
	}
	return retval;
}

/********************************************************************************
 *  Function Name:  Gprs_Check_PPP_Connection
 *  Description:
 *                  Function will link with GPRS device.
 *
 * Parameters : - 					   RETURN VALUE    : 		0 : Success
 *                                               		-ve value : Failure
 *********************************************************************************/
int Gprs_Check_PPP_Connection()
{
	struct timeval start,cur, diff, end;
	struct timezone tz;
	int retval = 0;
	gettimeofday(&start, &tz);
	diff.tv_sec = 20;//60;
	diff.tv_usec = 0;

	CstcLog_printf("THREAD:: Gprs_Check_PPP_Connection.....");


	timeradd(&start, &diff, &end);
	while (1){
		retval = ppp_check(GPRS_PPP_DEVICE_ADDR);
		if (0 == retval){
			break;
		}
		else{
			gettimeofday(&cur, &tz);
			if (timercmp(&cur, &end, < ))
				usleep(1000);//10000);
			else {
				break;
			}
		}
	}
	return retval;
}


/********************************************************************************
 *  Function Name:  Gprs_Close_PPP_Connection
 *  Description:
 *                  Function will close ppp link with GPRS device.
 *
 * Parameters : - 					   RETURN VALUE    : 		0 : Success
 *                                               		-ve value : Failure
 *********************************************************************************/
int Gprs_Close_PPP_Connection()
{
	CstcLog_printf("THREAD:: Gprs_Close_PPP_Connection....");
//	all_flags_struct_var.ppp_on_flag = 0;
	return ppp_close(GPRS_PPP_DEVICE_ADDR);
}

