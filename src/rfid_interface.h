/*
 * rfid_interface.h
 *
 *  Created on: 28-Jan-2016
 *      Author: root
 */
#ifndef RFID_INTERFACE_H_
#define RFID_INTERFACE_H_

int nfc_Init(void);
int conductor_authentication(void);
int read_write_rfid(int fd, uint8_t cardno_block_no, uint8_t keytype,const void *key,char *uid_data,char *data,int read_flag);
int read_epurse_card_details(void);
int get_read_rfid_data_to_struct(uint8_t block_no,char* read_data);
int get_read_rfid_data_to_struct_epurse(uint8_t block_no,char* read_data);
int update_epurse_card(void);
int verify_data(void);
void RemoveSpaces(char* source);
int conductor_entry(void);
int desfire_func(void);

void PubBcd2Asc(char *psIHex, int iHexLen, char *psOAsc);
void ContactlessSmartCard_Demo(void);
int read_desfire_card_details(void);
int update_epurse_desfire_card(void);
int desfire_close(void);
int exchange_apdu_func(int fd,char* apdu_cmd,int apdu_cmd_len,int is_sam_flag);
unsigned long int hex_to_dec(char sTemp1[]);
int sam_authentication(void);

#endif /* RFID_INTERFACE_H_ */
