#include "printer_drvr.h"
extern IDirectFBSurface         *main_surface;

DFBSurfaceDescription	surfdesc;
IDirectFBSurface        *surface = NULL;
IDirectFBFont		    *font_16 = NULL;
IDirectFBFont		    *font_20 = NULL;
IDirectFBFont		    *font_24 = NULL;
IDirectFBFont		    *font_26 = NULL;
IDirectFBFont		    *font_28 = NULL;
IDirectFBFont		    *font_30 = NULL;
IDirectFBFont		    *font_32 = NULL;
IDirectFBFont		    *font_35 = NULL;
IDirectFBFont		    *font_38 = NULL;
IDirectFBFont		    *font_40 = NULL;
IDirectFBFont		    *font_45 = NULL;
IDirectFBFont		    *font_60 = NULL;
IDirectFBFont		    *font_50 = NULL;
DFBFontDescription	    fdesc;

printer_param_t  param;
printer_status_t status;
static int ifd = -1;

void Printer_Init(void)
{
	int retval = 0;

	memset(&fdesc,0, sizeof(fdesc));
	fdesc.flags  = DFDESC_HEIGHT;
	fdesc.height = 16;
	dfb->CreateFont(dfb, font_file_bold, &fdesc, &font_16);

	if (NULL == font_16){
		lcd_printf(ALG_LEFT, 16, "create font16 failed.");
		lcd_flip();
		retval = -1;
	}

	/*******for bold*************/
	if (0 == retval){
		fdesc.height = 20;
//		dfb->CreateFont(dfb, font_file_01, &fdesc, &font_20);
		dfb->CreateFont(dfb, font_file_bold, &fdesc, &font_20);
		if (NULL == font_20){
			lcd_printf(ALG_LEFT, 16, "create font20 failed");
			lcd_flip();
			retval = -1;
		}
	}
	/**************************/


	if (0 == retval){
		fdesc.height = 24;
		dfb->CreateFont(dfb, font_file_bold, &fdesc, &font_24);
		if (NULL == font_24){
			lcd_printf(ALG_LEFT, 16, "create font24 failed");
			lcd_flip();
			retval = -1;
		}
	}

	if (0 == retval){
		fdesc.height = 26;
		dfb->CreateFont(dfb, font_file_bold, &fdesc, &font_26);
		if (NULL == font_26){
			lcd_printf(ALG_LEFT, 16, "create font26 failed");
			lcd_flip();
			retval = -1;
		}
	}

	if (0 == retval){
		fdesc.height = 28;
		dfb->CreateFont(dfb, font_file_bold, &fdesc, &font_28);
		if (NULL == font_28){
			lcd_printf(ALG_LEFT, 16, "create font28 failed");
			lcd_flip();
			retval = -1;
		}
	}

	if (0 == retval){
		fdesc.height = 30;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_30);
		if (NULL == font_30){
			lcd_printf(ALG_LEFT, 16, "create font30 failed");
			lcd_flip();
			retval = -1;
		}
	}

	if (0 == retval){
		fdesc.height = 32;
		dfb->CreateFont(dfb, font_file_bold, &fdesc, &font_32);
		if (NULL == font_32){
			lcd_printf(ALG_LEFT, 16, "create font32 failed");
			lcd_flip();
			retval = -1;

		}
	}

	if (0 == retval){
		fdesc.height = 35;
		dfb->CreateFont(dfb, font_file_bold, &fdesc, &font_35);
		if (NULL == font_35){
			lcd_printf(ALG_LEFT, 16, "create font35 failed");
			lcd_flip();
			retval = -1;
		}
	}

	/*******for bold*************/
	if (0 == retval){
		fdesc.height = 38;
		dfb->CreateFont(dfb, font_file_bold, &fdesc, &font_38);
		if (NULL == font_38){
			lcd_printf(ALG_LEFT, 16, "create font35 failed");
			lcd_flip();
			retval = -1;
		}
	}
	/**************************/


	if (0 == retval){
		fdesc.height = 40;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_40);
		if (NULL == font_40){
			lcd_printf(ALG_LEFT, 16, "create font40 failed");
			lcd_flip();
			retval = -1;
		}
	}

	if (0 == retval){
		fdesc.height = 45;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_45);
		if (NULL == font_45){
			lcd_printf(ALG_LEFT, 16, "create font16 failed");
			lcd_flip();
			retval = -1;
		}
	}

	if (0 == retval){
		fdesc.height = 50;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_50);
		if (NULL == font_50){
			lcd_printf(ALG_LEFT, 16, "create font16 failed");
			lcd_flip();
			retval = -1;

		}
	}
	if (0 == retval){
		fdesc.height = 60;
		dfb->CreateFont(dfb, font_file_02, &fdesc, &font_60);
		if (NULL == font_60){
			lcd_printf(ALG_LEFT, 16, "create font16 failed");
			lcd_flip();
			retval = -1;

		}

	}
}


//void Printer_Start(void)
//{
//	int retval=0;
//	if (0 == retval){
//		ifd = printer_open(printer_device_name, O_WRONLY | O_NONBLOCK);
//		if (ifd < 0){
//			lcd_printf(ALG_LEFT, 16, "Device Open failed.");
//			lcd_flip();
//		}
//	}
//
//	if (0 == retval){
//
//		printer_get_param(ifd, &param);
//
//		surfdesc.flags = DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT;
//		surfdesc.caps  = DSCAPS_NONE;
//		surfdesc.width = param.ro_width;
//		surfdesc.height= 1000;
//		dfb->CreateSurface(dfb, &surfdesc, &surface);
//		surface->Clear(surface,
//				0xFF,
//				0xFF,
//				0xFF,
//				0xFF);
//
//		surface->SetColor(surface, 0x00, 0x00, 0x00, 0xFF);
//	}
//}

void Printer_String_Alignment(char* dataStr, int bytes, int x, int *y,int txtAlign)
{
	//CstcLog_printf("x= %d y= %d txtAlign= %d ",x,*y,txtAlign);

	switch(txtAlign)
	{
	case 0:
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_LEFT);
		//CstcLog_printf("case 0");
		break;
	case 1:
		x = param.ro_width/2;
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_CENTER);
		//CstcLog_printf("case 1");
		break;
	case 2:
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_RIGHT);
		//CstcLog_printf("case 2");
		break;
	case 3:
		x = param.ro_width/2;
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_LEFT);
		//CstcLog_printf("case 3");
		break;
	case 4:
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_TOP);
		break;
	case 8:
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_BOTTOM);
		break;
	case 10:
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_OUTLINE);
		break;
	case 12://DSTF_TOPCENTER:
		x = param.ro_width/2;
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_TOPCENTER);
		//CstcLog_printf("case 12");
		break;
	case 14://DSTF_BOTTOMCENTER:
		//CstcLog_printf("case 14");
		x = param.ro_width/2;
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_BOTTOMCENTER);

		break;
	default:
		surface->DrawString(surface, dataStr, bytes, x, *y, DSTF_LEFT);
		break;
	}
}

void Printer_String_Font16(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_16);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_16->GetHeight(font_16, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font20(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_20);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_20->GetHeight(font_20, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font24(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_24);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_24->GetHeight(font_24, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font26(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_26);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_26->GetHeight(font_26, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font28(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_28);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_28->GetHeight(font_28, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font30(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_30);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_30->GetHeight(font_30, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font32(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_32);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_32->GetHeight(font_32, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font35(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;

	//horizontal alignment
	//	x = param.ro_width/2;
	//	y = 20;

	surface->SetFont(surface, font_35);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_35->GetHeight(font_35, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font38(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_38);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_32->GetHeight(font_38, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font40(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_40);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_40->GetHeight(font_40, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font45(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_45);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_45->GetHeight(font_45, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}


void Printer_String_Font50(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_50);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_50->GetHeight(font_50, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

void Printer_String_Font60(char* dataStr, int bytes, int x, int *y,int lineFeed,int txtAlign)
{
	int height;
	surface->SetFont(surface, font_60);
	//	surface->DrawString(surface, dataStr, bytes, x, *y, txtAlign);
	Printer_String_Alignment(dataStr, bytes, x, y,txtAlign);
	font_60->GetHeight(font_60, &height);
	//	*y+= height*lineFeed + 1;
	if(lineFeed!=0)
		*y+= height+lineFeed;
}

int Printer_Start_printing(int y,int pagefeed)
{
#if DEBUG_LOG_ENABLE
	lcd_printf(ALG_LEFT, 16, "Printing....");
	lcd_flip();
//	Kb_Get_Key_Sym();
#endif

	print_surface(ifd, surface, (y + pagefeed));
#if DEBUG_LOG_ENABLE
//	Printer_Get_Status();
	lcd_printf(ALG_LEFT, 16, "Finished.");
	lcd_flip();
#endif

	return 1;
}

//int Printer_Get_Status(void)
//{
//	do
//	{
//		usleep(100000);
//		printer_get_status(ifd, &status);
//		//	lcd_clean();
//		if (((status.status  >> PRINTER_STATUS_BUSY) & 0x01) == 0x01)
//			//lcd_printf(ALG_LEFT, 16, "printer status = busy");
//			CstcLog_printf("printer status = busy");
//		else if (((status.status  >> PRINTER_STATUS_HIGHT_TEMP) & 0x01) == 0x01)
//			//lcd_printf(ALG_LEFT, 16, "printer status = temprature high");
//			CstcLog_printf("printer printer status = temprature high");
//		else if (((status.status  >> PRINTER_STATUS_PAPER_LACK) & 0x01) == 0x01)
//			//lcd_printf(ALG_LEFT, 16, "printer status = no paper");
//			CstcLog_printf("printer status = no paper");
//		else if (((status.status  >> PRINTER_STATUS_FEED) & 0x01) == 0x01)
//			//lcd_printf(ALG_LEFT, 16, "printer status = feed paper");
//			CstcLog_printf("printer status = feed paper");
//		else if (((status.status  >> PRINTER_STATUS_PRINT) & 0x01) == 0x01)
//			//lcd_printf(ALG_LEFT, 16, "printer status = printing");
//			CstcLog_printf("printer status = printing");
//		else if (((status.status  >> PRINTER_STATUS_FORCE_FEED) & 0x01) == 0x01)
//			//lcd_printf(ALG_LEFT, 16, "printer status = force feed paper");
//			CstcLog_printf("printer status = force feed paper");
//		else if (((status.status  >> PRINTER_STATUS_POWER_ON) & 0x01) == 0x01)
//			//lcd_printf(ALG_LEFT, 16, "printer status = power on");
//			CstcLog_printf("printer status = power on");
//		//lcd_flip();
//
//	}while (status.status != 0);
//	return 1;
//}

int Printer_Get_Status(void)
{
//	do
//	{
		CstcLog_printf("in Printer_Get_Status.......");
		usleep(100000);
		printer_get_status(ifd, &status);

//			lcd_clean();
//		if (((status.status  >> PRINTER_STATUS_BUSY) & 0x01) == 0x01)
//		{
//			lcd_printf(ALG_LEFT, 16, "printer status = busy");
//			CstcLog_printf("printer status = busy");
//			return 0;
//		}
//		else if (((status.status  >> PRINTER_STATUS_HIGHT_TEMP) & 0x01) == 0x01)
//		{
//			lcd_printf(ALG_LEFT, 16, "printer status = temprature high");
//			CstcLog_printf("printer printer status = temprature high");
//			return 0;
//		}
//		else if (((status.status  >> PRINTER_STATUS_PAPER_LACK) & 0x01) == 0x01)
//		{
//			lcd_printf(ALG_LEFT, 16, "printer status = no paper");
//			CstcLog_printf("printer status = no paper");
//			return 0;
//		}
//		else if (((status.status  >> PRINTER_STATUS_FEED) & 0x01) == 0x01)
//		{
//			lcd_printf(ALG_LEFT, 16, "printer status = feed paper");
//			CstcLog_printf("printer status = feed paper");
//			return 0;
//		}
//		else if (((status.status  >> PRINTER_STATUS_PRINT) & 0x01) == 0x01)
//		{
//			lcd_printf(ALG_LEFT, 16, "printer status = printing");
//			CstcLog_printf("printer status = printing");
//			return 0;
//		}
//		else if (((status.status  >> PRINTER_STATUS_FORCE_FEED) & 0x01) == 0x01)
//		{
//			lcd_printf(ALG_LEFT, 16, "printer status = force feed paper");
//			CstcLog_printf("printer status = force feed paper");
//			return 0;
//		}
//		else if (((status.status  >> PRINTER_STATUS_POWER_ON) & 0x01) == 0x01)
//		{
//			lcd_printf(ALG_LEFT, 16, "printer status = power on");
//			CstcLog_printf("printer status = power on");
//		lcd_flip();
//			return 0;
//		}
//		else
//			return 1;

//	}while (status.status != 0);
		CstcLog_printf("status %lu", status.status);

	return status.status;
}


void Printer_Deinit(void)
{
	if (NULL != surface)
		surface->Release(surface);

	if (NULL != font_16)
		font_16->Release(font_16);

	if (NULL != font_20)
		font_20->Release(font_20);

	if (NULL != font_24)
		font_24->Release(font_24);

	if (NULL != font_26)
		font_26->Release(font_26);

	if (NULL != font_28)
		font_28->Release(font_28);

	if (NULL != font_32)
		font_32->Release(font_32);

	if (NULL != font_35)
		font_35->Release(font_35);

	if (NULL != font_45)
		font_45->Release(font_45);

	if (NULL != font_50)
		font_50->Release(font_50);

	if (NULL != font_60)
		font_60->Release(font_60);

}

void Printer_Stop(void)
{
	surface_deinit();
	if (ifd >= 0){
		printer_close(ifd);
		ifd = -1;
	}
}



int Printer_Start(void)
{
	int retval=0;
	if (0 == retval){
		/*
		 * Swap:-
		 * O_WRONLY | O_NONBLOCK -->> for non blocking if the internal print memory is full and operation fails
		 * O_WRONLY -->> for blocking and continue operation after resuming
		 */
//		ifd = printer_open(printer_device_name, O_WRONLY | O_NONBLOCK);
		ifd = printer_open(printer_device_name, O_WRONLY );
		if (ifd < 0){
			lcd_printf(ALG_LEFT, 16, "Device Open failed.");
			lcd_flip();
			return 1;
		}
//		surface_init(retval,5000);
		surface_init(0,6000);
	}
	return 0;
}

void surface_init(int retval,int surf_height)
{

	if (0 == retval){
			printer_get_param(ifd, &param);

			surfdesc.flags = DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT;
			surfdesc.caps  = DSCAPS_NONE;
			surfdesc.width = param.ro_width;
//			surfdesc.height= 5000;
			surfdesc.height= surf_height;
			dfb->CreateSurface(dfb, &surfdesc, &surface);
			surface->Clear(surface,
					0xFF,
					0xFF,
					0xFF,
					0xFF);
			surface->SetColor(surface, 0x00, 0x00, 0x00, 0xFF);
		}
}

void surface_deinit(void)
{
	if (NULL != surface)
		surface->Release(surface);
}

