/*
 * lan_interface.c
 *
 *  Created on: 09-Apr-2014
 *      Author: root
 */


#include <openssl/rsa.h>
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "lan_interface.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "cstclog.h"
#include "cstc_defines.h"
#include "tcp_drvr.h"
#include "db_interface.h"
#include "cstc_error.h"


#define	SIGPIPE	13
#define SIG_IGN	((__sighandler_t) 1)
extern void *memset (void *__s, int __c, size_t __n) __THROW __nonnull ((1));
extern int close (int __fd);
extern int usleep (__useconds_t __useconds);

#define MAX_CONNECTIONS 1
#define MAC_ID_VERIFY_DISABLED 0
//#define SERVER_PORT_LAN 9000
//#define SERVER_ADDR "10.20.41.199"

extern packet_format_struct packet_format_struct_var;
extern waybill_mas_packet_id_struct waybill_mas_packet_id_struct_var;
extern sched_mas_packet_id_struct sched_mas_packet_id_struct_var;
extern route_mas_packet_id_struct route_mas_packet_id_struct_var;
extern fare_chart_packet_id_struct fare_chart_packet_id_struct_var;
extern duty_mas_packet_id_struct duty_mas_packet_id_struct_var;
extern req_for_wb_struct req_for_wb_struct_var;
extern req_wb_packet_id_struct req_wb_packet_id_struct_var;
extern waybill_master_struct waybill_master_struct_var;
//extern schedule_master_struct schedule_master_struct_var;  //SWAP_REMOVE_THIS_CODE
extern duty_master_struct duty_master_struct_var;
//extern route_master_struct route_master_struct_var;
//fare_chart_struct fare_chart_struct_var;
extern registration_response_struct registration_response_struct_var;
extern reg_response_packet_id_struct reg_response_packet_id_struct_var;
extern bs_sync_req_struct bs_sync_req_struct_var;
extern ack_packet_struct ack_packet_struct_var;
extern master_status_struct master_status_struct_var;
extern download_response_struct download_response_struct_var;
extern download_response_packet_struct download_response_packet_struct_var;
extern transaction_log_struct transaction_log_struct_var;
extern download_transaction_log_struct download_transaction_log_struct_var;
duty_master_struct *lan_pduty_master_struct_var;

extern pthread_mutex_t upload_semaphore_waybill ;


int lan_MultipleScheduleFlag=0;

int lan_g_checksumFlag=0;

char lan_global_buff[2048],lan_g_checksumbuffer[50];;
int lan_global_iTemp=0;
static int lan_global_temp ;
static int lan_gUpdateDataFlag=0;
int lan_New_pkt_recv_flag = 0;
int lan_Total_packet_length = 0;
char lan_Err_Msg[ERROR_MSG_BUFFER_LEN];//,Err_Msg1[ERROR_MSG_BUFFER_LEN];
int lan_Err_Len=0;
int lan_RecordCntToRecv =0,lan_TotalRecordCntRecvd =0,lan_Global_rowCount=0,lan_GDownload_pktCnt=0;
int lan_rt_count=0;
int lan_checkSumFlag=0;

int lan_prev_end_tkt_count =0;

extern void memset_packet_format(void);
extern void remove_stars(const char *input, char *result);

char lan_global_schedule[SCHEDULE_NO_LEN];
char lan_global_Rate_Id[SCHEDULE_NO_LEN];
char lan_global_Route_Id[ROUTE_ID_LEN];
char lan_global_waybill[WAYBILL_NO_LEN];

int socket_server_demo(void){
	int iserver_socket = -1;
	int iclient_socket = -1;
	unsigned char bTemp[8000];
	unsigned int  iTemp = 0;
	int j = 0;
	char client_addr[255];
	unsigned int client_port = 0;
	struct ifreq;
	int error = 0;
	int count = 0;

	iserver_socket = tcp_server(SERVER_PORT_LAN, 1);
	CstcLog_printf("\n***************** SOCKET SERVER **************************\n\n");
	if (iserver_socket >=0)
	{
		printf("\nWait connection on %d...", SERVER_PORT_LAN);
		while (1)
		{
			//            printf("\n First while loop");
			iclient_socket = tcp_accept(iserver_socket, client_addr, &client_port);
			CstcLog_printf("iclient_socket = %d",iclient_socket);
			if (iclient_socket < 0)
				continue;
			else
			{
				while (1)
				{
					CstcLog_printf("Ready to read data from client..");
					iTemp = sizeof(bTemp);
					CstcLog_printf("Before data read iTemp = %d",iTemp);
					error =  tcp_read(iclient_socket, bTemp, &iTemp);
					CstcLog_printf("error=%d",error);

					if (error == 0)
					{
						CstcLog_printf("After data read iTemp = %d",iTemp);
						if (iTemp > 0)
						{
							CstcLog_printf("\nRead bytes are : ");
							for(j = 0; j < iTemp ; j++)
								printf("%2x, ", bTemp[j]);
							CstcLog_printf("\nRead %d bytes", iTemp);

							if((compose_response_packet((char*)bTemp, iclient_socket,iTemp,&j)))
								break;
						}
					}
				}
				count = 0;
				tcp_close(iclient_socket);
				iclient_socket = -1;
			}
			CstcLog_printf("Wait connection on %d...", SERVER_PORT_LAN);
			//lcd_flip();
		}
		tcp_close(iserver_socket);
		iserver_socket = -1;
		return 1;

	}
	else
	{
		return 0;
	}
}

int Manual_Duty_Assignment_server(int DownloadEnableFlag)
{
	int iserver_socket = -1;
	int iclient_socket = -1;
	//	unsigned char bTemp[1024];
	unsigned int  iTemp = 0;
	int i, retval=0;
	int active_net = 0;
	char client_addr[255];
	unsigned int client_port = 0;
	char iflist[16][IFNAMSIZ];
	struct ifreq ifr;

#if DEBUG_LOG_ENABLE
	lcd_clean();
	lcd_printf_ex(ALG_CENTER,16,20, "socket server");
	CstcLog_printf("socket server");
#endif
	iTemp = netif_get_list(iflist, sizeof(iflist)/IFNAMSIZ);
	for (i = 0; i < iTemp; i++){
		memset(&ifr, 0, sizeof(ifr));
		netif_get_flags(iflist[i], &ifr);
		if (ifr.ifr_flags & IFF_RUNNING){
			netif_get_ipaddr(iflist[i], &ifr);
#if DEBUG_LOG_ENABLE
			lcd_printf_ex(ALG_CENTER,16,40, "Address:%s:%d", inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr), SERVER_PORT_LAN);
			CstcLog_printf( "Address:%s:%d", inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr), SERVER_PORT_LAN);
#endif
			active_net++;
		}
	}

	if (active_net == 0){
#if DEBUG_LOG_ENABLE
		lcd_printf_ex(ALG_CENTER,16,60, "NO Active net work");
		lcd_flip();
		CstcLog_printf("NO Active net work");
		//		Kb_Get_Key_Code();
#endif
		return 0;
	}

	//	lcd_flip();
	iserver_socket = tcp_server(SERVER_PORT_LAN, 1);
	CstcLog_printf("iserver_socket=%d",iserver_socket);
	if (iserver_socket >=0){
		CstcLog_printf("iserver_socket created successfully......iserver_socket= %d",iserver_socket);

#if DEBUG_LOG_ENABLE
		//		lcd_printf_ex(ALG_LEFT,16,80, "Wait connection on %d...", SERVER_PORT_LAN);
		//		lcd_flip();
		CstcLog_printf("Wait connection on %d...", SERVER_PORT_LAN);
#endif
		while (1){
			if(check_master_status()==1)
				break;
			CstcLog_printf("iclient_socket empty 2...");
			//			if (Kb_Hit()){
			//			if (0x1B == Kb_Get_Key_Sym_new())
			//				break;
			//			}

			CstcLog_printf("iclient_socket empty 2... %s---%d",client_addr,client_port);
			iclient_socket = tcp_accept(iserver_socket, client_addr, &client_port);
			CstcLog_printf("iclient_socket %d - %s - %d",iclient_socket,client_addr,client_port);
			if (iclient_socket < 0)
				continue;
			else
				//if (iclient_socket >= 0)
			{
#if DEBUG_LOG_ENABLE
				//				lcd_printf_ex(ALG_LEFT,16,100, "Connected from %s:%d...", client_addr, client_port);
				//				lcd_flip();
				CstcLog_printf("Connected from %s:%d...", client_addr, client_port);
#endif
				if(Do_duty_data_exchange(iclient_socket))
					retval = 1;
				else
					retval = 0;
				//				break;
			}
#if DEBUG_LOG_ENABLE
			//			lcd_printf_ex(ALG_LEFT,16,120, "Wait connection on 1 %d...", SERVER_PORT_LAN);
			//			lcd_flip();
			CstcLog_printf("Wait connection on 1 %d...", SERVER_PORT_LAN);
#endif
		}
		tcp_close(iserver_socket);
		iserver_socket = -1;
	}else {
		CstcLog_printf("iserver_socket creation failed......iserver_socket = %d",iserver_socket);
#if DEBUG_LOG_ENABLE
		//		lcd_flip();
		//		Kb_Get_Key_Code();
#endif
		retval= 0;
	}
	return retval;
}

int Do_duty_data_exchange_old(int iclient_socket)
{
	unsigned char bTemp[1024];
	unsigned int  iTemp = 0;
	int error;
	while (1){
		if (Kb_Hit()){
			if (0x1B == Kb_Get_Key_Sym())
				break;
		}
		iTemp = sizeof(bTemp);
		error =  tcp_read(iclient_socket, bTemp, &iTemp);

		if (error == 0){
			lcd_flip();
			if (iTemp > 0)
			{
#if DEBUG_LOG_ENABLE
				lcd_printf_ex(ALG_LEFT,16,20, "Readed %d bytes", iTemp);
				lcd_flip();
#endif
				error = tcp_write(iclient_socket, bTemp, iTemp);
				if (error != 0){
#if DEBUG_LOG_ENABLE
					lcd_printf_ex(ALG_LEFT,16,20, "connection was broken");
					lcd_flip();
#endif
					break;
				}
			}
			else {
				usleep(100000);
			}
		}else {
#if DEBUG_LOG_ENABLE
			lcd_printf_ex(ALG_LEFT,16,20, "connection was broken");
			lcd_flip();
#endif
			break;
		}
	}
	tcp_close(iclient_socket);
	iclient_socket = -1;
	return 1;
}

int Do_duty_data_exchange(int iclient_socket)
{
	unsigned char bTemp[1024];
	unsigned int  iTemp = 0;
	int error, retval=0;
	int j;
	int count = 0;

	while (1){
		if(check_master_status()==1)
			break;
		//        if (Kb_Hit()){
		//		if (0x1B == Kb_Get_Key_Sym_new())
		//			break;
		//        }
		//        if (Kb_Hit()){
		//                    if (0x1B == Kb_Get_Key_Sym_new())
		//                        break;
		//        }
		iTemp = sizeof(bTemp);
		error =  tcp_read(iclient_socket, bTemp, &iTemp);
		CstcLog_printf("error=%d bTemp = %s iTemp=%d",error,bTemp,iTemp);
		if (error == 0)
		{
			//            lcd_flip();
			CstcLog_printf("iTemp=%d",iTemp);

			if (iTemp > 0)
			{
				CstcLog_printf("Read bytes are : ");
				CstcLog_HexDump(bTemp,iTemp);
				//				for(j = 0; j < iTemp ; j++)
				//					CstcLog_printf("%2x, ", bTemp[j]);
				CstcLog_printf("Read %d bytes", iTemp);

				if((compose_response_packet((char*)bTemp, iclient_socket,iTemp,&j)))
				{
					retval =0;
					break;
				}
			}
		}

		else {
			CstcLog_printf( "connection was broken");
			//            lcd_printf_ex(ALG_LEFT,16,20, "connection was broken");
			//            lcd_flip();
			break;
		}
	}

	//}
	count = 0;
	tcp_close(iclient_socket);
	iclient_socket = -1;
	return retval;
}
/****************************************************************************************************
                                  COMPOSE DIFFERENT PACKETS - FUNCTIONS
 *****************************************************************************************************/

void final_packet_lan(char * pkt_format_ptr, char * pkt_ptr, int *length)
{
	int temp = 0;
	packet_format_struct_var.start_indicator = ';';
	temp = *length + START_INDICATOR_LEN+DATA_LENGTH_LEN+CHECKSUM_LEN; // (length - 45);
	//	temp = htonl(temp);
	sprintf((packet_format_struct_var.data_length), "%08d", temp);

	// packet_format_struct_var.data_length = length ; //(length - 45);
	packet_format_struct_var.data = pkt_ptr;

	//	sprintf(packet_format_struct_var.checksum, "%s","0000000000000000000000000000000000000000");
	//strcpy(packet_format_struct_var.checksum, "0000000000000000000000000000000000000000");
	memset(packet_format_struct_var.checksum,'0', CHECKSUM_LEN);

	memcpy(pkt_format_ptr, &(packet_format_struct_var.start_indicator), START_INDICATOR_LEN);
	memcpy((pkt_format_ptr + START_INDICATOR_LEN) , (packet_format_struct_var.data_length), DATA_LENGTH_LEN);
	memcpy((pkt_format_ptr + START_INDICATOR_LEN+DATA_LENGTH_LEN), packet_format_struct_var.data, *length);
	memcpy((pkt_format_ptr + START_INDICATOR_LEN+DATA_LENGTH_LEN + *length), packet_format_struct_var.checksum,CHECKSUM_LEN);
	*length += START_INDICATOR_LEN+DATA_LENGTH_LEN+CHECKSUM_LEN;
}

void compose_registration_response_packet(char * pkt_ptr,/*char * etim_no,*/int *length)
{
	char *sn = NULL;
	char *etim_no = NULL;
	char *mac_ID = NULL;
	FILE* tempFile= NULL;
	int nbytes = 10;
	int lan_Err_Len=0;
	int temp = REGISTRATION_RESPONSE_PKT_ID;
	char lan_Err_Msg[ERROR_MSG_BUFFER_LEN];//,Err_Msg1[ERROR_MSG_BUFFER_LEN];

	char Err_MsgAry[3]={'\0'};

	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);
	get_machine_info(sn, nbytes);

	etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
	memset(etim_no,0,(TERMINAL_ID_LEN+1));
	tempFile= fopen(ETIM_NO_FILE_ADDR,"r+");
	if(tempFile == NULL)
	{
		CstcLog_printf("ERROR OPENING FILE etim_no.txt");
		//strcat(lan_Err_Msg,"ERROR OPENING FILE etim_no.txt");
		//		return 0;
		sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
		strcat(lan_Err_Msg,Err_MsgAry);
		lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
		CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
		memset(Err_MsgAry,0,3);
	}
	else
	{
		fscanf(tempFile,"%s",etim_no);
		//fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile);
		fflush(tempFile);
		CstcLog_printf("etim_no :- %s", etim_no);
		fclose(tempFile);
	}

	/*	mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
	memset(mac_ID,0,(MAC_ADDR_LEN+1));
	tempFile= fopen(MAC_FILE_ADDR,"r+");
	if(tempFile == NULL)
	{
		CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
	//	strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
		//return 0;
		sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_mac_ID);
		strcat(lan_Err_Msg,Err_MsgAry);
		lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
		CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
		memset(Err_MsgAry,0,3);
	}
	else
	{
		fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
		fflush(tempFile);
		CstcLog_printf("mac_ID :- %s", mac_ID);
		fclose(tempFile);
	}*/

	sprintf(registration_response_struct_var.pkt_id_reg_response, "%d", temp);
	memcpy(registration_response_struct_var.machine_no, sn, nbytes); //serial no
	//	if(strlen(mac_ID)==0)
	//		memset(registration_response_struct_var.mac_address,'0', MAC_ADDR_LEN);
	//	else
	strcpy(registration_response_struct_var.mac_address,"0000000000000000");//"00257E019279");
	if(strlen(etim_no)==0)
		memset(registration_response_struct_var.terminal_id,'0', TERMINAL_ID_LEN);
	else
		strcpy(registration_response_struct_var.terminal_id, etim_no);//etim no
	strcpy(registration_response_struct_var.version_no,APP_VERSION);
	strcpy(registration_response_struct_var.version_date,APP_VER_DATETIME);
	if(strlen(waybill_master_struct_var.waybill_no)>0)
		sprintf(registration_response_struct_var.waybill_no,"%14s",waybill_master_struct_var.waybill_no);
	else
		memset(registration_response_struct_var.waybill_no,'0',WAYBILL_NO_LEN);
	CstcLog_printf("Reg Res waybill_no = %s",registration_response_struct_var.waybill_no);

	memcpy(pkt_ptr,registration_response_struct_var.pkt_id_reg_response, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),registration_response_struct_var.machine_no, MACHINE_SN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN),registration_response_struct_var.mac_address, MAC_ADDR_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), registration_response_struct_var.terminal_id, TERMINAL_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN),registration_response_struct_var.version_no, APP_VER_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+APP_VER_NO_LEN),registration_response_struct_var.version_date, APP_VER_DATETIME_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+APP_VER_NO_LEN+APP_VER_DATETIME_LEN),registration_response_struct_var.waybill_no, WAYBILL_NO_LEN);
	//	memcpy((pkt_ptr + 2), registration_response_struct_var.terminal_id, TERMINAL_ID_LEN);
	CstcLog_printf("pkt_ptr = %s",pkt_ptr);

	*length += PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+APP_VER_NO_LEN+APP_VER_DATETIME_LEN+WAYBILL_NO_LEN;
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	if(etim_no!=NULL)
	{
		free(etim_no);
		etim_no=NULL;
	}
	if(mac_ID!=NULL)
	{
		free(mac_ID);
		mac_ID=NULL;
	}
}

//----------------------------------------ACKNOWLEDGEMENT PACKET---------------------------------------

//void compose_ack_packet(char * pkt_ptr)
void compose_ack_packet(char *pkt_ptr,int *length)
{/*
	int temp = ACK_PKT_ID;
	CstcLog_printf("yes start here..");
	//    temp = htons(temp);
	memcpy(ack_packet_struct_var.pkt_id_ack_packet, &temp, PKT_ID_LEN);
	strcpy(ack_packet_struct_var.terminal_id, "SNS00001");
	strcpy(ack_packet_struct_var.machine_no, "82036617" );
	strcpy(ack_packet_struct_var.mac_address, "00257E0200D0");
	strcpy(ack_packet_struct_var.sec_key_ack, "SecAck123");
	strcpy(ack_packet_struct_var.err_length, "2");
	strcpy(ack_packet_struct_var.err_ack, "OK");

//	sprintf(ack_packet_struct_var.pkt_id_ack_packet, "%d", temp);
	memcpy(pkt_ptr, ack_packet_struct_var.pkt_id_ack_packet, PKT_ID_LEN);
	memcpy((pkt_ptr + 2),ack_packet_struct_var.terminal_id, TERMINAL_ID_LEN);
	memcpy((pkt_ptr + 10), ack_packet_struct_var.machine_no, MACHINE_SN_LEN);
	memcpy((pkt_ptr + 18), ack_packet_struct_var.mac_address, MAC_ADDR_LEN);
	memcpy((pkt_ptr + 35), ack_packet_struct_var.sec_key_ack, SEC_KEY_LEN);
	memcpy((pkt_ptr + 51), ack_packet_struct_var.err_length, ERR_LENGTH_LEN);
	memcpy((pkt_ptr + 53), ack_packet_struct_var.err_ack, lan_Err_Len); //"OK"
	CstcLog_printf("yes end here..");

 */

	char *sn = NULL;
	char *etim_no = NULL;
	char *mac_ID = NULL;
	FILE* tempFile= NULL;
	int nbytes = 10;
	int temp = ACK_PKT_ID;

	char Err_MsgAry[3]={'\0'};

	sn =(char *)malloc(nbytes*sizeof(char));
	memset(sn,0,nbytes);
	get_machine_info(sn, nbytes);

	etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
	memset(etim_no,0,(TERMINAL_ID_LEN+1));
	tempFile= fopen(ETIM_NO_FILE_ADDR,"r+");
	if(tempFile == NULL)
	{
		CstcLog_printf("ERROR OPENING FILE etim_no.txt");
		sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
		strcat(lan_Err_Msg,Err_MsgAry);
		lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
		memset(Err_MsgAry,0,3);
	}
	else
	{
		fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile);
		fflush(tempFile);
		CstcLog_printf("etim_no :- %s", etim_no);
		fclose(tempFile);
	}

	sprintf(ack_packet_struct_var.pkt_id_ack_packet, "%d", temp);

	memcpy(ack_packet_struct_var.machine_no, sn, nbytes);

	strcpy(ack_packet_struct_var.mac_address,"000000000000");

	if(strlen(etim_no)==0)
		memset(ack_packet_struct_var.terminal_id,'0', TERMINAL_ID_LEN);
	else
		strcpy(ack_packet_struct_var.terminal_id, etim_no);

	ack_packet_struct_var.err_ack=(char *)malloc((lan_Err_Len+1)*sizeof(int));
	memset(ack_packet_struct_var.err_ack,0,(lan_Err_Len+1));

	if(lan_Err_Len==0)
	{
		sprintf(lan_Err_Msg,"%02d",SUCCESS);
		lan_Err_Len=strlen(lan_Err_Msg);
		CstcLog_printf("lan_Err_Len =%d ,lan_Err_Msg = %s",lan_Err_Len,lan_Err_Msg);
	}
	else
	{
		CstcLog_printf("lan_Err_Len =%d ,lan_Err_Msg = %s",lan_Err_Len,lan_Err_Msg);
	}

	CstcLog_HexDump(lan_Err_Msg,lan_Err_Len);

	memcpy(ack_packet_struct_var.err_ack,lan_Err_Msg,lan_Err_Len);
	ack_packet_struct_var.err_ack[lan_Err_Len]='\0';

	CstcLog_printf("lan_Err_Len =%d",lan_Err_Len);

	sprintf(ack_packet_struct_var.err_length,"%02d",lan_Err_Len);

	memcpy(pkt_ptr, ack_packet_struct_var.pkt_id_ack_packet, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),ack_packet_struct_var.terminal_id, TERMINAL_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN), ack_packet_struct_var.machine_no, MACHINE_SN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN), ack_packet_struct_var.mac_address, MAC_ADDR_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), ack_packet_struct_var.err_length,ERR_LENGTH_LEN );
	memcpy((pkt_ptr + PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+ERR_LENGTH_LEN), ack_packet_struct_var.err_ack,lan_Err_Len );

	CstcLog_printf("ack pkt_ptr %s ",pkt_ptr);
	*length += PKT_ID_LEN+TERMINAL_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+ERR_LENGTH_LEN+lan_Err_Len;
	if(sn!=NULL)
	{
		free(sn);
		sn=NULL;
	}
	if(etim_no!=NULL)
	{
		free(etim_no);
		etim_no=NULL;
	}
	if(mac_ID!=NULL)
	{
		free(mac_ID);
		mac_ID=NULL;
	}
	if(ack_packet_struct_var.err_ack!=NULL)
	{
		free(ack_packet_struct_var.err_ack);
		ack_packet_struct_var.err_ack=NULL;
	}

}

//**************************************************************************************************

int compose_response_packet(char * bTemp, int iclient_socket ,int iTemp,int *j)
{
	char tempCh = bTemp[0];
	char recvPktID[(PKT_ID_LEN+1)]={'\0'};
	char recvPktLen[(DATA_LENGTH_LEN+1)] ={'\0'};
	char recvSchedule[(SCHEDULE_NO_LEN+1)]={'\0'};
	char result[(SCHEDULE_NO_LEN+1)]={'\0'};
	char dbTemp[5120]={'\0'};
	char tempName[100]={'\0'};

	FILE* dbDataFile= NULL;
	int ack_flag=0,i=0;
	int length = 0;
	char * pkt_ptr = NULL;
	char * pkt_format_ptr  = NULL;

	char Err_MsgAry[3]={'\0'};
	char temp_cmd[50];
	char recvCount[5]={'\0'};

	//	char *sub_string = "union all select";
	//	char *sub_string1 = "union";
	//int sub_string_len = strlen(sub_string);

	//	char *bTemp_string = NULL;
	//	char *bTemp_string1 = NULL;

	if(strstr(bTemp,"|") != NULL)
	{
		for(i=0;i<iTemp;i++)
		{
			if(bTemp[i] != '|')
			{
				dbTemp[i] = bTemp[i];
			}
			else if(bTemp[i] == '|')
				break;
		}
		dbTemp[i] = '\0';
		memset(lan_g_checksumbuffer,0,sizeof(lan_g_checksumbuffer));
		memcpy(lan_g_checksumbuffer,(bTemp+i+1),(iTemp-i-1));
	}
	else
	{
		memset(dbTemp,0,5120);
		memcpy(dbTemp,bTemp,iTemp);
		dbTemp[iTemp]='\0';
	}
	//	bTemp_string = dbTemp;

	CstcLog_printf("dbTemp = %s \n lan_g_checksumbuffer = %s",dbTemp,lan_g_checksumbuffer);

	CstcLog_printf("tempCh =%c",tempCh);
	if(tempCh== ';')
	{
		memcpy(recvPktLen,bTemp+START_INDICATOR_LEN,DATA_LENGTH_LEN);

		CstcLog_printf("recvPktLen =%s bytes", recvPktLen);

		lan_Total_packet_length = atoi(recvPktLen);
		CstcLog_printf("lan_Total_packet_length %d ", lan_Total_packet_length);

		memcpy(recvPktID,dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),PKT_ID_LEN);
		CstcLog_printf("recvPktID =%s bTemp[%d]=%s bytes", recvPktID,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN), bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN));

		memset(temp_cmd,0,50);
		memcpy(temp_cmd,dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN),1);
		lan_gUpdateDataFlag=atoi(temp_cmd);
		CstcLog_printf("updateDataFlag =%d bTemp[%d]=%c bytes", lan_gUpdateDataFlag,(START_INDICATOR_LEN+DATA_LENGTH_LEN), bTemp[(START_INDICATOR_LEN+DATA_LENGTH_LEN)]);
		lan_global_temp=atoi(recvPktID);//temp;
		CstcLog_printf("lan_global_temp %d ", lan_global_temp);
		memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
		lan_Err_Len=0;

		//if(lan_global_temp == SCHEDULE_MASTER_PKT_ID)
		if((lan_global_temp == SCHEDULE_MASTER_PKT_ID) || (lan_global_temp == ROUTE_MASTER_PKT_ID) || (lan_global_temp == FARE_CHART_PKT_ID) || (lan_global_temp == ROUTE_HEADER_PKT_ID) )
		{
			CstcLog_printf("if lan_global_temp == SCHEDULE_MASTER_PKT_ID");
			memcpy(recvSchedule,dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN),SCHEDULE_NO_LEN);
			CstcLog_printf("recvSchedule =%s bTemp[%d]=%s bytes", recvSchedule,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN));
			remove_stars(recvSchedule,result);
			CstcLog_printf("after remove_stars result %s ", result);
			strcpy(lan_global_schedule,result);
			lan_global_schedule[strlen(result)]='\0';
			CstcLog_printf("lan_global_schedule = %s ", lan_global_schedule);
		}
		if(lan_global_temp == 9)
		{
			CstcLog_printf("if lan_global_temp == 9");
			memcpy(recvCount,(dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)),3);
			lan_rt_count=atoi(recvCount);
			CstcLog_printf("recvCount =%s bTemp[%d]=%s bytes lan_rt_count=%d", recvCount,(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN), bTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),lan_rt_count);
		}

		if(iTemp != lan_Total_packet_length)
		{
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memcpy(lan_global_buff,dbTemp,iTemp);
			lan_global_buff[iTemp]='\0';
			CstcLog_printf("lan_global_buff %s", lan_global_buff);
			dbDataFile = NULL;
			dbDataFile= fopen(DB_INPUT_FILE_ADDR,"w");
			if(dbDataFile == NULL)
			{
				CstcLog_printf("ERROR_OPENING_FILE_db_input");
				sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_db_input);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				ack_flag=1;
			}
			else
			{
				CstcLog_printf("Tempfile open success :%d",dbDataFile);

				if((lan_global_temp != 54) || (lan_global_temp != 56) || (lan_global_temp !=45) || (lan_global_temp != 47) || (lan_global_temp !=50)|| (lan_global_temp !=02) ||(lan_global_temp !=06) )
				{
					switch(lan_global_temp)
					{
					CstcLog_printf("before insert table atoi(recvPktID) =%s ",lan_global_temp);

					case 1 :/*-------------------WAYBILL----------------------*/
						memcpy(tempName, "waybill_master",14);
						break;

					case 3: /*-----------------SCHEDULE-----------------------*/
						memcpy(tempName, "schedule_master", 15);
						break;

					case 5:/*--------------------DUTY------------------------*/
						memcpy(tempName, "duty_master",11);
						break;

					case 7: /*-------------------ROUTE------------------------*/
						memcpy(tempName, "route_master",12);
						break;

					case 8:/*--------------------FARE RECORD LENGTH------------------------*/
						lan_RecordCntToRecv = atoi((dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));
						CstcLog_printf("lan_RecordCntToRecv %d = int(tempStr) %d ",lan_RecordCntToRecv, atoi((dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN))));
						lan_TotalRecordCntRecvd=0;
						lan_rt_count=0;
						CstcLog_printf(" In case 8 lan_RecordCntToRecv is %d ",lan_RecordCntToRecv);
						break;

					case 9:/*--------------------FARE------------------------*/
						memcpy(tempName, "fare_chart",10);
						CstcLog_printf("lan_RecordCntToRecv = %d ",lan_RecordCntToRecv);
						CstcLog_printf("Incr lan_RecordCntToRecv = %d ",lan_RecordCntToRecv);
						break;

					case 10:/*-----------------Ticket Type-------------------------*/
						memcpy(tempName, "ticket_type",11);
						break;

					case 11:/*---------------------Ticket sub Type------------------------*/
						memcpy(tempName, "ticket_sub_type",15);
						break;

					case 12:/*--------------------Bus Services------------------------------*/
						memcpy(tempName, "bus_service",11);
						break;

					case 13:/*----------------------Bus Brand-------------------------------*/
						memcpy(tempName, "bus_brand",9);
						break;

					case 14:/*----------------------Depot-------------------------------*/
						memcpy(tempName, "depot",5);
						break;

					case 15:/*-----------------Route Header------------------------*/
						memcpy(tempName, "route_header",12);
						break;

					case 16:/*--------------------ETM RW key---------------------------*/
						memcpy(tempName, "etim_rw_keys",13);
						break;

					case 17:/*--------------------TRUNK AND FEEDER---------------------------*/
						memcpy(tempName, "trunk_feeder_master",19);
						break;

					case 18:/*-------------------SERVICE OP-----------------------*/
						memcpy(tempName, "service_op",10);
						break;

					default:
						break;
					}
					if(lan_global_temp != 8)
					{
						CstcLog_printf("Its not packet id 8.............");
						fprintf(dbDataFile,"%s","insert into ");
						CstcLog_printf("insert into");
						fprintf(dbDataFile,"%s",tempName);
						CstcLog_printf("%s",tempName);
						fprintf(dbDataFile,"%s"," select ");
						CstcLog_printf("select");
					}
				}
				//if(lan_global_temp == SCHEDULE_MASTER_PKT_ID)
				if((lan_global_temp == SCHEDULE_MASTER_PKT_ID) || (lan_global_temp == 7) || (lan_global_temp == 9) || (lan_global_temp == 15) )
				{
					CstcLog_printf("Inside if(lan_global_temp == SCHEDULE_MASTER_PKT_ID)");
					fprintf(dbDataFile,"%s",(dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)));
				}
				else
				{
					fprintf(dbDataFile,"%s",(dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));
				}
				fflush(dbDataFile);
				fclose(dbDataFile);
			}
			dbDataFile = NULL;
			memset(dbTemp,0,sizeof(dbTemp));
			lan_global_iTemp=iTemp;
			CstcLog_printf("lan_global_iTemp(%d) in iTemp(%d) != lan_Total_packet_length condition  %d", lan_global_iTemp,iTemp,lan_Total_packet_length);
			lan_New_pkt_recv_flag=1;
			return 0;
		}
		else
		{
			dbDataFile = NULL;
			dbDataFile= fopen(DB_INPUT_FILE_ADDR,"w");
			if(dbDataFile == NULL)
			{
				CstcLog_printf("ERROR_OPENING_FILE_db_input");
				sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_db_input);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s Err_MsgAry= %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				ack_flag=1;
			}
			else
			{
				CstcLog_printf("Tempfile open success :%d",dbDataFile);
				if((lan_global_temp != 54) || (lan_global_temp != 56) || (lan_global_temp !=45) || (lan_global_temp != 47) || (lan_global_temp !=50) || (lan_global_temp !=02) ||(lan_global_temp !=06) )
				{
					switch(lan_global_temp)
					{
					CstcLog_printf("before insert table atoi(recvPktID)=%s ",lan_global_temp);

					case 1 :/*-------------------WAYBILL----------------------*/
						memcpy(tempName, "waybill_master",14);
						break;

					case 3: /*-----------------SCHEDULE-----------------------*/
						memcpy(tempName, "schedule_master", 15);
						break;

					case 5:/*--------------------DUTY------------------------*/
						memcpy(tempName, "duty_master",11);
						break;

					case 7: /*-------------------ROUTE------------------------*/
						memcpy(tempName, "route_master",12);
						break;

					case 8:/*--------------------FARE RECORD LENGTH------------------------*/
						lan_RecordCntToRecv = atoi((dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));
						CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",lan_RecordCntToRecv, atoi((dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN))));
						lan_RecordCntToRecv=0;
						lan_rt_count=0;
						CstcLog_printf(" In case 8 lan_RecordCntToRecv is %d ",lan_RecordCntToRecv);
						break;

					case 9:/*--------------------FARE------------------------*/
						memcpy(tempName, "fare_chart",10); /*--search for union all select in dbTemp add that count in lan_RecordCntToRecv--*/
						CstcLog_printf("lan_RecordCntToRecv = %d ",lan_RecordCntToRecv);
						CstcLog_printf("Incr lan_RecordCntToRecv = %d ",lan_RecordCntToRecv);
						break;

					case 10:/*-----------------Ticket Type-------------------------*/
						memcpy(tempName, "ticket_type",11);
						break;

					case 11:/*---------------------Ticket sub Type------------------------*/
						memcpy(tempName, "ticket_sub_type",15);
						break;

					case 12:/*--------------------Bus Services------------------------------*/
						memcpy(tempName, "bus_service",11);
						break;

					case 13:/*----------------------Bus Brand-------------------------------*/
						memcpy(tempName, "bus_brand",9);
						break;

					case 14:/*----------------------Depot-------------------------------*/
						memcpy(tempName, "depot",5);
						break;

					case 15:/*-----------------Route Header------------------------*/
						memcpy(tempName, "route_header",12);
						break;

					case 16:/*--------------------ETM RW key---------------------------*/
						memcpy(tempName, "etim_rw_keys",13);
						break;

					case 17:/*--------------------TRUNK AND FEEDER---------------------------*/
						memcpy(tempName, "trunk_feeder_master",19);
						break;

					case 18:/*-------------------SERVICE OP-----------------------*/
						memcpy(tempName, "service_op",10);
						break;

					default:
						break;
					}
					if(lan_global_temp != 8)
					{
						fprintf(dbDataFile,"%s","insert into ");
						CstcLog_printf("insert into");
						fprintf(dbDataFile,"%s",tempName);
						CstcLog_printf("%s",tempName);
						fprintf(dbDataFile,"%s"," select ");
						CstcLog_printf("select");
					}
				}
				if((lan_global_temp == SCHEDULE_MASTER_PKT_ID) || (lan_global_temp == 7) || (lan_global_temp == 9) || (lan_global_temp == 15) )
				{
					CstcLog_printf("Inside if(lan_global_temp == SCHEDULE_MASTER_PKT_ID)");
					fprintf(dbDataFile,"%s;",(dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN+SCHEDULE_NO_LEN)));
				}
				else
				{
					fprintf(dbDataFile,"%s;",(dbTemp+(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+PKT_ID_LEN)));
				}
				fflush(dbDataFile);
				fclose(dbDataFile);
			}
			dbDataFile = NULL;
			lan_global_iTemp=iTemp;
			CstcLog_printf("lan_global_iTemp in else condition %d", lan_global_iTemp);
			if(received_packet_lan(lan_global_temp, tempCh, iclient_socket,dbTemp, lan_gUpdateDataFlag,j) == 1)

				memset(temp_cmd,0,50);
			strcpy(temp_cmd,"rm -f ");
			strcat(temp_cmd,DB_INPUT_FILE_ADDR);
			CstcLog_printf("temp_cmd :%s",temp_cmd);
			system(temp_cmd);
			memset(temp_cmd,0,50);

			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(bTemp,0,sizeof(bTemp));
			memset(lan_g_checksumbuffer,0,sizeof(lan_g_checksumbuffer));
			memset(dbTemp,0,sizeof(dbTemp));
			lan_gUpdateDataFlag=0;
			//			sleep(1);
			return 1;
			//}
		}
	}

	else
	{
		if(lan_New_pkt_recv_flag)
		{
			CstcLog_printf("lan_global_iTemp + iTemp  %d", iTemp+lan_global_iTemp);
			CstcLog_printf("In New_flag lan_RecordCntToRecv = %d ",lan_RecordCntToRecv);
			//			if(lan_global_temp == 9)
			//			{
			//				CstcLog_printf("In New_flag sub_string_len = %d ",sub_string_len);
			//
			//				CstcLog_printf("Incr in New_flag lan_RecordCntToRecv = %d ",lan_RecordCntToRecv);
			//			}
			dbDataFile = NULL;
			dbDataFile= fopen(DB_INPUT_FILE_ADDR,"a+");
			if(dbDataFile == NULL)
			{
				CstcLog_printf("ERROR OPENING FILE etim_no.txt");
				sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_db_input);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len = lan_Err_Len + strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				ack_flag=1;
				memset(temp_cmd,0,50);
				strcpy(temp_cmd,"rm -f ");
				strcat(temp_cmd,DB_INPUT_FILE_ADDR);
				CstcLog_printf("temp_cmd :%s",temp_cmd);
				system(temp_cmd);
				memset(temp_cmd,0,50);
				memset(lan_global_buff,0,sizeof(lan_global_buff));
				memset(bTemp,0,sizeof(bTemp));
				memset(lan_g_checksumbuffer,0,sizeof(lan_g_checksumbuffer));
				memset(dbTemp,0,sizeof(dbTemp));
				lan_gUpdateDataFlag=0;
			}
			else
			{
				CstcLog_printf("Tempfile open success :%d",dbDataFile);
				fprintf(dbDataFile,"%s",dbTemp);
			}
			CstcLog_printf("Inside (iTemp(%d)+lan_global_iTemp(%d)) == lan_Total_packet_length(%d)",iTemp,lan_global_iTemp,lan_Total_packet_length);
			lan_global_iTemp=iTemp+lan_global_iTemp;
			CstcLog_printf("Inside lan_global_iTemp(%d) == lan_Total_packet_length(%d)",lan_global_iTemp,lan_Total_packet_length);

			if(lan_global_iTemp == lan_Total_packet_length)
			{
				CstcLog_printf("Inside iTemp(%d),lan_global_iTemp(%d) == lan_Total_packet_length(%d)",iTemp,lan_global_iTemp,lan_Total_packet_length);
				fprintf(dbDataFile,"%s",(char*)";");
				fflush(dbDataFile);
				fclose(dbDataFile);
				dbDataFile = NULL;

				if(received_packet_lan(lan_global_temp, lan_global_buff[0],iclient_socket,lan_global_buff, lan_gUpdateDataFlag,j) == 1)

					memset(temp_cmd,0,50);
				strcpy(temp_cmd,"rm -f ");
				strcat(temp_cmd,DB_INPUT_FILE_ADDR);
				CstcLog_printf("temp_cmd :%s",temp_cmd);
				system(temp_cmd);
				memset(temp_cmd,0,50);

				lan_global_temp=0;
				lan_New_pkt_recv_flag=0;
				memset(lan_global_buff,0,sizeof(lan_global_buff));
				memset(bTemp,0,sizeof(bTemp));
				memset(lan_g_checksumbuffer,0,sizeof(lan_g_checksumbuffer));
				memset(dbTemp,0,sizeof(dbTemp));
				lan_gUpdateDataFlag=0;
				sleep(1);
				return 1;

			}
			else if(lan_global_iTemp > lan_Total_packet_length)
			{
				Show_Error_Msg("Received Packet size is larger than total packet length in the packet");

				sprintf(Err_MsgAry,"%02d",GREATER_PACKET_LENGTH);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				ack_flag=1;
				memset(temp_cmd,0,50);
				strcpy(temp_cmd,"rm -f ");
				strcat(temp_cmd,DB_INPUT_FILE_ADDR);
				CstcLog_printf("temp_cmd :%s",temp_cmd);
				system(temp_cmd);
				memset(temp_cmd,0,50);
				memset(lan_global_buff,0,sizeof(lan_global_buff));
				memset(bTemp,0,sizeof(bTemp));
				memset(lan_g_checksumbuffer,0,sizeof(lan_g_checksumbuffer));
				memset(dbTemp,0,sizeof(dbTemp));
				lan_gUpdateDataFlag=0;
			}
			else
			{
				fflush(dbDataFile);

				fclose(dbDataFile);
				dbDataFile = NULL;
				return 0;
			}
			memset(bTemp,0,sizeof(bTemp));
			memset(dbTemp,0,sizeof(dbTemp));
		}
		else
		{
			Show_Error_Msg("Resend Packets");

			sprintf(Err_MsgAry,"%02d",RESEND_PACKETS);
			strcat(lan_Err_Msg,Err_MsgAry);
			lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
			CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s ",lan_Err_Len,lan_Err_Msg);
			memset(Err_MsgAry,0,3);

			ack_flag=1;
			memset(temp_cmd,0,50);
			strcpy(temp_cmd,"rm -f ");
			strcat(temp_cmd,DB_INPUT_FILE_ADDR);
			CstcLog_printf("temp_cmd :%s",temp_cmd);
			system(temp_cmd);
			memset(temp_cmd,0,50);
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(bTemp,0,sizeof(bTemp));
			memset(lan_g_checksumbuffer,0,sizeof(lan_g_checksumbuffer));
			memset(dbTemp,0,sizeof(dbTemp));
			lan_gUpdateDataFlag=0;
		}
	}
	if(ack_flag==1)
	{

		CstcLog_printf(">>------ACK1--------<< %d",(sizeof(ack_packet_struct)+lan_Err_Len+1));
		pkt_ptr = (char*)malloc((sizeof(ack_packet_struct)+lan_Err_Len+1));
		memset(pkt_ptr,0,(sizeof(ack_packet_struct)+lan_Err_Len+1));

		CstcLog_printf(">>------ACK2--------<<");

		compose_ack_packet(pkt_ptr,&length);
		CstcLog_printf("out here..");


		pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length+1));
		memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length+1));

		CstcLog_printf("black bird");

		CstcLog_printf("%d>>------ACK3--------<<%d",length,(sizeof(packet_format_struct)+length+1));
		final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

		CstcLog_printf("Sent bytes are : ");
		CstcLog_HexDump(pkt_format_ptr,length);

		CstcLog_printf("Sent %d bytes",length);
		CstcLog_printf(">>-----------------<<");

		if(write(iclient_socket, pkt_format_ptr, length) < 0)
		{
			CstcLog_printf("ACK Connection was broken!!!");
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			lan_Err_Len=0;
			return 0;
		}
		else
		{
			CstcLog_printf("ACK Connection was Not broken!!!");
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			CstcLog_printf("1. ACK Connection was Not broken!!!");
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			CstcLog_printf("2. ACK Connection was Not broken!!!");
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);

			lan_Err_Len=0;
			CstcLog_printf("3. ACK Connection was Not broken!!!");
			return 1;
		}
	}
	memset(lan_global_buff,0,sizeof(lan_global_buff));
	return 0;
}


//int received_packet_lan(int indx, char tempCh, int iclient_socket,char * bTemp)
int received_packet_lan(int i, char tempCh, int iclient_socket,char * bTemp,int updateDataFlag,int *j)
{
	int length = 0;
	int error=0;
	int ack_flag=0;
	//	char *mac_ID = NULL;
	//		char *etim_no=NULL;
	//		FILE* tempFile= NULL;
	//int temp_terminal_id,terminal_id;
	int retval = 0;
	char * pkt_ptr = NULL;
	char * pkt_format_ptr  = NULL;
	char temp_dsply[50] = {'\0'};

	char Err_MsgAry[3]={'\0'};

	if(tempCh == ';')
	{
		CstcLog_printf("lan_global_temp/indx %d ", lan_global_temp);
		switch(lan_global_temp)
		{
		case 1:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			//			Show_Error_Msg("Inserting Waybill Master");
			if(DbInterface_Get_Row_Count("waybill_master",strlen("waybill_master"),0)>0)
			{
				ack_flag=1;
				CstcLog_printf("WAYBILL MASTER parsed ");
				//				strcat(lan_Err_Msg,"WAYBILL MASTER Already Present");
				//				lan_Err_Len = lan_Err_Len + strlen("WAYBILL MASTER  Already Present");
				sprintf(lan_Err_Msg,"%02d",WAYBILL_PRESENT);
				lan_Err_Len=lan_Err_Len+strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);

				//check_master_status();
				//				Show_Error_Msg("Waybill Master Already Present");
				break;
			}
			if(master_status_struct_var.insert_waybill==0)
				if(DbInterface_Update_Master_Status_Data("update master_status set insert_waybill ='1'"))
				{
					master_status_struct_var.insert_waybill=1;
					check_master_status();
				}
			CstcLog_printf("WAYBILL MASTER RECEIVED ");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("WAYBILL MASTER parsed ");
				//				strcat(lan_Err_Msg,"WAYBILL MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("WAYBILL MASTER Success");

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				//				memset(Err_Msg1,0,sizeof(Err_Msg1));
				lan_Err_Len=0;

				//check_master_status();
				//				Show_Error_Msg("Insert Waybill Master Complete");
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("WAYBILL MASTER parsing failed");
				//				strcat(lan_Err_Msg,"WAYBILL MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("WAYBILL MASTER failed");

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",WAYBILL_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg=%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
				//				Show_Error_Msg("Insert Waybill Master Failed");
			}
			//check_master_status();
			break;

		case 3 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("SCHEDULE MASTER RECEIVED ");
			Show_Error_Msg("UPLOADING \nSCHEDULE MASTER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("SCHEDULE MASTER parsed ");
				Show_Error_Msg("UPLOADING \nSCHEDULE MASTER SUCCESS");
				//				strcat(lan_Err_Msg,"SCHEDULE MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("SCHEDULE MASTER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				//				memset(Err_Msg1,0,sizeof(Err_Msg1));
				lan_Err_Len=0;
				//check_master_status();
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("SCHEDULE MASTER parsing failed");
				Show_Error_Msg("UPLOADING \nSCHEDULE MASTER FAILED");
				//				strcat(lan_Err_Msg,"SCHEDULE MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("SCHEDULE MASTER failed");
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",SCHEDULE_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg=%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;
		case 5 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("DUTY MASTER RECEIVED ");
			Show_Error_Msg("UPLOADING \nDUTY MASTER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("DUTY MASTER parsed ");
				Show_Error_Msg("UPLOADING \nDUTY MASTER SUCCESS");
				//				strcat(lan_Err_Msg,"DUTY MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("DUTY MASTER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;
				if(master_status_struct_var.insert_waybill==1)
				{
					if(DbInterface_Update_Master_Status_Data("update master_status set insert_waybill ='2'"))
					{
						master_status_struct_var.insert_waybill=2;
						*j=0;
					}
				}
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("DUTY MASTER parsing failed");

				Show_Error_Msg("UPLOADING \nDUTY MASTER FAILED");
				//				strcat(lan_Err_Msg,"DUTY MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("DUTY MASTER failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",DUTY_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg=%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;

		case 6 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			if(master_status_struct_var.start_duty==0)
				if(DbInterface_Update_Master_Status_Data("update master_status set start_duty ='1'"))
					master_status_struct_var.start_duty=1;
			check_master_status();
			CstcLog_printf("START DUTY RECEIVED ");
			retval =packet_parser_nresponder(bTemp);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("START DUTY parsed ");

				//				strcat(lan_Err_Msg,"START DUTY Success");
				//				lan_Err_Len = lan_Err_Len + strlen("START DUTY Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;

				if(DbInterface_Update_Master_Status_Data("update master_status set start_duty ='2'"))
				{
					master_status_struct_var.start_duty=2;
					*j=0;
					//					master_status_struct_var.startDutyFlag=1;
				}
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("START DUTY parsing failed");

				//				strcat(lan_Err_Msg,"START DUTY failed");
				//				lan_Err_Len = lan_Err_Len + strlen("START DUTY failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",START_DUTY_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg=%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;
		case 7 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			if(master_status_struct_var.upload_master==0)
				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='1'"))
					master_status_struct_var.upload_master=1;
			check_master_status();
			CstcLog_printf("ROUTE MASTER RECEIVED ");
			Show_Error_Msg("UPLOADING \nROUTE MASTER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("ROUTE MASTER parsed ");
				Show_Error_Msg("UPLOADING \nROUTE MASTER SUCCESS");

				//				strcat(lan_Err_Msg,"ROUTE MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("ROUTE MASTER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;

			}
			else
			{
				ack_flag=1;
				CstcLog_printf("ROUTE MASTER parsing failed");
				Show_Error_Msg("UPLOADING \nROUTE MASTER FAILED");
				//				strcat(lan_Err_Msg,"ROUTE MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("ROUTE MASTER failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,ERROR_MSG_BUFFER_LEN);
				sprintf(Err_MsgAry,"%02d",ROUTE_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg=%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;

		case 8 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("FARE CHART RECORD COUNT Recvd updateDataFlag = %d",updateDataFlag);
			if(updateDataFlag==0) //if(updateDataFlag==1)
			{
				if(master_status_struct_var.ticket_fare == 0)
				{
					if(DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='1'"))
						master_status_struct_var.ticket_fare=1;
					CstcLog_printf("1. FARE CHART RECORD COUNT updateDataFlag = %d",updateDataFlag);
					check_master_status();
				}
			}
			if(updateDataFlag==2)
			{
				if(master_status_struct_var.ticket_fare ==1)
				{
					if(DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'"))
					{
						master_status_struct_var.ticket_fare=2;
						*j=0;
					}
				}
				CstcLog_printf("2. FARE CHART RECORD COUNT updateDataFlag = %d",updateDataFlag);
			}
			CstcLog_printf("FARE CHART RECORD COUNT RECEIVED ");

			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("FARE CHART RECORD COUNT parsed ");

				//				strcat(lan_Err_Msg,"FARE CHART Success");
				//				lan_Err_Len = lan_Err_Len + strlen("FARE CHART RECORD COUNT Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				//				memset(Err_Msg1,0,sizeof(Err_Msg1));
				lan_Err_Len=0;


				//				DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'");
				//				master_status_struct_var.ticket_fare=2;
				//					check_master_status();
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("FARE CHART RECORD COUNT parsing failed");

				//				strcat(lan_Err_Msg,"FARE CHART RECORD COUNT failed");
				//				lan_Err_Len = lan_Err_Len + strlen("FARE CHART RECORD COUNT failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",FARE_CHART_COUNT_FAILED);
				lan_Err_Msg[strlen(lan_Err_Msg)]='\0';
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg=%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			break;
		case 9 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			//			if(DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='1'"))
			//				master_status_struct_var.ticket_fare=1;
			//			check_master_status();
			CstcLog_printf("FARE CHART RECEIVED ");

			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("FARE CHART parsed ");
				CstcLog_printf("FARE CHART parsed lan_TotalRecordCntRecvd =%d and lan_RecordCntToRecv =%d",lan_TotalRecordCntRecvd,lan_RecordCntToRecv);

				if(lan_TotalRecordCntRecvd>=lan_RecordCntToRecv)
				{
					//					strcat(lan_Err_Msg,"FARE CHART Complete Success");
					//					lan_Err_Len = lan_Err_Len + strlen("FARE CHART Complete Success");
					//					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
					CstcLog_printf("FARE CHART parsed 1.swap");

					sprintf(lan_Err_Msg,"%02d",SUCCESS);
					lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
					lan_Err_Msg[lan_Err_Len]='\0';
					memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//					memset(Err_Msg1,0,sizeof(Err_Msg1));
					lan_Err_Len=0;

					//					if(DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='2'"))
					//						master_status_struct_var.ticket_fare=2;
					//					check_master_status();
				}
				else
				{
					//					strcat(lan_Err_Msg,"FARE CHART Partial Success");
					//					lan_Err_Len = lan_Err_Len + strlen("FARE CHART Partial Success");
					//					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
					CstcLog_printf("FARE CHART parsed 2.swap");
					CstcLog_printf("lan_Err_Len = %d ",lan_Err_Len);
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",FARE_CHART_PARTIAL_SUCCESS);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s ",lan_Err_Len,lan_Err_Msg);
					CstcLog_printf("Err_MsgAry_Len = %d Err_MsgAry =%s ",strlen(Err_MsgAry),Err_MsgAry);
					lan_Err_Msg[strlen(lan_Err_Msg)]='\0';
					strcat(lan_Err_Msg,Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s ",lan_Err_Len,lan_Err_Msg);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s ",lan_Err_Len,lan_Err_Msg);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					beep(2800,1500);
				}

			}
			else
			{
				ack_flag=1;

				//				strcat(lan_Err_Msg,"FARE CHART failed");
				//				lan_Err_Len = lan_Err_Len + strlen("FARE CHART failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",FARE_CHART_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}

			break;
		case 10 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("TICKET TYPE MASTER RECEIVED ");
			Show_Error_Msg("UPLOADING \nTICKET TYPE MASTER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("TICKET TYPE MASTER parsed ");
				Show_Error_Msg("UPLOADING \nTICKET TYPE MASTER SUCCESS");
				//				strcat(lan_Err_Msg,"TICKET TYPE MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("TICKET TYPE MASTER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("TICKET TYPE MASTER parsing failed");
				Show_Error_Msg("UPLOADING \nTICKET TYPE MASTER FAILED");
				//				strcat(lan_Err_Msg,"TICKET TYPE MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("TICKET TYPE MASTER failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",TICKET_TYPE_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}

			//check_master_status();
			break;
		case 11:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("TICKET SUB TYPE MASTER RECEIVED ");
			Show_Error_Msg("UPLOADING \nTICKET SUB TYPE MASTER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("TICKET SUB TYPE parsed ");

				Show_Error_Msg("UPLOADING \nTICKET SUB TYPE MASTER SUCCESS");
				//				strcat(lan_Err_Msg,"TICKET SUB TYPE MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("TICKET SUB TYPE MASTER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg); //need modification for the length
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;

			}
			else
			{
				ack_flag=1;
				CstcLog_printf("TICKET SUB TYPE MASTER parsing failed");
				Show_Error_Msg("UPLOADING \nTICKET SUB TYPE MASTER FAILED");
				//				strcat(lan_Err_Msg,"TICKET SUB TYPE MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("TICKET SUB TYPE MASTER failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",TICKET_SUB_TYPE_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;
		case 12:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("BUS SERVICE MASTER RECEIVED ");
			Show_Error_Msg("UPLOADING \nBUS SERVICE MASTER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("BUS SERVICE MASTER parsed ");
				Show_Error_Msg("UPLOADING \nBUS SERVICE MASTER SUCCESS");
				//				strcat(lan_Err_Msg,"BUS SERVICE MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("BUS SERVICE MASTER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;

			}
			else
			{
				ack_flag=1;
				CstcLog_printf("BUS SERVICE MASTER parsing failed");
				Show_Error_Msg("UPLOADING \nBUS SERVICE MASTER FAILED");
				//				strcat(lan_Err_Msg,"BUS SERVICE MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("BUS SERVICE MASTER failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",BUS_SERVICE_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;
		case 13:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("BUS BRAND MASTER RECEIVED ");
			Show_Error_Msg("UPLOADING \nBUS BRAND MASTER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("BUS BRAND MASTER parsed ");
				Show_Error_Msg("UPLOADING \nBUS BRAND MASTER SUCCESS");
				//				strcat(lan_Err_Msg,"BUS BRAND MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("BUS BRAND MASTER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("BUS BRAND MASTER parsing failed");
				Show_Error_Msg("UPLOADING \nBUS BRAND MASTER FAILED");
				//				strcat(lan_Err_Msg,"BUS BRAND MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("BUS BRAND MASTER failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",BUS_BRAND_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;
		case 14:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("BUS DEPOT MASTER RECEIVED ");
			Show_Error_Msg("UPLOADING \nBUS DEPOT MASTER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("BUS DEPOT MASTER parsed ");
				Show_Error_Msg("UPLOADING \nBUS DEPOT MASTER SUCCESS");
				//				strcat(lan_Err_Msg,"BUS DEPOT MASTER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("BUS DEPOT MASTER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("BUS DEPOT MASTER parsing failed");
				Show_Error_Msg("UPLOADING \nBUS DEPOT MASTER FAILED");
				//				strcat(lan_Err_Msg,"BUS DEPOT MASTER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("BUS DEPOT MASTER failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",DEPOT_MASTER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;
		case 15:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("Route Header RECEIVED ");
			Show_Error_Msg("UPLOADING \nROUTE HEADER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("Route Header parsed ");
				Show_Error_Msg("UPLOADING \nROUTE HEADER SUCCESS");
				//				strcat(lan_Err_Msg,"Route Header Success");
				//				lan_Err_Len = lan_Err_Len + strlen("Route Header Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("Route Header parsing failed");
				Show_Error_Msg("UPLOADING \nROUTE HEADER FAILED");
				//				strcat(lan_Err_Msg,"Route Header failed");
				//				lan_Err_Len = lan_Err_Len + strlen("Route Header failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",ROUTE_HEADER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//check_master_status();
			break;

		case 16:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			//			check_master_status();
			CstcLog_printf("ETM READ WRITE KEY RECEIVED ");
			Show_Error_Msg("UPLOADING \nETM READ WRITE KEY");
			retval =packet_parser_input_master(tempCh,lan_global_temp,updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("ETM READ WRITE KEY parsed ");
				Show_Error_Msg("UPLOADING \nETM READ WRITE KEY SUCCESS");
				//				strcat(lan_Err_Msg,"ETM READ WRITE KEY Success");
				//				lan_Err_Len = lan_Err_Len + strlen("ETM READ WRITE KEY Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;

				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
				{
					master_status_struct_var.upload_master=2;
					*j=0;
				}

				//				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
				//					master_status_struct_var.upload_master=2;
				//check_master_status();
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("ETM READ WRITE KEY parsing failed");
				Show_Error_Msg("UPLOADING \nETM READ WRITE KEY FAILED");
				//				strcat(lan_Err_Msg,"ETM READ WRITE KEY failed");
				//				lan_Err_Len = lan_Err_Len + strlen("ETM READ WRITE KEY failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",ETM_READ_WRITE_KEY_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			//			check_master_status();
			//			return 1;
			break;


		case 17:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("TRUNK AND FEEDER RECEIVED ");
			Show_Error_Msg("UPLOADING \nTRUNK AND FEEDER");
			retval =packet_parser_input_master(tempCh,lan_global_temp, updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("TRUNK AND FEEDER parsed ");
				Show_Error_Msg("UPLOADING \nTRUNK AND FEEDER SUCCESS");
				//				strcat(lan_Err_Msg,"TRUNK AND FEEDER Success");
				//				lan_Err_Len = lan_Err_Len + strlen("TRUNK AND FEEDER Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;

			}
			else
			{
				ack_flag=1;
				CstcLog_printf("TRUNK AND FEEDER failed");
				Show_Error_Msg("UPLOADING \nTRUNK AND FEEDER FAILED");
				//				strcat(lan_Err_Msg,"TRUNK AND FEEDER failed");
				//				lan_Err_Len = lan_Err_Len + strlen("TRUNK AND FEEDER failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",TRUNK_FEEDER_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);


				beep(2800,1500);
			}
			//check_master_status();
			break;


		case 18:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("SERVICE_OP RECEIVED ");
			Show_Error_Msg("UPLOADING \nSERVICE_OP");
			retval =packet_parser_input_master(tempCh,lan_global_temp,updateDataFlag);
			if(retval)
			{
				ack_flag=1;
				CstcLog_printf("SERVICE_OP parsed ");
				Show_Error_Msg("UPLOADING \nSERVICE_OP SUCCESS");
				//				strcat(lan_Err_Msg,"SERVICE_OP Success");
				//				lan_Err_Len = lan_Err_Len + strlen("SERVICE_OP Success");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;

				//				if(DbInterface_Update_Master_Status_Data("update master_status set upload_master ='2'"))
				//					master_status_struct_var.upload_master=2;
				//check_master_status();
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("SERVICE_OP parsing failed");
				Show_Error_Msg("UPLOADING \nSERVICE_OP FAILED");
				//				strcat(lan_Err_Msg,"SERVICE_OP failed");
				//				lan_Err_Len = lan_Err_Len + strlen("SERVICE_OP failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				sprintf(lan_Err_Msg,"%02d",SERVICE_OP_FAILED);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);

				beep(2800,1500);
			}
			break;

		case 45:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			retval =packet_parser_nresponder(bTemp);
			if(retval)
			{
				pkt_ptr= (char *)malloc((sizeof(download_response_struct)+length));
				memset(pkt_ptr,0,(sizeof(download_response_struct)+length));
				compose_download_response_packet(pkt_ptr,&length);
				CstcLog_printf("*****************************************");
				memset(temp_dsply,0,50);
				sprintf(temp_dsply,"UPLOADING DATA\nRECORDS = %d/%d",(lan_GDownload_pktCnt * DOWNLOAD_TICKET_RANGE),lan_Global_rowCount);
				Show_Error_Msg(temp_dsply);
				CstcLog_printf("Sending Download Response Packet");
				//				length += 26 ;
				CstcLog_printf("pkt_ptr bytes are : ");
				CstcLog_HexDump(pkt_ptr,length);
				if(master_status_struct_var.download_ticket==0)
					if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='1'"))
						master_status_struct_var.download_ticket=1;
				check_master_status();
				if(DbInterface_Update_Master_Status_Data("update waybill_master set collection_status = 'D'"))
				{
					//						get_waybill_details();
					CstcLog_printf("waybill_master shift_status ==> %c ",waybill_master_struct_var.shift_status);
					//						check_collection_status();
				}

				//					if(download_response_struct_var.start_tkt_cnt==0)
				//					{
				//						if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='2'"))
				//							check_master_status();
				//					}
				pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
				memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
				final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

				CstcLog_printf("Sent bytes are : ");
				CstcLog_HexDump(pkt_format_ptr,length);
				CstcLog_printf("Sent %d bytes ", length);
				error = write(iclient_socket,pkt_format_ptr, length);
				CstcLog_printf("error %d bytes ", error);
				if (error <= 0)
				{
					CstcLog_printf("Download Response Packet 46 Connection was broken !!");
					//					strcat(lan_Err_Msg,"Download Response Packet 46 Connection was broken !!");
					//					lan_Err_Len = lan_Err_Len + strlen("Download Response Packet 46 Connection was broken !!");
					//					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

					sprintf(lan_Err_Msg,"%02d",CONNECTION_WAS_BROKEN);
					lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);


					//					memset(pkt_ptr,0,(sizeof(download_response_struct)+1));
					//					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
					Show_Error_Msg("DOWNLOADING FAILED");
					beep(2800,1500);
					if(pkt_ptr!=NULL)
					{
						free(pkt_ptr);
						pkt_ptr=NULL;
					}
					if(pkt_format_ptr!=NULL)
					{
						free(pkt_format_ptr);
						pkt_format_ptr=NULL;
					}
					return 0;
				}
				else
				{
					CstcLog_printf("Download Response Packet 46 Connection was Not broken !!");
					//					strcat(lan_Err_Msg,"Download Response Packet 46 Connection was Not broken !!");
					//					lan_Err_Len = lan_Err_Len + strlen("Download Response Packet 46 Connection was Not broken !!");
					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

					//					memset(pkt_ptr,0,(sizeof(download_response_struct)+1));
					//					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
					if(pkt_ptr!=NULL)
					{
						free(pkt_ptr);
						pkt_ptr=NULL;
					}
					if(pkt_format_ptr!=NULL)
					{
						free(pkt_format_ptr);
						pkt_format_ptr=NULL;
					}
					return 1;
				}
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("Download Request failed");
				Show_Error_Msg("DOWNLOADING FAILED");
				//				strcat(lan_Err_Msg,"Download Request failed");
				//				lan_Err_Len = lan_Err_Len + strlen("Download Request failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",DOWNLOAD_REQUEST_FAILED);
				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);

				beep(2800,1500);
			}
			break;

		case 47:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("---------After DB_DATA_FILE_ENABLE---------------");
			retval =packet_parser_nresponder(bTemp);
			CstcLog_printf("retval = %d",retval);
			if(retval)
			{
				pkt_ptr= (char *)malloc((DOWNLOAD_TICKET_RANGE*sizeof(transaction_log_struct))+1+DOWNLOAD_TICKET_RANGE);
				CstcLog_printf("(DOWNLOAD_TICKET_RANGE*sizeof(transaction_log_struct))+1+DOWNLOAD_TICKET_RANGE = %d",(DOWNLOAD_TICKET_RANGE*sizeof(transaction_log_struct))+1+DOWNLOAD_TICKET_RANGE);
				memset(pkt_ptr,0,((DOWNLOAD_TICKET_RANGE*sizeof(transaction_log_struct))+1+DOWNLOAD_TICKET_RANGE));

				compose_transaction_data_packets(pkt_ptr,&length);
				CstcLog_printf("*****************************************");
				memset(temp_dsply,0,50);
				sprintf(temp_dsply,"UPLOADING DATA\nRECORDS = %d/%d",(lan_GDownload_pktCnt * DOWNLOAD_TICKET_RANGE),(lan_Global_rowCount+(lan_GDownload_pktCnt * DOWNLOAD_TICKET_RANGE)));
				Show_Error_Msg(temp_dsply);
				//					Show_Error_Msg("UPLOADING DATA\nRECORDS = %d/%d",(lan_GDownload_pktCnt * DOWNLOAD_TICKET_RANGE),(lan_Global_rowCount+(lan_GDownload_pktCnt * DOWNLOAD_TICKET_RANGE)));
				CstcLog_printf("Sending Download Response Packet");
				//				length += 26 ;
				CstcLog_printf("pkt_ptr bytes are : ");
				CstcLog_HexDump(pkt_ptr,length);
				//					if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='1'"))
				//						check_master_status();
				//				if(strlen(download_response_struct_var.terminal_id)> 0)
				//					if(strcmp(download_response_struct_var.terminal_id,"00000000")== 0)
				//					{
				//						DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='2'");
				//					}
				//check_master_status();
				if(strcmp(download_response_struct_var.start_tkt_cnt,"00000") == 0)
				{
					CstcLog_printf("inside strcmp");
					if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='2'"))
					{
						master_status_struct_var.download_ticket=2;//check_master_status();
						*j=0;
					}
					CstcLog_printf("after update download_ticket 2");
					ack_flag=1;
					CstcLog_printf("Download Completed Successfully");

					sprintf(lan_Err_Msg,"%02d",SUCCESS);
					lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
					lan_Err_Msg[lan_Err_Len]='\0';
					memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					lan_Err_Len=0;

					//					strcat(lan_Err_Msg,"Download Completed Successfully");
					//					lan_Err_Len = lan_Err_Len + strlen("Download Completed Successfully");
					//					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
					lan_GDownload_pktCnt=0;
				}
				else
				{
					//						if(DbInterface_Update_Master_Status_Data("update master_status set download_ticket ='1'"))
					//							check_master_status();
					//					}

					pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
					final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

					CstcLog_printf("Sent bytes are : ");
					CstcLog_HexDump(pkt_format_ptr,length);
					//				for(j=0 ; j< length ; j++)
					//					CstcLog_printf("%2x, ", pkt_format_ptr[j]);
					CstcLog_printf("Sent %d bytes ", length);
					error = write(iclient_socket,pkt_format_ptr, length);
					CstcLog_printf("error %d bytes ", error);
					if (error <= 0)
					{
						CstcLog_printf("Download Response Packet 47 Connection was broken !!");
						//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
						//memcpy(lan_Err_Msg,"Download Response Packet 47 Connection was broken !!",52);
						strcat(lan_Err_Msg,"Download Response Packet 47 Connection was broken !!");
						lan_Err_Len = lan_Err_Len + strlen("Download Response Packet 47 Connection was broken !!");
						CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
						//					memset(pkt_ptr,0,(sizeof(transaction_log_struct)+1));
						//					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
						Show_Error_Msg("DOWNLOADING FAILED");
						beep(2800,1500);
						if(pkt_ptr!=NULL)
						{
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}
						return 0;
					}
					else
					{
						CstcLog_printf("Download Response Packet 47 Connection was Not broken !!");

						//						strcat(lan_Err_Msg,"Download Response Packet 47 Connection was Not broken !!");
						//						lan_Err_Len = lan_Err_Len + strlen("Download Response Packet 47 Connection was Not broken !!");
						//						CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
						//					memset(pkt_ptr,0,(sizeof(transaction_log_struct)+1));
						//					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
						if(pkt_ptr!=NULL)
						{
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}
						return 1;
					}
				}
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("Download Request failed");
				Show_Error_Msg("DOWNLOADING FAILED");
				//				strcat(lan_Err_Msg,"Download Request failed");
				//				lan_Err_Len = lan_Err_Len + strlen("Download Request failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",DOWNLOAD_REQUEST_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			break;

		case 50:
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("ERASE PACKET RECEIVED");
			if(master_status_struct_var.erase_ticket==0)
				if(DbInterface_Update_Master_Status_Data("update master_status set erase_ticket ='1'"))
					master_status_struct_var.erase_ticket=1;
			check_master_status();
			retval =packet_parser_nresponder(bTemp);
			if(retval)
			{
				ack_flag=1;
				//					if(strlen(download_response_struct_var.terminal_id)> 0)
				//						if(strcmp(download_response_struct_var.terminal_id,"00000000")!= 0)
				//						{
				if(!DbInterface_Delete_From_All_Tables(updateDataFlag,lan_global_schedule))
				{
					ack_flag=1;
					CstcLog_printf("Erase Request failed");
					Show_Error_Msg("ERASE FAILED");
					//					strcat(lan_Err_Msg,"Erase Request failed");
					//					lan_Err_Len = lan_Err_Len + strlen("Erase Request failed");
					//					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERASE_DATA_FAILED);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					beep(2800,1500);
					break;
				}

				if(DbInterface_Update_Master_Status_Data("update master_status set erase_ticket ='2'"))
				{
					master_status_struct_var.erase_ticket=2;
					*j=0;
				}
				//				check_master_status();
				//				get_waybill_details();
				//				DbInterface_Delete_Table("Delete from master_status");
				//				DbInterface_Insert_In_All_Tables1("master_status","'0','0','2','0','0','0','0'",27);
				//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				//memcpy(lan_Err_Msg,"Data Deleted",12);
				//				strcat(lan_Err_Msg,"Data Deleted");
				//				lan_Err_Len = lan_Err_Len + strlen("Data Deleted");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				sprintf(lan_Err_Msg,"%02d",SUCCESS);
				lan_Err_Len = lan_Err_Len + strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
				lan_Err_Len=0;

				//						}
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("Erase Request failed");
				Show_Error_Msg("ERASE FAILED");
				//				strcat(lan_Err_Msg,"Erase Request failed");
				//				lan_Err_Len = lan_Err_Len + strlen("Erase Request failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",ERASE_DATA_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				beep(2800,1500);
			}
			break;

		case 54 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			CstcLog_printf("******************case 54***********************");
			if(master_status_struct_var.machine_register==0)
			{
				if(DbInterface_Update_Master_Status_Data("update master_status set machine_register ='1'"))
				{
					master_status_struct_var.machine_register=1;
					check_master_status();
				}
			}
			CstcLog_printf("======case 54======");
			retval =packet_parser_nresponder(bTemp);
			if(retval)
			{
				pkt_ptr= (char *)malloc(sizeof(registration_response_struct));
				memset(pkt_ptr,0,sizeof(registration_response_struct));
				compose_registration_response_packet(pkt_ptr,&length);
				CstcLog_printf("*****************************************");
				CstcLog_printf("Sending Registration Response Packet");

				if(strlen(registration_response_struct_var.terminal_id)> 0)
					if(strcmp(registration_response_struct_var.terminal_id,"00000000")!= 0)
					{
						if(master_status_struct_var.machine_register!=2)
							if(DbInterface_Update_Master_Status_Data("update master_status set machine_register ='2'"))
							{
								master_status_struct_var.machine_register=2;
								*j=0;
							}
					}
				pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
				memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
				final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

				CstcLog_printf("Sent bytes are : ");
				CstcLog_HexDump(pkt_format_ptr,length);
				//				for(j=0 ; j< length ; j++)
				//					CstcLog_printf("%2x, ", pkt_format_ptr[j]);
				CstcLog_printf("Sent %d bytes ", length);
				error = write(iclient_socket,pkt_format_ptr, length);

				if (error <= 0)
				{
					CstcLog_printf("Registration Response Packet 55 Connection was broken !!");

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",CONNECTION_WAS_BROKEN);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Registration Response Packet 55 Connection was broken !!");
					//					lan_Err_Len = lan_Err_Len + strlen("Registration Response Packet 55 Connection was broken !!");
					//					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
					Show_Error_Msg("REGISTRATION CHECK FAILED");
					beep(2800,1500);
					if(pkt_ptr!=NULL)
					{
						free(pkt_ptr);
						pkt_ptr=NULL;
					}
					if(pkt_format_ptr!=NULL)
					{
						free(pkt_format_ptr);
						pkt_format_ptr=NULL;
					}
					return 0;
				}
				else
				{
					CstcLog_printf("Registration Response Packet 55 Connection was Not broken !!");

					//					strcat(lan_Err_Msg,"Registration Response Packet 55 Connection was Not broken !!");
					//					lan_Err_Len = lan_Err_Len + strlen("Registration Response Packet 55 Connection was Not broken !!");
					//					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
					if(pkt_ptr!=NULL)
					{
						free(pkt_ptr);
						pkt_ptr=NULL;
					}
					if(pkt_format_ptr!=NULL)
					{
						free(pkt_format_ptr);
						pkt_format_ptr=NULL;
					}
					return 1;
				}
			}
			else
			{
				ack_flag=1;
				CstcLog_printf("ETM Request failed");
				//				strcat(lan_Err_Msg,"ETM Request failed");
				//				lan_Err_Len = lan_Err_Len + strlen("ETM Request failed");
				//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",REGISTRATION_REQUEST_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				Show_Error_Msg("ETM REGISTRATION CHECK FAILED");
				beep(2800,1500);
			}
			check_master_status();
			break;
		case 56 :
			memset(lan_global_buff,0,sizeof(lan_global_buff));
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			{
				CstcLog_printf("Machine Registration RECEIVED ");
				if(master_status_struct_var.machine_register==0)
					if(DbInterface_Update_Master_Status_Data("update master_status set machine_register ='1'"))
					{
						master_status_struct_var.machine_register=1;
						check_master_status();
					}
				retval = packet_parser_nresponder(bTemp);
				if(retval)
				{
					CstcLog_printf("*****************************************");
					pkt_ptr= (char *)malloc(sizeof(registration_response_struct));
					memset(pkt_ptr,0,sizeof(registration_response_struct));
					compose_registration_response_packet(pkt_ptr,&length);
					CstcLog_printf("Sending Registration Response Packet");
					//						length += 26 ;

					if(strlen(registration_response_struct_var.terminal_id)> 0)
					{
						if(master_status_struct_var.machine_register!=2)
							if(DbInterface_Update_Master_Status_Data("update master_status set machine_register ='2'"))
							{
								master_status_struct_var.machine_register=2;
								*j=0;
							}
					}

					pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length));
					memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length));
					final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

					CstcLog_printf("Sent bytes are : ");
					CstcLog_HexDump(pkt_format_ptr,length);

					CstcLog_printf("Sent %d bytes ", length);
					error = write(iclient_socket,pkt_format_ptr, length);

					if (error <= 0)
					{
						CstcLog_printf("Registration Response Packet 56 Connection was broken !!");

						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",CONNECTION_WAS_BROKEN);
						strcat(lan_Err_Msg,Err_MsgAry);
						lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
						CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);


						//					strcat(lan_Err_Msg,"Registration Response Packet 56 Connection was broken !!");
						//					lan_Err_Len = lan_Err_Len + strlen("Registration Response Packet 56 Connection was broken !!");
						//					CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
						check_master_status();
						Show_Error_Msg("REGISTRATION FAILED");
						beep(2800,1500);
						if(pkt_ptr!=NULL)
						{
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}
						return 0;
					}
					else
					{
						strcat(lan_Err_Msg,"Registration Response Packet 56 Connection was Not broken !!");
						lan_Err_Len = lan_Err_Len + strlen("Registration Response Packet 56 Connection was Not broken !!");
						CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);

						check_master_status();
						if(pkt_ptr!=NULL)
						{
							free(pkt_ptr);
							pkt_ptr=NULL;
						}
						if(pkt_format_ptr!=NULL)
						{
							free(pkt_format_ptr);
							pkt_format_ptr=NULL;
						}
						return 1;
					}

				}
				else
				{
					ack_flag=1;
					CstcLog_printf("ETM REGISTRATION failed");

					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",REGISTRATION_REQUEST_FAILED);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//				strcat(lan_Err_Msg,"ETM Request failed");
					//				lan_Err_Len = lan_Err_Len + strlen("ETM Request failed");
					//				CstcLog_printf("lan_Err_Len = %d",lan_Err_Len);
					Show_Error_Msg("REGISTRATION FAILED");
					beep(2800,1500);
				}
			}
			check_master_status();
			break;
		default :
			break;
		}
	}
	if(ack_flag)
	{
		//length=100;
		CstcLog_printf("lan_Err_Len=%d>>------ACK1--------<<%d",lan_Err_Len,(sizeof(ack_packet_struct)+lan_Err_Len+1));
		pkt_ptr = (char*)malloc((sizeof(ack_packet_struct)+lan_Err_Len+1));
		memset(pkt_ptr,0,(sizeof(ack_packet_struct)+lan_Err_Len+1));

		CstcLog_printf(">>------ACK2--------<<");
		//pkt_ptr = (char*)malloc(sizeof(ack_packet_struct));
		compose_ack_packet(pkt_ptr,&length);

		pkt_format_ptr = (char *)malloc((sizeof(packet_format_struct)+length+1));
		memset(pkt_format_ptr,0,(sizeof(packet_format_struct)+length+1));

		CstcLog_printf("%d>>------ACK3--------<<%d",length,(sizeof(packet_format_struct)+length+1));
		final_packet_lan(pkt_format_ptr, pkt_ptr, &length);

		CstcLog_printf("Sent bytes are : ");
		CstcLog_HexDump(pkt_format_ptr,length);
		//	for(j=0 ; j< 100 ; j++)
		//		CstcLog_printf("%2x, ",pkt_format_ptr[j]);
		CstcLog_printf("Sent %d bytes",length);
		CstcLog_printf(">>-----------------<<");

		if(write(iclient_socket, pkt_format_ptr, length) < 0)
		{
			CstcLog_printf("ACK Connection was broken!!!");
			Show_Error_Msg("ACKNOWLEDGE FAILED");
			beep(2800,1500);
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			//			memset(Err_Msg1,0,500);
			lan_Err_Len=0;
			return 0;
		}
		else
		{
			CstcLog_printf("ACK Connection was Not broken!!!");
			if(pkt_ptr!=NULL)
			{
				free(pkt_ptr);
				pkt_ptr=NULL;
			}
			CstcLog_printf("1. ACK Connection was Not broken!!!");
			if(pkt_format_ptr!=NULL)
			{
				free(pkt_format_ptr);
				pkt_format_ptr=NULL;
			}
			CstcLog_printf("2. ACK Connection was Not broken!!!");
			memset(lan_Err_Msg,0,ERROR_MSG_BUFFER_LEN);
			//			memset(Err_Msg1,0,500);
			CstcLog_printf("3. ACK Connection was Not broken!!!");
			lan_Err_Len=0;
			CstcLog_printf("4. ACK Connection was Not broken!!!");
			return 1;
		}
	}
	if(pkt_ptr!=NULL)
	{
		free(pkt_ptr);
		pkt_ptr=NULL;
	}
	if(pkt_format_ptr!=NULL)
	{
		free(pkt_format_ptr);
		pkt_format_ptr=NULL;
	}
	return 0;
}

//----------------------------------------WAYBILL MASTER PACKET------------------------------------
void compose_waybill_packet(char * pkt_ptr)
{
#if 0
	int temp = WAYBILL_MASTER_PKT_ID;
	temp = htons(temp);
	memcpy(waybill_mas_packet_id_struct_var.waybill_mas_pkt_id, &temp, PKT_ID_LEN);
	strcpy(waybill_master_struct_var.waybill_no,"0000000001");
	strcpy(waybill_master_struct_var.depot_name_english,"Koramangala" );
	strcpy(waybill_master_struct_var.depot_name_bangla,"Koramangala");
	strcpy(waybill_master_struct_var.conductor_id,"Con1");
	strcpy(waybill_master_struct_var.conductor_name_english,"Prathamesh");
	strcpy(waybill_master_struct_var.conductor_name_bangla,"Prathamesh");
	strcpy(waybill_master_struct_var.driver_id, "Dri1");
	strcpy(waybill_master_struct_var.driver_name_english,"sudhakar");
	strcpy(waybill_master_struct_var.driver_name_bangla,"sudhakar");
	strcpy(waybill_master_struct_var.vehicle_no,"KA 3425");
	strcpy(waybill_master_struct_var.etim_no,"CSTC000001" );
	strcpy(waybill_master_struct_var.waybill_date,"15/04/2014" );
	strcpy(waybill_master_struct_var.waybill_time,"18:15:09" );
	strcpy(waybill_master_struct_var.duty_start_date,"20/04/2014");
	strcpy(waybill_master_struct_var.duty_start_time,"05:15:09" );
	strcpy(waybill_master_struct_var.locking_date,"21/04/2014" );
	strcpy(waybill_master_struct_var.locking_time,"05:45:10" );
	strcpy(waybill_master_struct_var.locking_reciept,"99" );
	strcpy(waybill_master_struct_var.locking_amt, "999");
	strcpy(waybill_master_struct_var.master_password,"4321");
	strcpy(waybill_master_struct_var.mac_address,"ac.b9.0c.e4.cd");
	strcpy(waybill_master_struct_var.software_ver,"1.1" );
	strcpy(waybill_master_struct_var.duty_no,"10");
	strcpy(waybill_master_struct_var.max_adult,"5" );
	strcpy(waybill_master_struct_var.max_child, "5");
	strcpy(waybill_master_struct_var.tkt_interval, "Y");
	strcpy(&waybill_master_struct_var.shift_status,  "Y");
	strcpy(&waybill_master_struct_var.collection_status, "01" );
	strcpy(waybill_master_struct_var.waybill_lock_code,"4321" );
	strcpy(&waybill_master_struct_var.manual_duty_enable_flag,"Y" );
	strcpy(waybill_master_struct_var.functions,"1" );

	memcpy(pkt_ptr, waybill_mas_packet_id_struct_var.waybill_mas_pkt_id,PKT_ID_LEN);
	memcpy((pkt_ptr + 2),waybill_master_struct_var.waybill_no,  WAYBILL_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN),waybill_master_struct_var.depot_name_english, NAME_ENG_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN),waybill_master_struct_var.depot_name_bangla, NAME_KAN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN), waybill_master_struct_var.conductor_id,COND_ID_LEN);
	memcpy( (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN),waybill_master_struct_var.conductor_name_english, NAME_ENG_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN),waybill_master_struct_var.conductor_name_bangla,  NAME_KAN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN),waybill_master_struct_var.driver_id,  DRVR_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN),waybill_master_struct_var.driver_name_english,  NAME_ENG_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN), waybill_master_struct_var.driver_name_bangla, NAME_KAN_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN),waybill_master_struct_var.vehicle_no,  VEHICLE_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN),waybill_master_struct_var.etim_no, TERMINAL_ID_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN), waybill_master_struct_var.waybill_date, DATE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN),waybill_master_struct_var.waybill_time, TIME_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN),waybill_master_struct_var.duty_start_date,  DATE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN), waybill_master_struct_var.duty_start_time, TIME_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),waybill_master_struct_var.locking_date, DATE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN),waybill_master_struct_var.locking_time,  TIME_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),waybill_master_struct_var.locking_reciept,  LOCK_RCPT_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN), waybill_master_struct_var.locking_amt, LOCK_AMT_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN),waybill_master_struct_var.master_password, MASTER_PW_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN),waybill_master_struct_var.mac_address,  MAC_ADDR_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN), waybill_master_struct_var.software_ver, SOFTWARE_VER_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN),waybill_master_struct_var.max_adult,  MAX_ADULT_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN),waybill_master_struct_var.max_child,  MAX_CHILD_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN),waybill_master_struct_var.tkt_interval, TKT_INTERVAL_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN), &waybill_master_struct_var.shift_status, SHIFT_STATUS_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN),&waybill_master_struct_var.collection_status, COLL_STATUS_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN), waybill_master_struct_var.duty_no, DUTY_NO_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN+DUTY_NO_LEN),waybill_master_struct_var.waybill_lock_code, WAYBILL_LOCK_CD_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN+DUTY_NO_LEN+WAYBILL_LOCK_CD_LEN),&waybill_master_struct_var.manual_duty_enable_flag,  MANUAL_DUTY_EN_FLAG_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN+DUTY_NO_LEN+WAYBILL_LOCK_CD_LEN+MANUAL_DUTY_EN_FLAG_LEN),waybill_master_struct_var.functions, FUNCTIONS_LEN);
#endif
}

//----------------------------------------SCHEDULE MASTER PACKET------------------------------------
#if 0 //SWAP_REMOVE_THIS_CODE
void compose_schedule_packet(char * pkt_ptr)
{
	int temp = SCHEDULE_MASTER_PKT_ID;
	temp = htons(temp);
	memcpy(sched_mas_packet_id_struct_var.sched_mas_pkt_id, &temp, PKT_ID_LEN);
	strcpy(schedule_master_struct_var.serial_no,"1");
	strcpy(schedule_master_struct_var.trip_id,"001");
	strcpy(schedule_master_struct_var.schedule_no,"37-A/1");
	strcpy(schedule_master_struct_var.route_no,"37-A");
	//    strcpy(schedule_master_struct_var.brand_code,"ORD");
	strcpy(schedule_master_struct_var.bus_service_id,"01");
	strcpy(schedule_master_struct_var.depot_id,"02");
	strcpy(schedule_master_struct_var.vehicle_no,"CSTC1");
	schedule_master_struct_var.shift='Y';
	strcpy(schedule_master_struct_var.effective_from_date,"27/02/2004");
	strcpy(schedule_master_struct_var.effective_from_time,"12:00");
	strcpy(schedule_master_struct_var.effective_till_date,"27/02/2024");
	strcpy(schedule_master_struct_var.effective_till_time,"12:00");
	strcpy(schedule_master_struct_var.start_date,"27/02/2004");
	strcpy(schedule_master_struct_var.start_time,"07:00");
	strcpy(schedule_master_struct_var.end_date,"27/02/2024");
	strcpy(schedule_master_struct_var.end_time,"07:40");
	strcpy(schedule_master_struct_var.start_bus_stop_code,"101");
	strcpy(schedule_master_struct_var.end_bus_stop_code,"108");
	schedule_master_struct_var.is_flexi_fare='N';
	strcpy(schedule_master_struct_var.trip_status,"Y");

	memcpy(pkt_ptr,sched_mas_packet_id_struct_var.sched_mas_pkt_id,  PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),schedule_master_struct_var.serial_no,SERIAL_NO_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN),schedule_master_struct_var.trip_id,TRIP_ID_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN),schedule_master_struct_var.schedule_no,SCHEDULE_NO_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN),schedule_master_struct_var.route_no,ROUTE_NO_LEN);
	//	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN),schedule_master_struct_var.brand_code,BRAND_CODE_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN),schedule_master_struct_var.bus_service_id,BUS_SRVC_ID_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN),schedule_master_struct_var.depot_id,DEPOT_ID_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN),schedule_master_struct_var.vehicle_no,VEHICLE_NO_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN),&schedule_master_struct_var.shift,SHIFT_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN),schedule_master_struct_var.effective_from_date,DATE_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN),schedule_master_struct_var.effective_from_time,TIME_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN),schedule_master_struct_var.effective_till_date,DATE_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN),schedule_master_struct_var.effective_till_time,TIME_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),schedule_master_struct_var.start_date,DATE_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN),schedule_master_struct_var.start_time,TIME_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),schedule_master_struct_var.end_date,DATE_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN),schedule_master_struct_var.end_time,TIME_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),schedule_master_struct_var.start_bus_stop_code,BUS_STOP_CODE_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+BUS_STOP_CODE_LEN),schedule_master_struct_var.end_bus_stop_code,BUS_STOP_CODE_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+BUS_STOP_CODE_LEN+BUS_STOP_CODE_LEN),&schedule_master_struct_var.is_flexi_fare,1);
	memcpy((pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_ID_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+BUS_STOP_CODE_LEN+BUS_STOP_CODE_LEN+1),&schedule_master_struct_var.trip_status,TRIP_STATUS_LEN);

}
#endif
//-------------------------------------------DUTY MASTER PACKET----------------------------------------
void compose_duty_packet(char * pkt_ptr)
{
	int temp = DUTY_MASTER_PKT_ID;
	temp = htons(temp);
	//	memcpy(duty_mas_packet_id_struct_var.duty_mas_pkt_id, &temp,PKT_ID_LEN);
	//	strcpy(duty_master_struct_var.duty_no,"000001");
	//	strcpy(duty_master_struct_var.schedule_no,"1");
	//
	//	memcpy(pkt_ptr,duty_mas_packet_id_struct_var.duty_mas_pkt_id, 2);
	//	memcpy((pkt_ptr + 2),duty_master_struct_var.duty_no, DUTY_NO_LEN);
	//	memcpy((pkt_ptr + 2+DUTY_NO_LEN),duty_master_struct_var.schedule_no,SCHEDULE_NO_LEN);
}

//----------------------------------------ROUTE MASTER PACKET-----------------------------------------
void compose_route_packet(char * pkt_ptr)
{
#if 0
	int temp = ROUTE_MASTER_PKT_ID;
	temp = htons(temp);
	memcpy(route_mas_packet_id_struct_var.route_mas_pkt_id,&temp,PKT_ID_LEN);
	strcpy(route_master_struct_var.route_no,"37-A");
	strcpy(route_master_struct_var.bus_stop_code,"101");
	strcpy(route_master_struct_var.bus_stop_seq_no,"1");
	strcpy(route_master_struct_var.bus_stop_name_english,"Shivajinagar Bus Station");
	strcpy(route_master_struct_var.bus_stop_name_kanadda,"Shivajinagar Bus Station");
	strcpy(route_master_struct_var.bus_stop_alias_english,"Shivajinagar Bus Station");
	strcpy(route_master_struct_var.sub_stage,"SBS");
	strcpy(route_master_struct_var.fare_change_point,"00001");
	strcpy(route_master_struct_var.distance,"0.3");
	route_master_struct_var.fare_stage ='8';

	memcpy(pkt_ptr,route_mas_packet_id_struct_var.route_mas_pkt_id, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),route_master_struct_var.route_no,ROUTE_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+ROUTE_NO_LEN),route_master_struct_var.bus_stop_code,BUS_STOP_CODE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN),route_master_struct_var.bus_stop_seq_no,BUS_STOP_SEQ_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN),route_master_struct_var.bus_stop_name_english,NAME_ENG_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN),route_master_struct_var.bus_stop_name_kanadda,NAME_KAN_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN),route_master_struct_var.bus_stop_alias_english,BUS_STOP_ALIAS_ENG_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_ALIAS_ENG_LEN),route_master_struct_var.sub_stage,SUB_STG_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+SUB_STG_LEN+BUS_STOP_ALIAS_ENG_LEN+SUB_STG_LEN),route_master_struct_var.fare_change_point,FARE_STG_PT_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_ALIAS_ENG_LEN+SUB_STG_LEN+FARE_STG_PT_LEN),route_master_struct_var.distance,DISTANCE_LEN);
	memcpy((pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_ALIAS_ENG_LEN+SUB_STG_LEN+FARE_STG_PT_LEN+DISTANCE_LEN), &route_master_struct_var.fare_stage,FARE_STAGE_LEN);
#endif
}

//-----------------------------------------FARE CHART PACKET-------------------------------------------
void compose_fare_chart_packet(char * pkt_ptr)
{
#if 0
	int temp = FARE_CHART_PKT_ID;
	temp = htons(temp);
	memcpy(fare_chart_packet_id_struct_var.fare_chart_pkt_id,&temp,PKT_ID_LEN);
	strcpy(fare_chart_struct_var.version_id,"001");
	strcpy(fare_chart_struct_var.route_no,"37-A");
	strcpy(fare_chart_struct_var.bus_service_id,"ORD");
	strcpy(fare_chart_struct_var.from_stop_seq_no,"1");
	strcpy(fare_chart_struct_var.from_bus_stop_code,"SBS");
	strcpy(fare_chart_struct_var.from_bus_stop_english,"Shivajinagar Bus Station");
	strcpy(fare_chart_struct_var.from_bus_stop_bangla,"Shivajinagar Bus Station");
	strcpy(fare_chart_struct_var.till_stop_seq_no,"2");
	strcpy(fare_chart_struct_var.till_bus_stop_code,"MNB");
	strcpy(fare_chart_struct_var.till_bus_stop_english,"Muneshwara Block");
	strcpy(fare_chart_struct_var.till_bus_stop_bangla,"Muneshwara Block");
	strcpy(fare_chart_struct_var.travelled_km,"11.5");
	strcpy(fare_chart_struct_var.adult_fare,"5.00");
	strcpy(fare_chart_struct_var.child_fare,"2.50");
	strcpy(fare_chart_struct_var.sr_citizen_fare,"3.00");
	strcpy(fare_chart_struct_var.luggage_amt_per_unit,"0.5");
	strcpy(fare_chart_struct_var.toll_amt,"2.00");
	fare_chart_struct_var.factor_type='Y';
	fare_chart_struct_var.fare_type='Y';
	strcpy(fare_chart_struct_var.effective_from_date,"27-02-2004");
	strcpy(fare_chart_struct_var.effective_from_time,"12.00");
	strcpy(fare_chart_struct_var.effective_till_date,"27-02-2024");
	strcpy(fare_chart_struct_var.effective_till_time,"12.00");

	memcpy(pkt_ptr,fare_chart_packet_id_struct_var.fare_chart_pkt_id, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),fare_chart_struct_var.version_id,VERSION_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN),fare_chart_struct_var.route_no,ROUTE_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN),fare_chart_struct_var.bus_service_id,BUS_SRVC_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN),fare_chart_struct_var.from_stop_seq_no,BUS_STOP_SEQ_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN),fare_chart_struct_var.from_bus_stop_code,BUS_STOP_CODE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN),fare_chart_struct_var.from_bus_stop_english,NAME_ENG_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN),fare_chart_struct_var.from_bus_stop_bangla,NAME_KAN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN),fare_chart_struct_var.till_stop_seq_no,BUS_STOP_SEQ_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN),fare_chart_struct_var.till_bus_stop_code,BUS_STOP_CODE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN),fare_chart_struct_var.till_bus_stop_english,NAME_ENG_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN),fare_chart_struct_var.till_bus_stop_bangla,NAME_KAN_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN),fare_chart_struct_var.travelled_km,TRAVELLED_KM_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN),fare_chart_struct_var.adult_fare,ADULT_FARE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN),fare_chart_struct_var.child_fare,CHILD_FARE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN),fare_chart_struct_var.sr_citizen_fare,SR_CIT_FARE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN),fare_chart_struct_var.luggage_amt_per_unit,LUGG_AMT_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN),fare_chart_struct_var.toll_amt,AMOUNT_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN),&fare_chart_struct_var.factor_type, FACTOR_TYPE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN), &fare_chart_struct_var.fare_type,FARE_TYPE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN),fare_chart_struct_var.effective_from_date,DATE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN+DATE_LEN),fare_chart_struct_var.effective_from_time,TIME_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN+DATE_LEN+TIME_LEN),fare_chart_struct_var.effective_till_date,DATE_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN+DATE_LEN+TIME_LEN+DATE_LEN),fare_chart_struct_var.effective_till_time,TIME_LEN);
#endif
}

int packet_parser_lan(char * bTemp)
{
	char * pkt_ptr;
	char *mac_ID = NULL;
	FILE* tempFile= NULL;
	int temp = 0;
	char tempChID[3]={'\0'};
	int len = 0,pkt_data_len=0;
	char tempName[100]={'\0'};
	char *tempStr; //[1024];
	int tempStrLen=0;
	char *sn = NULL;
	if(bTemp[0] == ';')
	{
		memcpy(&(packet_format_struct_var.start_indicator), bTemp,START_INDICATOR_LEN);
		memcpy(&(packet_format_struct_var.data_length),(bTemp+START_INDICATOR_LEN) ,DATA_LENGTH_LEN);
		len = atoi(packet_format_struct_var.data_length);
		CstcLog_printf("data_length in packet %s = %d",packet_format_struct_var.data_length, len);
		pkt_data_len = len-(START_INDICATOR_LEN+DATA_LENGTH_LEN+CHECKSUM_LEN);
		packet_format_struct_var.data = (char*)malloc((pkt_data_len+1));
		memset(packet_format_struct_var.data,0,(pkt_data_len+1));
		memcpy(packet_format_struct_var.data,(bTemp+START_INDICATOR_LEN+DATA_LENGTH_LEN),pkt_data_len);
		packet_format_struct_var.data[pkt_data_len]='\0';
		memcpy(packet_format_struct_var.checksum,(bTemp+START_INDICATOR_LEN+DATA_LENGTH_LEN+len),CHECKSUM_LEN);
		//		pkt_ptr = malloc(len);
		//		memcpy(pkt_ptr,packet_format_struct_var.data,len);
		pkt_ptr=packet_format_struct_var.data;

		tempChID[0] = pkt_ptr[0];
		tempChID[1] = pkt_ptr[1];

		//		memcpy(&temp, tempChID, 2);
		temp = atoi(tempChID);
		CstcLog_printf("packet data length = %d  & Id = %d",pkt_data_len, temp);

		switch(temp)
		{
		case 1 :/*-------------------WAYBILL----------------------*/
			//		memcpy(waybill_mas_packet_id_struct_var.waybill_mas_pkt_id, (pkt_ptr), PKT_ID_LEN);
			//		memcpy(waybill_master_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN), WAYBILL_NO_LEN);
			//		memcpy(waybill_master_struct_var.depot_name_english, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN),NAME_ENG_LEN);
			//		memcpy(waybill_master_struct_var.depot_name_bangla, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN), NAME_KAN_LEN);
			//		memcpy(waybill_master_struct_var.conductor_id,(pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN), COND_ID_LEN);
			//		memcpy(waybill_master_struct_var.conductor_name_english, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN), NAME_ENG_LEN);
			//		memcpy(waybill_master_struct_var.conductor_name_bangla, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN), NAME_KAN_LEN);
			//		memcpy(waybill_master_struct_var.driver_id, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN), DRVR_ID_LEN);
			//		memcpy(waybill_master_struct_var.driver_name_english, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN), NAME_ENG_LEN);
			//		memcpy(waybill_master_struct_var.driver_name_bangla, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN), NAME_KAN_LEN);
			//		memcpy(waybill_master_struct_var.vehicle_no, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN), VEHICLE_NO_LEN);
			//		memcpy(waybill_master_struct_var.etim_no, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN),TERMINAL_ID_LEN);
			//		memcpy(waybill_master_struct_var.waybill_date, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN), DATE_LEN);
			//		memcpy(waybill_master_struct_var.waybill_time, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN),TIME_LEN);
			//		memcpy(waybill_master_struct_var.duty_start_date, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN), DATE_LEN);
			//		memcpy(waybill_master_struct_var.duty_start_time, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN), TIME_LEN);
			//		memcpy(waybill_master_struct_var.locking_date, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),DATE_LEN);
			//		memcpy(waybill_master_struct_var.locking_time, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN), TIME_LEN);
			//		memcpy(waybill_master_struct_var.locking_reciept, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN), LOCK_RCPT_LEN);
			//		memcpy(waybill_master_struct_var.locking_amt, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN), LOCK_AMT_LEN);
			//		memcpy(waybill_master_struct_var.master_password, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN),MASTER_PW_LEN);
			//		memcpy(waybill_master_struct_var.mac_address, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN), MAC_ADDR_LEN);
			//		memcpy(waybill_master_struct_var.software_ver, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN), SOFTWARE_VER_LEN);
			//		memcpy(waybill_master_struct_var.max_adult, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN), MAX_ADULT_LEN);
			//		memcpy(waybill_master_struct_var.max_child, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN), MAX_CHILD_LEN);
			//		memcpy(waybill_master_struct_var.tkt_interval, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN), TKT_INTERVAL_LEN);
			//		memcpy(&waybill_master_struct_var.shift_status, (pkt_ptr +PKT_ID_LEN+ WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN), SHIFT_STATUS_LEN);
			//		memcpy(&waybill_master_struct_var.collection_status, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN),COLL_STATUS_LEN);
			//		memcpy(waybill_master_struct_var.duty_no, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN), DUTY_NO_LEN);
			//		memcpy(waybill_master_struct_var.waybill_lock_code, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN+DUTY_NO_LEN), WAYBILL_LOCK_CD_LEN);
			//		memcpy(&waybill_master_struct_var.manual_duty_enable_flag, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN+DUTY_NO_LEN+WAYBILL_LOCK_CD_LEN), MANUAL_DUTY_EN_FLAG_LEN);
			//		memcpy(waybill_master_struct_var.functions, (pkt_ptr + PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN+DUTY_NO_LEN+WAYBILL_LOCK_CD_LEN+MANUAL_DUTY_EN_FLAG_LEN), FUNCTIONS_LEN);
			//
			tempStrLen = pkt_data_len-PKT_ID_LEN+1;//PKT_ID_LEN+WAYBILL_NO_LEN+NAME_ENG_LEN+NAME_KAN_LEN+COND_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+DRVR_ID_LEN+NAME_ENG_LEN+NAME_KAN_LEN+VEHICLE_NO_LEN+TERMINAL_ID_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+LOCK_RCPT_LEN+LOCK_AMT_LEN+MASTER_PW_LEN+MAC_ADDR_LEN+SOFTWARE_VER_LEN+MAX_ADULT_LEN+MAX_CHILD_LEN+TKT_INTERVAL_LEN+SHIFT_STATUS_LEN+COLL_STATUS_LEN+DUTY_NO_LEN+WAYBILL_LOCK_CD_LEN+MANUAL_DUTY_EN_FLAG_LEN+ FUNCTIONS_LEN;
			tempStr = malloc(tempStrLen*sizeof(char));
			memset(tempStr,0,tempStrLen);
			memcpy(tempStr, (pkt_ptr + PKT_ID_LEN),(pkt_data_len-PKT_ID_LEN));
			tempStr[tempStrLen]='\0';
			//		sprintf(tempStr,"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
			//				"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%c\',\'%c\',\'%s\',\'%s\',\'%c\',\'%s\'",
			//				waybill_master_struct_var.waybill_no,
			//				waybill_master_struct_var.depot_name_english, waybill_master_struct_var.depot_name_bangla,
			//				waybill_master_struct_var.conductor_id, waybill_master_struct_var.conductor_name_english,
			//				waybill_master_struct_var.conductor_name_bangla, waybill_master_struct_var.driver_id,
			//				waybill_master_struct_var.driver_name_english, waybill_master_struct_var.driver_name_bangla,
			//				waybill_master_struct_var.vehicle_no, waybill_master_struct_var.etim_no,
			//				waybill_master_struct_var.waybill_date, waybill_master_struct_var.waybill_time,
			//				waybill_master_struct_var.duty_start_date,waybill_master_struct_var.duty_start_time,
			//				waybill_master_struct_var.locking_date, waybill_master_struct_var.locking_time,
			//				waybill_master_struct_var.locking_reciept, waybill_master_struct_var.locking_amt,
			//				waybill_master_struct_var.master_password, waybill_master_struct_var.mac_address,
			//				waybill_master_struct_var.software_ver,waybill_master_struct_var.max_adult,
			//				waybill_master_struct_var.max_child,waybill_master_struct_var.tkt_interval,
			//				waybill_master_struct_var.shift_status,	waybill_master_struct_var.collection_status,
			//				waybill_master_struct_var.duty_no,waybill_master_struct_var.waybill_lock_code,
			//				waybill_master_struct_var.manual_duty_enable_flag, waybill_master_struct_var.functions);
			memcpy(tempName, "waybill_master",14);
			CstcLog_printf("tempName %s = tempStr %s",tempName, tempStr);
			break;

		case 3: /*-----------------SCHEDULE-----------------------*/
			//			memcpy(sched_mas_packet_id_struct_var.sched_mas_pkt_id, pkt_ptr, PKT_ID_LEN);
			//			memcpy(schedule_master_struct_var.serial_no,(pkt_ptr + PKT_ID_LEN),SERIAL_NO_LEN);
			//			memcpy(schedule_master_struct_var.trip_id,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN),TRIP_ID_LEN);
			//			memcpy(schedule_master_struct_var.schedule_no,(pkt_ptr +PKT_ID_LEN+ SERIAL_NO_LEN+TRIP_ID_LEN),SCHEDULE_NO_LEN);
			//			memcpy(schedule_master_struct_var.route_no,(pkt_ptr +PKT_ID_LEN+ SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN),ROUTE_NO_LEN);
			//			memcpy(schedule_master_struct_var.brand_code,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN),BRAND_CODE_LEN);
			//			memcpy(schedule_master_struct_var.bus_service_id,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN),BUS_SRVC_ID_LEN);
			//			memcpy(schedule_master_struct_var.depot_code,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN),DEPOT_CODE_LEN);
			//			memcpy(schedule_master_struct_var.vehicle_no,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN),VEHICLE_NO_LEN);
			//			memcpy(&schedule_master_struct_var.shift,(pkt_ptr +SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN),SHIFT_LEN);
			//			memcpy(schedule_master_struct_var.effective_from_date,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN),DATE_LEN);
			//			memcpy(schedule_master_struct_var.effective_from_time,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN),TIME_LEN);
			//			memcpy(schedule_master_struct_var.effective_till_date,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN),DATE_LEN);
			//			memcpy(schedule_master_struct_var.effective_till_time,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN),TIME_LEN);
			//			memcpy(schedule_master_struct_var.start_date,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),DATE_LEN);
			//			memcpy(schedule_master_struct_var.start_time,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN),TIME_LEN);
			//			memcpy(schedule_master_struct_var.end_date,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),DATE_LEN);
			//			memcpy(schedule_master_struct_var.end_time,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN),TIME_LEN);
			//			memcpy(schedule_master_struct_var.start_bus_stop_code,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN),BUS_STOP_CODE_LEN);
			//			memcpy(schedule_master_struct_var.end_bus_stop_code,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+BUS_STOP_CODE_LEN),BUS_STOP_CODE_LEN);
			//			memcpy(&schedule_master_struct_var.fare_type,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+BUS_STOP_CODE_LEN+BUS_STOP_CODE_LEN),FARE_TYPE_LEN);
			//			memcpy(&schedule_master_struct_var.trip_status,(pkt_ptr +PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+BUS_STOP_CODE_LEN+BUS_STOP_CODE_LEN+FARE_TYPE_LEN),TRIP_STATUS_LEN);

			tempStrLen = pkt_data_len-PKT_ID_LEN+1; //PKT_ID_LEN+SERIAL_NO_LEN+TRIP_ID_LEN+SCHEDULE_NO_LEN+ROUTE_NO_LEN+BRAND_CODE_LEN+BUS_SRVC_ID_LEN+DEPOT_CODE_LEN+VEHICLE_NO_LEN+SHIFT_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN+BUS_STOP_CODE_LEN+BUS_STOP_CODE_LEN+FARE_TYPE_LEN+TRIP_STATUS_LEN;
			tempStr = malloc(tempStrLen*sizeof(char));
			memset(tempStr,0,tempStrLen);
			memcpy(tempStr, (pkt_ptr + PKT_ID_LEN),(pkt_data_len-PKT_ID_LEN));
			tempStr[tempStrLen]='\0';
			//			sprintf(tempStr,"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%c\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
			//					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%c\',\'%c\'",
			//					schedule_master_struct_var.serial_no,
			//					schedule_master_struct_var.trip_id,
			//					schedule_master_struct_var.schedule_no,
			//					schedule_master_struct_var.route_no,
			//					schedule_master_struct_var.brand_code,
			//					schedule_master_struct_var.bus_service_id,
			//					schedule_master_struct_var.depot_code,
			//					schedule_master_struct_var.vehicle_no,
			//					schedule_master_struct_var.shift,
			//					schedule_master_struct_var.effective_from_date,
			//					schedule_master_struct_var.effective_from_time,
			//					schedule_master_struct_var.effective_till_date,
			//					schedule_master_struct_var.effective_till_time,
			//					schedule_master_struct_var.start_date,
			//					schedule_master_struct_var.start_time,
			//					schedule_master_struct_var.end_date,
			//					schedule_master_struct_var.end_time,
			//					schedule_master_struct_var.start_bus_stop_code,
			//					schedule_master_struct_var.end_bus_stop_code,
			//					schedule_master_struct_var.fare_type,
			//					schedule_master_struct_var.trip_status);
			memcpy(tempName, "schedule_master", 15);
			CstcLog_printf("tempName %s = tempStr %s",tempName, tempStr);
			break;

		case 5:/*--------------------DUTY------------------------*/
			//			memcpy(duty_mas_packet_id_struct_var.duty_mas_pkt_id, pkt_ptr, PKT_ID_LEN);
			//			memcpy(duty_master_struct_var.duty_no,(pkt_ptr + PKT_ID_LEN),DUTY_NO_LEN);
			//			memcpy(duty_master_struct_var.schedule_no,(pkt_ptr + PKT_ID_LEN + SCHEDULE_NO_LEN),SCHEDULE_NO_LEN);

			tempStrLen = pkt_data_len-PKT_ID_LEN+1; //PKT_ID_LEN + SCHEDULE_NO_LEN+SCHEDULE_NO_LEN;
			tempStr = malloc(tempStrLen*sizeof(char));
			memset(tempStr,0,tempStrLen);
			memcpy(tempStr, (pkt_ptr + PKT_ID_LEN),(pkt_data_len-PKT_ID_LEN));
			tempStr[tempStrLen]='\0';
			//		sprintf(tempStr,"\'%s\',\'%s\'",duty_master_struct_var.duty_no,	duty_master_struct_var.schedule_no);
			memcpy(tempName, "duty_master",11);
			CstcLog_printf("tempName %s = tempStr %s",tempName, tempStr);
			break;

		case 7: /*-------------------ROUTE------------------------*/
			//			memcpy(route_mas_packet_id_struct_var.route_mas_pkt_id, pkt_ptr, PKT_ID_LEN);
			//			memcpy(route_master_struct_var.route_no,(pkt_ptr + PKT_ID_LEN),ROUTE_NO_LEN);
			//			memcpy(route_master_struct_var.bus_stop_code,(pkt_ptr + PKT_ID_LEN+ROUTE_NO_LEN),BUS_STOP_CODE_LEN);
			//			memcpy(route_master_struct_var.bus_stop_seq_no,(pkt_ptr + PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN),BUS_STOP_SEQ_LEN);
			//			memcpy(route_master_struct_var.bus_stop_name_english,(pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN),NAME_ENG_LEN);
			//			memcpy(route_master_struct_var.bus_stop_name_kanadda,(pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN),NAME_KAN_LEN);
			//			memcpy(route_master_struct_var.bus_stop_alias_english,(pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN),BUS_STOP_ALIAS_ENG_LEN);
			//			memcpy(route_master_struct_var.sub_stage,(pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_ALIAS_ENG_LEN), SUB_STAGE_LEN);
			//			memcpy(route_master_struct_var.fare_change_point,(pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+SUB_STAGE_LEN+BUS_STOP_ALIAS_ENG_LEN+SUB_STAGE_LEN),FARE_STG_PT_LEN);
			//			memcpy(route_master_struct_var.distance,(pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_ALIAS_ENG_LEN+SUB_STAGE_LEN+FARE_STG_PT_LEN),DISTANCE_LEN);
			//			memcpy(&route_master_struct_var.fare_stage,(pkt_ptr +PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_ALIAS_ENG_LEN+SUB_STAGE_LEN+FARE_STG_PT_LEN+DISTANCE_LEN), FARE_STAGE_LEN);

			tempStrLen = pkt_data_len-PKT_ID_LEN+1; //PKT_ID_LEN+ROUTE_NO_LEN+BUS_STOP_CODE_LEN+BUS_STOP_SEQ_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_ALIAS_ENG_LEN+SUB_STAGE_LEN+FARE_STG_PT_LEN+DISTANCE_LEN+ FARE_STAGE_LEN;
			tempStr = malloc(tempStrLen*sizeof(char));
			memset(tempStr,0,tempStrLen);
			memcpy(tempStr, (pkt_ptr + PKT_ID_LEN),(pkt_data_len-PKT_ID_LEN));
			tempStr[tempStrLen]='\0';
			//			sprintf(tempStr,"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%c\'",
			//					route_master_struct_var.route_no,
			//					route_master_struct_var.bus_stop_code,
			//					route_master_struct_var.bus_stop_seq_no,
			//					route_master_struct_var.bus_stop_name_english,
			//					route_master_struct_var.bus_stop_name_kanadda,
			//					route_master_struct_var.bus_stop_alias_english,
			//					route_master_struct_var.sub_stage,
			//					route_master_struct_var.fare_change_point,
			//					route_master_struct_var.distance,
			//					route_master_struct_var.fare_stage);
			memcpy(tempName, "route_master",12);
			CstcLog_printf("tempName %s = tempStr %s",tempName, tempStr);
			break;

		case 9:/*--------------------FARE------------------------*/
			//			memcpy(fare_chart_packet_id_struct_var.fare_chart_pkt_id,pkt_ptr, PKT_ID_LEN);
			//			memcpy(fare_chart_struct_var.version_id,(pkt_ptr + PKT_ID_LEN),VERSION_NO_LEN);
			//			memcpy(fare_chart_struct_var.route_no,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN),ROUTE_NO_LEN);
			//			memcpy(fare_chart_struct_var.bus_service_id,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN),BUS_SRVC_ID_LEN);
			//			memcpy(fare_chart_struct_var.from_stop_seq_no,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN),BUS_STOP_SEQ_LEN);
			//			memcpy(fare_chart_struct_var.from_bus_stop_code,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN),BUS_STOP_CODE_LEN);
			//			memcpy(fare_chart_struct_var.from_bus_stop_english,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN),NAME_ENG_LEN);
			//			memcpy(fare_chart_struct_var.from_bus_stop_bangla,(pkt_ptr +PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN),NAME_KAN_LEN);
			//			memcpy(fare_chart_struct_var.till_stop_seq_no,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN),BUS_STOP_SEQ_LEN);
			//			memcpy(fare_chart_struct_var.till_bus_stop_code,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN),BUS_STOP_CODE_LEN);
			//			memcpy(fare_chart_struct_var.till_bus_stop_english,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN),NAME_ENG_LEN);
			//			memcpy(fare_chart_struct_var.till_bus_stop_bangla,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN),NAME_KAN_LEN);
			//			memcpy(fare_chart_struct_var.travelled_km,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN),TRAVELLED_KM_LEN);
			//			memcpy(fare_chart_struct_var.adult_fare,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN),ADULT_FARE_LEN);
			//			memcpy(fare_chart_struct_var.child_fare,(pkt_ptr +PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN),CHILD_FARE_LEN);
			//			memcpy(fare_chart_struct_var.sr_citizen_fare,(pkt_ptr +PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN),SR_CIT_FARE_LEN);
			//			memcpy(fare_chart_struct_var.luggage_amt_per_unit,(pkt_ptr+PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN),LUGG_AMT_LEN);
			//			memcpy(fare_chart_struct_var.toll_amt,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN),AMOUNT_LEN);
			//			memcpy(&fare_chart_struct_var.factor_type,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN), FACTOR_TYPE_LEN);
			//			memcpy(&fare_chart_struct_var.fare_type,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN), FARE_TYPE_LEN);
			//			memcpy(fare_chart_struct_var.effective_from_date,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN),DATE_LEN);
			//			memcpy(fare_chart_struct_var.effective_from_time,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN+DATE_LEN),TIME_LEN);
			//			memcpy(fare_chart_struct_var.effective_till_date,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN+DATE_LEN+TIME_LEN),DATE_LEN);
			//			memcpy(fare_chart_struct_var.effective_till_time,(pkt_ptr + PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN+DATE_LEN+TIME_LEN+DATE_LEN),TIME_LEN);

			tempStrLen = pkt_data_len-PKT_ID_LEN+1; //PKT_ID_LEN+VERSION_NO_LEN+BUS_SRVC_ID_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+BUS_STOP_SEQ_LEN+BUS_STOP_CODE_LEN+NAME_ENG_LEN+NAME_KAN_LEN+TRAVELLED_KM_LEN+ADULT_FARE_LEN+CHILD_FARE_LEN+SR_CIT_FARE_LEN+LUGG_AMT_LEN+AMOUNT_LEN+FACTOR_TYPE_LEN+FARE_TYPE_LEN+DATE_LEN+TIME_LEN+DATE_LEN+TIME_LEN;
			tempStr = malloc(tempStrLen*sizeof(char));
			memset(tempStr,0,tempStrLen);
			memcpy(tempStr, (pkt_ptr + PKT_ID_LEN),(pkt_data_len-PKT_ID_LEN));
			tempStr[tempStrLen]='\0';

			//			sprintf(tempStr,"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
			//					"\'%s\',\'%s\',\'%s\',\'%c\',\'%c\',\'%s\',\'%s\',\'%s\',\'%s\'",
			//					fare_chart_struct_var.version_id,
			//					fare_chart_struct_var.route_no,
			//					fare_chart_struct_var.bus_service_id,
			//					fare_chart_struct_var.from_stop_seq_no,
			//					fare_chart_struct_var.from_bus_stop_code,
			//					fare_chart_struct_var.from_bus_stop_english,
			//					fare_chart_struct_var.from_bus_stop_bangla,
			//					fare_chart_struct_var.till_stop_seq_no,
			//					fare_chart_struct_var.till_bus_stop_code,
			//					fare_chart_struct_var.till_bus_stop_english,
			//					fare_chart_struct_var.till_bus_stop_bangla,
			//					fare_chart_struct_var.travelled_km,
			//					fare_chart_struct_var.adult_fare,
			//					fare_chart_struct_var.child_fare,
			//					fare_chart_struct_var.sr_citizen_fare,
			//					fare_chart_struct_var.luggage_amt_per_unit,
			//					fare_chart_struct_var.toll_amt,
			//					fare_chart_struct_var.factor_type,
			//					fare_chart_struct_var.fare_type,
			//					fare_chart_struct_var.effective_from_date,
			//					fare_chart_struct_var.effective_from_time,
			//					fare_chart_struct_var.effective_till_date,
			//					fare_chart_struct_var.effective_till_time);
			memcpy(tempName, "fare_chart",10);
			CstcLog_printf("tempName %s = tempStr %s",tempName, tempStr);
			break;
		case 54:/*--------------------GET ETIM NO------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				return 0;
			}
			else
			{
				memcpy(registration_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(registration_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));
				//memset(tempStr,0,1024);///Add file read operation for MAC number as well as ETIM NO.
				////For writing we can use script may be  we can try write from here too
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen("/home/user1/CSTC/mac_ID.txt","r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);

				CstcLog_printf("mac_address  %s ", registration_response_struct_var.mac_address);
				if(strcmp(registration_response_struct_var.mac_address,mac_ID)==0) //"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", registration_response_struct_var.mac_address);
					if(strcmp(registration_response_struct_var.machine_no,sn)==0)
					{
						CstcLog_printf("machine_no  %s verified", registration_response_struct_var.machine_no);
						return 1;
					}
					else
						return 0;
				}
				else
					return 0;
			}
			break;
		case 56:/*--------------------ETIM NO REG------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));
				//			memset(tempStr,0,1024);
				//			strcpy(tempStr,"./etim_no W ");
				memcpy(registration_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(registration_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(registration_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				CstcLog_printf("mac_address  %s ", registration_response_struct_var.mac_address);

				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen("/home/user1/CSTC/mac_ID.txt","r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);

				if(strcmp(registration_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", registration_response_struct_var.mac_address);
					if(strcmp(registration_response_struct_var.machine_no,sn)==0)
					{
						CstcLog_printf("machine_no  %s verified", registration_response_struct_var.machine_no);
						tempFile= fopen("/home/user1/CSTC/etim_no.txt","w");
						if(tempFile == NULL)
						{
							CstcLog_printf("ERROR OPENING FILE etim_no.txt");
							return 0;
						}
						fputs(registration_response_struct_var.terminal_id,tempFile);
						fflush(tempFile);
						CstcLog_printf("terminal_id :- %s", registration_response_struct_var.terminal_id);
						fclose(tempFile);
						//					memcpy(tempStr+strlen("./etim_no W "),registration_response_struct_var.terminal_id, TERMINAL_ID_LEN);
						//					CstcLog_printf("system cmd  %s ", tempStr);
						//					if(system(tempStr)>0)
						return 1;
						//					else
						//						return 0;
					}
					else
						return 0;
				}
				else
					return 0;
			}
			break;
		default:
			return 0;
			break;
		}

		if(!(DbInterface_Insert_In_All_Tables1(tempName, tempStr, (pkt_data_len-PKT_ID_LEN))))
		{
			CstcLog_printf("Inserted in table \'%s\' failed", tempName);
			return 0;
		}
		else
		{
			CstcLog_printf("Insertion in table \'%s\' success", tempName);
			return 1;
		}
	}
	return 0;
}

void get_machine_info(char * sn, int nbytes)
{
	CstcLog_printf("In get_machine_info");
	sys_get_sn(sn, nbytes);
	CstcLog_printf("%s", sn);

}

int check_master_status()
{
	//	char ticket_fare, download_ticket, machine_register, upload_master, erase_ticket, start_duty, insert_waybill;

	//	int exit = 0;
	int checkFlag = 0;
	//    char key = 0;
	int resetonce = 0;

	//	int  downloadTicketFlag = 0, machineRegisterFlag = 0, uploadMasterFlag = 0, eraseTicketFlag = 0, startDutyFlag = 0, insertWaybillFlag = 0;

	//	DbInterface_Get_Master_Status_Data(&ticket_fare, &download_ticket, &machine_register, &upload_master, &erase_ticket, &start_duty, &insert_waybill);

	//	if((master_status_struct_var.ticket_fare == '2') && (master_status_struct_var.download_ticket == '0') &&
	//       (master_status_struct_var.machine_register == '2') && (master_status_struct_var.upload_master == '2') &&
	//       (master_status_struct_var.erase_ticket == '0') && (master_status_struct_var.start_duty == '2') &&
	//       (master_status_struct_var.insert_waybill == '2'))
	//	{
	//		CstcLog_printf("ALL MASTERS PRESENT");
	//		strcat(lan_Err_Msg,"ALL MASTERS PRESENT");
	//		lan_Err_Len =lan_Err_Len+strlen("ALL MASTERS PRESENT");
	//		CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
	//		return 1;
	//	}
	//	else if((master_status_struct_var.ticket_fare == '2') && (master_status_struct_var.download_ticket == '2') &&
	//	   (master_status_struct_var.machine_register == '2') && (master_status_struct_var.upload_master == '2') &&
	//	   (master_status_struct_var.erase_ticket == '0') && (master_status_struct_var.start_duty == '2') &&
	//       (master_status_struct_var.insert_waybill == '2'))
	//	{
	//		CstcLog_printf("Data Download Complete");
	//		strcat(lan_Err_Msg,"Data Download Complete");
	//		lan_Err_Len =lan_Err_Len+strlen("Data Download Complete");
	//		CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
	//		return 2;
	//	}
	//	else
	{
		if(master_status_struct_var.machine_register == 0)
		{
			checkFlag = 1;
			Show_Error_Msg("    MACHINE NOT    \n    REGISTERED     ");
			//                goto tryagain;
			return 0;
		}
		if(master_status_struct_var.machine_register == 1 /* && (!machineRegisterFlag)*/)
		{
			Show_Error_Msg("   REGISTERING ETM  \n    PLEASE WAIT..!   ");
			master_status_struct_var.machineRegisterFlag = 1;
			sleep(1);
			//                goto tryagain;
			return 0;
		}
		if((master_status_struct_var.machine_register == 2) && (master_status_struct_var.machineRegisterFlag))
		{
			Show_Error_Msg("   REGISTERING ETM  \n       COMPLETE     ");
			master_status_struct_var.machineRegisterFlag = 0;
			beep(2800,1500);
			sleep(1);
			//                goto tryagain;
			return 0;
		}

		//insert waybill
		if(master_status_struct_var.insert_waybill == 0)
		{
			checkFlag = 1;
			Show_Error_Msg(" NO WAYBILL IN ETM ");
			//                goto tryagain;
			return 0;
		}
		if(master_status_struct_var.insert_waybill == 1/* && (!insertWaybillFlag)*/)
		{
			Show_Error_Msg("  INSERTING WAYBILL \n     PLEASE WAIT!   ");
			master_status_struct_var.insertWaybillFlag = 1;
			sleep(1);
			//                goto tryagain;
			return 0;
		}
		if((master_status_struct_var.insert_waybill == 2) && (master_status_struct_var.insertWaybillFlag))
		{
			Show_Error_Msg("NEW WAYBILL RECEIVED");
			master_status_struct_var.insertWaybillFlag = 0;
			beep(2800,1500);
			sleep(1);
			if(!resetonce){
				get_waybill_details();
				resetonce =1;
			}
			//			goto tryagain;
			return 0;
		}

		//upload master
		if(master_status_struct_var.upload_master == 0)
		{
			checkFlag = 1;
			Show_Error_Msg("   NO MASTER DATA   ");
			//                goto tryagain;
			return 0;
		}
		if(master_status_struct_var.upload_master == 1 /*&& (!uploadMasterFlag)*/)
		{
			Show_Error_Msg("  UPLOADING MASTER  \n     PLEASE WAIT!   ");
			master_status_struct_var.uploadMasterFlag = 1;
			sleep(1);
			//                goto tryagain;
			return 0;
		}
		if((master_status_struct_var.upload_master == 2) && (master_status_struct_var.uploadMasterFlag))
		{
			Show_Error_Msg("  UPLOADING MASTER  \n       COMPLETE     ");
			master_status_struct_var.uploadMasterFlag = 0;
			beep(2800,1500);
			sleep(1);

			//                goto tryagain;
			return 0;
		}
		if(master_status_struct_var.ticket_fare == 0)
		{
			checkFlag = 1;
			Show_Error_Msg("   NO FARE CHART DATA   ");
			//                goto tryagain;
			return 0;
		}
		if(master_status_struct_var.ticket_fare == 1 /*&& (!uploadMasterFlag)*/)
		{
			Show_Error_Msg("  UPLOADING FARE CHART  \n     PLEASE WAIT!   ");
			master_status_struct_var.ticketFareFlag = 1;
			sleep(1);
			//                goto tryagain;
			return 0;
		}
		if((master_status_struct_var.ticket_fare == 2) && (master_status_struct_var.ticketFareFlag))
		{
			Show_Error_Msg("  UPLOADING FARE CHART  \n       COMPLETE     ");
			master_status_struct_var.ticketFareFlag = 0;
			beep(2800,1500);
			sleep(1);

			//                goto tryagain;
			return 0;
		}
		if(master_status_struct_var.start_duty == 0)
		{
			checkFlag = 1;
			Show_Error_Msg("  DUTY NOT STARTED  ");
			//                goto tryagain;
			return 0;
		}
		if(master_status_struct_var.start_duty == 1/* check_master_status&& (!startDutyFlag)*/)
		{

			Show_Error_Msg("   ALLOCATING DUTY  \n     PLEASE WAIT    ");
			master_status_struct_var.startDutyFlag = 1;
			sleep(1);
			//                goto tryagain;
			return 0;
		}
		if((master_status_struct_var.start_duty == 2) && (master_status_struct_var.startDutyFlag))
		{
			Show_Error_Msg("  DUTY ALLOCATION  \n     COMPLETE      ");
			master_status_struct_var.startDutyFlag = 0;
			beep(2800,1500);
			sleep(1);

			//                goto tryagain;
			return 1;
		}
		if(master_status_struct_var.download_ticket == 0)
		{
			checkFlag = 1;
			Show_Error_Msg("  DOWNLOAD DATA  ");
			//			sleep(1);
			//                goto tryagain;
			return 0;
		}

		if((master_status_struct_var.download_ticket == 1) /*&& (!master_status_struct_var.downloadTicketFlag)*/)
		{
			Show_Error_Msg("  DOWNLOADING DATA  \n    PLEASE WAIT     ");
			master_status_struct_var.downloadTicketFlag = 1;
			sleep(1);
			//                goto tryagain;
			return 0;
		}
		if((master_status_struct_var.download_ticket == 2) && (master_status_struct_var.downloadTicketFlag))
		{

			Show_Error_Msg("  DOWNLOADING DATA  \n      COMPLETE      ");
			master_status_struct_var.downloadTicketFlag = 0;
			sleep(1);
			beep(2800,1500);
			//                goto tryagain;
			return 1;
			//			return 0;
		}

		if(master_status_struct_var.erase_ticket == 0)
		{
			checkFlag = 1;
			Show_Error_Msg("  ERASE DATA  ");
			//			sleep(1);
			//                goto tryagain;
			return 0;
		}
		if((master_status_struct_var.erase_ticket == 1) /*&& (!master_status_struct_var.eraseTicketFlag)*/)
		{
			Show_Error_Msg("    ERASING DATA    \n    PLEASE WAIT...!   ");
			master_status_struct_var.eraseTicketFlag = 1;
			resetonce =0;
			sleep(1);
			//                goto tryagain;
			return 0;
		}
		if((master_status_struct_var.erase_ticket == 2) && (master_status_struct_var.eraseTicketFlag))
		{
			Show_Error_Msg("     ERASING DATA   \n       COMPLETE     ");
			master_status_struct_var.eraseTicketFlag = 0;
			beep(2800,1500);
			resetonce =0;
			DbInterface_Delete_Table("Delete from master_status");
			if(lan_gUpdateDataFlag)
				DbInterface_Insert_In_All_Tables1("master_status","'0','0','2','0','0','0','0'",27);
			else
				DbInterface_Insert_In_All_Tables1("master_status","'2','0','2','2','0','0','0'",27);

			sleep(1);

			//                goto tryagain;
			return 1;
		}

		//		if( (download_ticket == '0') && (machine_register == '2') && (upload_master == '2') && (erase_ticket == '0') && (insert_waybill == '2')&& (start_duty == '2'))
		//		{
		//			CstcLog_printf("ALL MASTERS PRESENT DOWN");
		//			strcat(lan_Err_Msg,"ALL MASTERS PRESENT DOWN");
		//			lan_Err_Len =lan_Err_Len+strlen("ALL MASTERS PRESENT DOWN");
		//			CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
		//			exit = 1;
		//			return 1;
		//		}
		//		if( (download_ticket == '2') && (machine_register == '2') && (upload_master == '2') && (erase_ticket == '0') && (insert_waybill == '2')&& (start_duty == '2'))
		//		{
		//			CstcLog_printf("Data Download Complete DOWN");
		//			strcat(lan_Err_Msg,"Data Download Complete DOWN");
		//			lan_Err_Len =lan_Err_Len+strlen("Data Download Complete DOWN");
		//			CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len ,lan_Err_Msg);
		//			exit = 1;
		//			return 2;
		//		}
		//		if( (download_ticket == '2') && (machine_register == '2') && (upload_master == '2') && (erase_ticket == '2') && (insert_waybill == '2')&& (start_duty == '2'))
		//		{
		//			CstcLog_printf("Machine erased completely DOWN");
		//			strcat(lan_Err_Msg,"Machine erased completely DOWN");
		//			lan_Err_Len =lan_Err_Len+strlen("Machine erased completely DOWN");
		//			CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len ,lan_Err_Msg);
		//			exit = 1;
		//			return 0;
		//		}

		//            get_waybill_details();
		//            get_waybill_wise_info();
		//            get_service_op_details();
		//            check_collection_status();

	}
	//    }
	return 0;
}


int packet_parser_input_master(char tempCh,int recvPktID,int updateDataFlag)
{
	//	char *mac_ID = NULL;
	//	char *etim_no=NULL;
	//FILE* dbTblDataFile= NULL;
	//FILE* dbDataFile= NULL;
	//	int temp = 0;
	//	int len = 0,pkt_data_len=0,recLen=0,notFirstRecFlag=0;
	char tempName[100]={'\0'};
	char tempStr[500] = {'\0'}; //[1024];
	char sysCmdStr[100] = {'\0'};
	int tempStrLen=0,dbretval=0,retrycnt =0,count=0;
	//	char *sn = NULL;
	char Err_MsgAry[3]={'\0'};

	if(tempCh == ';')
	{
#if 0
		dbDataFile = NULL;
		dbDataFile= fopen(DB_INPUT_FILE_ADDR,"r+");
		if(dbDataFile == NULL)
		{
			CstcLog_printf("Parser ERROR OPENING FILE db_input.txt");
			memset(Err_MsgAry,0,3);
			sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_db_input);
			strcat(lan_Err_Msg,Err_MsgAry);
			lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
			CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
			memset(Err_MsgAry,0,3);

			//			strcat(lan_Err_Msg,"ERROR OPENING FILE db_input.txt");
			//			lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE db_input.txt");
			//			CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
			if(tempStr!=NULL)
			{
				free(tempStr);
				tempStr=NULL;
			}
			return 0;
		}
		else
		{
			CstcLog_printf("Parser Tempfile open success :%d",dbDataFile);
			//				strcat(lan_Err_Msg,"Tempfile open success");
			//				lan_Err_Len=lan_Err_Len+strlen("Tempfile open success");
			//				CstcLog_printf("lan_Err_Len =%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
		}
#endif

		//			fputs(bTemp+7,dbDataFile);
		//			fprintf(dbDataFile,"%s",bTemp);
		CstcLog_printf("recvPktID %d updateDataFlag %d",recvPktID,updateDataFlag);
		memset(tempName,0,100);

		switch(recvPktID)
		{
		case 1 :/*-------------------WAYBILL----------------------*/
			//			tempStrLen = sizeof(waybill_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "waybill_master",14);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from waybill_master"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;


		case 3: /*-----------------SCHEDULE-----------------------*/
			count=duty_master_schedule_check(lan_global_schedule);
			CstcLog_printf("duty_master_schedule_check lan_global_schedule = %s ",lan_global_schedule);
			if(count > 0)
			{
				CstcLog_printf("Schedule is same..........");
			}
			else
			{
				DbInterface_Update_Master_Status_Data("update master_status set upload_master ='0'");
				DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='0'");
			}

			//			tempStrLen = sizeof(schedule_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "schedule_master", 15);
			if(updateDataFlag==1)
			{
				//				DbInterface_Update_Master_Status_Data("update master_status set upload_master ='1'");
				//				DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='1'");
				if(!DbInterface_Delete_Table_active_schedule("Delete from schedule_master where schedule_no = ",lan_global_schedule))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				if(!DbInterface_Delete_Table("Delete from schedule_master"))
				//					return 0;
#if MULTIPLE_SCHEDULE_ENABLE
				//	if(!DbInterface_Delete_Table("Delete from route_master where "))
				if(!DbInterface_Delete_Table_active_schedule("Delete from route_master where schedule_no = ",lan_global_schedule))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart where schedule_no = ",lan_global_schedule))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				if(!DbInterface_Delete_Table_active_schedule("Delete from route_header where schedule_no = ",lan_global_schedule))
					//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				{
					//					DbInterface_Delete_Table("ROLLBACK;");
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
				//				else
				//					DbInterface_Delete_Table("COMMIT;");
				DbInterface_Update_Master_Status_Data("update master_status set upload_master ='1'");
				DbInterface_Update_Master_Status_Data("update master_status set ticket_fare ='1'");

#endif

			}
			break;

		case 5:/*--------------------DUTY------------------------*/
			//			tempStrLen = sizeof(duty_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "duty_master",11);
			if(updateDataFlag==1)
			{
				insert_duty_master_in_restore();
				if(!DbInterface_Delete_Table("Delete from duty_master"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 7: /*-------------------ROUTE------------------------*/
			//			tempStrLen = sizeof(route_master_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "route_master",12);
			if(updateDataFlag==1)
			{
				//if(!DbInterface_Delete_Table("Delete from route_master"))
				if(!DbInterface_Delete_Table_active_schedule("Delete from route_master where schedule_no = ",lan_global_schedule))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 8:
		case 9:/*--------------------FARE------------------------*/
			//tempStrLen =sizeof(fare_chart_struct)+40;
			//			tempStrLen =sizeof(fare_chart_struct)+(VERSION_NO_LEN+1)+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)
			//			+(BUS_SRVC_ID_LEN+1)+(BUS_STOP_SEQ_LEN+1)+(BUS_STOP_CODE_LEN+1)
			//			+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)+(BUS_STOP_SEQ_LEN+1)
			//			+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)
			//			+1+(DATE_LEN+1)+(TIME_LEN+1)+(DATE_LEN+1)+(TIME_LEN+1)+1+1+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "fare_chart",10);
			if(updateDataFlag==1)
			{
				//if(!DbInterface_Delete_Table("Delete from fare_chart"))
				if(!DbInterface_Delete_Table_active_schedule("Delete from fare_chart where schedule_no = ",lan_global_schedule))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 10:/*-----------------Ticket Type-------------------------*/
			//			tempStrLen = sizeof(ticket_type_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "ticket_type",11);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from ticket_type"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 11:/*---------------------Ticket sub Type------------------------*/
			//			tempStrLen = sizeof(ticket_sub_type_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "ticket_sub_type",15);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from ticket_sub_type"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 12:/*--------------------Bus Services------------------------------*/
			//			tempStrLen = sizeof(bus_service_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "bus_service",11);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from bus_service"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 13:/*----------------------Bus Brand-------------------------------*/
			//			tempStrLen = sizeof(bus_brand_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "bus_brand",9);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from bus_brand"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 14:/*----------------------Depot-------------------------------*/
			CstcLog_printf("Inside Depot Parser function");
			//			tempStrLen = sizeof(depot_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "depot",5);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from depot"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 15:/*-----------------Route Header------------------------*/
			//			tempStrLen = sizeof(route_header_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "route_header",12);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from route_header"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 16:/*--------------------ETM RW key---------------------------*/
			//			tempStrLen = sizeof(etim_rw_keys_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "etim_rw_keys",13);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from etim_rw_keys"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 17:/*--------------------TRUNK AND FEEDER---------------------------*/
			//tempStrLen = sizeof(trunk_feeder_master_struct)+40;

			//			tempStrLen = 2+1+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)+(ROUTE_NO_LEN+1)+(ROUTE_ID_LEN+1)
			//			+(BUS_STOP_SEQ_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)+(BUS_STOP_CODE_LEN+1)+(NAME_ENG_LEN+1)
			//			+(BUS_STOP_CODE_LEN+1)+(NAME_KAN_LEN+1)+(BUS_STOP_ID_LEN+1)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "trunk_feeder_master",19);
			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from trunk_feeder_master"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;

		case 18:/*-------------------SERVICE OP-----------------------*/
			//			tempStrLen = sizeof(service_op_struct)+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "service_op",10);

			//			tempStrLen = 3+(NAME_ENG_LEN+1)+(NAME_KAN_LEN+1)+1+40;
			//			tempStr = malloc((tempStrLen+1));
			//			memcpy(tempName, "service_op",10);

			if(updateDataFlag==1)
			{
				if(!DbInterface_Delete_Table("Delete from service_op"))
				{
					//					if(tempStr!=NULL)
					//					{
					//						free(tempStr);
					//						tempStr=NULL;
					//					}
					return 0;
				}
			}
			break;


		default:
			return 0;
			break;
		}
		CstcLog_printf("reading file tempstr allocated with required memory of length = %d",(tempStrLen+1));
		//			do
		//			{
		//				fread(dbDataFile,"%c",tempStr);
		//			}while(bTemp!=EOF);
#if 0
		dbTblDataFile = NULL;
		dbTblDataFile= fopen(DB_TBL_INPUT_FILE_ADDR,"w+");
		if(dbTblDataFile == NULL)
		{
			CstcLog_printf("Parser ERROR OPENING FILE db_tbl_input.txt");
			memset(Err_MsgAry,0,3);
			sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_db_tbl_input);
			strcat(lan_Err_Msg,Err_MsgAry);
			lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
			CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
			memset(Err_MsgAry,0,3);

			//			strcat(lan_Err_Msg,"ERROR OPENING FILE db_tbl_input.txt");
			//			lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE db_tbl_input.txt");
			//			CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
			if(tempStr!=NULL)
			{
				free(tempStr);
				tempStr=NULL;
			}
			return 0;
		}
		else
		{
			CstcLog_printf("Parser dbTblDataFile open success :%d",dbTblDataFile);
			//				strcat(lan_Err_Msg,"Tempfile open success");
			//				lan_Err_Len=lan_Err_Len+strlen("Tempfile open success");
			//				CstcLog_printf("lan_Err_Len =%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
		}
		fprintf(dbTblDataFile,"%s","Insert into ");
		fprintf(dbTblDataFile,"%s",tempName);
		fprintf(dbTblDataFile,"%s"," select ");

		recLen = 0;
		memset(tempStr,0,(tempStrLen+1));
		CstcLog_printf("reading file recvPktID %d ",recvPktID);


		while ( ( ch = fgetc(dbDataFile) ) != EOF ) {
			if(ch=='$')//||(ch=='|'))
			{
				recLen = 0;
				memset(tempStr,0,(tempStrLen+1));
				notFirstRecFlag=0;
				//				memset(tempName,0,100);
				CstcLog_printf("%d tempStr is cleared",recLen);
			}
			else if((ch!='|')&&(ch!='@')&&(ch!='$'))
			{
				tempStr[recLen]= ch;
				recLen++;
				//				CstcLog_printf("%d tempStr %s",recLen ,tempStr);
			}
			else if((ch=='|')||(ch=='@'))
			{
				if(recLen>0)
				{
					CstcLog_printf("%d tempStr %s",recLen, tempStr);
					if(ch=='@')
					{
						//						if(recEndFlag==1)
						//							recLen=recLen-40-strlen(" union all select ");
						//						else
						recLen=recLen-40;
						tempStr[recLen]='\0';
						fprintf(dbTblDataFile,"%s;",tempStr);
					}
					else
					{
						tempStr[recLen]='\0';
						if(notFirstRecFlag)
							fprintf(dbTblDataFile," union all select %s",tempStr);
						else
							fprintf(dbTblDataFile,"%s",tempStr);
						notFirstRecFlag=1;
					}
					CstcLog_printf("%d recvPktID %d = tempStr %s",recLen ,recvPktID, tempStr);
					switch(recvPktID)
					{
					case 8:/*--------------------FARE RECORD LENGTH------------------------*/
						//						memcpy(tempName, "fare_chart",10);
						CstcLog_printf("tempName %s = tempStr %s = recLen %d",tempName, tempStr,recLen);
						RecordCntToRecv = atoi(tempStr);
						CstcLog_printf("RecordCntToRecv %d = int(tempStr) %d ",RecordCntToRecv, atoi(tempStr));
						lan_RecordCntToRecv=0;
						break;

					case 9:/*--------------------FARE------------------------*/
						//						memcpy(tempName, "fare_chart",10);
						CstcLog_printf("lan_RecordCntToRecv =%d :: tempName %s = tempStr %s = recLen %d",lan_RecordCntToRecv,tempName, tempStr,recLen);
						if(recLen>0)
							lan_RecordCntToRecv++;
						CstcLog_printf("Incr lan_RecordCntToRecv = %d ",lan_RecordCntToRecv);
						break;
					default:
						//return 0;
						break;
					}
					if(ch=='@')
						break;
				}
				recLen=0;
				memset(tempStr,0,(tempStrLen+1));
				//				memset(tempName,0,100);
				dbretval=0;
			}
		}

		fflush(dbTblDataFile);
		CstcLog_printf("dbTblDataFile is closing" );
		fclose(dbTblDataFile);
		dbTblDataFile = NULL;*/
		CstcLog_printf("dbTblDataFile close success" );
#endif

		if(recvPktID!=8)
		{
			//			sleep(5);
			memset(tempStr,0,500);
			sprintf(tempStr,"chmod -R 777 %s",DB_INPUT_FILE_ADDR);
			dbretval=system(tempStr);
			CstcLog_printf("System chmod command retval \"%d\" ", dbretval);
			//memset(tempStr,0,500);
			memset(sysCmdStr,0,100);
			sprintf(sysCmdStr,"sqlite3 %s < %s",DB_ADDR,DB_INPUT_FILE_ADDR);
			//sprintf(tempStr,"sqlite3 %s < %s",DB_ADDR,DB_INPUT_FILE_ADDR);
			//						memcpy(tempStr,"sqlite3 ",strlen("sqlite3 "));
			//						memcpy(tempStr+strlen("sqlite3 "),DB_ADDR,strlen(DB_ADDR));
			//						memcpy(tempStr+strlen("sqlite3 ")+strlen(DB_ADDR),"sqlite3 ",strlen("sqlite3 "));
			CstcLog_printf("Insertion in table command \"%s\" failed", tempStr);

			/*-------------------Sudhakar deer------------------------------------------*/
			if(recvPktID == 9)
			{
				sleep(4);
			}
			else
			{
				sleep(1);
			}


			// while(!system(NULL)){
			//				 CstcLog_printf("Shell is unavailable");
			//				  sleep(1);
			//			 }
			/*-------------------Sudhakar deer------------------------------------------*/

			//			sprintf(lan_Err_Msg,"%02d",SQLITE_DB_INSERTION_FAILED);
			//			strcat(Err_Msg1,lan_Err_Msg);
			//			lan_Err_Len=lan_Err_Len+strlen(Err_Msg1);
			//			CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_Msg1);

			dbretval=0;
			dbretval=system(sysCmdStr);//DbInterface_Insert_In_All_Tables1(tempName, tempStr,recLen);
			retrycnt =0;
			//			while(retrycnt<5)
			//			{
			//				dbretval=system(tempStr);
			CstcLog_printf("System command retval \"%d\" ", dbretval);
			//				if(dbretval==0)
			//					break;
			//				retrycnt++;
			//
			//			}
			if(dbretval == 0)
			{
				CstcLog_printf("Insertion in table  success");

				sprintf(lan_Err_Msg,"%02d",SUCCESS);

				lan_Err_Len=lan_Err_Len+2;//strlen(lan_Err_Msg);
				CstcLog_printf("lan_Err_Msg = %s lan_Err_Len = %d",lan_Err_Msg,lan_Err_Len);
				lan_Err_Msg[lan_Err_Len]='\0';
				memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));            /*May be need to change...*/
				lan_Err_Len=0;
				//memcpy(lan_Err_Msg,"Data Insertion successful",25);
				//				strcat(lan_Err_Msg,"Data Insertion successful");
				//				lan_Err_Len =lan_Err_Len+strlen("Data Insertion successful");
				//				CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len, lan_Err_Msg);
				if(recvPktID == 5)
				{
					DbInterface_Delete_Table("Delete from duty_master_restore");
				}
				return 1;

			}
			else
			{
				CstcLog_printf("Insertion in table  failed");
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",SQLITE_DB_INSERTION_FAILED);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				//				strcat(lan_Err_Msg,"Data Insertion in table failed");
				//				lan_Err_Len =lan_Err_Len+strlen("Data Insertion in table failed");
				//				CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);

				//							break;
				if(recvPktID == 5)
				{
					reinsert_duty_master_restore_in_duty_master();
				}
				return 0;
			}
		}

	}
	else
	{
		CstcLog_printf("Parsing Fail Resend Packet");
		sprintf(Err_MsgAry,"%02d",RESEND_PACKETS);
		strcat(lan_Err_Msg,Err_MsgAry);
		lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
		CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);

		return 0;
	}

	return 1;
}

int packet_parser_nresponder(char * bTemp)
{
	char * pkt_ptr;
	//	char *mac_ID = NULL;
	char *etim_no=NULL;
	FILE* tempFile= NULL;
	FILE* tempFile2= NULL;
	int temp = 0;
	char recvPktID[3]={'\0'};
	int len = 0,pkt_data_len=0;
	//	char tempName[100]={'\0'};
	//	char *tempStr; //[1024];
	//	int tempStrLen=0;
	char *sn = NULL;
	char Err_MsgAry[3]={'\0'};

	//	int update_data_flag=0;
	if(bTemp[0] == ';')
	{
		CstcLog_printf("Found start indicator......");
		memset_packet_format();
		memcpy(&(packet_format_struct_var.start_indicator), bTemp,START_INDICATOR_LEN);
		CstcLog_printf("1......");
		memcpy((packet_format_struct_var.data_length),(bTemp+START_INDICATOR_LEN) ,DATA_LENGTH_LEN);
		CstcLog_printf("2......");
		packet_format_struct_var.data_length[DATA_LENGTH_LEN]='\0';
		packet_format_struct_var.update_data_flag = bTemp[(START_INDICATOR_LEN+DATA_LENGTH_LEN)];
		CstcLog_printf("3......");
		len = atoi(packet_format_struct_var.data_length);
		CstcLog_printf("data_length in packet %s = %d",packet_format_struct_var.data_length, len);
		pkt_data_len = len-(START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+CHECKSUM_LEN);
		packet_format_struct_var.data = (char*)malloc((pkt_data_len+1));
		memset(packet_format_struct_var.data,0,pkt_data_len+1);
		CstcLog_printf("pkt_data_len %d......",pkt_data_len);
		memcpy(packet_format_struct_var.data,(bTemp+START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN),pkt_data_len);
		packet_format_struct_var.data[pkt_data_len]='\0';
		memcpy(packet_format_struct_var.checksum,(bTemp+START_INDICATOR_LEN+DATA_LENGTH_LEN+UPDATE_DATA_FLAG_LEN+pkt_data_len),CHECKSUM_LEN);
		packet_format_struct_var.checksum[CHECKSUM_LEN]='\0';
		//		pkt_ptr = malloc(len);
		//		memcpy(pkt_ptr,packet_format_struct_var.data,len);
		pkt_ptr=packet_format_struct_var.data;

		recvPktID[0] = pkt_ptr[0];
		recvPktID[1] = pkt_ptr[1];

		memcpy(recvPktID, pkt_ptr, 2);
		//memcpy(&temp, recvPktID, 2);
		////////////////
		////////////////
		temp = atoi(recvPktID);
		CstcLog_printf("packet data length = %d  & Id = %d",pkt_data_len, temp);
		CstcLog_printf("pkt_ptr %s ",pkt_ptr);

		switch(temp)
		{
		case 2:/*----------------------Schedule Details-------------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN/*+SEC_KEY_LEN+WAYBILL_NO_LEN*/))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				//lan_Err_Msg[lan_Err_Len]='\0';

				//				strcat(Err_Msg1,lan_Err_Msg);
				//				lan_Err_Len=lan_Err_Len+strlen(Err_Msg1);
				//CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_Msg1);

				//				strcat(lan_Err_Msg,"Packet length is less than actual required");//LESS_PACKET_LENGTH
				//				lan_Err_Len=lan_Err_Len+strlen("Packet length is less than actual required");
				//				CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				//memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				//memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN/*+SEC_KEY_LEN*/), WAYBILL_NO_LEN);

				CstcLog_printf("machine_no  %s ", download_response_struct_var.machine_no);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);
				CstcLog_printf("terminal_id  %s ", download_response_struct_var.terminal_id);
				//				CstcLog_printf("sec_key_ack  %s ", download_response_struct_var.sec_key_ack);
				//				CstcLog_printf("waybill_no  %s ", download_response_struct_var.waybill_no);
#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//lan_Err_Msg[lan_Err_Len]='\0';

					//					strcat(Err_Msg1,lan_Err_Msg);
					//					lan_Err_Len=lan_Err_Len+strlen(Err_Msg1);
					//CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_Msg1);
					//					strcat(lan_Err_Msg,"Machine Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//lan_Err_Msg[lan_Err_Len]='\0';

					//					strcat(Err_Msg1,lan_Err_Msg);
					//					lan_Err_Len=lan_Err_Len+strlen(Err_Msg1);
					//CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_Msg1);

					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					CstcLog_printf("TERMINAL_ID_NOT_FOUND");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//lan_Err_Msg[lan_Err_Len]='\0';

					//					strcat(Err_Msg1,lan_Err_Msg);
					//					lan_Err_Len=lan_Err_Len+strlen(Err_Msg1);
					//CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_Msg1);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				/*if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
						{
							CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
							//					strcat(lan_Err_Msg,"Security Key verified");
							//					lan_Err_Len=lan_Err_Len+strlen("Security Key verified");
							//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						}
						else
						{
							sprintf(lan_Err_Msg,"%02d",SECURITY_KEY_NOT_FOUND);
							strcat(Err_Msg1,lan_Err_Msg);
							lan_Err_Len=strlen(Err_Msg1);
							//lan_Err_Msg[lan_Err_Len]='\0';

		//					strcat(Err_Msg1,lan_Err_Msg);
		//					lan_Err_Len=lan_Err_Len+strlen(Err_Msg1);
							CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_Msg1);
							if(sn!=NULL)
								free(sn);
							if(etim_no!=NULL)
								free(etim_no);
		#if MAC_ID_VERIFY_DISABLED
							if(mac_ID!=NULL)
								free(mac_ID);
		#endif
							return 0;
						}
				if((atoi(download_response_struct_var.waybill_no))==(atoi(waybill_master_struct_var.waybill_no)))
				{
					CstcLog_printf("waybill number  %s verified", download_response_struct_var.waybill_no);
					//					strcat(lan_Err_Msg,"Waybill number verified");
					//					lan_Err_Len=lan_Err_Len+strlen("Waybill number verified");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);

					return 1;
				}

				else
				{
					sprintf(lan_Err_Msg,"%02d",WAYBILL_NUMBER_NOT_FOUND);
					strcat(Err_Msg1,lan_Err_Msg);
					lan_Err_Len=strlen(Err_Msg1);
					//lan_Err_Msg[lan_Err_Len]='\0';

					//					strcat(Err_Msg1,lan_Err_Msg);
					//					lan_Err_Len=lan_Err_Len+strlen(Err_Msg1);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_Msg1);
					//					strcat(lan_Err_Msg,"Waybill Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Waybill Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}*/
			}
			break;
		case 6:/*----------------------Start Duty-------------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+WAYBILL_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);

				//				strcat(lan_Err_Msg,"Packet length is less than actual required");//LESS_PACKET_LENGTH
				//				lan_Err_Len=lan_Err_Len+strlen("Packet length is less than actual required");
				//				CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg)
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN), WAYBILL_NO_LEN);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);
#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(lan_Err_Msg,"Machine Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					CstcLog_printf("TERMINAL_ID_NOT_FOUND");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}
					if(etim_no!=NULL)
					{
						free(etim_no);
						etim_no=NULL;
					}
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
				{
					CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
					//					strcat(lan_Err_Msg,"Security Key verified");
					//					lan_Err_Len=lan_Err_Len+strlen("Security Key verified");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SECURITY_KEY_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					if(sn!=NULL)
					{
						free(sn);
						sn=NULL;
					}if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.waybill_no))==(atoi(waybill_master_struct_var.waybill_no)))
				{
					CstcLog_printf("waybill number  %s verified", download_response_struct_var.waybill_no);
					//					strcat(lan_Err_Msg,"Waybill number verified");
					//					lan_Err_Len=lan_Err_Len+strlen("Waybill number verified");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(DbInterface_Update_Master_Status_Data("update waybill_master set shift_status ='Y'"))
					{
						//						strcat(lan_Err_Msg,"Start Duty Done");
						//						lan_Err_Len=lan_Err_Len+strlen("Start Duty Done");
						//						CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						CstcLog_printf("Start Duty Done");
						if(sn!=NULL)
							free(sn);
						if(etim_no!=NULL)
							free(etim_no);
#if MAC_ID_VERIFY_DISABLED
						if(mac_ID!=NULL)
							free(mac_ID);
#endif
						return 1;
					}
					else
					{
						memset(Err_MsgAry,0,3);
						sprintf(Err_MsgAry,"%02d",START_DUTY_FAILED);
						strcat(lan_Err_Msg,Err_MsgAry);
						lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
						CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
						memset(Err_MsgAry,0,3);

						//						strcat(lan_Err_Msg,"Start Duty failed");
						//						lan_Err_Len=lan_Err_Len+strlen("Start Duty failed");
						//						CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						if(sn!=NULL)
							free(sn);
						if(etim_no!=NULL)
							free(etim_no);
#if MAC_ID_VERIFY_DISABLED
						if(mac_ID!=NULL)
							free(mac_ID);
#endif
						return 0;
					}
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",WAYBILL_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(lan_Err_Msg,"Waybill Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Waybill Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
			}
			break;
		case 45:/*----------------------Download data-------------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+WAYBILL_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				//				strcat(lan_Err_Msg,"Packet length is less than actual required");//LESS_PACKET_LENGTH
				//				lan_Err_Len=lan_Err_Len+strlen("Packet length is less than actual required");
				//				CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
				return 0;
			}
			else
			{
				//DbInterface_Update_Master_Status_Data("update waybill_master set collection_status ='D'");
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				memset(download_response_struct_var.machine_no, 0, MACHINE_SN_LEN);
				memset(download_response_struct_var.mac_address, 0, MAC_ADDR_LEN);
				memset(download_response_struct_var.terminal_id, 0, TERMINAL_ID_LEN);
				memset(download_response_struct_var.sec_key_ack, 0, SEC_KEY_LEN);
				memset(download_response_struct_var.waybill_no, 0,WAYBILL_NO_LEN );

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+ SEC_KEY_LEN),WAYBILL_NO_LEN);


				CstcLog_printf("download_response_struct_var.machine_no = %s",download_response_struct_var.machine_no);
				CstcLog_printf("download_response_struct_var.mac_address = %s",download_response_struct_var.mac_address);
				CstcLog_printf("download_response_struct_var.terminal_id = %s",download_response_struct_var.terminal_id);
				CstcLog_printf("download_response_struct_var.sec_key_ack = %s",download_response_struct_var.sec_key_ack);
				CstcLog_printf("download_response_struct_var.waybill_no = %s",download_response_struct_var.waybill_no);

				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);


#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Machine Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"ERROR OPENING FILE etim_no.txt");
					//					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Terminal Id : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Terminal Id : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.sec_key_ack))==(atoi(waybill_master_struct_var.waybill_lock_code)))
				{
					CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
					//					strcat(lan_Err_Msg,"Security Key verified");
					//					lan_Err_Len=lan_Err_Len+strlen("Security Key verified");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 1;
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SECURITY_KEY_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);


					//					strcat(lan_Err_Msg,"Security Key : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Security Key : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
#if 0
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				tempStr = malloc(1024*sizeof(char));
				memset(tempStr,0,1024);
				strcpy(tempStr,"./etim_no R ");

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);

				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
					if(strcmp(download_response_struct_var.machine_no,sn)==0)
					{
						CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
						tempFile= fopen("ETIM_NO_FILE_ADDR","r");
						if(tempFile == NULL)
						{
							CstcLog_printf("ERROR OPENING FILE etim_no.txt");
							//							return 0;
							//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
							//memcpy(lan_Err_Msg,"ERROR OPENING FILE etim_no.txt",30);
							strcat(lan_Err_Msg,"ERROR OPENING FILE etim_no.txt");
							//lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
						}
						fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile);
						fflush(tempFile);
						CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
						fclose(tempFile);
						if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
						{
							CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
							//if((strcmp(download_response_struct_var.sec_key_ack,waybill_master_struct_var.waybill_lock_code)==0))
							if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
							{
								CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
								strcat(lan_Err_Msg,"Security Key verified");
								lan_Err_Len=lan_Err_Len+strlen("Security Key verified");
								CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
								return 1;
							}
							else
							{
								//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
								//memcpy(lan_Err_Msg,"Security Key : No match found",29);
								strcat(lan_Err_Msg,"Security Key : No match found");
								lan_Err_Len=lan_Err_Len+strlen("Security Key : No match found");
								CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
								return 0;
							}
							//return 1;
						}
						//						else
						//						{
						//							//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
						//memcpy(lan_Err_Msg,"Terminal_Id : No match found",28);
						//						    strcat(lan_Err_Msg,"No match found for Terminal_Id");
						//						lan_Err_Len=lan_Err_Len+strlen("No match found for Security Key");
						//						CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						//							return 0;
						//						}
					}
					else
					{
						//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
						//memcpy(lan_Err_Msg,"Machine Number : No match found",31);
						strcat(lan_Err_Msg,"Machine Number : No match found");
						lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
						CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						return 0;
					}
				}
				else
				{
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"MAC Address : No match found",28);
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 0;
				}
#endif
			}
			break;

		case 47:
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+TICKET_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				memset(download_response_struct_var.machine_no, 0, MACHINE_SN_LEN);
				memset(download_response_struct_var.mac_address, 0, MAC_ADDR_LEN);
				memset(download_response_struct_var.terminal_id, 0, TERMINAL_ID_LEN);
				memset(download_response_struct_var.sec_key_ack, 0, SEC_KEY_LEN);
				memset(download_response_struct_var.start_tkt_cnt, 0,TICKET_NO_LEN );
				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.start_tkt_cnt, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN),TICKET_NO_LEN );
				download_response_struct_var.start_tkt_cnt[TICKET_NO_LEN]='\0';

				CstcLog_printf("\n--------------------------- Check Request  ---------------------------------\n");
				CstcLog_printf("download_response_struct_var.machine_no %s ",download_response_struct_var.machine_no);
				CstcLog_printf("download_response_struct_var.mac_address %s ",download_response_struct_var.mac_address);
				CstcLog_printf("download_response_struct_var.terminal_id %s ",download_response_struct_var.terminal_id);
				CstcLog_printf("download_response_struct_var.sec_key_ack %s ",download_response_struct_var.sec_key_ack);
				CstcLog_printf("download_response_struct_var.start_tkt_cnt %s ",download_response_struct_var.start_tkt_cnt);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);

#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(lan_Err_Msg,"Machine Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(lan_Err_Msg,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"ERROR OPENING FILE etim_no.txt");
					//					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Terminal Id : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Terminal Id : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
				{
					CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
					//					strcat(lan_Err_Msg,"Security Key verified");
					//					lan_Err_Len=lan_Err_Len+strlen("Security Key verified");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 1;
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SECURITY_KEY_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Security Key : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Security Key : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}

#if 0
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				tempStr = malloc(1024*sizeof(char));
				memset(tempStr,0,1024);
				strcpy(tempStr,"./etim_no W ");

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.start_tkt_cnt, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN), TICKET_NO_LEN);

				CstcLog_printf("download_response_struct_var.machine_no %s ",download_response_struct_var.machine_no);
				CstcLog_printf("download_response_struct_var.mac_address %s ",download_response_struct_var.mac_address);
				CstcLog_printf("download_response_struct_var.terminal_id %s ",download_response_struct_var.terminal_id);
				CstcLog_printf("download_response_struct_var.sec_key_ack %s ",download_response_struct_var.sec_key_ack);
				CstcLog_printf("download_response_struct_var.start_tkt_cnt %s ",download_response_struct_var.start_tkt_cnt);

				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);

				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
					if(strcmp(download_response_struct_var.machine_no,sn)==0)
					{
						CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
						tempFile= fopen(ETIM_NO_FILE_ADDR,"r+");
						if(tempFile == NULL)
						{
							CstcLog_printf("ERROR OPENING FILE etim_no.txt");
							return 0;
						}
						fgets(etim_no,TERMINAL_ID_LEN,tempFile);
						fflush(tempFile);
						CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
						fclose(tempFile);
						//						if((strcmp(download_response_struct_var.terminal_id,""/*etim_no*/)==0))
						//						{
						//							//CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
						if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
						{
							CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
							return 1;
						}
						else
						{
							//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
							//memcpy(lan_Err_Msg,"Security Key : No match found",29);
							strcat(lan_Err_Msg,"No match found for Security Key");
							lan_Err_Len=lan_Err_Len+strlen("Terminal_Id : No match found");
							CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
							return 0;
						}
						return 1;
						//						}
						//						else
						//						{
						//							//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
						//							//memcpy(lan_Err_Msg,"Terminal_Id : No match found",28);
						//						    strcat(lan_Err_Msg,"Terminal_Id : No match found");
						//							lan_Err_Len=lan_Err_Len+strlen("Terminal_Id : No match found");
						//							CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						//							return 0;
						//						}
					}
					else
					{
						//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
						//memcpy(lan_Err_Msg,"Machine Number : No match found",31);
						strcat(lan_Err_Msg,"Machine Number : No match found");
						lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
						CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						return 0;
					}
				}
				else
				{
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"MAC Address : No match found",28);
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 0;
				}
#endif
			}
			break;

		case 50:/*--------------------ERASE MASTER DATA-------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+WAYBILL_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				memset(download_response_struct_var.machine_no, 0, MACHINE_SN_LEN);
				memset(download_response_struct_var.mac_address, 0, MAC_ADDR_LEN);
				memset(download_response_struct_var.terminal_id, 0, TERMINAL_ID_LEN);
				memset(download_response_struct_var.sec_key_ack, 0, SEC_KEY_LEN);
				memset(download_response_struct_var.waybill_no, 0,WAYBILL_NO_LEN );

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN), WAYBILL_NO_LEN);
				CstcLog_printf("mac_address  %s ", download_response_struct_var.mac_address);

#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt",29);
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile=NULL;

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
				}
				else
				{
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
#endif
				if(strcmp(download_response_struct_var.machine_no,sn)==0)
				{
					CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_Msg1 = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Machine Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				tempFile2= fopen(ETIM_NO_FILE_ADDR,"r+");
				if(tempFile2 == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"ERROR OPENING FILE etim_no.txt");
					//					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile2);
				fflush(tempFile2);
				CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
				fclose(tempFile2);
				if((strcmp(download_response_struct_var.terminal_id,/*"SNS00009"*/etim_no)==0))
				{
					CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",TERMINAL_ID_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Terminal Id : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Terminal Id : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.sec_key_ack)==(atoi(waybill_master_struct_var.waybill_lock_code))))
				{
					CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
					//					strcat(lan_Err_Msg,"Security Key verified");
					//					lan_Err_Len=lan_Err_Len+strlen("Security Key verified");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",SECURITY_KEY_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,ERROR_MSG_BUFFER_LEN);

					//					strcat(lan_Err_Msg,"Security Key : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Security Key : No match found");
					//					CstcLog_printf("lan_Err_Len=%d , lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				if((atoi(download_response_struct_var.waybill_no))==(atoi(waybill_master_struct_var.waybill_no)))
				{
					CstcLog_printf("waybill number  %s verified", download_response_struct_var.waybill_no);
					//					strcat(lan_Err_Msg,"Waybill number verified");
					//					lan_Err_Len=lan_Err_Len+strlen("Waybill number verified");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					//					if(waybill_master_struct_var.collection_status=='D')
					//					{
					//						strcat(lan_Err_Msg,"Start Duty Done");
					//						lan_Err_Len=lan_Err_Len+strlen("Start Duty Done");
					//						CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 1;
					//					}
					//					else
					//					{
					//						strcat(lan_Err_Msg,"Start Duty failed");
					//						lan_Err_Len=lan_Err_Len+strlen("Start Duty failed");
					//						CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					//						if(sn!=NULL)
					//							free(sn);
					//						if(etim_no!=NULL)
					//							free(etim_no);
					//						if(mac_ID!=NULL)
					//							free(mac_ID);
					//						return 0;
					//					}
				}
				else
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",WAYBILL_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Waybill Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Waybill Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					if(sn!=NULL)
						free(sn);
					if(etim_no!=NULL)
						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
			}
#if 0
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN+WAYBILL_NO_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				memcpy(download_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(download_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(download_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				memcpy(download_response_struct_var.sec_key_ack, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN), SEC_KEY_LEN);
				memcpy(download_response_struct_var.waybill_no, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN+SEC_KEY_LEN), WAYBILL_NO_LEN);

				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len=%d ,lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);

				if(strcmp(download_response_struct_var.mac_address,mac_ID)==0)//"00257E019279")==0)
				{
					CstcLog_printf("mac_address  %s verified", download_response_struct_var.mac_address);
					if(strcmp(download_response_struct_var.machine_no,sn)==0)
					{
						CstcLog_printf("machine_no  %s verified", download_response_struct_var.machine_no);
						//						tempFile= fopen(ETIM_NO_FILE_ADDR,"a");
						//						if(tempFile == NULL)
						//						{
						//							CstcLog_printf("ERROR OPENING FILE etim_no.txt");
						//						    strcat(lan_Err_Msg,"ERROR OPENING FILE etim_no.txt");
						//							lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
						//							CstcLog_printf("lan_Err_Len=%d ,lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						//							return 0;
						//						}
						//						fgets(etim_no,download_response_struct_var.terminal_id,tempFile);
						//						fflush(tempFile);
						//						CstcLog_printf("terminal_id :- %s", download_response_struct_var.terminal_id);
						//						fclose(tempFile);
						//						//					memcpy(tempStr+strlen("./etim_no W "),registration_response_struct_var.terminal_id, TERMINAL_ID_LEN);
						//						//					CstcLog_printf("system cmd  %s ", tempStr);
						//						//					if(system(tempStr)>0)
						//						if((strcmp(download_response_struct_var.terminal_id,""/*etim_no*/)==0))
						{
							//CstcLog_printf("Terminal Id  %s verified", download_response_struct_var.terminal_id);
							if((strcmp(download_response_struct_var.sec_key_ack,waybill_master_struct_var.waybill_lock_code)==0))
							{
								CstcLog_printf("Security Key %s verified", download_response_struct_var.sec_key_ack);
								if((strcmp(download_response_struct_var.waybill_no,waybill_master_struct_var.waybill_no))==0)
								{
									CstcLog_printf("waybill number  %s verified", download_response_struct_var.waybill_no);
									strcat(lan_Err_Msg,"Waybill number verified");
									lan_Err_Len=lan_Err_Len+strlen("Waybill number verified");
									CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
								}
								else
								{
									//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
									//memcpy(lan_Err_Msg,"Waybill Number : No match found",31);
									strcat(lan_Err_Msg,"Waybill Number : No match found");
									lan_Err_Len=lan_Err_Len+strlen("Waybill Number : No match found");
									CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);

									return 0;
								}
								strcat(lan_Err_Msg,"Security Key verified");
								lan_Err_Len=lan_Err_Len+strlen("Security Key verified");
								CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
								return 1;
							}
							else
							{
								//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
								//memcpy(lan_Err_Msg,"Security Key : No match found",29);
								strcat(lan_Err_Msg,"Security Key : No match found");
								lan_Err_Len=lan_Err_Len+strlen("Security Key : No match found");
								CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
								return 0;
							}
							return 1;
						}
						//						else
						//						{
						//							//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
						//							//memcpy(lan_Err_Msg,"Terminal Id : No match found",28);
						//						    strcat(lan_Err_Msg,"Terminal Id : No match found");
						//							lan_Err_Len=lan_Err_Len+strlen("Terminal Id : No match found");
						//							CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						//							return 0;
						//						}
					}
					else
					{
						//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
						//memcpy(lan_Err_Msg,"Machine Number : No match found",31);
						strcat(lan_Err_Msg,"Machine Number : No match found");
						lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
						CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
						return 0;
					}
				}
				else
				{
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"MAC Address : No match found",28);
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 0;
				}
			}
#endif
			break;

		case 54:/*--------------------GET ETIM NO------------------------*/
			CstcLog_printf("******************GET ETIM NO case 54***********************");
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				if(strncmp((pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN),"000000000000",MAC_ADDR_LEN)== 0)
				{
					CstcLog_printf("mac_ID :- %s", (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN));
				}
				else
				{
					CstcLog_printf("mac_ID :- %sFailed to verify", (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN));
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MAC_ADDRESS_NO_MATCH_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(lan_Err_Msg,"MAC Address : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					//					CstcLog_printf("lan_Err_Len =%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 0;
				}
				if(strncmp((pkt_ptr + PKT_ID_LEN),"00000000",MACHINE_SN_LEN)== 0)
				{
					CstcLog_printf("machine_no = %s ", (pkt_ptr + PKT_ID_LEN));
				}
				else
				{
					CstcLog_printf("machine_no = %s Failed to verify", (pkt_ptr + PKT_ID_LEN));
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);
					//					strcat(lan_Err_Msg,"Machine Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					return 0;
				}
				return 1;

			}
			break;
		case 56:/*--------------------ETIM NO REG------------------------*/
			if(len<(PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN+TERMINAL_ID_LEN))
			{
				CstcLog_printf("Packet length %d is less than actual required", len);
				memset(Err_MsgAry,0,3);
				sprintf(Err_MsgAry,"%02d",LESS_PACKET_LENGTH);
				strcat(lan_Err_Msg,Err_MsgAry);
				lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
				CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
				memset(Err_MsgAry,0,3);
				return 0;
			}
			else
			{
				sn =(char *)malloc((MACHINE_SN_LEN+1)*sizeof(char));
				memset(sn,0,(MACHINE_SN_LEN+1));
				//				etim_no =(char *)malloc((TERMINAL_ID_LEN+1)*sizeof(char));
				//				memset(etim_no,0,(TERMINAL_ID_LEN+1));
				get_machine_info(sn, (MACHINE_SN_LEN+1));

				//				tempStr = malloc(1024*sizeof(char));
				//				memset(tempStr,0,1024);
				//				strcpy(tempStr,"./etim_no W ");

				memset(registration_response_struct_var.machine_no, 0, MACHINE_SN_LEN);
				memset(registration_response_struct_var.mac_address, 0, MAC_ADDR_LEN);
				memset(registration_response_struct_var.terminal_id, 0, TERMINAL_ID_LEN);
				memcpy(registration_response_struct_var.machine_no, (pkt_ptr + PKT_ID_LEN), MACHINE_SN_LEN);
				memcpy(registration_response_struct_var.mac_address, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN), MAC_ADDR_LEN);
				memcpy(registration_response_struct_var.terminal_id, (pkt_ptr + PKT_ID_LEN+MACHINE_SN_LEN+MAC_ADDR_LEN), TERMINAL_ID_LEN);
				CstcLog_printf("mac_address  %s ", registration_response_struct_var.mac_address);

#if MAC_ID_VERIFY_DISABLED
				mac_ID =(char *)malloc((MAC_ADDR_LEN+1)*sizeof(char));
				memset(mac_ID,0,(MAC_ADDR_LEN+1));
				tempFile = NULL;
				tempFile= fopen(MAC_FILE_ADDR,"r+");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE mac_ID.txt");
					strcat(lan_Err_Msg,"ERROR OPENING FILE mac_ID.txt");
					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE mac_ID.txt");
					CstcLog_printf("lan_Err_Len =%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
						free(sn);
					//					if(etim_no!=NULL)
					//						free(etim_no);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				fgets(mac_ID,(MAC_ADDR_LEN+1),tempFile);
				//			strcpy(etim_no,"SNS00001");
				fflush(tempFile);
				CstcLog_printf("mac_ID :- %s", mac_ID);
				fclose(tempFile);
				tempFile = NULL;

				if(strcmp(registration_response_struct_var.mac_address,mac_ID)!=0)//"00257E019279")==0)
				{
					//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
					//memcpy(lan_Err_Msg,"MAC Address : No match found",28);
					strcat(lan_Err_Msg,"MAC Address : No match found");
					lan_Err_Len=lan_Err_Len+strlen("MAC Address : No match found");
					CstcLog_printf("lan_Err_Len =%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
						free(sn);
					//					if(etim_no!=NULL)
					//						free(etim_no);
					if(mac_ID!=NULL)
						free(mac_ID);
					return 0;
				}
				else
					CstcLog_printf("mac_address  %s verified", registration_response_struct_var.mac_address);
#endif
				if(strcmp(registration_response_struct_var.machine_no,sn)!=0)
				{
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",MACHINE_NUMBER_NOT_FOUND);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"Machine Number : No match found");
					//					lan_Err_Len=lan_Err_Len+strlen("Machine Number : No match found");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
						free(sn);
					//					if(etim_no!=NULL)
					//						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				else
					CstcLog_printf("machine_no  %s verified", registration_response_struct_var.machine_no);
				tempFile = NULL;
				tempFile= fopen(ETIM_NO_FILE_ADDR,"w");
				if(tempFile == NULL)
				{
					CstcLog_printf("ERROR OPENING FILE etim_no.txt");
					memset(Err_MsgAry,0,3);
					sprintf(Err_MsgAry,"%02d",ERROR_OPENING_FILE_etim_no);
					strcat(lan_Err_Msg,Err_MsgAry);
					lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
					CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
					memset(Err_MsgAry,0,3);

					//					strcat(lan_Err_Msg,"ERROR OPENING FILE etim_no.txt");
					//					lan_Err_Len=lan_Err_Len+strlen("ERROR OPENING FILE etim_no.txt");
					//					CstcLog_printf("lan_Err_Len=%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
						free(sn);
					//					if(etim_no!=NULL)
					//						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
				else
				{
					CstcLog_printf("Tempfile open success :%d",tempFile);
					//					strcat(lan_Err_Msg,"Tempfile open success");
					//					lan_Err_Len=lan_Err_Len+strlen("Tempfile open success");
					//					CstcLog_printf("lan_Err_Len =%d lan_Err_Msg=%s",lan_Err_Len,lan_Err_Msg);
				}
				fputs(registration_response_struct_var.terminal_id,tempFile);
				//				fprintf(tempFile,"%s",registration_response_struct_var.terminal_id);
				//				fscanf(tempFile,"%s",etim_no);
				//				fgets(etim_no,(TERMINAL_ID_LEN+1),tempFile);
				fflush(tempFile);
				CstcLog_printf("terminal_id :- %s", registration_response_struct_var.terminal_id);
				//				CstcLog_printf("etim_no :- %s",etim_no );
				fclose(tempFile);
				tempFile = NULL;
				//									memcpy(tempStr+strlen("./etim_no W "),registration_response_struct_var.terminal_id, TERMINAL_ID_LEN);
				//									CstcLog_printf("system cmd  %s ", tempStr);
				//									system(tempStr);
				if(strlen(registration_response_struct_var.terminal_id)>0)//					if(system(tempStr)>0)
				{
					return 1;
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
						free(sn);
					//					if(etim_no!=NULL)
					//						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
				}
				else
				{
					memset(registration_response_struct_var.terminal_id,'0',TERMINAL_ID_LEN);
					//					if(tempStr!=NULL)
					//						free(tempStr);
					if(sn!=NULL)
						free(sn);
					//					if(etim_no!=NULL)
					//						free(etim_no);
#if MAC_ID_VERIFY_DISABLED
					if(mac_ID!=NULL)
						free(mac_ID);
#endif
					return 0;
				}
			}
			break;
		default:
			return 0;
			break;
		}
		sprintf(lan_Err_Msg,"%02d",SUCCESS);
		lan_Err_Len = lan_Err_Len + strlen(lan_Err_Msg);
		CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
		lan_Err_Msg[lan_Err_Len]='\0';
		memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
		lan_Err_Len=0;
		//		strcat(lan_Err_Msg,"Parsing Successful");
		//		lan_Err_Len=lan_Err_Len+strlen("Parsing Successful");
		//		CstcLog_printf("lan_Err_Len=%d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
		CstcLog_printf("Parsing Successful");

		//		if(!(DbInterface_Insert_In_All_Tables1(tempName, tempStr,tempStrLen)))
		//		{
		//			CstcLog_printf("Insertion in table \'%s\' failed", tempName);
		//			//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
		//			//memcpy(lan_Err_Msg,"Data Insertion in table failed",30);
		//			strcat(lan_Err_Msg,"Data Insertion in table failed");
		//			lan_Err_Len =lan_Err_Len+strlen("Data Insertion in table failed");
		//			CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len,lan_Err_Msg);
		//			memset_packet_format();
		//			if(tempStr!=NULL)
		//				free(tempStr);
		//			return 0;
		//		}
		//		else
		//		{
		//			CstcLog_printf("Insertion in table \'%s\' success", tempName);
		//			//memset(lan_Err_Msg,0,sizeof(lan_Err_Msg));
		//			//memcpy(lan_Err_Msg,"Data Insertion successful",25);
		//			strcat(lan_Err_Msg,"Data Insertion successful");
		//			lan_Err_Len =lan_Err_Len+strlen("Data Insertion successful");
		//			CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len, lan_Err_Msg);
		//			memset_packet_format();
		//			if(tempStr!=NULL)
		//				free(tempStr);
		//			return 1;
		//		}
		memset_packet_format();
		//		memset(tempStr,0,tempStrLen);
		//		if(tempStr!=NULL)
		//			free(tempStr);
		return 1;
	}
	else
	{
		CstcLog_printf("Parsing Fail Resend Packet");
		memset(Err_MsgAry,0,3);
		sprintf(Err_MsgAry,"%02d",RESEND_PACKETS);
		strcat(lan_Err_Msg,Err_MsgAry);
		lan_Err_Len=lan_Err_Len+strlen(Err_MsgAry);
		CstcLog_printf("lan_Err_Len = %d lan_Err_Msg =%s Err_MsgAry = %s",lan_Err_Len,lan_Err_Msg,Err_MsgAry);
		memset(Err_MsgAry,0,3);
		//		strcat(lan_Err_Msg,"Parsing Fail Resend Packet");
		//		lan_Err_Len =lan_Err_Len+strlen("Parsing Fail Resend Packet");
		//		CstcLog_printf("lan_Err_Len =%d lan_Err_Msg =%s",lan_Err_Len , lan_Err_Msg);
	}
	return 0;
}

void compose_download_response_packet(char * pkt_ptr,int *length)
{
	int rowCount=0;
	int temp = DOWNLOAD_DATA_COUNT_RESPONSE_PCK_ID;

	CstcLog_printf("INSIDE compose_download_response_packet FUNCTION");
	CstcLog_printf("DOWNLOAD_RESPONSE_PCK_ID = %d ",temp);
	sprintf(download_response_packet_struct_var.pkt_id_download_response, "%d", temp);

	rowCount=DbInterface_Get_Row_Count("transaction_log",16,0);
	CstcLog_printf("rowCount = %d ",rowCount);
	lan_Global_rowCount=rowCount;
	lan_GDownload_pktCnt =0;

	if(rowCount == 0)
	{
		strcpy(download_response_struct_var.row_cnt,"00000");
		strcpy(download_response_struct_var.start_tkt_cnt,"00000");
		strcpy(download_response_struct_var.end_tkt_cnt, "00000");
	}
	else
	{
		get_start_end_db_data(download_response_struct_var.start_tkt_cnt,download_response_struct_var.end_tkt_cnt);
		sprintf(download_response_struct_var.row_cnt,"%05d",rowCount);
	}
	CstcLog_printf("start_tkt_cnt = %s , end_tkt_cnt =%s , row_cnt=%d",download_response_struct_var.start_tkt_cnt,download_response_struct_var.end_tkt_cnt,rowCount);

	memcpy(pkt_ptr,download_response_packet_struct_var.pkt_id_download_response, PKT_ID_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN),download_response_struct_var.row_cnt, ROW_CNT_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+ROW_CNT_LEN),download_response_struct_var.start_tkt_cnt, TICKET_NO_LEN);
	memcpy((pkt_ptr + PKT_ID_LEN+ROW_CNT_LEN+TICKET_NO_LEN), download_response_struct_var.end_tkt_cnt, TICKET_NO_LEN);

	CstcLog_printf("PKT_PTR %s", pkt_ptr);
	*length += PKT_ID_LEN+ROW_CNT_LEN+TICKET_NO_LEN+TICKET_NO_LEN;
}

void compose_transaction_data_packets(char * pkt_ptr,int *length)
{
	int Transaction_no,loop_count=0,i,record_length=0;
	int temp=TRANSACTION_DATA;
	CstcLog_printf("TRANSACTION_DATA = %d ",temp);

	//	rowCount=DbInterface_Get_Row_Count("transaction_log",16);
	//	CstcLog_printf("rowCount = %d ",rowCount);

	if(lan_Global_rowCount>DOWNLOAD_TICKET_RANGE)
	{
		loop_count=DOWNLOAD_TICKET_RANGE;
		lan_Global_rowCount=lan_Global_rowCount-DOWNLOAD_TICKET_RANGE;
		CstcLog_printf("loop_count = %d lan_Global_rowCount=%d ",loop_count,lan_Global_rowCount);
	}
	else
	{
		loop_count=lan_Global_rowCount;
		lan_Global_rowCount=0;
		CstcLog_printf("loop_count = %d lan_Global_rowCount =%d",loop_count,lan_Global_rowCount);
	}

	CstcLog_printf("download_response_struct_var.start_tkt_cnt %s",download_response_struct_var.start_tkt_cnt);
	Transaction_no=atoi(download_response_struct_var.start_tkt_cnt);
	CstcLog_printf("Transaction_no = %d ",Transaction_no);
	if(Transaction_no == 0)
	{
		strcpy(pkt_ptr,"Transaction Complete");
	}
	else
	{
		sprintf(download_transaction_log_struct_var.pkt_id_ack_packet, "%d", temp);
		sprintf(pkt_ptr,"%s",download_transaction_log_struct_var.pkt_id_ack_packet);
		record_length=PKT_ID_LEN;
		CstcLog_printf("record_length = %d ",record_length);

		for(i=0;i<loop_count;i++)
		{
			CstcLog_printf("i = %d ",i);
			//sprintf(download_transaction_log_struct_var.pkt_id_ack_packet, "%d", temp);
			get_transaction_log_data(Transaction_no,pkt_ptr);

			sprintf((pkt_ptr+record_length),"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',"
					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"

					"\'%s\',\'%s\',\'%s\',"/*\'%s\',\'%s\',\'%s\',"*/
					"\'%s\',\'%s\',\'%s\',"/*\'%s\',\'%s\',\'%s\',"*/

					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%d\',\'%d\',\'%d\',\'%s\',"

					"\'%s\',\'%c\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',"
					"\'%s\',\'%s\',\'%s\',\'%c\'|",

					//download_transaction_log_struct_var.pkt_id_ack_packet,
					transaction_log_struct_var.waybill_no,
					transaction_log_struct_var.schedule_no,
					transaction_log_struct_var.trip_no,
					transaction_log_struct_var.etim_no,
					transaction_log_struct_var.route_no,

					transaction_log_struct_var.route_id,            //new requirement

					transaction_log_struct_var.transaction_no,
					transaction_log_struct_var.ticket_no,
					transaction_log_struct_var.tkt_type_short_code,
					transaction_log_struct_var.tkt_sub_type_short_code,
					transaction_log_struct_var.from_stop_seq_no,

					transaction_log_struct_var.from_bus_stop_code_english,
					transaction_log_struct_var.from_bus_stop_id, 			//new requirement
					//			transaction_log_struct_var.from_bus_stop_name_english,
					//			transaction_log_struct_var.from_bus_stop_code_bangla,
					//			transaction_log_struct_var.from_bus_stop_name_bangla,
					transaction_log_struct_var.till_stop_seq_no,

					transaction_log_struct_var.till_bus_stop_code_english,
					transaction_log_struct_var.till_bus_stop_id, 				 //new requirement
					//			transaction_log_struct_var.till_bus_stop_name_english,
					//			transaction_log_struct_var.till_bus_stop_code_bangla,
					//			transaction_log_struct_var.till_bus_stop_name_bangla,
					transaction_log_struct_var.travelled_km,

					transaction_log_struct_var.px_count,
					transaction_log_struct_var.lugg_units,
					transaction_log_struct_var.px_total_amount,
					transaction_log_struct_var.lugg_total_amount,
					transaction_log_struct_var.toll_amt,

					transaction_log_struct_var.total_ticket_amount,
					transaction_log_struct_var.concession_type,
					transaction_log_struct_var.group_ticket_mode,
					transaction_log_struct_var.payment_mode,
					transaction_log_struct_var.ticket_date,

					transaction_log_struct_var.ticket_time,
					transaction_log_struct_var.upload_flag,
					transaction_log_struct_var.bus_service_id,
					//transaction_log_struct_var.fare_type,
					transaction_log_struct_var.line_check_status,
					transaction_log_struct_var.depot_id,

					transaction_log_struct_var.vehicle_no,
					transaction_log_struct_var.ticket_code,
					transaction_log_struct_var.pass_id,
					transaction_log_struct_var.epurse_card_no,
					transaction_log_struct_var.epurse_card_issuer_name,

					transaction_log_struct_var.epurse_curr_amount_qstr,
					transaction_log_struct_var.epurse_last_amount_qstr,
					transaction_log_struct_var.epurse_last_waybill_no_qstr,
					transaction_log_struct_var.epurse_last_tkt_no,
					transaction_log_struct_var.epurse_last_tkt_date,

					transaction_log_struct_var.epurse_last_tkt_time,
					transaction_log_struct_var.epurse_last_from_stop_code ,
					transaction_log_struct_var.epurse_last_to_stop_code,
					transaction_log_struct_var.tkt_printed_flag
			);

			//		CstcLog_printf("PKT_PTR %s", pkt_ptr);
			//		CstcLog_printf("PKT_PTR_Len = %d", strlen(pkt_ptr));
			//		*length+=strlen(pkt_ptr);
			//pkt_ptr++;
			record_length = strlen(pkt_ptr);
			CstcLog_printf("record_length = %d ",record_length);

			CstcLog_printf("PKT_PTR1:::: %s", pkt_ptr);
			//	CstcLog_HexDump(pkt_ptr,record_length);
			Transaction_no++;
			CstcLog_printf("Transaction_no= %d ",Transaction_no);
			transaction_log_struct_clear(&transaction_log_struct_var);
		}

	}
	CstcLog_printf("PKT_PTR_Main %s", pkt_ptr);
	CstcLog_printf("PKT_PTR_Len = %d", strlen(pkt_ptr));
	*length+=strlen(pkt_ptr);
}






//**************************************************************************************************
#if 0
int compose_response_packet(char * bTemp, int * count, int iclient_socket)
{
	int length = 45;
	char *pkt_ptr = NULL;
	int i = 0;
	int error = 0;
	char *pkt_format_ptr = NULL;
	char tempChrr = bTemp[0];
	if(tempChrr == ';')
		(*count)++;
	switch(*count)
	{
	case 1: /*-------------------WAYBILL----------------------*/
		pkt_ptr= (char *)malloc(sizeof(struct waybill_master));
		memset(pkt_ptr,0,sizeof(struct waybill_master));
		compose_waybill_packet(pkt_ptr);
		CstcLog_printf("\n*****************************************");
		CstcLog_printf("\nSending Waybill Master");
		length += 286;
		break;

	case 2 : /*-----------------SCHEDULE-----------------------*/
		pkt_ptr = (char *)malloc(sizeof(struct schedule_master));
		memset(pkt_ptr,0,sizeof(struct schedule_master));
		compose_schedule_packet(pkt_ptr);
		CstcLog_printf("\n*****************************************");
		CstcLog_printf("\nSending Schedule Master");
		length += 166;
		break;

	case 3 : /*--------------------DUTY------------------------*/
		pkt_ptr = (char *)malloc(sizeof(struct duty_master));
		memset(pkt_ptr,0,sizeof(struct duty_master));
		compose_duty_packet(pkt_ptr);
		CstcLog_printf("\n*****************************************");
		CstcLog_printf("\nSending Duty Master");
		length += 15;
		break;

	case 4 : /*-------------------ROUTE------------------------*/
		pkt_ptr = (char *)malloc(sizeof(struct route_master));
		memset(pkt_ptr,0,sizeof(struct route_master));
		compose_route_packet(pkt_ptr);
		CstcLog_printf("\n*****************************************");
		CstcLog_printf("\nSending Route Master");
		length += 88;
		break;

	case 5 : /*--------------------FARE------------------------*/
		pkt_ptr = (char *)malloc(sizeof(struct fare_chart));
		memset(pkt_ptr,0,sizeof(struct fare_chart));
		compose_fare_chart_packet(pkt_ptr);
		CstcLog_printf("\n*****************************************");
		CstcLog_printf("\nSending Fare Chart");
		length += 212;
		break;

	default:
		break;
	}
	if(*count < 6)
	{
		pkt_format_ptr = (char *)malloc(length);
		memset(pkt_format_ptr,0,length);
		final_packet(pkt_format_ptr, pkt_ptr, length);

		CstcLog_printf("\n\nSent bytes are : ");
		for(i=0 ; i< packet_format_struct_var.data_length ; i++)
			printf("%2x, ", pkt_format_ptr[i]);
		CstcLog_printf("\nSent %d bytes\n ", packet_format_struct_var.data_length);
		error = tcp_write(iclient_socket, pkt_format_ptr, packet_format_struct_var.data_length);
		if (error != 0)
		{
			CstcLog_printf("\nConnection was broken !!\n");
			*count = 0;
			return 0;
		}
	}
	else
	{
		//        memset(pkt_ptr,0,sizeof(pkt_ptr));
		//        memset(pkt_format_ptr,0,sizeof(pkt_format_ptr));
		*count = 0;
		return 0;
	}
	return 1;
}
#endif
#if 0
void socket_client_demo(void){
	int iclient_socket = -1;
	unsigned char bTemp[1024];
	unsigned int  iTemp;
	lcd_clean();
	lcd_printf_ex(ALG_CENTER, "socket client");

	lcd_printf_ex(ALG_LEFT, "connect to %s:%d...", SERVER_ADDR, SERVER_PORT_LAN);
	lcd_flip();
	iclient_socket = tcp_connect(SERVER_ADDR, SERVER_PORT_LAN);
	if (iclient_socket < 0){
		lcd_printf_ex(ALG_LEFT, "Connect failed.");
		lcd_flip();
		kb_getkey();
	}else {
		while (1){
			if (Kb_Hit()){
				if (0x1B == kb_getkey())
					break;
			}
			strcpy((char*)bTemp, "HELLO WORLD");
			iTemp = strlen((char*)bTemp);
			if (tcp_write(iclient_socket , bTemp, iTemp) < 0){
				lcd_printf_ex(ALG_LEFT, "connection was broken");
				lcd_flip();
				kb_getkey();
				break;
			}else{
				usleep(500000);
				iTemp = sizeof(bTemp);
				if (tcp_read(iclient_socket, bTemp, &iTemp) < 0){
					lcd_printf_ex(ALG_LEFT, "connection was broken");
					lcd_flip();
					kb_getkey();
					break;
				}else if (iTemp > 0){
					lcd_printf_ex(ALG_LEFT, "Readed %d bytes", iTemp);
				}else {

				}
			}
		}
		tcp_close(iclient_socket);
	}


}


#define CA_CERT     "CA.crt.pem"

#define SERVER_CERT "SERVER.crt.pem"
#define SERVER_KEY  "SERVER.key.pem"

#define CLIENT_CERT "CLIENT.crt.pem"
#define CLIENT_KEY  "CLIENT.key.pem"


void ssl_server_demo(void){
	int retval = 0;
	int iserver_socket = -1;
	int iclient_socket = -1;
	unsigned char bTemp[1024];
	unsigned int  iTemp = 0;
	int i;
	int active_net = 0;
	char client_addr[255];
	unsigned int client_port = 0;
	char iflist[16][IFNAMSIZ];
	struct ifreq ifr;
	int error;

	SSL_CTX* ctx = NULL;
	SSL* ssl = NULL;
	const SSL_METHOD *meth;


	lcd_clean();


	SSL_load_error_strings();
	OpenSSL_add_ssl_algorithms();
	meth = TLSv1_server_method();


	ctx = SSL_CTX_new (meth);
	ssl = SSL_new (ctx);

	if ((NULL == ctx) ||
			(NULL == ssl)){
		lcd_printf_ex(ALG_LEFT, "SSL_CTX_new() or SSL_new() failed");
		lcd_flip();
		kb_getkey();
		retval = -1;
	}else{
		SSL_CTX_set_verify(ctx,SSL_VERIFY_PEER,NULL);
		SSL_CTX_set_verify_depth(ctx,1);

		ctx->default_passwd_callback_userdata = NULL;
		if (SSL_CTX_load_verify_locations(ctx,CA_CERT,NULL)<=0)
		{
			lcd_printf_ex(ALG_LEFT, "SSL_CTX_load_verify_locations() failed");
			ERR_print_errors_fp(stderr);
			retval = -1;
			lcd_flip();
			kb_getkey();
		}
		else if (SSL_CTX_use_certificate_file(ctx, SERVER_CERT, SSL_FILETYPE_PEM) <= 0) {
			lcd_printf_ex(ALG_LEFT, "SSL_CTX_use_certificate_file() failed");
			ERR_print_errors_fp(stderr);
			retval = -1;
			lcd_flip();
			kb_getkey();
		}else if (SSL_CTX_use_PrivateKey_file(ctx, SERVER_KEY, SSL_FILETYPE_PEM) <= 0) {
			ERR_print_errors_fp(stderr);
			retval = -1;
		}else if (!SSL_CTX_check_private_key(ctx)) {
			printf("Private key does not match the certificate public key\n");
			lcd_printf_ex(ALG_LEFT, "SSL_CTX_check_private_key() failed");
			lcd_flip();
			kb_getkey();
			retval = -1;
		}else
			SSL_CTX_set_cipher_list(ctx,"RC4-MD5");
	}



	if (0 == retval){
		lcd_printf_ex(ALG_CENTER, "ssl server");
		iTemp = netif_get_list(iflist, sizeof(iflist)/IFNAMSIZ);
		for (i = 0; i < iTemp; i++){
			memset(&ifr, 0, sizeof(ifr));
			netif_get_flags(iflist[i], &ifr);
			if (ifr.ifr_flags & IFF_RUNNING){
				netif_get_ipaddr(iflist[i], &ifr);
				lcd_printf_ex(ALG_CENTER, "Address:%s:%d", inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr), SERVER_PORT_LAN);
				active_net++;
			}
		}

		if (active_net == 0){
			lcd_printf_ex(ALG_CENTER, "NO Active net work");
			lcd_flip();
			kb_getkey();
			retval = -1;
		}

		lcd_flip();
	}





	if (0 == retval) {
		iserver_socket = tcp_server(SERVER_PORT_LAN, 1);
		if (iserver_socket >=0){
			lcd_printf_ex(ALG_LEFT, "Wait connection on %d...", SERVER_PORT_LAN);
			lcd_flip();
			while (1){
				if (Kb_Hit()){
					if (0x1B == kb_getkey())
						break;
				}

				iclient_socket = tcp_accept(iserver_socket, client_addr, &client_port);
				if (iclient_socket < 0)
					continue;
				else {
					lcd_printf_ex(ALG_LEFT, "Connected from %s:%d...", client_addr, client_port);
					lcd_printf_ex(ALG_LEFT, "start to ssl hand shake...");
					SSL_set_fd (ssl, iclient_socket);
					if ( SSL_accept (ssl) < 0){
						tcp_close(iclient_socket);
						iclient_socket = -1;
						lcd_printf_ex(ALG_LEFT, "SSL_accept() failed.");
						lcd_flip();
						continue;
					}else if (X509_V_OK==SSL_get_verify_result(ssl)){
						int flags = fcntl(iclient_socket, F_GETFL, 0);
						fcntl(iclient_socket, F_SETFL, flags | O_NONBLOCK);

						printf("Certificate verify success\n");

					}else {
						printf("Certificate verify fail\n");
						tcp_close(iclient_socket);
						iclient_socket = -1;
						lcd_printf_ex(ALG_LEFT, "SSL_get_verify_result() failed.");
						lcd_flip();
						continue;
					}
				}


				//Receive and send data
				while (1){
					if (Kb_Hit()){
						if (0x1B == kb_getkey())
							break;
					}
					iTemp = sizeof(bTemp);
					error =  SSL_read(ssl, bTemp, iTemp);
					if (error >= 0){
						iTemp = error;
						if (iTemp > 0)
						{
							lcd_printf_ex(ALG_LEFT, "Readed %d bytes", iTemp);
							lcd_flip();
							error = SSL_write(ssl, bTemp, iTemp);
							if (error != 0){
								lcd_printf_ex(ALG_LEFT, "connection was broken");
								lcd_flip();
								break;
							}
						}
						else {
							usleep(100000);
						}
					}else {
						lcd_printf_ex(ALG_LEFT, "connection was broken");
						lcd_flip();
						break;
					}
				}
				if (iclient_socket >=0){
					tcp_close(iclient_socket);
					iclient_socket = -1;
				}
				lcd_printf_ex(ALG_LEFT, "Wait connection on %d...", SERVER_PORT_LAN);
				lcd_flip();
			}


			tcp_close(iserver_socket);
			iserver_socket = -1;
		}else {
			lcd_printf_ex(ALG_LEFT, "Create server socket faile.");
			lcd_flip();
			kb_getkey();
		}

	}

	if (NULL != ssl){
		SSL_free (ssl);
	}

	if (NULL != ctx){
		SSL_CTX_free (ctx);
	}

}
void ssl_client_demo(void){
	int retval = 0;
	int iclient_socket = -1;
	unsigned char bTemp[1024];
	unsigned int  iTemp;
	SSL_CTX* ctx = NULL;
	SSL* ssl = NULL;

	const SSL_METHOD *meth;
	int error;


	lcd_clean();


	SSL_load_error_strings();
	OpenSSL_add_ssl_algorithms();
	meth = TLSv1_client_method();


	ctx = SSL_CTX_new (meth);
	ssl = SSL_new (ctx);

	if ((NULL == ctx) ||
			(NULL == ssl)){
		lcd_printf_ex(ALG_LEFT, "SSL_CTX_new() or SSL_new() failed");
		lcd_flip();
		kb_getkey();
		retval = -1;
	}else{
		SSL_CTX_set_verify(ctx,SSL_VERIFY_PEER,NULL);
		SSL_CTX_set_verify_depth(ctx,1);

		ctx->default_passwd_callback_userdata = NULL;
		if (SSL_CTX_load_verify_locations(ctx,CA_CERT,NULL)<=0)
		{
			lcd_printf_ex(ALG_LEFT, "SSL_CTX_load_verify_locations() failed");
			ERR_print_errors_fp(stderr);
			retval = -1;
			lcd_flip();
			kb_getkey();
		}
		else if (SSL_CTX_use_certificate_file(ctx, CLIENT_CERT, SSL_FILETYPE_PEM) <= 0) {
			lcd_printf_ex(ALG_LEFT, "SSL_CTX_use_certificate_file() failed");
			ERR_print_errors_fp(stderr);
			retval = -1;
			lcd_flip();
			kb_getkey();
		}else if (SSL_CTX_use_PrivateKey_file(ctx, CLIENT_KEY, SSL_FILETYPE_PEM) <= 0) {
			ERR_print_errors_fp(stderr);
			retval = -1;
		}else if (!SSL_CTX_check_private_key(ctx)) {
			printf("Private key does not match the certificate public key\n");
			lcd_printf_ex(ALG_LEFT, "SSL_CTX_check_private_key() failed");
			lcd_flip();
			kb_getkey();
			retval = -1;
		}else
			SSL_CTX_set_cipher_list(ctx,"RC4-MD5");
	}

	if (0 == retval){
		lcd_clean();
		lcd_printf_ex(ALG_CENTER, "ssl client");

		lcd_printf_ex(ALG_LEFT, "connect to %s:%d...", SERVER_ADDR, SERVER_PORT_LAN);
		lcd_flip();
		iclient_socket = tcp_connect(SERVER_ADDR, SERVER_PORT_LAN);
		if (iclient_socket < 0){
			lcd_printf_ex(ALG_LEFT, "Connect failed.");
			lcd_flip();
			kb_getkey();
			retval = -1;
		}else {
			lcd_printf_ex(ALG_LEFT, "start to ssl hand shake...");
			SSL_set_fd (ssl, iclient_socket);
			if ( SSL_connect (ssl) < 0){
				tcp_close(iclient_socket);
				iclient_socket = -1;
				lcd_printf_ex(ALG_LEFT, "SSL_connect() failed.");
				lcd_flip();
				retval = -1;
				kb_getkey();
			}else if (X509_V_OK==SSL_get_verify_result(ssl)){
				int flags = fcntl(iclient_socket, F_GETFL, 0);
				fcntl(iclient_socket, F_SETFL, flags | O_NONBLOCK);

				printf("Certificate verify success\n");

			}else {
				printf("Certificate verify fail\n");
				tcp_close(iclient_socket);
				iclient_socket = -1;
				lcd_printf_ex(ALG_LEFT, "SSL_get_verify_result() failed.");
				lcd_flip();
				retval = -1;
				kb_getkey();
			}
		}


		while (0 == retval){
			if (Kb_Hit()){
				if (0x1B == kb_getkey())
					break;
			}
			strcpy((char*)bTemp, "HELLO WORLD");
			iTemp = strlen((char*)bTemp);
			if (SSL_write(ssl , bTemp, iTemp) < 0){
				lcd_printf_ex(ALG_LEFT, "connection was broken");
				lcd_flip();
				kb_getkey();
				break;
			}else{
				usleep(500000);
				iTemp = sizeof(bTemp);
				error = SSL_read(ssl, bTemp, iTemp);
				if (error < 0){
					lcd_printf_ex(ALG_LEFT, "connection was broken");
					lcd_flip();
					kb_getkey();
					break;
				}else if (error > 0){
					lcd_printf_ex(ALG_LEFT, "Readed %d bytes", error);
				}else {

				}
			}
		}
		tcp_close(iclient_socket);
		iclient_socket = -1;
	}
	if (NULL != ssl){
		SSL_free (ssl);
	}

	if (NULL != ctx){
		SSL_CTX_free (ctx);
	}
}

#endif
