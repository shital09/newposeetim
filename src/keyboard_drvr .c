//#include "cstc_defines.h"
#include "keyboard_drvr.h"
#include <posapi.h>
#include "cstclog.h"

//#include <stdarg.h>
//#include "string_convert.h"
static int reqkeyIndex = 1,alphakey=0;//key & 0x0F;
//static struct tm	pretm;
static struct timeval	pretmvl;
static unsigned long long pretmvl_micro_sec=0;

const char keyLevelArr[5][10]={{0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39},
		{'.','Q',  'A',  'D', 'G', 'J','M', 'P', 'T', 'W'},
		{',','Z',  'B',  'E', 'H', 'K','N', 'R', 'U', 'X'},
		{'*','0',  'C',  'F', 'I', 'L','O', 'S', 'V', 'Y'},
		{'#','0',  '0',  '0', '0', '0','0', '0', '0', '0'}};

//int kb_hit(void)
//{
//	DFBWindowEvent  windowEvent;
//	DFBResult ret;
//
//	while(1){
//		if (events->HasEvent(events) == DFB_OK){
//			if (events->PeekEvent(events, DFB_EVENT(&windowEvent)) == DFB_OK) {
//				if (/*windowEvent.type == DWET_KEYDOWN || */ windowEvent.type == DWET_KEYUP)
//					return windowEvent.type;
//
//				ret = events->GetEvent(events, DFB_EVENT(&windowEvent));
//				if(ret){
//					DirectFBError("IDirectFBEventBuffer::GetEvent() failed", ret);
//				}
//			}
//		} else {
//			break;
//		}
//	}
//
//	return 0;
//}
//
// int kb_getkey(void)
//{
//	DFBWindowEvent windowEvent;
//	DFBResult ret;
//	while (1){
//		events->WaitForEvent(events);
//		if ((ret = events->GetEvent(events, DFB_EVENT(&windowEvent))) == DFB_OK) {
//			if (windowEvent.type == DWET_KEYUP)
//					return windowEvent.key_symbol;
//		} else {
//			break;
//		}
//	}
//
//	return 0;
//}
//


unsigned short int getKeySymbol(unsigned short int key, int shiftKeyLevel,int preKeyFlag)
{
	int keyIndex = key & 0x0F;
	unsigned int reqKey=0, range =4;
	//	time_t ti = time(NULL);
	unsigned long long total_micro_seconds=0;
	unsigned long long limit_time_micros=3000000; //micrseconds
	struct timeval	ti,tmsec;
	struct tm	*nowtm = localtime(&(ti.tv_sec));
	ti.tv_sec= time(NULL);
	ti.tv_usec=ti.tv_sec*1000;
	tmsec.tv_sec=2; //1;
	tmsec.tv_usec=100;//pretmvl
	if((keyIndex>1)&&(keyIndex<10))
		range =3;
	else if(keyIndex==1)
		range =2;
	if(preKeyFlag)
	{
		if(shiftKeyLevel>0)
		{
			ti.tv_sec=time(NULL);
			nowtm =localtime(&(ti.tv_sec));
//			ti.tv_usec=ti.tv_sec*1000;
			ti.tv_usec=time(NULL);
			ti.tv_sec=time(NULL);
			gettimeofday(&ti, NULL);
//			CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
//			CstcLog_printf("%ld:::%ld", pretmvl.tv_usec,ti.tv_usec);
//			CstcLog_printf("%ld:::%ld",(ti.tv_sec-pretmvl.tv_sec),tmsec.tv_usec);

			total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;
			if((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))
			{
				alphakey = 0;reqkeyIndex = 1;
				pretmvl.tv_sec=ti.tv_sec;
				pretmvl.tv_usec=ti.tv_usec;
				pretmvl_micro_sec = total_micro_seconds;
				CstcLog_printf("Cleared Alhpa %02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon , nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
			}
//			if((ti.tv_sec-pretmvl.tv_sec)>=tmsec.tv_sec)
//			{
//				alphakey = 0;
//				pretmvl.tv_sec=ti.tv_sec;
//				pretmvl.tv_usec=ti.tv_usec;
//				CstcLog_printf("%02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon + 1, nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
//			}
			if(alphakey==0)
			{
				alphakey=key & 0x0F;
				reqkeyIndex=1;
				reqKey=keyLevelArr[reqkeyIndex][keyIndex];
				CstcLog_printf("get NEW key reqKey Alhpa %c",reqKey);
			}
			else if(alphakey==keyIndex)//(key & 0x0F))
			{
				//keyIndex = key & 0x0F;
				if(reqkeyIndex<range)
					reqkeyIndex++;
				else
					reqkeyIndex=1;
				reqKey=keyLevelArr[reqkeyIndex][keyIndex];
				CstcLog_printf("get next level key reqKey Alhpa %c",reqKey);
			}
			else
			{
				reqkeyIndex=1;
				reqKey=keyLevelArr[reqkeyIndex][keyIndex];
				alphakey=keyIndex;//key & 0x0F;
				CstcLog_printf("get Next NEW key reqKey Alhpa %c",reqKey);
			}
		}
		else
			reqKey=keyLevelArr[reqkeyIndex][keyIndex];
		CstcLog_printf("get key reqKey Alhpa %c",reqKey);
		return reqKey;
	}
	else
	{
		ti.tv_usec=time(NULL);
		ti.tv_sec=time(NULL);
		gettimeofday(&ti, NULL);
//			CstcLog_printf("%ld:::%ld", pretmvl.tv_sec,ti.tv_sec);
//			CstcLog_printf("%ld:::%ld", pretmvl.tv_usec,ti.tv_usec);
//			CstcLog_printf("%ld:::%ld",(ti.tv_sec-pretmvl.tv_sec),tmsec.tv_usec);

		total_micro_seconds = ((ti.tv_sec * 1000000) + ti.tv_usec)/1000;
		if(((total_micro_seconds-pretmvl_micro_sec)>=(limit_time_micros/1000))||(alphakey!=keyIndex))
		{
			alphakey = 0;reqkeyIndex = 1;
			pretmvl.tv_sec=ti.tv_sec;
			pretmvl.tv_usec=ti.tv_usec;
			pretmvl_micro_sec = total_micro_seconds;
			CstcLog_printf("Cleared Alhpa %02d/%02d/%d %02d:%02d:%02d", nowtm->tm_mday, nowtm->tm_mon , nowtm->tm_year + 1900, nowtm->tm_hour, nowtm->tm_min, nowtm->tm_sec);
		}
		reqKey=keyLevelArr[reqkeyIndex][alphakey];
		CstcLog_printf("pre- reqKey Alhpa %c",reqKey);
		return reqKey;
	}

}

int changeStrUpperToLower(char *str)
{
	int i=0;
	if(strlen(str)>0)
	{
		for(i=0;i<=strlen(str);i++){
			if(str[i]>=65&&str[i]<=90)
				str[i]=str[i]+32;
		}
		return 1;
	}
	return 0;

}
