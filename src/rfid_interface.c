
#include <posapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <math.h>
#include "keyboard_drvr.h"
#include "cstc_defines.h"
#include "string_defines.h"
#include "db_interface.h"
#include "display_interface.h"
#include "gprs_interface.h"
#include "thread_interface.h"
#include "lan_interface.h"
#include "printer_interface.h"
#include "uart_interface.h"
#include "System_information.h"
#include "rfid_interface.h"

#include <stdio.h>
#include<error.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <curl/curl.h>

#include <mifone.h>
#include <iso14443.h>
#include "serial_receive.h"
#include <desfire.h>
//#include "seos.h"
#include "iccard.h"

//#define static_auth 0

int fd_des=-1;
int fd_sam = -1;
int fd = -1;
uint8_t uid_len;
uint8_t uid_data[20];
uint8_t cid;
uint8_t fsdi;
uint8_t atslen;
uint8_t ats[70]={0}; // 64 bytes
uint8_t br;
uint8_t num;
uint8_t aid[3]={0}; // 64 bytes
desfire_version des_ver;
unsigned char bApduResp[300];
int auth_sucess_flg=0;

/*-------------------------- Mutex Used ---------------------------------*/
extern pthread_mutex_t db_mutex;
extern pthread_mutex_t upload_semaphore_waybill ;
extern pthread_mutex_t upload_semaphore_schedule ;
//extern pthread_mutex_t printing_mutex;
extern pthread_mutex_t GPRS_init_deinit_mutex;

extern etim_rw_keys_struct etim_rw_keys_struct_var;
extern conductor_pass_registration_struct conductor_pass_registration_struct_var;
extern schedule_master_struct schedule_master_struct_var;
extern transaction_log_struct  temp_transaction_log_struct_var;
extern waybill_master_struct waybill_master_struct_var;
extern duty_master_struct duty_master_struct_var;
extern duty_master_struct * full_duty_master_struct_var;
extern fare_chart_struct fare_chart_struct_var;
extern transaction_log_struct transaction_log_struct_var;
extern waybill_wise_info_struct waybill_wise_info_struct_var;
extern trip_details_struct trip_details_struct_var;
extern selected_trip_info_struct selected_trip_info_struct_var;
extern current_passengers_details_struct current_passengers_details_struct_var;
extern all_flags_struct all_flags_struct_var;
extern ticket_display_struct ticket_display_struct_var;
extern trip_end_flag_struct trip_end_flag_struct_var;
extern cancel_menu_flag_struct cancel_menu_flag_struct_var;
extern bus_service_struct bus_service_struct_var;
extern depot_struct depot_struct_var;
extern current_selected_trip_stops_struct *current_selected_trip_stops_struct_var;
extern current_selected_trip_stops_struct *line_checking_current_selected_trip_stops_struct_var;
extern ticket_type_struct ticket_type_struct_var;
extern conductor_pass_registration_struct conductor_pass_registration_struct_var;
extern etim_rw_keys_struct etim_rw_keys_struct_var;
extern transaction_log_struct gprs_transaction_log_struct_var;
extern pass_select_flag_struct pass_select_flag_struct_var;
extern lc_login_struct  lc_login_struct_var;
extern apn_name_struct apn_name_struct_var;
extern route_master_struct route_master_struct_var;
extern concession_type_struct concession_type_struct_var;
extern concession_type_struct *full_concession_type_struct_var;
extern shift_type_struct shift_type_struct_var;
extern shift_type_struct *full_shift_type_struct_var;
extern route_card_struct route_card_struct_var;
extern rfid_smartcard_struct rfid_smartcard_struct_var;

extern int system_poweroff(void);


/********************************************************************************
 *  Function Name:  nfc_Init
 *  Description:
 *                  Initialize NFC
 *
 * Parameters : Refer below		    RETURN VALUE    : 1 : Sucess
 *													  0 : Failure
 *********************************************************************************/
int nfc_Init(void)
{
	int 	ret=0,key=0;
	int 	recv_len;
	int 	i;
	char recv_data[256];
	/*uint8_t uid_len;
		uint8_t uid_data[20];*/
	int     Disp_uid[40];

	CstcLog_printf("Mifare One Card Test");

	fd=-1;

	/* Open device */
	fd = mif_open("/dev/nfc");
	if (fd < 0) {
		CstcLog_printf("Open nfc failed! errno:%d", errno);
		//		goto close_exit;
		/* close device */
		CstcLog_printf("nfc closed!");
		mif_close(fd);
		return 0;
	}
	else
	{

		/* Detect Card */
		CstcLog_printf("Open nfc OK! Please swipe card...fd = %d",fd);
		while (1) {
			lcd_clean();
			lcd_printf_ex(ALG_CENTER,20,70,"READING CARD...");
			lcd_flip();
			ret = compat_InListPassiveTarget(fd, 0, 0, NULL, &recv_len, recv_data);
			if (!ret) {
				CstcLog_printf("Find card!");
				uid_len = recv_data[4];
				memcpy(uid_data, &recv_data[5], uid_len);
				CstcLog_printf("UID:");
				for (i = 0; i < uid_len; i++) {
					CstcLog_printf("%02x", uid_data[i]);
				}

				PubBcd2Asc((char*)uid_data,uid_len,(char*)Disp_uid);

				//			lcd_printf(ALG_CENTER,"UID: %s ",Disp_uid);
				//			lcd_flip();
				//	CstcLog_printf("\r\n");

				/*******************copying rfid card id to global struct****************/
				sprintf(conductor_pass_registration_struct_var.card_id_no,"%s",(char*)Disp_uid);
				conductor_pass_registration_struct_var.card_id_no[CARD_ID_LEN]='\0';
				break;
			}

			key = Kb_Get_Key_Sym_new();
			//			key = Kb_Get_Key_Sym();
			if(key==KEY_SYM_ESCAPE)
			{
				mif_close(fd);
				CstcLog_printf("mif closed here...");
				return 0;
			}
		}
	}
	return 1;
}



/********************************************************************************
 *  Function Name:  conductor_authentication
 *  Description:
 *                  Login authentication
 *
 * Parameters : None			    RETURN VALUE    : 1 : Authorized
 *													  0 : Unauthorized
 *********************************************************************************/
int conductor_authentication()
{
	//	int 	fd = -1;
	int 	i=0,ret=0,sector_id=0,block_cnt=3;
	int  block_arr[]={1,5,8};       //VERY IMP : block numbers used for verification
	char recv_data[256]={0};

	//uint8_t uid_len;
	//uint8_t uid_data[20];
	CstcLog_printf("In conductor_authentication............");

	if(!nfc_Init())
	{
		CstcLog_printf("Failed to Init RFID Module");
		return 0;
	}
	for(i=0;i<block_cnt;i++)
	{
		switch(block_arr[i])
		{
		case 1:
			sector_id=1;
			break;
		case 5:
			sector_id=2;
			break;
		case 8:
			sector_id=3;
			break;
		default :
			sector_id=-1;
			break;
		}

		if(sector_id >-1)
		{
			memset(recv_data,0,256);
			if(get_key_function(sector_id)==1)
			{
				CstcLog_printf("auth_key_a = %s",etim_rw_keys_struct_var.auth_key_a);
				CstcLog_printf("auth_key_b = %s",etim_rw_keys_struct_var.auth_key_b);

				if (uid_len == 4)
				{
					ret = read_write_rfid(fd, block_arr[i], 'B', etim_rw_keys_struct_var.auth_key_b, (char*) &uid_data[0],recv_data,1);
					CstcLog_printf(" block_arr[i]= %d data= %s",block_arr[i],recv_data);
				}
				else
				{
					ret = read_write_rfid(fd, block_arr[i], 'B', etim_rw_keys_struct_var.auth_key_b, (char*) &uid_data[3],recv_data,1);
					CstcLog_printf(" block_arr[i]= %d data= %s",block_arr[i],recv_data);
				}
				get_read_rfid_data_to_struct(block_arr[i],recv_data);
			}
			else
			{
				/* close device */
				CstcLog_printf("nfc closed!");
				mif_close(fd);
				return 0;
			}
		}
	}
	/* close device */
	CstcLog_printf("nfc closed!");
	mif_close(fd);
	if(verify_data())
	{
		CstcLog_printf("Authentication Successful");
		return 1;
	}
	else
	{
		CstcLog_printf("Authentication Failed");
		return 0;

	}
}

/********************************************************************************
 *  Function Name:  get_read_rfid_data_to_struct
 *  Description:
 *                  Copy the data read from RFID card into the struct
 *
 * Parameters : Refer below		    RETURN VALUE    : 1
 *
 *********************************************************************************/
int get_read_rfid_data_to_struct(uint8_t block_no,char* read_data)
{
	CstcLog_printf("In get_read_rfid_data_to_struct..." );

	CstcLog_printf("block_no= %d",block_no);
	CstcLog_printf("read_data= %s",read_data);

	switch(block_no)
	{
	case 1: strcpy(conductor_pass_registration_struct_var.corp_name,read_data);
	CstcLog_printf("rfid_corp_name= %s",conductor_pass_registration_struct_var.corp_name);
	break;
	case 5: strcpy(conductor_pass_registration_struct_var.pass_type_name,read_data);
	CstcLog_printf("pass_type_name= %s",conductor_pass_registration_struct_var.pass_type_name);
	break;
	case 8:		strcpy(conductor_pass_registration_struct_var.conductor_id,read_data);
	CstcLog_printf("conductor_id= %s",conductor_pass_registration_struct_var.conductor_id);
	break;
	default :
		break;
	}
	return 1;
}

/********************************************************************************
 *  Function Name:  read_write_rfid
 *  Description:
 *                  RFID card/key block authentication
 *
 * Parameters : Refer below		    RETURN VALUE    : 1 : Sucess
 * 													  0 : Failure
 *********************************************************************************/
int read_write_rfid(int fd, uint8_t cardno_block_no, uint8_t keytype,const void *key,char *uid_data,char *data,int read_flag)
{
	int ret=0;
	//	char msg[500]={0};
	//	uint8_t key_b_card_no[6] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};


	ret = mifare_one_authenticate(fd, cardno_block_no, keytype, key, uid_data);

	if (ret)
	{
		//		sprintf(msg,"auth card fail");
		//		Show_Error_Msg("CARD AUTHENTICATION FAILED");
		CstcLog_printf("authenticate Card Number failed, ret: %d ", ret);
		return 0;
	}
	CstcLog_printf("authenticate Card Number success");

	if(read_flag)
	{
		ret= mifare_one_read_block(fd,cardno_block_no,data);
	}
	else
	{
		ret= mifare_one_write_block(fd,cardno_block_no,data);
	}
	if (ret)
	{
		CstcLog_printf("block access failed, ret:%d errno: %d", ret);
		return 0;
	}
	else
	{
		CstcLog_printf("block access success...block_no=%d",cardno_block_no);

	}

	return 1;
}


/********************************************************************************
 *  Function Name:  verify_data
 *  Description:
 *                  Check if card data is same as table data
 *
 * Parameters : None			    RETURN VALUE    : 1 : Sucess
 * 													  0 : Failure
 *********************************************************************************/
int verify_data(void)
{
	char sql[50]={'\0'};
	CstcLog_printf("*********In verify_data*********");

	//remove white spaces---------------------------------------
	RemoveSpaces(conductor_pass_registration_struct_var.corp_name);
	RemoveSpaces(conductor_pass_registration_struct_var.pass_type_name);
	RemoveSpaces(conductor_pass_registration_struct_var.conductor_id);
	//-----------------------------------------------------------
	CstcLog_printf("conductor_pass_registration_struct_var.corp_name= [%s]",conductor_pass_registration_struct_var.corp_name);
	CstcLog_printf("conductor_pass_registration_struct_var.pass_type_name= [%s]",conductor_pass_registration_struct_var.pass_type_name);
	CstcLog_printf("conductor_pass_registration_struct_var.conductor_id= [%s]",conductor_pass_registration_struct_var.conductor_id);

	if(strcmp(conductor_pass_registration_struct_var.corp_name,"CSTC")==0)   // data from the live database
	{
		CstcLog_printf("Corp name verified");
		if(strcmp(conductor_pass_registration_struct_var.pass_type_name,"11")==0)   // data from the live database
		{
			CstcLog_printf("Depot name verified");
			if(all_flags_struct_var.line_check_auth_flag) // LC card
			{
				sprintf(sql,"lc_login where token_no LIKE '%s'",conductor_pass_registration_struct_var.conductor_id);	// LIKE used because it can be upper case or lower case
				if(DbInterface_Get_Row_Count(sql,strlen(sql),0))
				{
					get_lc_name();
					CstcLog_printf("got lc name successfully........");
					return 1;
				}
			}
			else // normal card
			{
				if(strcmp(conductor_pass_registration_struct_var.conductor_id,waybill_master_struct_var.conductor_id)==0)                                              // data from the live database
				{
					return 1;
				}
			}
		}
	}

	return 0;
}



/********************************************************************************
 *  Function Name:  conductor_registration
 *  Description:
 *                  Conductor card registration main function
 *
 * Parameters : None			    RETURN VALUE    : 1/0
 *
 *********************************************************************************/
int conductor_registration(void)
{
	//	int 	fd = -1;
	int 	i,sector_id;
	char dataBuff[256];
	uint8_t key_a_card_no[6];
	uint8_t key_b_card_no[6];// = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	int 	cardno_block_no  = 1;
	int 	ret;//,retval=0;
	//uint8_t uid_len;
	//uint8_t uid_data[20];
	CstcLog_printf("In conductor_registration............");

	if(conductor_entry())
	{
		if(!nfc_Init())
		{
			CstcLog_printf("Failed to Init RFID Module");
			Show_Error_Msg("FAILED TO INIT RFID MODULE");
			beep(2800,1200);
			return 0;
		}

		//		for(i=0;i<0x30;i++)
		for(i=1;i<11;i++)
		{
			memset(dataBuff,0,256);
			switch(i)
			{
			case 1:
				cardno_block_no=1;
				sector_id=1;
				strcpy(dataBuff,CORP_NAME);
				break;
			case 5:
				cardno_block_no=5;
				sector_id=2;
				strcpy(dataBuff,conductor_pass_registration_struct_var.pass_type_name);
				break;
			case 6:
				cardno_block_no=6;
				sector_id=2;
				strcpy(dataBuff,conductor_pass_registration_struct_var.conductor_type);
				break;
			case 8:
				cardno_block_no =8;
				sector_id=3;
				strcpy(dataBuff,conductor_pass_registration_struct_var.conductor_id);
				break;
			case 9:
				cardno_block_no=9;
				sector_id=3;
				strcpy(dataBuff,conductor_pass_registration_struct_var.conductor_name);
				break;
			case 10:
				cardno_block_no=10;
				sector_id=3;
				strcpy(dataBuff,conductor_pass_registration_struct_var.conductor_password);
				break;
			default :
				cardno_block_no=-1;
				sector_id=-1;
				break;
			}

			if(sector_id>-1)
			{
				get_key_function(sector_id);

				CstcLog_printf("auth_key_a = %s",etim_rw_keys_struct_var.auth_key_a);
				CstcLog_printf("auth_key_b = %s",etim_rw_keys_struct_var.auth_key_b);

				//	strcpy(&key_b_card_no,etim_rw_keys_struct_var.auth_key_a);
				memcpy(key_a_card_no,etim_rw_keys_struct_var.auth_key_a,strlen(etim_rw_keys_struct_var.auth_key_a));
				memcpy(key_b_card_no,etim_rw_keys_struct_var.auth_key_b,strlen(etim_rw_keys_struct_var.auth_key_b));
			}
			if(cardno_block_no >-1)
			{
				if (uid_len == 4)
				{
					ret = read_write_rfid(fd, cardno_block_no, 'B', key_b_card_no, (char*) &uid_data[0],dataBuff,0);
				}
				else
				{
					ret = read_write_rfid(fd, cardno_block_no, 'B', key_b_card_no, (char*) &uid_data[3],dataBuff,0);
				}
				if(ret)
				{
					//					check=1;
					CstcLog_printf("writing successful .....block no= %d  dataBuff= %s ",cardno_block_no,dataBuff);
				}
				else
				{
					CstcLog_printf("writing failed ...block no= %d  dataBuff= %s ",cardno_block_no,dataBuff);
					Show_Error_Msg("REGISTRATION FAILED");
					beep(2800,1200);
					mif_close(fd);
					return 0;
				}
				//				Show_Error_Msg("card write successful");
			}
		}
		CstcLog_printf("nfc closed!");
		mif_close(fd);
		//		if(check)
		//		{
		DbInterface_transaction_begin("BEGIN TRANSACTION;",0);
		if(!get_registration_log())
		{
			DbInterface_transaction_commit("COMMIT;",0);
			Show_Error_Msg("REGISTRATION SUCCESSFUL");
			CstcLog_printf(" REGISTRATION SUCCESSFUL");
		}
		else
		{
			Show_Error_Msg("REGISTRATION UNSUCCESSFUL");
			CstcLog_printf("REGISTRATION UNSUCCESSFUL");
			beep(2800,1200);
		}
		//		}
	}
	CstcLog_printf("End of conductor_registration............");
	return 1;
}
/*************************************************************************************/

int read_epurse_card_details(void)
{
	//	int fd = -1;
	int i=0,ret=0,sector_id=0,block_cnt=11;
	int read_block_arr[]={1,4,9,10,18,24,28,25,26,29,30};       //VERY IMP : block numbers (in hex) used for verification
	//	int write_block_arr[]={};
	char recv_data[256]={'\0'};
	//	char temp_data[200]={0};
	//	char balance_money[AMOUNT_LEN+1]={0};

	/*uint8_t uid_len;
	uint8_t uid_data[20];*/
	CstcLog_printf("In read_epurse_card_details............");

	if(!nfc_Init())
	{
		CstcLog_printf("Failed to Init RFID Module");
		return 0;
	}
	for(i=0;i<block_cnt;i++)
	{
		switch(read_block_arr[i])
		{
		case 1:
			sector_id=0;
			break;
		case 4:
			sector_id=1;
			break;
		case 9:
		case 10:
			sector_id=2;
			break;
		case 18:
			sector_id=4;
			break;
		case 24:
		case 25:
		case 26:
			sector_id=6;
			break;
		case 28:
			sector_id=7;
			break;
		default :
			sector_id=-1;
			break;
		}
		if(sector_id >-1)
		{
			memset(recv_data,0,256);
			if(get_key_function(sector_id)==1)
			{
				CstcLog_printf("auth_key_a = %s",etim_rw_keys_struct_var.auth_key_a);
				CstcLog_printf("auth_key_b = %s",etim_rw_keys_struct_var.auth_key_b);
				if (uid_len == 4)
				{
					ret = read_write_rfid(fd, read_block_arr[i], 'B', etim_rw_keys_struct_var.auth_key_b, (char*) &uid_data[0],recv_data,1);
				}
				else
				{
					ret = read_write_rfid(fd, read_block_arr[i], 'B', etim_rw_keys_struct_var.auth_key_b, (char*) &uid_data[3],recv_data,1);
				}
				CstcLog_printf(" read_block_arr[%d] : data= %s",read_block_arr[i],recv_data);

				if(!ret){
					CstcLog_printf("Returning due to read_write_rfid() func failed...!!");
					Show_Error_Msg("CARD READ FAILED");
					beep(2800,1200);
					mif_close(fd);
					CstcLog_printf("Card Read Failed....nfc closed!");
					return 0;
				}

				// verify_epurse_card Data  --  remaining
				// verify corporation_name,pass_type

				get_read_rfid_data_to_struct_epurse(read_block_arr[i],recv_data);  //epurse_card_data_read
			}
			else
			{
				/* close device */
				Show_Error_Msg("CARD KEYS READ FAILED");
				beep(2800,1200);
				mif_close(fd);
				CstcLog_printf("Card Keys Read Failed....nfc closed!");
				return 0;
			}
		}
	}
	/* close device */
	//	CstcLog_printf("nfc closed!");
	//	mif_close(fd);


	/*	sprintf(temp_data,"Pass User :%s",rfid_smartcard_struct_var.Pass_user);
	Show_Error_Msg(temp_data);
	sleep(2);

	memset(temp_data,0,200);
	sprintf(temp_data,"PREVIOUS BALANCE : Rs %s/-",rfid_smartcard_struct_var.e_purse_amt);
	Show_Error_Msg(temp_data);
	sleep(2);*/

	Show_Error_Msg("CARD READ SUCCESS");
	sleep(1);

	/****check if data is valid *****/
	// balance_amt>ticket_amt
	CstcLog_printf("e_purse_amt = %s  totalAmt = %s",rfid_smartcard_struct_var.e_purse_amt,ticket_display_struct_var.totalAmt);
	if((atoi(rfid_smartcard_struct_var.e_purse_amt)<atoi(ticket_display_struct_var.totalAmt))||(atoi(rfid_smartcard_struct_var.e_purse_amt)<=0))
	{
		Show_Error_Msg("INSUFFICIENT BALANCE IN CARD");
		beep(2800,1200);
		CstcLog_printf("nfc closed!");
		mif_close(fd);
		return 0;
	}

	// card_expiry date should not be less than current date

	/******** deduct ticket amount *******/
	// balance = epurse_amt - ticket amt

	CstcLog_printf("temp_transaction_log_struct_var.total_ticket_amount = %s",temp_transaction_log_struct_var.total_ticket_amount);

	if((all_flags_struct_var.refund_flag)||(all_flags_struct_var.cancel_ticket_flag))
	{

		sprintf(rfid_smartcard_struct_var.e_purse_amt,"%d",(atoi(rfid_smartcard_struct_var.e_purse_amt)+atoi(temp_transaction_log_struct_var.total_ticket_amount)-atoi(ticket_display_struct_var.totalAmt)));
		CstcLog_printf("refund/Cancel epurse_amt calculation...cur_transaction_amt='%s'",rfid_smartcard_struct_var.e_purse_amt);

	}else
	{
		sprintf(rfid_smartcard_struct_var.e_purse_amt,"%d",(atoi(rfid_smartcard_struct_var.e_purse_amt)-atoi(ticket_display_struct_var.totalAmt)));
		CstcLog_printf("Normal epurse_amt calculation...cur_transaction_amt='%s'",rfid_smartcard_struct_var.e_purse_amt);

	}
	//	sprintf(balance_money,"%d",(atoi(rfid_smartcard_struct_var.e_purse_amt)-atoi(ticket_display_struct_var.totalAmt)));
	CstcLog_printf("balance = %s",rfid_smartcard_struct_var.e_purse_amt);

	/*	memset(temp_data,0,200);
	sprintf(temp_data,"BALANCE : Rs %s/-",rfid_smartcard_struct_var.cur_transaction_amt);
	Show_Error_Msg(temp_data);
    sleep(2);*/


#if 0
	/*********** reopen rfid card for write mode *****/
	/*	if(!nfc_Init(&fd,&uid_len,&uid_data[0]))
	{
		CstcLog_printf("Failed to Init RFID Module");
		return 0;
	}*/

	/****** write balance amount on epurse_card ******/
	{

		//		if(get_key_function(sector_id)==1)
		{
			CstcLog_printf("Writing balance amont in epurse card......");
			CstcLog_printf("auth_key_a = %s",etim_rw_keys_struct_var.auth_key_a);
			CstcLog_printf("auth_key_b = %s",etim_rw_keys_struct_var.auth_key_b);
			if (uid_len == 4)
			{
				ret = read_write_rfid(fd, 24, 'B', etim_rw_keys_struct_var.auth_key_b, (char*) &uid_data[0],rfid_smartcard_struct_var.cur_transaction_amt,0);
			}
			else
			{
				ret = read_write_rfid(fd,24, 'B', etim_rw_keys_struct_var.auth_key_b, (char*) &uid_data[3],rfid_smartcard_struct_var.cur_transaction_amt,0);
			}
			if(!ret)
			{
				CstcLog_printf("Data Write failed.. :(");
				Show_Error_Msg("CARD WRITE FAILED");
				beep(2800,1200);
				CstcLog_printf("nfc closed...!!");
				mif_close(fd);
				return 0;
			}

			Show_Error_Msg("CARD WRITE DONE");
			sleep(2);
			CstcLog_printf("Data Write Success ....for uid_len == 4");
			CstcLog_printf(" block_no=18 Balance Amount= %s",rfid_smartcard_struct_var.cur_transaction_amt);
		}
		/*		else
		{
			CstcLog_printf("nfc closed!");
			mif_close(fd);
			return 0;
		}*/
	}
#endif
	/* close device */
	//	CstcLog_printf("nfc closed!");
	//	mif_close(fd);
	return 1;
}


/********************************************************************************
 *  Function Name:  get_read_rfid_data_to_struct
 *  Description:
 *                  Copy the data read from RFID card into the struct
 *
 * Parameters : Refer below		    RETURN VALUE    : 1
 *
 *********************************************************************************/
int get_read_rfid_data_to_struct_epurse(uint8_t block_no,char* read_data)
{
	CstcLog_printf("In get_read_rfid_data_to_struct_epurse..." );
	CstcLog_printf("block_no= %d",block_no);
	CstcLog_printf("read_data= |%s|",read_data);
	CstcLog_printf("read_data_len= %d",strlen(read_data));

	remove_space(read_data,read_data);
	CstcLog_printf("read_data= |%s|",read_data);

	switch(block_no)
	{
	case 1:
		if(strlen(read_data)>CORP_NAME_LEN)
		{
			memcpy(rfid_smartcard_struct_var.name_corp,read_data,CORP_NAME_LEN);
			rfid_smartcard_struct_var.name_corp[CORP_NAME_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.name_corp,read_data);
		CstcLog_printf("Corporation Name = %s",rfid_smartcard_struct_var.name_corp);
		break;

	case 4:
		if(strlen(read_data)>CARD_ID_LEN)
		{
			memcpy(rfid_smartcard_struct_var.card_ID_No,read_data,CARD_ID_LEN);
			rfid_smartcard_struct_var.card_ID_No[CARD_ID_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.card_ID_No,read_data);
		CstcLog_printf("card_ID_No= %s",rfid_smartcard_struct_var.card_ID_No);
		break;

	case 9:
		if(strlen(read_data)>BUS_STOP_CODE_LEN)
		{
			memcpy(rfid_smartcard_struct_var.temp_from_stage_code,read_data,BUS_STOP_CODE_LEN);
			rfid_smartcard_struct_var.temp_from_stage_code[BUS_STOP_CODE_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.temp_from_stage_code,read_data);
		CstcLog_printf("temp_from_stage_code= %s",rfid_smartcard_struct_var.temp_from_stage_code);
		break;
	case 10:
		if(strlen(read_data)>BUS_STOP_CODE_LEN)
		{
			memcpy(rfid_smartcard_struct_var.temp_till_stage_code,read_data,BUS_STOP_CODE_LEN);
			rfid_smartcard_struct_var.temp_till_stage_code[BUS_STOP_CODE_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.temp_till_stage_code,read_data);
		CstcLog_printf("temp_till_stage_code= %s",rfid_smartcard_struct_var.temp_till_stage_code);
		break;
	case 18:
		if(strlen(read_data)>NAME_ENG_LEN)
		{
			memcpy(rfid_smartcard_struct_var.Pass_user,read_data,NAME_ENG_LEN);
			rfid_smartcard_struct_var.Pass_user[NAME_ENG_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.Pass_user,read_data);
		CstcLog_printf("Pass_user name = %s",rfid_smartcard_struct_var.Pass_user);
		break;
	case 24:
		if(strlen(read_data)>AMOUNT_LEN)
		{
			memcpy(rfid_smartcard_struct_var.e_purse_amt,read_data,AMOUNT_LEN);
			rfid_smartcard_struct_var.e_purse_amt[AMOUNT_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.e_purse_amt,read_data);
		CstcLog_printf("e_purse_amt= %s",rfid_smartcard_struct_var.e_purse_amt);

	case 25:
		if(strlen(read_data)>WAYBILL_NO_LEN)
		{
			memcpy(rfid_smartcard_struct_var.last_waybill_no,read_data,WAYBILL_NO_LEN);
			rfid_smartcard_struct_var.last_waybill_no[WAYBILL_NO_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.last_waybill_no,read_data);
		CstcLog_printf("last_waybill_no= %s",rfid_smartcard_struct_var.last_waybill_no);

	case 26:
		if(strlen(read_data)>TICKET_NO_LEN)
		{
			memcpy(rfid_smartcard_struct_var.last_ticket_no,read_data,TICKET_NO_LEN);
			rfid_smartcard_struct_var.last_ticket_no[TICKET_NO_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.last_ticket_no,read_data);
		CstcLog_printf("last_ticket_no= %s",rfid_smartcard_struct_var.last_ticket_no);
		break;
	case 28:
		if(strlen(read_data)>AMOUNT_LEN)
		{
			memcpy(rfid_smartcard_struct_var.cur_transaction_amt,read_data,AMOUNT_LEN);
			rfid_smartcard_struct_var.cur_transaction_amt[AMOUNT_LEN]='\0';
		}
		else
			strcpy(rfid_smartcard_struct_var.cur_transaction_amt,read_data);
		CstcLog_printf("cur_transaction_amt= %s",rfid_smartcard_struct_var.cur_transaction_amt);

	default :
		break;
	}
	return 1;
}



int update_epurse_card(void)
{
	//	int fd = -1;
	int ret=0;
	//	int write_block_arr[]={4,18,24};       //VERY IMP : block numbers used for verification
	//	int write_block_arr[]={};
	//	char recv_data[256]={0};
	//	char temp_data[200]={0};
	//	char balance_money[AMOUNT_LEN+1]={0};
	int sector_id=0,i=0;
	int block_cnt=5;
	int write_block_arr[]={/*24,*/9,10,28,25,26};       //VERY IMP : block numbers (in hex) used for verification
	char write_data[256]={0};

	CstcLog_printf("In update_epurse_card...........");


	/*	uint8_t uid_len;
	uint8_t uid_data[20];*/

#if 1
	/*********** reopen rfid card for write mode *****/
	/*	if(!nfc_Init(&fd,&uid_len,&uid_data[0]))
	{
		CstcLog_printf("Failed to Init RFID Module");
		return 0;
	}*/


	for(i=0;i<block_cnt;i++)
	{
		memset(write_data,0,256);
		switch(write_block_arr[i])
		{
		/*		case 24: strcpy(write_data,rfid_smartcard_struct_var.e_purse_amt);
//			sector_id=1;
			break;*/
		case 9:strcpy(write_data,transaction_log_struct_var.from_bus_stop_code_english);
		//sector_id=2;
		break;
		case 10:strcpy(write_data,transaction_log_struct_var.till_bus_stop_code_english);
		//sector_id=2;
		break;
		case 28:strcpy(write_data,transaction_log_struct_var.total_ticket_amount);
		//sector_id=2;
		break;
		case 25:strcpy(write_data,transaction_log_struct_var.waybill_no);
		//sector_id=3;
		break;
		case 26:sprintf(write_data,"%05d",atoi(transaction_log_struct_var.ticket_no));
		//sector_id=3;
		break;
		default :
			//sector_id=-1;
			break;
		}
		if(sector_id >-1)
		{
			if(get_key_function(sector_id)==1)
			{
				CstcLog_printf("auth_key_a = %s",etim_rw_keys_struct_var.auth_key_a);
				CstcLog_printf("auth_key_b = %s",etim_rw_keys_struct_var.auth_key_b);
				if (uid_len == 4)
				{
					ret = read_write_rfid(fd, write_block_arr[i], 'B', etim_rw_keys_struct_var.auth_key_b, (char*) &uid_data[0],write_data,0);
				}
				else
				{
					ret = read_write_rfid(fd, write_block_arr[i], 'B', etim_rw_keys_struct_var.auth_key_b, (char*) &uid_data[3],write_data,0);
				}
				CstcLog_printf(" write_block_arr[%d] : data= %s",write_block_arr[i],write_data);

				if(!ret){
					CstcLog_printf("Returning due to read_write_rfid() func failed...!!");
					Show_Error_Msg("CARD WRITE FAILED");
					beep(2800,1200);
					mif_close(fd);
					CstcLog_printf("Card Read Failed....nfc closed!");
					return 0;
				}
			}
			else
			{
				/* close device */
				Show_Error_Msg("CARD KEYS READ FAILED");
				beep(2800,1200);
				mif_close(fd);
				CstcLog_printf("Card Keys Read Failed....nfc closed!");
				return 0;
			}
		}
	}
	/* close device */
	CstcLog_printf("nfc closed!");
	mif_close(fd);
#endif
	return 1;
}
/********************************************************************************
 *  Function Name:  RemoveSpaces
 *  Description:
 *                  Remove any white spaces present in <source>
 *
 * Parameters : string			    RETURN VALUE    : None
 *
 *********************************************************************************/
void RemoveSpaces(char* source)
{
	char* i = source;
	char* j = source;
	while(*j != 0)
	{
		*i = *j++;
		if(*i != ' ')
			i++;
	}
	*i = 0;
}

#if 1
void PubBcd2Asc(char *psIHex, int iHexLen, char *psOAsc)
{
	static const char szMapTable[17] = {"0123456789ABCDEF"};
	int   iCnt,index;
	unsigned char  ChTemp;

	for(iCnt = 0; iCnt < iHexLen; iCnt++)
	{
		ChTemp = (unsigned char)psIHex[iCnt];
		index = (ChTemp / 16) & 0x0F;
		psOAsc[2*iCnt]   = szMapTable[index];

		ChTemp = (unsigned char) psIHex[iCnt];
		index = ChTemp & 0x0F;
		psOAsc[2*iCnt + 1] = szMapTable[index];
	}
}

#endif
/********************************************************************************
 *  Function Name:  conductor_entry
 *  Description:
 *                  Conductor registration entry screen
 *
 * Parameters : None			    RETURN VALUE    : retval
 *
 *********************************************************************************/
int conductor_entry(void)
{
	unsigned short int key=0;
	int exit =0,retval=0;
	int shiftKeyLevel=0,cursrFlag=1;
	char* conductor_id, *conductor_name,*conductor_pswd,*conductor_type,*licence_expiry_date;

	conductor_id = (char*)malloc(((COND_ID_LEN+1)*sizeof(char)));
	conductor_name = (char*)malloc(((CONDR_NAME_LEN+1)*sizeof(char)));
	conductor_pswd = (char*)malloc(((PSWRD_LEN+1)*sizeof(char)));
	conductor_type = (char*)malloc(((CONDR_TYPE+1)*sizeof(char)));
	licence_expiry_date= (char*)malloc(((DATE_LEN+1)*sizeof(char)));
	memset(conductor_id,0,(COND_ID_LEN+1));
	memcpy(conductor_id,"|",1);
	memset(conductor_name,0,(CONDR_NAME_LEN+1));
	memset(conductor_pswd,0,(PSWRD_LEN+1));
	memset(conductor_type,0,(CONDR_TYPE+1));
	memset(licence_expiry_date,0,(DATE_LEN+1));

	CstcLog_printf("In conductor_entry..............");

	//Add labels to the screen
	while(!exit)
	{
		key=0;
		display_conductor_entry_screen(conductor_id, conductor_name,conductor_pswd,conductor_type,licence_expiry_date, key,&cursrFlag,&shiftKeyLevel);

		if((key = Kb_Get_Key_Sym())>1)
		{
			if(key==KEY_SYM_ESCAPE)
			{
				exit =1;
				retval=0;
				key=0;
				break;
			}
			else if(key==KEY_SYM_ENTER)
			{
				if((strlen(conductor_id)==0) || (strlen(conductor_name)==0) || (strlen(conductor_pswd)==0)||(strlen(conductor_type)==0)||(strlen(licence_expiry_date)==0))
				{
					key=0;
					Show_Error_Msg("PLEASE ENTER ALL VALUES");
					beep(2800,1200);
				}
				else
					break;
			}
			else if(key == KEY_SYM_POWEROFF)
			{
				key=0;
				if(system_poweroff())
				{
					exit =1;
					retval=0;
					free(conductor_id);
					free(conductor_name);
					free(conductor_pswd);
					free(conductor_type);
					free(licence_expiry_date);
					curl_global_cleanup();
					pthread_mutex_destroy(&db_mutex);
					pthread_mutex_destroy(&upload_semaphore_schedule);
					pthread_mutex_destroy(&upload_semaphore_waybill);
					//					pthread_mutex_destroy(&GPRS_init_deinit_mutex);  12/06/15 temp solution for 10 min hang
					//					pthread_mutex_destroy(&printing_mutex);
					sys_poweroff();
				}
				break;
			}
			else if(key==KEY_SYM_CONTROL)
			{
				key=0;
				//				break;
			}
			else if(key==KEY_SYM_CURSOR_UP)
			{
				if(cursrFlag>1)
				{
					cursrFlag--;
				}
				if(cursrFlag==1)
				{
					if((strlen(conductor_id)<(COND_ID_LEN+1))&&(NULL==strchr(conductor_id,'|')))
						conductor_id[strlen(conductor_id)]='|';
					if(NULL!=strchr(conductor_name,'|'))
					{
						conductor_name[strlen(conductor_name)-1]='\0';
					}
				}

				else if(cursrFlag==2)
				{
					if((strlen(conductor_name)<(CONDR_NAME_LEN+1))&&(NULL==strchr(conductor_name,'|')))
						conductor_name[strlen(conductor_name)]='|';
					if(NULL!=strchr(conductor_pswd,'|'))
					{
						conductor_pswd[strlen(conductor_pswd)-1]='\0';
					}
				}

				else if(cursrFlag==3)
				{
					if((strlen(conductor_pswd)<(PSWRD_LEN+1))&&(NULL==strchr(conductor_pswd,'|')))
						conductor_pswd[strlen(conductor_pswd)]='|';
					if(NULL!=strchr(conductor_type,'|'))
					{
						conductor_type[strlen(conductor_type)-1]='\0';
					}
				}

				else if(cursrFlag==4)
				{
					if((strlen(conductor_type)<(CONDR_TYPE+1))&&(NULL==strchr(conductor_type,'|')))
						conductor_type[strlen(conductor_type)]='|';
					if(NULL!=strchr(licence_expiry_date,'|'))
					{
						licence_expiry_date[strlen(licence_expiry_date)-1]='\0';
					}
				}
				key=0;
			}
			else if(key==KEY_SYM_CURSOR_DOWN)
			{
				if(cursrFlag<5)
				{
					cursrFlag++;
				}
				if(cursrFlag==2)
				{
					if(NULL!=strchr(conductor_id,'|'))
					{
						conductor_id[strlen(conductor_id)-1]='\0';
					}

					if((strlen(conductor_name)<(CONDR_NAME_LEN+1))&&(NULL==strchr(conductor_name,'|')))
					{
						conductor_name[strlen(conductor_name)]='|';
					}
				}

				else if(cursrFlag==3)
				{
					if(NULL!=strchr(conductor_name,'|'))
						conductor_name[strlen(conductor_name)-1]='\0';
					if((strlen(conductor_pswd)<(PSWRD_LEN+1))&&(NULL==strchr(conductor_pswd,'|')))
					{
						conductor_pswd[strlen(conductor_pswd)]='|';
					}
				}

				else if(cursrFlag==4)
				{
					if(NULL!=strchr(conductor_pswd,'|'))
						conductor_pswd[strlen(conductor_pswd)-1]='\0';
					if((strlen(conductor_type)<(CONDR_TYPE+1))&&(NULL==strchr(conductor_type,'|')))
					{
						conductor_type[strlen(conductor_type)]='|';
					}
				}

				else if(cursrFlag==5)
				{
					if(NULL!=strchr(conductor_type,'|'))
						conductor_type[strlen(conductor_type)-1]='\0';
					if((strlen(licence_expiry_date)<(DATE_LEN+1))&&(NULL==strchr(licence_expiry_date,'|')))
					{
						licence_expiry_date[strlen(licence_expiry_date)]='|';
					}
				}
				key=0;
			}

			else if(key==KEY_SYM_SHIFT)
			{
				if(shiftKeyLevel)
					shiftKeyLevel=0;
				else
					shiftKeyLevel=1;
				key=0;
			}

			else if(!((key >= KEY_SYM_0) && (key <= KEY_SYM_9)) && (key != KEY_SYM_ALT)
					&& (key != KEY_SYM_SHIFT) && (key != KEY_SYM_ENTER) && (key != KEY_SYM_ESCAPE)
					&& (key != KEY_SYM_CONTROL) && (key != KEY_SYM_CURSOR_UP)&& (key != KEY_SYM_CURSOR_DOWN)
					&& (key != KEY_SYM_BACKSPACE)&& (key != KEY_SYM_POWEROFF))
				key  =0;

			display_conductor_entry_screen(conductor_id, conductor_name,conductor_pswd,conductor_type,licence_expiry_date, key,&cursrFlag,&shiftKeyLevel);
		}
	}
	if(key==KEY_SYM_ENTER)
	{
		if(NULL!=strchr(conductor_id,'|'))
			conductor_id[strlen(conductor_id)-1]='\0';
		if(NULL!=strchr(conductor_name,'|'))
			conductor_name[strlen(conductor_name)-1]='\0';
		if(NULL!=strchr(conductor_pswd,'|'))
			conductor_pswd[strlen(conductor_pswd)-1]='\0';
		if(NULL!=strchr(conductor_type,'|'))
			conductor_type[strlen(conductor_type)-1]='\0';
		if(NULL!=strchr(licence_expiry_date,'|'))
			licence_expiry_date[strlen(licence_expiry_date)-1]='\0';


		CstcLog_printf("Before memcpy....");
		CstcLog_printf("card_id copied in nfc_init()....");
		memcpy(conductor_pass_registration_struct_var.conductor_id,conductor_id,strlen(conductor_id));
		memcpy(conductor_pass_registration_struct_var.conductor_name,conductor_name, strlen(conductor_name));
		memcpy(conductor_pass_registration_struct_var.conductor_password,conductor_pswd, strlen(conductor_pswd));
		memcpy(conductor_pass_registration_struct_var.conductor_type,conductor_type, strlen(conductor_type));
		memcpy(conductor_pass_registration_struct_var.conductor_licn_expy_date,licence_expiry_date, strlen(licence_expiry_date));

		CstcLog_printf("rfid_conductor_id= %s",conductor_pass_registration_struct_var.conductor_id);
		CstcLog_printf("rfid_conductor_name= %s",conductor_pass_registration_struct_var.conductor_name);
		CstcLog_printf("rfid_conductor_password= %s",conductor_pass_registration_struct_var.conductor_password);
		CstcLog_printf("rfid_conductor_type= %s",conductor_pass_registration_struct_var.conductor_type);
		CstcLog_printf("licence_expiry_date= %s",conductor_pass_registration_struct_var.conductor_licn_expy_date);

		cursrFlag = 0;
		retval=1;
	}
	//	else if(key==KEY_SYM_CONTROL)
	//	{
	//		if(conductor_authentication())
	//		{
	//			retval=1;
	//		}
	//		else
	//		{
	//			retval=0;
	//		}
	//
	//	}
	free(conductor_id);
	free(conductor_name);
	free(conductor_pswd);
	free(conductor_type);
	free(licence_expiry_date);
	CstcLog_printf("Crashing heree...");
	return retval;
}

# if 0
int desfire_func(void)
{
	int ret=-1;
	cid=0;
	fsdi=5;
	br=0;


	CstcLog_printf("In desfire_func..........");
	if(!nfc_Init()){
		CstcLog_printf("Error: Failed to Init RFID Module");
		return -1;
	}
	CstcLog_printf("RFID Module Initiated Successfully...");

	/*********************** RATS & PPS ********************/

	ret=iso14443_RATS(fd,cid,fsdi,&atslen,ats);
	if(ret!=0){
		CstcLog_printf("rats ret= %d   %02x",ret,ret);
		CstcLog_printf("Error: iso14443_RATS failed...!!");
		Show_Error_Msg("RATS FAILED..!!");
		mif_close(fd);
		return -1;
	}
	CstcLog_printf("iso14443_RATS successful...!!");
	CstcLog_printf("atslen=%d ats=%s",atslen,ats);
	CstcLog_printf("%d : %02x %02x %02x %02x %02x %02x",ats,ats[0],ats[1],ats[2],ats[3],ats[4],ats[5]);

	ret=iso14443_PPS(fd,cid,0);
	if(ret!=0){
		CstcLog_printf("pps ret= %d   %02x",ret,ret);
		CstcLog_printf("Error: iso14443_PPS failed...!!");
		Show_Error_Msg("PPS FAILED..!!");
		mif_close(fd);
		return -1;
	}
	CstcLog_printf("iso14443_PPS successful...!!");
	CstcLog_printf("br = %d",br);

	CstcLog_printf("version size=%d",sizeof(des_ver));
	ret=desfire_get_version(fd, &des_ver);
	if(ret!=0){
		CstcLog_printf("version ret= %d   %02x",ret,ret);
		CstcLog_printf("Error: desfire_get_version failed...!!");
		Show_Error_Msg("Get Version Failed..!!");
		mif_close(fd);
		return -1;
	}
	CstcLog_printf(" BatchNo = %s HW_Size=%d",des_ver.BatchNo,des_ver.HW_Size);

	ret=desfire_get_application_ids(fd,&num,aid);
	if(ret!=0){
		CstcLog_printf("id ret= %d   %02x",ret,ret);
		CstcLog_printf("Error: desfire_get_application_ids failed...!!");
		Show_Error_Msg("GET APP IDS FAILED..!!");
		mif_close(fd);
		return -1;
	}

	CstcLog_printf("desfire_get_application_ids successful...!!");
	CstcLog_printf("num = %d aid=%s",num,aid);

	ret=desfire_select_application(fd,"000");
	if(ret!=0){
		CstcLog_printf("select app ret=%d   %02x",ret,ret);
		CstcLog_printf("Error: desfire_select_application failed...!!");
		Show_Error_Msg("Select Aapplication Failed..!!");
		mif_close(fd);
		return -1;
	}

	CstcLog_printf("2 desfire_select_application successful...!!");

	ret=desfire_select_application(fd,aid);
	if(ret!=0){
		CstcLog_printf(" ret= %02x",ret);
		CstcLog_printf("Error: desfire_select_application failed...!!");
		Show_Error_Msg("Select Aapplication Failed..!!");
		mif_close(fd);
		return -1;
	}

	CstcLog_printf("desfire_select_application successful...!!");


#if 0
	int desfire_select_application(int fd,const void *aid);
	int desfire_get_version(int fd, desfire_version *version);
	int desfire_free_memory(int fd,uint32_t *freesize);
	int desfire_get_fids(int fd,uint8_t *num, void *fid);
	int desfire_read_data(int fd,
			uint8_t fileno,
			uint8_t comm_set,
			uint32_t offset,
			uint32_t len,
			uint32_t *outlen,
			void *dataout);
#endif




	/****************** nfc close after use **************/

	if(mif_close(fd)){
		CstcLog_printf("Error: Failed to close nfc card...!!");
		return -1;
	}
	CstcLog_printf("RFID Module Closed Successfully...!!");

	return 0;
}
#endif


void ContactlessSmartCard_Demo(void){
#define reader_name  "/dev/nfc"

//	int ifd = -1;
	int retval = 0;
	int status;
	int key=0;

	int i,num=-1;

	long int x,ticket_amt=2;
	char szTemp[800];
	uint32_t uiApdu= 0;
	unsigned char bApdu[30];
	uint32_t uiApduResp= 0;
	unsigned char bApduResp[300];

	fd_des = mif_open(reader_name);
	CstcLog_printf("mif_open(%s) return %d\r\n", reader_name, fd_des);

	if (fd_des < 0){
		CstcLog_printf("mif_open() return %d\r\n", fd_des);
		Show_Error_Msg("Device Open failed.");
		/*lcdDisplay(0,0,DISP_CFONT, "Device Open failed.");
		lcdFlip();*/
		//		kbGetKey();
	}else {
		/*lcdDisplay(0,0,DISP_CFONT, "Device open success....");
		lcdDisplay(0,2,DISP_CFONT, "Please show card....");
		lcdFlip();
	    lcdDrawPicture(0,0,48,32,CL_SHOW_CARD_PICTURE);*/
		Show_Error_Msg("Please show card....");
		emv_contactless_active_picc(fd_des);

		while (1){
			emv_contactless_obtain_status(fd_des, &status);
			if (status == NFC_OP_EXCHANGE_APDU)
			{
				break;
			}
			else{
				emv_contactless_get_lasterror(fd_des, &status);
				if (EMULTIHOP == status)
				{

					emv_contactless_obtain_status(fd_des, &status);
					if (status == NFC_OP_EXCHANGE_APDU)
					{
						break;
					}
					else
					{
						emv_contactless_get_lasterror(fd_des, &status);
						if (EMULTIHOP == status)
						{

							/*lcdDisplay(0,4,DISP_CFONT, "multi card....");
							lcdFlip();*/
							CstcLog_printf("Multi Card...!!");
//							Show_Error_Msg("Multi Card...!!");

						}
					}
				}
			}


			if((key = Kb_Get_Key_Sym())>1)
			{
				if(key==0x1B)
					break;
			}
			else
				key=0;
		}

		if (0 == retval){


			for(num=0;num<=10;num++)
			{

				uiApdu = 0;
				switch(num)
				{
				case 0: CstcLog_printf("get app id's");
					memcpy(bApdu + uiApdu, "\x90\x6A\x00\x00\x00", 5); //get app id's
				uiApdu += 5;
				break;

				case 1:CstcLog_printf("select application");
					memcpy(bApdu + uiApdu, "\x90\x5A\x00\x00\x03\x03\x54\x43\x00", 9); //90 5A 00 00 03 03 54 43 00
				uiApdu += 9;
				break;


				case 2:CstcLog_printf("read_file id:4");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x04\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  04 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 3:CstcLog_printf("read_file id:5");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x05\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  05 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 4:CstcLog_printf("read_file id:6");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x06\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  06 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 5:CstcLog_printf("read_file id:7");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x07\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  07 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 6:CstcLog_printf("read balance");
					memcpy(bApdu + uiApdu, "\x90\x6C\x00\x00\x01\x02\x00", 7); //90 6C 00 00 01 02 00
				uiApdu += 7;
				break;

				case 7:CstcLog_printf("Debit transaction_amt"); // debit Rs.1

					memcpy(bApdu + uiApdu, "\x90\xDC\x00\x00\x05\x02\x01\x00\x00\x00\x00",11); //90 DC 00  00 05 02  01(i.e.amt field) 00 00  00 00
					memcpy(bApdu + uiApdu+6,&ticket_amt,4);
					uiApdu += 11;
				break;

				case 8:CstcLog_printf("Credit Transaction Counter");
					memcpy(bApdu + uiApdu, "\x90\x0C\x00\x00\x05\x01\x01\x00\x00\x00\x00",11); //90 0C 00 00 05 01 01 00 00 00 00
				uiApdu += 11;
				break;

				case 9:CstcLog_printf("Commit Transaction");
					memcpy(bApdu + uiApdu, "\x90\xC7\x00\x00\x00",5); //90 C7 00 00 00
				uiApdu += 5;
				break;

				case 10:CstcLog_printf("Read Remaining Balance");
					memcpy(bApdu + uiApdu, "\x90\x6C\x00\x00\x01\x02\x00",7); //90 6C 00 00 01 02 00
				uiApdu += 7;
				break;

				default:
					break;
				}



			/*	  bApdu[uiApdu++] = 0x00;
			bApdu[uiApdu++] = 0xA4;
			bApdu[uiApdu++] = 0x04;
			bApdu[uiApdu++] = 0x00;
			bApdu[uiApdu++] = 0x07;
			memcpy(bApdu + uiApdu, "\x90\x6A\x00\x00\x00", 5); //get app id's
			uiApdu += 5;    */

				memset(szTemp, 0, sizeof(szTemp));
				for (i = 0; i < (int)uiApdu; i++){
					sprintf(szTemp + strlen(szTemp), "%02X", bApdu[i]);
				}
				/*lcdCls();
			lcdDisplay(0,0,DISP_CFONT, "Send Apdu:%s", szTemp);
			lcdFlip();*/
				Show_Error_Msg("Sending Apdu");
				CstcLog_printf("Send Apdu = %s",szTemp);


				//            kbGetKeyMs(1000);
				sleep(1);

				uiApduResp = sizeof(bApduResp);
				//			lcdCls();
				if (0 == emv_contactless_exchange_apdu(fd_des, uiApdu, bApdu, &uiApduResp, bApduResp)){
					memset(szTemp, 0, sizeof(szTemp));
					for (i = 0; i < (int)uiApduResp; i++){
						sprintf(szTemp + strlen(szTemp), "%02X", bApduResp[i]);
					}
					//				lcdDisplay(0,0,DISP_CFONT, "Card Response:%s", szTemp);
					Show_Error_Msg("Getting Card Response");
					CstcLog_printf("card response=%s",szTemp);
				}
				else {
					//				lcdDisplay(0,0,DISP_CFONT, "Exchage apdu failed");
					Show_Error_Msg("Exchage apdu failed");
					CstcLog_printf("Exchage apdu failed");
				}

				if(num==6)  // card_amt
				{
					// convert hex to string

					CstcLog_printf("(num==6) card response= %s ",szTemp);
					//									memcpy(amount,szTemp,3);

					memcpy(&x,bApduResp,4);
					CstcLog_printf("amount(x) = %ld ",x);


					// assign it to e_purse_amt
					sprintf(rfid_smartcard_struct_var.e_purse_amt,"%ld",x);
				}
				/*********************************/
			}


			sleep(1);
			/*lcdFlip();
            kbGetKeyMs(1000);
            lcdCls();
			lcdDisplay(0,0,DISP_CFONT, "Please remove card");
			lcdFlip();*/
			Show_Error_Msg("Please remove card");

			emv_contactless_deactive_picc(fd_des);

			/*lcdFlip();
			kbGetKey();*/

		}
	}

	if (fd_des >=0){
		CstcLog_printf("mif_close(%d)", fd_des);
		mif_close(fd_des);
		fd_des = -1;
	}

}


int read_desfire_card_details(void)
{
#define reader_name  "/dev/nfc"

	int retval = 0;


	int i,num=-1;
	//char cardId[100]={0};
	char amount[50]={0};
	long int x;

	char szTemp[800];
	char szTemp1[800];
	uint32_t uiApdu= 0;
	unsigned char bApdu[30];
	uint32_t uiApduResp= 0;
	unsigned char bApduResp[300];
	char *ID_ptr;
	int szTemp1_len;
	char *ptr;
	unsigned long long int y=0;
#if 0
	ifd = mif_open(reader_name);
	CstcLog_printf("mif_open(%s) return %d\r\n", reader_name, ifd);

	if (ifd < 0){
		CstcLog_printf("mif_open() return %d\r\n", ifd);
		Show_Error_Msg("Device Open failed.");
		/*lcdDisplay(0,0,DISP_CFONT, "Device Open failed.");
		lcdFlip();*/
		//		kbGetKey();
	}else {
		/*lcdDisplay(0,0,DISP_CFONT, "Device open success....");
		lcdDisplay(0,2,DISP_CFONT, "Please show card....");
		lcdFlip();
	    lcdDrawPicture(0,0,48,32,CL_SHOW_CARD_PICTURE);*/
		Show_Error_Msg("Please show card....");
		emv_contactless_active_picc(ifd);

		while (1){
			emv_contactless_obtain_status(ifd, &status);
			if (status == NFC_OP_EXCHANGE_APDU)
			{
				break;
			}
			else{
				emv_contactless_get_lasterror(ifd, &status);
				if (EMULTIHOP == status)
				{

					emv_contactless_obtain_status(ifd, &status);
					if (status == NFC_OP_EXCHANGE_APDU)
					{
						break;
					}
					else
					{
						emv_contactless_get_lasterror(ifd, &status);
						if (EMULTIHOP == status)
						{

							/*lcdDisplay(0,4,DISP_CFONT, "multi card....");
							lcdFlip();*/
							CstcLog_printf("Multi Card...!!");
//							Show_Error_Msg("Multi Card...!!");

						}
					}
				}
			}


			/*if((key = Kb_Get_Key_Sym())>1)
			{
				if(key==0x1B)
				{
					emv_contactless_deactive_picc(ifd);
					if (ifd >=0){
						CstcLog_printf("mif_close(%d)", ifd);
						mif_close(ifd);
						ifd = -1;
					}
					return 0;
				}
				//					break;
			}
			else
				key=0;*/


			key = Kb_Get_Key_Sym_new();
						//			key = Kb_Get_Key_Sym();
						if(key==KEY_SYM_ESCAPE)
						{
							emv_contactless_deactive_picc(ifd);
						if (ifd >=0){
							CstcLog_printf("mif_close(%d)", ifd);
							mif_close(ifd);
							ifd = -1;
						}
						return 0;
						}
		}
#endif

		if (0 == retval){


			for(num=2;num< 7;num++)
			{

				uiApdu = 0;
				switch(num)
				{

#if 0
				case 0: CstcLog_printf("get app id's");
					memcpy(bApdu + uiApdu, "\x90\x6A\x00\x00\x00", 5); //get app id's
				uiApdu += 5;
				break;

				case 1:CstcLog_printf("select application");
					memcpy(bApdu + uiApdu, "\x90\x5A\x00\x00\x03\x01\x54\x43\x00", 9); //90 5A 00 00 03 03 54 43 00
				uiApdu += 9;
				break;
#endif

				case 2:CstcLog_printf("read_file id:4");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x04\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  04 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 3:CstcLog_printf("read_file id:5");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x05\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  05 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 4:CstcLog_printf("read_file id:6");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x06\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  06 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 5:CstcLog_printf("read_file id:7");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x07\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  07 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 6:CstcLog_printf("read balance");
					memcpy(bApdu + uiApdu, "\x90\x6C\x00\x00\x01\x02\x00", 7); //90 6C 00 00 01 02 00

				uiApdu += 7;
				break;



			/*	case 7:CstcLog_printf("Debit transaction_amt");
					memcpy(bApdu + uiApdu, "\x90\xDC\x00\x00\x05\x02\x05\x00\x00\x00\x00",11); //90 DC 00  00 05 02  05 00 00  00 00
				uiApdu += 11;
				break;

				case 8:CstcLog_printf("Credit Transaction Counter");
					memcpy(bApdu + uiApdu, "\x90\x0C\x00\x00\x05\x01\x01\x00\x00\x00\x00",11); //90 0C 00 00 05 01 01 00 00 00 00
				uiApdu += 11;
				break;

				case 9:CstcLog_printf("Commit Transaction");
					memcpy(bApdu + uiApdu, "\x90\xC7\x00\x00\x00",5); //90 C7 00 00 00
				uiApdu += 5;
				break;

				case 10:CstcLog_printf("Read Remaining Balance");
					memcpy(bApdu + uiApdu, "\x90\x6C\x00\x00\x01\x02\x00",7); //90 6C 00 00 01 02 00
				uiApdu += 7;
				break;*/

				default:
					break;
				}



			/*	  bApdu[uiApdu++] = 0x00;
			bApdu[uiApdu++] = 0xA4;
			bApdu[uiApdu++] = 0x04;
			bApdu[uiApdu++] = 0x00;
			bApdu[uiApdu++] = 0x07;
			memcpy(bApdu + uiApdu, "\x90\x6A\x00\x00\x00", 5); //get app id's
			uiApdu += 5;    */

				memset(szTemp, 0, sizeof(szTemp));
				for (i = 0; i < (int)uiApdu; i++){
					sprintf(szTemp + strlen(szTemp), "%02X", bApdu[i]);
				}
				/*lcdCls();
			lcdDisplay(0,0,DISP_CFONT, "Send Apdu:%s", szTemp);
			lcdFlip();*/
//				Show_Error_Msg("Sending Apdu");
				CstcLog_printf("Send Apdu = %s",szTemp);


				//            kbGetKeyMs(1000);
//				sleep(1);

				uiApduResp = sizeof(bApduResp);
				//			lcdCls();
				if (0 == emv_contactless_exchange_apdu(fd_des, uiApdu, bApdu, &uiApduResp, bApduResp)){
					memset(szTemp, 0, sizeof(szTemp));
					for (i = 0; i < (int)uiApduResp; i++){
						sprintf(szTemp + strlen(szTemp), "%02X", bApduResp[i]);

					}
					//				lcdDisplay(0,0,DISP_CFONT, "Card Response:%s", szTemp);
//					Show_Error_Msg("Getting Card Response");

					CstcLog_printf("card response=%s",szTemp);
				}
				else {
					//				lcdDisplay(0,0,DISP_CFONT, "Exchage apdu failed");
					CstcLog_printf("Exchage apdu failed");
					Show_Error_Msg("Exchage apdu failed");
					emv_contactless_deactive_picc(fd_des);
					if (fd_des >=0){
						CstcLog_printf("mif_close(%d)", fd_des);
						mif_close(fd_des);
						fd_des = -1;
					}
					return 0;
				}

				if(num==6)  // card_amt
				{
					// convert hex to string

					CstcLog_printf("(num==6) card response= %s ",szTemp);
//									memcpy(amount,szTemp,3);
					if((bApduResp[4]==0x91)&&(bApduResp[5]==0x00)){

						memcpy(&x,bApduResp,4);
						sprintf(amount,"Card Amount :Rs. %ld/- \n please wait...",x);
						CstcLog_printf("amount(x) = %ld ",x);
						Show_Error_Msg_UART(amount);
						/*x=ntohl(x);
										CstcLog_printf("amount(x) = %ld ",x);*/

						/*sscanf(amount, "%x", &x);
											printf("%u", x);*/

						// assign it to e_purse_amt
						sprintf(rfid_smartcard_struct_var.e_purse_amt,"%ld",x);

						CstcLog_printf("rfid_smartcard_struct_var.e_purse_amt = %s ",rfid_smartcard_struct_var.e_purse_amt);


						//Show_Error_Msg("SAM authentication success");
					}
					else{


						Show_Error_Msg("error in reading balance");
						CstcLog_printf("error in reading balance");

						if (fd_des >=0){
							CstcLog_printf("mif_close..");
							emv_contactless_deactive_picc(fd_des);
							mif_close(fd_des);
							fd_des = -1;
						}
						return 0;

					}
				}
				/*********************************/
				if(num == 5){

					CstcLog_printf("(num == 5) card response= %s ",szTemp);
					//									memcpy(amount,szTemp,3);



					memset(szTemp1, 0, sizeof(szTemp1));

					szTemp1_len =0;
					for (i = 25; i < 31; i++){

						sprintf(szTemp1 + strlen(szTemp1), "%02X", bApduResp[i]);

						CstcLog_printf("szTemp1[%d]=%02X",i,szTemp1[i]);
					}
					//memcpy(szTemp1,bApduResp+25,6);
					CstcLog_printf("szTemp1 = %s",szTemp1);

					ptr = szTemp1;
					CstcLog_printf("ptr = %s",ptr);
					y = strtoull(ptr,&ID_ptr,16);
					CstcLog_printf("y = %llu ",y);
					CstcLog_printf("ID_ptr = %s",ID_ptr);

					/*  if ((y == LONG_MAX || y == LONG_MIN)) {
										  CstcLog_printf("strtol");
										               exit(EXIT_FAILURE);
										           }*/

					if (ID_ptr == szTemp1) {
						CstcLog_printf( "No digits were found\n");
						// exit(EXIT_FAILURE);
					}

					/* If we got here, strtol() successfully parsed a number */

					CstcLog_printf("strtol() returned %llu\n", y);

					if (*ID_ptr != '\0')        /* Not necessarily an error... */
						printf("Further characters after number: %s\n", ID_ptr);



					// assign it to e_purse_card_ID
					sprintf(rfid_smartcard_struct_var.card_ID_No,"%llu",y);
					CstcLog_printf("rfid_smartcard_struct_var.card_ID_No = %s ",rfid_smartcard_struct_var.card_ID_No);

				}
			}

			sleep(1);
			/*lcdFlip();
            kbGetKeyMs(1000);
            lcdCls();
			lcdDisplay(0,0,DISP_CFONT, "Please remove card");
			lcdFlip();*/
			/*Show_Error_Msg("Please remove card");

			emv_contactless_deactive_picc(ifd);*/

			/*lcdFlip();
			kbGetKey();*/

		}
	//}

	/*if (ifd >=0){
		CstcLog_printf("mif_close(%d)", ifd);
		mif_close(ifd);
		ifd = -1;
	}*/

	return 1;

}


int update_epurse_desfire_card(void)
{
#define reader_name  "/dev/nfc"

//	int ifd = -1;
	int retval = 0;


	int i,num=-1;
	long int ticket_amt=0;

	char szTemp[800];
	char amount[AMOUNT_LEN+1]={0};
	uint32_t uiApdu= 0;
	unsigned char bApdu[30];
	uint32_t uiApduResp= 0;
	unsigned char bApduResp[300];

#if 0

	ifd = mif_open(reader_name);
	CstcLog_printf("mif_open(%s) return %d\r\n", reader_name, ifd);

	if (ifd < 0){
		CstcLog_printf("mif_open() return %d\r\n", ifd);
		Show_Error_Msg("Device Open failed.");
		/*lcdDisplay(0,0,DISP_CFONT, "Device Open failed.");
		lcdFlip();*/
		//		kbGetKey();
	}
	else
	{
		/*lcdDisplay(0,0,DISP_CFONT, "Device open success....");
		lcdDisplay(0,2,DISP_CFONT, "Please show card....");
		lcdFlip();
	    lcdDrawPicture(0,0,48,32,CL_SHOW_CARD_PICTURE);*/
		Show_Error_Msg("Please show card....");
		emv_contactless_active_picc(ifd);

		while (1){
			emv_contactless_obtain_status(ifd, &status);
			if (status == NFC_OP_EXCHANGE_APDU)
			{
				break;
			}
			else{
				emv_contactless_get_lasterror(ifd, &status);
				if (EMULTIHOP == status)
				{

					emv_contactless_obtain_status(ifd, &status);
					if (status == NFC_OP_EXCHANGE_APDU)
					{
						break;
					}
					else
					{
						emv_contactless_get_lasterror(ifd, &status);
						if (EMULTIHOP == status)
						{

							/*lcdDisplay(0,4,DISP_CFONT, "multi card....");
							lcdFlip();*/
							Show_Error_Msg("multi card....");

						}
					}
				}
			}


			/*if((key = Kb_Get_Key_Sym())>1)
			{
				if(key==0x1B)
					break;
			}
			else
				key=0;*/
		}
	}

#endif

		if (0 == retval){


			for(num=7;num<=10;num++)
			{

				uiApdu = 0;
				switch(num)
				{
				/*case 0: CstcLog_printf("get app id's");
					memcpy(bApdu + uiApdu, "\x90\x6A\x00\x00\x00", 5); //get app id's
				uiApdu += 5;
				break;

				case 1:CstcLog_printf("select application");
					memcpy(bApdu + uiApdu, "\x90\x5A\x00\x00\x03\x03\x54\x43\x00", 9); //90 5A 00 00 03 03 54 43 00
				uiApdu += 9;
				break;


				case 2:CstcLog_printf("read_file id:4");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x04\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  04 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 3:CstcLog_printf("read_file id:5");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x05\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  05 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 4:CstcLog_printf("read_file id:6");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x06\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  06 00 00 00 20  00 00 00
				uiApdu += 13;
				break;


				case 5:CstcLog_printf("read_file id:7");
					memcpy(bApdu + uiApdu, "\x90\xBD\x00\x00\x07\x07\x00\x00\x00\x20\x00\x00\x00", 13); //90 BD 00 00 07  07 00 00 00 20  00 00 00
				uiApdu += 13;
				break;

				case 6:CstcLog_printf("read balance");
					memcpy(bApdu + uiApdu, "\x90\x6C\x00\x00\x01\x02\x00", 7); //90 6C 00 00 01 02 00
				uiApdu += 7;
				break;*/

				case 7:CstcLog_printf("Debit transaction_amt");
				ticket_amt=atoi(transaction_log_struct_var.total_ticket_amount);
					memcpy(bApdu + uiApdu, "\x90\xDC\x00\x00\x05\x02\x05\x00\x00\x00\x00",11); //90 DC 00  00 05 02  05 00 00  00 00
					memcpy(bApdu + uiApdu+6,&ticket_amt,4);
				uiApdu += 11;
				break;

				case 8:CstcLog_printf("Credit Transaction Counter");
					memcpy(bApdu + uiApdu, "\x90\x0C\x00\x00\x05\x01\x01\x00\x00\x00\x00",11); //90 0C 00 00 05 01 01 00 00 00 00
				uiApdu += 11;
				break;

				case 9:CstcLog_printf("Commit Transaction");
					memcpy(bApdu + uiApdu, "\x90\xC7\x00\x00\x00", 5); //90 C7 00 00 00
				uiApdu += 5;
				break;

				case 10:CstcLog_printf("Read Remaining Balance");
					memcpy(bApdu + uiApdu, "\x90\x6C\x00\x00\x01\x02\x00", 7); //90 6C 00 00 01 02 00
				uiApdu += 7;
				break;

				default:
					break;
				}





				memset(szTemp, 0, sizeof(szTemp));
				for (i = 0; i < (int)uiApdu; i++){
					sprintf(szTemp + strlen(szTemp), "%02X", bApdu[i]);
				}
				/*lcdCls();
			lcdDisplay(0,0,DISP_CFONT, "Send Apdu:%s", szTemp);
			lcdFlip();*/
//				Show_Error_Msg("Sending Apdu");
				CstcLog_printf("Send Apdu = %s",szTemp);


				//            kbGetKeyMs(1000);
//				sleep(1);

				uiApduResp = sizeof(bApduResp);
				//			lcdCls();
				if (0 == emv_contactless_exchange_apdu(fd_des, uiApdu, bApdu, &uiApduResp, bApduResp)){
					memset(szTemp, 0, sizeof(szTemp));
					for (i = 0; i < (int)uiApduResp; i++){
						sprintf(szTemp + strlen(szTemp), "%02X", bApduResp[i]);
					}
					//				lcdDisplay(0,0,DISP_CFONT, "Card Response:%s", szTemp);
//					Show_Error_Msg("Getting Card Response");
					CstcLog_printf("card response=%s",szTemp);
					}
				else {
					//				lcdDisplay(0,0,DISP_CFONT, "Exchange apdu failed");
					CstcLog_printf("Exchage apdu failed");
					Show_Error_Msg("Exchage apdu failed");
					emv_contactless_deactive_picc(fd_des);
					if (fd_des >=0){
						CstcLog_printf("mif_close(%d)", fd_des);
						mif_close(fd_des);
						fd_des = -1;
					}
					return 0;
				}
				if(num==10)  // card_amt
				{
					// convert hex to string

					CstcLog_printf("(num==10) card response= %s ",szTemp);
					memcpy(amount,bApduResp,4);
					CstcLog_printf("amount after deduction = %s ",amount);

					// assign it to e_purse_amt

				}

				/*********************************/
			}

//			sleep(1);

			/*lcdFlip();
            kbGetKeyMs(1000);
            lcdCls();
			lcdDisplay(0,0,DISP_CFONT, "Please remove card");
			lcdFlip();*/

			Show_Error_Msg("Please remove card");

			emv_contactless_deactive_picc(fd_des);

			/*lcdFlip();
			kbGetKey();*/

		}
//	}

	if (fd_des >=0){
		CstcLog_printf("mif_close(%d)", fd_des);
		mif_close(fd_des);
		fd_des = -1;
	}

return 1;


}

int desfire_close(void)
{
	emv_contactless_deactive_picc(fd_des);
	if (fd_des >=0){
		CstcLog_printf("mif_close(%d)", fd_des);
		mif_close(fd_des);
		fd_des = -1;
	}
	return 0;
}
/********************************************************************************************/


int sam_authentication(void){
#define sam_reader_name  "/dev/samcard1"
#define des_reader_name  "/dev/nfc"

	int ret=-1,key=0;

	int status;
	int i, len;
	unsigned char atr[40];
	char szTemp[100];
	char temp[200]={0};
	unsigned char apdu_cmd[30]={0};
	uint32_t apdu_cmd_len=0;
	uint32_t  apdu_cmd_len1=0;

	char *ptr = "\x90\xAF\x00\x00\x10";
	char *ptr1 = "\x00";
	char *ptr2 = "\x80\x7A\x00\x01\x08";
	CstcLog_printf("In sam_function ...............");

	fd_sam = iccard_open_cardslot(sam_reader_name);

	if (fd_sam < 0){

		Show_Error_Msg("Device open failed..");
		CstcLog_printf("Device Open failed.");
		goto END;

	}else {

		CstcLog_printf("SAM card slot opened successfully....");

		//check card in slot

		while (iccard_check_card_in_slot(fd_sam) != 0){

			CstcLog_printf("SAM Card Not Found.");
			Show_Error_Msg("SAM Card Not Found");
			iccard_close_cardslot(fd_sam);
			fd_sam = -1;
			return -1;

			/*if(kbhit()){
					key = kbGetKey();
			    	if (key == 0x1B){
						goto END;
					}
				}*/
		}

		Show_Error_Msg("SAM Card Found");
		CstcLog_printf("SAM Card Found");

		len = sizeof(atr);
		if (0 != iccard_power_up(fd_sam, CVCC_5_VOLT, EMV_MODE, &len, atr)){
			CstcLog_printf("Card Power up Failed");
			Show_Error_Msg("Card Power up Failed");
			sleep(1);
			goto END;
		}else {
			CstcLog_printf("Card Power up successfully");

			memset(szTemp, 0, sizeof(szTemp));
			for (i = 0; i < len; i++){
				sprintf(szTemp + strlen(szTemp), "%02X", atr[i]);
			}

			/*lcdDisplay(0,0,DISP_CFONT,"ATR:%s", szTemp);
				lcdFlip();
				kbhit();*/

			memset(temp,0,200);
			sprintf(temp,"ATR:%s",szTemp);
			CstcLog_printf("temp: %s",temp);
			//			Show_Error_Msg(temp);

		}




		CstcLog_printf("In desfire_command ...............");
		fd_des = mif_open(des_reader_name);

		if (fd_des < 0){
			CstcLog_printf("mif_open() failed = %d", fd_des);
			goto END;
		}
		CstcLog_printf("Desfire open success(%d)", fd_des);

		while(1){

			CstcLog_printf("Please show card....");
			Show_Error_Msg("Please show card....");

			ret = emv_contactless_active_picc(fd_des);
			if(ret == 0){

			CstcLog_printf("1.ret =%d",ret);
			usleep(100000);
			}
			else{

				Show_Error_Msg(" Error in \n Reading card");
				CstcLog_printf(" Error in \n Reading card");

			}

			emv_contactless_obtain_status(fd_des, &status);
			if (status == NFC_OP_EXCHANGE_APDU)
			{
				Show_Error_Msg("Reading card \n please wait..");
				CstcLog_printf("1.Reading card \n please wait..");
				CstcLog_printf("in if");
				break;
			}
			else{
				//CstcLog_printf("fd_des =%d",fd_des);
				CstcLog_printf("in else");
				emv_contactless_get_lasterror(fd_des, &status);
				//CstcLog_printf("status =%d",status);
				if (EMULTIHOP == status)
				{
					Show_Error_Msg("Reading card \n please wait..");
					CstcLog_printf("2.Reading card \n please wait..");
					CstcLog_printf("1.in if");

					emv_contactless_obtain_status(fd_des, &status);
					if (status == NFC_OP_EXCHANGE_APDU)
					{
						Show_Error_Msg("Reading card \n please wait..");
						CstcLog_printf("3.Reading card \n please wait..");
						CstcLog_printf("2.in else");
						break;
					}
					else
					{
						emv_contactless_get_lasterror(fd_des, &status);
						if (EMULTIHOP == status)
						{
							Show_Error_Msg("Reading card \n please wait..");
							CstcLog_printf("4.Reading card \n please wait..");
							CstcLog_printf("3.in if");
							/*lcdDisplay(0,4,DISP_CFONT, "multi card....");
								lcdFlip();*/
							//							CstcLog_printf("Multi Card...!!");
							//							Show_Error_Msg("Multi Card...!!");
							break;

						}
					CstcLog_printf("1.out of if");
					}
				}
			CstcLog_printf("in else end");
			}

			key = Kb_Get_Key_Sym_new();
			//			key = Kb_Get_Key_Sym();
			if(key==KEY_SYM_ESCAPE)
			{
				if (fd_sam >=0){

					CstcLog_printf("SAM Closed..");
					iccard_power_down(fd_sam);
					iccard_close_cardslot(fd_sam);
					fd_sam = -1;
				}

				if (fd_des >=0){
					CstcLog_printf("mif_close..");
					emv_contactless_deactive_picc(fd_des);
					mif_close(fd_des);
					fd_des = -1;
				}
				return -1;
			}






			//										CstcLog_printf("While loop end");
		}

		for(num=0;num < 23;num++)
		{


			switch(num){

			case 0:
				// for creating SAM application & Keys

				/***** SAM ********/

				CstcLog_printf("clear SAM card");
				ret=exchange_apdu_func(fd_sam,"\x80\x30\x00\x00\x00",5,1); //this command is to clear SAM card

				if((bApduResp[0]==0x90)&&(bApduResp[1]==0x00)){

					CstcLog_printf("clear SAM card success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("clear SAM card failed");

				if(ret)
					goto END;
				break;
			case 1:
				CstcLog_printf("create Master file..");
				//ret=exchange_apdu_func(fd_sam,"\x00\xE0\x00\x00\x0D\x62\x0B\x82\x02\x3F\x00\x83\x02\x3F\x00\x8A\x01\x01\x00",19,1);
				//00 E0 00 00 1A 62 18 82 01 3F 83 02 3F 00 8A 01 01 8C 08 7F FF FF FF FF FF FF FF 8D 02 00 03
				//00 E0 00 00 0D 62 0B 82 02 3F 00 83 02 3F 00 8A 01 01
				ret=exchange_apdu_func(fd_sam,"\x00\xE0\x00\x00\x0D\x62\x0B\x82\x02\x3F\x00\x83\x02\x3F\x00\x8A\x01\x01",18,1);

				if((bApduResp[0]==0x90)&&(bApduResp[1]==0x00)){

					CstcLog_printf("create Master file success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("create Master file failed");

				if(ret)
					goto END;
				break;
			case 2:
				//CstcLog_printf("Send Command to create elementary file");
				CstcLog_printf("Key File 41 02, with 2 records");
				ret=exchange_apdu_func(fd_sam,"\x00\xE0\x00\x00\x1C\x62\x1A\x82\x05\x0C\x00\x00\x22\x02\x83\x02\x41\x02\x88\x01\x02\x8A\x01\x01\x8C\x07\x7B\x00\x00\x00\x00\xFF",32,1);
				//00 E0 00 00 1D 62 1B 82 05 0C 00 00 16 05 83 02 00 02 88 01 02 8A 01 01 8C 08 7F FF FF FF FF FF 01 FF
				//00 E0 00 00 1C 62 1A 82 05 0C 00 00 22 02 83 02 41 02 88 01 02 8A 01 01 8C 07 7B 00 00 00 00 00 FF
				//ret=exchange_apdu_func(fd_sam,"\x00\xE0\x00\x00\x1D\x62\x1B\x82\x05\x0C\x00\x00\x16\x05\x83\x02\x00\x02\x88\x01\x02\x8A\x01\x01\x8C\x08\x7F\xFF\xFF\xFF\xFF\xFF\x01\xFF",34,1);
				if((bApduResp[0]==0x90)&&(bApduResp[1]==0x00)){

					CstcLog_printf("create elementary file success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication fai5led");
					CstcLog_printf("create elementary file failed");
				if(ret)
					goto END;
				break;
			case 3:
				//00 E2 00 00 14 81 01 33 00 8C A6 4D E9 C1 B1 23 A7 35 55 50 B2 15 0E 24 51

				CstcLog_printf("1st record Default Authentication Key for DESFire, Key ID=81, key Type = 04");
				//00 E2 00 00 13 81 04 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
				ret=exchange_apdu_func(fd_sam,"\x00\xE2\x00\x00\x13\x81\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",24,1);
				if((bApduResp[0]==0x90)&&(bApduResp[1]==0x00)){

					CstcLog_printf("append record to end of EF file success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("append record to end of EF file failed");
				if(ret)
					goto END;
				break;

				//00 E2 00 00 15 82 06 FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
			case 4:

				CstcLog_printf("2nd record New Key for DESFire, Key ID=82, key Type = 03 ");
				//00 E2 00 00 16 82 03 FF FF 00 00 11 11 11 11 11 11 11 11 22 22 22 22 22 22 22 22
				ret=exchange_apdu_func(fd_sam,"\x00\xE2\x00\x00\x16\x82\x03\xFF\xFF\x00\x00\x11\x11\x11\x11\x11\x11\x11\x11\x22\x22\x22\x22\x22\x22\x22\x22",27,1);
				if(ret)
					goto END;
				break;

				/////////////////////////////////////////////
//#if static_auth
			case 5:     //for static key authentication
				//80 14 00 00 085

				CstcLog_printf("Generate bulk encryption Key");
				ret=exchange_apdu_func(fd_sam,"\x80\x72\x05\x81\x00",5,1);
				if((bApduResp[0]==0x90)&&(bApduResp[1]==0x00)){

					CstcLog_printf("Generate bulk encryption Key success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("Generate bulk encryption Key failed");

				if(ret)
					goto END;
				break;

				/**********************************************************/
			/*case 6:
				**** des *******
				CstcLog_printf("get app id's");
				ret=exchange_apdu_func(fd_des,"\x90\x6A\x00\x00\x00",5,0);


				if(ret)
					goto END;
				break;*/

			case 7:
				CstcLog_printf("select application");
				ret=exchange_apdu_func(fd_des,"\x90\x5A\x00\x00\x03\x01\x54\x43\x00",9,0);
				//ret=exchange_apdu_func(fd_des,"\x90\x5A\x00\x00\x03\x01\x00\x00\x00",9,0);
				if((bApduResp[0]==0x91)&&(bApduResp[1]==0x00)){
					CstcLog_printf("APPLICATION FOUND");


				}
				else{

					CstcLog_printf("APPLICATION NOT FOUND");

					Show_Error_Msg("APPLICATION NOT FOUND");
					emv_contactless_deactive_picc(fd_des);
					if (fd_des >=0){
						CstcLog_printf("mif_close(%d)", fd_des);
						mif_close(fd_des);
						fd_des = -1;
					}
					if (fd_sam >=0){
						CstcLog_printf("SAM Closed..");
						iccard_power_down(fd_sam);
						iccard_close_cardslot(fd_sam);
						fd_sam = -1;
					}

					return 0;


				}

				if(ret)
					goto END;
				break;
			case 8:

				//CstcLog_printf("cmd for authentication with key");
				CstcLog_printf("Generate Challagen Data from Desfire Card");
				ret=exchange_apdu_func(fd_des,"\x90\x0A\x00\x00\x01\x00\x00",7,0);
				//ret=exchange_apdu_func(fd_des,"\x90\xAA\x00\x00\x01\x00\x00",7,0);
				if(ret)
					goto END;
				break;


			case 9:

				/***** sam ********/

				CstcLog_printf("Send Command to SAM using the received challenge data of DESFire EV1.....");
				//ret=exchange_apdu_func(fd_sam,"\x80\x78\x80\x05\x08\x9D\xFA\x79\x6A\xDC\x84\x30\xF8\x00",14,1);

				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				memcpy(apdu_cmd, "\x80\x78\x80\x05\x08",5);
				memcpy(apdu_cmd + 5,&bApduResp,8);

				ret=exchange_apdu_func(fd_sam,apdu_cmd,13,1);
#if 0
				if((bApduResp[16]==0x90)&&(bApduResp[17]==0x00)){

					CstcLog_printf("Response received from SAM ");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("SAM response failed");
#endif
				if(ret)
					goto END;
				break;
			case 10:

				CstcLog_printf("Send Command to SAM to get response..");
				ret=exchange_apdu_func(fd_sam,"\x00\xC0\x00\x00\x10",5,1);

				if(ret)
					goto END;

				break;

			case 11:

				/******* desfire ***********/

				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				CstcLog_printf("ptr =%x",ptr);
				apdu_cmd_len = 0;
				memcpy(apdu_cmd+apdu_cmd_len,ptr,5);
				apdu_cmd_len += 5;
				CstcLog_printf("apdu_cmd_len =%d",apdu_cmd_len);
				CstcLog_printf("apdu_cmd =%x",apdu_cmd);
				memcpy(apdu_cmd+5,bApduResp,16);
				apdu_cmd_len += 16;
				memcpy(apdu_cmd+apdu_cmd_len,ptr1,1);
				CstcLog_printf("Send Command to Desfire with Response from SAM for the desfire challenge");
				ret=exchange_apdu_func(fd_des,apdu_cmd,22,0);
				if((bApduResp[8]==0x91)&&(bApduResp[9]==0x00)){

					CstcLog_printf("desfire challenge received ");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("desfire challenge receive failed");
				if(ret)
					goto END;
				break;
			case 12:
				/***** sam ********/
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				apdu_cmd_len1 = 0;
				memcpy(apdu_cmd+apdu_cmd_len1,ptr2,5);
				apdu_cmd_len1 += 5;
				memcpy(apdu_cmd+apdu_cmd_len1,bApduResp,8);
				CstcLog_printf("verify response from Desfire and declare success");

				ret=exchange_apdu_func(fd_sam,apdu_cmd,13,1);
				//ret=exchange_apdu_func(fd_sam,"\x80\x7A\x00\x01\x08\xE5\x2A\x39\xDE\x4F\xD7\x5D\x0D\x00",14,1);
				auth_sucess_flg =0;
				if((bApduResp[0]==0x90)&&(bApduResp[1]==0x00)){
					auth_sucess_flg =1;
					CstcLog_printf("SAM authentication success");
					//Show_Error_Msg("SAM authentication success");

				}
				else
					CstcLog_printf("SAM authentication failed");

				CstcLog_printf("before break case 11");
				if(ret)
					goto END;
				break;

//#endif

#if 0   // not in our use(from java side)
			case 5:  // for diversify key serial no

				CstcLog_printf("get card serial number cmd form desfire 1");
				ret=exchange_apdu_func(fd_des,"\x90\x60\x00\x00\x00",5,0);
				/*	if((bApduResp[8]==0x90)&&(bApduResp[9]==0x00)){

						CstcLog_printf("card serial number success");
						//Show_Error_Msg("SAM authentication success");
					}
					else
						//Show_Error_Msg("SAM authentication failed");
						CstcLog_printf("card serial number failed");*/


				if(ret)
					goto END;

				break;

			case 6:

				CstcLog_printf("get card serial number cmd form desfire 2");
				ret=exchange_apdu_func(fd_des,"\x90\xAF\x00\x00\x00",5,0);

				if(ret)
					goto END;

				break;

			case 7:

				CstcLog_printf("get card serial number cmd form desfire 3");
				ret=exchange_apdu_func(fd_des,"\x90\xAF\x00\x00\x00",5,0);
				if(ret)
					goto END;

				break;


			case 8:
				//
				CstcLog_printf("get key from sam ");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x80\xCA\x88\x82\x0B",5);
				memcpy(apdu_cmd + 5,&bApduResp,7);
				memcpy(apdu_cmd+12,"\x00\x00\x00\x00",4);
				ret=exchange_apdu_func(fd_sam,apdu_cmd,16,1);
				if(ret)
					goto END;

				break;

				//////

			case 9:
				//
				CstcLog_printf("Read sam..Return encrypted data");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x00\xC0\x00\x00\x18",5);
				//memcpy(apdu_cmd + 5,&bApduResp,7);
				//memcpy(apdu_cmd+12,"\x00\x00\x00\x00",4);
				ret=exchange_apdu_func(fd_sam,apdu_cmd,5,1);

				if(ret)
					goto END;

				break;


			case 10:
				//
				CstcLog_printf("Inject Key In Desfire card");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x90\xC4\x00\x00\x19\x00",6);
				memcpy(apdu_cmd + 6,&bApduResp,24);
				memcpy(apdu_cmd+30,"\x00",1);
				ret=exchange_apdu_func(fd_des,apdu_cmd,31,0);
				if(ret)
					goto END;

				break;
			case 11:
				//
				CstcLog_printf("Check CMAC");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x80\x74\x0F\x01\x01\x00",6);

				ret=exchange_apdu_func(fd_sam,apdu_cmd,6,1);
				if(ret)
					goto END;

				break;
			case 12:
				//
				//
				CstcLog_printf("get CMAC");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x00\xC0\x00\x00\x08",5);

				ret=exchange_apdu_func(fd_sam,apdu_cmd,5,1);
				if(ret)
					goto END;

				break;

#endif

//#if !(static_auth)
				/////////////Authenticate with new diversified key/////////////////////
			case 13:
				//
				//
				CstcLog_printf("Select Application");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x90\x5A\x00\x00\x03\x01\x54\x43\x00",9);

				ret=exchange_apdu_func(fd_des,apdu_cmd,9,0);
				if((bApduResp[0]==0x91)&&(bApduResp[1]==0x00)){

					CstcLog_printf("application done");
					//Show_Error_Msg("SAM authentication success");
				}
				else if((bApduResp[0]==0x91)&&(bApduResp[1]==0xa0)){
					Show_Error_Msg("application not found");
					CstcLog_printf("application not found");
				}
				else
				{
				 CstcLog_printf("application failed");
				 Show_Error_Msg(" application failed");
				}
				if(ret)
				goto END;

				break;

			case 14:

				CstcLog_printf("get card serial number cmd form desfire 1");
				ret=exchange_apdu_func(fd_des,"\x90\x60\x00\x00\x00",5,0);
				/*	if((bApduResp[8]==0x90)&&(bApduResp[9]==0x00)){

						CstcLog_printf("card serial number success");
						//Show_Error_Msg("SAM authentication success");
					}
					else
						//Show_Error_Msg("SAM authentication failed");
						CstcLog_printf("card serial number failed");*/


				if(ret)
					goto END;

				break;

			case 15:

				CstcLog_printf("get card serial number cmd form desfire 2");
				ret=exchange_apdu_func(fd_des,"\x90\xAF\x00\x00\x00",5,0);

				if(ret)
					goto END;

				break;

			case 16:

				CstcLog_printf("get card serial number cmd form desfire 3");
				ret=exchange_apdu_func(fd_des,"\x90\xAF\x00\x00\x00",5,0);
				if(ret)
					goto END;

				break;

				/////////////Authenticate with new diversified key/////////////////////


			case 17:

				CstcLog_printf("Load Key");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				//memcpy(apdu_cmd,"\x80\x72\x03\x82\x08\x04\x45\x66\xBA\x5E\x3F\x80\x00",13);
				memcpy(apdu_cmd,"\x80\x72\x03\x82\x08",5);
				memcpy(apdu_cmd + 5,&bApduResp,7);//add card uid in cmd
				memcpy(apdu_cmd+12,"\x00",1);
				ret=exchange_apdu_func(fd_sam,apdu_cmd,13,1);

				if((bApduResp[0]==0x90)&&(bApduResp[1]==0x00)){

					CstcLog_printf("load key success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("load key failed");
				if(ret)
					goto END;

				break;

			case 18:

				CstcLog_printf("Get Challenge");
				//CstcLog_printf("cmd for authentication with key");
				CstcLog_printf("Generate Challagen Data from Desfire Card");
				ret=exchange_apdu_func(fd_des,"\x90\x0A\x00\x00\x01\x00\x00",7,0);
				//ret=exchange_apdu_func(fd_des,"\x90\xAA\x00\x00\x01\x00\x00",7,0);
				if((bApduResp[8]==0x91)&&(bApduResp[9]==0xAF)){

					CstcLog_printf("challenge data success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("challenge data failed");
				if(ret)
					goto END;

				break;

				//Prepare auth
			case 19:

				/***** sam ********/

				CstcLog_printf("Prepare Authentication.....");
				//ret=exchange_apdu_func(fd_sam,"\x80\x78\x80\x05\x08\x9D\xFA\x79\x6A\xDC\x84\x30\xF8\x00",14,1);

				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				memcpy(apdu_cmd, "\x80\x78\x80\x01\x08",5);
				memcpy(apdu_cmd + 5,&bApduResp,8);

				ret=exchange_apdu_func(fd_sam,apdu_cmd,13,1);
				if((bApduResp[0]==0x61)&&(bApduResp[1]==0x10)){

					CstcLog_printf("Authentication success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("Authentication failed");

				if(ret)
					goto END;
				break;


			case 20:

				CstcLog_printf("read authentcation data from sam");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x00\xC0\x00\x00\x10",5);

				ret=exchange_apdu_func(fd_sam,apdu_cmd,5,1);
				if((bApduResp[16]==0x90)&&(bApduResp[17]==0x00)){

					CstcLog_printf("Authentication data read success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("Authenticationdata read fail");
				if(ret)
					goto END;

				break;


			case 21:

				CstcLog_printf("DESFire Verify Authenticate");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x90\xAF\x00\x00\x10",5);
				memcpy(apdu_cmd + 5,&bApduResp,16);
				memcpy(apdu_cmd + 21, "\x00",1);
				ret=exchange_apdu_func(fd_des,apdu_cmd,22,0);
				if((bApduResp[8]==0x91)&&(bApduResp[9]==0x00)){

					CstcLog_printf("DESFire Verify Authentication success");
					//Show_Error_Msg("SAM authentication success");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("DESFire Verify Authentication fail");

				if(ret)
					goto END;

				break;

			case 22:

				CstcLog_printf("read authentcation data from sam");
				memset(apdu_cmd, 0, sizeof(apdu_cmd));
				//memcpy(apdu_cmd,"\x80\xCA\x81\x82\x0B",5);
				memcpy(apdu_cmd,"\x80\x7A\x00\x01\x08",5);
				memcpy(apdu_cmd + 5,&bApduResp,8);
				ret=exchange_apdu_func(fd_sam,apdu_cmd,13,1);

				if((bApduResp[0]==0x90)&&(bApduResp[1]==0x00)){

					CstcLog_printf("read authentcation data from sam succesfully");
					auth_sucess_flg =1;
					CstcLog_printf("Process Completed....");
				}
				else
					//Show_Error_Msg("SAM authentication failed");
					CstcLog_printf("read authentcation data from sam failed");
				if(ret)
					goto END;


				break;
//#endif
			default:
				break;
			}
		}
		//}
	}

	END:
	if (fd_sam >=0){
		CstcLog_printf("SAM Closed..");
		iccard_power_down(fd_sam);
		iccard_close_cardslot(fd_sam);
		fd_sam = -1;

        /*if (fd_des >=0){
			CstcLog_printf("mif_close..");
			emv_contactless_deactive_picc(fd_des);
			mif_close(fd_des);
			fd_des = -1;*/
		}





	if(num<= 22)
	{
		if (fd_des >=0){
			CstcLog_printf("mif_close..");
			emv_contactless_deactive_picc(fd_des);
			mif_close(fd_des);
			fd_des = -1;
		}

	}
	return 0;
}
/**************************************************************************************/
int exchange_apdu_func(int fd,char* apdu_cmd,int apdu_cmd_len,int is_sam_flag)
{
	int i;
	int ret=0;
	char szTemp[800];
	uint32_t uiApdu= 0;
	unsigned char bApdu[30];
	uint32_t uiApduResp= 0;
	//unsigned char bApduResp[300];

	uiApdu = 0;
	memset(bApdu,0,30);
	memset(bApduResp,0,300);
	//CstcLog_printf("apdu_cmd=%x",apdu_cmd);
	memcpy(bApdu + uiApdu, apdu_cmd,apdu_cmd_len);//get app id's
	uiApdu += apdu_cmd_len;




	memset(szTemp, 0, sizeof(szTemp));
	for (i = 0; i < (int)uiApdu; i++){

		sprintf(szTemp + strlen(szTemp), "%02X", bApdu[i]);
	}
	CstcLog_printf("Send Apdu:%s",szTemp);


	if(is_sam_flag)
		ret=iccard_exchange_apdu(fd, uiApdu, bApdu, &uiApduResp, bApduResp);
	else
		ret= emv_contactless_exchange_apdu(fd, uiApdu, bApdu, &uiApduResp, bApduResp);

	if (ret==0){
		memset(szTemp, 0, sizeof(szTemp));
		for (i = 0; i < (int)uiApduResp; i++){
			CstcLog_printf("bApduResp[%d]=%02x",i,bApduResp[i]);
			sprintf(szTemp + strlen(szTemp), "%02X", bApduResp[i]);

		}

		/*lcdDisplay(0,0,DISP_CFONT, "Card Response: %s",szTemp);
	 																lcdFlip();*/
		if(is_sam_flag)
			CstcLog_printf(" SAM Card Response: %s",szTemp);
		else
			CstcLog_printf(" Desfire Card Response: %s",szTemp);

	}
	else {

		CstcLog_printf("Exchage apdu failed");
		Show_Error_Msg("Exchage apdu failed");
		if (is_sam_flag){
			CstcLog_printf("SAM Closed..");
			iccard_power_down(fd);
			iccard_close_cardslot(fd);
			fd = -1;
		}
		else{
			CstcLog_printf("mif_close..");
			emv_contactless_deactive_picc(fd);
			mif_close(fd);
			fd = -1;
		}



		return -1;
	}


	return 0;

}



/*****************************************************************************************/







